

Installation instructions
###################

In summary you have to:
1) ensure that all the prerequisites are installed
2) build the code
3) set up an account on mysql

if you run the script 'ofsipreliminaries.sh' there's a chance it will do all thats needed


Operating system
-----------------
ubuntu works fine.  If you use V10.10 you'll be OK. There's no problem using an
older ubuntu except that you will have to install manually a newer version of Qt
you make need Qt version > Qt 4.7.3. we know it wont build under Qt 4.6.2.

Build Prerequisites
-----------------
 
You will need Qt, the cross-platform integrated development environment (IDE) 

        You'll probable have qt pre-installed, (it is on the recent ubuntus) but if not,(or just to check)  go:

	sudo apt-get install qt4-qmake


Compilers
	the code contains  c++ and fortran. 
	You can build the c++ with gcc (which come with ubuntu) or with the intel compilers.  The gnu fortran compiler isnt compatible with the code. You have to use the intel fortran compiler.  You can get a d 30-day demo license from intel.
	(would anyone like to contribute a port of the fortran code to gfortran?)

Libraries ..

        opennurbs
        we include a copy of opennurbs V5 . You'll need to go to ./thirdparty/opennurbs and go 'make clean'  then 'make'
        otherwise,  you can  download the opennurbs toolkit(C++)  from www.opennurbs.org , unzip it into ./thirdparty/opennurbs and follow its build instructions.

	SoQt 
	install the package.  On ubuntu you go:

 	sudo apt-get install libsoqt4-dev 

	That will also instal Coin3D, which is also required. 
    if that doesnt work, it may mean that your ubuntu version is no longer supported.
    You should upgrade your ubuntu,  

mysql
MySQL is an open-source database.  You can install it on the same machine, or somehere else on the network. To install it locally,the easiest way is to go Applications/Ubuntu Software Centre and install MySQL Administrator, MySQL Client, MySQL Server, MySQL Query Browser. 
I think that after all hat you'll have  the package libmysqlclient-dev 
 type 'sudo apt-get install  libmysqlclient-dev' to make sure. 

	

Downloading and Building  the code
--------------------

in you home directory, make a new folder named ofsi 
in that folder make a new one named  build

cd ~ 
mkdir ofsi
mkdir ofsi/build


In a terminal, go to the folder containing the file openFSI.pro and go:
(taking care with the case)

rm Makefile  */Makefile */*/Makefile
make clean
qmake
make

it will start compiling.  If 
then you ought to be able to go 'sudo make install' but

(jan 27 2012)  sudo make install  isnt configured  so  you have
to move a few things manually

cd (installdir)/rxserver
cp ../thirdparty/*/lib*  .

cd (installdir)/code/bin
cp  ../clients/rxclientbase/rxclientbase  .

now you can go ./rxserver to launch the app.

 the sample client executable is in (installdir)/clients/rxclientbase
 and is named 'rxclientbase'
 this app contains the demonstration  parameter editor, etc. You use it for trimming





sample Data
------------
ofsidata.tar.gz provides the support files needed by the app, and a sample dataset.




Run Prerequisites
-----------------
path
you'll need to set the path  so the kernel can see the libraries it needs.

mysql

OpenFSI.org requires a connection to the open-source database mysql. 
You can run openFSI.org without a database but many things will not work.

So before running for the first time you need to set up a mysql account.  You will find it helpful to install the tools MySQL Administrator and MySQL Query Browser 

We suggest an account name 'openfsi' with password 'openfsi'  That will be consistent with the demonstrations that come with the installation.

I prefer 'ofsi' so you could make a second user

from the command-line go:
mysql -h localhost -u root -p
.. and give the root password which you defined when you installed mysql.

you are now at the mysql prompt
let's create a user named'openfsi', create a database named 'ofsidb' and give the user 'openfsi' the right to read, write and change it. (you can paste the following into the
mysql prompt)

INSERT INTO mysql.user (Host,User,Password) VALUES('%','openfsi',PASSWORD('openfsi'));
INSERT INTO mysql.user (Host,User,Password) VALUES('%','ofsi',PASSWORD('ofsi'));
CREATE DATABASE ofsidb;
GRANT ALL ON ofsidb.* to openfsi@% identified by 'openfsi';
GRANT ALL ON ofsidb.* to ofsi@% identified by 'openfsi';
FLUSH PRIVILEGES;
quit



paraview
OpenFSI.org has a minimal internal post-processor.  To gain a good understanding of your designs you should use paraview (www.paraview.org). 
We recommend that you build paraview from source because this will let you use its TensorGlyph plug-in.

openfsi reference data

you will need some data and model files.  Unzip 'ofsidata' into your working directory

Your first run
##############

execute 'rxserver'
select the command-line (at the bottom of its screen) and type 'm' . Subsequently select the command 'macro' press 'enter'
This brings up a file dialog. Navigate to 'generic/BMG.mac'   (thats 'boat Main Genoa')
give it a minute or so to build the models (its the graphic system which is slow)  and then go 'calculate'

To trim 



Obvious contributions needed:
############################

The menusbars for the kernel and the client need implementing.  QML would be good for this. each menu button should execute a script command like 'snapshot'


Microsoft Windows.
If you really want to take a performance hit you could modify the .pro files to enable a Windows build.
My top-line Windows 7 machine now takes 10 minutes to boot....

Mac OS/X
enable a Mac build by modifying the .pro files.

gfortran - the gcc fortran compiler
 As far as I know this just means ifdeffing-out the useage of intel's Math Kernel Library mkl.  The challenge will be to provide alternative functions with equal performance.

Friendly clients.
The package includes 'rxclientbase' which is a minimal view on your openFSI world.
The idea is that if you want a particular 'look' you clone it and use it as a base for developing your own front-end components.

Connectivity
If you want your app to communicate with the ofsi kernel, you link it with the relevant bits of rxclientbase.


Enable other databases than mysql.
re-write the DB access section using the Qt database classes which provide drivers for most popular databases.

Integrate a paraview server.





