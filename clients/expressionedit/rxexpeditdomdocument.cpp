#include <QDomDocument>
#include <QFile>
#include <QStringList>
#include <QTextStream>

#include "rxexpeditdomdocument.h"

RXExpEditDomDocument::RXExpEditDomDocument( )
    :QDomDocument()
{
}

RXExpEditDomDocument::RXExpEditDomDocument(const QString&name)
        :QDomDocument(name)
    {
    }

RXExpEditDomDocument::~RXExpEditDomDocument( )

{
//    QString ss = this->toString();
//    qDebug()<<ss;
//    qDebug()<< "now compressed";
//    QByteArray b = qCompress(ss.toAscii());
//    qDebug()<< b.count()<<" was "<< ss.count();
//    QString cc(b);
//    qDebug()<<cc<<">thatsall";
}

int RXExpEditDomDocument::SetContentByString(const QString&lp)
{
    // typically
    //"boat$global$grid=0.5 \n

    QStringList hdrs;
    hdrs<<"model"<<"entity type"<<"entity name"<<"Expression"<<"value"<<"pansailFlags"<<"windfilesFactors"<<"nCycles"<<"wakefile";
  //  what<<"AeroType"<<"float"   <<"int"     <<"int"     <<"RFlags"<<"int"<<"file$*.uvp"<<"int"<<"file$*.wak";

    QDomElement root = createElement( "expressions" );
    this->appendChild( root );
    int nl,nw,i,j;
    QStringList lines =lp.split("\n");
    nl = lines.count();

    for(i=0;i<nl;i++  ){   // for each line
        if(lines[i].isEmpty())
            continue;
        QStringList phr= lines[i].split("=");
        if(phr.size()<2)
            continue;
        QStringList wds= phr[0].split("$"); nw = wds.count();
        QDomElement parentEl = root;
           QString nn;
        for(j=0;j<nw;j++)
        {
            QDomElement word ;
            bool parentExists = false;
            // for each level (column)...
            // if the word exists in the parent, it becomes our 'current'
            // else create one using the word and it becomes our current.
            QDomNodeList chn = parentEl.childNodes ();
            for(unsigned int k=0;k<chn.length(); k++)
            {
                QDomNode c =chn.item (k ) ;
                nn = c.nodeName();
                if(nn ==wds[j] ){
                    parentExists=true;
                    word =c.toElement();
                    break;
                }
            }
            if(!parentExists)
            {
                word=  createElement(wds[j] );
                parentEl.appendChild(word);
            }
            parentEl=word;
        }
        parentEl.setNodeValue(phr[1]);


    }
    return 1;
}
bool RXExpEditDomDocument::Write(const QString &fname)
{
    QString ss = this->toString(3);

    //<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    if(!ss.startsWith("<?xml"))
        ss.prepend("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    QFile ff(fname);
    if(!ff.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream out(&ff);
    out<<ss;
    //   d.documentElement().save(out,1);

    ff.close();
    return true;
}
QList<QDomNode>  RXExpEditDomDocument::GetEditedElements(QDomNode seed )
// find all nodes with attribute "edited"==1
// NOTE this function SKIPS THE ROOT OF THE TREE
{
    QList<QDomNode>  l, rc;
    QDomNode n ;
    QDomNodeList nl = seed.childNodes ();
    for(uint i=0; i<nl.length();i++){
        n=nl.at(i);
        QDomElement  e = n.toElement();
        QString aa = e.attribute("edited");
        if(aa == "1")
        {
            rc+=n;
        }
        l= GetEditedElements(n);
        rc+=l;
    }
    return rc;
}

