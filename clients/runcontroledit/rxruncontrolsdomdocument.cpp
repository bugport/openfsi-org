#include <QDomDocument>
#include <QFile>
#include <QStringList>
#include <QTextStream>

#include "rxruncontrolsdomdocument.h"

RXRunControlsDomDocument::RXRunControlsDomDocument( )
    :QDomDocument()
{
}

RXRunControlsDomDocument::RXRunControlsDomDocument(const QString&name)
        :QDomDocument(name)
    {
    }

RXRunControlsDomDocument::~RXRunControlsDomDocument( )

{
//    QString ss = this->toString();
//    qDebug()<<ss;
//    qDebug()<< "now compressed";
//    QByteArray b = qCompress(ss.toAscii());
//    qDebug()<< b.count()<<" was "<< ss.count();
//    QString cc(b);
//    qDebug()<<cc<<">thatsall";
}

int RXRunControlsDomDocument::SetContentByACString(const QString&lp)
{
    // typically
    //"1$4$-120.$0$0$/FI/HC/S6/LV/CC $0$ $1$NONE $
    //  2$4$-120.0$0$0$ $0$ $0$phase1.wak $
    //  3$4$-120.0$0$0$/FI/S6/D2 $0$ $99$phase2.wak $";
    //<?xml version=\"1.0\" encoding=\"UTF-8\"?>

// old defintions from summary.cpp
//            if(lp) 			 {		cp->phase = atoi(lp);
//             if((lp= strtok(NULL,"$"))) {		cp->press = atoi(lp);
//              if((lp= strtok(NULL,"$"))) {		cp->value = atof(lp);
//               if((lp= strtok(NULL,"$"))) {		cp->nrelax = atoi(lp); // pansail wake relaxations
//                if((lp= strtok(NULL,"$"))) {		cp->relax_flags = atoi(lp); // nonlinear/creasing
//                 if((lp= strtok(NULL,"$"))) {      	strcpy(cp->extra_flags_str ,lp); // /HC/FI etc
//                  if((lp= strtok(NULL,"$"))) {	cp->pansail_flags = atoi(lp); // old wakes, old matrix, etc
//                   if((lp= strtok(NULL,"$"))) {     	strcpy( cp->windfile ,lp);  // some UVDps
//                    if((lp= strtok(NULL,"$"))) {	cp->ncycle = atoi(lp); // no of FSI iterations
//                     if((lp= strtok(NULL,"$"))) {   	strcpy(cp->wakefile ,lp); // pansail wakefile

    QStringList hdrs,what;
    hdrs<<"CFDType"<<"Pressure"<<"NWakeRel"<<"Wakerelax_Flags"<<"StructFlags"<<"pansailFlags"<<"windfilesFactors"<<"nCycles"<<"wakefile";
    what<<"AeroType"<<"float"   <<"int"     <<"int"     <<"RFlags"<<"int"<<"file$*.uvp"<<"int"<<"file$*.wak";
    //   QDomProcessingInstruction header = createProcessingInstruction( "xml","version=\"1.0\" encoding=\"UTF-8\"");
    //    this->appendChild( header );

    QDomElement root = createElement( "controls" );
    this->appendChild( root );
    int nw,i,j,k;
    QStringList wds =lp.split("$");
    nw = wds.count(); nw--;
    for(k=0,i=0,j=0;i<nw;  ){
        QDomElement phase =  createElement(QString("Phase%1").arg(k++) );
        i++;
        root.appendChild(phase);
        for(j=0;j<9&&i<nw;i++,j++) {
//            if(0){
//                QDomElement word=  createElement(hdrs[j] );
//                word.setNodeValue(wds[i]);
//                word.setAttribute("value",wds[i]);
//                phase.appendChild(word);
//            }
//            else if(0) {
//                QDomCharacterData word=  createCDATASection(hdrs[j] );
//                ; //word.setTagName(hdrs[j]);
//                word.setNodeValue(hdrs[j]);
//                word.setData(wds[i]);
//                phase.appendChild(word);
//            }
//            else if(0) {
//                QDomText word=  createTextNode(hdrs[j] );
//                ; //word.setTagName(hdrs[j]);
//                word.setNodeValue(hdrs[j]);
//                word.setData(wds[i]);
//                phase.appendChild(word);
//            }
//            else
                if(1) {
                QDomElement word=  createElement(hdrs[j] );

                phase.appendChild(word);
                QDomText xx=  createTextNode(hdrs[j] );
                xx.setData(wds[i]);
                word.setAttribute("rxwhat",what[j]);
                word.appendChild(xx);

            }

        }

    }
    return 1;
}
bool RXRunControlsDomDocument::Write(const QString &fname)
{
    QString ss = this->toString(3);

       //<?xml version=\"1.0\" encoding=\"UTF-8\"?>
    if(!ss.startsWith("<?xml"))
        ss.prepend("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
    QFile ff(fname);
    if(!ff.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;

    QTextStream out(&ff);
   out<<ss;
 //   d.documentElement().save(out,1);

    ff.close();
    return true;
}
