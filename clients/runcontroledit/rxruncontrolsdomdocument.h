#ifndef RXRCDOMDOCUMENT_H
#define RXRCDOMDOCUMENT_H
#include <QObject>
 #include <QDomDocument>

 class QString;

//! [0]
class RXRunControlsDomDocument:
        public QDomDocument
{
 //     Q_OBJECT



public:
    RXRunControlsDomDocument();
    RXRunControlsDomDocument(const QString&name);
    ~RXRunControlsDomDocument();
    int SetContentByACString(const QString &lp);
    bool Write(const QString &fname);
signals:

public slots:

};
//! [0]

#endif // RXRCDOMDOCUMENT_H
