QT       += core gui
QT           += network

TARGET = basictrimwindow
TEMPLATE = app

DEFINES      += RLX

INCLUDEPATH += ../rxcommon
INCLUDEPATH +=../rxserver



HEADERS    += basictrimwindow.h
SOURCES    += main.cpp \
              basictrimwindow.cpp
CONFIG     += qt

# install
#target.path = /home/r3/Documents/qt/basictrimmenu

#sources.files = $$SOURCES $$HEADERS $$RESOURCES *.pro
#sources.path = /home/r3/Documents/qt/basictrimmenu
#INSTALLS += target sources

#symbian: include($$PWD/../../symbianpkgrules.pri)
#maemo5: include($$PWD/../../maemo5pkgrules.pri)

