/*
rxclientbase is the prototype for all clients to the relax kernel-server.
 This client just does:
2) post any text coming from the server to the screen
3) Let User enter a relax command which it passes to the kernel.
  It would be polite if it ran its comms in a second thread so long transfers don't
    block the GUI.

 Server-side there will be exactly one RXSocketThread which talks to this client.
 A RXSocketThread is the server-side proxy of a client.  All Relax objects belong
 either to a RXSocketThread or to the kernel.
 The RXSocketThread persists after an abnormally terminated call. It becomes an 'orphan'.
 RXSocketThread s are destroyed by a correctly terminated call. Then the objects belonging to the
RXSocketThread are (on user choice) destroyed or assigned to the kernel.

An Orphan RXSocketThread directs its output to the kernel-server.
(which probably ignores it)

rxclientbase has to:
1) offer a connection dialog to ring up the server.
    When we call, we ask the server to list the orphan socket-servers. (unless User says not to)
    We have the option to pick up one of them.
    If we choose not to, the server will make a new RXSocketThread for us.

2) be able to live in 'connected' or 'unconnected' state.


On normal exit it tells the server whether to delete its RXSocketThread (and hence its RX objects)
or whether to leave its RXSocketThread as an orphan.

On abnormal exit it leaves its RXSocketThread as an orphan.

 */

#include <QTableView>
#include <QtNetwork>
#include <QSqlDatabase>
#include <QSqlTableModel>
#include <QSqlError>
#include <QtGlobal>
#ifdef COIN3D
#include <Inventor/Qt/SoQt.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/nodes/SoSeparator.h>
#endif
#include <iostream>
#include <stdio.h>
using namespace std;
#include "rxclientbase.h"
#include "ui_rxclientbase.h"
#include <QDebug>
#include "StdRedirector.h"
#include "rxexpeditsubwindow.h"
#include "rxruncontrolsubwindow.h"
#include "rxdbmirrortable.h"
#include "rxdbcelledit.h"
#include "databasedialog.h"

rxclientbase::rxclientbase(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::rxclientbase),
    myRedirector(0),
    tcpSocket(0),
    m_ServerAddress (QHostAddress::LocalHost),
    m_ServerPort  (61291) ,
    m_HasCommands(false),
    m_once(false),
    m_millisecs(5000),
    m_dbdialog(0),
    m_bytesOutstandingCount(0),
    m_bytesAvailable(0)
{
    ui->setupUi(this);


    QTextBrowser * tb =  ui->textBrowser;
    myRedirector = new StdRedirector<>( std::cout, rxclientbase::outcallback,(void *)tb );

    tcpSocket = new QTcpSocket(this);

    connect(tcpSocket, SIGNAL(readyRead()), this, SLOT(readOnSocket()));
    connect(tcpSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));

    connect(ui->actionExit,SIGNAL(triggered()),SLOT(close()));
    connect(ui->actionConnect,SIGNAL(triggered())  ,SLOT(ConnectToServer()));
    connect(ui->actionRun_Control,SIGNAL(triggered()),SLOT(NewRunControlEditor()));
    connect(ui->actionTrim,SIGNAL(triggered()),SLOT(NewTrimEditor()));
    connect(ui->actionExpressions ,SIGNAL(triggered()),SLOT(NewExpressionEditor()));
    connect(ui->actionDB_mirror_view,SIGNAL(triggered()),SLOT(onDBDlg()  ));//NewDBMirrorEditor()
#ifdef COIN3D
    connect(ui->actionViewer,SIGNAL(triggered()),SLOT( NewViewer() ));
#endif
    connect(ui->actionAbout,SIGNAL(triggered()),SLOT(OnAbout()));

    connect(ui->actionSendMessage,SIGNAL(triggered()) ,this,SLOT(onSendTestMessage()));
    connect(ui->actionDatabase_Connect,SIGNAL(triggered()) ,this,SLOT(onDBDlg()  ))  ;
    this->connect(this,SIGNAL(onReceiveText(QString)),this,SLOT(DisplayText(QString)));

    m_args= QCoreApplication::arguments ();
    // this->ProcessArguments(); // called too early
}

rxclientbase::~rxclientbase()
{
    if(tcpSocket)  delete tcpSocket ;tcpSocket=0;
    if(myRedirector ) delete  myRedirector;myRedirector=0;
    if(this->m_dbdialog) delete  this->m_dbdialog;m_dbdialog=0;

    if(ui) delete ui; ui=0;
}
void rxclientbase::OnAbout() {
     QString text = " this is OpenFSI.org (ofsi to its friends)"
            " written by Peter Heppel and colaborators since 1980\n\n"

            "with special thanks to Peter Ronan Rice for the concept\n\n"

            "Thanks also to the many open-source authors who have made it possible.\nIn no particular order:\n\n"
            "John Biddiscombe , Jerome Soumagne , Guillaume Oger , David Guibert , Jean-Guillaume Piccinali\n"
            " Parallel Computational Steering and Analysis for HPC Applications\n"
            " using a ParaView Interface and the HDF5 DSM  Virtual File Driver\n"
            " Eurographics Symposium on Parallel Graphics and Visualization (2011) \n\n"
            "Mathieu Jacques for his excellent MTParser which we have extended\n"
            "Jonathan Richard Shewchuk,  Triangle: Engineering a 2D Quality Mesh Generator and Delaunay Triangulator, in ``Applied Computational Geometry: Towards Geometric Engineering''  volume 1148 of Lecture Notes in Computer Science, pages 203-222, Springer-Verlag, Berlin, May 1996.\n"

            "Mathieu Desbrun, Mark Meyer, and Pierre Alliez. Intrinsic Parameterizations of Surface Meshes. In Eurographics ’02 Proceed ings, 2002.\n"

            "Coin3D\n"
            "the GNU xml library\n"
            "sparskit\n"
            "openNURBS"
            "\n Sofia Werner for her sail++ panel method - which is integrated AS IS\n"
            "and of  course the people at Qt\n"
            "\nreleased under LGPL license\n"

            "\nThe program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.\n"


            "\nFor reference documentation you can try the rather dated www.peterheppel.com/r2_doc \n"

            "\n(Hint for getting started: type a space into the command-line box at the bottom)\n"
            "\nbuild date: " __DATE__ " " __TIME__ " "
        #ifdef  __INTEL_COMPILER
              "\nCompiler (Intel)        : __INTEL_COMPILER"
        #endif
       #ifdef __GNUG__
             "\nCompiler (g++)        :   __GNUG__   __GNUC_MINOR__  "
       #endif
            "\nbuilt with Qt Version: "  QT_VERSION_STR
            "\nrunning    Qt Version: ";
    text +=   qVersion();


    QMessageBox::about (this, "About OpenFSI.org", text );
}
// see gnu getopt_long . http://www.gnu.org/software/libc/manual/html_node/Getopt-Long-Option-Example.html#Getopt-Long-Option-Example
int rxclientbase::ProcessArguments(){
    QString ss ;
    //   m_args= QCoreApplication::arguments();
    qDebug()<<m_args;
    if(m_args.contains("--help",Qt::CaseInsensitive)) PrintHelp();
    if(m_args.contains("--server",Qt::CaseInsensitive)) SetServer();
    if(m_args.contains("--port",Qt::CaseInsensitive)) SetPort();
    if(m_args.contains("--exec",Qt::CaseInsensitive))  {
        int ii = m_args.indexOf("--exec",Qt::CaseInsensitive);

        if(ii >=0) {
            if(ii <m_args.count()-1)
            { ss = m_args[ii+1];
                qDebug()<<" initial command = "<<ss;
            }
        }

        SetExec(ss);
    }
    if(m_args.contains("--once",Qt::CaseInsensitive)){
        int ii = m_args.indexOf("--once",Qt::CaseInsensitive);

        if(ii >=0) {
            if(ii <m_args.count()-1)
            { ss = m_args[ii+1];
              this->m_millisecs =ss.toInt();
            }
        }

        SetOnce();
    }
    if(m_args.contains("--connect",Qt::CaseInsensitive))ConnectToServer();
    return 1;
}
void rxclientbase::PrintHelp(){
    cout<<"syntax :\n" ;
    cout<<m_args[0].toStdString().c_str() ;
    cout<<" [--server <URL>]\n --port <portno> \n[--connect]\n[-exec semi-colon-separated RX commands (in quotes)]\n[--once[millisecs]]";
    cout<<"\ndefault server is localhost.\ndefault port is 61291 because that is the port on which rxserver listens";

    cout<<endl;

    qDebug()<<"syntax :" ;
     qDebug()<<m_args[0].toStdString().c_str() ;
     qDebug()<<" [--server <URL>]\n --port <portno> \n[--connect]\n[-exec semi-colon-separated RX commands (in quotes)]\n[--once [millisecs]]\n";
     qDebug() <<"\ndefault server is localhost.\ndefault port is 61291 because that is the port on which rxserver listens \n";

    ;}
void rxclientbase::SetServer()
{
    int i = m_args.indexOf("--server",Qt::CaseInsensitive);
    QString v = m_args[i+1];
    this->m_ServerAddress=QHostAddress(v);
}

void rxclientbase::SetPort(){
    int i = m_args.indexOf("--port",Qt::CaseInsensitive);
    QString v = m_args[i+1];
    this->m_ServerPort=v.toInt();
}
void rxclientbase::SetExec(const QString &s){
    this->m_HasCommands=true;
    this->m_CommandToRun=s;
}
void rxclientbase::SetOnce(){ this->m_once=true;  }

void rxclientbase::onSendTestMessage() {
    WriteOnSocket( QString ("this is a test message from a client"));
}


void rxclientbase::on_rxcommandline_returnPressed()
{
    QString myline = ui->rxcommandline->displayText();
    if(!myline.isEmpty())
    {
        WriteOnSocket(myline);
        ui->rxcommandline->PushToHistory();
        ui->rxcommandline->clear();
    }
}
void rxclientbase::NewTrimEditor()
{
    // roughly
    // issue a 'getheaders:input'
    // create a tree by parsing by '$'
    // for each entry, do a 'getdata'

}
void rxclientbase::HelloMessage( )
{
    this->connect(this,SIGNAL(onReceiveText(QString)),this,SLOT(FirstMessage()));
    WriteOnSocket(QString("Hello from client"));
}
void rxclientbase::FirstMessage( )
{
    this->disconnect(this,SIGNAL(onReceiveText(QString)),0,0);
    cout<<"FirstMessage '"<<qPrintable(m_CommandToRun) <<"'"<<endl;
    if(this->m_HasCommands)
    {
        WriteOnSocket(this->m_CommandToRun);
        this->m_CommandToRun.clear();
        this->m_HasCommands=false;
        if(this->m_once) {
         //   this->parentWidget()->deleteLater();
            QTimer *timer = new QTimer(this); timer->setSingleShot(true);
            connect(timer, SIGNAL(timeout()), this, SLOT(close()));
                timer->start(this->m_millisecs);
// the --once flag isnt very clever.
// The hope is that it can get its work done in 5 seconds.
// but anyway (Jan 2014 Qt 4.8.3) deleteLater causes a crash on ~rxclientbase
// a better technique is to send a 'broadcast: quitallclients'
// the client(s) will quit when they receive this.
// if issued after a 'calculate' , normally this command will be executed when the analysis
// is in its first few cycles.
// if you want the quit to happen after the analysis, you should send
//setting: after_each_run: "broadcast:quitallclients"
        }
    }
    this->connect(this,SIGNAL(onReceiveText(QString)),this,SLOT(DisplayText(QString)));
}

void rxclientbase::DisplayText(const QString &s)
{
    cout<<qPrintable(s)<<endl;
        if(s.contains("quitallclients",Qt::CaseInsensitive))
            close();
       if(s.startsWith("(from RX): (broadcast)models",Qt::CaseInsensitive))
            this->ui->rxcommandline->SetModelList(s);
}

void rxclientbase::NewExpressionEditor()
{
    this->disconnect(this,SIGNAL(onReceiveText(QString)),0,0); // try Jan 2014 to see if it speeds things
    this->connect(this,SIGNAL(onReceiveText(QString)),this,SLOT(PopulateExpressionEditor(QString)));
    WriteOnSocket(QString("getExpressions"));
}

void rxclientbase:: PopulateExpressionEditor(const QString &tin)// should start with '"##ExpList##"'
{
    QString text; text=tin;
    if(text.left(32).contains("##ExpList##"))
    {
        int ii = text.indexOf("##ExpList##")+11;
        text=tin.mid(ii);
    }
    else
    {
        qDebug()<<" Expression editor skipping message "<<tin.left(80);
        return ;
    }
    this->disconnect(this,SIGNAL(onReceiveText(QString)),0,0);

    RXExpEditSubWindow  *rcsw =new RXExpEditSubWindow( ); //derived from QWidget
    connect(rcsw,SIGNAL(Send(QString)),this,SLOT(WriteOnSocket(QString)));
    rcsw->openText(text);
    rcsw->setMinimumSize(400,400);
    ui->mymdiArea->addSubWindow(rcsw);
    rcsw->show();
    this->connect(this,SIGNAL(onReceiveText(QString)),this,SLOT(DisplayText(QString)));
}

void rxclientbase::NewRunControlEditor()
{
    qDebug()<< "NewRunControlEditor";
    if(0) {
        QMdiSubWindow *subwindow = new QMdiSubWindow ();

        subwindow->setObjectName(QString::fromUtf8("Run Control"));
        subwindow->setAttribute(Qt::WA_DeleteOnClose);
        subwindow->setWindowTitle(tr("Run Control"));
        subwindow->setMinimumSize(500,500);

        RXRunControlSubWindow  *rcsw =new RXRunControlSubWindow(); //derived from QWidget
        rcsw->setParent(subwindow);
        subwindow->setWidget(rcsw);
        rcsw->setMinimumSize(900,900);
        rcsw->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
        rcsw->setAttribute(Qt::WA_DeleteOnClose);
        rcsw->openText(); // here we ask the serve for 'runcontrols'
        ui->mymdiArea->addSubWindow(subwindow,Qt::SubWindow);
        subwindow->show();
    }
    else
    {
        RXRunControlSubWindow  *rcsw =new RXRunControlSubWindow( ); //derived from QWidget
        rcsw->openText();// here we ask the serve for 'runcontrols'
        rcsw->setMinimumSize(400,400);
        ui->mymdiArea->addSubWindow(rcsw);
        rcsw->show();
    }


}
#ifdef COIN3D
void rxclientbase::NewViewer()
{
    QMdiSubWindow *subwindow = new QMdiSubWindow ();

    subwindow->setObjectName(QString::fromUtf8("Viewer"));
    subwindow->setAttribute(Qt::WA_DeleteOnClose);
    subwindow->setWindowTitle(tr("simple coin viewer"));
    subwindow->setMinimumSize(300,300);

    ui->mymdiArea->addSubWindow(subwindow,Qt::SubWindow);
    QFrame *view = new QFrame(subwindow);

    view->setParent(subwindow);
    view->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    subwindow->setWidget(view);
    view->setAttribute(Qt::WA_DeleteOnClose);

    // Use one of the convenient SoQt viewer classes.
    SoQtExaminerViewer * eviewer = new SoQtExaminerViewer(view);

    // to hook up a model we go:

    SoSeparator * root = new SoSeparator;
    root->ref();
    SoGroup *groot = new SoGroup; //

    root->addChild(groot);
    // our model tree is a tree of SoGroup*'s.   We should pass back groot
    // and it becomes roughtly  equivalent to the HOOPS modelsegment.

    eviewer->setSceneGraph(root);


    SoQt::show(view);

    // to clean up we need to::
    //   delete eviewer;
    //   root->unref();
}
#endif
void rxclientbase::onDBDlg() {
    if (m_dbdialog) delete m_dbdialog;
    m_dbdialog = new DatabaseConnectionDialog(this);

    QSettings settings("openfsi", "basicclient");




    // optional: set the data that will be presented to the user as auto-filled form
    int portno = settings.value("dbdlg/portno").toInt() ;
    m_dbdialog->setDatabaseName(settings.value("dbdlg/dbname").toString());
    m_dbdialog->setDatabasePortNumber(portno );
    m_dbdialog->setDatabaseHostName(settings.value("dbdlg/hostname").toString() );
    m_dbdialog->setDatabaseUsername(settings.value("dbdlg/username").toString() );
    m_dbdialog->setDatabaseDriverName(settings.value("dbdlg/dbDriverName").toString() );
    m_dbdialog->setDatabasePassword(settings.value("dbdlg/dbPW").toString());

    // enable the connect button if all the data is correct
    m_dbdialog->checkFormData();
    // connect the dialog signal to a slot where I will use the connection
    connect( m_dbdialog,
             SIGNAL(databaseConnect(QSqlDatabase&)),
             this,
             SLOT(slotHandleNewDatabaseConnection(QSqlDatabase&)));

    // show the dialog (without auto-connection)
    m_dbdialog->run( false );

}
void rxclientbase::slotHandleNewDatabaseConnection(QSqlDatabase& db)
{
    // 1) try to connect to a DB
    // 2  create a model
    // 3) create a MDI table view
    // 4) set up a timer to call select() every so often.

    bool ok = db.open();
    if(!ok) {
        QSqlError ee= db.lastError () ;
        cout<<qPrintable(ee.text());
    }


    QSettings settings("openfsi", "basicclient");

    settings.setValue("dbdlg/portno"      ,db.port());
    settings.setValue("dbdlg/dbname"      ,db.databaseName());
    settings.setValue("dbdlg/hostname"    ,db.hostName());
    settings.setValue("dbdlg/username"    ,db.userName());
    settings.setValue("dbdlg/dbDriverName",db.driverName());
    settings.setValue("dbdlg/dbPW"        ,db.password());

    QMdiSubWindow *subwindow = new QMdiSubWindow ();

    subwindow->setObjectName(QString::fromUtf8("Mirror"));
    subwindow->setAttribute(Qt::WA_DeleteOnClose);
    subwindow->setWindowTitle(tr("Mirror DB"));
    subwindow->setMinimumSize(400,250);

    RXdbMirrorModel *model = new RXdbMirrorModel(this, db);
    model->setTable("mirror");
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->select();
    model->removeColumn(3); // don't show the ID
    model->removeColumn(2); // don't show the ID

    ui->mymdiArea->addSubWindow(subwindow,Qt::SubWindow);
    QTableView *view = new QTableView(subwindow);
    view->setModel(model);
    view->setParent(subwindow);
    view->setItemDelegateForColumn(1, &(model->m_tbDelegate));
    view->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    subwindow->setWidget(view);
    view->setAttribute(Qt::WA_DeleteOnClose);

    view->setColumnWidth(0,220);
    view->setColumnWidth(1,180);
    subwindow->show();
    if(!model->m_DBMirrorTimer)
    {
        model->m_DBMirrorTimer = new QTimer(this);
        model-> m_DBMirrorTimer->setSingleShot(false);
        connect(model->m_DBMirrorTimer, SIGNAL(timeout()), model, SLOT(refresh()));
        model->m_DBMirrorTimer->start(2000);
    }
}
void rxclientbase::NewDBMirrorEditor()
{
    // 1) try to connect to a DB
    // 2  create a model
    // 3) create a MDI table view
    // 4) set up a timer to call select() every so often.


    QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("ofsidb");
    db.setUserName("openfsi");
    db.setPassword("openfsi");
    bool ok = db.open();
    if(!ok) {
        QSqlError ee= db.lastError () ;
        cout<<qPrintable(ee.text());
    }

    QMdiSubWindow *subwindow = new QMdiSubWindow ();

    subwindow->setObjectName(QString::fromUtf8("Mirror"));
    subwindow->setAttribute(Qt::WA_DeleteOnClose);
    subwindow->setWindowTitle(tr("Mirror DB"));
    subwindow->setMinimumSize(300,300);

    RXdbMirrorModel *model = new RXdbMirrorModel(this, db);
    model->setTable("mirror");
    model->setEditStrategy(QSqlTableModel::OnFieldChange);
    model->select();
    model->removeColumn(3); // don't show the ID
    model->removeColumn(2); // don't show the ID

    ui->mymdiArea->addSubWindow(subwindow,Qt::SubWindow);
    QTableView *view = new QTableView(subwindow);
    view->setModel(model);
    view->setParent(subwindow);
    view->setItemDelegateForColumn(1, &(model->m_tbDelegate));
    view->setSizePolicy(QSizePolicy::Ignored,QSizePolicy::Ignored);
    subwindow->setWidget(view);
    view->setAttribute(Qt::WA_DeleteOnClose);

    view->setColumnWidth(0,180);
    view->setColumnWidth(1,180);
    subwindow->show();
    if(!model->m_DBMirrorTimer)
    {
        model->m_DBMirrorTimer = new QTimer(this);
        model-> m_DBMirrorTimer->setSingleShot(false);
        connect(model->m_DBMirrorTimer, SIGNAL(timeout()), model, SLOT(refresh()));
        model->m_DBMirrorTimer->start(2000);
    }
}


void rxclientbase::ConnectToServer()
{
#ifdef Q_OS_SYMBIAN
    if(!isDefaultIapSet) {
        qt_SetDefaultIap();
        isDefaultIapSet = true;
    }
#endif
    m_bytesOutstandingCount = 0; m_dataComingInBuffer.clear();
    tcpSocket->abort();
    connect(tcpSocket,SIGNAL(connected()),this, SLOT(HelloMessage()));
    tcpSocket->connectToHost( m_ServerAddress, m_ServerPort) ;

    if (tcpSocket->waitForConnected(1000))
        qDebug("Connected to the RX server OK!");
    else{
        qDebug()<<"Connect failed:port="<<m_ServerPort<<" server= "<<m_ServerAddress.toString()  <<endl;
        qDebug()<<"error ="<< tcpSocket->errorString()<<endl;
    }
}

void rxclientbase::WriteOnSocket(const QString &s)
{
    static int messagecount;
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_7);
    out << (quint16)s.length();
    out << s;
    out.device()->seek(0);
    out << (quint16)(block.size() - sizeof(quint16));

    qint64  rc = tcpSocket->write(block);
    if(rc<0)
        cout<<"(socket not connected "<<endl;
    else
        cout<<"("<<messagecount++ <<") Client Sent "<< qPrintable(QDateTime::currentDateTime ().toString()) <<  " '" <<s.toStdString()<<"'"<<endl;
}
#define EXPERIMENT
#ifdef NEVER

void rxclientbase::readOnSocket( )// for small packets only - but it works
{
    QString s;
    qint64 ba;
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_7);
    qint64 blockSize=0;
    if (blockSize == 0) {
        if (tcpSocket->bytesAvailable() < (int)sizeof(qint64))
            return;
        in >> blockSize;
    }
    ba = tcpSocket->bytesAvailable();qDebug()<< "announced ="<<blockSize<<" available="<<ba;
    if (ba < blockSize)
        return;
    in >> s;//rcvd;    s = QString(rcvd);
    qDebug()<<"after received <"<<s<<  "  ba is"<<tcpSocket->bytesAvailable() ;

    // to act on the message received, we must have already connected the action to this signal
    // the default action is just to print to the text window.
    emit this->onReceiveText(s);
}
#elif defined (EXPERIMENT)
// the above showed that with quint64s, we announce 46 then send 54 (46+8)
// we then receive the number 46 and the string.
// but sometimes there is a logjam - so we should only read the text up until we have the announced number of bytes
void rxclientbase::readOnSocket( )
{
    /*   members.
    qint64 m_bytesOutstandingCount  ;
    QString m_dataComingInBuffer;
    qint64 m_bytesAvailable; */
    QString s;
    qint64  nBytesRead=0, oldba=0 , newba=0;
    QByteArray rcvd;
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_7);
    if (m_bytesOutstandingCount <= 0) // a new header is coming in.
    {
        Q_ASSERT (m_dataComingInBuffer.size()==0);
        if (tcpSocket->bytesAvailable() < (int)sizeof(quint64))
        {
            qDebug()<<  "starting, but"<< tcpSocket->bytesAvailable() << " less than"<< sizeof(quint64)<<"are available, so return" ;
            return;
        }
        in >> m_bytesOutstandingCount; //  (qint64)
        m_bytesOutstandingCount-=sizeof(qint32 );
        qDebug()<<" new message announces length ="<< m_bytesOutstandingCount;
    } // a new message
    else
    {
        qFatal("We shouldntbe here");
    }
    oldba =   tcpSocket->bytesAvailable();
    qDebug()<<oldba<< " are available "<< m_bytesOutstandingCount <<" expected " ;

    // we should read no more than  m_bytesOutstandingCount bytes, even though there may be more in the pipeline.
    // once the count is zero, we act and return
    // else we go round again and try to read  m_bytesOutstandingCount

    // this only works for small data   in>>rcvd;
    char buf[oldba+1];
    nBytesRead = in.readRawData(buf,oldba); // the first 4 bytes are the string length
    nBytesRead -=sizeof(qint32)  ;
    rcvd= QByteArray(buf+sizeof(qint32),nBytesRead);
    qDebug() << "Start Rcv  qba len= "<<rcvd.size();
    newba =   tcpSocket->bytesAvailable();

    if(nBytesRead<0) {
        qDebug()<<" socket error";
        return;
    }

    m_bytesOutstandingCount-=nBytesRead;
    while (m_bytesOutstandingCount>0 )
    {
        qDebug()<<"waitForReadyRead with  m_bytesOutstandingCount="<<m_bytesOutstandingCount;
        if(!tcpSocket->waitForReadyRead() )
            break;
        QByteArray a;
        int nnaa =  tcpSocket->bytesAvailable();
        qDebug()<<"looping read with "<<     nnaa;
        char buf[nnaa+1];
        nBytesRead = in.readRawData(buf,nnaa);
        a= QByteArray(buf,nBytesRead);
        qDebug() << "(re-read loop) received qba len= "<<a.size();

        m_bytesOutstandingCount-=  nBytesRead;

        qDebug() <<"(re-read loop) " <<nBytesRead<<" bytes were just read. n outstanding= "<< m_bytesOutstandingCount;
        rcvd+=a;
        if ( m_bytesOutstandingCount<=0)
            break;
    }
    m_dataComingInBuffer.append( rcvd);

    emit this->onReceiveText(m_dataComingInBuffer );
    m_dataComingInBuffer.clear();
}
#else
void rxclientbase::readOnSocket( )// this version triess to read the whole QString
{
    QString s;
    qint64 ba;
    QDataStream in(tcpSocket);
    in.setVersion(QDataStream::Qt_4_7);
    qint64 blockSize=0;
    if (blockSize == 0) {
        if (tcpSocket->bytesAvailable() < (int)sizeof(qint64))
            return;
        in >> blockSize;
    }
    ba = tcpSocket->bytesAvailable();qDebug()<< "announced ="<<blockSize<<" available="<<ba;
    //    if (ba < blockSize)
    //        return;
    in >> s;
    qDebug()<<"after received <"<<s<<  "  ba is"<<tcpSocket->bytesAvailable() ;

    // to act on the message received, we must have already connected the action to this signal
    // the default action is just to print to the text window.
    emit this->onReceiveText(s);
}
#endif

void rxclientbase::displayError(QAbstractSocket::SocketError socketError)
{
    switch (socketError) {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        QMessageBox::information(this, tr("rx Client"),
                                 tr("The host was not found. Please check the "
                                    "host name and port settings. : ")
                                 + this->m_ServerAddress.toString()
                                 +QString(",   port : %1" ).arg(this->m_ServerPort ) );
        break;
    case QAbstractSocket::ConnectionRefusedError:
        QMessageBox::information(this, tr("rx Client"),
                                 tr("The connection was refused by the OFSI server. "
                                    "Make sure the server is running on  ")
                                 + this->m_ServerAddress.toString()
                                 +QString(",   port : %1" ).arg(this->m_ServerPort ) );
        break;
    default:
        QMessageBox::information(this, tr("rx Client"),
                                 tr("The following error occurred: %1.")
                                 .arg(tcpSocket->errorString()));
    }
}


void rxclientbase::outcallback( const char* ptr, std::streamsize count, void *pTextBox )
{
    (void) count;
    QTextBrowser * p = static_cast< QTextBrowser * >( pTextBox );

    QString input;
    static QString lLine;
    const char *lp = ptr;
    int i;
    for(i=0;i<count;i++,lp++){
        input.append(*lp);
    }
    // qDebug()<<"(RXMainWindow::outcallback)input string is" <<input<<endl;
    // logic::
    // if input doesnt contain a EOL, we append it to lLine and return
    //  if it does contain a EOL, {
    //     we append it up to but not including the EOL , to lLine
    //     we remove the first chars from input;
    //     we send lLine.
    //     we flush lLine
    //      if input is short we break;
    //    }
    for(;!input.isEmpty();){
        i = input.indexOf("\n");
        if(i<0){
            lLine.append(input);
            return;
        }
        lLine.append(input.left(i)); input.remove(0,i+1);
        p->append( lLine );  lLine.clear();
    }
}

void rxclientbase::on_subwindow_destroyed()
{

}
