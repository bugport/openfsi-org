#ifndef RXUSERCHOICEDLG_H
#define RXUSERCHOICEDLG_H

#include <QDialog>

namespace Ui {
    class rxUserChoicedlg;
}

class rxUserChoicedlg : public QDialog
{
    Q_OBJECT

public:
    explicit rxUserChoicedlg(QWidget *parent = 0);
    ~rxUserChoicedlg();

private:
    Ui::rxUserChoicedlg *ui;
};

#endif // RXUSERCHOICEDLG_H
