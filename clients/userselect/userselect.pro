#-------------------------------------------------
#
# Project created by QtCreator 2012-10-08T09:48:44
#
#-------------------------------------------------

QT       += core
QT += widgets

TARGET = userselect
TEMPLATE = app


SOURCES += main.cpp\
        rxuserchoicedlg.cpp

HEADERS  += rxuserchoicedlg.h

FORMS    += rxuserchoicedlg.ui
