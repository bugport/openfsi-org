#-------------------------------------------------
#
# Project created by QtCreator 2014-04-12T10:20:33
#
#-------------------------------------------------

QT -= gui

TARGET = nrrk
TEMPLATE = lib
CONFIG  +=staticlib

include (../../openfsicompilerconfig.pri)


INCLUDEPATH +=~/software/nr
INCLUDEPATH +=   ../../../../../software/nr/cpp/other
INCLUDEPATH += ../../src/preprocess
INCLUDEPATH += ../../thirdparty/opennurbs
INCLUDEPATH += ../../src/stringtools
INCLUDEPATH += ../../thirdparty/MTParserLib
INCLUDEPATH += ../../src/ODE



SOURCES +=  \
# ../../../../../software/nr/cpp/recipes/ludcmp.cpp \
#../../../../../software/nr/cpp/recipes/lubksb.cpp \
#    nrrk.cpp \
    ../../src/ODE/rxodestepper.cpp \
    ../../src/ODE/rxrungekutta.cpp \
    ../../src/ODE/rxstiffsolver.cpp \
    ../../src/ODE/rxstiffsolve_invmass.cpp



HEADERS += nrrk.h\
        nrrk_global.h \
    ../../../../../software/nr/cpp/other/nr.h \
    stdafx.h \
    ../../src/ODE/rxodestepper.h \
    ../../src/ODE/rxrungekutta.h \
    ../../src/ODE/rxstiffsolver.h \
    ../../src/ODE/rxstiffsolve_invmass.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xED3E43F5
    TARGET.CAPABILITY = 
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = nrrk.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
