#pragma once
//#include <iostream>
//#include <assert.h>
//using namespace std;


#include "MTParserPublic.h"
#include "MTParserPrivate.h"
#include "MTParserCompiler.h"
#include "MTTools.h"
class RXInterpFn : public MTFunctionI
{		
	virtual const MTCHAR* getID(){return _T("linear Interpolate "); }
	virtual const MTCHAR* getSymbol(){return _T("interp"); }

	virtual const MTCHAR* getHelpString(){ return _T("interp(t,v1,v2,v3,...)"); }
	virtual const MTCHAR* getDescription(){ return _T("Returns interpolation between Vi,which are equally spaced 0->1"); }	
	virtual int getNbArgs(){ return c_MTNbArgUndefined; }
	virtual MTDOUBLE evaluate(unsigned int nbArgs, const MTDOUBLE *pArg)
	{
	//unsigned int nbArgs =5;
	//const double pArg[5] = {0,2,1,4,3};
		MTDOUBLE rv;
		double t = pArg[0];
 // the Ys are at 1/8,3/8,5/8,7/8
 		int np = nbArgs-1;  // number of spans

#define  y(i) pArg[(i)+1]
#define  x(i) ((double)(i) +0.5 )/(double) np

				double td = t*((double)np);
				int i = (int) floor(td-.5);
				i=max(0,i); i=min(np-2,i);
				double f = (t-x(i))/(x(i+1)-x(i));
				rv = y(i+1) *f + y(i)*(1.-f);
				return rv;
#undef y 
#undef x 
}

	virtual MTFunctionI* spawn() { return new RXInterpFn(); }
};
class RXInterp2Fn : public MTFunctionI
{		
	virtual const MTCHAR* getID(){return _T("linear Interpolate between {x,y} pairs "); }
	virtual const MTCHAR* getSymbol(){return _T("interp2"); }

	virtual const MTCHAR* getHelpString(){ return _T("interp2(t,x1,y1,x2,y2,...)"); }
	virtual const MTCHAR* getDescription(){ return _T("Returns interpolation between {xi,yi} pairs.  Extrapolates beyond the ends"); }	
	virtual int getNbArgs(){ return c_MTNbArgUndefined; }
	virtual MTDOUBLE evaluate(unsigned int nbArgs, const MTDOUBLE *pArg)
	{
		MTDOUBLE rv;
		double t = pArg[0];
 
 		int np = (nbArgs-1)/2;  // number of points
		int i;
#define  x(i) pArg[2*(i)+1]
#define  y(i) pArg[2*(i)+2]
		//RhinoApp().Print("interp2 with  %d args\n", nbArgs);
		//for(i=0;i< nbArgs ;i++)
		//	RhinoApp().Print("  %f ", pArg[i] );
		//RhinoApp().Print("\n paired\n");
		//for(i=0;i<np;i++)
		//	RhinoApp().Print("  %d  %f  %f \n", i,x(i),y(i));


		if(t==x(0))			return y(0);
		if(t==x(np-1))		return y(np-1);
		if(t<x(0))			i=0;
		else if(t>x(np-1))	i=np-2;
		else {
			for(i=0;i<np-1;i++){
				if(x(i+1)== t)  return y(i+1);
				if(x(i+1)> t) 
					break;
			}
		}
		i = min(i,np-2); // safety
		double f = (t-x(i))/(x(i+1)-x(i));
		rv = y(i+1) *f + y(i)*(1.-f);
		//RhinoApp().Print("t=%f  rv=%f \n",t,rv);
		return rv;


#undef y 
#undef x 
}

	virtual MTFunctionI* spawn() { return new RXInterp2Fn(); }
};

