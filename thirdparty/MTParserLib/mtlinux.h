#ifndef _MTLINUX_H__
#define _MTLINUX_H__

#ifdef linux
#include <math.h>
#include <malloc.h>
#include <cstdlib>
#include "MTUnicodeANSIDefs.h"
#ifdef UNICODE
 
	typedef wchar_t  *BSTR;
	#define lstrcmp wcscmp

	#define lstrlen wcslen
	#define lstrcpy  wcscpy
	#define _TEOF WEOF
	#define __T(x)	L ## x

	#define _T(x)	__T(x)
	#define _TEXT(x) __T(x)	 
	typedef unsigned long DWORD;
	typedef DWORD  LCID;

	#define SysFreeString(a) free(a)
	BSTR SysAllocStringByteLen(const char* string,const unsigned int c);
   
	int CharUpper( MTCHAR *s );


#else
	typedef char  *BSTR;
	#define lstrcmp strcmp
	#define lstrlen wstrlen
	#define lstrcpy strcpy
#endif // char

#define __max(a,b) (((a) > (b))  ? (a)  : (b))
#define __min(a,b) (((a) < (b))  ? (a)  : (b))

#define _isnan  isnan
#define _finite(x) ( !isnan(x) && !isinf(x) )
#define LOCALE_SDECIMAL   0x0000000E
#elif !defined( MICROSOFT)
    #define __max(a,b) (((a) > (b))  ? (a)  : (b))
    #define __min(a,b) (((a) < (b))  ? (a)  : (b))
#endif //linux

/*
_ltow Converts a long integer to a wide string
could use 
*/
#endif

