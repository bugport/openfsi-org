{\rtf1\ansi\ansicpg1252\deff0\deflang2057{\fonttbl{\f0\fswiss\fcharset0 Arial;}}
{\*\generator Msftedit 5.41.21.2509;}\viewkind4\uc1\pard\f0\fs20 C     ================================================================\par
C     ... SAMPLE CODE FOR READING A GENERAL SPARSE MATRIX, POSSIBLY\par
C         WITH RIGHT-HAND SIDE VECTORS\par
C     ================================================================\par
\par
      CHARACTER      TITLE*72 , KEY*8    , MXTYPE*3 , RHSTYP*3,\par
     1               PTRFMT*16, INDFMT*16, VALFMT*20, RHSFMT*20\par
\par
      INTEGER        TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD,\par
     1               NROW  , NCOL  , NNZERO, NELTVL,\par
     2               NRHS  , NRHSIX, NRHSVL, NGUESS, NEXACT\par
\par
      INTEGER        POINTR (*), ROWIND (*), RHSPTR (*), RHSIND(*)\par
\par
      REAL           VALUES (*) , RHSVAL (*), XEXACT (*), SGUESS (*)\par
\par
C     ------------------------\par
C     ... READ IN HEADER BLOCK\par
C     ------------------------\par
\par
      READ ( LUNIT, 1000 ) TITLE , KEY   ,\par
     1                     TOTCRD, PTRCRD, INDCRD, VALCRD, RHSCRD,\par
     2                     MXTYPE, NROW  , NCOL  , NNZERO, NELTVL,\par
     3                     PTRFMT, INDFMT, VALFMT, RHSFMT\par
\par
      IF  ( RHSCRD .GT. 0 )\par
     1    READ ( LUNIT, 1001 ) RHSTYP, NRHS, NRHSIX\par
\par
 1000 FORMAT ( A72, A8 / 5I14 / A3, 11X, 4I14 / 2A16, 2A20 )\par
 1001 FORMAT ( A3, 11X, 2I14 )\par
\par
C     -------------------------\par
C     ... READ MATRIX STRUCTURE\par
C     -------------------------\par
\par
      READ ( LUNIT, PTRFMT ) ( POINTR (I), I = 1, NCOL+1 )\par
\par
      READ ( LUNIT, INDFMT ) ( ROWIND (I), I = 1, NNZERO )\par
\par
      IF  ( VALCRD .GT. 0 )  THEN\par
\par
C         ----------------------\par
C         ... READ MATRIX VALUES\par
C         ----------------------\par
\par
          IF  ( MXTYPE (3:3) .EQ. `A' )  THEN\par
              READ ( LUNIT, VALFMT ) ( VALUES (I), I = 1, NNZERO )\par
          ELSE\par
              READ ( LUNIT, VALFMT ) ( VALUES (I), I = 1, NELTVL )\par
          ENDIF\par
\par
C         -------------------------\par
C         ... READ RIGHT-HAND SIDES\par
C         -------------------------\par
\par
          IF  ( NRHS .GT. 0 )  THEN\par
\par
              IF  ( RHSTYP(1:1) .EQ. `F' ) THEN\par
\par
C                 -------------------------------\par
C                 ... READ DENSE RIGHT-HAND SIDES\par
C                 -------------------------------\par
\par
                  NRHSVL = NROW * NRHS\par
                  READ ( LUNIT, RHSFMT ) ( RHSVAL (I), I = 1, NRHSVL )\par
\par
              ELSE\par
\par
C                 ---------------------------------------------\par
C                 ... READ SPARSE OR ELEMENTAL RIGHT-HAND SIDES\par
C                 ---------------------------------------------\par
\par
\par
                  IF (MXTYPE(3:3) .EQ. `A') THEN\par
\par
C                    ------------------------------------------------\par
C                    ... SPARSE RIGHT-HAND SIDES - READ POINTER ARRAY\par
C                    ------------------------------------------------\par
\par
                     READ (LUNIT, PTRFMT) ( RHSPTR (I), I = 1, NRHS+1 )\par
\par
C                    ----------------------------------------\par
C                    ... READ SPARSITY PATTERN FOR RIGHT-HAND\par
C                        SIDES\par
C                    ----------------------------------------\par
\par
                     READ (LUNIT, INDFMT) ( RHSIND (I), I = 1, NRHSIX )\par
\par
C                    --------------------------------------\par
C                    ... READ SPARSE RIGHT-HAND SIDE VALUES\par
C                    --------------------------------------\par
\par
                     READ (LUNIT, RHSFMT) ( RHSVAL (I), I = 1, NRHSIX )\par
\par
                  ELSE\par
\par
C                    -----------------------------------\par
C                    ... READ ELEMENTAL RIGHT-HAND SIDES\par
C                    -----------------------------------\par
\par
                     NRHSVL = NNZERO * NRHS\par
                     READ (LUNIT, RHSFMT) ( RHSVAL (I), I = 1, NRHSVL )\par
\par
                  ENDIF\par
\par
              END IF\par
\par
              IF  ( RHSTYP(2:2) .EQ. `G' ) THEN\par
\par
C                 -------------------------\par
C                 ... READ STARTING GUESSES\par
C                 -------------------------\par
\par
                 NGUESS = NROW * NRHS\par
                 READ (LUNIT, RHSFMT) ( SGUESS (I), I = 1, NGUESS )\par
\par
              END IF\par
\par
              IF  ( RHSTYP(3:3) .EQ. `X' ) THEN\par
\par
C                 -------------------------\par
C                 ... READ SOLUTION VECTORS\par
C                 -------------------------\par
\par
                 NEXACT = NROW * NRHS\par
                 READ (LUNIT, RHSFMT) ( XEXACT (I), I = 1, NEXACT )\par
              END IF\par
          END IF\par
      END IF\par
\par
}
 