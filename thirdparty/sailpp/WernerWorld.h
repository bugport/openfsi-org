#pragma once
#ifdef SAILPLUSPLUS_LIBRARY
#include <QString>
#endif
#include <list>
#include "point.h"
#include "lattice.h"
#include "wakeline.h"

#define pi 3.141592653589793238462643
class WernerWorld
{
    friend class WernerSail;

public:
    WernerWorld(void);
    virtual ~WernerWorld(void);
    int SetParameters(double pinc,int pN,int pdist,int pNWLS,int pNWLSF,int pihwl,const double pv0);
    class WernerSail*  AddSail( const QString pname);
    QString justRun();
    int run (int pinc,int pN,int pdist,int pNWLS,int pNWLSF,int pihwl);
    void SetupModels();//class WernerSail *jib,class WernerSail *main );



protected:
    //Calculates the induced velocity from wing and wake
    WernerPoint  vInduced_w(WernerPoint cp, double *G );//, double *GblobF, int *NMWLF,double *GblobH, int *NMWLH,int blob_av );


//    void readData(lattice *laJ,lattice *laM, int N);
    void influence_coefficients_wakeline(//lattice *laJ,lattice *laM,wakeline **wlJ,wakeline **wlM,
                                          double **A, double **B );
//    void influence_coefficients(lattice *laJ, lattice *laM,double **A );
     void influence_coefficients( double **A );
  //  void neighbours(lattice *la , int N, int *wake_sides);
  //  void wakelines(lattice *la, wakeline **wl,   int N, int NWLS, double hwl, point Uo );
    void wakelinesFoot(lattice *la,   wakeline **wlf, int N, int NWLS,int NWLSF, double hwl);
    void  gauss(double **A, int NOE, double *r);
    int  mklSolve( double **A, int NOE, double *r);
//    QString trefftz_plane(lattice *la,wakeline **wl, int N, int NWLS,  double *G );
    static double interpol(double x1,double x2,double x,double y1, double y2);
    static WernerPoint interpol(WernerPoint x1, WernerPoint x2, WernerPoint x, WernerPoint y1 ,WernerPoint y2);

    std::list<class WernerSail*> m_Sails;

    int  mirr;			//=1 for water surface boundary condition
    double tol;		//iteration tolerance
    WernerPoint Uo;	//freestream for sail

    double Incidence_Deg;
    int N;
    int  dist;	//Distribution(1=simple, 2=semi cosine, 3=full cosine, 4=elliptic planform)
    int  NWLS;	//Number of wake line segments
    int NWLSF;			//Number of wake line segments, foot
    int  ihwl;	//Length of wake line segments




};
