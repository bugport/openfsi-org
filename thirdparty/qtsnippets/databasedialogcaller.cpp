    DatabaseConnectionDialog* dialog = new DatabaseConnectionDialog(this);
     
        // optional: set the data that will be presented to the user as auto-filled form
        dialog->setDatabaseName( "mydb" );
        dialog->setDatabasePortNumber( 1234 );
        dialog->setDatabaseHostName( "localhost" );
        dialog->setDatabaseUsername( "luca" );
        dialog->setDatabaseDriverName( "QPSQL" );
        dialog->setDatabasePassword( "pwd" );
     
        // enable the connect button if all the data is correct
        dialog->checkFormData();
        // connect the dialog signal to a slot where I will use the connection
        connect( dialog,
                 SIGNAL(databaseConnect(QSqlDatabase&)),
                 this,
                 SLOT(slotHandleNewDatabaseConnection(QSqlDatabase&)));
     
         // show the dialog (without auto-connection)
        dialog->run( false );

The dialog allows for pre-initialization of form fields, as well as an auto-connect mode that make the connection to happen automatically if all the data is in place.
Please note that this dialog can be improved in several ways, and represents therefore a starting point for a more specific database connection dialog.
Auto-Connect mode

When the run() method is invoked an autoconnect mode can be specified as boolean value true. In such mode, if all the form data is filled, the dialog immediately attempts to connect to the database, and in the case it succeed the dialog is not shown to the user and the signal is emitted. In the case the form data is not complete or the connection cannot be established, the dialog is shown to the user. If the autoconnect mode is off (parameter = false) the dialog is always shown.
Source code

 
