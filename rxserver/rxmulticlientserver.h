#ifndef RXMULTICLIENTSERVER_H
#define RXMULTICLIENTSERVER_H

#include <QTcpServer>

 #include "rxtcpsocket.h"


class RXMultiClientServer : public QTcpServer
{
    Q_OBJECT
public:
    explicit RXMultiClientServer(QObject *parent = 0);
    ~RXMultiClientServer();

    void start();
    void stop();
    void WriteOnSocket(RXTcpSocket *client, const QString &s);
protected:
    void sendHello(RXTcpSocket *client);
    void Interpret(RXTcpSocket *client, const QString &s);
signals:
     void RXMCVCommand(QObject *client, const QString &s);
protected slots:
    void handleNewConnection();
    void clientDisconnected();

public slots:
       void readOnSocket();
    void sendMessageToAllClients(const QString &s);
private:
     QList<RXTcpSocket *> clientConnections;

};

#endif // RXMULTICLIENTSERVER_H
