#include "StdAfx.h"
#include "rxstdoutredirector.h"
#include <iostream>
#include <QDebug>
#include <QSocketNotifier>
#ifdef _WINDOWS
#include <FCNTL.H>
#elif linux
#include <unistd.h>
#endif
#include "rxmainwindow.h"
/*
////////////////////////////////////////////////////////////////////////////////////////
// the stdout redirection
/// class to capture printf output (not cout) so we can display it in a widget
/// but this is buggy - better to disable it via command-line flag -noredirprintf

  // See (though it doesnt work)  http://lists.trolltech.com/qt-interest/2004-12/thread00816-0.html
// in linux at least there is no way to check if anything is left to read
// in MS (mingw) the slot never gets called.
// so this loop is blocking. We have to run it in a separate thread.
// we create a thread in whose run method we declare the stream

// create the notifier and start a forever which reads and emits whenever it gets an EOL
  */
rxStdoutRedirector::rxStdoutRedirector(QWidget *parent):
    QThread(parent),
    m_PleaseStop(0)
{
}
// normally we stop the thread by writing 'ciao' before
rxStdoutRedirector::~rxStdoutRedirector()
{
    if(this->isRunning())
     { //qDebug()<<"~rxStdoutRedirector is still running";
            m_PleaseStop=1;
        puts("ciao");
        this->terminate();
        wait();

    }
// we need to print something because the loop has a blocking read
    m_PleaseStop=1;
}

void rxStdoutRedirector::run()
{
    this->setPriority( QThread::LowPriority);
    int fd[2];
#ifdef linux
    // duplicate write end of the pipe to stdout (dup2 closes it first)
	int kk= pipe(fd);
    if(kk<0)      qDebug()<<"pipe failed";
	     kk = dup2(fd[1], 1); 
	 if(kk<0)       qDebug()<<"dup2 failed";
     const char * const mode = "r";
    FILE * f = fdopen(fd[0], mode);
	    QTextStream s(f, QIODevice::ReadOnly); // Windows doesnt like this: why dontwe just use low-level reading

    // from now on, any reads from s will be in fact reads from whatever was
    // written to stdout

    QString line;
    char data[2]; data[1]=0;
    while(true) {//!m_PleaseStop
        s.device()->read(data,1); // the loop blocks here waiting for text
        if(*data == '\n')
        {
            if(line=="ciao")
            {
               // qDebug()<<"break on CIAO";
                break;
            }
            if(!line.isNull())
                emit g_rxmw->RXPleasePrint(NULL,/*QString("(print):")+*/line  ); //
            line.clear();
        }
        else
            line+=data;
    }
    qDebug()<<"redirector run done"; return;
#else
		// see http://msdn.microsoft.com/en-us/library/edze9h7e%28v=VS.71%29.aspx
	int kk= _pipe(fd,256,_O_TEXT|_O_NOINHERIT );
   //int *pfds, ( use fd above)
   //unsigned int psize, say 2048
   //int textmode  = _O_TEXT 
//);
    if(kk<0)      qDebug()<<"pipe failed";
    // duplicate write end of the pipe to stdout (dup2 closes it first)
	    kk = _dup2(fd[1], 1); 
	    if(kk<0) qDebug()<<"dup2 failed";
    const char * const mode = "r";
  FILE * f = _fdopen(fd[0], mode);  assert(f);
 
    // from now on, any reads from s will be in fact reads from whatever was
    // written to stdout
	puts("are you awake?"); fflush(stdout);
    QString line;
    char data[2]; data[1]=0;
    while(true) {//!m_PleaseStop
        _read(fd[0],data,1);//    // the loop blocks here waiting for text
        if(*data == '\n')
        {
            if(line=="ciao")
            {
               // qDebug()<<"break on CIAO";
                break;
            }
            if(!line.isNull())
                emit g_rxmw->RXPleasePrint(NULL,QString("(print):")+line); //
            line.clear();
        }
        else
            line+=data;
    }
    qDebug()<<"redirector run done"; return;
#endif
}

