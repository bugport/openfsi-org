
#include <QtGui/QApplication>
#include "rxmainwindow.h"
#include "rxstdoutredirector.h"
#ifdef linux
#include <unistd.h>
#endif
#include "rxqapplication.h"
#define  RXSTD
#ifdef COIN3D
#include <Inventor/Qt/SoQt.h>

int main(int argc,char*argv[])
{
    QApplication a(argc,argv);
    QStringList args =     a.arguments() ;
    RXMainWindow *w=new RXMainWindow();
    SoQt::init(w);
    SoQt::show(w);
#ifdef RXSTD
    // rxStdoutRedirector directs printf output to our text window, not cout
    rxStdoutRedirector *rsd = new rxStdoutRedirector();
    rsd->start();
#endif
    w->ProcessCommandLine(args);

   SoQt::mainLoop();
#ifdef RXSTD
    rsd->m_PleaseStop=1; printf("ciao\n");fflush(stdout);
    usleep(1000);
#endif

    SoQt::done();
    delete w;
#ifdef RXSTD
    delete rsd;
#endif
    return 0;
}
#else  // pure QT
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QStringList args =  a.arguments() ;
    RXMainWindow w;

    w.show();
    rxStdoutRedirector *rsd = new rxStdoutRedirector();
    rsd->start();

    w.ProcessCommandLine(args);
    int rc = a.exec();
    rsd->m_PleaseStop=1; puts("ciao");cout<<"ciao"<<endl; 
#ifdef linux
	usleep(1000);
#endif
    rsd->wait(1000);
    delete rsd;
   // qDebug()<<"CloSing down";
    return rc;
}
#endif
