#include <QKeyEvent>
#include <QCompleter>
#include <iostream>

using namespace std;
#include <QDebug>
#include <QSettings>
#include "rxcommandlinebox.h"

RXcommandlinebox::RXcommandlinebox(QWidget *parent) :
  QLineEdit(parent),
  m_historyIndex(0)
{
  QStringList wordList;
  wordList
      << "aero"
      << "append"
      << "apply"
      << "ca"
      << "calculate"
      << "calculate:stop"
      << "calculate:autorun=OFF"
      << "calculate:AUTOrun=on"
      << "cd"
      << "change"
      << "updateexpression: model, type, name, exp, newvalue"
      << "edgeswap: model : stringname(wildcard) : testExpression(>0)"
      << "choose line"
      << "close_input"
      << "close_summary"
      << "delete"
      << "deletemodel : modelname"
      << "draw Edge"
      << "drop : modelname"
      << "editScript : modelname"
      << "editword"
      << "envEdit"
      << "EOF"
      << "exit"
      << "export pan"
      << "export Plot"
      << "Freeze : model "
      << "getdata"
      << "getExpressions"
      << "getheaders"
      << "GLE export"
      <<"graphics: 1 |0|on|off"
     << "hoist"
     << "igesout: model : filename.igs"
     <<"write_iges"
    << "Initialise Optimisation"
    << "listing"
    << "Load Optimisation History"
    << "load State"
    << "macro : mymacrofile "
    << "MakeLastKnownGood"
    << "memcheck"
    << "mesh"
    << "open bag"
    << "open boat"
    << "open camera"

    << "open_input"
    << "open_log: qt : MYSQL$localhost$ofsidb$tablename$username$password$anything"
    << "open_mirror: qt : MYSQL$localhost$ofsidb$username$password$something"
    << "open sail"
    << "polyswap"
    << "post"
    << "pressure"
    << "print_object"
    << "printall : model"
    << "process changes"
    << "ReDove"
    << "Refresh"
    << "resize"
    << "Run Optimisation"
    << "saillist"
    << "saveas : model : scriptname [ :gen ] "
    << "SaveState"
    << "SendAsClient"
    << "sendasserver"
    << "Set Model"
    << "SetLastKnownGood"
    << "SetServer"
    << "snapshot : file.mac"
    << "solve"
    << "StartAsServer"
    << "system"
    << "UnFreeze"
    << "update"
    << "write DMatrix"
    << "write expressions"
    << "write log"
    << "write pressure: model : filename"
    << "write stress: model : filename"
    << "write 3dm: model : filename"
    << "write_nastran: model : filename"
    << "write_stl: model :  filename"
    << "write_attributes: model : filename"
    << "write_uvdp"
    << "write_vtk : model :  filename"
  << "zoom"
  << "settings edit"
  << "settings apply"
  << "setting: (eg)Convergence_Limit : 0.1"
  <<"postBlob :[ label: fname ]"
  <<"broadcast :  message "
  <<"drawStress :  model "
  <<"drawStrain :  model "
  <<"drawRefVector :  model "
  <<"drawPrinStiff : model"
  <<"Discretize :  model "
  <<"BuildMaterials : modelname : [modelspace|GLOBALspace] "
  <<"prestress"
  <<"shrinkEdges"
  <<"ResetEdges"
  <<"dsmInit :  : ![non-zero for file, else DSM]"
  <<"dsmEnd"
  <<"write_dsm"
  <<"dsmWrite"
  <<"loop: postableLabel : startval : endval : inc [:postableLabel: startval: endval:inc [ etc...]  ]"
  <<"modellist"
  <<"dependencies: [niece,&c]"
  <<"debug: [ unresolveconnects | ?? ]"
  <<"open macro"   //synonym for `macro`
  <<"script"  //synonym for `macro`
;
  wordList.sort();

  QCompleter *rxmacrocompleter= new QCompleter(wordList, this);
  rxmacrocompleter->setCaseSensitivity(Qt::CaseInsensitive);
  rxmacrocompleter->setCompletionMode ( QCompleter::PopupCompletion);//UnfilteredPopupCompletion
  setCompleter(rxmacrocompleter);
  //readSettings()

  QSettings settings("openfsi", "main");
  settings.beginGroup("commandline");
  m_history=settings.value("history") .toStringList();
  settings.endGroup();
}

RXcommandlinebox::~RXcommandlinebox(){
  QSettings settings("openfsi", "main");
  settings.beginGroup("commandline");
  settings.setValue("history",m_history);
  settings.endGroup();
}

void RXcommandlinebox::keyPressEvent ( QKeyEvent * event ) {
  QKeyEvent * e = event;
  switch(e->key()  ){
  case Qt::Key_Up:
    this->onUpArrow();
    break;
  case Qt::Key_Down:
    this->onDownArrow();
    break;
  default:
    QLineEdit::keyPressEvent (  event );
  }
}

int RXcommandlinebox::PushToHistory()
{
  int rc=0;
  if(!this->m_history.isEmpty()){
    if( this->m_history.first() != this->displayText() )
      this->m_history.push_front(this->displayText());
  }
  else
    this->m_history.push_front(this->displayText());
  this->m_historyIndex=0;
  if(m_history.count()>50)
    m_history.removeLast();
  return rc;
}

int RXcommandlinebox::onUpArrow()
{
  int rc=0;
  if(this->m_historyIndex< this->m_history.count())
  {
    this->setText( this->m_history[ this->m_historyIndex++]);
  }

  return rc;
}
int RXcommandlinebox::onDownArrow()
{
  int rc=0;
  if(this->m_historyIndex>0 )
  {
    this->setText( this->m_history[ --this->m_historyIndex]);
  }
  return rc;
}
int RXcommandlinebox::SetModelList(const QString s)
{
    cout<<" TODO set modellist in RXcommandlinebox\n "<<qPrintable(s)<<endl;
    this->m_mlist=s;
    return 1;
}
