Welcome  to this wiki. 
#Installation

The easiest way to install ofsi is to run this script (in ubuntu using the bash shell) 
but first you need to create file   ofsiscript..sql containing
      INSERT INTO mysql.user (Host,User,Password) VALUES('%','openfsi',PASSWORD('openfsi'));
      INSERT INTO mysql.user (Host,User,Password) VALUES('%','ofsi',PASSWORD('ofsi'));
      CREATE DATABASE ofsidb;
      GRANT ALL ON ofsidb.* to 'openfsi'@'%' identified by 'openfsi';
      GRANT ALL ON ofsidb.* to 'ofsi'@'%' identified by 'ofsi';
      FLUSH PRIVILEGES;
      quit

now the installation script ofsiInstall.sh

      echo "this is ofsiInstall.sh"
      echo "openfsi.org installation.  (ofsi to its friends)  "
      echo ""
      echo "this script is designed for recent ubuntu releases. "
      echo "it installs the prerequisites (apart from the intel compilers)"
      echo "it downloads the source and compiles it "
      echo "it also does a minimal mysql setup "
      echo ""
      echo "before starting the installation you should install the intel fortran compiler, and optionally the intel C compiler "
      echo "(you can get evaluation copies from the Intel website)"
      echo "test the compiler installation(s) by going 'ifort --version'  (and 'icpc -version')"
      echo "if you want to use the intel c compiler you'll need to uncomment line 81 (qmake ...) and comment line 84 (qmake...)"
      echo ""
      echo "copy this file ofsiInstall.sh and the file ofsiscript.sql into your home folder "
      echo "put your name and email in lines 54 and 56."
      echo "change its permissions  to make it executable  by going"
      
      echo "            chmod a+x  ofsipreliminaries.sh "
      
      echo " and then run it by typing"
      
      echo "             sudo ./ofsipreliminaries.sh "
      
      echo "once you are happy with your edits,comment out line  25('exit') "
      exit

      ###############start of ofsi installation..

      #### install the required packages (if you do it one-by one you have to prefix 'sudo '

      apt-get install git-core 
      apt-get install qt4-qmake
      apt-get install libsoqt4-dev
      apt-get install libmysqlclient-dev
      apt-get install mysql-client 
      apt-get install mysql-server 
      apt-get install paraview
      
      
      
      ######  make a folder to download to

      cd ~ 
      mkdir ofsi
      mkdir ofsi/build

      #####

      cd ofsi/build/
      git init

      # Of course you should use your name

      git config --global user.name "My Name"
      # Same for the email address
      git config --global user.email "mymail@mydomain.com"


      ###### download the source - and some models 

      git pull git://gitorious.org/openfsi-org/openfsi-org.git

      mv ofsidata.tar.gz ..
      cd ..

      tar -xvf ofsidata.tar.gz
      cd ~
      echo "#########added by ofsi installation######" >>.bashrc
      echo "export R3ROOT=$PWD/ofsi" >>.bashrc
      echo "export R3CODE=$PWD/ofsi/code" >>.bashrc
      echo "export RELAXII_APPLICATION_PATH=$PWD/ofsi/code" >>.bashrc
      echo "export PATH=$PWD/ofsi/code/bin:$PATH" >>.bashrc
      echo "export RX_EDIT=gedit" >>.bashrc
      echo " " >>.bashrc

      source .bashrc


      cd ofsi/build/
      ## if you have the intel compiler,  use this command
      ##qmake -spec linux-icc-64 
 
      ## if you are using gcc (and there's no reason why not)  use this command
      qmake


      make

      ln -s rxserver/rxserver $HOME/ofsi/code/bin/rxserver
      ln -s clients/rxclientbase/rxclientbase $HOME/ofsi/code/bin/rxclientbase

      cd ~

      mysql -u root -p   < ofsiscript.sql > output.tab
      
      rxserver
      






## random old notes   

Feb 23 there is a build issue on ubuntu 11.10  You get a link error mentioning SoSeparator.  This is because the qmake directove CONFIG+=coin3d doesnt seem to work for this o/s
You have to edit the files  rxserver/ rxserver.pro and   clients/rxclientbase/ rxclientbase.pro.    Locate the line
LIBS += -lSoQt and change it to

LIBS += -lSoQt  -lCoin

for good measure you can delete the line CONFIG+=coin3d

Now go 
make clean
qmake
make

and you should be OK

There's one more installation step:  copy the executable  clients/rxclientbase/rxclientbase  to somewhere on your path, like (installdir)/code/bin

Theere's an error in (installdir)/BMG..mac - delete it. 
Whenyou start the kernel, type 'macro' in the command-line and navigare to 'generic/BMG.mac

Have fun!



