#include "StdAfx.h"
#include <QDebug>
#include <iomanip>
#include "cfromf.h"
#include "offset.h"
#include "rxrungekutta.h"
using namespace std;
RXRungeKutta::RXRungeKutta()
{
}
int RXRungeKutta::runtopeak()  // based on  'main from xodeint
{
    int N,i,nbad,nok;
    DP eps=1.0e-5,h1=0.01,hmin=0.01;
    N = cf_JaySize()-1;

    Vec_DP *ystart = new Vec_DP(2*N);
    for(i=0;i<2*N;i++) (*ystart)[i]=0.;

    cf_PackCoordinates(ystart->v); // X first, then V
    cf_zeroVelocity();
    nrhs=0;
    odeint(*ystart,eps,h1,hmin,nok,nbad);
    cf_ApplyDisplacements(ystart->v, 0);
    if(1){
        cout  << "steps: ok :"<< setw(4) << nok;
        cout << "  bad:" << setw(4) << nbad << " ";
        cout << "function calls:"   << " ";
        cout << setw(4) << nrhs<<endl;
    }
    delete ystart;
    return 0;
}

int RXRungeKutta::test()   //testrkdumb;
 {
    std::cout<< "Test rkdumb"<<endl;
    //  xrkdumb_main();
    return 0;

}

void RXRungeKutta::derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx)
   {
    nrhs++;
    int n = y.size();
    cf_DerivativeList(x,y.v  ,dydx.v,m_damp,n);
    return;
   }
int RXRungeKutta::odeint(Vec_IO_DP &ystart,  const DP eps,
                 const DP h1, const DP hmin, int &nok, int &nbad)
{
    int iexit=0;
    double ekt,backtrack, x,hnext,hdid,h;
    int i,k,nstp , nvar=ystart.size();
    Vec_DP yscal(nvar),y(nvar),dydx(nvar);
    x=0.0;    nok = nbad = 0;
    h=h1;
    for (i=0;i<nvar;i++) y[i]=ystart[i];

    //for nstp is the integration loop
    // we follow it until ekt passes a max, or until residuals have disappeared

    time_t now, olddrawtime; time(&olddrawtime);

    for (nstp=0;;nstp++) { //nstp<MAXSTP
        this->m_kit++;
        //    enable the UI
        k=2;  k=check_messages(&k);
        if (relaxflagcommon.Stop_Running != 0) { iexit=2 ; break;}

        // do we post-process now?
        if(relaxflagcommon.g_UpdateTime>=0  ) {
            time(& now);
            double drawtime = difftime(now,olddrawtime);
            if(drawtime >= relaxflagcommon.g_UpdateTime)  {
                cf_ApplyDisplacements(y.v, 0);
                k = cf_postprocess();
                olddrawtime = now;
            }
        }
        derivs(x,y,dydx);
        for (i=0;i<nvar/2;i++)
        {
            //  yscal[i]=fabs(y[i]-ystart[i])+fabs(dydx[i]*h)+TINY; // peter added -ystart. Ita very slow to start
            //yscal[i]= 1.*fabs(dydx[i]*h)+TINY;
            //    yscal[i]= 1.*fabs(dydx[i] )+TINY+0.01;
            yscal[i]= 5. *max( fabs(dydx[i]*h),100.* eps*eps);// +TINY;
        }
        for (i=nvar/2;i<nvar;i++) // velocity limits
        {
            yscal[i]=  fabs(dydx[i]*h) +10.;
        }

        rkqs(y,dydx,x,h,eps,yscal,hdid,hnext);
        if (hdid == h) ++nok; else ++nbad;

        ekt = cf_MeasureKE(y.v,nvar);

        qDebug()<< x<< hdid<<"   "<< ekt<<"   "<<nbad<<nok;

        if( LogEnergy (x,ekt)) // do one more step (x+backtrack)
        { // WHY NOT  go all the way back to 'backtrack' using multiple steps?
            h= backtrack = EnergyPeak(); assert(backtrack<=0.);
            derivs(x,y,dydx);
            rkqs(y,dydx,x,h,eps,yscal,hdid,hnext);
            // cout<<" backtrack step=" <<backtrack<<", hdid = "<<hdid<<endl;
            for (i=0;i<nvar;i++) ystart[i]=y[i];
            return iexit;
        }

        if (fabs(hnext) <= hmin)
            hnext=SIGN(hmin,hnext);// cout<<"Step size too small in odeint"<<endl;
        h=hnext;
    }
    if(iexit !=2)
        cout<<"Too many steps in routine odeint"<<endl;
    return iexit;
}
void RXRungeKutta::rkqs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &x, const DP htry,
                const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext )
{
    const DP SAFETY=0.9, PGROW=-0.2, PSHRNK=-0.25, ERRCON=1.89e-4;
    int i;
    DP errmax,h,htemp,xnew;

    int n=y.size();
    h=htry;
    Vec_DP yerr(n),ytemp(n);
    for (;;) {
        rkck(y,dydx,x,h,ytemp,yerr);
        errmax=0.0;
        for (i=0;i<n;i++) errmax=MAX(errmax,fabs(yerr[i]/yscal[i]));
        errmax /= eps;
        if (errmax <= 1.0) break;
        htemp=SAFETY*h*pow(errmax,PSHRNK);
        h=(h >= 0.0 ? MAX(htemp,0.1*h) : MIN(htemp,0.1*h));
        xnew=x+h;
        if (xnew == x) cout<<"stepsize underflow in rkqs"<<endl;
    }
    if (errmax > ERRCON) hnext=SAFETY*h*pow(errmax,PGROW);
    else hnext=5.0*h;
    x += (hdid=h);
    for (i=0;i<n;i++) y[i]=ytemp[i];
}
void RXRungeKutta::rkck(Vec_I_DP &y, Vec_I_DP &dydx, const DP x,
                const DP h, Vec_O_DP &yout, Vec_O_DP &yerr)
{
    static const DP a2=0.2, a3=0.3, a4=0.6, a5=1.0, a6=0.875,
            b21=0.2, b31=3.0/40.0, b32=9.0/40.0, b41=0.3, b42 = -0.9,
            b43=1.2, b51 = -11.0/54.0, b52=2.5, b53 = -70.0/27.0,
            b54=35.0/27.0, b61=1631.0/55296.0, b62=175.0/512.0,
            b63=575.0/13824.0, b64=44275.0/110592.0, b65=253.0/4096.0,
            c1=37.0/378.0, c3=250.0/621.0, c4=125.0/594.0, c6=512.0/1771.0,
            dc1=c1-2825.0/27648.0, dc3=c3-18575.0/48384.0,
            dc4=c4-13525.0/55296.0, dc5 = -277.00/14336.0, dc6=c6-0.25;
    int i;

    int n=y.size();
    Vec_DP ak2(n),ak3(n),ak4(n),ak5(n),ak6(n),ytemp(n);
    for (i=0;i<n;i++)
        ytemp[i]=y[i]+b21*h*dydx[i];
    derivs(x+a2*h,ytemp,ak2);
    for (i=0;i<n;i++)
        ytemp[i]=y[i]+h*(b31*dydx[i]+b32*ak2[i]);
    derivs(x+a3*h,ytemp,ak3);
    for (i=0;i<n;i++)
        ytemp[i]=y[i]+h*(b41*dydx[i]+b42*ak2[i]+b43*ak3[i]);
    derivs(x+a4*h,ytemp,ak4);
    for (i=0;i<n;i++)
        ytemp[i]=y[i]+h*(b51*dydx[i]+b52*ak2[i]+b53*ak3[i]+b54*ak4[i]);
    derivs(x+a5*h,ytemp,ak5);
    for (i=0;i<n;i++)
        ytemp[i]=y[i]+h*(b61*dydx[i]+b62*ak2[i]+b63*ak3[i]+b64*ak4[i]+b65*ak5[i]);
    derivs(x+a6*h,ytemp,ak6);
    for (i=0;i<n;i++)
        yout[i]=y[i]+h*(c1*dydx[i]+c3*ak3[i]+c4*ak4[i]+c6*ak6[i]);
    for (i=0;i<n;i++)
        yerr[i]=h*(dc1*dydx[i]+dc3*ak3[i]+dc4*ak4[i]+dc5*ak5[i]+dc6*ak6[i]);
}
