#ifndef RXSTIFFSOLVE_INVMASS_H
#define RXSTIFFSOLVE_INVMASS_H
#include "RXSparseMatrix1.h"

#include "rxodestepper.h"
/*
 * a method for solving the structure as a set of 'stiff' ODEs.
 *
 *This variant takes the mass and damping matrices to be proportional to the global stiffness
 *matrix.
 *Thus the jacobian is a simple combination of identity matrices
 *
 *But the derivative  y''  aka  (Mass^-1).Residual
 *needs a fresh stiffness matrix each cycle .


*/


class RXStiffSolve_InvMass : public RXODEStepper
{
public:
    RXStiffSolve_InvMass();
    ~RXStiffSolve_InvMass();
    int test() ; //testrkdumb;
    int runtopeak() ;
protected:
    int odeint(Vec_IO_DP &ystart, const DP eps,
               const DP h1, const DP hmin, int &nok, int &nbad );
    void derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx);
    int formStiffnessMatrix(); // places it in m_jSj
    class RXSparseMatrix1  m_IMinusBh,m_jSj;
private:  // for stif
     void jacobian(const DP x, Vec_I_DP &y, Vec_O_DP &dfdx, RXSparseMatrix1 &BS);

     void stifbs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &xx, const DP htry,
                const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext);//,
             //   void derivs(const DP, Vec_I_DP &, Vec_O_DP &));
     void simpr(Vec_I_DP &y, Vec_I_DP &dydx, Vec_I_DP &dfdx, class RXSparseMatrix1 &BS,
                      const DP xs, const DP htot, const int nstep, Vec_O_DP &yout);

    void pzextr(const int iest, const DP xest, Vec_I_DP &yest, Vec_O_DP &yz,
        Vec_O_DP &dy);
};

#endif // RXSTIFFSOLVE_INVMASS_H
