#include "StdAfx.h"
#include <QDebug>

#include "nr.h"
#include "rxodestepper.h"

RXODEStepper::RXODEStepper():
    nrhs(0),
    m_kit(0),
    dxsav(0),
    kmax(0),
    kount(0),
    d_p(0),
    x_p(0),
    xp_p(0),
    yp_p(0),
    m_damp(0.02),
    m_h1(0.25),
    m_eps(1e-7),
    m_dbg(0)
{
}
RXODEStepper::~RXODEStepper()
{

}
std::string RXODEStepper::Describe()const
    {
        std::string rv;
        rv = "ODE Stepper h1="+to_string(this->m_h1);
         rv += " eps="+to_string(this->m_eps);
         rv += " damp="+to_string(this->m_damp);

        return rv;
    }
int RXODEStepper:: LogCoords(const double x, double* y, const int n)
{
    int i, rc=1;
    double*lp;
    std::vector <double> v;

    if(m_oldcoords.size()>3) m_oldcoords.pop_front();
    v.reserve(n);
    for(i=0,lp=y;i<n;i++,lp++) v.push_back(*lp);
    m_oldcoords.push_back(make_pair(x,v));
    return rc;
}

int RXODEStepper::SetCoordsToPeak(double*y, const int n)
{
    int rc=0;
    // 1) extract the last 3 step-times and KEs.
    //     establish the time that KE peaked by fitting a quadratic
    // 2) determine the weights to apply to the last 3 step positions
    // 3) set y to weighted mean
    double num,den; int i;
    double t0,t1,t2,tP, c0,c1,c2,ke0,ke1,ke2;
    pair<double,double> last;
    std::deque<pair<double,std::vector<double> > >::reverse_iterator i0,i1,i2;
    const double *y0,*y1,*y2; double*lp;
    assert(m_history.size()>1 );

    if(m_history.size()==2) { // the first is the start - dont go back there
                last = m_history.back(); t0=last.first;
        this->m_h1 = min(this->m_h1,t0/16. );
        this->m_h1 =max(0.000001, this->m_h1 );
        return 0;
    }

        last = m_history.back(); t0=last.first; ke0 = last.second; m_history.pop_back();
        last = m_history.back(); t1=last.first; ke1 = last.second; m_history.pop_back();
        last = m_history.back(); t2=last.first; ke2 = last.second;

        num=ke2*(-pow(t0,2) + pow(t1,2)) + ke1*(pow(t0,2) - pow(t2,2)) +
                ke0*(-pow(t1,2) + pow(t2,2));
        den=2*(ke2*(-t0 + t1) + ke1*(t0 - t2) + ke0*(-t1 + t2));
        tP=num/den;

        c0= ((t1 - tP)*(t2 - tP)) /((t0 - t1)*(t0 - t2));
        c1= ((t0 - tP)*(-t2 + tP))/((t0 - t1)*(t1 - t2));
        c2= ((t0 - tP)*(t1 - tP)) /((t0 - t2)*(t1 - t2));

        assert(m_oldcoords.size()>=3);
        i0 = m_oldcoords.rbegin();
        i1 = i0; i1++;
        i2=i1; i2++;
        t0 = i0->first; // SB identical
        t1 = i1->first;
        t2 = i2->first;
        std::vector<double> &Y0 = i0->second;
        std::vector<double> &Y1 = i1->second;
        std::vector<double> &Y2 = i2->second;
        for(i=0,lp=y;i<n;i++,lp++)
            *lp = c0 * Y0[i] + c1*Y1[i]+ c2*Y2[i] ;

       this->m_h1 = min(this->m_h1,(t0-t2)/16. );
    return rc;
}

int RXODEStepper::LogEnergy (const double x, const double ekt)
{
    int rc=0; assert(ekt>=0);
    pair<double,double> last;
    double ekl;
    if(m_history.size()) {
        last = m_history.back();
        ekl = last.second;
        if(ekl>ekt) rc=1;
    }
    m_history.push_back( make_pair(x,ekt));
    return rc;
}
/// energy peak; we know that the last history item had less KE than the previous
/// we fit a 2nd order curve to the last 3 points
/// we return  the delta-X to get back to the peak from the
double RXODEStepper::EnergyPeak()
{
    double dx = 0;
    double x0,y0, x2,y2 , x3,y3 ,xtop;
    pair<double,double> last; qDebug()<<" history size="<<m_history.size();
    if(m_history.size()< 2 )
    {  this->m_h1 =max(0.000001, this->m_h1 /32.);  return 0;}
    if(m_history.size()< 3) {
        last = m_history.back(); x0=last.first; y0 = last.second; m_history.pop_back();
        last = m_history.back(); x2=last.first-x0; y2 = last.second-y0;
        dx=x2;//-x0;
        assert(dx<0)  ;
        this->m_h1 = min(this->m_h1,-dx );
        this->m_h1 =max(0.000001, this->m_h1 /16.);
        return dx;
    }
    last = m_history.back(); x0=last.first; y0 = last.second; m_history.pop_back();
    last = m_history.back(); x2=last.first-x0; y2 = last.second-y0; m_history.pop_back();
    last = m_history.back(); x3=last.first-x0; y3 = last.second-y0;

    // where we have 3 pts (0,0), (x2,y2),( x3,y3), the extremum is at
    xtop = (pow(x3,2)*y2 - pow(x2,2)*y3)/(2*x3*y2 - 2*x2*y3);
    xtop=max(xtop,x3);  // safety.
    dx = xtop-x0;
    // set initial step using the time back to the peak
//    this->m_h1 = min(this->m_h1,-dx/8. );
//    this->m_h1 =max(0.000001, this->m_h1);
    // but go back one timestep
    //    dx = x2;
    assert(dx<0)  ;

    return dx;
}
