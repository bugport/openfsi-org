#ifndef RXRUNGEKUTTA_H
#define RXRUNGEKUTTA_H
#include "rxodestepper.h"
class RXRungeKutta : public RXODEStepper
{
public:
    RXRungeKutta();
    int test() ; //testrkdumb;
    int runtopeak() ;
protected:
    int odeint(Vec_IO_DP &ystart, const DP eps,
               const DP h1, const DP hmin, int &nok, int &nbad );
       void derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx);

    void  rkqs(Vec_IO_DP &y, Vec_IO_DP &dydx, DP &x, const DP htry,
               const DP eps, Vec_I_DP &yscal, DP &hdid, DP &hnext );
    void rkck(Vec_I_DP &y, Vec_I_DP &dydx, const DP x,
              const DP h, Vec_O_DP &yout, Vec_O_DP &yerr) ;
};

#endif // RXRUNGEKUTTA_H
