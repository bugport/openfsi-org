#ifndef RXODESTEPPER_H
#define RXODESTEPPER_H
#include "/home/r3/software/nr/cpp/other/nr.h"
#define MKL_INT int
#include <vector>
#include <deque>

class RXODEStepper
{
public:
    RXODEStepper();
    virtual ~RXODEStepper();
    virtual int test()=0; //  testrkdumb(), testodeint();
    virtual int runtopeak()=0;  //rksolve, stiffsolve
    void SetKit(const int k){ m_kit=k;}
    int Kit(){ return m_kit;}
    double GetH1(void){return m_h1;}
    void setH1(const double x) {m_h1=x;}
    void  SetDamping(const double x) {m_damp=x;}
    void  SetEps(const double x) {m_eps=x;}
    std::string Describe()const;
protected:
    virtual int odeint(Vec_IO_DP &ystart, const DP eps,
                       const DP h1, const DP hmin, int &nok, int &nbad )=0;
    virtual void derivs(const DP x, Vec_I_DP &y, Vec_O_DP &dydx)=0;
    int nrhs; // count of the number of calls to derivs
    int m_kit;
    // NR  'globals'
    DP dxsav;
    int kmax,kount;
    Vec_DP *xp_p;
    Mat_DP *yp_p;

    Vec_DP *x_p;
    Mat_DP *d_p;
    double m_damp;
    double m_h1;
    double m_eps;
    int m_dbg;

    int LogEnergy (const double x, const double ekt);
    double EnergyPeak();
    std::vector< pair<double,double > > m_history;

    int LogCoords(const double x,double* y, const int n);
    int SetCoordsToPeak(double *y, const int n);
    std::deque<pair<double,std::vector<double> > >  m_oldcoords;
};

#endif // RXODESTEPPER_H
