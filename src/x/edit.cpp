/* call backs for asscol.c 

26 june 02  edit_choose_cb was using an unitialised
15/3/97 prototype of choose_database_item added 


*/

#include "StdAfx.h"  
#include "RXSail.h"

#ifndef _WINDOWS
	#include <X11/Intrinsic.h>
	#include <X11/Shell.h>

	#include <Xm/Xm.h>
	#include <Xm/List.h>
#endif


#include "griddefs.h"
#include "entities.h"
#include "elements.h"
#include "GraphicStruct.h"
#include "resdiag.h"
#include "resolve.h"
#include "recolor.h"
#include "RXQuantity.h"
#include "redcb.h"

#include "edit.h"

/* Move to edit.h Thomas 11 06 04
struct EDIT_DATA {
	Widget fab_label;
	SAIL *sail;
	Widget shell;
};

typedef struct EDIT_DATA Edit_Data;
*/

static int NOT_DONE;
static int RETVAL;



/*    NOTE : This routine is MODAL. It only returns when
          the dialog is destroyed in some way.

 */

int do_entity_edit(Widget parent,SAIL *const sail,const char *filter)
{
Widget eshell = (Widget) NULL;
XtInputMask xmask;
RXEntity_p  eEnt;
int retVal=0;
/*
SailData *sd;
char buf[128];
struct PC_SEAMCURVE *sc;
float value;
static FILE *fp=NULL;
static int EditCount=0;
if(!fp)
  fp = FOPEN("editlog.out","w");
sd = SailToData(sail);

eEnt = choose_database_item(sail,parent,filter,"Select Entity to Edit");

if(eEnt) {
  retVal = 1;
  printf("Now edit the '%s' '%s'\n",eEnt->type,eEnt->name);
  sc = (PC_SEAMCURVE*)eEnt->dataptr;
  printf("original line = '%s'\n",eEnt->line);
  printf("Enter new depth : ");
//  gets(buf);
	fgets(buf,128,stdin);  // linux gcc says this is safer
  value = atof(buf);
  
  fprintf(fp,"Edit %d:%s: '%s' '%s' DEPTH changed from %f -> %f\n",EditCount,sd->isail->GetType().c_str(),eEnt->type,eEnt->name,PC_Value_Of_Q(sc->D),value);
 
 PCQ_Set_Quantity_By_Value(&(sc->D), value);
  eEnt->Needs_Computing = 1;  
}

eshell = create_entity_shell(parent,sail);	
RETVAL = 0;
NOT_DONE = 1;
InEdit = 1;



while(NOT_DONE) {	
    xmask = XtAppPending(app_context);
    if(xmask) 
	XtAppProcessEvent(app_context,xmask);
}

printf("leaving edit_entity\n");

InEdit = 0;
XtDestroyWidget(eshell); 	
*/
return(retVal);
}



void edit_cancel_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
/* simply remove the window and tidy up 
 * 
 */
Edit_Data *ad = (Edit_Data *)client_data;
RETVAL=0;
NOT_DONE = 0;
free(ad);
}//void edit_cancel_CB


void edit_choose_CB(Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{

RXEntity_p  ce;
int 	ac = 0;
// Arg al[3];
Edit_Data *ad = (Edit_Data *)client_data;
XmString xmstring;
/*
oldsail = SetCurrentSail(ad->sail);
sprintf(buf,"Please choose a %s to ACTIVATE",ad->typeStr);
ce = choose_database_item(ad->shell,ad->typeStr,buf);
if(ce) {
	xmstring = XmStringCreateLtoR(ce->name, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[ac], XmNlabelString, xmstring); ac++;
        XtSetValues ( ad->fab_label,al, ac );
	XmStringFree ( xmstring );
	ac = 0;
	InEdit = 1;

}

SetCurrentSail(oldsail); 
*/
}//void edit_choose_CB




