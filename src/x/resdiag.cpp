/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 *      27.11.94                AKM             changed to move resolve dialog to top left of screen
 */


#include "StdAfx.h"

#include "RXSail.h"
/* NOTES:
 * 
 *
 */



#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/Protocols.h>
#include <Xm/List.h>
#include <Xm/DialogS.h>
#include <Xm/Form.h>
#include <Xm/Frame.h>
#include <Xm/Label.h>
#include <Xm/PushB.h>
#include <Xm/ScrollBar.h>
#include <Xm/ScrolledW.h>
#include <Xm/TextF.h>
#include <Xm/LabelG.h>


#include "elements.h"
#include "controlmenu.h"
#include "mast.h"
#include "GraphicStruct.h"
#include "resolve.h"
#include "report.h"

#include "rescb.h"
#include "global_declarations.h"

#include "resdiag.h"

#ifndef VERT_BUTTON_SPACE
#define VERT_BUTTON_SPACE  ((int) (20))
#endif //#ifndef VERT_BUTTON_SPACE

#ifndef RIGHT_OF_LIST
#define RIGHT_OF_LIST  ((int) (300))
#endif //#ifndef RIGHT_OF_LIST


static Widget l_ParentWidget = (Widget)NULL;	/* main l_ParentWidget - later to be used to only generate 
			 		* once and hence save a bit of time / effort 
			 		*/

Widget create_resolve_shell(Graphic *g)
{
	Display *display;
	Widget children[100];      /* Children to manage */

	Widget w_shell = (Widget)NULL;	/* main shell  */
	Widget w_form = (Widget)NULL;	/* main form  */
	Widget w_list = (Widget)NULL;	/* list widget */
	Widget w_listSW = (Widget)NULL;	/* list widget */

	Widget wb_select = (Widget)NULL;	/* buttons */
	Widget wb_abort = (Widget)NULL;
	Widget wb_append = (Widget)NULL;
	Widget wb_scan = (Widget)NULL;
	Widget wb_rename = (Widget)NULL;

	Widget w_list_label = (Widget) NULL;	/* label */

	XmString *Items; 		/* For list items */
	int nitems;        		/* Index for list_items */
	XmString xmstrings[15];    	/* temporary storage for XmStrings */

	Atom	wm_delete_window;
	char buf[256];
	Arg al[64]; 
	ResData *rd;
	SAIL *s = g->GSail;
	int l_ac=0;
	rd = (ResData*) MALLOC(sizeof(ResData));


	l_ParentWidget = g->mainWindow;
	display = XtDisplay ( l_ParentWidget );

	XtSetArg(al[l_ac], XmNallowShellResize, FALSE); l_ac++;
	XtSetArg(al[l_ac], XtNy, 50); l_ac++;
	XtSetArg(al[l_ac], XtNx, 30); l_ac++;
	XtSetArg(al[l_ac], XmNy, 50); l_ac++;
	XtSetArg(al[l_ac], XmNx, 30); l_ac++;
 	
	if(s)
		sprintf(buf,"Unresolved References in '%s'",s->GetType().c_str());
	else 
		sprintf(buf,"Unresolved References in '%s'","unknown");
	XtSetArg(al[l_ac],XmNtitle,buf); l_ac++;
	w_shell = XtCreatePopupShell("Resolve", topLevelShellWidgetClass,l_ParentWidget,al,l_ac);

	l_ac = 0;


	/* This makes 'closing' the window == aborting the build */
        wm_delete_window = XmInternAtom(XtDisplay(w_shell),
				(char*)"WM_DELETE_WINDOW",
				FALSE);
    
        XmAddWMProtocolCallback(w_shell, wm_delete_window, (XtCallbackProc)resolve_abort_CB, (XtPointer) rd);



	XtSetArg(al[l_ac], XmNautoUnmanage, FALSE); l_ac++;
	w_form = XmCreateForm ( w_shell, (char*)"resolve_form", al, l_ac );
	l_ac = 0;

	/* create the text label */
	xmstrings[0] = XmStringCreateLtoR((char*)"Missing Items", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	w_list_label = XmCreateLabel ( w_form, (char*)"resolve_label", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );



/* add code here to generate the item list */

	get_unresolved_items(&Items,&nitems);
	
	XtSetArg(al[l_ac], XmNitemCount, nitems); l_ac++;
	XtSetArg(al[l_ac], XmNitems, Items); l_ac++;
	XtSetArg(al[l_ac], XmNselectionPolicy, XmSINGLE_SELECT); l_ac++;
	XtSetArg(al[l_ac], XmNlistSizePolicy, XmCONSTANT); l_ac++;
	XtSetArg(al[l_ac], XmNscrollBarDisplayPolicy, XmAS_NEEDED); l_ac++;
	XtSetArg(al[l_ac], XmNvisibleItemCount, 12); l_ac++;
	w_list = XmCreateScrolledList ( w_form,(char*) "resolve_list",al,l_ac);
	l_ac=0;
	
	w_listSW = XtParent(w_list);

	/* create the push buttons */

	xmstrings[0] = XmStringCreateLtoR((char*)"Select...", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_select = XmCreatePushButton ( w_form,(char*) "resolve_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );

	xmstrings[0] = XmStringCreateLtoR((char*)"Append File...", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_append = XmCreatePushButton ( w_form, (char*)"resolve_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );

	xmstrings[0] = XmStringCreateLtoR((char*)"Del whats asking", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_scan = XmCreatePushButton ( w_form, (char*)"resolve_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );

	xmstrings[0] = XmStringCreateLtoR((char*)"Alias...", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_rename = XmCreatePushButton ( w_form,(char*) "resolve_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );

	xmstrings[0] = XmStringCreateLtoR((char*)"Abort Build", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	wb_abort = XmCreatePushButton ( w_form, (char*)"resolve_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );


/* 	Now give each a means of positioning iteself relative to 
	either another widget or the form.
 */
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_OPPOSITE_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, -RIGHT_OF_LIST); l_ac++;
        XtSetValues ( w_list_label,al, l_ac );
	l_ac = 0;


	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, w_list_label); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_select,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, VERT_BUTTON_SPACE); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, wb_select); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_append,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, VERT_BUTTON_SPACE); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, wb_append); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_scan,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, VERT_BUTTON_SPACE); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, wb_scan); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_rename,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, VERT_BUTTON_SPACE); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, wb_rename); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNbottomOffset,20); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, RIGHT_OF_LIST+20); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, 20); l_ac++;
        XtSetValues ( wb_abort,al, l_ac );
	l_ac = 0;

	children[l_ac++] = w_list_label;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = wb_select;
	children[l_ac++] = wb_append;
	children[l_ac++] = wb_scan;
	children[l_ac++] = wb_rename;
	children[l_ac++] = wb_abort;
	XtManageChildren(children, l_ac);
	l_ac=0;


	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNtopWidget, w_list_label); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_OPPOSITE_WIDGET); l_ac++;
	XtSetArg(al[l_ac], XmNbottomWidget, wb_abort); l_ac++;
	XtSetArg(al[l_ac], XmNbottomOffset, 0); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 10); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_OPPOSITE_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNrightOffset, -RIGHT_OF_LIST); l_ac++;
        XtSetValues ( w_listSW,al, l_ac );
	l_ac = 0;

	XtAddCallback(w_list, XmNsingleSelectionCallback, (XtCallbackProc)single_select_CB,  (XtPointer)rd);
	XtAddCallback(wb_select, XmNactivateCallback, (XtCallbackProc)resolve_select_CB,  (XtPointer)rd);
	XtAddCallback(wb_append, XmNactivateCallback, (XtCallbackProc)resolve_append_CB,  (XtPointer)rd);
	XtAddCallback(wb_scan, XmNactivateCallback, (XtCallbackProc)resolve_scan_CB,  (XtPointer)rd);
	XtAddCallback(wb_rename, XmNactivateCallback, (XtCallbackProc)resolve_rename_CB,  (XtPointer)rd);
	XtAddCallback(wb_abort, XmNactivateCallback, (XtCallbackProc)resolve_abort_CB,  (XtPointer)rd);

	children[l_ac++] = w_list;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = w_listSW;
	XtManageChildren(children, l_ac);
	l_ac=0;

	children[l_ac++] = w_form;
	XtManageChildren(children, l_ac);
	l_ac=0;

/*
	children[l_ac++] = w_shell;
	XtManageChildren(children, l_ac);
	l_ac=0;
*/


	l_ac=0;
	XtSetArg(al[l_ac],XtNx,30);l_ac++;
	XtSetArg(al[l_ac],XtNy,50);l_ac++;
	XtSetValues(w_shell,al,l_ac);


	    rd->shell = w_shell;
	    rd->select = wb_select;
	    rd->append = wb_append;
	    rd->scan = wb_scan;
	    rd->rename = wb_rename;
	    rd->abort = wb_abort;
	    rd->chosen = -1;
	    rd->m_g =  g;
	    rd->m_ri=0;
	    rd->m_elist=0;
	    rd->Items = (char*) Items;

	XtSetSensitive(rd->select,0);
	XtSetSensitive(rd->append,1);	/* always append */
	XtSetSensitive(rd->scan,0);
	XtSetSensitive(rd->rename,0);
	XtSetSensitive(rd->abort,1);	/* always abort */

	HC_Update_Display();

return(w_shell);
}



