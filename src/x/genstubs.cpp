#include "StdAfx.h" 
#include "GraphicStruct.h"

#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/PushB.h>

#include <gen.h>

#include "global_declarations.h"

#include "genstubs.h"

#ifndef MAX_VALUES
	#define MAX_VALUES 17
#endif 

static int SUCCESS;
static int NOT_DONE;
Widget boatWidget = NULL;

Widget create_boat_shell (Widget parent);

int do_boat_dialog(Graphic *g)
{
	Widget rshell = (Widget) NULL;
	XtInputMask xmask;
	//Arg al[5];
	int ac=0;

	rshell = create_boat_shell(g->topLevel);	
	boatWidget = rshell;

	SUCCESS=0;
	NOT_DONE = 1;
	while(NOT_DONE) {		/* MODAL event loop */
		xmask = XtAppPending(g_app_context);
		if(xmask) 
		XtAppProcessEvent(g_app_context,xmask);
	}

	XFlush(XtDisplay(g->topLevel));
	boatWidget = NULL;
	return(SUCCESS);
}  //int do_boat_dialog


void boat_generate_CB (Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
	int i;
	for(i=0;i<MAX_VALUES;i++) { 
	  set_one_data_item(i);
	}
	SUCCESS=1;
	NOT_DONE=0;
	XtDestroyWidget(boatWidget); 	
}//void boat_generate_CB 

void boat_cancel_CB (Widget w,caddr_t client_data,XmAnyCallbackStruct *call_data)
{
	SUCCESS=0;
	NOT_DONE=0;
	XtDestroyWidget(boatWidget); 	
}//void boat_cancel_CB 

