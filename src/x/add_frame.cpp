/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 */
 ///////////////////////////////////////////////////////////////////////////
 //
 // January 2006 a major re-working.
 // the Options menu is supposed to fill itself up from the exclusive and non-ex segments of each model
 // and to take it's current settings from UOs in the graphic segment. (in the same place that OpenView puts them) 
 // It's job is just to manipulate these UOs. and other atts like visibility
 // somewhere downstream (like UpdateAll) we read these atts and include the right segments. 
 // Ideally the frame would check from time to time whether 
 // A) there are any segs in the models which aren't reflected in its widgets and
 // B) whether it is listing anything which doesnt exist. 
 // It is important that the menu is safe whether or not the segments exist.
 //
 ////////////////////////////////////////////////////////////////////////////
 
 /* 
 *
 * Modified :
 Jan 2003  a linux bug. There was a double XtStringFree
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h" 
#include "RXSail.h" 
#ifdef _X
	#include <X11/Xatom.h>
	#include <X11/Intrinsic.h>
	#include <X11/Shell.h>

	#include <Xm/Xm.h>
	#include <Xm/DialogS.h>
	#include <Xm/Form.h>
	#include <Xm/Frame.h>
	#include <Xm/Label.h>
	#include <Xm/PushB.h>
	#include <Xm/RowColumn.h>
	#include <Xm/ScrollBar.h>
	#include <Xm/ScrolledW.h>
	#include <Xm/ToggleB.h>
	#include <Xm/CascadeBG.h>
	#include <Xm/LabelG.h>
#endif
//#include <stdio.h> 
//#include <string.h>
#include "GraphicStruct.h"
#include "freeze.h"
#include "elements.h"
#include "attachmod.h"

#include "find_include.h"
#include "global_declarations.h"

#include "add_frame.h"
#include "cp2mainstub.h"

#include "akmpretty.h" 


#define DATA_FRAME_WIDTH 200


/* routine to add a sail frame to the Options Menu*/

Frame *add_sail_frame(Widget parent,int position,char *name,SAIL *p_sail,FrameData *p_fd)
{
	Widget data_frame_1 = (Widget)NULL;
	Widget data_form_1 = (Widget)NULL;
	Widget exc_window = (Widget)NULL;
	Widget exc_sbar2 = (Widget)NULL;
	Widget exc_sbar1 = (Widget)NULL;
	Widget exc_rowcol = (Widget)NULL;

        //Widget EXXclusive_tog[MAX_TOGGLES];
        //int l_EXXclusive_tog_count=(int) 0;

	Widget 	NonExclusive_tog[MAX_TOGGLES];
	int 	NonExclusive_tog_count=(int) 0;

	Widget disp_window = (Widget)NULL;
		Widget disp_sbar2 = (Widget)NULL;
	Widget disp_sbar1 = (Widget)NULL;
	Widget disp_rowcol = (Widget)NULL;
	Widget disp_tog[MAX_TOGGLES];
	int disp_tog_count=(int) 0;
	Widget prop_menu = (Widget)NULL;
	Widget prop_cascade = (Widget)NULL;
	Widget prop_list = (Widget)NULL;
	Widget prop_button[MAX_TOGGLES];
	int prop_button_count=(int) 0;
	Widget name_label = (Widget)NULL;
	Widget name_frame = (Widget)NULL;
	Widget l_asgraph = (Widget)NULL;
	Widget l_freezebutton = (Widget)NULL;
	Widget children[10];

	HoopsItem *inc_items = NULL;
	HoopsItem *disp_items =NULL;
	HoopsItem *prop_items =NULL;
	int l_graphstate,l_freeze_state;
	Frame *frame;		/* return data */
	Arg al[128]; 
	int l_ac=0;
	int chosen=0;
	Graphic *g=0;
	char bgcolor[256];
	int i;	/* just a counter */
	char buf[512];
	XmString xmstrings[256];    /* temporary storage for XmStrings */
	Widget contour_button;

	g = p_fd->m_g;
	frame = (Frame *) CALLOC(1,sizeof(Frame));
	frame->position = position;
	frame->sailname = STRDUP(name);

	XtSetArg(al[l_ac], XmNshadowType, XmSHADOW_ETCHED_IN); l_ac++;
	XtSetArg(al[l_ac], XmNmarginHeight, 5); l_ac++;
	XtSetArg(al[l_ac], XmNmarginWidth, 5); l_ac++;
	data_frame_1 = XmCreateFrame ( parent, (char*)"data_frame_1", al, l_ac );
	l_ac = 0;
	data_form_1 = XmCreateForm ( data_frame_1, (char*)"data_form_1", al, l_ac );
	XtSetArg(al[l_ac], XmNx, 3); l_ac++;
	XtSetArg(al[l_ac], XmNwidth, 170); l_ac++;
	XtSetArg(al[l_ac], XmNheight, 68); l_ac++;
	XtSetArg(al[l_ac], XmNborderWidth, 0); l_ac++;
	XtSetArg(al[l_ac], XmNspacing, 0); l_ac++;
	XtSetArg(al[l_ac], XmNscrollBarDisplayPolicy, XmAS_NEEDED); l_ac++;
	XtSetArg(al[l_ac], XmNscrollingPolicy, XmAUTOMATIC); l_ac++;
	XtSetArg(al[l_ac], XmNvisualPolicy, XmCONSTANT); l_ac++;
	exc_window = XmCreateScrolledWindow ( data_form_1, (char*)"exc_window", al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNhorizontalScrollBar, &exc_sbar2 ); l_ac++;
	XtSetArg(al[l_ac], XmNverticalScrollBar, &exc_sbar1 ); l_ac++;
	XtGetValues(exc_window, al, l_ac );
	l_ac = 0;
	XtSetArg(al[l_ac], XmNincrement, 18); l_ac++;
	XtSetValues(exc_sbar1, al, l_ac );

	l_ac = 0;
	XtSetArg(al[l_ac], XmNspacing, 0); l_ac++;
	XtSetArg(al[l_ac], XmNmarginHeight, 0); l_ac++;
	XtSetArg(al[l_ac], XmNentryAlignment, XmALIGNMENT_BEGINNING); l_ac++;
	XtSetArg(al[l_ac], XmNradioAlwaysOne, FALSE); l_ac++;
	XtSetArg(al[l_ac], XmNradioBehavior, FALSE); l_ac++;
	XtSetArg(al[l_ac], XmNresizeHeight, TRUE); l_ac++;
	exc_rowcol = XmCreateRowColumn ( exc_window, (char*)"exc_rowcol", al, l_ac ); l_ac=0;

/* add routine to find include items here */
	NonExclusive_tog_count = 0;

// the nonexclusive items.  We should search the sail's PPNonExc segment. for candidates.		
	find_all_include_items_by_sail(p_sail,&NonExclusive_tog_count,&inc_items);

// this appears to search the display graphics non-excl segment for 'includes' to any of the
// segments found by the call above, and sets the state member of the item. 	
	Get_state_NonEx_items(g,name,NonExclusive_tog_count,inc_items);
	
	frame->inc_items = inc_items;
	frame->inc_count = NonExclusive_tog_count;
	assert(NonExclusive_tog_count<MAX_TOGGLES);


	for(i=0;i<NonExclusive_tog_count;i++) { // things like 'horizon'
		l_ac=0;
		xmstrings[0] = XmStringCreateLtoR(inc_items[i].name, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
		XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
		XtSetArg(al[l_ac], XmNset,inc_items[i].state); l_ac++;
		XtSetArg(al[l_ac], XmNmarginTop, 0); l_ac++;
		XtSetArg(al[l_ac], XmNmarginBottom, 0); l_ac++;
		XtSetArg(al[l_ac], XmNmarginHeight, 0); l_ac++;
		XtSetArg(al[l_ac], XmNalignment, XmALIGNMENT_BEGINNING); l_ac++;
		XtSetArg(al[l_ac], XmNindicatorOn, TRUE); l_ac++;
		XtSetArg(al[l_ac], XmNindicatorType, XmN_OF_MANY); l_ac++;
		XtSetArg(al[l_ac], XmNspacing, 0); l_ac++;
		NonExclusive_tog[i] = XmCreateToggleButton ( exc_rowcol, (char*)"exc_tog", al, l_ac );
		inc_items[i].widget = 	NonExclusive_tog[i];
		
		XtAddCallback (NonExclusive_tog[i], XmNvalueChangedCallback, (XtCallbackProc)inc_value_changed,(XPointer) frame);
		l_ac = 0;
		XmStringFree ( xmstrings [ 0 ] );
	} /* set all include toggle buttons */




/* now  the display scroll window */
	l_ac = 0;
	XtSetArg(al[l_ac], XmNx, 3); l_ac++;
	XtSetArg(al[l_ac], XmNwidth, 170); l_ac++;
	XtSetArg(al[l_ac], XmNheight, 68); l_ac++;
	XtSetArg(al[l_ac], XmNborderWidth, 0); l_ac++;
	XtSetArg(al[l_ac], XmNspacing, 0); l_ac++;
	XtSetArg(al[l_ac], XmNscrollBarDisplayPolicy, XmAS_NEEDED); l_ac++;
	XtSetArg(al[l_ac], XmNscrollingPolicy, XmAUTOMATIC); l_ac++;
	XtSetArg(al[l_ac], XmNvisualPolicy, XmCONSTANT); l_ac++;
	disp_window = XmCreateScrolledWindow ( data_form_1, (char*)"disp_window", al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNhorizontalScrollBar, &disp_sbar2 ); l_ac++;
	XtSetArg(al[l_ac], XmNverticalScrollBar, &disp_sbar1 ); l_ac++;
	XtGetValues(disp_window, al, l_ac );
	l_ac = 0;
	XtSetArg(al[l_ac], XmNspacing, 0); l_ac++;
	XtSetArg(al[l_ac], XmNmarginHeight, 0); l_ac++;
	XtSetArg(al[l_ac], XmNentryAlignment, XmALIGNMENT_BEGINNING); l_ac++;
	XtSetArg(al[l_ac], XmNradioAlwaysOne, FALSE); l_ac++;
	XtSetArg(al[l_ac], XmNradioBehavior, FALSE); l_ac++;
	XtSetArg(al[l_ac], XmNresizeHeight, TRUE); l_ac++;
	disp_rowcol = XmCreateRowColumn ( disp_window,(char*) "disp_rowcol", al, l_ac );
	l_ac = 0;



/////////////////////////////////////////////////////////
///
/// THe 'How to display' buttons at the bottom
///
//////////////////////////////////////////////////////

/* add the routine to get the names etc. of display info here 
 * including states of each toggle 
 */
	disp_tog_count=0;
	Initialize_OptionStyle_Items(name,&disp_tog_count,&disp_items); // mesh, hard contours, etc

// synch the buttons to the user options.

	Get_State_OptionStyle_Items(g,name,disp_tog_count,disp_items);
	frame->disp_items = disp_items;
	frame->disp_count = disp_tog_count;
	assert(disp_tog_count<MAX_TOGGLES);
	for(i=0;i<disp_tog_count;i++) {
		assert(l_ac==0);
		xmstrings[0] = XmStringCreateLtoR(disp_items[i].name, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
		XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
		XtSetArg(al[l_ac], XmNset, disp_items[i].state); l_ac++;
		XtSetArg(al[l_ac], XmNuserData, i); l_ac++;
		XtSetArg(al[l_ac], XmNmarginTop, 0); l_ac++;
		XtSetArg(al[l_ac], XmNmarginBottom, 0); l_ac++;
		XtSetArg(al[l_ac], XmNmarginHeight, 0); l_ac++;
		XtSetArg(al[l_ac], XmNalignment, XmALIGNMENT_BEGINNING); l_ac++;
		XtSetArg(al[l_ac], XmNindicatorOn, TRUE); l_ac++;
		XtSetArg(al[l_ac], XmNindicatorType, XmN_OF_MANY); l_ac++;
		XtSetArg(al[l_ac], XmNspacing, 0); l_ac++;
		disp_items[i].widget = 	disp_tog[i] = XmCreateToggleButton ( disp_rowcol,(char*) "disp_tog", al, l_ac );
		XtAddCallback (disp_tog[i], XmNvalueChangedCallback, (XtCallbackProc)disp_value_changed,(XPointer) frame);
		l_ac = 0;
		XmStringFree ( xmstrings [ 0 ] );
	}




/* now for the pop up stuff These are the Exclusive Items */

	l_ac = 0;

	l_graphstate = Get_state_graph_button(g->m_ModelSeg,name);

	xmstrings[0] = XmStringCreateLtoR((char*) "As Graph", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	XtSetArg(al[l_ac], XmNset, l_graphstate); l_ac++;
	l_asgraph = XmCreateToggleButton ( data_form_1,(char*) "asgraph", al, l_ac );
	XtAddCallback (l_asgraph, XmNvalueChangedCallback, (XtCallbackProc)asgraph_value_changed,(XPointer) p_fd);
	l_ac = 0;
	frame->m_asgraph = l_asgraph;

// the freeze button
	l_freeze_state = p_sail->GetFlag(FL_ISFROZEN);

	xmstrings[0] = XmStringCreateLtoR((char*)"Freeze", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	XtSetArg(al[l_ac], XmNset, l_freeze_state); l_ac++;
	l_freezebutton = XmCreateToggleButton ( data_form_1, (char*)"freeze", al, l_ac );
	XtAddCallback (l_freezebutton, XmNvalueChangedCallback,(XtCallbackProc) freeze_value_changed_CB,(XPointer) p_fd);
	l_ac = 0;
	frame->m_freeze = l_freezebutton;
// end freeze button

	XmStringFree ( xmstrings [ 0 ] );
	XtSetArg(al[l_ac], XmNmarginWidth, 5); l_ac++;
	XtSetArg(al[l_ac], XmNmarginHeight, 1); l_ac++;
	XtSetArg(al[l_ac], XmNradioBehavior, TRUE); l_ac++;
	XtSetArg(al[l_ac], XmNradioAlwaysOne, TRUE); l_ac++;
	prop_list = XmCreatePulldownMenu ( data_form_1,(char*) "prop_list", al, l_ac );
	strcpy(bgcolor, "yellow");
	XtVaSetValues(prop_list,
       		XtVaTypedArg, XmNforeground, XtRString,bgcolor,
       		strlen(bgcolor) + 1, NULL);

	l_ac = 0;


/* add the routine to get the names etc. of exclusive info  here */

	prop_button_count = 0;
	
	Find_All_Exclusive_Items(p_sail,&prop_button_count,&prop_items);
	assert(prop_button_count<MAX_TOGGLES);
	Get_current_shell(g,name,buf);
	
	set_state_property_items(prop_items,prop_button_count,l_graphstate);
	set_chosen_property_items(prop_items,prop_button_count,buf);
	frame->prop_items = prop_items;
	frame->prop_count = prop_button_count;

	for(i=0;i<prop_button_count;i++) {
		if(g_ArgDebug) 
			printf(" (add_sail_frame) create exclusive property button %d  %s\n",i,prop_items[i].name );
		assert(l_ac==0);
		xmstrings[0] = XmStringCreateLtoR(prop_items[i].name, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
		XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
		XtSetArg(al[l_ac], XmNsensitive, prop_items[i].state); l_ac++;
		if(prop_items[i].m_HIChosen) {
			XtSetArg(al[l_ac], XmNset, True); l_ac++;
			chosen = i;
			frame->m_AFchosen = i;
		}
		XtSetArg(al[l_ac],XmNindicatorType,XmONE_OF_MANY);l_ac++;
		prop_items[i].widget = prop_button[i] = XmCreateToggleButton ( prop_list, buf, al, l_ac );
	 
		XtAddCallback (prop_button[i], XmNvalueChangedCallback, (XtCallbackProc)cp_prop_CB,(XPointer) frame);

		l_ac = 0;
		XmStringFree ( xmstrings [ 0 ] );
	}

	l_ac = 0;

	XtSetArg(al[l_ac], XmNspacing, 3); l_ac++;
	XtSetArg(al[l_ac], XmNmarginWidth, 0); l_ac++;
	XtSetArg(al[l_ac], XmNentryAlignment, XmALIGNMENT_CENTER); l_ac++;
	XtSetArg(al[l_ac], XmNadjustLast, TRUE); l_ac++;
	XtSetArg(al[l_ac], XmNsubMenuId, prop_list); l_ac++;
	XtSetArg(al[l_ac], XmNradioBehavior, TRUE); l_ac++;
	XtSetArg(al[l_ac], XmNradioAlwaysOne, TRUE); l_ac++;
	XtSetArg(al[l_ac], XmNmenuHistory, prop_button[chosen]); l_ac++;
	prop_menu = XmCreateOptionMenu ( data_form_1,(char*) "prop_menu", al, l_ac );
	l_ac = 0;
	prop_cascade = XmOptionButtonGadget ( prop_menu );
	XtSetArg(al[l_ac], XmNmarginWidth, 0); l_ac++;
	XtSetArg(al[l_ac], XmNalignment, XmALIGNMENT_CENTER); l_ac++;
        XtSetValues ( prop_cascade,al, l_ac );
	l_ac = 0;


	sprintf(buf,"name_frame_%d",position);
	name_frame = XmCreateFrame ( parent, buf, al, l_ac );
	char mybuf[512]; strcpy(mybuf,p_sail->GetType().c_str());
	xmstrings[0] = XmStringCreateLtoR(mybuf, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
	sprintf(buf,"name_label_%d",position);
	name_label = XmCreateLabel ( name_frame, buf, al, l_ac );
	strcpy(bgcolor, "red");
	XtVaSetValues(name_label,
       		XtVaTypedArg, XmNbackground, XtRString,bgcolor,
       		strlen(bgcolor) + 1, NULL);
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );
	xmstrings[0] = XmStringCreateLtoR( (char*)"Levels", (XmStringCharSet)XmFONTLIST_DEFAULT_TAG);
	XtSetArg(al[l_ac], XmNlabelString, xmstrings[0]); l_ac++;
        contour_button = XmCreatePushButton ( data_form_1,(char*) "contour_button", al, l_ac );
	l_ac = 0;
	XmStringFree ( xmstrings [ 0 ] );
	XtAddCallback (contour_button, XmNactivateCallback, (XtCallbackProc)set_contours_cb,(XtPointer) frame);

/* position the items accordingly in the surrounding frame */

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 56); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 3); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
        XtSetValues ( exc_window,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 146); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 3); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
        XtSetValues ( disp_window,al, l_ac );
	l_ac = 0;

	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 2); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, -7); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
        XtSetValues ( prop_menu,al, l_ac );
// as graph button
	l_ac = 0;
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 33); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 8); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNsensitive,graphmask(prop_items[chosen].name)); l_ac++;
        XtSetValues ( l_asgraph,al, l_ac );

// freeze button
	l_ac = 0;
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 33); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 90); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
//	XtSetArg(al[l_ac], XmNsensitive,graphmask(prop_items[chosen].name)); l_ac++;
        XtSetValues ( l_freezebutton,al, l_ac );

// name_frame is the field at the top saying 'boat', etc
	l_ac=0;
	XtSetArg(al[l_ac], XmNwidth, 76); l_ac++; // for a debug, was 76
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_POSITION); l_ac++;
	XtSetArg(al[l_ac], XmNtopPosition, 2); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 5+DATA_FRAME_WIDTH*(position-1)+60); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;

        XtSetValues ( name_frame,al, l_ac );
//levels button
        l_ac=0;
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNtopOffset, 8); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 120); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
        XtSetValues (contour_button,al, l_ac );


XtManageChildren(NonExclusive_tog,NonExclusive_tog_count);   /* manage include toggles */
XtManageChildren(disp_tog,disp_tog_count); /* and disp toggles */

	l_ac = 0;
	children[l_ac++] = exc_rowcol;		/* and row column widgets */
	XtManageChildren(children, l_ac);
	l_ac = 0;
	children[l_ac++] = disp_rowcol;		/* and row column widgets */
	XtManageChildren(children, l_ac);

	l_ac = 0;
	children[l_ac++] = name_label;		/* and property menu */

	XtManageChildren(children, l_ac);
	l_ac = 0;
	XmScrolledWindowSetAreas(exc_window, exc_sbar2, exc_sbar1, exc_rowcol );
	XmScrolledWindowSetAreas(disp_window, disp_sbar2, disp_sbar1, disp_rowcol );

XtManageChildren(prop_button,prop_button_count); /* and property buttons */
	l_ac = 0;
	children[l_ac++] = prop_menu;		/* all children  */
	children[l_ac++] = l_asgraph;		/* of data_form_1 */
	children[l_ac++] = l_freezebutton;	
	children[l_ac++] = disp_window;	
	children[l_ac++] = exc_window;		
        children[l_ac++] = contour_button;
	XtManageChildren(children, l_ac);


	l_ac = 0;
	children[l_ac++] = name_frame;		/* and property menu */
	XtManageChildren(children, l_ac);

	l_ac = 0;
	children[l_ac++] = data_form_1;		/* and property menu */
	XtManageChildren(children, l_ac);

// LOOKS WRONG NOT TO RE-ZERO Sept 13 2006
	l_ac=0;
	XtSetArg(al[l_ac], XmNtopAttachment, XmATTACH_POSITION); l_ac++;
	XtSetArg(al[l_ac], XmNtopPosition, 12); l_ac++;
	XtSetArg(al[l_ac], XmNbottomAttachment, XmATTACH_NONE); l_ac++;
	XtSetArg(al[l_ac], XmNleftAttachment, XmATTACH_FORM); l_ac++;
	XtSetArg(al[l_ac], XmNleftOffset, 5+DATA_FRAME_WIDTH*(position-1)); l_ac++;
	XtSetArg(al[l_ac], XmNrightAttachment, XmATTACH_NONE); l_ac++;
        XtSetValues ( data_frame_1,al, l_ac );
	l_ac = 0;

	l_ac = 0;
	children[l_ac++] = data_frame_1;		
	XtManageChildren(children, l_ac);

frame->frame_widget = data_frame_1;
frame->sail = p_sail;
return(frame);
}
