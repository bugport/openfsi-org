/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by  P Heppel
 *
 * 
 *
 * Modified :
 *  aug 97 reset Solve button removed
 * 15/5/96 menu names Open BOAT.. etc 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* CreateMenuBar func. - Sailmenu */

#include "StdAfx.h" 

#include "GraphicStruct.h"

#include "Global.h"

#include "newmenu.h"
#include "menuCB.h"
#include "custom.h"
#include <string.h>
#include "trimmenu.h"
#include "global_declarations.h"

#include "Menu.h"

Widget CreateMenuBar(Widget parent,Graphic *g)
{
	Widget menuBar;
	Widget cascade;
	Widget pulldownPane;
	XmString label;
	Arg al[64];
	/* Create menu bar */
	int l_ac=0;
	menuBar = XmCreateMenuBar(parent,(char*) "menuBar",al,l_ac);
	assert(XtIsWidget(menuBar));
	/*****************************************************************************/
	/***************************   FILE MENU   ***********************************/
	/*****************************************************************************/
	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
	/**** new button *****/
	/* XtSetSensitive( */

	l_ac=0;
	g->New_Menu = XmCreatePulldownMenu(pulldownPane,(char*)"submenuNew",al,l_ac);
	assert(XtIsWidget(g->New_Menu));
	label = XmStringCreateSimple((char*)"New");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,g->New_Menu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'N');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,(char*)"New",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);
	 Create_NewMenu(g->New_Menu,g);

	l_ac=0;
	g->Open_Menu = XmCreatePulldownMenu(pulldownPane,(char*)"submenuOpen",al,l_ac);

	label = XmStringCreateSimple((char*)"Open");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,g->Open_Menu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'O');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,(char*)"Open",al,l_ac); 
	XtManageChild(cascade);
	XmStringFree(label);
	Create_OpenMenu(g->Open_Menu,g);

	//CreateMenuButton(pulldownPane,NULL,MENU_File_New,"New Boat",'N',PushButton,NULL,NULL,g);


	/**** open button *****/
	//CreateMenuButton(pulldownPane,NULL,MENU_File_Open,"Open Boat",'O',PushButton,NULL,NULL,g);

	/**** Append button *****/
	//CreateMenuButton(pulldownPane,NULL,MENU_SAIL_APPEND,"Append To Boat",'A',PushButton,NULL,NULL,g);

	AddSeparator(pulldownPane);

	/**** Append button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_RUN_SCRIPT,"Run Macro",'M',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_RECORD_SCRIPT,"Record Macro",'R',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_END_SCRIPT,"End Record Macro",'E',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_SNAPSHOT,"Save Session",'e',PushButton,NULL,NULL,g);

	AddSeparator(pulldownPane);

	/**** save button *****/
	//saveBoatButton=NULL;
	//saveBoatButton = CreateMenuButton(pulldownPane,NULL,MENU_BOAT_SAVE,"Save Boat",'S',PushButton,NULL,NULL,g);
	//XtSetSensitive(saveBoatButton,0);

	/**** save as button *****/

	CreateMenuButton(pulldownPane,NULL,MENU_BOAT_SAVEAS,"Save Boat As",'v',PushButton,NULL,NULL,g);
	AddSeparator(pulldownPane);

	l_ac=0;
	g->saveStateSubmenu = XmCreatePulldownMenu(pulldownPane,(char*)"submenuState",al,l_ac);

	label = XmStringCreateSimple((char*)"Save State");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,g->saveStateSubmenu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'t');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,NULL,al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);

	g->loadInterpStateSubmenu = XmCreatePulldownMenu(pulldownPane,(char*)"submenuLoadState",al,l_ac);
	label = XmStringCreateSimple((char*)"Load State");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,g->loadInterpStateSubmenu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'I');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,NULL,al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);

	AddSeparator(pulldownPane);
	/**** CWD button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_FILE_CWD,"Change Dir",'A',PushButton,NULL,NULL,g);

	Insert_Custom_Buttons("MainFile",pulldownPane, g);

	AddSeparator(pulldownPane);

	/**** exit button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_FILE_EXIT,"Exit",'E',PushButton,NULL,NULL,g);

	/*** File Cascade button ***/
	label = XmStringCreateSimple((char*)"File");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'F');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,(char*)"File",al,l_ac);
	XtManageChild(cascade);
	XtAddCallback(cascade,XmNcascadingCallback,(XtCallbackProc) MenuCB,mkID(MENU_FILE_CASCADE,g));
	XmStringFree(label);

	/*****************************************************************************/
	/***************************   SAIL MENU   ***********************************/
	/*****************************************************************************/
	/*
	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);
        // *** New button ****
	CreateMenuButton(pulldownPane,NULL,MENU_Sail_New,"From Script",'N',PushButton,NULL,NULL,g);

	AddSeparator(pulldownPane);

        ///  **** Open button *****
	// XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_Sail_Open,"Open Sail",'O',PushButton,NULL,NULL,g),0);
	Insert_Custom_Buttons("MainSail",pulldownPane, g);

        //  *** File Cascade button **
	label = XmStringCreateSimple("Sail");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'S');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,"Sail",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);
	 */
	/*****************************************************************************/
	/***************************   CAMERA MENU   ***********************************/
	/*****************************************************************************/
	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
	/**** New View *****/
	CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_NEW,"New Viewport",'N',PushButton,NULL,NULL,g);


	/**** Open View button *****/
	XtSetSensitive(CreateMenuButton(pulldownPane,NULL,MENU_CAMERA_OPEN,"Open Viewport",'O',PushButton,NULL,NULL,g),0);
	Insert_Custom_Buttons("MainCamera",pulldownPane, g);


	/*** File Cascade button ***/
	label = XmStringCreateSimple((char*)"Viewports");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'C');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,(char*)"Viewports",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);

	/*****************************************************************************/
	/***************************   Hoist MENU   ***********************************/
	/*****************************************************************************/
	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
	/**** opt A button *****/
	// CreateMenuButton(pulldownPane,NULL,MENU_ATTACH_SAIL,"Hoist Bagged",'B',PushButton,NULL,NULL,g);

	/**** save as button *****/
	l_ac=0;
	g->Hoist_SailMenu = XmCreatePulldownMenu(pulldownPane,(char*)"submenuSail",al,l_ac);

	label = XmStringCreateSimple((char*) "Hoist");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,g->Hoist_SailMenu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'D');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,(char*) "Hoist",al,l_ac); 
	XtManageChild(cascade);
	XmStringFree(label);
	l_ac=0;


	g->dropSailSubmenu = XmCreatePulldownMenu(pulldownPane,(char*)"submenuSail",al,l_ac);

	label = XmStringCreateSimple((char*)"Drop");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,g->dropSailSubmenu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'D');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,(char*)"Drop",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);


	/*** File Cascade button ***/
	label = XmStringCreateSimple((char*)"Hoist");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'H');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,(char*)"HoistMenu",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);



	/*****************************************************************************/
	/***************************   Analysis MENU   ***********************************/
	/*****************************************************************************/

	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);


	/**** Settings button *****/
	/*g_trimButton = */CreateMenuButton(pulldownPane,"Trim",MENU_ANALYSIS_TRIM,"Trim",'T',PushButton,NULL,NULL,g);

	CreateMenuButton(pulldownPane,NULL,MENU_AERO_CONTROLS,"Controls",'o',PushButton,NULL,NULL,g);

	AddSeparator(pulldownPane);


	/**** Calculate Now  *****/
	g_AnalysisCalcWidget =CreateMenuButton(pulldownPane,NULL,MENU_ANALYSIS_CALCNOW,"Calculate Now",'C',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_UTILS_CHOOSE_RUN,"Run Input Line(s)",'L',PushButton,NULL,NULL,g);

	AddSeparator(pulldownPane);
	/**** Auto - Calc  *****/
	CreateMenuButton(pulldownPane,NULL,MENU_ANALYSIS_AUTOCALC,"Auto Calculate",'A',CheckBox,NULL,NULL,g);

	#ifdef VPP_INTERFACE
	g_Active_VPP_Widget = CreateMenuButton(pulldownPane,NULL,MENU_ANALYSIS_VPPACTIVE,"Active VPP",'A',CheckBox,NULL,NULL,g);
	g_Passive_VPP_Widget = CreateMenuButton(pulldownPane,NULL,MENU_ANALYSIS_VPPPASSIVE,"Passive VPP",'V',CheckBox,NULL,NULL,g);
	#endif

	AddSeparator(pulldownPane);
	CreateMenuButton(pulldownPane,NULL,MENU_AERO_WIND,"Wind Gradient",'W',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_DO_STRLIN,"Streamlines",'S',PushButton,NULL,NULL,g);

	/**** Auto - Calc  *****/
	g_AnalysisKillWidget = CreateMenuButton(pulldownPane,NULL,MENU_ANALYSIS_KILL,"Stop Analysis",'S',PushButton,NULL,NULL,g);

	Set_Kill_Sensitivity(0);

	//XtSetSensitive(g_AnalysisKillWidget,0);
	Insert_Custom_Buttons("mainAnalysis",pulldownPane, g);

	/*** File Cascade button ***/
	label = XmStringCreateSimple((char*)"Analysis");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'A');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,(char*)"Analysis",al,l_ac);
	XtManageChild(cascade);
	XtAddCallback(cascade,XmNcascadingCallback,(XtCallbackProc) MenuCB,mkID(MENU_ANALYSIS_CASCADE,g));
	XmStringFree(label);

	/*****************************************************************************/
	/***************************   UTILS MENU   ***********************************/
	/*****************************************************************************/

	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
	/**** New button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_UTILS_OPEN_SUMMARY,"Open LogFile",'O',PushButton,NULL,NULL,g);

	/**** New button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_UTILS_OPEN_IN,"Open LogFile as Input",'I',PushButton,NULL,NULL,g);

	CreateMenuButton(pulldownPane,NULL,MENU_UTILS_CHOOSE_RUN,"Run Input Line(s)",'L',PushButton,NULL,NULL,g);

	/**** New button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_UTILS_CLOSE_SUMMARY,"Close & Flush Summaries",'C',PushButton,NULL,NULL,g);

	AddSeparator(pulldownPane);

	/**** New button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_SAIL_BUILD_ASK_QUEST,"Rig Dimensions",'Q',PushButton,NULL,NULL,g);


	/**** debug 4 file button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_EDIT_DEFFILE,"Edit INIT file",'E',PushButton,NULL,NULL,g);

	/**** debug 4 file button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_LOAD_DEFFILE,"Reload INIT file",'R',PushButton,NULL,NULL,g);

	Insert_Custom_Buttons("mainutils",pulldownPane, g);


	/*** File Cascade button ***/
	label = XmStringCreateSimple((char*)"Utils");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'U');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,(char*)"Utils",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);



	/*****************************************************************************/
	/***************************   DEBUG MENU   ***********************************/
	/*****************************************************************************/

	#ifdef DEBUG_MENU

	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,(char*)"pulldownPane",al,l_ac);
	/**** New button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_WRITEALL,"Write .hmf",'W',PushButton,NULL,NULL,g);

	/**** Print All Entities button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_PRINTALLENTITIES,"Print All Entities",'P',PushButton,NULL,NULL,g);

	/*
	// g->loadStateSubmenu = XmCreatePulldownMenu(pulldownPane,"submenuState",al,l_ac);

	label = XmStringCreateSimple("Load State");
	l_ac=0;
	// XtSetArg(al[l_ac],XmNsubMenuId,g->loadStateSubmenu);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'L');l_ac++;
	cascade = XmCreateCascadeButtonGadget(pulldownPane,"LoadState",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);
	*/
	/**** Debug 2 button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_2,"Debug 2(heapCheck)",'D',PushButton,NULL,NULL,g);

	/**** debug 3 file button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_DEBUG_3,"Debug_3",'R',PushButton,NULL,NULL,g);

	/**** debug 4 file button *****/


	/**** DXF  button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_IMPORT_DXF,"Import Fixed DXF",'X',PushButton,NULL,NULL,g);

	/**** SetPressures from file  button *****/
	CreateMenuButton(pulldownPane,NULL,MENU_READ_PRESSURES,"Read Pressures",'P',PushButton,NULL,NULL,g);

	/**** Set Color file button *****/
	//CreateMenuButton(pulldownPane,NULL,MENU_SET_COLOR,"Set Color",'S',PushButton,NULL,NULL,g);

	/**** Turn on recoloring of panels by  mouse button *****/
	//CreateMenuButton(pulldownPane,NULL,MENU_RECOLOR,"ReColor",'R',CheckBox,NULL,NULL,g);
	Insert_Custom_Buttons("maindebug",pulldownPane, g);


	/*** File Cascade button ***/
	label = XmStringCreateSimple((char*)"Debug");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'D');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,(char*)"Debug",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);

	#endif
	/*****************************************************************************/
	/***************************   Prestress MENU   ***********************************/
	/*****************************************************************************/

	#ifdef PRE_STRESS

	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);

	/**** prestress button  *****/
	g_prestressWidget = CreateMenuButton(pulldownPane,NULL,MENU_USE_PRESTRESS,"Use Prestress",'U',CheckBox,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_RESET_ALL_EDGES,"Reset All Edges",'R',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_SHRINK_ALL_EDGES,"Shrink All Edges",'S',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_FORM_FIND,"FormFind",'S',PushButton,NULL,NULL,g);





	/*** File Cascade button ***/
	label = XmStringCreateSimple("PreStress");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'S');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,"Prestress",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);

	#endif
	/*****************************************************************************/
	/***************************   OPtimise MENU   ***********************************/
	/*****************************************************************************/

	#ifdef OPTIMISE

	l_ac=0;
	pulldownPane = XmCreatePulldownMenu(menuBar,"pulldownPane",al,l_ac);

	CreateMenuButton(pulldownPane,NULL,MENU_OPTIMISE_INIT,"Initialise Optimisation",'I',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_OPTIMISE_LOAD_HISTORY,"Load Optimisation History",'H',PushButton,NULL,NULL,g);
	CreateMenuButton(pulldownPane,NULL,MENU_OPTIMISE,"OPTIMISE",'O',PushButton,NULL,NULL,g);






	/*** File Cascade button ***/
	label = XmStringCreateSimple("Optimisation");
	l_ac=0;
	XtSetArg(al[l_ac],XmNsubMenuId,pulldownPane);l_ac++;
	XtSetArg(al[l_ac],XmNlabelString,label);l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,'O');l_ac++;
	cascade = XmCreateCascadeButtonGadget(menuBar,"Optimisation",al,l_ac);
	XtManageChild(cascade);
	XmStringFree(label);
	#endif

	XtManageChild(menuBar);
	return(menuBar);

} /* endo of CreateMenuBar()  */

//Widget CreateMenuButtonForLP(Widget parent,char *name,int bID,char *bLabelStr,int bMnemonic,int bType,
//char *bAcc,char *bAccTextStr,Graphic *g,char*lp);

Widget CreateMenuButtonForLP(Widget parent,
				const char *NAme,
				const int button_ID,
				const char *p_buttonLabelStr,
				int buttonMnemonic, 
				int buttonType,
				const char *buttonAccelerator,
				const char *p_buttonAcceleratorTextStr, 
				Graphic *gr,
				const char *lp)
{

Widget button;
XmString buttonLabel, buttonAcceleratorText;

char buttonLabelStr[256]; 

strcpy(buttonLabelStr,p_buttonLabelStr);

if(strlen(buttonLabelStr) > 0)
	buttonLabel = XmStringCreateSimple(buttonLabelStr);
else
	buttonLabel = NULL;

if( p_buttonAcceleratorTextStr && *p_buttonAcceleratorTextStr){
	char  buttonAcceleratorTextStr[256]; 
	strcpy( buttonAcceleratorTextStr,p_buttonAcceleratorTextStr);
	buttonAcceleratorText = XmStringCreateSimple(buttonAcceleratorTextStr);
}
else
	buttonAcceleratorText = NULL;

	int l_ac=0;
	Arg al[64];
	XtSetArg(al[l_ac],XmNlabelString,buttonLabel); l_ac++;
	XtSetArg(al[l_ac],XmNmnemonic,buttonMnemonic); l_ac++;
	XtSetArg(al[l_ac],XmNaccelerator,buttonAccelerator); l_ac++;
	if( buttonAcceleratorText ){
		XtSetArg(al[l_ac],XmNacceleratorText,buttonAcceleratorText); l_ac++;}

  		if(buttonType == CascadeButton) {
 			button = XmCreateCascadeButtonGadget(parent,buttonLabelStr,al,l_ac);
 		 }
  else if(buttonType == CheckBox || buttonType ==CheckBoxOn ||
          buttonType == RadioButton || buttonType == RadioButtonOn) {
	if( buttonType == RadioButton || buttonType == RadioButtonOn) {
		XtSetArg(al[l_ac],XmNindicatorType,XmONE_OF_MANY);l_ac++;
	}
        if(buttonType == CheckBoxOn || buttonType == RadioButtonOn) {
		XtSetArg(al[l_ac],XmNset,True);l_ac++;
	}
	button = XmCreateToggleButtonGadget(parent,buttonLabelStr,al,l_ac);
	XtAddCallback(button,XmNvalueChangedCallback,(XtCallbackProc) Menu_TextCB,mkIDLP(button_ID,gr,lp));
  }
  else { 
	button = XmCreatePushButtonGadget(parent,buttonLabelStr,al,l_ac);
	XtAddCallback(button,XmNactivateCallback,(XtCallbackProc)Menu_TextCB,mkIDLP(button_ID,gr,lp));
  }
XtManageChild(button);

assert(buttonLabel ); 		XmStringFree(buttonLabel);

	if(!buttonAcceleratorText ){
		//printf("NULL BAC in <%s> (LP)\n",buttonLabelStr);
	}
	else
		XmStringFree(buttonAcceleratorText);
return(button);
}

Widget CreateMenuButton(Widget parent,
				const char *NAme,
				const int buttonID,
				const char *buttonLabelStr,
				char buttonMnemonic,
				const int buttonType,
				const char *buttonAccelerator,
				const char *p_buttonAcceleratorTextStr,
	Graphic* gr)
{
	Widget button=NULL;
	XmString buttonLabel, buttonAcceleratorText;
	Arg al[64];
	int l_ac=0;
	if(strlen(buttonLabelStr) > 0)
		buttonLabel = XmStringCreateSimple((char*)buttonLabelStr);
	else
		buttonLabel = NULL;

if( p_buttonAcceleratorTextStr && *p_buttonAcceleratorTextStr){
	char buttonAcceleratorTextStr[256]; 
 	strcpy(buttonAcceleratorTextStr, p_buttonAcceleratorTextStr);
	buttonAcceleratorText = XmStringCreateSimple( buttonAcceleratorTextStr);
}
else
	buttonAcceleratorText = NULL;

l_ac=0;
XtSetArg(al[l_ac],XmNlabelString,buttonLabel); l_ac++;
XtSetArg(al[l_ac],XmNmnemonic,buttonMnemonic); l_ac++;
XtSetArg(al[l_ac],XmNaccelerator,buttonAccelerator); l_ac++;
if( buttonAcceleratorText) {
XtSetArg(al[l_ac],XmNacceleratorText,buttonAcceleratorText); l_ac++;}

  if(buttonType == CascadeButton) {
 	button = XmCreateCascadeButtonGadget(parent,(char*)buttonLabelStr,al,l_ac);
  }
  else if(buttonType == CheckBox || buttonType ==CheckBoxOn ||
          buttonType == RadioButton || buttonType == RadioButtonOn) {
	if( buttonType == RadioButton || buttonType == RadioButtonOn) {
		XtSetArg(al[l_ac],XmNindicatorType,XmONE_OF_MANY);l_ac++;
	}
        if(buttonType == CheckBoxOn || buttonType == RadioButtonOn) {
		XtSetArg(al[l_ac],XmNset,True);l_ac++;
	}
	button = XmCreateToggleButtonGadget(parent,(char*)buttonLabelStr,al,l_ac);
	XtAddCallback(button,XmNvalueChangedCallback,(XtCallbackProc)MenuCB,mkIDLP(buttonID,gr,buttonLabelStr));
  }
  else {  
	button = XmCreatePushButtonGadget(parent,(char*)buttonLabelStr,al,l_ac);
	XtAddCallback(button,XmNactivateCallback,(XtCallbackProc)MenuCB,mkIDLP(buttonID,gr,buttonLabelStr));
  }
XtManageChild(button);
	assert(buttonLabel ); 		XmStringFree(buttonLabel);
	if(! buttonAcceleratorText ){
		//printf("NULL BAC in <%s>\n",buttonLabelStr); 
	}
	else
		XmStringFree(buttonAcceleratorText);

return(button);
}

void AddSeparator(Widget parent) {
Widget separator;

separator = XmCreateSeparatorGadget(parent,NULL,NULL,0);
XtManageChild(separator);

}	


void *mkID(int id,void *g) {	/* utility */
MenuID *m;

m = (MenuID*)MALLOC(sizeof(MenuID));
if(m) {
  m->g = g;
  m->id = id;
  m->lp = NULL;
}
return((XtPointer)m);
}

void *mkIDLP(int id,void *g,const char *lp) {	/* utility */
MenuID *m;

m = (MenuID*)MALLOC(sizeof(MenuID));
if(m) {
  m->g = g;
  m->id = id;
  m->lp = STRDUP(lp);

}

return((XtPointer)m);
}

int Set_Kill_Sensitivity(int i){
Widget w,p;
int j;

	XtSetSensitive(g_AnalysisKillWidget,i);
	XtSetSensitive(g_AnalysisCalcWidget,1-i);
	//XtSetSensitive(g_trimButton,1-i);

//	if(g_trim_shell && g_trim_shell->trim_shell)
//		XtSetSensitive(g_trim_shell->trim_shell,1-i);

	for(j=g_hoops_count-1;  j>=0 ; j--)  {
		p = g_graph[j].topLevel;  
		if(!p) continue;
		w = XtNameToWidget(p, "*Hoist");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Drop");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*New");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Open");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*New Boat");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Open Boat");
		if(w)  XtSetSensitive(w,1-i);  
		//w = XtNameToWidget(p, "*Trim");
		//if(w)  XtSetSensitive(w,1-i);   
	}

	return 1;
}

int Set_USelect_Sensitivity(int i){
Widget w,p;
int j;

	for(j=g_hoops_count-1;  j>=0 ; j--)  {
		p = g_graph[j].topLevel;  
		if(!p) continue;
		w = XtNameToWidget(p, "*Hoist");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Drop");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*New");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Open");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*New Boat");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Open Boat");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Append Data");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*From Script");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Import script");
		if(w)  XtSetSensitive(w,1-i); 
 
		w = XtNameToWidget(p, "*Import DXF");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "Parameters");
		if(w)  XtSetSensitive(w,1-i); 
		XmUpdateDisplay(p);


	}

	return 1;
}

int Set_Demo_Sensitivity(int i){
Widget w,p;
int j;
	for(j=g_hoops_count-1;  j>=0 ; j--)  {
		p = g_graph[j].topLevel;  
		if(!p) continue;
 
		w = XtNameToWidget(p, "*New");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Save");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Save As");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Save Boat As");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Sail From Script");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Change Dir");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Save Bagged As");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*New Boat");
		if(w)  XtSetSensitive(w,1-i);   
		w = XtNameToWidget(p, "*Open Boat");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Append Data");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*From Script");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Import script");
		if(w)  XtSetSensitive(w,1-i); 
 
		w = XtNameToWidget(p, "*Import DXF");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*iges out");
		if(w)  XtSetSensitive(w,1-i); 
		w = XtNameToWidget(p, "*Export");
		if(w)  XtSetSensitive(w,1-i); 
		XmUpdateDisplay(p);


	}

	return 1;
}






