
#ifndef _WINDOWS

#ifndef _MENUCB_H_
#define _MENUCB_H_
//#include "pcwin.h"
#ifdef _X

XtCallbackProc Menu_TextCB(Widget w,caddr_t client_data,caddr_t call_data);
XtCallbackProc  MenuCB(Widget w,caddr_t client_data,caddr_t call_data);
EXTERN_C void ViewDeleteCB(Widget w, XtPointer client_data, XtPointer call_data);

#endif

EXTERN_C int UpdateMenu();
EXTERN_C int SetCameraList(void);

#endif //#ifndef _MENUCB_H_

#endif  //#ifndef _WINDOWS



