#ifndef CBOX_STUBS_16NOV04
#define CBOX_STUBS_16NOV04


EXTERN_C void pop_contour_box(long int data,SAIL *sail, Widget parent);

EXTERN_C void pop_up_contour_CB (Widget w,XtPointer client_data, XtPointer call_data);
EXTERN_C void c_cancel_CB (Widget w, XtPointer client_data, XmPushButtonCallbackStruct *call_data );
EXTERN_C void pop_down_contour_CB (Widget w, XtPointer client_data, XtPointer call_data );
EXTERN_C void default_toggle_changed_CB (Widget w, XtPointer client_data, XmToggleButtonCallbackStruct *call_data );
EXTERN_C void c_ok_CB (Widget w, XtPointer client_data, XmPushButtonCallbackStruct *call_data );


#endif //#ifndef CBOX_STUBS_16NOV04
