//CreateMsgArea.h  header file for CreateMsgArea.cpp
//Thomas 10 06 04

#ifndef RX_CREATEMASGAREA_H
#define RX_CREATEMASGAREA_H
EXTERN_C int CreateMsgArea(Graphic * const  g);
//EXTERN_C int SetMsgText(const char *text, QObject *const client=0);  /* deals with LHS only */
//EXTERN_C int SetMsgText2(const char *text,const int right ,class QObject*const client=0);  /* deals with both only */

#endif //#ifndef RX_CREATEMASGAREA_H
