

#ifndef _ch_rowt_hdr_
#define _ch_rowt_hdr_
#ifdef _X
#include <X11/X.h>
#endif


typedef struct ch_data_s {
#ifdef _X
    Widget		dlg_shell;
#endif
    int			chosen;
    char		**list;   /* array of pointers to char */
    int			N;         /* no of things in list */
    std::string         m_label;      /* text string (eg. filename ) */
   class RXDataBaseLogI   *sum;
    int                 run_to_line;  /* line to continue to */
} ch_data_t, *ch_data_p;


#endif


