#ifndef _CP2MAINSTUB_H_
#define _CP2MAINSTUB_H_


extern void cp_cancel_CB(Widget w, caddr_t cl_d,  caddr_t  call_data); // why not XmAnyCallbackStruct *
extern void cp_apply_CB (Widget w, caddr_t cl_d,  caddr_t  call_data);
extern void cp_close_CB (Widget w, caddr_t cl_d,  caddr_t  call_data);

extern void inc_value_changed (Widget w, caddr_t client_data, XmToggleButtonCallbackStruct * call_data );
extern void disp_value_changed (Widget w, caddr_t client_data, XmToggleButtonCallbackStruct *call_data );

extern void set_contours_cb (Widget w, caddr_t client_data, XmAnyCallbackStruct *call_data );

extern void asgraph_value_changed (Widget w, caddr_t client_data, XmToggleButtonCallbackStruct *call_data );

#ifdef linux
// version for X11R6 where this is only called on the newly selected item. So the old Chosen flag never gets cleared
// so lets clear all the chosen flags below this widget.  We don't care about any above.
extern void cp_prop_CB (Widget w, caddr_t client_data, XmAnyCallbackStruct *call_data );
#endif//#ifdef linux


#endif  //#ifdef _CP2MAINSTUB_H_

