/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

// this nasy bit o code is used in reportSort_2


#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntity.h"
#include <ctype.h>
#include "stringutils.h"
#include "files.h"
#include "global_declarations.h"
#ifndef NEVER
#include "tree.h"

static FILE *ftree=NULL;

static Tnode *Type_Root=NULL;	/* just the entity types */

/* addtree */

/* routines to generate trees -
   used for deleted entitities at the moment
   */

struct tnode *talloc()
{
  return((struct tnode *) MALLOC(sizeof(struct tnode)));
}

void t_RXFREE(struct tnode *p) {

	assert(p) ;
//	memset(p,'/0',sizeof(struct tnode));
// in linux just 0
	memset(p,0,sizeof(struct tnode));

	RXFREE(p);
}

struct tnode *addtree( struct tnode *p,char *w,void *data)
{
  int cond;
  char errStr[128];
  
  if(!ftree) {
 //   char buf[120];
 //   sprintf(buf,"%s/treedebug.out",traceDir);
 //     ftree = FOPEN(buf,"w");
  }
  
 
  if(p==NULL) {
 
    p=talloc();
	assert(p);
	assert(w);
    p->word =  STRDUP(w);
    p->data = data;
    p->count = 1;
    p->left =p->right = NULL;
if(g_ArgDebug) {
    sprintf(errStr,"adding node '%s' to tree",w);
    g_World->OutputToClient (errStr,0);
}
    if(ftree)	fprintf(ftree,"ADD : '%s' \n",p->word); 
  }
  else if((cond = strcmp(w,p->word)) == 0) {
    /* repeated word - should not happen here !! */
    if(p->count) {
        sprintf(errStr,"(addtree)While trying to add\n'%s'\n found \n'%s' count %d",w,p->word,p->count);
     	g_World->OutputToClient (errStr,2); 
    }
    p->count++;
  }
  else if(cond < 0) /* less than so put in left subtree */
    p->left = addtree(p->left,w,data);
  else
    p->right = addtree(p->right,w,data);
  return(p);
}


struct tnode *deltree( struct tnode *p,char *w,void *data)
{
  int cond;
  char errStr[128];
  if(!p)  {
		sprintf(errStr,"Node with key '%s' not found !!",w);
		g_World->OutputToClient (errStr,2);
		if(g_ArgDebug && ftree) fprintf(ftree,"DELETE 0: '%s'\n",w);
		return(NULL);     
  }
 
	if((cond = strcmp(w,p->word)) == 0) {  /* found the node to delete */
    /* either it is a leaf or not */
if(g_ArgDebug) {
  sprintf(errStr,"deleteing '%s' from tree",w);
  g_World->OutputToClient (errStr,0);
}
    if(p->left) {								/* a left subtree */
			struct tnode *t = p->left;			// follow down rightmost side of left subtree
			struct tnode **th = &(p->left);		//* until reach a leaf.
			struct tnode *lt=NULL;				//* Then replace p with the leaf 
			//struct tnode *told = p->left;
			struct tnode **thold = &(p->left);
													/*printf("Has left subtree\n",p->word); */
			if(!(*th)->right) {						/* left child has no right subtree */
				RXFREE(p->word);						/* just move up to parents place */ 
				(*th)->right = p->right;	
				t_RXFREE(p);
				/* if(ftree) fprintf(ftree,"DELETE 1: '%s'\n",w); */
				return(t);
		  }
		  while(1) {
				if((*th)->right)  { /* t has right subtree */
				  thold = th;
				  th = &((*th)->right);
				}	
				else
				  break;  /* found NULL right subtree at t*/
		  }
      
		RXFREE(p->word);  
		(*th)->right = p->right;			 /* right of (*t) set to right sub of p */
		lt = (*th); 
		while(lt->left) 
			lt = lt->left;					/* left most leaf of node (*t) */
		lt->left = p->left;
		lt = *th;
      (*thold)->right = NULL;				/* parent ->right of (*t) set to NULL */
	   t_RXFREE(p);		/* peter moved this to after the previous */ /*	if(ftree) fprintf(ftree,"DELETE 2: '%s'\n",w); */
      return(lt);
    } 
    else {									/* left is NULL */
      struct tnode *rt = p->right;
      RXFREE(p->word);
      t_RXFREE(p);						/*	if(ftree) fprintf(ftree,"DELETE 3: '%s'\n",w); */
      return(rt);
    }
  }
  else if(cond < 0) /* less than so put in left subtree */
    p->left = deltree(p->left,w,data);
  else
    p->right = deltree(p->right,w,data);
  return(p);
}

struct tnode *findnode(struct tnode *tp,char *w,size_t len)
{
  long int cond;
  
  if(tp==NULL) {
    return(NULL);  /* p should be root of the tree first time through */
  }
  
  else if((cond = strncmp(w,tp->word,len)) == 0) {
    return(tp);				/* found key exactly */
  }
  else if(cond < 0) /* less than so put in left subtree */
    tp = findnode(tp->left,w,len);
  else
    tp = findnode(tp->right,w,len);
  
  return(tp);
}


int print_nothing(FILE *fp,struct tnode *p) 
{
  RXEntity_p e;
  
  e=( RXEntity_p ) p->data;
  if(e)
    fprintf(fp,"%4d %s (type = '%s',name = '%s')\n",p->count,p->word,e->type(),e->name());
  else
    fprintf(fp,"%4d %s (NULL DATA)\n",p->count,p->word);
  return(0);
}

void treeprint(struct tnode *p,FILE *fp,TPrintFuncPtr printmyself)
{
  static int level=0;
  level++;

  if(p) {
/* 	if (AfxIsValidAddress( p, sizeof(struct tnode),  TRUE ); */
    treeprint(p->left,fp,printmyself);
    printmyself(fp,p);
    treeprint(p->right,fp,printmyself);
  }
  level--;
}


int getLastNumber(char *type)
{
  struct tnode *tp;
  
  if(!(tp =  findnode(Type_Root,type,strlen(type)))) {
    /* none of that type found ? */
    /* then add */
    Type_Root = addtree(Type_Root,type,NULL);
    tp = findnode(Type_Root,type,strlen(type));
  }
  
  tp->count++;
  return(tp->count-2); /* to start at zero !! */
  
}


void found(struct tnode *t)
{
  if(!t)
    printf("Key NOT found\n");
  printf("Found key '%s'\n",t->word);
  printf("count = %d\n",t->count);
  
}


void read_file(struct tnode *root,FILE *fpinp,struct tnode * func(struct tnode *r,char *ch, void*  ))
{
  char rest[250];
  //int i=0;
  char *ch;
  
  
  
  do {
    ch=fgets(rest,249,fpinp);
    if(!ch) break;
    ch = strtok(rest," ,\n");
    printf("sortname is %s\n",ch);
    root = func(root,ch,NULL);
    
  }while(1);
} /* end of read file */
/*
void testmain(struct tnode * getdata(struct tnode *t, int *addtree()))
{
  FILE *fp;
  char search_name[15],inpfilname[64];
  char ch;
  struct tnode *t,*root=NULL;
  
  root = getdata(root,addtree);
  
  while(1) { 
    printf("\n\rALL Delete (A), Delete (D), Insert (I), Search (S), Print (P), Quit (Q),  ");
    do
      {
	ch=getchar();
	ch=toupper(ch);
      }while(ch!='A' && ch!='D' && ch!='I' && ch!='S' && ch!='P' && ch!='Q');
    if(ch=='Q') break;
    
    printf("enter a name (4 letters)  ");
    scanf("%s",search_name);
    switch (ch)
      {
      case 'D' :
	root = deltree(root,search_name,NULL);
	break;
      case 'I' :
	root = addtree(root,search_name,NULL);
	break;
      case 'S' :
	t = findnode(root,search_name,strlen(search_name));
	found(t);
	break;
      case 'P' : {
	char buf[120];
	sprintf(buf,"%s/tree.out",traceDir);
	fp = FOPEN(buf,"w");
	treeprint(root,fp,print_nothing);
	FCLOSE(fp);
	break;
      }
      case 'A' :
	{
	  root = getdata(root,deltree);
	}
	break;
      }
  }
}
*/

char *MakeKey(char *type,char *name, char *out)
{
  sprintf(out,"%s$%s",name,type);
  strtolower(out);
  return(out);
}

int Rename_Entity_Index(SAIL *sail, char *oldname,char *newname,char *type)
{
  assert(0); return 0;
/*
  struct tnode *tn;
  RXEntity_p e;
  char buf[256];
  
  MakeKey(type,oldname,buf);
  tn = findnode(sail->list_root,buf,strlen(buf));
  if(!tn)
    return(-1);
  e = ( RXEntity_p  )tn->data;
  
  sail->list_root = deltree(sail->list_root,buf,NULL);
  MakeKey(type,newname,buf);
  sail->list_root = addtree(sail->list_root,buf,(void * ) e);
  
  return(1);
*/
}
#endif







