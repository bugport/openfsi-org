/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 Dec 2009, rewritten in terms of RXPressureInterpolation objects:
 July 97 included pansail.h for decls  of goffset, etc
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXSail.h"
#include "RXAttributes.h"
#include "RXEntityDefault.h"
#include "RXSitePt.h"
#include "RX_FETri3.h"
#include "RXPressureInterpolation.h"
#include "pctable.h"
#include "script.h"
#include "aero.h"
#include "aeromik.h"
#include "interpln.h"
#include "words.h"
#include "controlmenu.h"
#ifdef USE_PANSAIL
#include "pansail.h"
#endif
#include "pressure.h"
#include "rxvectorfield.h"
#include "global_declarations.h"

#define mydebug (0)

int RelaxWorld::SetPressuresOnActive(control_data_t *data)
{
    size_t i;
    RXSTRING ff;size_t k; vector<RXSTRING> wds;
    map<int,double> plist;
    string a;  const char *b;
    if(data->press == SCRIPT_PRESSURE) {
        //run relax script  '$RX_CFD_MACRO'
        // $RX_CFD_MACRO should
        //1) execute any pre-CFD tasks such as generating geometry files (igesout, write_stl, etc)
        //2) execute 'system: myCFDBashScript'
        //	the bash script should run the CFD and create uvDp files. Each sail may have multiple uvDp files
        //3) execute any post-CFD tasks such as importing non-pressure results, etc.
        // $RX_CFD_MACRO  does NOT have to do anything to import the pressures
        // except to place them in uvDp files.
        //You must have already defined 'windfiles_[sail]=file1;[file2][;file3]...  for each model. NOTE THE USE OF ';'

        b = getenv("RX_CFD_MACRO");
        if(!b) { cout<<" no `$RX_CFD_MACRO` is defined "<<endl; a.clear();}
        else
            a = string(b);
        if(a.empty())
            cout<<"`RX_CFD_MACRO` is defined  but it is empty"<<endl;
        else{
            Read_Script(a.c_str(),0, 0,false);
        }
    }


    vector<RXSail*> lsl = this->ListModels(RXSAIL_ALLHOISTED); // boat too

    for(i=0;i<lsl.size();i++)  {
        SAIL *theSail = lsl[i];
        RXAttributes rxa(theSail->m_attributes.Array());
        switch (data->press) {
#ifdef USE_PANSAIL
        case PANSAIL_PRESSURE:
            if(theSail->GetFlag(FL_HAS_AERODATA)) { // could mean there is a pressure entity
                if(theSail->SetUpPansailPressures()) {
                    theSail->SetOnePressures( );
                    theSail ->SetFlag(FL_NEEDS_POSTP );
                }
            }
            break;
#endif
        case WERNER_PRESSURE:
            if(theSail->GetFlag(FL_HAS_AERODATA)) { // could mean there is a pressure entity
                theSail->SetOnePressures( );
                theSail ->SetFlag(FL_NEEDS_POSTP );
            }
            break;


        case SCRIPT_PRESSURE:  // like FILE_PRESSURE except the filenames come from environment $windfiles_<sailname>
            a=string("windfiles_")+theSail->GetType();
            b = getenv(a.c_str()); if(!b) { cout<<" no `"<<a.c_str()<<"` is defined "<<endl; break;}
            ff = TOSTRING(b);
            if(ff.empty())
                break;
            wds =  rxparsestring(ff, L",");

            if(wds.size() < 1){
                wds.push_back(ff);
            }
            if(wds.size() > 1 &&fabs(g_Relaxation_Factor) >1e-8 )	{
                cout << "FSI under-relaxation needs special care when there is more than one pressure file"<<endl;
            }
            if(theSail->SetUpFilePressures(wds[0],data->extra_flags_str)) { /* the last is the args */
                theSail->SetOnePressures(&plist );
                theSail->SetFlag(FL_NEEDS_POSTP);
            }
            for(k=1;k<wds.size();k++){
                if(theSail->SetUpFilePressures(wds[k],data->extra_flags_str)) {
                    theSail->AddOnePressures(&plist );
                    theSail->SetFlag(FL_NEEDS_POSTP);
                }
            }
            theSail->ApplyPressureList(plist);
            break;

            break;
        case FILE_PRESSURE:
            wcout<<L" Setting File pressures on "<<theSail->GetName();

            if(rxa.Extract_Word(L"$windfiles",ff))
            {
                wcout<<L": using wind files from sail atts "<<ff.c_str()<<endl;
            }
            else {
                ff=TOSTRING(data->windfile );
                if(!ff.empty()) wcout<<L": using wind files from control menu "<<ff.c_str()<<endl;
            }
            if(ff.empty())
                break;
            wds =  rxparsestring(ff, L";");

            if(wds.size() < 1){
                wcout<<L" you need a `;` in `"<<ff.c_str()<<L"`"<<endl;
                break;
            }
            if(wds.size() > 1 &&fabs(g_Relaxation_Factor) >1e-8 )	{
                cout << "FSI under-relaxation needs special care when there is more than one pressure file"<<endl;
            }
            if(theSail->SetUpFilePressures(wds[0],data->extra_flags_str)) { /* the last is the args */
                theSail->SetOnePressures(&plist );
                theSail->SetFlag(FL_NEEDS_POSTP);
            }
            for(k=1;k<wds.size();k++){
                if(theSail->SetUpFilePressures(wds[k],data->extra_flags_str)) {
                    theSail->AddOnePressures(&plist );
                    theSail->SetFlag(FL_NEEDS_POSTP);
                }
            }
            theSail->ApplyPressureList(plist);
            break;

        case CUSTOM_PRESSURE: // custom means aeromik
            wcout<<L" Setting Custom pressures on "<<theSail->GetName();
            // find a filename
            if(rxa.Extract_Word(L"$windfiles",ff))
            {
                wcout<<L": using wind files from sail atts "<<ff.c_str()<<endl;
            }
            else {
                ff=TOSTRING(data->windfile );
                if(!ff.empty()) wcout<<L": using wind files from control menu "<<ff.c_str()<<endl;
            }
            if(ff.empty())
                break;
            // break it into words by ';'
            wds =  rxparsestring(ff, L";");

            if(wds.size() < 1){
                wcout<<L" you need a `;` in `"<<ff.c_str()<<L"`"<<endl;
                break;
            }
            if(wds.size() > 1 &&fabs(g_Relaxation_Factor) >1e-8 )	{
                cout << "FSI under-relaxation needs special care when there is more than one pressure file"<<endl;
            }
            if(theSail->SetUpCustomPressures(wds[0],data->extra_flags_str)) { /* the last is the args */
                theSail->SetOnePressures(&plist );
                theSail->SetFlag(FL_NEEDS_POSTP);
            }
            for(k=1;k<wds.size();k++){
                if(theSail->SetUpCustomPressures(wds[k],data->extra_flags_str)) {
                    theSail->AddOnePressures(&plist );
                    theSail->SetFlag(FL_NEEDS_POSTP);
                }
            }
            theSail->ApplyPressureList(plist);
            break;
        case RELAX_PRESSURE:
            break;
        case CONSTANT_PRESSURE:
            theSail->SetOneConstantPressure(theSail->m_Pressure_Factor*data->value);
            break;
        case FIELD_PRESSURE:
            theSail->SetOneFieldPressure( );
        case INTERPOLATE_PRESSURE:
            theSail->SetOneInterpolatedPressure( );
            break;
        }
        theSail->SetFlag(FL_HAS_PRESSURES);
    } // for I
    Set_Previous_MALLOC_DATA();
    return(0);
}
int RXSail::ApplyPressureList(map< int,double> list){
    double Pd;
    int rc=0;
    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!(te && (te->GetN() ) )  ) continue;
        Pd=list[te->GetN()]; rc++;

        if(	GetFlag(FL_HAS_PRESSURES))
            te->SetPressure(Pd *(1.0 - g_Relaxation_Factor) + te->GetPressure() * g_Relaxation_Factor);
        else
            te->SetPressure(Pd );
    } // tri loop
    return rc;
}
// like SetOne but it adds onto the existing pressures.
// is arg plist is present, it sums them into plist
// else it places them on the elements;
int RXSail::AddOnePressures(std::map<int,double>*plist){ 
    int rc=0;
    /*
find each Fortrantriangle centroid position
find the pressure triangle containing it
call regenerate one point on it
add to the fortran value.
*/
    MeshSite xyz;
    double Pd;
    wcout <<" ADDING one pressure set to plist with pressure factor  =" <<this->m_Pressure_Factor <<endl;

    if(this->m_FTri3s.size() <2)
        return 0;
    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!(te && (te->GetN() ) )  ) continue;
        te->Centroid(&xyz);
        Pd=this->m_pressInt->ValueAt(ON_2dPoint(xyz.m_u,xyz.m_v));
        Pd=Pd*this->m_Pressure_Factor;
        if (isnan(Pd) ) {rc++;  Pd=0.0  ; }
        (*plist)[te->GetN()]=Pd + (*plist)[te->GetN()];
    } // tri loop
    if(rc) cout << "(AddOnePressures) NAN pressure  count= "<<rc<<endl;
    return 1;
}
// if plist is non-null, set values in it. Olterwise set them on the elements
int RXSail::SetOnePressures(map<int,double>*plist) // the pressint is expected to have values on the unit square.
{
    /*
    find each Fortrantriangle centroid position
    find the pressure triangle containing it
    call regenerate one point on it
    set the fortran value.
    */
    int nancount=0, count2=0;
    MeshSite xyz;
    double Pd;
    ON_2dPoint puv;

    if(this->m_FTri3s.size() <2)
        return 0;
    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!te) continue; if(!te->GetN()) continue;
        te->Centroid(&xyz);
        puv = ON_2dPoint(xyz.m_u,xyz.m_v); // i half-think these are on unit square
        Pd=this->m_pressInt->ValueAt(puv);

        Pd=Pd*this->m_Pressure_Factor;

        if (isnan(Pd) ) { nancount++; Pd=0.0  ; }
        if(plist){
            (*plist)[te->GetN()]=Pd;
        }
        else {
            if(	GetFlag(FL_HAS_PRESSURES))
                te->SetPressure(Pd *(1.0 - g_Relaxation_Factor) +   te->GetPressure() * g_Relaxation_Factor ) ;
            else
                te->SetPressure( Pd) ;
        }
        count2++;
    } // tri loop
    if(nancount) cout << "(SetOnePressures) NAN pressure  count= "<<nancount<<endl;
    //if(mydebug) cout<<"Done Setting Pressures on "<<this->GetType() <<" : ntris=" << count2<<endl;
#ifdef PRESSURE_DEBUG
    FCLOSE(ff);
#endif 	
    return(1);
}
#ifdef NEVER
int   Fudge_Separation(float *dpin,float *cpuin,float *cplin,int m,int n,const char*s){
    int i,j;
    double lim;
    float *dp=dpin, *cpl=cplin, *cpu=cpuin;
    FILE *fp =NULL;
    if(mydebug) fp = RXFOPEN("cpout.txt","a");
    if(mydebug) fprintf(fp," %s\n M N %d %d\n", s,m,n);
    lim = g_PansailIn.Flow_Separation/10.0;
    for(i=0;i<n;i++) {
        if(mydebug) fprintf(fp," Row  %d\n", i);
        for(j=0;j<m;j++){
            if(mydebug) fprintf(fp, " %f  %f  %f\t", *cpu,*cpl,*dp);
            if(j >= m/2 && *cpu > -lim) {
                printf(" '%s' hi cpu =%f (%d %d) dp %f ",s,*cpu , i,j,*dp);
                *cpu =  - (float) lim;
                *dp =  *cpl - *cpu;
                printf(" -> %f\n" , *dp);
            }
            if(j >= m/2 && *cpl <  lim) {
                printf(" '%s' cpl low %f) (%d %d) dp %f ", s,*cpl, i,j,*dp);
                *cpl = (float) lim;
                *dp =  *cpl - *cpu;
                printf(" -> %f\n" , *dp);
            }
            dp++ ; cpu++; cpl++;
        }
        if(mydebug) fprintf(fp,"\n");
    }
    if(mydebug) fprintf(fp,"Does this match  pansail.cp?\n");

    if(mydebug) FCLOSE(fp);
    return 1;
}

#endif
#ifdef USE_PANSAIL
int RXPressureInterpolationP::FromInternalArrays( SAIL *sail) DEAD
{
    vector<int> edgeNos;// not used
    float *uc,*vc;
    float *dp;//,*cpl,*cpu ;
    float *UC,*VC,*DP;
    int mod,m,n,count,i,q,r,col;

    double PP;
    float refwind,u,v,w;
    int extra=0;
    RXEntity_p presEnt;

    if(mydebug)
        printf("Setting Pressures on %s with multiplier %f \n",sail->GetType().c_str(),sail->m_Pressure_Factor);

    mod = sail->GetLoadingIndex()-1;
    m = g_M[mod]-1;
    n = g_N[mod]-1;
    // endssd = m*n;


    UC = uc = (float*)CALLOC(m+2,sizeof(float));
    VC = vc = (float*)CALLOC(n+2,sizeof(float));
    DP = dp =  (float*)CALLOC((g_M[mod] + 2)*(g_N[mod] + 2),sizeof(float));
    count=0;

    memcpy((uc+1),&(g_Uc[g_moffset[mod] -mod]),sizeof(float)*m); // Peter added -mod  nov 00
    memcpy((vc+1),&(g_Vc[g_noffset[mod]  -mod]),sizeof(float)*n);

    uc[0] =0.0;
    uc[m+1] =1.0;
    vc[0] = 0.0;
    vc[n+1] =1.0;


    if(mydebug) {
        printf("  sd->Pansail_model = %d\n", sail->GetLoadingIndex());
        cout<< " Uc first & last are ours\n"<<endl;
        for(i=0;i<m+2;i++) printf(" %d %f \n", i,uc[i]);
        cout<< " vc first & last are ours\n"<<endl;
        for(i=0;i<n+2;i++) printf(" %d %f \n", i,vc[i]);
    }

    presEnt = sail->GetFirst("pressure");
    if( presEnt) {
        Pressure *l_p = (Pressure *) presEnt->dataptr;
        sail->OutputToClient(" pressure entity exists ",1);
        // g_PansailIn.Flow_Separation=0;
        dp  = &(g_Dcp[g_coffset[mod]]);

        for(r=0;r<l_p->m_table->nr; r++) { // N is nr, M is Nc
            for(col=0;col<l_p->m_table->nc; col++) {
                *dp = (float) atof(l_p->m_table->data[r][col]);
                printf( "%d %d \t%f ", r,col,*dp);
                dp++;
            }
            cout<< " \n"<<endl;
        }
    }

    dp  = &(g_Dcp[g_coffset[mod]]);
    //cpu =  &(g_CpU[g_coffset[mod]]);
    //cpl  = &(g_CpL[g_coffset[mod]]);
    //if(g_PansailIn.Flow_Separation)
    // 	Fudge_Separation(dp,cpu,cpl,m,n,sail->GetType().c_str());
    //original
    for(i=1;i<=m;i++) {  // bottom row
        DP[i] = 2*(dp[i-1]) - dp[i-1+m];
        extra++;
    }
    for(i=1;i<=m;i++) {  // top row
        DP[i+(n+1)*(m+2)] = 2*(dp[(i-1)+(n-1)*m]) - dp[(i-1)+(n-2)*m];
        extra++;
    }
    for(i=1;i<=n;i++) {  // left side
        DP[i*(m+2)] = 2*(dp[(i-1)*m]) - dp[(i-1)*m + 1];
        extra++;
    }
    for(i=1;i<=n;i++) {  // right side
        DP[i*(m+2) + (m+1)] = 2*(dp[(i-1)*m + (m-1)]) - dp[(i-1)*m +(m-1) - 1];
        extra++;
    }
    //********************************
    //Peters Nov 2000.  dont extrapolate, just copy
    for(i=1;i<=m;i++) {  // bottom row
        DP[i] = (dp[i-1]) ;
        extra++;
    }
    for(i=1;i<=m;i++) {  // top row /
        DP[i+(n+1)*(m+2)] = (dp[(i-1)+(n-1)*m]);
        extra++;
    }
    for(i=1;i<=n;i++) {  //left side
        DP[i*(m+2)] =  (dp[(i-1)*m]) ;
        extra++;
    }
    for(i=1;i<=n;i++) {  // right side /
        DP[i*(m+2) + (m+1)] = (dp[(i-1)*m + (m-1)]);
        extra++;
    }



    //*********************************

    for(i=1;i<=n;i++) {
        for(q=1;q<=m;q++) {
            DP[q+i*(m+2)] = *dp;
            dp++;
        }
    }
    dp = DP;
    //  //adams
    // bottom left corner
    dp[0] = (dp[1] + dp[m+2])/2 ;
    // bottom right
    dp[m+1] = (dp[m] + dp[m+1+(m+2)])/2 ;
    // top left
    dp[(m+2)*(n+1)] = (dp[(m+2)*(n+1) + 1] + dp[(m+2)*(n)])/2 ;
    // top right
    dp[(m+2)*(n+1)+(m+1)] = (dp[(m+2)*(n+1)+(m)] + dp[(m+2)*(n)+(m+1)])/2 ;
    extra += 4;
    
    if(mydebug) printf("adding %d extra points\n",extra);

    count = 0;

    uc = UC;
    vc = VC;
    dp = DP;

    refwind = g_PansailIn.Reference_Length; WRONG
    app wind_(&refwind,&u,&v,&w);
    refwind = (u*u + v*v + w*w);

    PP = *dp;
    PP = -PP*0.6125*refwind*sail->m_Pressure_Factor;

    Points().push_back( RXSitePt( *uc,*vc,PP,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));

    for(i=0;i<n+2;i++) {
        for(q=0;q<m+2;q++) {
            PP = *dp;
            PP = -PP*0.6125*refwind*sail->m_Pressure_Factor;
            Points().push_back( RXSitePt( (uc[q]),(vc[i]),PP,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
            dp++;
        }
    }

    RXFREE(DP);  RXFREE(UC);  RXFREE(VC);

    if(!msh.Triangulate(Points(),edgeNos,"pcenzQ" ))
        return 0;

    return Points().size();
}
#endif
// the cps are expected to be on the unit square.
// but we will interrogate the interpolation object via ValueAt
int RXPressureInterpolationP::FromExternalArrays( SAIL *sail,std::vector<RXSitePt> cps, std::vector<double> dp)
{
    int  count=0 ;
    vector<int> edgeNos;// not used
    double uu,vv, PP;
    float refwind, refwindsq,u,v,w;
    std::vector<RXSitePt> ::iterator itx;
    std::vector<double>::iterator itp;

    if(mydebug)
        printf("Setting Pressures on %s with multiplier %f \n",sail->GetType().c_str(),sail->m_Pressure_Factor);

 //   int mod = sail->GetLoadingIndex()-1;

    // not sure this first p is needed. It's insulation against the interpolator skipping the first pt

    Points().push_back( RXSitePt( -1.1 ,0.5 ,0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));

    // 8 points all around because we will be interpolating outside the extents of the control points
    Points().push_back( RXSitePt( -1.0, 0.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt( -1.0, 1.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt(  0.0, 2.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt(  1.0, 2.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt(  2.0, 1.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt(  2.0, 0.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt(  1.0,-1.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    Points().push_back( RXSitePt(  0.0,-1.0, 0.0,ON_UNSET_VALUE,ON_UNSET_VALUE,count++));

    refwind = g_World->m_wind.Get( RXWindGeometry::ReferenceLength);
    appwind_(&refwind,&u,&v,&w);
    refwindsq = (u*u + v*v + w*w);
    for(itx=cps.begin(), itp=dp.begin(); itx != cps.end()&&itp!=dp.end();++itx,++itp)
    {
    //    RXSitePt &ppt = *itx;
        PP = *itp;
        PP = -PP*0.6125*refwindsq*sail->m_Pressure_Factor;
        vv = (*itx).m_u;
        uu = (*itx).m_v;
        Points().push_back( RXSitePt( uu,vv,PP,  ON_UNSET_VALUE,ON_UNSET_VALUE,count++));
    }
// points are in unit-square space
    if(!msh.Triangulate(Points(),edgeNos,"pcenzQ" ))
        return 0;

    return Points().size();
}


#ifdef USE_PANSAIL
int RXSail::SetUpPansailPressures()
{
    if(this->m_pressInt) delete this->m_pressInt;
    this->m_pressInt = new class RXPressureInterpolationP(this);
    this->m_pressInt->FromInternalArrays(this);   //Read(wstring(fname));

    if(!this->m_pressInt->HasMesh () ) { cout << "you must first make a mesh"<<endl; return 0 ;}

    this->m_pressInt->LocateInitialize();

    ON_2dPoint p2 (.33,.33);
    double uuu= this->m_pressInt->ValueAt (p2);
    return 1;
}
#endif
int RXSail::SetUpWernerPressures( std::vector<RXSitePt> cps, std::vector<double> dp )  // the cps are on the unit square.
{
    int rc=0;
    if(this->m_pressInt) delete this->m_pressInt;
    this->m_pressInt = new class RXPressureInterpolationP(this);
    rc=this->m_pressInt->FromExternalArrays(this,cps, dp) ;

    if(!this->m_pressInt->HasMesh () )
    {
        cout << "you must first  make a mesh"<<endl;
        return 0 ;
    }

    this->m_pressInt->LocateInitialize();

    ON_2dPoint p2 (.33,.33);
    double uuu= this->m_pressInt->ValueAt (p2);
    rc=1;
    this->SetFlag(FL_HAS_AERODATA);
    return rc;

}

// method is p_data->press;control_data_t *p_data,
//file is p_data->windfile

int RXSail::SetUpCustomPressures(const RXSTRING p_file, char*p_args)
{
    SAIL *sail =this;
    RXEntity_p aEnt=NULL; /* *panEnt; */
    static struct PC_AEROMIK *mik;
    int  iret;

    int cnt=0;
    int guess_at_V =0;

    aEnt = g_boat->GetFirst("aeromik");

    if(aEnt) { cout<<" boat has an aeromik"<<endl;
        mik = (PC_AEROMIK*)aEnt->dataptr;
        if(sail->IsBoat()) {
            cout<< "calling Make_AeroMik Geometry\n"<<endl;
            Make_Aeromik_Geometry(aEnt);       /* generates .mik file then calls program */
            cout<< "GUESSING V !!\n"<<endl;
            guess_at_V = 1;
            return(0);
        }
    }

    aEnt = g_boat->GetFirst("aero");

    if(!aEnt) {
        rxerror("no AERO entity found in boat ",2);
        return(0);
    }
    mik = (PC_AEROMIK*)aEnt->dataptr;
    cout <<" found the aero entity in the boat "<<endl;
    if(sail->IsBoat()) {
        cout<< " Make_Aero Geometry\n"<<endl;
        Make_Aero_Geometry_And_Run(aEnt, p_args);       /* generates .mik file then calls program */
        guess_at_V =0;
        cout<< " (IntPress) leaving Make_Aero_Geometry_And_Run\n"<<endl;
        return(0);
    }


    cout << "Trying to read pressures on "<< sail->GetType()<<endl;

    cnt=0;
    while(mik->sailnames[cnt]) {
        if( mik->sailnames[cnt]==sail->GetType())
            break;
        cnt++;
    }
    if(!mik->sailnames[cnt]) {
        string buf = "no pressures description found for sail  "  + sail->GetType();
        rxerror(buf.c_str(),2);
        return(0);
    }
    printf("Reading Pressures from '%s' onto '%s'\n",mik->files[cnt],sail->GetType().c_str());

    iret = sail->ReadOneFilePressure( TOSTRING(mik->files[cnt]), guess_at_V);
    return iret;
}


int RXSail::SetUpFilePressures(const RXSTRING file, char*p_args)
{
    int  iret;
    iret = this->ReadOneFilePressure(file , 0);
    return iret;
}




int RXSail::ReadOneFilePressure(const RXSTRING fname, int guess_at_V ){

    if(this->m_pressInt) delete this->m_pressInt;
    this->m_pressInt = new class RXPressureInterpolationP(this);
    this->m_pressInt->Read(fname);

    if(!this->m_pressInt->HasMesh () ) { cout << "you must first make a mesh"<<endl; return 0 ;}

    this->m_pressInt->LocateInitialize();


    return 1;
}

int RXSail::SetOneConstantPressure(const double val)
{

    /* apply the presure in val[0] to all the triangles.
   */

    int count2 =0;
    double PP  = val;
    if(this->m_FTri3s.size() <2)
        return 0;
    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!(te && te->GetN() )) continue;
        count2+=te->SetPressure(PP);
    }

    cout <<"Done Setting CONSTANT Pressures "<< PP <<" on "<<GetType() << " - Ntriangles ="<< count2<<endl;
    return(0);
}
//
int RXSail:: SetOneInterpolatedPressure()  // looks for a scalar field names 'pressure'
{

    /* apply the presure in val[0] to all the triangles.
           */


    int count2 =0;
    if(this->m_FTri3s.size() <2)
        return 0;
    RXSitePt p; double rv ;
    assert( "SetOneInterpolatedPressure"==0);
    RXEntity_p  e = this-> Entity_Exists("pressure","interpolation surface") ;
    if(!e) return 0;
    if(e->Needs_Resolving)
        return 0;

    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!(te && te->GetN() )) continue;
        te->Centroid(&p);
         rv= PC_Interpolate(e,  p);
        count2+=te->SetPressure(rv  * this->m_Pressure_Factor);
    }

    cout <<"Done Setting Interpolated Pressures " <<" on "<<GetType() << " - Ntriangles ="<< count2<<endl;
    return(0);
}

int RXSail::SetOneFieldPressure()  // looks for a scalar field names 'pressure'
{

    /* apply the presure in val[0] to all the triangles.
           */


    int count2 =0;
    if(this->m_FTri3s.size() <2)
        return 0;
    RXSitePt p; double rv[3];
    RXEntity_p  e = this-> Entity_Exists("pressure","vector field") ;
    class RXVectorField *f=dynamic_cast<class RXVectorField *>( e);
    if(!f) return 0;

    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!(te && te->GetN() )) continue;

        te->Centroid(&p);
        f->ValueAt(p, rv);

        count2+=te->SetPressure(rv[2] * this->m_Pressure_Factor);
    }

    cout <<"Done Setting FIELD Pressures " <<" on "<<GetType() << " - Ntriangles ="<< count2<<endl;
    return(0);
}

int RXSail::SetOneConstantPreStress(const double val[3])
{

    /* apply the pre-stress in val[0],val[1],val[2] to all the triangles.
   */

    int count2;


    cout<<"Setting PreStress on \n"<<this->GetType() << endl;
    count2 = 0;
    if(this->m_FTri3s.size() <2)
        return 0;
    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if(!(te && (te->GetN() )  )  ) continue;

        te->prestress[0] =  val[0];
        te->prestress[1] =  val[1];
        te->prestress[2] =  val[2];
        te->SetProperties();
        count2++;
    }

    cout << "Done Setting PRESTRESS  ( "<<val[0]<< ", "<<val[1]<< ", "<<val[2]<< " ) "<<" - "<< count2 <<" triangles in ";
    cout<<this->GetType().c_str()<<endl;

    return(count2);
}

#ifdef USE_PANSAIL
int RXSail::WritePansailPressures( const char *p_filename)
{
    float *dp;
    int mod,m,n,r,c;
    FILE *fp;

    fp = RXFOPEN(p_filename,"w");
    if(!fp) return 0;

    mod = GetLoadingIndex()-1;
    m = g_M[mod]-1;
    n = g_N[mod]-1;
    fprintf(fp,"pressure\t from_%s \tThis is dCp $m=%d, $n=%d \t $table\n", GetType().c_str(),m+1,n+1);

    dp  = &(g_Dcp[g_coffset[mod]]);

    for(r=0;r<n; r++) {			// N is spanwise, M is chordwise// N is nr, M is Nc
        for(c=0;c<m; c++) {		// nc is chordwise , nr is spanwise(N)
            fprintf(fp, " \t%f ", *dp);
            dp++;
        }
        fprintf(fp," \n");
    }
    fprintf(fp,"$endtable\n");
    FCLOSE(fp);
    return 1;

}
#endif
