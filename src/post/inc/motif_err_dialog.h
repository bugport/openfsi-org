
// motif_err_dialog.h header for motif_err_dialog.cpp
//Written by Thomas 07 06 04


#ifndef RX_MOTIF_ERR_DIALOG
#define RX_MOTIF_ERR_DIALOG

#ifdef _X
	void err_ok_CB(Widget w, caddr_t client_data, XmAnyCallbackStruct * call_data );

	void err_cancel_CB(Widget w, caddr_t client_data, XmAnyCallbackStruct * call_data );

	void err_degrade_CB(Widget w, caddr_t client_data, XmAnyCallbackStruct *call_data );
#endif //#ifdef _X

/* motif_err_dialog.c: */
EXTERN_C int do_err_dialog(const char *s, int severity);

//extern  int rxerror(const char *s,const  int i){ return do_err_dialog(s,i);}
//extern  int rxerror(const ON_String s, const int i){ return do_err_dialog(s.Array(),i);}

#endif   //#ifndef RX_MOTIF_ERR_DIALOG
