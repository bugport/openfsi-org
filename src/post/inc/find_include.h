#ifndef _find_include_h
#define _find_include_h  1



struct HOOPS_ITEM{
	char *name;
	long m_Key; // SSD
	int state;
	int indx;
	int m_HIChosen;
	Widget widget;
};
typedef struct HOOPS_ITEM HoopsItem;

struct AFRAME {
	Widget frame_widget;
	char *sailname;
	SAIL *sail;
	HoopsItem *inc_items;
	int inc_count;
	HoopsItem *disp_items;
	int disp_count;
	HoopsItem *prop_items;
	int prop_count;
	int graphstate;
	Widget m_asgraph;
	Widget m_freeze;
	int position;
	int m_AFchosen;	/* which property is chosen */
	int changed;
};
typedef struct AFRAME Frame;

struct FRAMEDATA {
	int m_nframes;
	Frame **m_frames;
	Graphic *m_g;
	Widget m_post_processor;
};

typedef struct FRAMEDATA FrameData;
EXTERN_C int find_all_include_items_by_sail(SAIL *sail,int *nitems,HoopsItem **hitems);

EXTERN_C int 	Get_state_NonEx_items		(Graphic *ViewGraphic,
						const char *sailname,int nitems,HoopsItem *hitems);

EXTERN_C int 	Initialize_OptionStyle_Items	(const char *sailname,int *nitems,HoopsItem **hitems);

EXTERN_C int 	Get_State_OptionStyle_Items	(Graphic *ViewGraphic,
						const char *sailname,int nitems,HoopsItem *hitems);
						
EXTERN_C int 	Find_All_Exclusive_Items		(SAIL *Sail,
							int *nitems,HoopsItem **hitems);
EXTERN_C char 	*Get_current_shell	(Graphic *g,const char *sailname,char *theShell);

EXTERN_C int 	Get_state_graph_button	(long base, const char *sailname);



int set_chosen_property_items	(HoopsItem *hitems,int nitems,const char *theShell);
int set_state_property_items	(HoopsItem *hitems,int nitems,int graphstate);
int set_state_property_buttons	(HoopsItem *prop_items,int count);




#endif  //#ifndef _find_include_h


