/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 26/3/96 PCN_PANSAIL
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "batdefs.h"
#include  "getfilename.h"
#include "stringutils.h"
#include "global_declarations.h"
#ifdef _WINDOWS
//#include "dummynt.h"
#endif
#include  "readedg.h"
 int  findStringForEDG(FILE *fp,const char *str)
{
char line[256];
long len;

while(fgets(line,255,fp)) {
	if(stristr(line,str)) {
		return(1);
	}
}
return(0);
}//int findString






int Read_Edg_File(char *fname,struct PCN_PANSAIL *sd)
{

FILE *fp;
int found=0;
char line[1024],data[2048];


data[0] = '\0';

if(sd->leechWakeString)
	RXFREE(sd->leechWakeString);
sd->leechWakeString = NULL;
if(sd->footWakeString)
	RXFREE(sd->footWakeString);
sd->footWakeString = NULL;

fp = FOPEN(fname,"r");
if(!fp) {
    std::string filename = getfilename( NULL,"*.edg","Open PANSAIL .edg File",g_currentDir,PC_FILE_READ) ;
    if(!filename.empty() )
        fp = FOPEN(filename.c_str(),"r");

    else {
    	g_World->OutputToClient("Sorry, PANSAIL is about to crash (edgfile missing)",2);
    	return(0);
    }
}


if(findStringForEDG(fp,"LeechWake")) {
	/* start by getting the leech wake */
	while(fgets(line,255,fp)) {
		if(stristr(line,"wake"))
			break;
		strcat(data,line);
	}
	sd->leechWakeString = STRDUP(data);
	rewind(fp);
}


data[0] = '\0';
if(findStringForEDG(fp,"FootWake")) {
	while(fgets(line,255,fp)) {
		if(stristr(line,"wake"))
			break;
		strcat(data,line);
	}
	sd->footWakeString = STRDUP(data);
	rewind(fp);
}

FCLOSE(fp);
return(1);
}


