/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *  16/10/96 the re allocs made a bit larger
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */



/* header with index nos of heading, speed etc. in */

#include "StdAfx.h" 
#include "RXGraphicCalls.h"
#include "RelaxWorld.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXExpression.h"
#include "RX_FEString.h"
#include "RX_FEPocket.h"

#ifdef _X
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/TextF.h>
#endif

#include "controlmenu.h"
#include "Gflags.h"
#include "RX_FEPocket.h"
 
#include "etypes.h"
#include "global_declarations.h"
#include "stringutils.h"
#include "trimmenu_s.h"
#include "settrim.h"

/* set control text values in the trim dialog */
#if 0
int update_control_trims(void)
{
if(!g_TrimExists)
  return(0);
set_control_trims(&(g_control_text[0]),CONTROL_COUNT);
return(CONTROL_COUNT);
}
#endif

int get_control_trims(Widget *Wlist)  /* from text boxes */
{
//char buf[64]; /* text string buffer for output text */
char *sz;
float value;
#ifdef _X
sz = XmTextFieldGetString(Wlist[HEADING]);

value = atof(sz);
XtFree(sz);
#else
value=999;
assert(0);
#endif
if(fabs(value - g_Heading) > DIFFERENT_TOLERANCE) {
	g_Heading = value;
	mkGFlag(GLOBAL_DO_APPWIND);
	mkGFlag(GLOBAL_DO_REFLECTION);
	mkFlag(g_Analysis_Flags,WIND_CHANGE);
}
#ifdef _X
sz = XmTextFieldGetString(Wlist[LEEWAY]);
value = atof(sz);
XtFree(sz);
#else
value=999;
assert(0);
#endif
if(fabs(value - g_Leeway) > DIFFERENT_TOLERANCE) {
	g_Leeway = value;
	mkGFlag(GLOBAL_DO_APPWIND);
	mkFlag(g_Analysis_Flags,WIND_CHANGE);
}
#ifdef _X
sz = XmTextFieldGetString(Wlist[BOAT_SPEED]);
value =atof(sz); //Evaluate_Quantity(NULL,sz,"speed");

XtFree(sz);
#else
value=999;
assert(0);
#endif
if(fabs(value - g_Vboat) > DIFFERENT_TOLERANCE) {
	g_Vboat = value;
	mkGFlag(GLOBAL_DO_APPWIND);
	mkFlag(g_Analysis_Flags,WIND_CHANGE);
}
#ifdef _X
sz = XmTextFieldGetString(Wlist[WIND_SPEED]);
g_World->OutputToClient ("(SETTRIM) calling eval",2);

value = atof(sz); // Evaluate_Quantity(NULL,sz,"speed");

XtFree(sz);
#else
value=999;
assert(0);
#endif
if(fabs(value - g_Vwind) > DIFFERENT_TOLERANCE) {
	g_Vwind = value;
	mkGFlag(GLOBAL_DO_APPWIND);
	mkFlag(g_Analysis_Flags,WIND_CHANGE);
}
#ifdef _X
sz = XmTextFieldGetString(Wlist[HEEL_ANGLE]);
value = atof(sz);
XtFree(sz);
#else
value=999;
assert(0);
#endif
g_World->SetHeelAngle(value) ;
#ifdef _X
sz = XmTextFieldGetString(Wlist[MAST_LENGTH]);
value = atof(sz);
XtFree(sz);
#else
value=999;
assert(0);
#endif
if(fabs(value - g_PansailIn.Reference_Length) > DIFFERENT_TOLERANCE) {
	g_PansailIn.Reference_Length = value;
	mkGFlag(GLOBAL_DO_APPWIND);
	mkFlag(g_Analysis_Flags,WIND_CHANGE);
}
#ifdef _X
sz = XmTextFieldGetString(Wlist[RUN_NAME]);
if(sz) 
       g_World->ModifyRunName(sz);

XtFree(sz);
#else
value=999;
assert(0);
#endif

return(CONTROL_COUNT);
}




int find_all_strings(int *count,StringItem **slist)  // pockets too
{

StringItem *StrList;
int cnt=0,size=10,i,j;
class RX_FEPocket *pock;
StrList = *slist;

// deal with a leak
for(i=0;i<*count;i++) {
	if(StrList[i].valStr[LENGTH] ) 	RXFREE( StrList[i].valStr[LENGTH] );
	if(StrList[i].valStr[TENSION]) 	RXFREE( StrList[i].valStr[TENSION] );
	//if(StrList[i].name) 			RXFREE( StrList[i].name );
}
StrList = (StringItem*)REALLOC(StrList,(size+2)*sizeof(StringItem));
*count = size+2; // gets reset later we hope

    cnt=0;

  	vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
	for(i=0;i<lsl.size();i++)  {
		SAIL *theSail = lsl[i];
		RXEntity_p p;
		vector<RXEntity_p >thelist;
		theSail->MapExtractList(PCE_STRING,  thelist,PCE_ISRESOLVED) ;

		vector<RXEntity_p >::iterator it;
		for (it = thelist.begin (); it != thelist.end(); it++)  {
 			class RX_FEString* TheString = dynamic_cast<class RX_FEString *> (*it);
	    		if ( !strchr(TheString->name(),'&')
				&& !stristr(TheString->attributes,"$notrim")   )  {
	     			StrList[cnt].m_sEnt = TheString;
					StrList[cnt].value[LENGTH] = TheString->GetTrimPtr();   
	      			StrList[cnt].value[TENSION] = TheString->GetPrestressPtr(); ;    /* change later !!!!!!!!! */
	     			StrList[cnt].valStr[LENGTH] = (char*)MALLOC(MAX_TEXT_LENGTH*sizeof(char));
	      			StrList[cnt].valStr[TENSION] = (char*)MALLOC(MAX_TEXT_LENGTH*sizeof(char));
	      			sprintf(StrList[cnt].valStr[LENGTH],"%8.3f",*(StrList[cnt].value[LENGTH]));
	      			sprintf(StrList[cnt].valStr[TENSION],"%8.3e",*(StrList[cnt].value[TENSION]));
	      			StrList[cnt].which = TheString->m_which;
	      			StrList[cnt].changed = 0;
			/* add code here to set the tension */
			  cnt++;	
			  if(cnt == size) {
			size = size +10;
			StrList = (StringItem*)REALLOC(StrList,(size+2)*sizeof(StringItem));
			  }		
			}
	} // for
		thelist.clear();
		theSail->MapExtractList(POCKET,  thelist,PCE_ISRESOLVED) ;
		for (it = thelist.begin (); it != thelist.end(); it++)  {
				pock =  dynamic_cast<RX_FEPocket *> (*it);      
				assert(pock->TYPE==POCKET );
				StrList[cnt].m_sEnt = pock;
				StrList[cnt].value[LENGTH] = pock->GetTrimPtr();
				StrList[cnt].valStr[TENSION] = NULL;
				StrList[cnt].valStr[LENGTH] = (char*)MALLOC(MAX_TEXT_LENGTH*sizeof(char));
				sprintf(StrList[cnt].valStr[LENGTH],"%8.3f",*(StrList[cnt].value[LENGTH]));
				StrList[cnt].which = LENGTH; 	      StrList[cnt].changed = 0;
				/* add code here to set the tension */
				cnt++;	
				if(cnt == size) {
					size = size +10;
					StrList = (StringItem*)REALLOC(StrList,(size+2)*sizeof(StringItem));
				}		

 	  }

    } // for i

*count = cnt;
*slist = StrList;
return(*count);
 
}

int find_all_editable(int *count,EditItem **slist) 
{
EditItem *EdList;
int cnt=0,size=10,i,j;

RXEntity_p  nodeEnt;
double a,b,c;
 int segopen=0;

EdList = *slist;
// deal with a leak march 2003
for(i=0;i<*count;i++) {
	if(EdList[i].ValStr[0]) 	RXFREE( EdList[i].ValStr[0] );
	if(EdList[i].ValStr[1]) 	RXFREE( EdList[i].ValStr[1] );
	if(EdList[i].ValStr[2]) 	RXFREE( EdList[i].ValStr[2] );
	EdList[i].ValStr[0]=NULL;	
	EdList[i].ValStr[1]=NULL;	
	EdList[i].ValStr[2]=NULL; 
//	if(EdList[i].Name) 		RXFREE( EdList[i].Name );
}
EdList = (EditItem*)REALLOC(EdList,(size+2)*sizeof(EditItem));


    cnt=0;

  	vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
	for(i=0;i<lsl.size();i++)  {
		SAIL *theSail = lsl[i];
		RXEntity_p p;
		vector<RXEntity_p >thelist;
		theSail->MapExtractList(SITE,  thelist,PCE_ISRESOLVED) ;
		//theSail->MapExtractList(RELSITE,  thelist,PCE_ISRESOLVED) ;
		vector<RXEntity_p >::iterator it;
		for (it = thelist.begin (); it != thelist.end(); it++)  {
		 nodeEnt = *it;
 

	if(stristr(nodeEnt->attributes,"editable"))  {
		assert(cnt<size);
	      EdList[cnt].s = dynamic_cast<RXSRelSite* > (nodeEnt );
	      EdList[cnt].e = nodeEnt;
	      EdList[cnt].sail  = theSail;
	      EdList[cnt].ValStr[0] = (char*)MALLOC(MAX_TEXT_LENGTH*sizeof(char));
	      EdList[cnt].ValStr[1] = (char*)MALLOC(MAX_TEXT_LENGTH*sizeof(char));
	      EdList[cnt].ValStr[2] = (char*)MALLOC(MAX_TEXT_LENGTH*sizeof(char));
/* changed here to reflect the coord type nature of the node */
 	 segopen=0;
	if(nodeEnt->hoopskey) {
			HC_Open_Segment_By_Key(nodeEnt->hoopskey);
			segopen=1;
		  }
 	else {
		segopen=0;
		printf("hoopskey zero %ld\n",  nodeEnt->hoopskey);
	}
	  a =  (EdList[cnt].s)->FindExpression(L"a",rxexpLocal)->evaluate()	;
	  b =  (EdList[cnt].s)->FindExpression(L"b",rxexpLocal)->evaluate()	;
	  c =  (EdList[cnt].s)->FindExpression(L"c",rxexpLocal)->evaluate()	;
 
  	  if(segopen) HC_Close_Segment();

	      sprintf(EdList[cnt].ValStr[0],"%5.3f",a);
	      sprintf(EdList[cnt].ValStr[1],"%5.3f",b);
	      sprintf(EdList[cnt].ValStr[2],"%5.3f",c);
	      EdList[cnt].changed = 0;

		/* add code here to set the tension */
	      cnt++;	
	      if(cnt == size) {
		size = size +10;
		EdList = (EditItem*)REALLOC(EdList,(size+2)*sizeof(EditItem));
	      }		
	    }
 	  } // for sails


     }

*count = cnt;
*slist = EdList;
return(*count);
}



