/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified ;
  Aug 2003 Do_Edit_Script is the template for line winning over g.
  * 26 mar 2003  Do_SaveAs is the template for these Do_... routines
 *   5 5 2002  there were some free's and strdups because of getfilename confusion. All capitalised

 * Synched from HP
 *   3 Feb 97 Adam Molyneaux.
 *                  Increased Firstword and SecondWord buffer sizes to 512 from 64
 *                  and nextDir from 128 to 512

  *   14/8/96 generates a script file

The crash in Choose Line is a consequence of the Choose line dialog not holding up the flow
 like a file selection box. Lets leav it for now

 * TESTING OF SCRIPT FILE
 Keyword			complete		incomplete	escaped
 CD			OK		OK		OK
    Open Boat		OK		OK		OK
  Hoist			OK		OK		OK
   Open Summary				OK		OK
  Open Input				OK		OK
  Choose Line		OK		CRASHES	OK
 Open Camera		OK		NA		NA
 Calculate			OK		NA		NA
 ( Open camera, set view & options, minimise)

 *   reset_one _Edges now calcs solid angle before & after
 *  27/4/96  dxfout added to panel export.
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
/* a script reader - based directly on the current Read_Macro
 * routine found in entities.c
 */
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "mkl_dss.h"
#include "mkl_types.h"
#include "mkl_spblas.h"
using namespace std;
#include "mkl_lapack.h"
#include "mkl_spblas.h"

#include "StdAfx.h"  //CAREFUL.  Htools redefines strieq to be case sensitive.
#include <fstream>
#ifdef RXQT
#include <QtCore>
#include <QDebug>
#include <QTextStream>

#endif
#include "Gflags.h"
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "RelaxWorld.h"
#include "RXLogFile.h"
#include "RXDBLink.h"
#include "rxmirrorsimple.h"
#include "RXEntity.h"
#include "RXQuantity.h"
#include "RX_FEString.h"
#include "rxsubwindowhelper.h"
#include "summary.h"
#include "rxmirrorqt.h"
#include "RXSparseMatrix.h"
#include "RXSparseMatrix1.h"
#ifdef WIN32
#include "akmutil.h"
#endif
#include "etypes.h"
#include "getglobal.h"
#ifdef _X
#include "Menu.h"
#include "menuCB.h"
#endif
#include "offset.h"
#include "entities.h"
#include "camera.h"

#include "files.h"
#include "scedit.h"
#include "reledit.h"

#ifdef _X
#include "ch_rowt.h"
#include "ch_rowcb.h"
#include "ch_row.h"
#include "OpenMat.h"
#endif
#include "OpenBuildFile.h"
#include "OpenView.h"
#include "drop.h"
#include "redraw.h"
#include "reset.h"

#include "akmutil.h"

#include "dxfout.h"
#include "question.h"

#include "optimise.h"

#include "getfilename.h"
#include "angcheck.h"

#include "trimmenu_s.h"
#include "ReadBagged.h"

#include "SaveSail.h"
#include "ReadBoat.h"

#include "printall.h"

#include "gle.h"
#include "gcurve.h"
#include "checktop.h"
#include "OpenView.h"

#include "pansin.h"
#include "cfromf.h"
#include "words.h"
#include "CheckAll.h"

#include "global_declarations.h"
#include "RLX3dm.h"

#include "freeze.h"
#include "f90_to_c.h"
#include "stringutils.h"
#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif
#ifdef RXQT
#include "rxqtdialogs.h"
#include "rxdblinklongqt.h"

#endif

#ifdef ODETESTING
#include "rxstiffsolver.h"
#include "rxrungekutta.h"
#pragma message(" compiling script.cpp for ODE test")
#endif
extern "C" int RXPrint(class QObject *q, const char*s,const int level);
#include "script.h"

int Edits_Exist;
static int SDB=0;

#define OFFSETS_CHANGED (1966)
#define EXTRA 0.05

struct DO_STRUCTURE  g_The_Dos[MAXDOS];
int g_N_Dos = 0;

int Initialise_Script(void) // returns g_N_Dos
{
    int i=0;
    if(g_N_Dos) return g_N_Dos;

    g_The_Dos[i].m_s="open boat";   	  g_The_Dos[i].m_gm= Do_Open; 	        	g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_READ;
    g_The_Dos[i].m_s="open sail";   	  g_The_Dos[i].m_gm= Do_Open; 	        	g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_READ;
    g_The_Dos[i].m_s="open bag";   	  g_The_Dos[i].m_gm= Do_Open; 	        	g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_READ;
    g_The_Dos[i].m_s="hoist";   	  g_The_Dos[i].m_gm= Do_Hoist; 	        	g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_READ;
    g_The_Dos[i].m_s="cd";                g_The_Dos[i].m_gm= Do_CWD;                    g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="editscript";	  g_The_Dos[i].m_mfg= Do_Edit_Script;           g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="Load State";	  g_The_Dos[i].m_mfg= Do_Load_State ;           g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_READ;
    g_The_Dos[i].m_s="saveas";  	  g_The_Dos[i].m_mfg= Do_SaveAs ;               g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_WRITE;
    g_The_Dos[i].m_s="savestate";  	  g_The_Dos[i].m_mfg= Do_Save_State ;           g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_WRITE;
    g_The_Dos[i].m_s="save state";  	  g_The_Dos[i].m_mfg= Do_Save_State ;           g_The_Dos[i++].m_flag=SPT_STREQ | SPT_FILE_WRITE;

    g_The_Dos[i].m_s="write 3dm"; 	  g_The_Dos[i].m_smf= Do_Write_3dm; 		g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write_3dm"; 	  g_The_Dos[i].m_smf= Do_Write_3dm; 		g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write_uvdp"; 	  g_The_Dos[i].m_smf= Do_Write_uvdp; 		g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write_vtk"; 	  g_The_Dos[i].m_smf= Do_Write_vtk; 		g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write_attributes"; 	  g_The_Dos[i].m_smf= Do_Write_Attributes; 		g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write_stl"; 	  g_The_Dos[i].m_smf= Do_Write_stl; 		g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write stress";      g_The_Dos[i].m_smf= write_stress_by_triangle; g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write DMatrix";     g_The_Dos[i].m_smf= write_D_by_triangle;	g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="write_nastran";     g_The_Dos[i].m_smf= Do_Write_Nastran;   	g_The_Dos[i++].m_flag=SPT_STREQ + SPT_FILE_WRITE;
    g_The_Dos[i].m_s="Refresh"; 	  g_The_Dos[i].m_sm= &Do_RefreshPC ; 		g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="printall"; 	  g_The_Dos[i].m_sm= &Do_PrintAllEntities ; 	      g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="mesh";     	  g_The_Dos[i].m_sm= &Do_Mesh ;          	          g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="Freeze"; 	        g_The_Dos[i].m_sm= &PCP_Freeze; 		          g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="UnFreeze"; 	      g_The_Dos[i].m_sm= &PCP_UnFreeze;		          g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="SetLastKnownGood";g_The_Dos[i].m_sm= &Do_SetLastKnownGood; 	    g_The_Dos[i++].m_flag=SPT_STREQ ; // set TO the saved LKG
    g_The_Dos[i].m_s="deletemodel"; 	  g_The_Dos[i].m_sm= &Do_DeleteModel ;          g_The_Dos[i++].m_flag=SPT_STREQ ;

    g_The_Dos[i].m_s="drawStress"; 	    g_The_Dos[i].m_sm= &Do_DrawStress ;           g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="drawstrain"; 	    g_The_Dos[i].m_sm= &Do_DrawStrain ;           g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="drawrefvector"; 	g_The_Dos[i].m_sm= &Do_DrawRefVector;         g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="drawPrinStiff"; 	g_The_Dos[i].m_sm= &Do_DrawPrinStiff ;        g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="Discretize";      g_The_Dos[i].m_sm= &Do_DiscretizeCurvature;   g_The_Dos[i++].m_flag=SPT_STREQ ;

    g_The_Dos[i].m_s="print_object";    g_The_Dos[i].m_lm= Do_PrintObject;            g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="apply";           g_The_Dos[i].m_lm= Do_Apply;                  g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="MakeLastKnownGood";g_The_Dos[i].m_lm= Do_MakeLastKnownGood; 	  g_The_Dos[i++].m_flag=SPT_STREQ; // generate a new LKG
    g_The_Dos[i].m_s="change";		      g_The_Dos[i].m_lm= Do_ChangeExpression ; 	    g_The_Dos[i++].m_flag=SPT_STREQ;
    g_The_Dos[i].m_s="updateexpression";g_The_Dos[i].m_lm= Do_UpdateExpressions; 	    g_The_Dos[i++].m_flag=SPT_STREQ;
    g_The_Dos[i].m_s="edgeswap";        g_The_Dos[i].m_lm= Do_EdgeSwap; 	            g_The_Dos[i++].m_flag=SPT_STREQ;
    g_The_Dos[i].m_s="listing";		      g_The_Dos[i].m_lm= Do_FEA_Listing ;           g_The_Dos[i++].m_flag=SPT_STREQ;
    g_The_Dos[i].m_s="settings apply";	g_The_Dos[i].m_lm= Do_RefreshSettings ; 	    g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="process changes";	g_The_Dos[i].m_lm= Do_BringAll ;              g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="Settings Edit";	  g_The_Dos[i].m_lm= Do_EditSettings;           g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="open camera";   	g_The_Dos[i].m_lm= Do_Open_View;              g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="open_mirror";   	g_The_Dos[i].m_lm= Do_Open_Mirror;            g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="system";   	      g_The_Dos[i].m_lm= Do_System;                 g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="write_log";   	  g_The_Dos[i].m_lm= Do_LogResult ;             g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="test";            g_The_Dos[i].m_lm= Do_Test ;                  g_The_Dos[i++].m_flag=SPT_STREQ ;

    g_The_Dos[i].m_s="postBlob";          g_The_Dos[i].m_lm=Do_Post_As_Blob;            g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="broadcast";         g_The_Dos[i].m_lm=Do_Broadcast;               g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="prestress"; 	      g_The_Dos[i].m_lm= Do_PrestressSet  ;         g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="shrinkEdges"; 	  g_The_Dos[i].m_lm=Do_Shrink_All_Edges;        g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="ResetEdges"; 	      g_The_Dos[i].m_lm=Do_Reset_All_Edges;         g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="graphics"; 	      g_The_Dos[i].m_lm= Do_SetGraphicsOn;          g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="BuildMaterials"; 	  g_The_Dos[i].m_lm= Do_RebuildMaterials;       g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="dsmInit";     	  g_The_Dos[i].m_lm= Do_DSM_Init;               g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="dsmEnd";      	  g_The_Dos[i].m_lm= Do_DSM_End;                g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="write_dsm";   	  g_The_Dos[i].m_lm= Do_DSM_TransferModel;      g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="dsmWrite";     	  g_The_Dos[i].m_lm= Do_DSM_TransferModel;      g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="loop";     	      g_The_Dos[i].m_lm= Do_RunLoop;                g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="setting";     	  g_The_Dos[i].m_lm= Do_OneSetting;             g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="modellist";     	  g_The_Dos[i].m_lm= Do_ModelList;              g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="dependencies";      g_The_Dos[i].m_lm= Do_ListDependencies;       g_The_Dos[i++].m_flag=SPT_STREQ ;
    g_The_Dos[i].m_s="debug";           g_The_Dos[i].m_lm= Do_Debug;                  g_The_Dos[i++].m_flag=SPT_STREQ ;
    //g_The_Dos[i].m_s="ReDove"; 	g_The_Dos[i].m_sm=ReDove;			g_The_Dos[i++].m_flag=SPT_STREQ + SPT_KW_SAIL;

    g_N_Dos=i;
    assert(i < MAXDOS);
    return g_N_Dos;
}
int Print_Script_List(void) // returns g_N_Dos
{
    int i;
    for(i=0;i< g_N_Dos;i++) {
        printf(" %s   %p  %d \n", g_The_Dos[i].m_s,(void*)0, g_The_Dos[i].m_flag);
    }


    return g_N_Dos;
}

Graphic *NameToGraph(const char*sailname) {// Finds the FIRST model with this name
    SAIL *sail = g_World->FindModel(sailname);
    if(!sail) { printf(" no hoisted model with name <%s>\n",sailname); return 0;}
    return(sail->GetGraphic());
}

#ifdef _X
int Do_Minimise_Window(Graphic *g, int i)  {


    int l_ac = 0;
    Arg al[64];
    if(i)	{
        XtSetArg(al[l_ac], XmNiconic, TRUE); l_ac++;
        cout<< " Minimise\n"<<endl;
    }
    else	{
        XtSetArg(al[l_ac], XmNiconic, FALSE); l_ac++;
        cout<< " UNMinimise\n"<<endl;
    }
    XtSetValues (g->topLevel,al, l_ac );
    XmUpdateDisplay(g->topLevel);

    return 1;
}
#endif

// the editword command is unusual in that it can be executed either
// by the command interpreter  (see Do_EditWord )
// or as an entity in a model script. (see Resolve_EditWord_Card )
// the format is identical, but the second word has a different meaning
// in the command form, it gives the modelname .
// in the model-script form, it is just an entity name (and should be unique)
// the command-form implementation is much more efficient.
//
//  an edit command is
// editword:genoa:seamcurve:lojo:<insertionPoint>:<newvalue>[newvalue[newvalue]]
// If the entity doesnt exist it is created.
//  - but note that in this case you need the insertion point to be 2
//  eg to make
//      'relsite 	gooseneck	track	BAS	'
//  you would go
//      editword: boat:relsite:gooseneck:2:track:BAS

// Ifthe entity [model/type/name]  already exists, it is modified.
// starting from the word at index <insertionPoint> it replaces consecutive words until the list of newvalues is exhausted.


int Do_EditWord(const char *line,Graphic *gin){

    RXEntity_p  e=NULL ;
    int  nw=0,  i=0 ;//, space

    int insertion_point;
    printf("Do_EditWord. line =<%s>\n",line );
    vector<string> w;
    string Buf(line);
    w = rxparsestring(Buf,":\t\n"); nw=w.size();

    if(nw>4) {
        insertion_point = i = atoi(w[4].c_str()); // index of the first word to replace
        if(i<2) {cout<<" you cant use editword to change the name or type of an object "<<endl; return 0;}
        printf("first 4  %s  %s  %s  %s (%d) \n", w[1].c_str() ,w[2].c_str(),w[3].c_str(),w[4].c_str(),i);
        SAIL *theSail = g_World->FindModel(w[1],1);
        if(!theSail  )theSail = g_World->FindModel(w[1],0);
        if(!theSail) return 0;

        e =theSail->GetKeyWithAlias(w[2].c_str(),w[3].c_str());


        if(e) {
            printf(" edit line has %d words\n",nw);
            Edits_Exist=true;
            theSail->SaveStateIfNecessary();
            //remove first 4 from words.
            w.erase (w.begin(),w.begin()+5);
            Buf=e->line; RXFREE(e->line);
            rxReplaceWords(Buf,i,w, RXENTITYSEPS );
            e->line=STRDUP(Buf.c_str());
            // we changed the words in e from i to i-1+size(w)
            // if any of these have int RelaxWorld::EditAction == RX_TOPOLOGYCHANGE we need to do a CClear
            // else we do a OnValue Change(); NOTE this isnt necessary because we have Do_Change for value changes.
            bool NeedToCClear=false;
            for(size_t k=i;k< i+w.size(); k++){ //
                if(theSail->GetWorld()->EditAction(e->TYPE,i)  == RX_TOPOLOGYCHANGE){
                    NeedToCClear=true; break;
                }
            }

            if(NeedToCClear)
                e->CClear();
            else{
                e->Resolve(); // not sure. calls at least e->OnValue Change();
            }
            e->SetNeedsComputing();
        } // if e
        else { // e is null. didnt find an entity
            char  *newent;
            int k, depth=0;
            // so create one type - words[2] name words[3] then fill in up to 2 with blanks
            //			 then add the other words
            //				 then resolve.
            newent = STRDUP(line);
            sprintf(newent,"%s%s%s", w[2].c_str(),RXENTITYSEPS,w[3].c_str());
            for (k=2; k< insertion_point;k++)
                strcat(newent,RXENTITYSEPS);
            for(k=5;k<nw;k++){
                strcat(newent,RXENTITYSEPS);
                strcat(newent, w[k].c_str());
            }
            Just_Read_Card(theSail,newent,  w[2].c_str(),w[3].c_str(),&depth);
            RXFREE(newent);
        } // if null so create new.

    } // too few words to treat
    else {
        QString b ("EditWord. less than 4 words in line " ); b+=line;
        b+=QString(" syntax\n editword modelname   entitytype  entityname  insertionpoint newvalue [next word...] ");
        b+=QString(  "\n (insertion point is counted from 0. IE the 6th word is insertion point 5) \n");
        b+=QString(  "\n If the entity doesnt exist it is created; So you need insertionpoint= 2 \n");
        g_World->OutputToClient(b,2);
        return 0;
    }
    return 1;
}

int Do_Export_Pic(const char*line,Graphic *gin){
    /* if gin is null, try to identify it from second word of line.
line is
Export Plot # <viewname> # filename # options
the options string may contain some of
everything  hmf  pict  eps  rad  jpg  jpeg  dxf iges fullscreen
see radiomenus.c
*/
    if(line) cout<<" Export Pic with line<"<<line<<">"<<endl;
    Graphic *g;
    char  fname[512];
    const char *cam=0;
    std::string options (g_Export_Options);
    std::string sline;
    if (line ){
        sline=string(line);
        rxstriptrailing(sline);
    }
    std::vector<std::string> wds = rxparsestring(sline,RXSCRIPTSEPS,false);
    int nw = wds.size();
    if(nw>1)  cam  =wds[1] .c_str();
    if(nw>2)  strcpy(fname,wds[2] .c_str());
    if(nw>3 && !wds[3].empty())
        options=wds[3];

    if(gin)
        g=gin;
    else
        g = RXSubWindowHelper::Find_Graphic_By_Camera(cam);

#ifndef HOOPS  // a development fixup to give at least something
    if(!g) {
        SAIL *sail = g_World->FindModel(cam ,0);
        if(!sail) {
            cout<<" no  view or model with name <"<<cam<<">"<<endl;
            return 0;
        }
        RXGRAPHICSEGMENT root = sail->GNode();
        if(!root)
            return 0;

        RXGNodeWrite(fname,root);
        return 0;
    }

#else
    char  buf[512],hmfname[512];
    HC_Show_Segment(g->m_ViewSeg,buf);

    if(!is_clean(fname,256)) {printf(" filename <%s> no good\n", fname); return 0;}
    if(is_empty(fname)) return 0;

    PC_Replace_Char(fname, ' ', '_');

    char *lp = strrchr(fname,'.'); if(lp) *lp=0;  // strip the extension
    if(!lp) { lp = fname; lp+=strlen(fname); }
    cout<<"export options are (must be minuscule) '"<<options.c_str () <<"'"<<endl;
    if(options.find("everything")||options.find("3dm")) {
        strcat(fname, ".3dm");
        HC_Show_Segment(g->m_ViewSeg,buf);
        cout<<" go to write 3dm on "<<buf<<endl;
        AMG_3dm_Write(g->m_ViewSeg,fname);

        lp = strrchr(fname,'.'); if(lp) *lp=0;  // strip the extension
    }
    if(options.find("everything")||options.find("hmf")) {
        strcat(fname, ".hmf");
        HC_Show_Segment(g->m_ViewSeg,buf); cout<<" viewseg is "<<buf;

#ifdef linux
        sprintf(hmfname, "%s",fname); cout<< " IBM/linux build>> file= "<< hmfname<<endl;
#else
        sprintf(hmfname, "'%s'",fname);cout<<"Windows build>> "<< hmfname<<endl;
#endif
        int l_mcount;
        char map[1024];
        HC_Open_Segment(buf);
        HC_Show_Net_Color_Map_Count ( &l_mcount);
        HC_Define_System_Options("C string length=1023");
        HC_Show_Net_Color_Map(map);

        HC_Close_Segment();
        HC_Copy_Segment(buf,"/output");
        HC_Open_Segment("/output");
        HC_Rotate_Object(-90.0,0.0,0.0);
        HC_QUnSet_Visibility("(.,*,...)");
        HC_UnSet_Window();
        HC_QUnSet_Camera("(.,*,...)");
        HC_Set_Color_Map ( map);
        if(HC_Show_Existence("style")) HC_Flush_Contents(".","style");

        HC_Flush_Contents("(.,*...)","user indices");
        HC_Delete_Segment("/output.../staticgeometry");

        cout<<" go to write metafile"<<endl;
        HC_Write_Metafile(".",hmfname,"follow cross-references,use color names");
        cout<<"written metafile"<<endl;
        HC_Close_Segment();
        HC_Define_System_Options("C string length=255");
        HC_Delete_Segment("/output");
    }

    if(options.find("everything") || options.find("rad") || options.find("dxf")) {
        *lp=0;
        Hoops_To_Radiance(fname,buf,options.c_str());  /*  also does DXF out  */
    }

    if(options.find("everything")||options.find("eps")||options.find("pict")||options.find("jpg")||options.find("jpeg")||options.find("pdf")) {
        *lp=0;
#ifdef linux
        Make_Image_File(g->m_ViewSeg,g_JPEG_x,g_JPEG_y,fname,options.c_str());
#else
        cout<< " Make_Image_File needs linux"<<endl;
#endif
    }
#endif
    return 1;
}

int Do_Exit(const char *lineIn,Graphic *p_g){

    /* prepare to quit */
    QStringList ll;
    Do_DSM_End(ll );
    if(!g_World) return 0;
    if (g_World->DBMirror() )
        delete g_World->DBMirror() ;
    g_World->SetDBMirror(0);
    if(g_World->DBout())
        delete g_World->DBout();
    g_World->SetDBout(0);
    if(g_World->DBin())
        delete g_World->DBin();
    g_World->SetDBin(0);
    cout<< "(broadcast)(Do_Exit) Closing down..."<<endl;

#ifdef linux
    system("rm -f  shout");
    system("rm -f  shapeold.*");
    system("rm -f  shape.0* rxshape* ");
    system("mv $R3ROOT/readfiles.txt  $R3ROOT/readfiles_2.txt");
    system("mv $R3ROOT/script.mac  $R3ROOT/last_script.mac");
#else
    system("del shout");
    system("del shapeold.*");
    system("del shape.0*");     system("del  rxshape*");
#endif


    delete g_World; g_World=0;
    CloseDown() ;
    return 0;
}
int Open_ScriptLog_File(const char*name) {

    if(name && *name) {
        if(g_SP) FCLOSE(g_SP);
        g_SP = RXFOPEN(  name,"w");
        fprintf(g_SP,"RELAX script file  <%s> \n",name);
        return ( (g_SP !=NULL) );
    }
    if(!g_SP) {
        g_SP = RXFOPEN("$R3ROOT/script.mac","w"); // feb 2015
        fprintf(g_SP,"RELAX default script file  <%s> \n","script.mac");
    }
    return ( (g_SP !=NULL) );
}

int Do_Close_Script() {
    if(g_SP) FCLOSE(g_SP);
    g_SP=NULL;
    return 1;
}

/***********************/
int Do_Delete_MatWindow( Graphic *g) {

    if(!g) return 0;
#ifdef _X
    XtUnrealizeWidget(g->topLevel); XtDestroyWidget(g->topLevel);
    MatDeleteCB((Widget) NULL,(XtPointer)g,(XtPointer) NULL);// should already have been called
#else
    assert(0);
#endif
    return 1;

}
#ifdef OPTIMISE
int Do_Initialise_Optimisation(const char*line, Graphic *g){
    QString theFile;

    std::string sline(line );
    std::vector<std::string> wds = rxparsestring(sline,RXSCRIPTSEPS,false);
    int nw = wds.size();
    if(nw>1)
        theFile=wds[1];
    else
        theFile = getfilename(0,"*.txt","Open Optimisation File",g_currentDir,PC_FILE_READ);
    if(!theFile.isEmpty()) {
        cout<< " OPT INITIALIZE MAY BE INCOMPLETE!!!!!!!!!!!\n"<<endl;
        g_Opti.Functions[0].coeffs=NULL; g_Opti.Functions[0].nc =0;
        g_Opti.Restraints=NULL; g_Opti.nres =0;
        g_Opti.History=NULL;  g_Opti.np=0	;
        g_Opti.ns=0;
#ifdef OPTIMISE
        Read_Optimisation(&g_Opti,theFile.c_str());
        Create_Soft_Constraints(&g_Opti);
#endif
        if( Open_ScriptLog_File(NULL))
            fprintf(g_SP,"Initialise Optimisation%s %s\n",RXSCRIPTSEPS, theFile.c_str());
        return 0;
    }
    return 1;
}


int Do_Load_Optimisation_History(const char*line, Graphic *g){

    std::string theFile;

    std::string sline(line );
    std::vector<std::string> wds = rxparsestring(sline,RXSCRIPTSEPS,false);
    int nw = wds.size();
    if(nw>1)
        theFile=wds[1];
    else
        theFile = getfilename(0,"*.txt","Open History File",g_currentDir,PC_FILE_READ);
    if(!theFile.empty()) {

        Read_Optimisation_History(&g_Opti, theFile.c_str());

        if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"Load Optimisation History%s %s\n",RXSCRIPTSEPS, theFile.c_str());
        return 0;
    }
    return 1;
}
#endif
int Do_Optimise(const char*line, Graphic *g){
    if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"Run Optimisation%s\n",RXSCRIPTSEPS);
#ifdef OPTIMISE
    Optimise(&g_Opti );
#endif
    return 1;
}
int Do_Drop_Sail(const char*line, Graphic *g){ // line wins over g

    SAIL *s =  PCS_Get_ScriptLine_Model(line, g) ;
    if(s)
        s->Drop();
    else
        return 0;

    if( Open_ScriptLog_File(NULL))
        fprintf(g_SP,"drop %s %s\n",RXSCRIPTSEPS, s->GetType().c_str());
    return 1;
}


ON_String Do_Delete_Entity (const char*line, Graphic *g){ // line  wins over graphic
    /*
 syntax
 delete	#[sailname]# type #[name] type and name may be comma-delimited lists.
*/
    const char *l_names=0,*l_types=0, *l_type,*l_name;
    std::string sline(line );
    std::vector<std::string> tps,nms, wds = rxparsestring(sline,RXSCRIPTSEPS,false);
    size_t nw = wds.size();
    SAIL *theSail =0;
    RXEntity_p TheEnt;
    size_t i, t ;
    ON_String rv; rv.Empty();

    if(nw>1)
        theSail = g_World->FindModel(wds[1],0);
    if(!theSail) return rv;
    if(nw>2) { l_names=wds[2].c_str(); nms = rxparsestring(wds[2],",");}
    if(nw>3) { l_types=wds[3].c_str(); tps = rxparsestring(wds[3],",");  }

    if(INTEGER_TYPE(l_names)){
        cout<<line<<" Old style delete instruction???.\n swap type and name "<<endl;
    }

    printf(" delete types = '%s' names = '%s'\n", l_types,l_names);

    int count=0;
    for(i=0;i<tps.size();i++){
        l_type = tps[i].c_str();
        if((t = INTEGER_TYPE(l_type))) {
            if( rxIsEmpty(l_names) || strieq(l_names,"*") ) {
                printf("TODO:  Delete All <%s>(%d) NOT IMPLEMENTED\n", l_type, (int) t);
                //count +=PC_Delete_All_By_Type( sd->isail,t);
            }
            else {
                for(i=0;i<nms.size();i++){
                    l_name =nms[i].c_str();
                    if((TheEnt = theSail->Entity_Exists(l_name,l_type))) {
                        printf("TODO: Step deleting %s %s\n", TheEnt->type(), TheEnt->name());
                        TheEnt->Kill();
                        count++;
                    }
                }
            }
        }
    }
    printf(" deleted %d entities \n",count);
    rv = ON_String(line);
    return rv ;
}
int Do_Save_State(QStringList &line, Graphic *p_g){ //0 is kw  1 is model, 2 is file.
    char   buf[256];
    QString  theFile;
    SAIL *s  =  PCS_Get_ScriptLine_Model(line,p_g);

    int nw = line.size();
    if(nw>2)
        theFile=line[2];
    else  {
        if(s)
            sprintf(buf,"Save State file - '%s'", s->GetType().c_str() );
        else
            sprintf(buf,"Save State file  "  );
        theFile = getfilename(0,"*.ndd",buf,g_currentDir,PC_FILE_WRITE);
    }
    if(!theFile.isEmpty()) {
        if(s) {
            s->save_state(qPrintable(theFile));
            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"Save State%s %s %s %s\n",RXSCRIPTSEPS, s->GetType().c_str(),RXSCRIPTSEPS,qPrintable(theFile));
            return 0;
        }
    }
    return 1;
}
SAIL * PCS_Get_ScriptLine_Model(const QStringList wds, const Graphic *g) { // line  wins over graphic
    SAIL *s=0;
    int nw=wds.size();
    if(nw>1)
    {
        SAIL *theSail = g_World->FindModel(wds[1].trimmed(),0);
        if(theSail)
            return theSail;
    }

    if(g && g->GSail &&  !g->GSail->GetType().empty()) {
        s= g->GSail;
    }
    return s;
}

SAIL * PCS_Get_ScriptLine_Model(const QString line, const Graphic *g) { // line  wins over graphic
    QStringList wds = line.split(QString(RXSCRIPTSEPS) );
    return PCS_Get_ScriptLine_Model(wds,g);
}


int Do_Write_Pressure (const char*line, Graphic *g){ // line  wins over graphic
    // write Pressure #sailtype# filename
    char  buf[256]; QString filename,ff;
    SAIL *s ;
    s=  PCS_Get_ScriptLine_Model(line,g) ;
    if(!s) return 0;
    std::vector<std::string>  wds = rxparsestring(std::string(line),RXSCRIPTSEPS,false);
    int nw = wds.size();
    if(nw>1)
    {
        if(nw>2)
            filename=QString(wds[2].c_str()) ;
        else
            filename=QString("pressure_out.txt");
        sprintf(buf,"Write Pressure File - '%s'", s->GetType().c_str());
        ff=getfilename(0,filename ,buf,g_currentDir,PC_FILE_WRITE);
        if(ff.isEmpty()) return 0;
        filename= ff ;
        sprintf(buf,"Write Pansail Pressures on %s with %s\n",s->GetType().c_str(),qPrintable(filename));
        s->OutputToClient(buf,1);
        cout<<" TODO: write_pressure "<<endl;
#ifdef USE_PANSAIL
        return s->WritePansailPressures( qPrintable(filename) );
#endif
    }
    return 0;
}

int Do_Edit_Script ( QStringList &pwords, Graphic *g){ // line  wins over graphic
    QString  buf;

    SAIL *s = PCS_Get_ScriptLine_Model(pwords,g)  ;
    if(s ) {
        buf = QString( "$RX_EDIT " ) + s->ScriptFile()+"  & \n";
        system( qPrintable(buf)  );
        return 1;
    }
    return 0;
}

int Do_Load_State(QStringList &pwords, Graphic *g) {
    char buf[512];
    if(pwords.size()<1) pwords<< "load state";
    if(pwords.size()<2) pwords<< " ";
    if(pwords.size()<3) pwords<< "*.ndd";
    QString &qfname =pwords.last();
    SAIL *s =  PCS_Get_ScriptLine_Model(pwords,g);
    if(!s)
        return 0;
    if(!s->IsHoisted() )
        return 0;
    if(!validfile(qfname,g_currentDir  )  ){
        sprintf(buf," Open State File - '%s'", s->GetType().c_str());
        qfname =  getfilename(0,"*.ndd",buf,g_currentDir,PC_FILE_READ);
        if(qfname.isEmpty()) return 0;

    }
    //    else
    //        qfname = pwords.last();

    s->SetFlag(FL_NEEDS_POSTP);  /* this should filter thru courtesy of the analysis*/
    s->read_ND_state(qPrintable(qfname));

    ReDraw_Nodes(s);
    g_World->PostProcessAllSails(3);
    return 1;
}

int Do_CWD(QStringList &wds,Graphic *g)
{
    if(wds.size()<=1) wds<<"*";
    QString &filename= wds[1];

    int retVal = 0 ;
    QString gcdir(g_currentDir  );
    QDir mycwd(gcdir);
    if(filename.isEmpty() || !is_directory( qPrintable(filename)) )
        filename = getfilename(0,"*","Choose WORKING Directory",g_currentDir,PC_FILE_DIR);

    if(!filename.isEmpty() ) {
        QFileInfo mfi(mycwd,filename );
        QString dirToOpen = mfi.absoluteFilePath();
        retVal = QDir::setCurrent (dirToOpen);

        strncpy(g_currentDir,qPrintable(dirToOpen),255);
        strncpy(g_workingDir,g_currentDir,255);
        Make_Directories();

        Do_RefreshSettings(wds);
    }
    return(retVal);
}
int Do_Utils_Open_In(const char *LineIn, Graphic *g)
{/*  open_input#login.csv
 or	open_input# mysql# server$user$password$database (ie localhost$root$dominique$relaxinput)
 */
    char * filename =0;
    char*lp,*what, *lineDEAD=NULL;
    char fname[256];
    int alloc=0,retVal=1;
    std::string buf;
    if(LineIn ) {
        vector<string> wds;
        wds=rxparsestring(LineIn,":\t\n!");        assert(!cout.bad());
        if(wds.size ()<2)
        { cout<< "too few words "<<wds.size()<<" in "<<LineIn<< endl; return 0;}
        retVal = 0;
        alloc = 0;
        if(wds[1]=="qt") {
            if(wds.size()<3)
                return 0;
            if (g_World->DBin() )
                delete g_World->DBin() ;

            QString ss(wds[2].c_str() );
            RXDBlinkQt *temp = new RXDBlinkQt(ss);
            if(!temp-> isOpen())   {
                delete temp; cout<<" cannot open input DB table "<<qPrintable(ss )<<endl;
                g_World->SetDBin(0);
                return 0;
            }
            g_World->SetDBin(temp);

            temp->SetType (RXDB_INPUT);

            if( g_World->DBout())
                buf="IN: "+g_World->DBin()->GetFileName() +" | OUT: "+g_World->DBout()->GetFileName();
            else
                buf="IN: "+g_World->DBin()->GetFileName() +" | OUT:(none) " ;

            SetMsgText2(buf.c_str(),1);
            retVal = 1;

            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"%s\n", LineIn);
            return retVal;
        }// qt
        //        else if(wds[1]=="mysql") {
        //#ifdef NOMYSQL
        //            cout<<" native sql driver not linked "<<endl;
        //            return 0;
        //#else
        //            if(wds.size()<3)
        //                return 0;
        //            if (g_World->DBin() )
        //                delete g_World->DBin() ;

        //            RXDBLinkMySQL *temp = new RXDBLinkMySQL();
        //            g_World->SetDBin(temp);

        //            temp->SetType (RXDB_INPUT);
        //            //            retVal=temp->Open(wds[2].c_str(),"r"); only for mirror

        //            //            if(!retVal ) {
        //            //                cout<<" failed mysql connection:"<<LineIn<<endl;
        //            //                g_World->SetSummin(0);
        //            //                return 0;
        //            //            }
        //            if( g_World->DBout())
        //                buf="IN: "+g_World->DBin()->GetFileName() +" | OUT:(mysql) "+g_World->DBout()->GetFileName();
        //            else
        //                buf="IN: "+g_World->DBin()->GetFileName() +" | OUT:(none) " ;
        //            SetMsgText2(buf.c_str(),1);
        //            retVal = 1;
        //            //     g_World->PostAllDBInputs();
        //            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"%s\n", LineIn);
        //            return retVal;
        //#endif
        //        }// mysql

        if(LineIn)
            lineDEAD = STRDUP( LineIn);
        printf(" Open_In on <%s>\n",lineDEAD);     assert(!cout.bad());
        if(lineDEAD) {
            what = strtok(lineDEAD,":");
            if(what) {
                filename = strtok(NULL,":!\n\0");
                PC_Strip_Leading(filename);        assert(!cout.bad());
                PC_Strip_Trailing(filename);        assert(!cout.bad());
            }
        }
    }//linein
    if(filename && validfile(filename,g_currentDir) !=OK_TO_READ ) {
        QString tt = getfilename(0,"*.csv","Open Log File as Input",g_currentDir,PC_FILE_READ);
        filename = STRDUP(qPrintable(tt) );
        alloc = 1;        assert(!cout.bad());
    }
    assert(!cout.bad());
    if(filename) {
        strcpy(fname,filename);        assert(!cout.bad());
        if(alloc)
            RXFREE(filename);
        PC_Strip_Leading(fname);        assert(!cout.bad());
        PC_Strip_Trailing(fname);        assert(!cout.bad());
        lp = strchr(fname,'.');        assert(!cout.bad());
        if(!lp)
            strcat(fname,".csv");
        if(g_World->DBin()) delete g_World->DBin() ;        assert(!cout.bad());
        g_World->SetDBin(new RXLogFile()) ;        assert(!cout.bad());
        g_World->DBin()->SetType (RXDB_INPUT);        assert(!cout.bad());
        g_World->DBin()->Open(fname,"r");        assert(!cout.bad());

        if(g_World->DBout())
            buf="IN: "+g_World->DBin()->GetFileName()+" | OUT: "+g_World->DBout()->GetFileName();
        else
            buf="IN: "+g_World->DBin()->GetFileName()+" | OUT: (none) ";
        assert(!cout.bad());
        SetMsgText2(buf.c_str(),1);
        if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"open_input: %s\n", fname);
    }
    if(lineDEAD) RXFREE(lineDEAD);
    assert(!cout.bad());
    return(retVal);
}
int Do_Open_Mirror ( QStringList &wds)
{
    //"open_mirror	 internal"
    int nw = wds.size();
    if(nw<2)
        return 0;
    if(wds[1].contains("internal")) {
        if (g_World->DBMirror() )
            delete g_World->DBMirror() ;
        RXMirrorDBI *temp = new RXMirrorSimple();
        g_World->SetDBMirror(temp);
        g_World->PostAllDBInputs();
        return 1;
    }
    else {
        if (g_World->DBMirror() )
            delete g_World->DBMirror() ;
        RXMirrorQt *temp = new RXMirrorQt(wds.back());//sept 29 was 1
        if(!temp->m_db.isOpen())   {
            delete temp; cout<<" cannot open mirror "<<qPrintable(wds.back() )<<endl;
            g_World->SetDBMirror(0);
            RXMirrorDBI *mirrDB = new RXMirrorSimple();
            g_World->SetDBMirror(mirrDB);
            g_World->PostAllDBInputs();
            return 0;
        }
        g_World->SetDBMirror(temp);
        temp->Open();//qPrintable(wds[1]) ,"w");
        cout<<"open mirror OK "<<qPrintable(wds.back() )<<endl;
        g_World->PostAllDBInputs();
        return 1;
    }
    return 0;
}

int Do_Utils_Open_Summary(const char *lineIn, Graphic *gin)
{
    // open_log :mysql:server$user$password$database (ie open_logfile:mysql:localhost$root$dominique$relaxinput
    // or open_logfile: r2sum.csv
    char *lp ;
    QString qfilename;
    std::string buf;
    char fname[256],line[512];
    int retVal = 0 ;
    if(lineIn ) {

        QStringList qwds = rxqparsestring(lineIn, QRegExp("[:\t\n!]") );
        //        vector<string> wdss;
        //        wdss=rxparsestring(lineIn,":\t\n!");
        if(qwds.size ()<2)
            return 0;
        qfilename = qwds[1];
        if(qwds.at(1).startsWith("qt")) {
            if(qwds.size()<3)
                return 0;
            if (g_World->DBout() )
                delete g_World->DBout() ;

            QString &ss = qwds[2];
            RXDBlinkQt *temp ;
            if(qwds[1]=="qt")
                temp = new RXDBlinkQt(ss);
            else if(qwds[1]=="qtlong")
                temp = new RXDBlinkLongQt(ss);
            else
            {
                cout<<"open_log keyword '" <<qPrintable(qwds[1] )<<"' not understood ('qt' or 'qtlong')"<<endl;
                return 0;
            }
            if(!temp->isOpen())   {
                delete temp; cout<<" cannot open output DB "<<qPrintable(ss)<<endl;
                g_World->SetDBout(0);
                return 0;
            }
            g_World->SetDBout(temp);

            temp->SetType (RXDB_OUTPUT);
            retVal=temp->Open(qPrintable(qwds[2]),"w");

            if( g_World->DBin())
                buf="IN: "+g_World->DBin()->GetFileName() +" | OUT:(mysql) "+ qwds[2].toStdString()  ;
            else
                buf="IN: none | OUT:(QtDriver) "+qwds[2].toStdString()  ;
            SetMsgText2(buf.c_str(),1);
            retVal = 1;
            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"%s\n", lineIn);
            return retVal;
        }// qt
        else if(qwds[1]=="mysql") {
            //#ifdef NOMYSQL
            cout<<" native sql driver not linked "<<endl;
            return 0;
            //#else
            //            if(wds.size()<3)
            //                return 0;
            //            if (g_World->DBMirror() )
            //                delete g_World->DBMirror() ;
            //            if (g_World->DBout() )
            //                delete g_World->DBout() ;

            //            RXDBLinkMySQL *temp = new RXDBLinkMySQL();
            //            g_World->SetDBout(temp);
            //            //  g_World->SetMirror(temp);

            //            temp->SetType (RXDB_OUTPUT);//+RXDB_MIRROR);
            //            retVal=temp->Open(wds[2].c_str(),"w");

            //            if(!retVal ) {
            //                cout<<" failed mysql connection:"<<lineIn<<endl;
            //                delete g_World->DBout();
            //                g_World->SetDBout(0);
            //                //   g_World->SetMirror(0);
            //                return 0;
            //            }

            //            if( g_World->DBin())
            //                buf="IN: "+g_World->DBin()->GetFileName() +" | OUT:(mysql) "+wds[2];
            //            else
            //                buf="IN: none | OUT:(mysql) "+wds[2];
            //            SetMsgText2(buf.c_str(),1);
            //            retVal = 1;
            //            //    g_World->PostAllDBInputs(); a mirror thing
            //            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"%s\n", lineIn);
            //            return retVal;
            //#endif
        }// mysql

        strcpy(line,lineIn);

    }//linein
    if( validfile(qfilename,g_currentDir)==-1) {
        QString ff = getfilename(0,"*.csv","Open Log File",g_currentDir,PC_FILE_READ);
        qfilename  = ff ;
    }

    if(!qfilename.isEmpty()) {
        strcpy(fname,qPrintable(qfilename));

        PC_Strip_Leading(fname);
        PC_Strip_Trailing(fname);
        lp = strchr(fname,'.');
        if(!lp)
            strcat(fname,".csv");
        if(SDB) printf(" Do_Utils_Open_Summary file= <%s>\n",fname);
        if (g_World->DBout() )
            delete g_World->DBout() ;
        class RXLogFile *temp = new RXLogFile() ;
        g_World->SetDBout(temp);
        g_World->SetDBMirror(temp);
        g_World->DBout()->SetType (RXDB_OUTPUT);
        g_World->DBout()->Open(fname,"w");
        g_World->PostAllDBInputs();
        if( g_World->DBin())
            buf="IN: "+g_World->DBin()->GetFileName()+" | OUT: "+g_World->DBout()->GetFileName();
        else
            buf= "IN: none | OUT: "+ g_World->DBout()->GetFileName();
        SetMsgText2(buf.c_str(),1);
        retVal = 1;
        if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"open_log : %s\n", fname);
    }//filename
    return(retVal);
}


int Do_Export_Panels(const char *lineIn, Graphic *g)
{
    /*  expects either NULL or a line of the form
 *  export panels # spacing (float) [ # filename ]
 */

    char *filename,*lp;
    char *what, *line=NULL;
    char fname[256];
    double space =  0.2;  /* default spacing */

    int retVal = 0;
    int alloc = 0;
    if(lineIn) line=STRDUP(lineIn);
    filename = NULL;

    if(line) {
        what = strtok(line,RXSCRIPTSEPS);
        if(what) {
            lp = strtok(NULL,RXSCRIPTSEPS);
            if(lp) {
                space = atof(lp);
                if(space <= 1.0e-6)
                    space =   0.2;
                filename = strtok(NULL,RXSCRIPTSEPS);
            }
        }
    }
    if(!filename || rxIsEmpty(filename)) {
        QString ff=  getfilename(0,"*","PANELLIST Directory",g_currentDir,PC_FILE_WRITE);
        filename = STRDUP(qPrintable(ff));
        alloc = 1;
    }

    if(filename) {
        char buf[256];
        strcpy(fname,filename);
        if(alloc)
            RXFREE(filename);
        PC_Strip_Leading(fname);
        PC_Strip_Trailing(fname);
        sprintf(buf,"%snew",fname);
        /*  PanelOut(buf,space); */

        g_World->OutputToClient("   To_Panellist(fname,space)",2 );
        strcat(fname,".dxf");
        g_World->OutputToClient("DXFout(fname);",2);

        retVal = 1;
        if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"Export Panels%s %s\n",RXSCRIPTSEPS, fname);
    }
    if(line) RXFREE(line);
    return(retVal);
}
int  Do_Post_As_Blob (QStringList &pwords )
{
    int retVal=1;
    if(pwords.count()<3)
        return 0;
    const QString &label=pwords.value(1);
    QString fname=pwords.value(2);

    g_World->AbsoluteFileName(fname);
    QFile  f(fname);
    if(!f.exists()) {
        qDebug()<<" blobbing file "<<fname << "but its not readable";
        return 0;
    }
    if(g_World->DBMirror())
        retVal=g_World->DBMirror()->PostFile(label,fname);// in fact just post_summary_value
    return(retVal);
}
int Do_Post_Data(const char *linein,Graphic *g){
    // format post:what: data)

    char line[1024];
    strncpy(line,linein,1023);

    if (!g_World->DBMirror() )
        return 0;
    char *what=NULL, *data=NULL, *name=NULL;

    name = strtok(line,":");
    if(name) what = strtok(NULL,":!\n\r\0");
    if(what) data = strtok(NULL,":!\n\r\0");
    if(!data) {
        printf(" POST didnt parse (%s)",line); return 0;}
    printf(" POST <%s>  <%s>\n", what,data);
    if(rxIsEmpty(what)) return 0;
    assert( g_World->DBMirror() );
    g_World->DBMirror()->post_summary_value( what,data);

    return g_World->DBMirror()->Apply_Summary_Values();

}

int  Do_Apply(QStringList &w){
    // apply : variable: value [:type [:flag [:value]]] // only for CSV-variables
    int iret=0, nw=0;
    int flag=-1;

    if(!w.size()) { cout<<"(Apply) line is empty so do nothing "<<endl; return 0;}
    if(!g_World ) {cout<<"No World (closing down) "<<endl; return 0;}
    if(!g_World->DBMirror() ) {cout<<"(Apply) No Mirror is open so do nothing "<<endl; return 0;}

    nw=w.size();
    if(nw<3) {
        cout<<"syntax is :apply <tab> variable <tab>  value [<tab> type [<tab> flag [<tab> value]]]  "<<endl;
        cout<<" input contains "<<nw<<" words"<<endl;
        for(int i=0;i<nw;i++) cout<<i<<"  "<< qPrintable(w[i])<<endl;
        return 0;
    }
    if(w[1].isEmpty() ) return 0;
    int type= RXMirrorDBI::Summary_Type(qPrintable(w[1]),&flag);

    iret=Justapply_one_summary_item(qPrintable(w[1] ), qPrintable(w[2] ), type, flag ) ;
    return 1;// the return value of JustApply is in iret ;
}


int Do_Utils_Close_Summary(const char *line, Graphic *g)
{
    int retVal = 0;
    if(!g_World->DBMirror() && !g_World->DBout()  ) return 0;
    if(!g_World->OutputToClient("Do You really want to CLOSE\n the output logs ?",4))
    {
        char buf[256];
        if(g_World->DBMirror())  g_World->DBMirror()->Close( );
        if(g_World->DBout())  g_World->DBout()->Close();
        if( g_World->DBin() )
            sprintf(buf,"IN: %s | OUT: NONE",g_World->DBin()->GetFileName().c_str());
        else
            sprintf(buf,"IN: NONE | OUT: NONE");
        SetMsgText2(buf,1);
        retVal = 1;
    }


    return(retVal);
}

int Do_Utils_Close_SummaryIn(const char *line, Graphic *g)
{
    int retVal = 0;
    if(!g_World->DBin() ) return 0;
    if(!g_World->OutputToClient("Do You really want to CLOSE\n the Input log file ?",4)) {
        char buf[256];
        g_World->DBin()->Close( );
        if(g_World->DBout())
            sprintf(buf,"IN: None | OUT: %s",g_World->DBout()->GetFileName().c_str());
        else
            sprintf(buf,"IN: None | OUT: noNe");
        SetMsgText2(buf,1);
        retVal = 1;
    }

    return(retVal);
}

int Do_Utils_Choose_Line(const char *lineIn, Graphic *g){//leak
    // syntax
    // choose_line: lineno : runToEndFlag
    // runToEndFlag non-zero means run all the remaining lines in the table.
    // NB we interpret '1' as the table's first line
    // even though the 'id' field in the DB starts at 0.

    char  *line=NULL;
    int  retVal = 0;
    int line_no = -1;
    int run_to_end = 0;
    char *lp;
    ch_data_t *dd;

    if(lineIn) line = STRDUP(lineIn);
    dd = &g_CH_Run_Data;

    dd->N = 0;
    dd->run_to_line = 0;
    dd->chosen = 0;
    dd->sum = g_World->DBin();
    if(! g_World->DBin() ) return 0;
    if(g_World->DBin()->GetFileName().empty() ) { if(line) RXFREE(line); return 0;}

    dd->m_label = g_World->DBin()->GetFileName();
    g_World->DBin()->Extract_Column(g_World->DBin()->GetFileName().c_str(),"Run_Name",&(dd->list),&(dd->N));
    printf("There are %d records in <%s>\n ", dd->N,  g_World->DBin()->GetFileName().c_str() );

    if(dd->N) { // some records were found.
        if(line) { // else its OK to call with a null line, just to put up a dialog
            lp = strtok(line,RXSCRIPTSEPS);
            if(lp)  lp = strtok(NULL,RXSCRIPTSEPS);
            if(lp) {
                PC_Strip_Leading(lp);
                PC_Strip_Trailing(lp);
            }
            if(lp && *lp) {
                line_no = atoi(lp);
                if(line_no >= dd->N)
                    line_no = dd->N - 1;
                lp = strtok(NULL,RXSCRIPTSEPS);//":!\n\0");
                if(lp)
                    run_to_end = atoi(lp);
                if(run_to_end)
                    dd->run_to_line = dd->N;
                if(line_no >= 0) {
                    dd->chosen = line_no;
                    cout<<"Selected line "<< dd->chosen<< " in datafile '"<<dd->m_label <<"'"<<endl;
                    cout<<"This is named '"<< dd->list[dd->chosen]<<"'"<<endl;

                    g_World->DBin()->read_and_post_one_row(dd->chosen, QString( dd->list[dd->chosen] ),g_World->DBMirror() );
                    if( g_World->DBMirror() )
                        g_World->DBMirror()->Apply_Summary_Values();

                    fprintf(g_SP,"choose_line: %d : %d\n",dd->chosen,(dd->run_to_line > dd->chosen) );
                    cout <<endl;
                }
            }
        }
        else { // the procedure was called with null 'line'- so we put up a dialog
            cout <<"no menu without X"<<endl;
            // should do something like:
            //            QString ee = rxWorker::UserChoice(prompt, candidates) ;// except that we need a'runtoend' checkbox. Or we allow multiple choices
            //            if(!ee .isEmpty()) ....
        }
    } // else there are no records named 'Run_Name' in the table

    if( dd->chosen && Open_ScriptLog_File(NULL)) {
        fprintf(g_SP,"choose_line%s %d %s %d\n",RXSCRIPTSEPS,dd->chosen,RXSCRIPTSEPS,run_to_end );
        /* dodgy	g_summout.Last_Line_Accessed= dd->chosen;*/
    }
    if(line) RXFREE(line);
    return retVal;
}

int Do_Draw_Fanned(const char *linein, Graphic *g){
    // line is 'keyword #  luff'

    char *name=NULL,*what;
    char *line = NULL;
    if(linein && *linein) line = STRDUP(linein);

    if(g->GSail)
        printf(" Do_Draw_Fanned on model  '%s'  ",g->GSail->GetType().c_str());
    else cout<< " NULL Gsail"<<endl;

    if(line) {
        what = strtok(line,":");
        if(what) {
            if((name = strtok(NULL,RXSCRIPTSEPS ))){//  ":!\n\0"

                PC_Strip_Leading(name);
                PC_Strip_Trailing(name);
                Draw_Edge_Curves(name,g->GSail);
            }
        }
    }
    RXFREE(line);
    return 1;
}
int Do_Export_IGES(const char *linein, Graphic *gin){
    // line is 'igesout # kite # $RWD/run_name.igs # 21 # 21# 3 '

    int  nu=12,nv=12,  deg=3;
    SAIL *sail  =  PCS_Get_ScriptLine_Model(linein,gin);
    if(!sail) return 0;
    string sline; if (linein) sline=string(linein);
    vector<string> wds = rxparsestring(sline,RXSCRIPTSEPS); int nw=wds.size();
    if(nw<3) return 0;
    const char *fname=wds[2].c_str();
    if(nw>3) nv= nu  = atoi(wds[3].c_str());
    if(nw>4) nv  = atoi(wds[4].c_str());
    if(nw>5) deg = atoi(wds[5].c_str());

    if(nu!=nv) { nu = max(nu,nv); nv=nu;}
    sail->Write_Iges(fname, nu,nv,deg) ;

    return 1;
}


int Read_Script(const char *pfilename,const Graphic *g,  class QObject *qobj ,const bool PleaseLog )
{
    FILE *script;
    int retVal=0;
    char fname[512];  *fname=0;
    int fNameOK =0;

    if(pfilename && *pfilename )
    if(validfile(pfilename)==OK_TO_READ )
        {
            fNameOK=1;
            strncpy(fname,pfilename,511);
        }


    if(!fNameOK) {
        QString qlp = getfilename(NULL,"*.mac","OFSI Script?",g_workingDir,PC_FILE_READ);
        if(!qlp.isEmpty()) {
            strncpy(fname,qPrintable(qlp),511); // valgrind
        }
    }

    if(rxIsEmpty(fname)) return 0;

    if((script = RXFOPEN((char*) fname,"r"))) {
        if(PleaseLog)
            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"! Macro %s %s\n",RXSCRIPTSEPS, fname);
        QFileInfo fi(fname);
        QDir sfdir= fi.absoluteDir();
        QString sfdname = sfdir.absolutePath();
        strncpy(g_currentDir,qPrintable(sfdname),255); // if windows change '/' to '\'
        strncpy(g_workingDir,g_currentDir,255);
        FCLOSE(script);// valgrind says get rid of the FCLOSE
        QFile inscript(fname);
        if(inscript.open(QIODevice::ReadOnly )){
            QTextStream in(&inscript );
            retVal = Process_Script(in,g,qobj);
            in.flush(); // maybe valgrind likes this
            inscript.close();
        }
    }
    cout<<" Finish reading script   "<< fname<<endl;
    setStatusBar(QString("Finish reading script '") + QString(fname) + QString("'"));
    if(PleaseLog)
        if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"! ENDMacro - %s\n", fname);// Just a comment in the log
    return(retVal);
}

int Do_Open(QStringList &pwords, Graphic *gIn) // boat,sail,bag,camera
{
    Graphic *gnew;

    int retVal=0;
    assert(pwords.size()>0) ;
    if(pwords.size()<2)
        pwords<<"*.*";

    QString &fw=pwords[0];
    QString &qfname = pwords[1];

    // fname is *.in. or  May be an absolute path or relative to g_currentDir
    if(fw.contains("boat")) {
        if(qfname.isEmpty()) qfname="*.in";
        if(validfile(qfname,g_currentDir)==1 ) {
            gnew=  new RXSubWindowHelperTopDesign();
            ReadBoat(qfname,gnew);
            retVal=1;
        }
        else { qfname = getfilename(0,qfname,"Open Boat File",g_boatDir,PC_FILE_READ);
            if(!qfname.isEmpty()) {
                gnew=  new RXSubWindowHelperTopDesign();
                ReadBoat( qfname ,gnew);
                retVal=1;//ON_String("open boat RXSCRIPTSEPS ") + ON_String ( fname.c_str());
            }
        }
    }
    else if(fw.contains("sail")) {
        if(qfname.isEmpty()) qfname="*.in";
        if(validfile(qfname ,g_currentDir) ==1 ) {

            gnew  = new RXSubWindowHelperDesign();// OpenDesWindow();

            ReadScript(gnew,qfname ,0);
            Do_Resize("anything",gnew);
            retVal=1;//ON_String("!open sail RXSCRIPTSEPS") + ON_String ( fname.c_str());
        }
        else{
            qfname= getfilename(0,qfname, "Open Sail",g_currentDir,PC_FILE_READ);
            if( !qfname.isEmpty()) {
                gnew  = new RXSubWindowHelperDesign();// OpenDesWindow();
                ReadScript(gnew,qfname,0);
                Do_Resize("anything",gnew);
                retVal=1;//ON_String("!open sail RXSCRIPTSEPS ") + ON_String ( fname.c_str());
            }
        }
    }
    else if(fw.contains("bag")) {
        if(qfname.isEmpty())  qfname="*.bag" ;

        if(validfile( qfname ,g_currentDir) == 1 ) {
            int err=0;
            gnew = OpenBag(qfname,&err);
            g_World->Bring_All_Models_To_Date();
#ifdef _X
            UpdateMenu();
            SetCameraList();
#endif
            Do_Resize("nothing",gnew);
            retVal=1;//ON_String("!open bag RXSCRIPTSEPS ") + ON_String ( qPrintable(qfname));
        }
        else {
            qfname = getfilename(0,qfname,"Open Sail(.bag) File",g_currentDir,PC_FILE_READ);
            if(!qfname.isEmpty()) {
                int err=0;
                gnew = OpenBag( qfname ,&err);

                g_World->Bring_All_Models_To_Date();
#ifdef _X
                UpdateMenu();
                SetCameraList();
#endif
                Do_Resize("nothing",gnew);
                retVal=1;//ON_String("!open bag RXSCRIPTSEPS ") + ON_String ( fname.c_str());
            }
        }
    }

    else if(fw.contains("camera"))
        OpenView(pwords,0);

    return(retVal);
}


int Do_Open_View( QStringList &pwords)
{
    OpenView(pwords,0);
    return(1);
}


int Do_Solve(const char *line, SAIL *s)   {
    HC_KEY segkey=0;
    int rc=0;
    if(!s )
        return(0);
    if(!s->GetFlag(FL_HASGEOMETRY)) {
        cout<< "(Do_Solve) but flags say no_geom on "<<s->GetType().c_str()<< endl;
        //   return(0);
    }
#ifdef HOOPS
    segkey = s->Graphic()->m_ModelSeg;
    if(!segkey) {
        cout<< "(Do_Solve) no segkey \n"<<endl;
        return(0);
    }
#endif
    setStatusBar( QString("Solving(1)..." ) +QString(s->GetType().c_str() ));
    HC_Open_Segment_By_Key(segkey);
    HC_Open_Segment("data");
    int oldflag =  s->ClearFlag(FL_NEEDS_GAUSSING ); // WHY??
    rc=s->Pre_Process(NULL);
    HC_Close_Segment();
    HC_Close_Segment();
    if(!rc)
    {
        s->SetFlag(FL_NEEDS_GAUSSING );
        return 0;
    }
    setStatusBar("Solve Done");
    s->SetFlag(FL_HASPANELS,1);
    s->ClearFlag(FL_NEEDS_GAUSSING );
    // s->AddToSummary(); // this propagates any edits to the log DB BUT may cause a flipflop while wildcard trimming.";
    return(rc);
}

int Do_Solve(const char *line, Graphic *g)
{
    int rc;
    HC_KEY segkey;
#ifdef _DEBUG
    if(g) g->Print_Graphic("Do_Solve");
#endif
    if(g == NULL) return(0);
    if(!g->GSail->GetFlag(FL_HASGEOMETRY) ||g->GSail->GetFlag(FL_NEEDS_GAUSSING)  ) {
        cout<< "(Do SOlve) flags say not needed"<<endl;
        return(0);
    }
    segkey = g->m_ModelSeg;
    if(!segkey) {
        cout<< "no segkey \n"<<endl;
        return(0);
    }
    std::string buf("Solving(2)..."); buf+=g->GSail->GetType();
    setStatusBar( buf.c_str());

    HC_Open_Segment_By_Key(segkey);
    HC_Open_Segment("data");
    /* calls display_panels again at this stage */
    rc=g->GSail->Pre_Process(NULL);
    HC_Close_Segment();
    HC_Close_Segment();
    if(!rc)
        return 0;
    setStatusBar("Solve Done");
    g->GSail->SetFlag(FL_HASPANELS,1);
    g->GSail->ClearFlag(FL_NEEDS_GAUSSING );
    // g->GSail->AddToSummary(); // this propagates any edits to the log DB but beware of a flip-flop
    // if a post: has come from elsewhere. SO we move it to Hoist and to Open Mirror
    return(1);
}


int Do_Zoom(const char *p_line,Graphic *p_g){
    //  parse to 'zoom:window(sail): factor'
    int iret=0;
#ifdef HOOPS
    char **words = NULL;
    int   nw=0;
    double l_Fac = 0.5;
    char *Buf=NULL;

    if(!p_line) return 0;
    if(!p_g) return 0;

    Buf = STRDUP(p_line);
    nw = make_into_words(Buf,&words,RXSCRIPTSEPS);
    if(nw)								// else 0.5
        l_Fac = atof(words[nw-1]);

    iret = PCWin_Zoom(p_g, l_Fac);

    if(words) RXFREE(words);
    if(Buf) RXFREE(Buf);
#endif
    return iret;
}
int Do_Resize(const char *LineIn,Graphic *g) // Linein may include the word 'reset'
{
#ifdef HOOPS
    HC_KEY segkey;
    //float x,y,z;

    if(!g) {
        cout<< "NULL graph Do_Resize!\n"<<endl;
        return(0);
    }
    segkey = g->m_ViewSeg;
    if(!segkey)
        return(0);

    HC_Open_Segment_By_Key(segkey);
    if(LineIn && stristr((char *)LineIn,"reset")) {
        HC_Flush_Contents("(.,*,...)","camera");
        HC_Set_Camera_Projection("ortho");
        HC_Set_Camera_Position(0,-50,15);
        HC_Set_Camera_Target(0,0,15);
        HC_Set_Camera_Up_Vector(0,0,1);
        HC_Set_Camera_Field(20,30);


        if(g->axisSeg) {
            HC_Open_Segment_By_Key(g->axisSeg); // see AttachHoops.c
            HC_Set_Camera_Position(0,-10,0);
            HC_Set_Camera_Projection("orthographic");
            HC_Set_Camera_Target(0.5,0.5,0.5);
            HC_Set_Camera_Up_Vector(0,0,1);
            HC_Set_Camera_Field(2.2,2.2);
            HC_Close_Segment();
        }
    }
    Record_Default_Camera();

#ifdef _X
    g->m_Phoops_widget->hoops.m_pHView->SetProjMode(ProjOrthographic);
    g->m_Phoops_widget->hoops.m_pHView->FitWorld();
#else
    assert(0);
#endif
    HC_Close_Segment();
    HC_Update_Display();
#endif
    return(1);
}
int Do_OneSetting  (QStringList &line )
// setting : DefaultFilelabel : DefaultFileValue

{
    int rc=0 ; bool ok=0;
    if(line.length()<2) {
        cout<<"settings " <<endl;
        cout<<"Set one variable as in the DefaultsFile (see settings edit)"<<endl;
        cout<<"Example:: "<<endl;
        cout<<"setting: Convergence_Limit : 0.1 "<<endl;
    }
    QStringList ll = line.mid(1);
    qDebug()<< "Setting with "<<ll;
    return g_World->qGetOneSettings(ll,1) ;


    if(!ok) {

    }

    return rc;
}
int Do_RunLoop (QStringList &line )
// loop : postableLabel: startval: endval:inc[:postableLabel: startval: endval:inc [ etc...]  ]
// do a series of analyses, incrementing a database variable each time
// the series can be nested: the last loop specified is the innermost

{
    int rc=0 ; bool ok=0;
    if  (relaxflagcommon.Stop_Running )
        return 1;
    if(0==line.value(0).compare( "loop",Qt::CaseInsensitive )  )
    {
        QStringList ll = line.mid(1);
        return Do_RunLoop(ll);
    }
    if(!line.length()){

        return Do_Calculate(" ",0);//RXSubWindowHelper::rootgraphic);
    }
    if(line.length()>3) {
        ok=1;
        QString what =  line.value(0);
        double end, inc , start = line.value(1).toDouble(&ok);
        if(ok) {
            end = line.value(2).toDouble(&ok);
            if(ok) {
                inc = line.value(3).toDouble(&ok);
                if(ok) {
                    for (double v = start; v<=end; v+=inc)
                    {
                        if  (relaxflagcommon.Stop_Running )
                            return rc;
                        QStringList ll;
                        //            ll<<"post"<<what<<QString::number(v);    qDebug()<<"(LOOP) Executing  '"<< ll.join(":")<<"'";
                        //            Execute_Script_Line(ll.join(":"),0);
                        ll<<"apply"<<what<<QString::number(v);    qDebug()<<"(LOOP) Executing  '"<< ll.join(":")<<"'";
                        Do_Apply(ll);
                        ll =  line.mid(4);
                        rc+=Do_RunLoop(ll);
                    }
                }}}
    }
    if(!ok) {
        cout<<" loop " <<endl;
        cout<< "do a series of analyses, incrementing a database variable each time"<<endl;
        cout<< "the series can be nested: the last loop given is the innermost "  <<endl;
        cout<< " without arguments, this command is equivalent to 'calculate'"<<endl;
        cout<<" Syntax is "<<endl;
        cout<< "loop : APPLYableLabel: startval: endval:inc[:postableLabel: startval: endval:inc [ etc...]  ]"<<endl;


    }

    return rc;
}
int Do_Calculate(const char *linein,Graphic *g) {

    char buf[256];
    fflush(stdout);

    g_World->Post_Wind_Details(); // updates apparent wind values for Werner
    if(linein) { /* it must be a script command */
        QString qline(linein) ;// for debug
        if(stristr(linein,"stop") ) {
            relaxflagcommon.Stop_Running = 1;
            return 1;
        }
        if(stristr(linein,"off") ) {
            g_World->SetAutoCalc(0);
            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"CALCULATE%s autorun=OFF\n",RXSCRIPTSEPS);
            cout<< "calculate - autorun=OFF"<<endl;
            return 1;
        }
        if(stristr(linein,"auto") ) {
            g_World->SetAutoCalc(1);
            if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"CALCULATE%s AUTOrun=on\n",RXSCRIPTSEPS);
            cout<< "calculate - AUTOrun=on"<<endl;

            return 1;
        }
    }

    relaxflagcommon.Stop_Running = 0;
    sprintf(buf,"Running %s...",g_World->GetRunName());
    setStatusBar(buf);
    g_World->CalcNow();

    setStatusBar("Analysis Done");
    if( Open_ScriptLog_File(NULL)) fprintf(g_SP,"Calculate\n");
    fflush(stdout);
    return(1);
}

int Do_SaveAs(QStringList &words,Graphic *Gin) // Mar 2003 reworked as the GP save routine
{
    Graphic *g = Gin;
    QString  title;
    int SaveGeneratedToo=0;
    // syntax
    // saveas # genoa # /home/relax/genoas/*.bag # (gentoo)
    // and g takes precedence over the word 'genoa'  because there may be more than one
    // Word(2) takes precedence over g->filename.

    //  so Do-SaveAs(NULL,g) is the same as 'Save'
    //  and if the input line is complete, no G is necessary,
    //  but we might save the wrong one.

    // step 1.  parse the input line
    // step 2  determine which graphic
    // step 3  determine the filename. take from line if poss otherwise from g.  Ask user if ambiguous
    // step 4  ask User if filename is ambiguous
    // step 5) Do a SaveSail

    if (words.size()<1) words<<"saveas";
    if (words.size()<2) words<<"WhichModel";
    if (words.size()<3) words<<"*.bag";
    QString &qfname = words[2];
    if (words.size()>3)
        if(words[3].contains("gen"))
            SaveGeneratedToo = 1;
    SAIL *s =  PCS_Get_ScriptLine_Model(words, g) ;
    if(s)
        g = s-> GetGraphic();

    if(validfile(qfname,g_currentDir   ) == BAD_FILENAME) {
        title=" Write '"+words[1]+"'";
        qfname =getfilename(0,qfname,title,g_currentDir,PC_FILE_WRITE);
    }

    if(validfile(qfname) != BAD_FILENAME)  { // the file is OK
        g->m_qfilename= qfname ;//  make the extension '.bag.'
        if(s && (s->IsBoat()))
            SaveSail(g,WT_BOATFILE,SaveGeneratedToo); // to  g->m_qfilename
        else
            SaveSail(g,WT_DESIGN,SaveGeneratedToo);
        return(1);
    }
    return(0);
}
/* Logic:  if bagname is given it primes over g unless g is null */
int Do_Hoist(QStringList &wds,Graphic *g){ // kw, bagname, [prompt I think]
    Graphic *gnew;
    int err;

    if(wds.size()<2) { if(g) wds<< g->m_qfilename;  else wds<<"*.bag";  }
    QString &qBagName = wds[1];

    if(qBagName.isEmpty()) { // there's a good chance it hasnt been  bagged yet
        g_World->OutputToClient("Save Bagged and then hoist!",2);
        return 0;
    }

    if(validfile(qBagName,g_currentDir) != OK_TO_READ) {
        qBagName = getfilename(0,"*.bag","Open Sail(.bag) File",g_currentDir,PC_FILE_READ);
    }
    if(!qBagName.isEmpty() && validfile(qBagName,g_currentDir) == OK_TO_READ ) {
        gnew = OpenBag(qBagName,&err);
        if(err == 0)
        {
            assert(gnew->GSail) ;
            gnew->GSail->Hoist();
        }
        else
            return 0;

        mkGFlag(GLOBAL_DO_TRANSFORMS); // december shouldnt be necessary
        g_World->Bring_All_Models_To_Date();
        Do_Resize("nothing",gnew);

        return 1;
    }

    return(0);
}

int Do_Constant_Pressure(const char *linein,Graphic *g)
{
    char *what=NULL, *name=NULL;
    double ff;
    SAIL *s;
    char *line=STRDUP(linein);
    cout<< "  Do_Constant_Pressure Feb 2007"<<endl;

    if(line) name = strtok(line,RXSCRIPTSEPS);
    if(name) name = strtok(NULL,RXSCRIPTSEPS);// ":!\n\r\0");
    if(name) {
        what = strtok(NULL,RXSCRIPTSEPS);// ":!\n\r\0");
        PC_Strip_Leading(name);
        PC_Strip_Trailing(name);
    }

    if(what) {
        ff = atof(what);
        s = g_World->FindModel(name);
        if(s)
            s->SetOneConstantPressure(ff);
    }
    RXFREE(line);
    return(1);
}

int Do_Snapshot(const char *linein,Graphic *g){
    // format snapshot # filename)
    assert(!cout.bad());
    vector<string> w;
    string Buf(linein);
    w = rxparsestring(Buf,RXSCRIPTSEPS);
    size_t nw=w.size(); w.push_back (string(""));
    QString qfname;
    if(nw>1) qfname=QString(w[1].c_str());

    if(!qfname.isEmpty())
        if(validfile(qfname) == OK_TO_READ ) {
            return(RXSubWindowHelper::snapshot(qfname));
        }
    qfname=getfilename(0,"*.mac","Snapshot Macro?",g_currentDir,PC_FILE_WRITE);
    if(!qfname.isEmpty())
        return(RXSubWindowHelper::snapshot(qfname));

    return 0;
}

int Do_Get_Data(const char *linein,Graphic *g){

    /*getdata  what  [server|client|file]  if the destination is blank we use stdout
where 'what' is a CSV variable name
You must immediately start listening for a reply.
*/
    char value[256]; int iret;
    if (!g_World->DBMirror() ){
        cout<<" getdata fails. It needs a logfile  to be open"<<endl;
        return 0;
    }
    iret=0;
    vector<string> w;
    string Buf(linein); string b;
    w = rxparsestring(Buf,RXSCRIPTSEPS);
    int nw=w.size(); w.push_back (string(""));
    if(nw<2) {
        cout<<" getdata didnt parse"<<endl;
        cout<<"syntax is :"<<endl<<"getdata <tab> what <tab> [server|client|file]"<<endl;
        cout<<"where 'what' is a CSV variable name "<<endl;
        cout<<"'server' means use existing open socket server "<<endl;
        cout<<"'client' means use existing open socket client "<<endl;
        cout<<"or else the 3rd word is interpretd as a filename"<<endl;
        return 0;
    }


    cout<<" getdata <"<<w[1]<<">  <"<<w[2]<<"> "<<endl;

    iret = g_World->DBMirror()->Extract_One_Text( w[1].c_str() ,value,256);
    b = w[1] + "="+value;

    if(w[2].empty()){
        iret=1;
        cout<< b<<endl;
    }
    else {
        FILE *fp = RXFOPEN(w[2].c_str(),"w");
        if(fp) {
            iret=fprintf(fp,"%s \n", b.c_str());
            FCLOSE(fp);
        }
        else {
            cout<<"cant open file "<<w[2]<<endl;
            iret=0;
        }
    }
    if(iret) cout<<" .. sent <"<<b <<"> to "<<w[2]<<endl;
    else   cout<<"getdata failed"<<endl;
    return iret;
}

int  Do_ModelList (QStringList &line ) {
    cout<<"modellist: " <<endl;
    cout<<"request a listing of open models  (TODO: keywords hoisted|etc) "<<endl;

    QStringList ll ; ll<< "models";
    vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(int i=0;i<lsl.size();i++)
        ll<<lsl[i]->GetQType();
    QString s = ll.join(";");
    ll.clear(); ll<< "broadcast"<<s;
    return Do_Broadcast(ll);
}
/*
********************************************************************************
*   Copyright(C) 2004-2014 Intel Corporation. All Rights Reserved.
*   The source code contained  or  described herein and all documents related to
*   the source code ("Material") are owned by Intel Corporation or its suppliers
*   or licensors.  Title to the  Material remains with  Intel Corporation or its
*   suppliers and licensors. The Material contains trade secrets and proprietary
*   and  confidential  information of  Intel or its suppliers and licensors. The
*   Material  is  protected  by  worldwide  copyright  and trade secret laws and
*   treaty  provisions. No part of the Material may be used, copied, reproduced,
*   modified, published, uploaded, posted, transmitted, distributed or disclosed
*   in any way without Intel's prior express written permission.
*   No license  under any  patent, copyright, trade secret or other intellectual
*   property right is granted to or conferred upon you by disclosure or delivery
*   of the Materials,  either expressly, by implication, inducement, estoppel or
*   otherwise.  Any  license  under  such  intellectual property  rights must be
*   express and approved by Intel in writing.
*
********************************************************************************
*   Content : MKL DSS C example
*
********************************************************************************
*/
/*
**
** Example program to solve real unsymmetric system of equations.
** The example also demonstrates how to solve transposed system ATx=b.
**
*/

/*
** Define the array and rhs vectors
*/
/*
#define NROWS       5
#define NCOLS       5
#define NNONZEROS   9
#define NRHS        1
static const MKL_INT nRows = NROWS;
static const MKL_INT nCols = NCOLS;
static const MKL_INT nNonZeros = NNONZEROS;
static const MKL_INT nRhs = NRHS;
static MKL_INT rowIndex[NROWS + 1] = { 1, 3, 5, 7, 9, 10 };
static MKL_INT columns[NNONZEROS] = { 1, 2, 1, 2, 3, 4, 3, 4, 5 };
static double values[NNONZEROS] = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
static _DOUBLE_PRECISION_t rhs[NCOLS * 2];
static _DOUBLE_PRECISION_t solValues[NROWS] = { 0, 1, 2, 3, 4 };
*/
#define NROWS       10
#define NCOLS       10
#define NNONZEROS   34
#define NRHS        1
static  MKL_INT nRows = NROWS;
static  MKL_INT nCols = NCOLS;
static  MKL_INT nNonZeros = NNONZEROS;
static  MKL_INT nRhs = NRHS;
static MKL_INT rowIndex[NROWS + 1] = {1 ,4 ,7 ,10 ,13 ,16 ,19 ,23 ,27 ,31 ,35 };
static MKL_INT columns[NNONZEROS] =  {1,2,3,1,2,3,1,2,3,4,5,6,4,5,6,4,5,6,7,8,9,10,7,8,9,10,7,8,9,10,7,8,9,10};
static double values[NNONZEROS] = { 1.4782349987155583E+05,-1.7462298274040222E-10,0,
-2.0372681319713593E-10,1.0477324424067691E+06,0,
0,0,2.0007692733286726E+02,
1.4782350129077787E+05,-6.5483618527650833E-11,0,
-7.2759576141834259E-11,1.0477323743228830E+06,0,
0,0,2.0007692762503103E+02,
1.3501495824579266E+05,4.3116908754246862E+04,0,
-1.9420926731421852E+04,4.3116908754246877E+04,1.2655346680445822E+06,
0,2.21125E+04,0,
0,1.8917210545559419E+02,0,
-1.9420926731421860E+04,2.21125E+04,0,
1.4678972912460755E+05};
static _DOUBLE_PRECISION_t rhs[NCOLS * 2];
static _DOUBLE_PRECISION_t solValues[NROWS] = { 0, 1, 2, 3, 4 ,5,6,7,8,9};

MKL_INT
dssmain ()
{
  MKL_INT i, j;
  /* Allocate storage for the solver handle and the right-hand side. */
  _MKL_DSS_HANDLE_t handle;
  _INTEGER_t error;
  _CHARACTER_t statIn[] = "determinant", *uplo;
  _DOUBLE_PRECISION_t statOut[5], eps = 1e-6;
  MKL_INT opt = MKL_DSS_DEFAULTS, opt1;
  MKL_INT sym = MKL_DSS_NON_SYMMETRIC;
  MKL_INT type = MKL_DSS_INDEFINITE;
/* --------------------- */
/* Initialize the solver */
/* --------------------- */

  error = dss_create (handle, opt);
  if (error != MKL_DSS_SUCCESS)
    goto printError;
/* ------------------------------------------- */
/* Define the non-zero structure of the matrix */
/* ------------------------------------------- */
  error = dss_define_structure (handle, sym, rowIndex, nRows, nCols, columns, nNonZeros);
  if (error != MKL_DSS_SUCCESS)
    goto printError;
/* ------------------ */
/* Reorder the matrix */
/* ------------------ */
  error = dss_reorder (handle, opt, 0);
  if (error != MKL_DSS_SUCCESS)
    goto printError;
/* ------------------ */
/* Factor the matrix  */
/* ------------------ */
  error = dss_factor_real (handle, type, values);
  if (error != MKL_DSS_SUCCESS)
    goto printError;
/* ------------------------ */
/* Get the solution vector for Ax=b and ATx=b and check correctness */
/* ------------------------ */
  for (i = 0; i < 3; i++)
    {
      if (i == 0)
        {
          uplo = "non-transposed";
          opt1 = MKL_DSS_DEFAULTS;
        }
      else if (i == 1)
        {
          uplo = "transposed";
          opt1 = MKL_DSS_TRANSPOSE_SOLVE;
        }
      else
// Conjugate transposed == transposed for real matrices
      if (i == 2)
        {
          uplo = "conjugate transposed";
          opt1 = MKL_DSS_CONJUGATE_SOLVE;
        }

      printf ("\nSolving %s system...\n", uplo);

 //    calculate  RHS = A.solvalues
// Compute rhs respectively to uplo to have solution solValue   X       Y
//      mkl_dcsrgemv (uplo,&nRows, values, rowIndex, columns, solValues, rhs);

// Nullify solution on entry (for sure)
      for (j = 0; j < nCols; j++){
          rhs[j] = solValues[j]; //qDebug()<< rhs[j];
 //       solValues[j] = 0.0;
      }

// Apply trans or non-trans option, solve system
      opt |= opt1;
      error = dss_solve_real (handle, opt, rhs, nRhs, solValues);
      if (error != MKL_DSS_SUCCESS)
        goto printError;
      opt &= ~opt1;

// Check solution vector: should be {0,1,2,3,4}
//      for (j = 0; j < nCols; j++)
//        {
//          if ((solValues[j] > j + eps) || (solValues[j] < j - eps))
//            {
//              printf ("Incorrect solution\n");
//              error = 1000 + i;
//              goto printError;
//            }
//        }
      printf ("Print solUtion array: ");
      for (j = 0; j < nCols; j++)
        printf (" %g", solValues[j]);

      printf ("\n");
    }
/* -------------------------- */
/* Deallocate solver storage  */
/* -------------------------- */
  error = dss_delete (handle, opt);
  if (error != MKL_DSS_SUCCESS)
    goto printError;
/* ---------------------- */
/* Print solution vector  */
/* ---------------------- */
  printf ("\nExample successfully PASSED!\n");
  return (0);
printError:
  printf ("Solver returned error code %d\n", error);
  return (1);
}


int   Do_Debug  (QStringList &wds ) {
    /*
   a variable syntax
   to start 'debug:unresolveConnects:(blank or sailname)
*/
    int rc=0;

    int nw=wds.size();
    QString what, w2;
    int length=0;

    if(nw>1) what=wds[1];
    else{
        what="unresolveConnects";
        cout<<"debug:dsstest| unresolveconnects | updatealltees |nrtest |stif|elementtest "<<endl;
        return 0;
    }
    if(0==what.compare("dsstest",Qt::CaseInsensitive))
    {

        RXSparseMatrix m(RU);rc= m.SolveDSS(NULL);
        return (rc==0);
    }

    if(0==what.compare("unresolveconnects",Qt::CaseInsensitive))
    {
        if(nw>2) w2=wds[2]; else w2="";
        length = w2.length();
        cf_unresolve_connects(qPrintable(w2),&length ) ; rc=1;
    }
    if(what.contains("updatealltees",Qt::CaseInsensitive))
    {
        cf_updateallteesbysv( ) ; rc=1;
    }
#ifdef ODETESTING
    if(what.contains("nrtest",Qt::CaseInsensitive))
    {
//        class Nr rk mytest;
//        mytest.testodeint();
        rc=1;
    }
    if(what.startsWith("rk",Qt::CaseInsensitive))
    {
        class RXRungeKutta mytest;
        mytest.test();//rksolve();
        rc=1;
    }
    if(what.contains("stif",Qt::CaseInsensitive))
    {
        class RXStiffSolver mytest;
        mytest.test();//teststifbs ();
        rc=1;
    }
#endif
    if(what.contains("elementtest",Qt::CaseInsensitive))
    {
        double a[36],b[36],c[36];
        int w,i; bool ok;
        for(i=0;i<36;i++) a[i] =  b[i]=c[i]=0;

        for(w=2,i=0;i<36;i++,w++)
        {
            a[i] = wds.value(w).toDouble(&ok);
            if(!ok )
            {
                qDebug()<<" bad numeric value"<<w<< " '"<<wds.value(w)<<"'";
                a[i]=0.;
            }
        }
        cf_elementTest(a,b,c);
        rc=1;
        cout<<"(*debug:elementtest:m1,m2,m1r,m2r (each 3x3) "<<endl;
        cout<<"such that"<<endl;
        cout<<"eg dx = m1. x + m2 x^2  . rotation = m3.x + m4 . x^2 *)"<<endl;
     }
    if (what.contains("dss",Qt::CaseInsensitive))
    {
         double*rhs=0;
         int i, err;
      //  return  dssmain ();
        class RXSparseMatrix1 a(RXM_RECTANGLE);
        a.HB_Read(qPrintable(wds.value(2)));

      //  a.PrettyPrint(cout);
        rhs = new double[ a.Nr() ] ;
        for(i=0;i<a.Nr();i++) rhs[i]=i;

        err=a.dss_start(); qDebug()<<" start  err= "<<err;
        if(!err) { qDebug()<< "  go to factor";
            err= a.dss_factor(a.m_hbio.values);qDebug()<<" factor err= "<<err;
            if(!err) {
                err= a.dss_solve( rhs);qDebug()<<" solve  err= "<<err; //returns the solution in rhs
                qDebug()<<"result= {";
                for(i=0;i<a.Nr();i++) qDebug()<< rhs[i]<<",";
                qDebug()<<"}";
            }
        }
        qDebug()<<" DONE";
         a.dss_end();
         delete[] rhs;

        rc=(err ==0);
    }
    if (what.contains("matrix",Qt::CaseInsensitive))
    {
         int   err;
        class RXSparseMatrix1 a(RXM_RECTANGLE),b(RXM_RECTANGLE),c(RXM_RECTANGLE);
        a.HB_Read(qPrintable(wds.value(2)));
        b.HB_Read(qPrintable(wds.value(2)));

        c.BinaryAdd( a,b );
        c.HB_Write("qa/addedAgain.RUA");

        rc=1;
    }
    if (what.contains("uvmap",Qt::CaseInsensitive))
    {
        bool ok;
        if(nw<5){ cout<<  " debug:uvmap:modelname:u:v:flag"<<endl; return 0;}
        QString qmodel = wds[2];
          QString &qu = wds[3];
          QString &qv = wds[4];

        double u = qu.toDouble(&ok);
        if(!ok) {
            cout<<  " debug:uvmap:modelname:U:v:flag"<<endl; return 0;
        }
        double v = qv.toDouble(&ok);
        if(!ok) {
            cout<<  " debug:uvmap:modelname:u:V:flag"<<endl; return 0;
        }
        ON_2dPoint p(u,v);
        class RXSail *sail = g_World->FindModel(qmodel);
        if(!sail) {
            cout<<  " debug:uvmap:MODELNAME:u:v:flag"<<endl; return 0;
        }
        ON_3dPoint x = sail->CoordsAt(p,RX_MODELSPACE );
        qDebug()<< "at uv= "<<u<<v<<" modelspaceCoords= "<<x.x<<x.y<<x.z;

        rc=1;
    }

    return rc;
}

int   Do_ListDependencies (QStringList &wds ) {
    /*
To get a list of expressions
dependencies: type, filename
 if 'type' is  recognised as a sail , just its expressions are sent
 else  all expressions in all models are sent.
keyword filename is 'echo' or blank, else is interpreted as a filename.
*/
    int rc=0;
    int echo=false;

    int nw=wds.size();
    QString what, filename;
    //   QString qbuf = QString("Do_Get_Expressions not implemented");


    if(nw>1) what=wds[1]; else what="all";
    if(nw>2) filename=wds[2];

    if(filename.isEmpty() )
        filename = "dependencies.txt";
    if(filename=="echo") {
        echo=true;
        filename = "dependencies.txt";
    }

    // if filename isnt blank or echo , open a scratch file "expressions.txt";
    // else open filename (with user input if the name is invalid)

    //  TODO  if(VerifyFileName())
    wofstream s(qPrintable (filename));
    if(!s.is_open())
        return 0;
    rc+= g_World->ExportAllDependencies(s,niece) ;
    s.close();
    cout<<rc<<" dependencies are in "<<qPrintable(filename)<<endl;
    if(!echo)
        return rc;

    //    QFile myfile(filename);
    //    if (myfile.open(QIODevice::ReadOnly | QIODevice::Text)){
    //        qbuf = myfile.readAll();
    //        myfile.close();
    //        qDebug()<<"deps filelength="<<qbuf.length();

    //        RXPrint(client, qPrintable(qbuf),1);

    //    }

    return rc;
}
int Do_Get_Expressions(const char *linein,Graphic *g, class QObject* client) {//
    /*
To get a list of expressions
getExpressions <tab> type, filename
 if 'type' is  recognised as a sail , just its expressions are sent
 else  all expressions in all models are sent.
keyword filename is 'echo' or blank, else is interpreted as a filename.
*/
    int rc=0;
#ifndef RXQT
    cout<<" Do_Get_Expressions needs Qt"<<endl;
#else
    vector<string> wds; int nw;
    string Buf(linein);
    wds = rxparsestring(Buf,RXSCRIPTSEPS);//":\t\n");
    nw=wds.size();
    string what, filename;
    QString qbuf = QString("Do_Get_Expressions not implemented");


    if(nw>1) what=wds[1]; else what="all";
    if(nw>2) filename=wds[2];

    if(filename.empty())
        filename = "expressions.txt";
    if(filename=="echo")
        filename = "expressions.txt";

    // if filename isnt blank or echo , open a scratch file "expressions.txt";
    // else open filename (with user input if the name is invalid)

    //  TODO  if(VerifyFileName())
    wofstream s(filename.c_str());
    if(!s.is_open())
        return 0;
    rc+= g_World->ExportAllExpressions(s) ;
    s.close();
    if(filename != "expressions.txt")
        return rc;

    QFile myfile(QString(filename.c_str()));
    if (myfile.open(QIODevice::ReadOnly | QIODevice::Text)){
        qbuf = myfile.readAll();
        myfile.close();
        qDebug()<<"expressions.txt length="<<qbuf.length();
#ifdef RXQT
        RXPrint(client, qPrintable(qbuf),1);
#else
        cout<<buf.toAscii()<<endl;
#endif
    }
#endif
    return rc;
}


int Do_Get_Headers(const char *linein,Graphic *g,QObject* client){ //getExpressions
    /*
To get a list of csv file headers
getheaders <tab> type, filename
keyword type is 'input', 'output' or 'all'
keyword filename is 'server', 'client' or else is interpreted as a filename.
If you used 'server' or 'client' you must immediately start listening for a reply.
*/
    vector<string> wds; int nw;
    string Buf(linein);
    wds = rxparsestring(Buf,":\t\n"); nw=wds.size();

    if (!g_World->DBMirror() )
        return 0;
    string what, filename;
    std::string buffer;
    FILE *fp=NULL;
    int iret=0;
    if(nw>1) what=wds[1]; else what="all";
    if(nw>2) filename=wds[2];
    //	if(!filename) {
    //		cout<<" getheaders ,To get a list of csv file headers. syntax is:"<<endl;
    //		cout<<"getheaders: type, filename"<<endl;
    //		cout<<"keyword type is 'input', 'output' or 'all'"<<endl;
    //		cout<<"keyword filename is 'server', 'client' or else is interpreted as a filename."<<endl;
    //		cout<<"If you used 'server' or 'client' you must immediately start listening for a reply."<<endl;
    //		return 0;
    //	}

    buffer = g_World->DBMirror()->Extract_Headers(what );//is 'input', 'output' or 'all'
    if(!buffer.empty()) {
        {
            if(!filename.empty()) {
                fp=RXFOPEN(filename.c_str(),"w");
                if(fp) {fprintf(fp,"%s",buffer.c_str()); fflush(fp); FCLOSE(fp); }
            }
            else
            {
#ifdef RXQT
                RXPrint(client, buffer.c_str(),2);
#else
                cout<<buffer.c_str()<<endl;
#endif
            }
        }
    }
    return 1 + (2-2)*iret;
}
int  Do_DSM_TransferModel (QStringList &line )
{
    int rc;
    rc= g_World->DSM_TransferModels(line.value(1));
    return rc;
}
int Do_DSM_Init (QStringList &line )
{
    int rc=0;
    int pNoUseDsm = line.value(1).toInt();

    cout<<qPrintable(line.join(" \t"))<<endl;
    rc=g_World->DSM_Init(!pNoUseDsm);
    return rc;
}
int  Do_DSM_End  (QStringList &line )
{
    int rc=0;
    if(g_World)
        rc= g_World->DSM_End();
    return rc;
}

int  Do_RebuildMaterials(QStringList &line )
{
    int nw = line.size();
    cout<<qPrintable(line.join(" \t"))<<endl;
    if(nw<2)
    {
        cout<<" syntax is :"<<endl;
        cout<<"  "<<endl;
    }

    SAIL *s  =  PCS_Get_ScriptLine_Model(line,NULL);

    RX_COORDSPACE space;
    space=  RX_MODELSPACE; // {RX_GLOBAL, RX_MODELSPACE, RX_REDSPACE,RX_BLACKSPACE}
    if (line.value(2).contains("world",Qt::CaseInsensitive))
        space = RX_GLOBAL ;
    if (line.value(2).contains("global",Qt::CaseInsensitive))
        space = RX_GLOBAL ;
    if( s )
        s-> ReBuild_All_Materials(space);

    return(1);
}

int Do_PrestressSet (QStringList &line ){
    int nw = line.size();
    cout<<qPrintable(line.join(" \t"))<<endl;
    cout<<" (see also commands 'resetEdges' and 'shrinkEdges'\n";
    if(nw<2)
    {
        cout<<" syntax is :"<<endl;
        cout<<"prestress:<sailname>:ps11: ps22:ps12:reduction factor 1:reduction factor 2: reduction factor 3  "<<endl;
        cout<<"as in  Stress =reduction factor . D . TR(reduction factor) . eps + prestress "<<endl;
        cout<<"and    Stiffness = EM. stiffness. EM(tr) \n"<<endl;
        cout<<"    example: prestress:genoa: 100:100:0:    0.01 " <<endl;
        cout<<"    or:      prestress:genoa: 100:100:0:    1 : 0 : 1 \n" <<endl;


        cout<<"NOTE that isotropic prestress is {ps11=xx, ps22=xx, ps12=0 }\n"<<endl;
        cout<<"NOTE with Wrinkling: the use of ElasticModifier and Prestress with Wrinkling is incorrect\n"<<endl;

        cout<<"alternatively "<<endl<< "prestress::off "<<endl<<" to disable prestress without losing the settings"<<endl;
        cout<<"If the three words ps11: ps22:ps12 cannot be read as numbers, the level of prestress is unchanged "<<endl;
        cout<< "this is useful because it is better to define prestress as $psxx, $psyy, $psxy on a material" <<endl;
        cout<<"that puts the prestress into material reference frame X=warp(chaine) rather than the element reference frame (which is arbitrary)"<<endl;
        cout<<"so you can go 'prestress:genoa: cat : dog :wombat :1:0:1 '  to re-use previous prestress values"<<endl;
    }
    if(line.value(1)=="off")
    {
        relaxflagcommon.prestress = 0;
        return 1;
    }
    relaxflagcommon.prestress = 1;

    SAIL *s  =  PCS_Get_ScriptLine_Model(line,NULL);

    double val[3]; int k,  SomePS=0;
    bool ok;
    double redfac;

    for(k=0;k<3;k++){
        val[k]=line.value(k+2).toDouble(&ok);  SomePS+=ok;
    }

    if(nw==6){
        redfac=line.value(5).toDouble(&ok);
        if(ok)
        {
            for (k=0;k<3;k++)
                relaxflagcommon.ElasticModifier[k] [k] = redfac;
        }
        cout<<" single value applied\n";
    }
    if(nw>=8) {
        for (k=0;k<3;k++)
        {
            redfac=line.value(k+5).toDouble(&ok);
            if(ok)
                relaxflagcommon.ElasticModifier[k] [k] = redfac;
        }
    }

    if(  (SomePS==3))
        cout<<"Prestress           "<<val[0]<<","<< val[1]<< ","  <<val[2]<<endl;
    cout<<"Prestress K-factors "
       << relaxflagcommon.ElasticModifier[0] [0] <<","
       << relaxflagcommon.ElasticModifier[1] [1] <<","
       << relaxflagcommon.ElasticModifier[2] [2] <<endl;

    if(s&&  (SomePS==3))
        s->SetOneConstantPreStress(val);

    return(1);
}

int  Do_Broadcast(QStringList &pwords )
{
    // to stress-test the client comms , go
    //broadcast \t 'file'\t fname
    if(pwords.count()<2)
        return 0;
    g_World->OutputToClient(pwords.back(),100);

    if(pwords.count()<3)
        return 1;
    if(pwords[1].contains("file")) {
        QFile file (pwords.back() ) ;
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return 0;

        while (!file.atEnd()) {
            QByteArray line = file.readLine();
            g_World->OutputToClient(QString(line),100);
        }
        file.close();
    }

    return(1);
}
int Do_System (QStringList &pwords )// was int Do_System(const char *in,Graphic *g)
{
    int retVal=1;
    if(pwords.count()<2)
        return 0;
    const QString &what=pwords.value(1);
    if(what.isEmpty())
        return 0;
    if(what.contains(QRegExp("\brm\b"))&& what.contains(QRegExp("\b*\b")) ){
        g_World->OutputToClient("IGNORING system call containing 'rm' and '*'",2);
        return 0;
    }

    char *buff = STRDUP(qPrintable(what));
    if(g_World->DBMirror() )
        while(g_World->DBMirror()->Replace_Words_From_Summary(&buff,0) ) ;
    cout<<"call system with '"<<buff<<"'"<<endl;
    retVal = !system(buff);
    RXFREE(buff);
    return(retVal);
}
EXTERN_C int BrentTest();
//extern int ssfindermain() ;
int Do_Test (QStringList &pwords )//  test: n1,n2,n3,....
{
    int retVal=1;
    //  int n = pwords.count();
    //  retVal=ssfindermain();//BrentTest();
    return(retVal);
}

int Do_Reset_One_Edges(SAIL *s)
{
    int  flag=1;
    int retVal=1;
    double total;
    cf_calc_s_angles(s->SailListIndex(),flag, &total);
    printf("= %f",total);
    cf_reset_edges(s->SailListIndex());
    cf_calc_s_angles(s->SailListIndex(),flag,&total);
    printf("...  AFTER model %d = %f\n",s->SailListIndex(), total);
    cf_calc_g_angles(s->SailListIndex(),flag,&total);
    s->SetFlag(FL_HASLACKOFFIT);
    g_World->PostProcessAllSails(1);
    return(retVal);
}
int Do_Shrink_One_Edges(SAIL *s)
{
    int flag=1;
    int retVal=1;
    double total;
    cout <<" ! Purpose of Shrink: "<<endl<<"To reset the edge lengths  of a run including prestress" <<endl
        << "!  so that next time, a run with  prestress zero would give the same result."<<endl
        <<  "! NOTE"<<endl
         <<  " ! MUST be run with No stiffness reduction and with a prestress value"<<endl
          <<  " ! Method"<<endl
           << "! 1) Get the current stress"<<endl
           <<  "! 2) Eps = dstfold(inv) x stress"<<endl
            <<  "! 3) delta edges =   G (transposed) * eps"<<endl
             << "! 4) put the new edge lengths in a local array." <<endl
             <<  " ! 5) At the end, copy these edge lengths into ZLINKo\n"  <<endl;

    int j,k, bad=0;
    for (j=0;j<3;j++){
        for (k=0;k<3;k++){
            if(j==k)
            {
                if(fabs(1. - relaxflagcommon.ElasticModifier[j] [k]) > 0.001 )
                    bad++;
            }
            else
            {
                if(fabs(relaxflagcommon.ElasticModifier[j] [k]) > 0.00001 )
                    bad++;
            }
        }
    }
    if(bad){
        cout<<"Please set prestress K-matrix to Identity and ensure the prestress is correct"
           << "and run again" <<endl;
    }


    cf_shrink_edges(s->SailListIndex());
    cf_calc_g_angles(s->SailListIndex(),flag,&total);
    return(retVal);
}

int Do_Reset_All_Edges(QStringList &line )
{
    unsigned int i;

    int  flag=1;
    double total;
    int retVal=1;
    cout<<" Reset sets the lengths of all the elements so that they are an exact fot to the current geometry\n";
    cout<<" (see also commands 'prestress' and 'shrinkEdges'\n";
    vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(i=0;i<lsl.size();i++)  {
        SAIL *theSail = lsl[i];

        cf_reset_edges(theSail->SailListIndex());
        cf_calc_g_angles(theSail->SailListIndex(),flag,&total);
        theSail->SetFlag(FL_NEEDS_RUNNING );
        cout<<"DONE Reset edges  on "<<theSail->GetType().c_str()<<endl;
    }
    g_World->Bring_All_Models_To_Date();
#ifdef _X
    UpdateMenu();
    SetCameraList();
#endif

    return(retVal);
}


int Do_Shrink_All_Edges(QStringList &line )
{
    unsigned int i;
    int flag=1, retVal=1;
    double total;
    cout <<" ! Purpose of Shrink: "<<endl<<"To reset the edge lengths  after a run with prestress" <<endl
        << "!  so that next time, a run with  prestress zero would give the same result.\n"<<endl

        <<  " ! Method"<<endl
         << "! 1) Using the current stress"<<endl
         <<  "!       set Eps = dstfold(inv) x stress"<<endl
          <<  "! 3) delta edges =   G (transposed) * eps"<<endl
           << "! 4) increment the edge lengths\n\n " <<endl;

    cout<< "The sequence for freezing formfinding with prestress is\n"
        << "first, set low K factors and finite prestress,\nthen"
        <<"1)    `calculate` with reduced K factor(s) and finite prestress \n"
       << "3)    `shrink ` to adjust the pattern \n"
       << "4)     As a check, set prestress to zero, K to 1 and calculate.\n You should get the same result\n"
       << "5)     repeat  ( 1-3 ) a few times  \n\n"
       << " Now you should find that with K = Identity and prestress zero   "<<endl
       << " that the stress equals your prestress."<<endl;
    cout<<" (see also commands 'prestress', 'resetEdges' \n"<<endl;

    vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(i=0;i<lsl.size();i++)  {
        SAIL *s = lsl[i];
        cf_shrink_edges(s->SailListIndex());
        cf_calc_g_angles(s->SailListIndex(),flag,&total);
        cout<< "DONE Shrink edges on "<<s->GetType().c_str()<<endl;
    }
    return(retVal);
}


int Do_DiscretizeCurvature (class RXSail *p_sail )
{
    cout<<" Discretizing of curvature is a step in the formfinding process"   <<endl;
    cout<< "After you have done the resetEdges/Calculate loop enough times" <<endl;
    cout<<" you go Shrink Edges ; prestress OFF; Calculate " << endl;
    cout<<" and then you run this. It generates gausscurves which contain " << endl;
    cout <<"the gaussian curvature of the nearby surface." <<endl;
    int what=PCG_DEFLECTED;
    if( !p_sail)
        return 0;
    Make_Relsite_Edits(p_sail); // only relsites with attribute 'Make_Edit'
    Make_SeamCurve_Edits(p_sail) ;
    cout <<" what is param WHAT in Gauss_All_Panels?????????????????"<<endl;
    Gauss_All_Panels(what,p_sail);//  collects the curvature onto the sc's gauss_pts
    Make_Gauss_Curves(p_sail);// (using make_gauss_curve) then creates the gauss BC
    Print_All_Entities(p_sail,"after discretizing");
    return 1;
}
//////////////


/************************************************************************/
int Do_Env_Edit(const char *linein,Graphic *p_ThisGraph){

    int iret=0;
#ifdef _X
    char *filter=NULL ;
    char*line = STRDUP(linein);

    if(line) filter = strtok(line,":\t"); // env edit
    if(filter)filter = strtok(NULL,":\t");
    if(filter) l_new = strtok(NULL,":!\n\0\t\r");

    iret = Env_Edit(p_ThisGraph,filter,l_new);

    RXFREE(line);
#endif

    return iret ;
}

/************************************************************************/
Graphic *Do_Set_Model(const char *linein){  // finds a Graphic corresponding to the 2nd word
    Graphic *g;

    char *what=NULL,*name=NULL;
    char*line = STRDUP(linein);
    if(SDB) printf("Do_Set_Model with <%s>\n",line);

    if(line) what = strtok(line,RXSCRIPTSEPS);
    if(what) name = strtok(NULL,RXSCRIPTSEPS);
    if(name) {
        PC_Strip_Leading(name);
        PC_Strip_Trailing(name);
    }
    g = NameToGraph(name);

    RXFREE(line);
    if(g) {
        printf("Do_Set_Modelcurrent  '%s' \n",qPrintable(g->m_qfilename));
        return g;
    }

    return NULL;
}
int  Do_LogResult (QStringList &pwords ){ //g_The_Dos[i].m_s="write_log";
    int rc=0;
    if (g_World->DBMirror() )
        if(g_World->DBout()){
            rc=  g_World->DBout()->Write_Line(g_World->DBMirror());
            if(rc)
            {
                cout<<"written logfile line"<<endl;
                return 1;
            }
            else
                cout<<" attempt to write logfile line failed" <<endl;
        }
    return 0;
}
int Do_BringAll (QStringList &pwords ){ //g_The_Dos[i].m_s="process changes";
    g_World->Bring_All_Models_To_Date();
    return 1;
}
int Do_RefreshSettings(QStringList &pwords){
    g_World->GetGlobalValues(DEFAULTS_FILE);
    return 1;
}
int Do_EditSettings(QStringList &pwords){
    Edit_Defaults_File() ;
    return 1;
}
int Do_FEA_Listing (QStringList &pwords){
    cf_model_hardcopy();
    return 1;
}
int Do_PrintObject  (QStringList & w )
{
    //print_object  genoa$relsite$c25 [recur]
    RXEntity_p  e=NULL ;
    SAIL *theSail =0;
    int  what, nw=0;

    class RXObject *theObject=0;
    nw=w.size();
    if(nw<4) {
        cout<<" syntax of <print_object> is"<<endl<< "  print_object:genoa$relsite$c25[:recur][:aunt,spawn,etc] "<<endl;
        return 0;
    }


    theSail = g_World->FindModel(w[1],1);
    if(!theSail  )theSail = g_World->FindModel(w[1],0);
    if(!theSail) return 0;

    e =theSail->GetKeyNoAlias( RX_QSTRINGTOWSTRING(w[2] ), RX_QSTRINGTOWSTRING(w[3]  ));// maybe WRONG not to use alias
    if(e)
        theObject = dynamic_cast<class RXObject *  >(e );

    if(!theObject) return 0;
    bool recur = false;
    if(nw>4) recur = w[4]== "recur" ;
    if(nw>5) {
        what=0;
        if( w[5].contains("parent")) what|=parent;
        else if( w[5].contains("child")) what|=child;
        if( w[5].contains("aunt")) what|=aunt;
        else if( w[5].contains("niece")) what|=niece;
        if( w[5].contains("frog")) what|=frog;
        else if( w[5].contains("spawn")) what|=spawn;
    }
    else
        what=niece;
    theObject->PrintOMap(stdout,"   ",what,recur);
    fflush(stdout); cout<<"printObject finished"<<endl;

    return 1;
}

int  Do_EdgeSwap (QStringList &w  )
{
    int rc=0;

    RXEntity_p  e=NULL ;
    SAIL *theSail =0;
    double xnew;
    int  nw=0;
    // the standard parser uses :' and '\t' for delimiters.

    nw=w.size();
    if(nw<4) {
        cout<<"syntax of <edgeswap> is"<<endl<< "edgeswap: model,stringname(wildcard), testExpression(>0)" <<endl;

        cout<<"delimiters may be   :   <tab>"<<endl;
        return 0;
    }

    theSail = g_World->FindModel(w[1],1);
    if(!theSail  )theSail = g_World->FindModel(w[1],0);
    if(!theSail) return 0;

    QRegExp  wname (w[2]);

    vector<RXEntity* > stringlist;
    vector<RXEntity* >::iterator it;

    theSail->MapExtractList(PCE_STRING,  stringlist,PCE_ISRESOLVED) ;
    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
        e  = *it;
        if(!wname.exactMatch(QString(e->name())))
            continue;

        int err=0 ;
        xnew = RXQuantity::OneTimeEvaluate(w[3].toStdWString()  ,L"",e,&err);
        if(xnew> 0.){
            qDebug()<< "edgeswap" <<e->type()<<e->name() ;
            class RX_FEString*s = (class RX_FEString*) e;
            rc+= s->EdgeSwap();
            Edits_Exist++;
            e->SetNeedsComputing();
        }
    }
    Print_All_Entities(theSail ,"On EdgeSwap ");    // cant  call this inside a Map loop
    return rc;
}

int Do_UpdateExpressions (QStringList &w  )
{
    //updateexpression: model:type: name: exp: newvalue
    int count=0;

    RXEntity_p  e=NULL ;
    SAIL *theSail =0;
    RXExpressionI *ex ; double xold,xnew;
    int  nw=0;
    // the standard parser uses :' and '\t' for delimiters.

    nw=w.size();
    if(nw<6) {
        cout<<"syntax of <updateexpression> is"<<endl<< "updateexpression:sailname:entitytype:entityname:expressionname:newvalue"<<endl;
        return 0;
    }

    theSail = g_World->FindModel(w[1],1);
    if(!theSail  )theSail = g_World->FindModel(w[1],0);
    if(!theSail) return 0;

    QRegExp wtype(w[2] );
    QRegExp  wname (w[3]);
    for (ent_citer  it = theSail->MapStart(); it != theSail->MapEnd(); it=theSail->MapIncrement(it)) {
        e  = dynamic_cast<RXEntity_p>( it->second);
        if(!wtype.exactMatch(QString(e->type())))
            continue;
        if(!wname.exactMatch(QString(e->name())))
            continue;

        int err=0,k=0;
        ex = e->FindExpression(w[4].toStdWString(),rxexpLocal);
        if(ex) {
            xold = ex->evaluate();
            xnew = RXQuantity::OneTimeEvaluate(w[5].toStdWString()  ,L"",e,&err);

            if(!err){
                k=ex->Change (TOSTRING(xnew));
                if(fabs(xold-xnew)> 0.0001){
                    qDebug()<< "updateexpression " <<e->type()<<e->name()<<w[4] << xold<<" -> " <<xnew;
                    count++;
                }
                ex->evaluate();
            }

        }
        Edits_Exist++;
        e->SetNeedsComputing();
    }
    cout<<" updated "<< count<<" expressions "<<endl;
    return 1;
}
int   Do_SetGraphicsOn(QStringList &w )
{
    int  nw=0;
    nw=w.size();
    if(nw<2) {
        cout<<"syntax of <graphics> is"<<endl<< "graphics: 1  (or 0) "<<endl;
        return 0;
    }
    if(0==w[1].compare("on",Qt::CaseInsensitive ))
        g_World->SetGraphicsEnabled(1);
    else if(0==w[1].compare("off",Qt::CaseInsensitive ))
        g_World->SetGraphicsEnabled(0);
    else
        g_World->SetGraphicsEnabled(w[1].toInt());


    return 1;
}

int Do_ChangeExpression (QStringList &win )
{
    //change:world$global$ig=10
    //change:boat$global$ig=10
    //change:genoa$global$p3=0.005*(kg+kh)
    //change:genoa$fabric$field_18ft$nu=0.0
    //change:genoa$relsite$c25$position=25%
    //int rc=0;

#ifdef DEBUG
    qDebug()<<endl<<"********DO CHANGE********  "<<  win<<endl<<endl;
#endif
    RXEntity_p  e=NULL ;
    SAIL *theSail =0;
    int  nw=0;
    class RXENode *theNode=0;
    // the standard parser uses :' and '\t' for delimiters.
    // but here we   have  '$' and '=' too.  So we need to re-parse
    QString qline =  win.join("\t") ;
    QRegExp r("[\t:$=]");
    QStringList w = qline.split(r);
    nw=w.size();
    for(int i=0;i<nw;i++)
        w[i]=w[i].trimmed();


    if(nw<5) {
        cout<<"syntax of <change> is"<<endl<< "change:sailname:entitytype:entityname:expressionname:newvalue"<<endl;
        cout<<"or for variables with model scope"<<endl;
        cout<<"change:sailname:global:expressionname:newvalue"<<endl;
        cout<<"or for variables with global scope"<<endl;
        cout<<"change:world:global:expressionname:newvalue"<<endl;
        cout<<"delimiters may be $ : = <tab>"<<endl;
        cout<<"for example:\nchange:main$site$clew$c=5\nchange:world:global:overlap:(jg/4)"<<endl;
        return 0;
    }

    if(w[1]=="world")
    {
        theNode = dynamic_cast<class RXENode *  >(g_World );
    }
    else{
        theSail = g_World->FindModel(w[1],1);
        if(!theSail  )theSail = g_World->FindModel(w[1],0);
        if(!theSail) {
            qDebug()<<" (Change) cant find model "<<w[1];
            return 0;
        }
        theNode = dynamic_cast<class RXENode *  >(theSail );
    }

    if(!theNode) {
        qDebug()<<" (Change) cant cast model "<<w[1];
        return 0;
    }

    if(w[2]=="global") {
        if(!theNode->FindAndChange(RX_QSTRINGTOWSTRING(w[3] )   ,RX_QSTRINGTOWSTRING(w[4] )  ,rxexpLocalAndTree)){
            qDebug()<<"(Change) cant find global expression '"<<qPrintable(w[3]) <<"`" <<qPrintable(w[4]);
            return 0;
        }
    }
    else{
        RXSTRING wtype =RX_QSTRINGTOWSTRING(w[2] );
        RXSTRING wname =RX_QSTRINGTOWSTRING( w[3] );
        e =theSail->GetKeyNoAlias(wtype,wname);// maybe WRONG. doesnt use alias
        if(e)
            theNode = dynamic_cast<class RXENode *  >(e );

        if(!theNode) {
            qDebug()<<" (Change) cant find enode "<<w[2]<<w[3];
            if(e)
                qDebug()<<" but DID find entity "<<e->type()<<","<<e->name();
            return 0;
        }

        if(!theNode->FindAndChange(RX_QSTRINGTOWSTRING(w[4] ) ,RX_QSTRINGTOWSTRING(w.value(5)) ,rxexpLocal)){
            qDebug()<<"(change) cant find expression '"<<qPrintable(w[4])<<"' in entity `"<<e->name() <<"` "<< endl;
            return 0;
        }
    }
    HC_Update_Display();
    Edits_Exist++;

    if(e)
        e->SetNeedsComputing();  //   needed even tho OnValueChange is supposed to do it
    if(theSail)
    {
        theSail->SetFlag(FL_NEEDS_GAUSSING );// already done - but not if e==NULL
        theSail->AddToSummary(); // this propagates any edits to the log DB BUT may cause a flipflop while wildcard trimming.";
    }

    return 1;
}
int Do_SetLastKnownGood	(class RXSail *p_sail)
{
    return cf_SetToLastKnownGood(p_sail->SailListIndex());
}


int Do_MakeLastKnownGood (QStringList &pwords){

    g_World->MakeLastKnownGood() ;
    return 1;
}
int Do_PrintAllEntities (SAIL *p_sail)
{
    return Print_All_Entities(p_sail, "on command");
}

int  Do_DeleteModel(SAIL *p_sail)
{
    if(!p_sail)
        return 0;
    Graphic*gg = p_sail->GetGraphic();
    delete p_sail;
    // send a message to delete its gg
    return 1;
}
int Do_Mesh(SAIL *s){
    assert(s);
    if(!s) return 0;
    return(s->Mesh());
}
int Do_Mesh(const char *line,Graphic *g){
    assert(!cout.bad());
    if(!g) return 0;
    if(!(g->GSail)) return 0;

    return(Do_Mesh(g->GSail) );
}

int Do_RefreshPC (class RXSail *p_sail )
{
    cout<<"Refresh PC file \n";

    if( !p_sail){
        cout<<"tried to Refresh PC file on a NULL sail"<<endl;
        return 0;
    }
    return	p_sail->Refresh_PC_File(0);
}

//////////////
int Do_DrawStress (class RXSail *p_sail )
{
    if( !p_sail)
        return 0;
    RXGRAPHICSEGMENT  k = p_sail->PostProcNonExclusive();
    p_sail->DrawPlotps(k);
    return 1;
}
//////////////
int Do_DrawStrain (class RXSail *p_sail )
{
    if( !p_sail)
        return 0;
    p_sail->DrawStrainVectors  (p_sail->PostProcNonExclusive());
    return 1;
}
//////////////
int Do_DrawPrinStiff (class RXSail *p_sail )
{
    if( !p_sail)
        return 0;
    p_sail->DrawPrinStiff (p_sail->PostProcNonExclusive());
    return 1;
}
//////////////

int Do_DrawRefVector (class RXSail *p_sail )
{
    if( !p_sail)
        return 0;
    p_sail->DrawMatRefVector(p_sail->PostProcNonExclusive());
    return 1;
}
int Do_Write_3dm(SAIL *p_pSail, const char * p_filename )
{
    return p_pSail->Write_3dm(p_filename);
}


////////////////
int Do_Write_stl( SAIL *p_sail, const char*filename)
{
    char  buf[1024];
    char *lp;
    const char *fname;

    if( !p_sail){
        cout<<"tried to write an STL on a NULL sail"<<endl;
        return 0;
    }

    if(filename)
    {fname =filename; }
    else {
        strcpy(buf,qPrintable(p_sail->ScriptFile()));
        lp=strrchr(buf,'.');
        if(lp) *lp=0;
        strcat(buf,"_dump.stl");
        fname=buf;cout<<" filename from sail's fileroot: "<<fname<<endl;
    }
    return p_sail->Write_stl(fname);
}
int Do_Write_uvdp(SAIL *p_sail,const char*file)
{
    if( !p_sail)
        return 0;
    return p_sail->Write_uvdp(file);
}
int Do_Write_Attributes (SAIL *p_sail,const char*file)
{
    if( !p_sail)
        return 0;
    QFileInfo fi(file);
    QString fp = fi.path();
    QString fb = fi.completeBaseName();
    QFileInfo f (fp, fb+ QString(".txt"));
    QString fname = f.filePath();
    return  p_sail-> Write_Attributes(fname);
}


int Do_Write_vtk(SAIL *p_sail,const char*file)
{
    if( !p_sail)
        return 0;
    QFileInfo fi(file);
    QString fp = fi.path();
    QString fb = fi.completeBaseName();
    {
        QFileInfo f (fp, fb+ QString("_surface.vtk"));
        QString fname = f.filePath();
        p_sail->WriteVtkSurface(fname);
    }
    {
        QFileInfo f (fp, fb+ QString("_lines.vtk"));
        QString fname = f.filePath();
        p_sail->WriteVtkStrings(fname);
    }
    return 1;
}
int Do_Write_Nastran(SAIL *p_sail,const char*file)
{
    if( !p_sail)
        return 0;
    return p_sail->WriteNastran(file);
}

QString Do_Action (QStringList &wds , Graphic *p_g ,struct DO_STRUCTURE p_TheDo){ // line  wins over graphic
    SAIL *s =NULL;
    QString l_ret, prompt("*.*");
    int nw=wds.size();

    setStatusBar(p_TheDo.m_s);

    if(p_TheDo.m_gm   )   {          // kw : filename
        //1  parse filename

        if(nw<=1)
            wds <<"*";
        QString &fname = wds[1];
        //2 ensure the filename is good
        int fstat =  g_World->AbsoluteFileName(fname)   ;

        if(p_TheDo.m_flag&SPT_FILE_READ) {
            switch(fstat) {
            case OK_TO_READ:
                break;
            case BAD_FILENAME:
                if(nw>3)
                    prompt= wds[2];
                else
                    prompt= " Open " + fname+" :  "+ wds.join(RXSCRIPTSEPS);
                fname = getfilename(0,qPrintable(fname),qPrintable(prompt),g_currentDir,PC_FILE_READ);
                break;
            default:
                g_World->OutputToClient(wds.join(RXSCRIPTSEPS),4);
            }
        } // read
        else if(p_TheDo.m_flag&SPT_FILE_WRITE) {
            switch(fstat) {
            case OK_TO_WRITE:
            case OK_TO_READ: // added feb18 2010. this makes us overwrite existing files.
                break;
            case BAD_FILENAME:
                if(nw>3)
                    prompt =wds[2];
                else
                    prompt="("+wds[0]+") Open `" + fname+"` for output:  "+wds.join(RXSCRIPTSEPS);
                fname = getfilename(0,fname,prompt,g_currentDir,PC_FILE_WRITE);
                break;
            default:
                g_World->OutputToClient(wds.join(RXSCRIPTSEPS),4);
            } //switch
        }// write

        //3 rebuild the line and runthe command on the complete line     // kw : filename
        if(p_TheDo.m_gm(wds ,p_g) )
            return  wds.join(RXSCRIPTSEPS);;
        return QString ("! (failed) ") +   wds.join(RXSCRIPTSEPS);;
    } //   if(p_TheDo.m_gm  )

    if( p_TheDo.m_mfg )   {          // kw : modelname: filename
        //1  parse filename
        if(nw<2) wds<<" ";
        if(nw<3) wds<<"*";
        QString & fname = wds[2];
        s =  PCS_Get_ScriptLine_Model(wds,p_g);  // identify model from the second word.


        //2 ensure the filename is good
        int fstat =  g_World->AbsoluteFileName(fname)   ;

        if(p_TheDo.m_flag&SPT_FILE_READ) {
            switch(fstat) {
            case OK_TO_READ:
                break;
            case BAD_FILENAME:
                if(nw>3)
                    prompt= wds[2];
                else
                    prompt= " Open " + fname+" :  "+ wds.join(RXSCRIPTSEPS);
                fname = getfilename(0,qPrintable(fname),qPrintable(prompt),g_currentDir,PC_FILE_READ);
                break;
            default:
                g_World->OutputToClient(wds.join(RXSCRIPTSEPS),4);
            }
        } // read
        else if(p_TheDo.m_flag&SPT_FILE_WRITE) {
            switch(fstat) {
            case OK_TO_WRITE:
            case OK_TO_READ: // added feb18 2010. this makes us overwrite existing files.
                break;
            case BAD_FILENAME:
                if(nw>3)
                    prompt =wds[2];
                else
                    prompt="("+wds[0]+") Open `" + fname+"` for output:  "+wds.join(RXSCRIPTSEPS);
                fname = getfilename(0,fname,prompt,g_currentDir,PC_FILE_WRITE);
                break;
            default:
                g_World->OutputToClient(wds.join(RXSCRIPTSEPS),4);
            } //switch
        }// write

        //3 rebuild the line and runthe command on the complete line  // kw : modelname: filename
        if(p_TheDo.m_mfg(wds ,p_g) )
            return  wds.join(RXSCRIPTSEPS);
        return QString (" ! (failed) ") +   wds.join(RXSCRIPTSEPS);

    } //   p_TheDo.m_mfg )


    // a command with non-standard parsing.
    if(p_TheDo.m_lm){
        if(p_TheDo.m_lm(wds) )
            return wds.join(RXSCRIPTSEPS) ;
        return QString("! Failed ") + wds.join(RXSCRIPTSEPS);
    }
    // all the other types have the sailname as 2nd word

    s =  PCS_Get_ScriptLine_Model(wds,p_g);  // identify a sail from the second word.

    if(s ) {
        setStatusBar(p_TheDo.m_s);
        if(NULL !=(p_TheDo.m_sm)) {
            l_ret=ON_String(p_TheDo.m_s) + ON_String("\t ") + ON_String(s->GetType().c_str()) ;
            if(p_TheDo.m_sm(s) )
                return l_ret;
            l_ret.clear();
        }

        if(p_TheDo.m_smf){ // kw : modelname: filename
            QString  name,theFile;
            int fstat;
            if(nw>2) name=wds[2] ;
            else  name="*";
            fstat =  g_World->AbsoluteFileName(name)   ;

            if(p_TheDo.m_flag&SPT_FILE_READ) {
                switch(fstat) {
                case OK_TO_READ:
                    theFile=name;
                    break;
                case BAD_FILENAME:
                    if(nw>3)
                        prompt= wds[2];
                    else
                        prompt= " Open " + name+" for import to "+ s->GetType().c_str();
                    theFile= getfilename(0,name ,prompt ,g_currentDir,PC_FILE_READ);
                    break;
                default:
                    g_World->OutputToClient(wds.join(RXSCRIPTSEPS),4);
                }
            } // read
            else if(p_TheDo.m_flag&SPT_FILE_WRITE) {
                switch(fstat) {
                case OK_TO_WRITE:
                case OK_TO_READ: // added feb18 2010. this makes us overwrite existing files.
                    theFile= name ;
                    break;
                case BAD_FILENAME:
                    if(nw>3)
                        prompt =wds[2];
                    else
                        prompt= " Open `" + name+"` for output from  "+ QString(s->GetType().c_str());
                    theFile= getfilename(0,name,prompt,g_currentDir,PC_FILE_WRITE);
                    break;
                default:
                    g_World->OutputToClient(wds.join(RXSCRIPTSEPS),4);
                } //switch
            }// write

            if(!theFile.isEmpty()) {
                if(p_TheDo.m_smf(s,qPrintable(theFile ) ) ) {
                    l_ret=ON_String(p_TheDo.m_s) + ON_String("\t ") + ON_String(s->GetType().c_str()) + ON_String("\t ") + ON_String(qPrintable(theFile ));
                    //if(s->GetGraphic() )  s->GetGraphic()->setStatusBar("Ready");
                    return l_ret;
                }
            }
        } // m_smf
    } // if 2nd word was a sail


    setStatusBar("Command Failed");
    return QString("! Failed (and shouldnt be here) ") + wds.join(RXSCRIPTSEPS);
}

QString ReadScriptLine(QTextStream &in) // <text> [&][!comment]
{
    QString rv; int k;
    bool keepGoing = true;
    while (keepGoing && !in.atEnd()) {
        QString line = in.readLine();
        if(line.isEmpty())
            continue;
        k = line.indexOf(QRegExp("[#!]"));
        if (k>=0)
            line.truncate(k);
        line=line.trimmed();
        keepGoing = line.endsWith('&') ;
        if(keepGoing)
        {
            line.chop(1);
            line.append(RXSCRIPTSEPS );
        }
        rv+=line;
    }
    return rv;
}
int Process_Script(QTextStream &in,const Graphic *g,  class QObject *  qobj){

    QString lLine;
    int pleaseBreak, errorval = 0;
    Edits_Exist = 0;
    g_NON_INTERACTIVE=1;

    cout<< "Start Process Script\n"<<endl;

    while (true)
    {
        lLine=ReadScriptLine(in);
        if (lLine.isNull())
            break;
        assert(!cout.bad());
        if(lLine.isEmpty())
            continue;
        cout<<"\nSCRIPT LINE < "<<qPrintable(lLine)<<">end ScriptLine"<<endl;
        if(  relaxflagcommon.Stop_Running  ) break;
        pleaseBreak = !Execute_Script_Line(lLine ,g,qobj);
        if(pleaseBreak)
        {qDebug()<<" break demanded in Process_Script"; break;}
        assert(!cout.bad());
    }
    assert(!cout.bad());
    if (Edits_Exist)
        g_World->Bring_All_Models_To_Date();
    assert(!cout.bad());
    if(errorval) g_World->OutputToClient("Read ended with some errors",1);
    g_NON_INTERACTIVE=0;
    return(0);
}

int Execute_Script_Line(const QString &linein, const Graphic *gin,  class QObject* qobj){
    char Firstword[256],second[256];
    QString qFirstW ,  qSecond ;
    static Graphic *c_This_Graph = NULL;
    static void *c_This_QObj = NULL;
    int ii, nw=0;
    bool bad= false;

    if(gin) 		// August 2004 to enable Set Model
        c_This_Graph = (Graphic *) gin;
    if(qobj)
        c_This_QObj =  qobj;

    QStringList wds = rxqparsestring(linein,QRegExp("[;]") );//protects quotes. default is QString::KeepEmptyParts
    nw = wds.size();

    for(ii=0;ii<nw;ii++) {
        if(wds[ii].isEmpty())
            continue;
        QString qline = wds[ii];
        int kt = qline.indexOf('!');  //strip any trailing comment
        if(kt>=0)
            qline= qline.left(kt);

        QStringList qsl= rxParseLine(qline,"(?![A-Z])[:\\t](?!\\\\)" );
        qline = qsl.join(RXSCRIPTSEPS);
        qFirstW.clear(); qSecond.clear();
        if(qsl.size()>0) {strncpy(Firstword,qPrintable(qsl[0]),255); qFirstW=qsl[0];} else  *Firstword=0;
        if(qsl.size()>1) { strncpy(second  ,qPrintable(qsl[1]),255); qSecond=qsl[1]; } else  *second=0;
        if(rxIsEmpty(Firstword))
            continue;

        assert(!cout.bad());

  //      if(!c_This_Graph) qDebug()<<" C This Grapph ZERO";
//            c_This_Graph = RXSubWindowHelper::rootgraphic; // may be zero NASTY
        if(stristr(Firstword,"environment"))
        { // environment: $windfiles:myfile
            char *old  = NULL;
            int l_env;
            vector<string>wds = rxparsestring(qPrintable(qline),RXSCRIPTSEPS);
            nw = wds.size ();
            if(nw>=3) {
                char s1[2048];  strcpy(s1,wds[1].c_str()); cout<<" try a getenv on "<<s1<<endl;
                old = getenv(s1);
                printf("Env '%s' ",s1);				if(old) printf(" was '%s'",old);
                string s3 = wds[1]+string("=")+wds[2];

                char *envbuf= (char*) malloc(2048); strncpy(envbuf,s3.c_str(),2048);
#ifdef linux
                l_env = putenv(envbuf);  // putenv requires us to leave the memory available
#else
                l_env = _putenv(envbuf);  // putenv requires us to leave the memory available
#endif

                old = getenv(s1);
                if(old) printf(" is now '%s'\n",old);
                else printf(" didnt putenv '%s' ='%s'\n", wds[1].c_str(),old);
            }// if nw
            else
                cout<<" not enough words in line "<<qPrintable(qline)<<endl<<" we need 3 or more. there are "<<nw<<endl;
        }
        else
            if(strieq(Firstword,"saillist")) {
#ifdef FORTRANLINKED
                int unit=6;
                cf_printsailstructures(unit);
                cf_printconnects(unit);
#endif
                g_World->PrintModelList(stdout);

            }
            else if (strieq(Firstword,"igesout") ||strieq(Firstword,"write_iges")  ) {
                Do_Export_IGES(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"envEdit")) {
                Do_Env_Edit(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"snapshot")) {
                Do_Snapshot(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"post")) {
                Do_Post_Data(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"getdata")) {
                Do_Get_Data(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"getheaders")) {
                Do_Get_Headers(qPrintable(qline),c_This_Graph ,qobj);
            }
        /*getheaders: type, filename
keyword type is 'input', 'output' or 'all'
keyword filename is 'server', 'client' or else is interpreted as a filename.
If you used 'server' or 'client' you must immediately start listening for a reply.
*/
        /*
To get the value of a variable, send
getdata: what:  */

            else if (strieq(Firstword,"getExpressions")) {
                Do_Get_Expressions(qPrintable(qline),c_This_Graph ,qobj);
            }
            else if (strieq(Firstword,"Delete")) {
                Do_Delete_Entity(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"Update")) {// here we may want to send any postprocessing data to clients.
                if(g_World) {
                    g_World->Set_Output_Flags();  // for now just sets DATA_HAS_CHANGED
                    g_World->PostProcessAllSails(3);
                    AKM_Each_Cycle(); // wierd. this calls PPAS
                }
                HC_Update_Display();
            }
            else if (strieq(Firstword,"Draw Edge")) {
                Do_Draw_Fanned(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"GLE export")) {
                cf_gle_export_forces();
            }
            else if (strieq(Firstword,"Set Model")) {
                Graphic * l_temp = Do_Set_Model(qPrintable(qline));
                if(l_temp) c_This_Graph = l_temp;
            }
            else if (strieq(Firstword,"memcheck")) {
                Check_All_Heap();
            }
            else if (strieq(Firstword,"Drop")) {
                Do_Drop_Sail( qPrintable(qline), c_This_Graph);
            }
            else if (strieq(Firstword,"exit")  || strieq(Firstword,"quit")   ) {
                Do_Exit( qPrintable(qline),c_This_Graph);
                return 1;
            }
            else if (strieq(Firstword,"solve")) {

                Do_Solve(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"editword")) {
                Do_EditWord(qPrintable(qline),c_This_Graph);
                Edits_Exist++;
            }
            else if (strieq(Firstword,"write pressure")) {
                Do_Write_Pressure(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"calculate") || qFirstW.startsWith("ca") ) {
                if (Edits_Exist || true){
                    g_World->Bring_All_Models_To_Date();// just before Do_Calc.
                    Edits_Exist=0;
                    if(!g_World->IsAutoCalc()) // else BAMTD already did it
                        Do_Calculate(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
                }
                else
                    Do_Calculate(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
            else if (strieq(Firstword,"macro") || strieq(Firstword,"script")  || strieq(Firstword,"open macro")   ) {
                Read_Script(second,c_This_Graph, qobj,true);
            }
            else if (stristr(Firstword,"export pan")) {
                Do_Export_Panels(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"resize")) {
                Do_Resize(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"pressure")) {
                Do_Constant_Pressure(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"open_summary") || qFirstW.startsWith(  "open_log") || qFirstW.startsWith(  "open log") ) {
                Do_Utils_Open_Summary(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
            else if (strieq(Firstword,"close_summary") || qFirstW.startsWith(  "close_log")  || qFirstW.startsWith(  "close log") ) {
                Do_Utils_Close_Summary(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
            else if (strieq(Firstword,"close_input")  || qFirstW.startsWith(  "close input")  ) {
                Do_Utils_Close_SummaryIn(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
            else if (strieq(Firstword,"choose_line") || qFirstW.startsWith("choose line")) {
                Do_Utils_Choose_Line(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
            else if (strieq(Firstword,"open_input")  || qFirstW.startsWith(  "open input") ) {
                Do_Utils_Open_In(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
            else if (strieq(Firstword,"Export Plot")) {
                Do_Export_Pic(qPrintable(qline),NULL);
            }
#ifdef OPTIMISE
            else if (strieq(Firstword,"Initialise Optimisation")) {
                Do_Initialise_Optimisation(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }

            else if (strieq(Firstword,"Load Optimisation History")) {
                Do_Load_Optimisation_History(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }

            else if (strieq(Firstword,"Run Optimisation")) {
                Do_Optimise(qPrintable(qline),c_This_Graph);// RXSubWindowHelper::rootgraphic);
            }
#endif
            else if (strieq(Firstword,"zoom")) {
                Do_Zoom(qPrintable(qline),c_This_Graph);
            }
            else if (strieq(Firstword,"EOF"))  return 0;
            else if (strieq(Firstword,"Hello from client"))  return 0;

            else  {
                int i;
                QString l_lineoutput = "";
                Initialise_Script();

                for(i=0;i< g_N_Dos;i++) {
                    if((g_The_Dos[i].m_flag&SPT_STREQ) && strieq(Firstword,g_The_Dos[i].m_s)){
                        l_lineoutput = Do_Action(qsl,c_This_Graph,g_The_Dos[i]);
                        if(!l_lineoutput.contains("dsm") ) qDebug()<< "ran "<<l_lineoutput;
                         if(l_lineoutput.startsWith("! (failed)") )
                         {
                             bad=true;
                         }

                        break;
                    }
                }
                if(! l_lineoutput.isEmpty() &&! l_lineoutput.toLower().contains(QRegExp("dsm(write|init|end)" ))  ) {
                    if( Open_ScriptLog_File(NULL))  		// make a record of the action
                        fprintf(g_SP,"%s\n",qPrintable(l_lineoutput));
                    if(g_World)
                        g_World->OutputToClient(QString("ran command  '") +l_lineoutput +"'",101);
                }
                else if (!qsl.length()) {
                    QString bb("script line NOT UNDERSTOOD = "); bb+=qsl.join(":" ) ;
                    if(g_World)
                        g_World->OutputToClient(bb,1);
                }
            } // end default
        Set_Previous_MALLOC_DATA();
        if(cout.bad()&&g_World)
            g_World->OutputToClient(QString( "COUT is BAD after executing " ) + qline  ,99);

    } // for each word
    // if (Edits_Exist)
    if( !bad && g_World)
        g_World->Bring_All_Models_To_Date();
    Edits_Exist=0;

    return 1;
}

