/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RXSail.h"

#include "segprint.h"
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include "GraphicStruct.h"
#include "stringutils.h"
//#include "akmutil.h"
#include "attachmod.h"
#include "words.h"
#include "find_include.h"
//#include "debugspecial.h" 
 


/* routine to find any items suitable for inclusion
 * in a sail.
 * Note the sail may be either the boat or truly a sail.
 */
int find_all_include_items_by_sail (SAIL*sail,int *nitems,HoopsItem **hitems)
{
int errVal = 0;
int count,i;
HoopsItem *h;
char child[256],type[256];
long key;

/* search hoops data base for non-exclusive children of  sailname */

/* printf("in find_all_include with '%s'\n",sailname); */


HC_Open_Segment_By_Key(sail->PostProcNonExclusive());
	HC_Begin_Contents_Search(".","segment");
		HC_Show_Contents_Count(&count);
		if((h = *hitems = (HoopsItem *) MALLOC((count+1)*sizeof(HoopsItem)))) {
		     i=0;
		     while(HC_Find_Contents(type,&key)) {
			HC_Show_Segment(key,child);
			h[i].name = STRDUP(LastWord(child));
			//printf(" found nonexclusive %s -> %s\n",child, h[i].name);
			h[i].m_Key = 0;// this is too dangerous key;
			h[i].indx = i;
			h[i].state = 0;
		 	i++;
		     }
		}
		else
			errVal=-1;
	HC_End_Contents_Search();
HC_Close_Segment();


*nitems = count;
return(errVal);
}

/*
int find_item(HoopsItem *h,int n,long key) TO DANGEROUS TO USE KEYS
{
char buf[512];
int i;
long rkey;

rkey = HC_KShow_Include_Segment(key);
HC_Show_Segment(rkey,buf);
 printf("CHECKING FOR SEGMENT '%s'\n",buf); 

for(i=0;i<n;i++) {
	if(h[i].key == rkey)
		return(i);
}

return(-1);  not found 
}*/

// set the stateFlag on the widget items according to whether the seg referred to is included or not.
// this should rather look at the UOs

int Get_state_NonEx_items(Graphic *ViewGraphic,const char *sailname,int nitems,HoopsItem *hitems)
{
// see finishview.cpp
int errVal = 0;
int i,j;

char opts[512];
// we are looking for a match between hitems and the inc_list. 
HC_Open_Segment_By_Key(ViewGraphic->m_ModelSeg);
   HC_Open_Segment("models");
     HC_Open_Segment(sailname);
	HC_Open_Segment("non_exclusive"); // i don't think this level matters
		HC_Show_One_Net_User_Option("inc_list",opts); 
		//printf(" opts are <%s>\n",opts);
		char **words=0;
		
		int nw=make_into_words(opts,&words,"+");
		for(i=0;i<nw;i++) {
			if(is_empty(words[i]))
				continue;
// now, if a HoopsItem has the same word, set it's state on
			for(j=0;j<nitems;j++) {
				if(strieq(words[i],hitems[j].name))
					hitems[j].state=1;
			}
		}
		RXFREE(words);
	HC_Close_Segment();   /*non-exclusive */
      HC_Close_Segment();  /* the sail */
   HC_Close_Segment();  /* sails */
HC_Close_Segment(); /* base */

return(errVal);
}



int Initialize_OptionStyle_Items(const char *sailname,int *nitems,HoopsItem **hitems)
{
int errVal = 0;
int count,i;
HoopsItem *h;
const char *attrib[] = {"mesh","faces","lines","hard contours","text","markers"};

count = 6;
if((h = *hitems = (HoopsItem *) MALLOC(count*sizeof(HoopsItem))) ){
	for(i=0;i<count;i++) {
		h[i].name = STRDUP(attrib[i]);
		h[i].m_Key = 0;
		h[i].indx = i;
		h[i].state = 0;
	}
}
else
	errVal=-1;


*nitems = count;
return(errVal);
}
/*
					HC_Open_Segment_By_Key(gfree->m_ModelSeg);
					HC_Open_Segment("models");
						HC_Open_Segment(sailname); 
							HC_Set_User_Options(options); 

*/

int Get_State_OptionStyle_Items(Graphic *ViewGraphic,const char *sailname,int nitems,HoopsItem *hitems)
{
int errVal = 0;
int i;
char theState[128];
//supposed to be searching for UOs in this sail's cormer of the graphics model seg;

	HC_Open_Segment_By_Key(ViewGraphic->m_ModelSeg);
	    HC_Open_Segment("models");
		HC_Open_Segment(sailname);
		    for(i=0;i<nitems;i++) {
			HC_Show_One_Net_User_Option(hitems[i].name,theState);
			//printf("OptionStyle item %s is '%s'\n",hitems[i].name,theState); 
			if(stristr(theState,"on"))
				hitems[i].state = 1;
			else
				hitems[i].state = 0;
		    }
		HC_Close_Segment();
	     HC_Close_Segment();
	HC_Close_Segment();

return(errVal);
}


int Find_All_Exclusive_Items(SAIL *p_Sail,int *nitems,HoopsItem **hitems)
{
int errVal = 0;
int count,i;
HoopsItem *h;
char  child[256];
count = 10;
/* search hoops data base for exclusive children of  sailname */

HC_Open_Segment_By_Key(p_Sail->PostProcExclusive() ); 
	HC_Begin_Segment_Search("*");
		HC_Show_Segment_Count(&count);
		if((h = *hitems = (HoopsItem *) MALLOC(count*sizeof(HoopsItem)))) {
		     i=0;
		     while(HC_Find_Segment(child)) {
			h[i].name = STRDUP(LastWord(child));
			h[i].m_Key = 0;//(long) HC_KOpen_Segment("."); 
			//HC_Close_Segment();
			h[i].indx = i;
			h[i].state = 0;
			h[i].m_HIChosen = 0;
		 	i++;
		     }
		}
		else
			errVal=-1;
	HC_End_Segment_Search();
HC_Close_Segment();


*nitems = count;
return(errVal);
}


char *Get_current_shell(Graphic *g,const char *sailname,char *theShell)
{
// traditionally called on ->m_ModelSeg
/* routine to return the currently set shell from hoops */
/* note you must supply memory  */

	HC_Open_Segment_By_Key(g->m_ModelSeg);
	    HC_Open_Segment("models");
		HC_Open_Segment(sailname);
			HC_Show_One_Net_User_Option("shellname",theShell);
		HC_Close_Segment();
	     HC_Close_Segment();
	HC_Close_Segment();
return(theShell);
}


int set_state_property_items(HoopsItem *hitems,int nitems,int graphstate)
{
int i;
/* set item.state to 0 if can't graph the property */
/* and graphstate is set to 1 */
/* theShell is the currently chosen shell */
/* returns the index of the chosen shell */

for(i=0;i<nitems;i++) {
	if(graphstate) 
		hitems[i].state = (graphmask(hitems[i].name) && graphstate);
	else
		hitems[i].state = colormask(hitems[i].name);
}

return(0);
}

int set_chosen_property_items(HoopsItem *hitems,int nitems,const char *theShell)
{
int i;
int chosen=-99; //linux found initialization bug
/* set item.state to 0 if can't graph the property */
/* and graphstate is set to 1 */
/* theShell is the currently chosen shell */
/* returns the index of the chosen shell */

for(i=0;i<nitems;i++) {
	if(stristr(hitems[i].name,theShell)) {
		hitems[i].m_HIChosen = 1;
		chosen = i;
	}
	else 
		hitems[i].m_HIChosen = 0;

}
// assert (chosen != -99); this can get triggered after a change of version
if(chosen==99) return 0;
return(chosen);
}

int set_state_property_buttons(HoopsItem *prop_items,int count)
{
int i;
Arg al[64]; 
register int ac=0;

for(i=0;i<count;i++) {
	XtSetArg(al[ac], XmNsensitive, prop_items[i].state); ac++;
        XtSetValues (prop_items[i].widget,al, ac );
	ac = 0;
}

return(count);
}

int Get_state_graph_button(long base,const char *sailname) 
// find out whether this sail is to be displayed 'as graph'
{
char theState[100];
if(base == 0) {
	HC_Show_One_Net_User_Option("graphstate",theState);
}
else {
	HC_Open_Segment_By_Key(base);
	    HC_Open_Segment("models");
		HC_Open_Segment(sailname);
			HC_Show_One_Net_User_Option("graphstate",theState);
		HC_Close_Segment();
	     HC_Close_Segment();
	HC_Close_Segment();
}
if(stristr(theState,"on")) 
	return(1);
else
	return(0);
}
/*
int SSDset_property(long base,const char *sailname,int nitems,HoopsItem *hitems,int graphstate)
{
int errVal = 0;
char buf[64];
const char *gStr[2] = {"off","on"};
	HC_Open_Segment_By_Key(base);
	    HC_Open_Segment("models");
		HC_Open_Segment(sailname);
			sprintf(buf,"graphstate=%s",gStr[graphstate]);
			HC_Set_User_Options(buf);
		HC_Close_Segment();
	     HC_Close_Segment();
	HC_Close_Segment();


return(errVal);
}*/

