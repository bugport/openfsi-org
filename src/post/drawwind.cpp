/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
#include "StdAfx.h"

#include "RXSail.h"
/* routine to draw the apparent wind 
 * into a hoops segment
 * which has already been opened
 */

#include "griddefs.h"

//#include "appwind.h"
#ifdef USE_PANSAIL
#include "pansail.h"
#endif

#include "global_declarations.h"

#include "drawwind.h"

#define SCALE_FACTOR 3
#define NO_OF_STEPS 32	/* was 10 till Feb 97 */

int Draw_Wind(float minHeight,float maxHeight)
{
#ifdef HOOPS
int i;
float z,u,v,w,weight;
float stepsize, awadummy;
long key;
VECTOR a,b;
VECTOR line;
char valStr[24];
float speed,normalize;
int nvert,vertoff;
VECTOR msh[NO_OF_STEPS+1][2];
float spd[NO_OF_STEPS+1][2];

 char buf[128];

/* debug bit */
line.x = line.y = 0;
line.z = 1;
z=maxHeight;
appwind_(&z,&u,&v,&w);
normalize = sqrt(u*u + v*v + w*w); /* length of longest arrow */

stepsize = maxHeight - minHeight;
if(fabs(stepsize) < 1e-6)  {
	sprintf(buf,"Draw_Wind. Small stepsize\n called with %f %f\n",minHeight,maxHeight);
	g_World->OutputToClient (buf,2); 
	return(0);

}
HC_Open_Segment("/include/various/boat");
HC_Open_Segment("wind/apparent wind");
HC_Flush_Contents(".","subsegment,geometry,style");
HC_Set_Color("text=black");

a.x = line.x * z;
a.y = line.y * z;
a.z = line.z * z;
/* draw reference line */


	weight = 4.0;
	HC_Set_Line_Weight(weight);
	HC_Set_Color("lines=black");
        HC_KInsert_Line(g_wind_pos.x,g_wind_pos.y,g_wind_pos.z,g_wind_pos.x+a.x,g_wind_pos.y+a.y,g_wind_pos.z+a.z,g_World->GNode());

stepsize = stepsize/NO_OF_STEPS;
z=minHeight;
a.x = line.x * z +g_wind_pos.x;
a.y = line.y * z +g_wind_pos.y;
a.z = line.z * z +g_wind_pos.z;


awadummy = g_AWA; g_AWA=0.0;

for(i=0;i<= NO_OF_STEPS;i++) {

	appwind_(&z,&u,&v,&w);
	speed = sqrt(u*u + v*v + w*w);
	sprintf(valStr,"z=%f v=%6.2f",z, speed);
	HC_Insert_Text(a.x,a.y,a.z,valStr);
	b.x = a.x + u;
	b.y = a.y + v;
	b.z = a.z + w;
	memcpy(&(msh[i][0]),&a,sizeof(VECTOR));
	memcpy(&(msh[i][1]),&b,sizeof(VECTOR));
	spd[i][0] = spd[i][1] = speed;

	z += stepsize;
	a.x = line.x * z +g_wind_pos.x;
	a.y = line.y * z +g_wind_pos.y;
	a.z = line.z * z +g_wind_pos.z;

}

 key=HC_KInsert_Mesh(NO_OF_STEPS+1,2,msh);

vertoff = 0;
nvert = (NO_OF_STEPS+1)*2;
 HC_MSet_Vertex_Colors_By_FIndex(key,"faces",vertoff,nvert,&(spd[0][0]));

/*  for(i=0;i<NO_OF_STEPS;i++) {
	a.x = ((msh[i][0]).x + (msh[i][1]).x + (msh[i+1][0]).x + (msh[i+1][1]).x)/4;
	a.y = ((msh[i][0]).y + (msh[i][1]).y + (msh[i+1][0]).y + (msh[i+1][1]).y)/4;
	a.z = ((msh[i][0]).z + (msh[i][1]).z + (msh[i+1][0]).z + (msh[i+1][1]).z)/4;
	speed = ((spd[i][0]) + (spd[i][1]) + (spd[i+1][0]) + (spd[i+1][1]))/4;

} */ 

g_AWA = awadummy;

HC_Close_Segment();
HC_Close_Segment();
#endif
return(1);
}
 

