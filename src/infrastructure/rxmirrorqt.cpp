#include "StdAfx.h"
#include <QtCore>
#include <QSqlQuery>

#include <QtSql>
#include <QDebug>
#include "stringutils.h"
#include "summary.h"
#include "RXDatabaseI.h"
#include "words.h"
#include "global_declarations.h"
#include "RelaxWorld.h"
#include "rxmirrorqt.h"
#include "rxmainwindow.h"
RXMirrorQt::RXMirrorQt():
    m_PumpThrd(0)
{
}
RXMirrorQt::~RXMirrorQt()
{
    QString     q=QString(" DROP TABLE IF EXISTS  ") + QString(RXMIRRORTABLE);
    QSqlQuery qq(q,m_db);qq.finish();
    q=QString(" DROP TABLE IF EXISTS `follower` ");
    QSqlQuery q2(q,m_db);q2.finish();

    m_db.close();
    if(m_PumpThrd )
        delete this->m_PumpThrd;
    m_PumpThrd=0;
    QSqlDatabase::removeDatabase( m_db.connectionName());

    Close();
}

RXMirrorQt::RXMirrorQt(QString &s):
    m_PumpThrd(0)
{ //MYSQL$localhost$dbname$user$password$myname No table;
    bool ok;
    QStringList w = s.split("$");
    if(w.size()<5 )return  ;
    if(w.size()<6 ) w<<"somename";
    w[0]= w[0].toUpper();
    if(!w[0].startsWith("Q"))  w[0].prepend("Q");
    while(QSqlDatabase::contains(w[5]))
        w[5]+='m';
    m_db = QSqlDatabase::addDatabase(w[0],w[5]);
    m_db.setHostName(w[1]);
    m_db.setDatabaseName(w[2]);
    m_db.setUserName(w[3]);
    m_db.setPassword(w[4]);
    ok = m_db.open();
    if(!ok) {
        QSqlError ee=m_db.lastError ();
        QString  message = "cannot open DB  : "+ ee.text();
        g_World->OutputToClient(message,4);
        qDebug()<<message;
    }
}

std::string  RXMirrorQt::ScriptCommand() const
{
    QString rv("open_mirror:qt: ");
    return rv.toStdString() + GetFileName()   ;
}
std::string  RXMirrorQt::GetFileName() const
{
    QString rv = m_db.driverName(); rv.remove(0,1); rv.toUpper();
    rv+= "$" +m_db.hostName()+"$";
    rv+= m_db.databaseName()+"$"  ;
    rv+= m_db.userName()+"$"+m_db.password()+"$";
    rv+=m_db.connectionName();
    return rv.toStdString()   ;
}

void RXMirrorQt::SetLast(const QString h, const QString v) // remember that the DB pump is in another thread so we use signal/slot to communicate
{
    //   qDebug()<< "DONT  Set Last "<<h<<v;//WE MIGHT TRY MAKING  the setlast and pump calls blocking
    //       emit this->SetLastSignal(h,v);;
}
int RXMirrorQt::Open()
{
    if(! m_db.isOpen())
        return 0;
    if(!IsMirror())
        return (0);
    QString q("SET sql_mode='STRICT_ALL_TABLES'");
    QSqlQuery qqqqq(q,m_db);
    if(!qqqqq.isActive())
        qDebug()<<qqqqq.lastError().text();
    qqqqq.finish();

    q=QString(" DROP TABLE IF EXISTS  ") + QString(RXMIRRORTABLE);
    QSqlQuery qq(q,m_db);qq.finish();

    // create the mirror table
    q=QString("CREATE TABLE IF NOT EXISTS ")+ QString(RXMIRRORTABLE ) + QString( "("
                                                                                 "`header` VARCHAR(64),"
                                                                                 "`text` VARCHAR(256),"
                                                                                 "`type` integer DEFAULT 0,"
                                                                                 "`flag` integer DEFAULT 0,"
                                                                                 "`id` int(11) unsigned NOT NULL auto_increment,"
                                                                                 "PRIMARY KEY  (`id`)"
                                                                                 ") "
                                                                                 );
    QSqlQuery qqq(q,m_db);
    if(!qqq.isActive())
        qDebug()<<qqq.lastError().text();
    qqq.finish();

    // follower
    q=QString(" DROP TABLE IF EXISTS `follower` ");
    QSqlQuery q2(q,m_db);q2.finish();

    // create the audit table

    q=QString("CREATE TABLE  `follower` ("
              "`id` int(11) unsigned NOT NULL auto_increment,"
              " `content_id` int(11) NOT NULL default '0',"
              "  `head` mediumtext,"
              "`before_value` mediumtext,"
              " `after_value` mediumtext,"
              "`type` integer DEFAULT 0,"
              "`flag` integer DEFAULT 0,"
              " `ack` integer DEFAULT 0,"
              " `date_created` timestamp NOT NULL default CURRENT_TIMESTAMP"
              "           on update CURRENT_TIMESTAMP,"
              " PRIMARY KEY  (`id`)"
              ") ;"
              );
    QSqlQuery q3(q,m_db);
    if(!q3.isActive())
        qDebug()<<q3.lastError().text();
    q3.finish();

    qDebug()<< " we have two problems with the follower table"
            << "first: we shouldnt post  lines which arent 'input' "
               "second, we shouldnt post when the value is unchanged"
               "and Hugely, this can flipflop too !";
    //
    //              "if OLD.text != new.text   and old.flag&2 and old.type&2 then "
    q=QString(" CREATE TRIGGER onChange BEFORE UPDATE ON ")
            + QString(RXMIRRORTABLE )
            + QString(" FOR EACH ROW BEGIN "
                      " if  old.text <> new.text and old.flag&2  then   "
                      "    INSERT INTO follower SET content_id = OLD.id;"
                      "    UPDATE follower SET before_value = OLD.text"
                      "          WHERE id = last_insert_id();"
                      "    UPDATE follower SET after_value = NEW.text"
                      "      WHERE id = last_insert_id();"
                      "    UPDATE follower SET type = OLD.type"
                      "      WHERE id = last_insert_id();"
                      "    UPDATE follower SET flag = OLD.flag"
                      "     WHERE id = last_insert_id();"
                      "    UPDATE follower SET head = OLD.header"
                      "     WHERE id = last_insert_id();"
                      "  end if ;"
                      "  END"
                      "       ");

    QSqlQuery q5(q,m_db);
    if(!q5.isActive())
        qDebug()<<q5.lastError().text();
    q5.finish();

    // start the thread which pumps the mirror-table's values to the App
    QString  xx = QString::fromStdString(this->GetFileName());

    m_PumpThrd = new RXDBLinkThreadLibQT(NULL, xx );
    m_PumpThrd->start();

    return 1;
}

int RXMirrorQt::Close ()
{
    if(!m_db.isOpen()) return 0;
    m_db.close();
    return 1;
}
int RXMirrorQt::StartTransaction () { //crashes the pump
    if(!m_db.isOpen()) return 0;
//   return  m_db.transaction();
}
int RXMirrorQt::Commit() {
    if(!m_db.isOpen()) return 0;
//    return m_db.commit();
}

int RXMirrorQt:: FlushMirrorTable ()
{
    int rc=0;
    if(!m_db.isOpen()) return 0;

    QString qs ;
    qs=QString("DELETE FROM ") + QString(RXMIRRORTABLE );
    QSqlQuery q(qs,m_db);
    if(!q.isActive())
        qDebug()<<q.lastError().text();


    return rc;
}

int  RXMirrorQt::post_summary_value(const char*label,const char*pvalue)
{
    if(!m_db.isOpen())
        return 0;
    if(!label) return 0;
    QString qs;

    int rc=0, k, l_type,l_flag ;

    const char*value="(null)";
    if(pvalue)
        value = pvalue;

    qs=   QString("SELECT * FROM ") + QString(RXMIRRORTABLE)
            +QString(" WHERE header LIKE '") +QString(label)+QString("'") ;

    QSqlQuery q1(qs,m_db);
    if(!q1.isActive())
        qDebug()<<q1.lastError().text();

    k=q1.size(); q1.finish();
    if(k>0) {
        qs=   QString("UPDATE ") + QString(RXMIRRORTABLE)
                +QString(" SET     text = '") +QString(value)
                +QString("' WHERE header LIKE '") +QString(label)+QString("'") ;

        QSqlQuery q2(qs,m_db);
        k=q2.numRowsAffected();
        if (q2.lastError().isValid())
            qDebug() << " RXMirrorQt::post_summary_value (1) error "<<q2.lastError();
        rc = k;   q2.finish();

    }
    else  { // it doesnt exist
        // INSERT INTO pet VALUES ('Puffball','Diane','hamster','f','1999-03-30',NULL);
        l_type   = RXMirrorDBI::Summary_Type(label,&l_flag);

        // this changed when we added the 'id' column
        qs=   QString("INSERT INTO ") + QString(RXMIRRORTABLE)
                +QString("( header,text,type,flag) VALUES  ( '") +QString(label)
                +QString("' , '") +QString(value)
                +QString ("',%1").arg(l_type )
                +QString ( ",%1  ) ").arg(l_flag)  ;

        // or insert into table  INSERT INTO tbl_name (col1,col2) VALUES(15,col1*2);

        QSqlQuery q2(qs,m_db);
        if (q2.lastError().isValid()){
            qDebug() << " RXMirrorQt::post_summary_value (2) error "<<q2.lastError();
            qDebug()<< "Query was "<< qs;
            qDebug()<< "Query length "<< qs.size();
        }
        if(q2.first())
            rc++;
        rc += q2.numRowsAffected(); q2.finish();
    }

    return rc>=0;
}


int RXMirrorQt:: post_summary_value(const std::string &label,const  std::string &value)
{
    return post_summary_value(label.c_str(),value.c_str());
}
int RXMirrorQt:: PostFile( const QString &label,const QString &fname)
{
    return post_summary_value( qPrintable(label) ,qPrintable( fname)  );
}
//searches in the mirror for 'label'. returns value.
int  RXMirrorQt::Extract_One_Text(const char *label,char*value,const int buflength)
{
    int rc=0;
    QString q,r;

    q=   QString("SELECT `text` FROM ") + QString(RXMIRRORTABLE)
            + " WHERE header= '"+QString(label)+"'";
    QSqlQuery query(q,m_db);
    if(!query.isActive())
        qDebug()<<query.lastError().text();
    rc=query.size();
    if(rc<0)
    {    return 0;}
    while (query.next()) {
        r = query.value(0).toString();
        strncpy(value,qPrintable(r) ,buflength);rc++;
    }

    return rc;
}

std::string  RXMirrorQt::Extract_Headers(const string &pwhat)   //is 'input', 'output' or 'all'
{
    QString rs;
    int flag;

    if(pwhat=="input") flag = SUM_INPUT;
    else if(pwhat=="output" ) flag = SUM_OUTPUT;
    else flag = SUM_OUTPUT+SUM_INPUT;
    QString what,sf; int f;
    QString qs=   QString("SELECT header, flag FROM ") + QString(RXMIRRORTABLE) ;
    QSqlQuery q(qs,m_db);
    if(!q.isActive())
        qDebug()<<q.lastError().text();
    QSqlRecord rec = q.record();
    qDebug() << "Number of columns: " << rec.count();

    while (q.next())
    {
        qDebug() << q.value(0).toString() << " flag = "<< q.value(1).toString() ;
        what = q.value(0).toString() ;
        sf =q.value(1).toString();
        f=sf.toInt();
        if(f&flag)
            rs+=what + ",";
    }
    rs.chop(1);

    return rs.toStdString();
}

int  RXMirrorQt::Replace_Words_From_Summary(char **strptr, const int append)
{
    /* if $header is found in buf, replace it with text
If 'append' is non-zero replace $AWA with $AWA=<value>
 or replace any string from $AWA to > (but not containing '$')  with $AWA=<value>
 */
    int   c=0;
    char buf[256], bnew[256], *cp, *a,*b, *d,*stmp;

    if(!strptr) return 0;
    if(! *strptr) return 0;
    QString what,val;
    QString qs=   QString("SELECT header,text FROM ") + QString(RXMIRRORTABLE) ;

    QSqlQuery q(qs,m_db);
    if(!q.isActive())
        qDebug()<<q.lastError().text();
    while (q.next())
    {
        // qDebug() <<"RXMirrorQt::Replace_Words_From_Summary "<< q.value(0).toString() << "  text= "<< q.value(1).toString() ;
        what = q.value(0).toString() ;
        val =q.value(1).toString() ;

        sprintf(buf,"$%s",qPrintable(what));
        if(append)
        {
            sprintf(bnew,"%s=<%s>", buf, qPrintable(val));
            stmp=STRDUP(*strptr);	RXFREE(*strptr);
            cp = Replace_String(&stmp, buf,bnew);
            *strptr=stmp;
            if(cp) {
                /* now strip $AWA=<21.936>=<22.157> to $AWA=<21.936> */
                cp += strlen(bnew);
                a = strchr(cp,'$'); b = strchr(cp,'>');
                if((b &&a &&(b < a)) || ( b && !a)) {
                    for( d = cp; d<=b; d++) *d = ' ';
                    PC_Strip_Leading_Chars(cp," ");
                }
                c++;
            }

        }
        else  {
            if(NULL !=Replace_String(strptr, buf,qPrintable(val)))
                c++;;
        }

    }
    return c;
}

//remove-all-with-prefix operates on the mirror
int  RXMirrorQt::RemoveAllWithPrefix(const std::string & prefix)
{
    //DELETE FROM mirror where header like '$boat$%'

    QString qs=   QString("DELETE FROM ") + QString(RXMIRRORTABLE) ;
    qs +="  WHERE header LIKE '$"+QString(prefix.c_str())+"$%'";
    QSqlQuery q(qs,m_db);
    if(!q.isActive())
        qDebug()<<q.lastError().text();
    return 1;
}

//  synch the app to the Mirror
int RXMirrorQt::Apply_Summary_Values()
{
    int rc=0;
    int  type, flag;;
    QString what,val;
    QString qs=   QString("SELECT header,text FROM ") + QString(RXMIRRORTABLE) ;
    {
        QSqlQuery q(qs,m_db);
        if(!q.isActive())
            qDebug()<<q.lastError().text();
        while (q.next())
        {
            what = q.value(0).toString();
            val =q.value(1).toString();
            type   =  RXMirrorDBI::Summary_Type(qPrintable(what) ,&flag);
            Justapply_one_summary_item(qPrintable(what), qPrintable(val), type, flag);

        }
    }
    rc= g_World->Bring_All_Models_To_Date();

    return rc;
}
// create-filtered-  posts from a mirror to a mirror   - called in write_ND_state
int RXMirrorQt::Create_Filtered_Summary(class RXMirrorDBI *snew,char *pfilter)
{
    int flag;
    int retVal=0;
    if(!snew)
        return 0;
    QString header,val ,filter(pfilter);
    QString qs=   QString("SELECT header,text FROM ") + QString(RXMIRRORTABLE) ;

    QSqlQuery q(qs,m_db);

    while (q.next())
    {
        header = q.value(0).toString();
        val =q.value(1).toString();
        if( filter.contains(header, Qt::CaseInsensitive) || header.contains(filter, Qt::CaseInsensitive )) {
            RXMirrorDBI::Summary_Type(qPrintable(header) ,&flag);
            if(!(flag &SUM_INPUT))
                continue;
            if( !val.contains("N/A")){ // should also test that its a sum_input;
                snew->post_summary_value(qPrintable(header),qPrintable(val));
                retVal++;
            }
        }
    }
    return(retVal);
}
int RXMirrorQt::Write_Script (FILE *fp)
{
    int flag;
    int retVal=0;
    if(!fp)
        return 0;
    fprintf(fp,"\n!  (from Qt mirror)\n");
    QString header,val;
    QString qs=   QString("SELECT header,text FROM ") + QString(RXMIRRORTABLE) ;

    QSqlQuery q(qs,m_db);

    while (q.next())
    {
        header = q.value(0).toString();
        val =q.value(1).toString();
        RXMirrorDBI::Summary_Type(qPrintable(header) ,&flag);
        if(!(flag &SUM_INPUT))
            continue;
        if(val.contains("N/A"))
            continue;
        retVal+= fprintf(fp,"apply: %s: %s\n",  qPrintable(header),qPrintable(val)  );
    }
    return(retVal);
}

RXDBLinkThreadLibQT::RXDBLinkThreadLibQT(QObject *parent, const QString &s) :
    QThread(parent),
    m_s(s),
    m_PleaseEndPump(false)
{
    connect(this,SIGNAL(RXDBCommand(QObject*,QString)),g_rxmw,SIGNAL(RXCommand(QObject*,QString)));//,Qt::BlockingQueuedConnection);

}

RXDBLinkThreadLibQT::~RXDBLinkThreadLibQT()
{
    this->m_PleaseEndPump = true;
    wait();
    qDebug()<<"RXDBLinkThread killed";
    m_dbP.close();

    QSqlDatabase::removeDatabase( m_dbP.connectionName());
}
// this loop is fairly resource-intensive so we poll every 5 seconds.
// When  something happens we poll faster then gradually ramp up to 5000 again
// recall that the trigger places a record of each change to the mirror table , in the audit table
// we  search for unconsumed (ack=0) records in the audit table
// for each one we emit an 'apply' command and we set 'ack' to 1

void RXDBLinkThreadLibQT::run()
{
    int count=0;
    //MYSQL$localhost$ofsidb$user$password$myname ;
    bool ok;
    QStringList w = m_s.split("$");
    if(w.size()<6 )return  ;
    w[0].prepend("Q");
    while(QSqlDatabase::contains(QString("pump_") +w[5]))
        w[5]+='m';
    m_dbP = QSqlDatabase::addDatabase(w[0],QString("pump_") +w[5]);
    m_dbP.setHostName(w[1]);
    m_dbP.setDatabaseName(w[2]);
    m_dbP.setUserName(w[3]);
    m_dbP.setPassword(w[4]);
    ok = m_dbP.open();
    if(!ok) qDebug()<<"cannot open pump DB"<<this->m_s<<m_dbP.lastError().text();

    int sleeptime = 509;

    assert( SUM_INPUT  ==2);
    qDebug()<<" Starting  DB pump";
    while (! this->m_PleaseEndPump )
    {
        if(!m_dbP.isOpen())
            break;
        count=0;
        QString line, id, head,before_value ,after_value ,ack; int  type,flag;
        QString qss=QString("SELECT id,head,before_value,after_value,ack,type,flag from `follower` WHERE (type & 2 and flag & 2 and ack=0 )");

        QSqlQuery q(qss, m_dbP);
        if(!q.isActive())
            qDebug()<<  "(in Pump Thread) "<< q.lastError().text();
        if(q.size()>0)
            qDebug()<< " pumpthread returned size = "<<q.size()<< " numRowsAffected="<<q.numRowsAffected();
        while (q.next())
        {
            id              = q.value(0).toString();
            head            = q.value(1).toString();
            before_value    = q.value(2).toString();
            after_value     = q.value(3).toString();
            ack             = q.value(4).toString();
            type            = q.value(5).toInt(); assert(type&2);
            flag            = q.value(6).toInt();  assert(flag&2);

            if( before_value!=after_value) // with the selective trigger this must be true
            {
                count++;
                line =  "apply:"+head+":"+after_value + " !old was '" + before_value +"' (via DB pump, count=" + QString("%1").arg (count) +")";
                emit this->RXDBCommand(g_rxmw,line); // forwards to signal RXCommand
            }

//   set ack to 1 so we don't consume it again.
            QString qs3 =   QString("UPDATE `follower` SET ack= 1  WHERE id= ") +id;
            QSqlQuery q3(qs3, m_dbP);
            if(!q3.isActive())
                qDebug()<<  "(in Pump Thread) SQL error"<< q3.lastError().text();
            q3.finish();
        }

        if(count) {
            // shrink the follower table
//            QString qs2 =   QString("DELETE FROM `follower` WHERE ack= 1");
//            QSqlQuery q2(qs2, m_dbP);
//            if(!q2.isActive())
//                qDebug()<<  "(in Pump Thread) SQL error"<< q2.lastError().text();
//            q2.finish();

            sleeptime = 51;
            emit this->RXDBCommand(g_rxmw,QString("process changes ! (via DB pump) ")); //was  emit g_rxmw->RXCommand(g_rxmw,QString("process changes! (via DB pump) "));
            count=0;
        }
        else
            sleeptime = min(sleeptime + 17 ,5000);

        this->msleep(sleeptime); //  // a strange number so it will eventually fall out of step with any other timers
        //   qDebug()<<" pumping....";
    } //while no 'pleaseEnd'
    qDebug()<<"Quitting  DB pump";
    return;
}



