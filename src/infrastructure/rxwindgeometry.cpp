#include "StdAfx.h"
#include "RelaxWorld.h"
#include <opennurbs.h>
#include "getfilename.h"
#include "Gflags.h"
#include "rxwindgeometry.h"

RXWindGeometry::RXWindGeometry():

    m_ReflectHeight(0),
    m_DoStreamline(0),
    m_Course(0),
    m_Vmc (0),
    m_AwaFact  ( 1.0 ),  //  1 means pansail is run in AWA axes, 0 means run in boat axes
    m_PansailDone(0),
    m_keepWake(0),
    m_nrelax(0),
    m_xyplane(2),
    m_isetup(0),	/* always do the generate nr matrix */
    m_windName (""),
    m_Appwind_Untransformed(0) ,	 // true for relax LL
    Pitch_Velocity(0) ,  /* bow going down */
    Roll_Velocity(0) ,  /* roll to stbd */
    Flow_Separation(0),
    m_TWA(ON_PI/4.0),
    m_TWS (4 ),
    m_Vboat (4  ),
    m_Leeway(0),
    m_sailpp_N(13),
    m_sailpp_Flags(0),
    m_relfac (0.005),
    m_Reference_Length(10) ,
    zz(0),
    vv(0),
    th(0),
    n(0)
{
    // remember  atan2 (0,0) is zero
    m_windPos=ON_3dPoint(-20,0,0  ) ;
    m_AppWind.z=0;
    this->SetLeeway(0.0); // force the wind triangle to solve

}
int RXWindGeometry::Set (const QString &pheader, const double value) {
    QString header = pheader.simplified();
    if(header.contains("True Wind sp",Qt::CaseInsensitive)) {
        if(fabs(m_TWS - value) > 0.001){
            SetTWS( value);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            return 1;
        }
    }
    if(header.contains("TWS",Qt::CaseInsensitive) ) {
        if(fabs(m_TWS - value) > 0.001){
            SetTWS( value);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            return 1;
        }

    }
    else if(header.contains("boat s",Qt::CaseInsensitive)) {
        /* boatspeed */
        if(fabs(m_Vboat - value) >0.0001){
            SetVBoat (value);
            Post_VMC();
            mkGFlag(GLOBAL_DO_APPWIND);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            return 1;
        }

    }
    else if(header.contains("Course",Qt::CaseInsensitive))  {
        /* direction in which vmc is measured*/
        m_Course = value;
        Post_VMC();
        return 1;
    }
    else if((header.contains("reflection height",Qt::CaseInsensitive))) {
        /* reflection plane height */
        if(fabs(m_ReflectHeight- value) > 0.001) {
            m_ReflectHeight = value;
            mkGFlag(GLOBAL_DO_REFLECTION);
            mkGFlag(GLOBAL_DO_APPWIND);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            return 1;
        }

    }

    else if((header.contains("leeway",Qt::CaseInsensitive))) {
        if(fabs(m_Leeway - value) >0.001) {
            SetLeeway   (value);
            Post_VMC();
            mkGFlag(GLOBAL_DO_APPWIND);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            return 1;
        }

    }
    else if( header.contains("heading",Qt::CaseInsensitive) ||  header.contains("twa",Qt::CaseInsensitive)     ) {
        if(fabs(m_TWA - value) > 0.001) {
            SetHeading(value);
            mkGFlag(GLOBAL_DO_APPWIND);
            mkGFlag(GLOBAL_DO_REFLECTION);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            Post_VMC();
            return 1;
        }

    }
    else if((header.contains("reference height",Qt::CaseInsensitive))) {
        if(fabs(m_Reference_Length - value) > 0.001) {
            m_Reference_Length=value;
            mkGFlag(GLOBAL_DO_APPWIND);
            mkGFlag(GLOBAL_DO_REFLECTION);
            mkFlag(g_Analysis_Flags,WIND_CHANGE);
            Post_VMC();
            return 1;
        }

    }
    else
        assert(0);
    return 0;
}

int RXWindGeometry::Set (const QString &line, const int pfirst_time )
{
    int first_time = pfirst_time; first_time=1;
    double f=0;
    QStringList wds = line.split(QRegExp("[:\t]"));
    if(wds.size()<2)
        return 0;
    QString &fw=wds.first();
    QString &lw = wds.last();

    if(fw.contains("Wind File",Qt::CaseInsensitive)) {
        m_windName = QString(lw);
        cout<<" Wind File now "<<  qPrintable(m_windName)<<endl;
        return 1;
    }
    else if(fw.contains("reference height",Qt::CaseInsensitive)) {  // 0 to 10
        if(first_time){
            m_Reference_Length =lw.toDouble();
            cout<<"Reference Height set to  "<< m_Reference_Length<<endl;
            return 1;
        }
    }
    else if(fw.contains("True Wind Speed",Qt::CaseInsensitive) ||fw.contains("tws",Qt::CaseInsensitive)) {  // 0 to 10
        if(first_time){
            SetTWS(  lw.toDouble());
            cout<<"True Wind Speed set to "<<m_TWS <<endl;
            return 1;
        }
    }
    else if(fw.contains("heading",Qt::CaseInsensitive) ||fw.contains("TWA",Qt::CaseInsensitive)   ) {
        if(first_time){
            SetHeading(lw.toDouble());
            cout<<"Heading set to "<<m_TWA <<endl;
            return 1;
        }
    }
    else if(fw.contains("Leeway",Qt::CaseInsensitive)) {  // 0 to 10
        if(first_time){
            SetLeeway ( lw.toDouble());
            cout<<"Leeway set to "<<m_Leeway <<endl;
            return 1;
        }
    }
    else if(fw.contains( "Boatspeed",Qt::CaseInsensitive)) {
        if(first_time){
            SetVBoat (lw.toDouble()) ;
            cout<<"Boatspeed set to "<<m_Vboat<<endl;
            return 1;
        }
    }
    else if(fw.contains( "AWAfact",Qt::CaseInsensitive)) {  // 0 to 1
        m_AwaFact =lw.toDouble();
        cout<<"AWA_fact set to "<<m_AwaFact<<endl;
        return 1;
    }
    else if(fw.contains("Separation",Qt::CaseInsensitive)) {  // 0 to 10
        Flow_Separation =   lw.toInt();
        cout<<"Global_Flow_Separation set to "<<Flow_Separation<<endl;
        return 1;
    }
    else if(fw.contains("Pitch Velocity",Qt::CaseInsensitive)) {
        f = lw.toDouble();
        Pitch_Velocity =  f;
        cout<<"Pitch_Velocity set to "<<f<<endl;
        return 1;
    }
    else if(fw.contains("Roll Velocity",Qt::CaseInsensitive)) {
        f = lw.toDouble();
        Roll_Velocity =  f;
        cout<<"Roll_Velocity set to "<<f<<endl;
        return 1;
    }
    else if(fw.contains("wind u",Qt::CaseInsensitive)) {
        f = lw.toDouble();
        SetWindX (f ) ;
        cout<<"Global Wind u set to "<<m_AppWind.x <<endl;
        return 1;
    }
    else if(fw.contains("wind v",Qt::CaseInsensitive)) {
        f = lw.toDouble();
        SetWindY (f );
        cout<<"Global Wind v set to "<< m_AppWind.y<<endl;
        return 1;
    }
    else if(fw.contains("wind w",Qt::CaseInsensitive)) {
        f = lw.toDouble();
        SetWindZ ( f );
        cout<<"Global Wind w set to "<< m_AppWind.z <<endl;
        return 1;
    }
    else if(fw.contains("sailpp N",Qt::CaseInsensitive)) {
        m_sailpp_N= lw.toInt();
        if(m_sailpp_N < 2) m_sailpp_N=2;
        cout<<"sailpp_N set to "<< m_sailpp_N<<endl;
        return 1;
    }
    else if(fw.contains("sailpp flags",Qt::CaseInsensitive)) { // currently  SPP_U_COSINE 2, SPP_V_COSINE 4
        m_sailpp_Flags= lw.toInt();
        cout<<"sailpp flags:  2 for U cosine dist, 4 for V cosine dist.\n";
        cout<<"sailpp flags set to "<<m_sailpp_Flags<<endl;
        return 1;
    }
    else if(fw.contains("Wake Relaxation",Qt::CaseInsensitive)) {
        m_relfac=lw.toDouble();
        cout<<"Wake Relaxation " << m_relfac << "\n";
        return 1;
    }



    return 0;

}

double RXWindGeometry::Get(const  enum  WindDataType what)
{
    switch (what) {
    case Heading:
        return m_TWA;
    case Vwind:
        return m_TWS;
    case  Vboat  :
        return m_Vboat;
    case ReflectHeight:
        return m_ReflectHeight;
    case AWA:
        return  atan2(  m_AppWind.y,m_AppWind.x);
    case  AWA_fact:
        return m_AwaFact;
    case ReferenceLength:
        return m_Reference_Length;
    case AWS:
        return m_AppWind.Length();
    default:
        qDebug()<<" Wind GET for "<<what;

    };
    return ON_UNSET_VALUE;
}


/*
  Analysis August 2012
  This is really nasty old C code

  function appwind_ provides the free-stream wind vector for:


  In CalcNow, we use ot to get AWA, aws then we set AWA to 0 (!)
  In drawwind we display the wind vector as f (z)
  in IntPress we call it to get a reference windspeed to denormalise the cps
  in   RelaxWorld::Post_Wind_Details( )  lots of nasty things happen


  Our problem is that we can specify a wind vector (u,v,w)
  OR we can specify a wind triangle.
  and its not clear how we manage the OR.
  */

#define r2d ((float)57.29577951)
void appwind_(float *z,float *u, float *v, float *w)
{
    g_World->m_wind.appwindfunc(z,u,v,w);
}

void RXWindGeometry::appwindfunc(float *z,float *u, float *v, float *w)
{

    float au,av,aw;

    float theta,speed,Vunfactored;
    au = AppWind(0);
    av = AppWind(1);
    aw = AppWind(2);
    if(m_windName.isEmpty()) {
        m_windName = QString(g_CodeDir)+ QString(_DIRSEP_)+QString("wind.txt");
     //   cout<<"setting windfile to '"<<qPrintable(m_windName)<<"'"<<endl;
    }
    *u = au;		   /* defaults*/
    *v = av;
    *w = aw;
    if(Old_Wind_Name.length()) {
        if(Old_Wind_Name!=m_windName) {
            cout<<" windname was "<< qPrintable(Old_Wind_Name )<<" is "<<qPrintable(m_windName)<<endl;
            RXFREE(zz); RXFREE(vv);RXFREE(th);
            zz=NULL; vv=NULL; th=NULL;
            n=0;
            Old_Wind_Name = m_windName;
        }
    }
    else
        Old_Wind_Name=m_windName;


    if(zz==NULL ) {
        FILE *cycleptr;
        float p,q,r;

        if ((cycleptr = RXFOPEN(qPrintable(m_windName),"rt"))==NULL){
            m_windName.clear();
            m_windName = getfilename( NULL ,"w*.txt","Open WIND file",g_CodeDir,PC_FILE_READ);
            if(!m_windName.isEmpty()) {// was last Graph->topLevel
                cycleptr = RXFOPEN(qPrintable(m_windName),"rt");
            }
            if(!cycleptr) {
                //error("Trying default wind file in code/wind.txt",1);
                m_windName = QString("$R3ROOT/code/wind.txt");
                cycleptr = RXFOPEN(qPrintable(m_windName),"rt");
                if(!cycleptr) {
                    return ;
                }
            }
        }

        while (3== fscanf(cycleptr,"%f %f %f",&p,&q,&r)){

            zz=(float*)REALLOC(zz,(n+1)*sizeof(float) );
            vv=(float*)REALLOC(vv,(n+1)*sizeof(float) );
            th=(float*)REALLOC(th,(n+1)*sizeof(float) );

            zz[n]=p;
            vv[n]=q;
            th[n]=r;
            n++;
        }
        FCLOSE(cycleptr);

    }

    if(n<2) {g_World->OutputToClient(" BAD wind file. Need more points ",2); RXFREE(zz); zz=NULL; return ;}

    WindSpeed(& m_Reference_Length,zz,th,vv,n,&theta,&Vunfactored);  /* unfactored speed at MH */
    WindSpeed(z,zz,th,vv,n,&theta,&speed);

    /*   printf("(APPWIND) ref V=%f at %f  Our Vraw = %f at %f\n",Vunfactored,g_PansailIn.Reference_Length,speed,*z);*/
    speed = speed * m_TWS/Vunfactored;
    /*   printf("so  corrected TWS at %f is %f (vwind=%f)\n" *z,speed,g_Vwind); */

    { double U,V;      /* wind components in global axes */
        double vbx,vby;  /* boat vector in wind axes */
        double cosH,sinH;
        double vappwx,vappwy; /* apparent wind in wind axes */

        U =  speed * cos(theta);
        V =  speed * sin(theta);

        cosH = cos(m_TWA/r2d);
        sinH = sin(m_TWA/r2d);

        vbx =  m_Vboat*cosH ;
        vby = -m_Vboat*sinH ;

        vappwx= vbx+U;
        vappwy= vby+V;
        if(fabs(m_Leeway) > 22.5) {
            g_World->OutputToClient(" unreasonable leeway value",199); assert(0);
            if(m_Leeway>0.0) m_Leeway= 22.5;
            if(m_Leeway<0.0) m_Leeway=-22.5;
        }

        *u =(float) ( cosH * vappwx - sinH * vappwy);
        *v= (float) ( sinH * vappwx + cosH * vappwy - tan(m_Leeway/r2d)*m_Vboat);
        *w= (float) 0.0;
        *u = *u + (float) Pitch_Velocity*(*z);
        *v = *v - (float) Roll_Velocity*(*z);

        if(  !m_Appwind_Untransformed){
            double awa = Get( AWA);
            float ubuf,vbuf;
            ubuf = *u;
            vbuf = *v;
            *u = ubuf * cos(awa) - vbuf*sin(awa);
            *v = vbuf*cos(awa)   + ubuf* sin(awa);
        }


    }
    return;
}

int RXWindGeometry::appwind_get_ref_length_(double *a){
    *a= (double) m_Reference_Length;
    return 0;
}

int RXWindGeometry::WindSpeed(float *z,float*zz,float*th,float*vv,int n,float*theta,float*speed) {
    double factor;
    int i, l=n-2;

    for(i=0;i<n-1;i++) {
        if(zz[i] > *z) { l=i-1;break;}	  /* i is last one just under *z */
    }

    if(l<0)l=0;
    /* interpolate from i to i+1 */

    factor = (*z-zz[l])/(zz[l+1] - zz[l]);
    *theta = -(th[l]*(1.0-factor) + th[l+1] * factor) /r2d;
    *speed = vv[l]*(1.0-factor) + vv[l+1] * factor;

    return(0);
}
int RXWindGeometry::SetHeading( const double h)
{
    this->m_TWA= h* ON_PI/180.0;
    m_AppWind.x =  m_Vboat*cos(m_Leeway) + m_TWS*cos(m_Leeway - m_TWA);
    m_AppWind.y = -(m_Vboat*sin(m_Leeway)) - m_TWS*sin(m_Leeway - m_TWA);
    //   double aws = sqrt(pow(m_TWS,2) + pow(m_Vboat,2) + 2*m_TWS*m_Vboat*cos(m_TWA));
    qDebug()<<" heading now "<< this->m_TWA ;

    return 1;
}

int RXWindGeometry::SetTWS( const double h){
    this->m_TWS=h;
    m_AppWind.x =  m_Vboat*cos(m_Leeway) + m_TWS*cos(m_Leeway - m_TWA);
    m_AppWind.y = -(m_Vboat*sin(m_Leeway)) - m_TWS*sin(m_Leeway - m_TWA);
    //   double aws = sqrt(pow(m_TWS,2) + pow(m_Vboat,2) + 2*m_TWS*m_Vboat*cos(m_TWA));
    qDebug()<<" TWS now "<< this->m_TWS ;

    return 1;
}

int RXWindGeometry::SetLeeway( const double h){
    this->m_Leeway=h * ON_PI/180.0;
    m_AppWind.x =  m_Vboat*cos(m_Leeway) + m_TWS*cos(m_Leeway - m_TWA);
    m_AppWind.y = -(m_Vboat*sin(m_Leeway)) - m_TWS*sin(m_Leeway - m_TWA);
    //   double aws = sqrt(pow(m_TWS,2) + pow(m_Vboat,2) + 2*m_TWS*m_Vboat*cos(m_TWA));

    return 1;
}
int RXWindGeometry::SetVBoat(const double h){
    this->m_Vboat  =h;
    m_AppWind.x =  m_Vboat*cos(m_Leeway) + m_TWS*cos(m_Leeway - m_TWA);
    m_AppWind.y = -(m_Vboat*sin(m_Leeway)) - m_TWS*sin(m_Leeway - m_TWA);
    //   double aws = sqrt(pow(m_TWS,2) + pow(m_Vboat,2) + 2*m_TWS*m_Vboat*cos(m_TWA));


    return 1;
}
int RXWindGeometry::SetWindX( const double h){
    this->m_AppWind.x=h;
    this->m_Vboat=0;
    this->m_Leeway=0;

    this->m_TWA = atan2(m_AppWind.y,m_AppWind.x);
    double s = sin(m_TWA);
    double c = cos(m_TWA);
    if(fabs(s) > fabs(c) )
        this->m_TWS = m_AppWind.y/s;
    else if(fabs(s) <= fabs(c) )
        this->m_TWS = m_AppWind.x/c;
    else
         this->m_TWS = m_AppWind.y/s;
    return 1;
}
int RXWindGeometry::SetWindY( const double h){

    this->m_AppWind.y=h;
    this->m_Vboat=0;
    this->m_Leeway=0;

    this->m_TWA = atan2(m_AppWind.y,m_AppWind.x);
    double s = sin(m_TWA);
    double c = cos(m_TWA);
    if(fabs(s) > fabs(c) )
        this->m_TWS = m_AppWind.y/s;
    else
        this->m_TWS = m_AppWind.x/c;

    return 1;
}
int RXWindGeometry::SetWindZ( const double h){
    this->m_AppWind.z=h;
    return 1;
}
