#ifndef RXDBlinkQt_H
#define RXDBlinkQt_H
#include <rxmirrorqt.h>
#include <QThread>
#include <QSqlDatabase>
#include <map>
//class RXDBLinkMySQL wass superceded by class RXDBlinkQt,which (should) enable all the qt SQL drivers tht you have installed.
// to use the Qt mysql driver you have to have installed libmysql and then re-configure Qt to compile it,
class RXDBlinkQt : public RXDataBaseLogI //  , public RXDBP umperQt The Log classes cannot pump - thats a mirror's job
{
public:
    RXDBlinkQt();
    RXDBlinkQt(const QString &s);
    virtual ~RXDBlinkQt();
    int Open(const char*filename,const char*what ) ;
    int Close (); // the delete

    std::string ScriptCommand() const;
    std::string GetFileName() const ;
    void SetLast(const QString h, const QString v);

// operations on the persistent DB
    //extract-column returns a list of values for  header = head, for all record rows
    virtual int Extract_Column(const char*fname,const char*head,char***clist,int*Nn) ;
    // transfer from mirror to persistent
    virtual int Write_Line (MIRRORPTR mirrorSource) ;
    // transfer from  persistent DB to  Mirror DB
    virtual int read_and_post_one_row(const int row, const QString runName,MIRRORPTR mirrorDest) ;
    virtual bool isOpen() const { return m_db.isOpen(); }
    virtual int MakeUnique(const QString &header, QString &value);

protected:
    QByteArray FileToBlobData(const QString &fname);
    QString BlobDataToFile(const QByteArray &data,  QString &filename);/// returns the filename
    QString TableName()const {return m_table;}
    QSqlDatabase m_db ;
private:
        QString m_table;
};
#endif // RXDBlinkQt_H
