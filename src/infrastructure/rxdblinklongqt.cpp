#include "StdAfx.h"
#include "rxdblinklongqt.h"

RXDBlinkLongQt::RXDBlinkLongQt()
{
}
RXDBlinkLongQt::RXDBlinkLongQt(const QString &s)
    :RXDBlinkQt(s)
    ,m_runTable (TableName()+"_run")
    ,m_DataTable (TableName()+"_data")
{
}
RXDBlinkLongQt::~RXDBlinkLongQt()
{
}
std::string  RXDBlinkLongQt::ScriptCommand() const
{
    if(m_DBtype&RXDB_INPUT)
        return string("open_input: qtlong: ") +this->GetFileName();

    if(m_DBtype&RXDB_OUTPUT)
        return std::string("open_log:qtlong: ") +this->GetFileName();
    assert(0);
    return this->GetFileName();
}
// operations on the persistent DB

//extract-column returns a list of values for  header = head, for all record rows
int RXDBlinkLongQt::Extract_Column(const char*fname,const char*head,char***clist,int*Nn)
{
    int i=0 ;
    assert(m_DBtype&RXDB_INPUT );
    return RXDBlinkQt::Extract_Column(fname,head,clist,Nn);
  }

// transfer from mirror to persistent
int RXDBlinkLongQt::Write_Line (MIRRORPTR mirrorSource)
{
   return RXDBlinkQt:: Write_Line(mirrorSource) ;
}

// transfer from  persistent DB to  Mirror DB
 int RXDBlinkLongQt::read_and_post_one_row(const int row, const QString runName,MIRRORPTR mirrorDest)
 {
    return RXDBlinkQt:: read_and_post_one_row(row,runName,mirrorDest) ;
 }


 int RXDBlinkLongQt::MakeUnique(const QString &header, QString &value)
 {
    return RXDBlinkQt::MakeUnique(header, value) ;
 }



