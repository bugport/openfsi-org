#ifndef RXWINDGEOMETRY_H
#define RXWINDGEOMETRY_H
#include <QString>
#include <opennurbs_point.h>

EXTERN_C void appwind_(float *z,float *u, float *v, float *w);
EXTERN_C int appwind_get_ref_length_(double *a);
EXTERN_C int WindSpeed(float *z,float*zz,float*th,float*vv,int n,float*theta,float*speed);

class RXWindGeometry
{

public:
    enum  WindDataType {
        Heading,
        Vwind,
        Vboat,
        ReflectHeight,
        AWA,
        AWA_fact,
        ReferenceLength,
        AWS
    };

    RXWindGeometry();

    int Set(const QString &line, const int first_time); // typically in GetGlobal
    int Set(const QString &what, const double value); // typically in DB apply

    double Get(const  enum  WindDataType what);
    QString WindName()const { return m_windName;}
    QString SetWindName(const QString &s) { QString old =  m_windName; m_windName=s; return old; }

    int SailppN()const {return m_sailpp_N;}
    int SailppFlags()const{return m_sailpp_Flags;}
    int Post_Wind_Details();
    double AppWind(const int k){return m_AppWind[k];}
    void appwindfunc(float *z,float *u, float *v, float *w);

    int SetHeading( const double h);
    int SetTWS( const double h);
    int SetWindX( const double h);
    int SetWindY( const double h);
    int SetWindZ( const double h);
    int SetLeeway( const double h);
    int SetVBoat(const double h);

public:


    float m_ReflectHeight;
    int m_DoStreamline ;


    double m_Course ;
    double m_Vmc  ;
    float m_AwaFact ;  //  1 means pansail is run in AWA axes, 0 means run in boat axes

    int m_PansailDone ;

    int m_keepWake ;
    int m_nrelax ;
    int m_xyplane ;
    int m_isetup ;	/* always do the generate nr matrix */

    QString m_windName;

    ON_3dPoint m_windPos ;
    int m_Appwind_Untransformed;	 // true for relax LL

    double Pitch_Velocity;  /* bow going down */
    double Roll_Velocity;  /* roll to stbd */

    int Flow_Separation;

private:
    float m_TWA;
    float m_TWS ;
    float m_Vboat ;
    float m_Leeway ;
  //  double m_AWA ; /* used to transform pansail analysis to an axis system where x-axis is along AWA */

    ON_3dVector m_AppWind;//derived
    int m_sailpp_N;
    int m_sailpp_Flags;
    double m_relfac ;
    QString Old_Wind_Name;
    float m_Reference_Length;
    float*zz;
    float*vv;
    float*th;
    int n;

    int Post_VMC();
    int appwind_get_ref_length_(double *a);

    int WindSpeed(float *z,float*zz,float*th,float*vv,int n,float*theta,float*speed);



};

#endif // RXWINDGEOMETRY_H
