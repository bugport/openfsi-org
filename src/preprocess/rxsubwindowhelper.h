#ifndef RXSUBWINDOWHELPER_H
#define RXSUBWINDOWHELPER_H
#include <set>
#include <QString>
typedef	struct {float x, y, z;}	Point;

// the values for GType are:
#define WT_DESIGNWINDOW 1
//#define WT_BOATWINDOW	2
//#define WT_BUILDFILE	3
//#define WT_BAGGEDFILE	4
#define WT_BOATFILE		5
#define WT_VIEWWINDOW	6
//#define WT_MATWINDOW	7

#define WT_DESIGN		10
//#define WT_STATE		11
//#define WT_TPN		12
//#define WT_SOCKET_TYPE 16 // so a socket can own a degenerate GS pointing to it.

class RXSubWindowHelper
{
public:
    RXSubWindowHelper();
    virtual ~RXSubWindowHelper();
    SAIL *GetSail(){return this->GSail;}
public:
    HC_KEY m_ModelSeg;
    HC_KEY m_ViewSeg;
    QString m_qfilename ;	/* eg. a sail filename  */
    SAIL * GSail;

    char *hoops_driver; // synonym for m_ViewSeg
 //   char *basename; // something like 'sailbase'. Better to use WT_TYPE instead

    //   long GType;   /* eg. WT_VIEWWINDOW Peter changed from Type for DB  */
    /* its used in  Close_All_Options (doview.c)
             It is set in nextGraphic (OpenBuildFile.c)
        */
    RXGRAPHICSEGMENT GNode( ) const
    {return m_GlcNode;}            // NO inheritance from parent
    void setGNode(RXGRAPHICSEGMENT n ){  m_GlcNode=n;}

//    static class RXSubWindowHelperTopDesign* rootgraphic;
protected:
    static std::set<RXSubWindowHelper*> m_graphs;
    RXGRAPHICSEGMENT m_GlcNode;

public:
    virtual int Write_ViewLine(FILE *fp) const;
    virtual int Write_StateSailLine(const QString &ScriptFile, FILE *fp,const QString &baseDir) const;
    virtual int Write_HoistSailLine( FILE *fp, const QString &baseDir) const;
    virtual int ConnectAllHoisted( ) ; // only for postproc
    virtual int ConnectToPostProc();   // only for designs
    int Print_Graphic(const char*s) const ;
//    int setStatusBar( const QString &text, const int right =0) const;

    // the statics
    static int snapshot(const QString filein);
    static class RXSubWindowHelper*is_bag_open(const QString filename);
    static int DropAll (void);
    static int Post_All_Hoisted();
    static RXSubWindowHelper *Find_Graphic_By_Camera(const char *cam);
    static int Write_All_Cameras(FILE *fp);
    static int Write_All_States(const char*scriptname, FILE *fp,const QString &baseDir);
    static int Write_All_Hoists(FILE *fp,  const QString &baseDir);
    static int Write_All_Graphics(FILE *fp);

};
class RXSubWindowHelperTopDesign: public RXSubWindowHelper // the boat
{
public:
    RXSubWindowHelperTopDesign();
    ~RXSubWindowHelperTopDesign();

    virtual int Write_StateSailLine( const QString &ScriptFile, FILE *fp,const QString &baseDir) const;
    virtual int Write_HoistSailLine( FILE *fp, const QString &baseDir) const;

    virtual int ConnectToPostProc();
};
class RXSubWindowHelperDesign: public RXSubWindowHelperTopDesign // a normal model ("sail")
{
public:
    RXSubWindowHelperDesign();
    ~RXSubWindowHelperDesign();

    virtual int Write_HoistSailLine( FILE *fp, const QString &baseDir) const;
};


class RXSubWindowHelperPostProc: public RXSubWindowHelper
{
public:
    RXSubWindowHelperPostProc();
    ~RXSubWindowHelperPostProc();
    /* only for views !!! */
    int defaultShell;
    virtual int ConnectAllHoisted( ) ; // only for postproc
    virtual int Write_ViewLine(FILE *fp) const;
};
class RXSubWindowHelperText: public RXSubWindowHelper
{
public:
    RXSubWindowHelperText();
    ~RXSubWindowHelperText();
};
class RXSubWindowHelperMaterial: public RXSubWindowHelper
{
public:
    RXSubWindowHelperMaterial();
    ~RXSubWindowHelperMaterial();
};


#endif // RXSUBWINDOWHELPER_H
