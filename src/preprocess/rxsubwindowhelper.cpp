#include "StdAfx.h"
#include "RelaxWorld.h"
#include <QString>
#include <QFileInfo>
#include <QDir>
#include "RXDatabaseI.h"

#include "rxmainwindow.h"
#include "global_declarations.h"
#include "rxsubwindowhelper.h"
#include "script.h"
#include "RXSail.h"
#include "stringutils.h"
#include "summary.h"

/************************************************************/ /*
NEXT
Think through the relation between these classes and QObject and
objects in RX.
SHould this object be tied to a 'client' or don't the two correlate?

what should the rootgraphic point to ? What's it for?
Or is the ONLY purpose to allow trawling as in Snapshot??


*/
/************************************************************/
// static
std::set<RXSubWindowHelper*> RXSubWindowHelper::m_graphs;
//class RXSubWindowHelperTopDesign* RXSubWindowHelper::rootgraphic;

RXSubWindowHelper::RXSubWindowHelper():
    m_ModelSeg (0),
    m_ViewSeg (0),
    GSail (0),
    hoops_driver (0) ,
    m_GlcNode(0)
{
    m_qfilename = "rxswh";
    m_graphs.insert(this);
    setGNode(RXNewGNode("rxgraphic",0));
}
RXSubWindowHelper::~RXSubWindowHelper()
{
    m_graphs.erase(this);
}
RXSubWindowHelperDesign::RXSubWindowHelperDesign()
{
    m_qfilename="rxswh Design" ;
#ifdef _X
    create_top_design(this,g_graph[0].topLevel);
    XtPopup(this->topLevel,XtGrabNone);
    XFlush(XtDisplay(this->topLevel));
#endif

    this->GSail = new RXSail(g_World);
    this->GSail->SetGraphic(this);
#ifdef RXQT
    RXGRAPHICSEGMENT w = RXNewWindow("Design ", this->GSail);
    this->GetSail()->setGNode(w);
    this->GetSail()->m_viewFrame=0;
#ifdef COIN3D
     this->GetSail()->m_viewFrame =(class RXViewFrame*) w->getUserData() ;
#endif
#endif
}

RXSubWindowHelperDesign::~RXSubWindowHelperDesign()
{
    cout<<" TODO: Unref GNode() and m_postProc" <<endl;
    if(GSail) delete GSail; GSail=0;
}
RXSubWindowHelperTopDesign::RXSubWindowHelperTopDesign() // 'the boat-window'
{
    GSail=0; m_qfilename="rxswh TopDesign" ;
}

RXSubWindowHelperTopDesign::~RXSubWindowHelperTopDesign()
{
    if(GSail) delete GSail; GSail=0;
//    if(this==RXSubWindowHelper::rootgraphic)
//        RXSubWindowHelper::rootgraphic=0;
}

RXSubWindowHelperPostProc::RXSubWindowHelperPostProc():
    defaultShell (0)
{
    m_qfilename="rxswh_pp" ;
    RXGRAPHICSEGMENT w = RXNewWindow("PostProcView", this->GSail);
    this->setGNode(w);
    this->ConnectAllHoisted();
   // RXGNodeWrite("rawPostProcWIn",w);
}
int RXSubWindowHelperPostProc::ConnectAllHoisted( ) //called when we create the postproc
{
    int rc=0;
    SAIL *s;
    class RXSubWindowHelper *g;
    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g =dynamic_cast<class RXSubWindowHelper*> (*it);
        assert(g);
        if(!g->GetSail())
            continue;
        s = g->GetSail();
        if(s->IsHoisted()  ) {
            RXGNodeAddChild(this->GNode(),s->PostProcExclusive() );
            rc++;
        }
    }
    return rc;
}
int RXSubWindowHelperTopDesign::ConnectToPostProc() // call on hoist
{
    int rc=0;
    SAIL *s;
    class RXSubWindowHelperPostProc *g;
    std::set<RXSubWindowHelper*>::iterator it;
    if(! GetSail())
        return 0;
    s =  GetSail();

    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g =dynamic_cast<class RXSubWindowHelperPostProc*> (*it);
        if(!g)
            continue;
        RXGNodeAddChild(g->GNode(),s->PostProcExclusive() );
        rc++;

    }
    return rc;

}

RXSubWindowHelperPostProc::~RXSubWindowHelperPostProc()
{

}
RXSubWindowHelperText::RXSubWindowHelperText()
{
    m_qfilename="rxswh_text" ;
}
RXSubWindowHelperText::~RXSubWindowHelperText()
{

}

RXSubWindowHelperMaterial::RXSubWindowHelperMaterial()
{

}
RXSubWindowHelperMaterial::~RXSubWindowHelperMaterial()
{

}
int RXSubWindowHelper::Print_Graphic(const char*s) const{
    const Graphic *g = this;
    printf("! SubWindow Helper <%s>\n",s);

    printf("! graphic \t%p\n",g);
    printf("! hoops_driver  %s\n",g->hoops_driver);
    printf("! Gsail    %p\t",g->GSail);
    if(g->GSail )
        printf("! sailtype =%s\n",g->GSail->GetType().c_str());
    else
        printf("! Gsail NULL\n");
    printf("! filename %s\n",qPrintable(m_qfilename));	/* eg. a sail filename  */
    return 0;
}


// static

class RXSubWindowHelper* RXSubWindowHelper::is_bag_open(const QString pfilename)
{
    class RXSubWindowHelper*theGraph=NULL;
    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
        if( ! (*it)->m_qfilename.isEmpty())
            if(  pfilename==(*it)->m_qfilename)  {
                theGraph = *it;
                break;
            }
    return(theGraph);
}
int RXSubWindowHelper::DropAll () {
    int   count=0;
    class RXSubWindowHelperDesign *g;

    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g =dynamic_cast<class RXSubWindowHelperDesign*> (*it);
        if(!g)
            continue;
        if((*it)->m_qfilename.isEmpty())
            continue;
        SAIL *s = g->GSail;
        if(s) {
            if(s->IsBoat())
                continue;
            count++;
            printf("  DroppingSail(sEnt) %s %S\n",s->GetType().c_str(),s->GetName().c_str() );
            Do_Drop_Sail( NULL, g);
        }
        else cout<< " NULL s"<<endl;
    }
    return count;
}
int RXSubWindowHelper::ConnectAllHoisted( ) {return 0;} // only for postproc
int RXSubWindowHelper::ConnectToPostProc()  {return 0;}    // only for designs
int RXSubWindowHelper::Post_All_Hoisted(){
    SAIL *s;
    class RXSubWindowHelperDesign *g;
    std::set<RXSubWindowHelper*>::reverse_iterator it;
    for(it=m_graphs.rbegin(); it != m_graphs.rend();++it)
    {
        g =dynamic_cast<class RXSubWindowHelperDesign*> (*it);
        if(!g)
            continue;
//        if(!g->GSail)
//            continue;

        if(!g->m_qfilename.isEmpty()   ) { /* its a sail */
            s = g->GSail;
            if(s->IsHoisted()  ) {
                Post_Summary_By_Sail(s, "file$",qPrintable(g->m_qfilename));
            }
            else {
                Post_Summary_By_Sail(s, "file$","DROPPED");
            }
        }
        else {
            qDebug()<<" g without filename ";
        }
    }
    return 1;
}
Graphic *RXSubWindowHelper::Find_Graphic_By_Camera(const char *cam) {
    char buf[2048];
    class RXSubWindowHelperPostProc *g;

    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g =dynamic_cast<RXSubWindowHelperPostProc*> (*it);
        if(!g)
            continue;
        HC_Open_Segment_By_Key(g->m_ViewSeg) ;
        HC_Show_One_Net_User_Option("petercam",buf);
        HC_Close_Segment();
        if(strieq(buf,cam)) return g;
    }
    return NULL;
}
int RXSubWindowHelper::Write_ViewLine(FILE *fp) const{
    return 0;
}

int RXSubWindowHelperPostProc::Write_ViewLine(FILE *fp) const{
    char buf[512];  *buf=0;

    fprintf(fp,"open camera :");
#ifdef HOOPS
    char  UOs[2048], shortseg[128],l_visibility[2048], seg[256] ;
    HC_Open_Segment_By_Key(g->m_ModelSeg) ;
    HC_Show_One_Net_User_Option("petercam",buf);
    fprintf(fp,"   %s  ",buf);

    HC_Begin_Segment_Search("models/*");
    while(HC_Find_Segment(seg)) {
        HC_Parse_String(seg,"/",-1,shortseg); // can stay
        fprintf (fp," : %s$ ",shortseg);
        if(HC_QShow_Existence(seg,"user options")){
            HC_QShow_User_Options(seg,UOs);
            fprintf (fp," %s ",UOs);
        }
        if(HC_QShow_Existence(seg,"visibility")){
            HC_QShow_Visibility(seg,l_visibility);
            fprintf (fp," , visibility=""%s"" ",l_visibility);
        }
    }
    HC_End_Segment_Search();

    HC_Close_Segment();

#ifdef linux
    HC_Open_Segment_By_Key(g->m_OverlaySeg); {
        HC_Open_Segment("text_overlay"); { // in overlaySeg
            HC_Define_System_Options("C string length=511");
            HC_Show_One_Net_User_Option("user_text",buf);
            HC_Define_System_Options("C string length=255");

        } HC_Close_Segment();
    } HC_Close_Segment();
#endif
#endif
    if(!rxIsEmpty(buf)) {
        PC_Replace_Char(buf, '\n', '~');
        fprintf(fp," : text : %s  \n", buf);
    }

    //  RX_Insert_Line( 0, 0, 0, 10, 11, 12, this->GNode() );

    //   RXGNodeWrite("postproc.txt",  this->GNode());
    return 1;
}

int RXSubWindowHelper::Write_StateSailLine(const QString &ScriptFile, FILE *fp ,const QString &baseDir)const{
    return 0;
}
//int RXSubWindowHelperDesign::Write_StateSailLine(const QString &ScriptFile, FILE *fp,const QString &baseDir )const{
//    return RXSubWindowHelperTopDesign::Write_StateSailLine( ScriptFile, fp, baseDir );
//}
int RXSubWindowHelperTopDesign::Write_StateSailLine(const QString &ScriptFile, FILE *fp,const QString &baseDir )const{

      std::string sailname;
    QString statename;
    SAIL *s = this->GSail;
    if(s){
        sailname = s->GetType() ;
        if(sailname.size()) {
            statename=ScriptFile;
            int k = statename.lastIndexOf(".");
            if( k>=0 ) statename.remove(k,statename.size());
            statename+= QString("_")  +  QString(sailname.c_str())+  QString(".ndd");
            if(!s->IsHoisted ()  )
                fprintf(fp, " ! %s probably not hoisted (NULL matrix) (hoisted flag) \n",sailname.c_str());
            else {

                QStringList wds;
                wds<< "Save State"<<sailname.c_str()<< statename;
                Do_Save_State(wds,NULL);
                statename = s->RelativeFileName(statename,baseDir);
                fprintf(fp,"load state : %s : %s   \n", sailname.c_str(), qPrintable(statename));
            }
        }
        else
            fprintf(fp,"! Load state :no sailname\n");
    }
    return 1;
}
int RXSubWindowHelper::Write_HoistSailLine( FILE *fp, const QString &baseDir) const {
    fprintf(fp,"!RXSubWindowHelper::Write_HoistSailLine\n");
    return 0;
}
int RXSubWindowHelperTopDesign::Write_HoistSailLine( FILE *fp, const QString &baseDir) const {
    fprintf(fp,"\n!RXSubWindowHelperTopDesign::Write_HoistSailLine \n") ;
    return 0;
}
int RXSubWindowHelperDesign::Write_HoistSailLine( FILE *fp, const QString &baseDir) const {
    SAIL *s = this->GSail;
    if(s){
        if(!s->IsHoisted ()) {
            fprintf(fp, " ! %s probably not hoisted (hoisted flag ) \n",qPrintable(this->m_qfilename));
            return 1;
        }
        if(s->GetFlag(FL_NEEDS_SAVING)) {
            char buf[256];
            sprintf (buf," Please save the %s and then Save Session again",s->GetType().c_str());
            s->OutputToClient(buf,2);
        }
        QString bag(s->m_fileBAG );
        QString shortFile = s->RelativeFileName(bag, baseDir );
        fprintf(fp,"Hoist : %s\n", qPrintable(shortFile));

        return 1;
    }
    fprintf(fp,"!Hoist : null\n" );
    return 0;
}

int RXSubWindowHelper::Write_All_Hoists(FILE *fp , const QString &baseDir){

    const class RXSubWindowHelper*g;
    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g = (*it);
        if(!g->GSail)
            continue;
        if(!g->GSail->IsBoat())
            g->Write_HoistSailLine( fp,baseDir);
    }
    return 1;
}
int RXSubWindowHelper::Write_All_States(const char*scriptname, FILE *fp,const QString &baseDir){

    const class RXSubWindowHelper *g;
    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g =  (*it);
        if(g)
            g->Write_StateSailLine(scriptname,fp, baseDir);
    }
    return 1;
}

int RXSubWindowHelper::Write_All_Cameras(FILE *fp){

    const class RXSubWindowHelper *g;
    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g = (*it);
        g->Write_ViewLine(fp);
    }
    return 1;
}
int RXSubWindowHelper::Write_All_Graphics(FILE *fp){
    int j=0;
    Graphic *g;

    std::set<RXSubWindowHelper*>::iterator it;
    for(it=m_graphs.begin(); it != m_graphs.end();++it)
    {
        g = (*it);
        fprintf(fp, "\n!RXSubWindowHelper %d \n!\tfilename='%s'\n!\tdriver=' %s'\n!\t\n",j++, qPrintable(g->m_qfilename), g->hoops_driver);
        fprintf(fp, "! Gnode is  %p\n", g->GNode());
        fflush(fp);
#ifdef HOOPS
        HC_KEY k=g->m_ViewSeg;
        char buf[256],status[256],type[256];
        if(0) {
            if(k){
                HC_Show_Key_Status(k,status);
                if(strieq(status,"invalid"))
                    fprintf(fp, "! \t\t ViewSeg status %s\n",status);
                else {
                    HC_Show_Key_Type(k,type);
                    if(strieq(type,"segment")){
                        HC_Show_Segment(k ,buf);
                        fprintf(fp, "! \t\t ViewSeg  %s\n",buf );
                    }
                    else
                        fprintf(fp, "! \t\t ViewSeg is a %s\n",type );
                }
            }
            k=g->m_ModelSeg;
            if(k){
                HC_Show_Key_Status(k,status);
                if(strieq(status,"invalid"))
                    fprintf(fp, "! \t\t Modelseg %s\n",status);
                else {
                    HC_Show_Key_Type(k,type);
                    if(strieq(type,"segment")){
                        HC_Show_Segment(k ,buf);
                        fprintf(fp, "! \t\t Modelseg %s\n",buf );
                    }
                    else
                        fprintf(fp, "! \t\t Modelseg is a %s\n",type );
                }
            }
            k=g->axisSeg;
            if(k){
                HC_Show_Key_Status(k,status);
                if(strieq(status,"invalid"))
                    fprintf(fp, "! \t\t Axisseg %s\n",status);
                else {
                    HC_Show_Key_Type(k,type);
                    if(strieq(type,"segment")){
                        HC_Show_Segment(k ,buf);
                        fprintf(fp, "! \t\t axisseg %s\n",buf );
                    }
                    else
                        fprintf(fp, "! \t\t axisseg is a %s\n",type );
                }
            }
        }
#endif
        fprintf(fp,"!\tGsail    %p\t\n",g->GSail);
        if(g->GSail)
            fprintf(fp,"!\t sailtype =%s, its gnode=%p\n",g->GSail->GetType().c_str(), g->GetSail()->GNode() );
        else
            fprintf(fp,"! Gsail or sd NULL\n");


    }
    return 1;
}

int RXSubWindowHelper::snapshot(const QString pfilein) {
    FILE *fp;
    QFileInfo fi(pfilein);
	if(!fi.isAbsolute()) 
		cout<<"careful!! - Snapshot without absolute pathname"<<endl;
 
// set extension to .mac
    QString fname = fi.dir().filePath(fi.completeBaseName())+".mac";


    if(!(fp =RXFOPEN(qPrintable(fname),"w") )) {  return 0;}

    fprintf(fp,"! ofsi snapshot script file. go ' macro: %s ' \n",qPrintable(fname));

    QDir scriptDir(fi.dir());
    QString CurrFileLocal = scriptDir.relativeFilePath(pfilein);
    QString CurrDirLocal = QFileInfo(CurrFileLocal ).path();
// this should be relative to the macname
    fprintf(fp,"cd: %s\n",qPrintable(CurrDirLocal));// g_currentDir);

    if(g_boat) {
        if(g_boat->GetFlag(FL_NEEDS_SAVING))
            g_boat->OutputToClient("Please save the boat and then Save Session again",2);
        if(g_boat->ScriptFile().isEmpty()) {
            char buf[350];
            g_boat->OutputToClient("Cant Save Session. Please save the boat (name null) and try again",2);
            FCLOSE(fp);
            sprintf(buf, "ls -l %s ;rm -f %s\n", qPrintable(fname) ,qPrintable(fname)  );
            system (buf);

            return 0;
        }
        QString shortfile = g_boat->RelativeFileName(g_boat->ScriptFile() ,g_currentDir);
        fprintf(fp,"open boat : %s \n", qPrintable(shortfile ));
    }
    else {
        fprintf(fp,"!NO BOAT FILENAME\n");
        fprintf(fp,"!open boat : \n");
    }

    fprintf(fp,"\n! the Hoists \n");
    RXSubWindowHelper::Write_All_Hoists(fp,QString(g_currentDir));

    fprintf(fp,"\n! the STATES \n");
    RXSubWindowHelper::Write_All_States(qPrintable(fname),fp,QString(g_currentDir));

    fprintf(fp,"\n! the mirror variables\ngraphics:off\n");
    if(g_World->DBMirror())
        g_World->DBMirror()-> Write_Script ( fp) ;

    fprintf(fp,"\n! the logs\n");
    if(g_World->DBMirror())
            fprintf(fp,"%s\n",g_World->DBMirror()->ScriptCommand ().c_str());


    if(g_World->DBin())
        fprintf(fp,"%s  ! that is DB in\n",g_World->DBin()->ScriptCommand ().c_str());
    if(g_World->DBout()  )
        fprintf(fp,"%s \n",g_World->DBout()->ScriptCommand ().c_str());


    fprintf(fp,"\ngraphics:on\n\n! the VIEWS \n");
    RXSubWindowHelper::Write_All_Cameras(fp);

    fprintf(fp,"\n!  graphics:off; dsminit; setting:updateinterval:2; dsmwrite;calculate\n");
#ifdef DEBUG
    Write_All_Graphics(fp);
#endif
    FCLOSE(fp);


    return(1);
}

//int RXSubWindowHelper::setStatusBar( const QString &text, const int right) const
//{
//    QObject *client=0;
//    if(text.isNull()) return 0;
//    return rxWorker::setStatusBar( QString (text),client,right) ;
//}


