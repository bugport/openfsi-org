#pragma once

//this interface class provides the vectorfield mapping.

#include "RXMesh.h"

class RXInterpolationI
{
public:
   RXInterpolationI( );  // dont ever call this
    RXInterpolationI(class RXSail *s);
    virtual ~RXInterpolationI();
    virtual double ValueAt(const ON_2dPoint& p);
    virtual int ValueAt(const ON_2dPoint& p, double*rv);
    int HasMesh(){return msh.HasMesh () ;}
    void LocateInitialize();
    virtual int Dump(const char* fname)=0;
    class RXMesh *GetMesh(){if(HasMesh()) return &msh; return 0;  }

protected:
    class RXMesh msh;
    virtual double _ValueAt(const ON_2dPoint& p)=0;
    virtual int _ValueAt(const ON_2dPoint& p, double*rv)=0;

public:
    class RXSail *m_sail;
    ON_Xform m_xf;
};
