#ifndef _HDR_SUMMARY_
#define _HDR_SUMMARY_ 1

/* summary.h 22/1/95  revised definitions for summary.c */ 


#define SUMMARY_TAIL (-1010)

class SUMMARY_ITEM{
public:
    SUMMARY_ITEM(void);
    virtual ~SUMMARY_ITEM(void);

public:
    // looks like an updateclass
    int PCS_Evaluate_Summary_Data(const char* value) ;
    int Init();
    int apply_one_summary_item( ) const;	// the workhorse for parsing and acting on DB items.
    static class RXENode *Context(const char*label);

public:
    char*header;
    double value;
    int type; /* int, string, double */
    int m_flag;
    class RXLogFile *owner;
    int in_memory;       /* set to 1 if currently in memory */
    /* else   0 if just read from file and hence OK to write 'live' data */
    /* only used in regenerate routine */
    char*text;
    int m_Has_Changed;
    double m_tol;

    double m_oldvalue;
    int m_check_Tol;

  protected:

    void*m_input_data; /* string, int double depending on type
       ONLY USED ON INPUT SIDE */

};

#define STRING	(SUM_STRING + SUM_OUTPUT + SUM_INPUT)// WHY ON EARTH???
#define DOUBLE	(SUM_DOUBLE + SUM_OUTPUT + SUM_INPUT)
#define FLOAT	(SUM_FLOAT + SUM_OUTPUT + SUM_INPUT)
#define TRIM	(SUM_TRIM + SUM_OUTPUT + SUM_INPUT)

#define SUM_STRING 16
#define SUM_DOUBLE 32
#define SUM_FLOAT  64
#define SUM_TRIM   128

#define SUM_OUTPUT 1			/* may be used for input */
#define SUM_INPUT 2			/* may be used for output */
#define SUM_BLOB  4			/* may be used for output */

// start of fns to stay as globals.

EXTERN_C int Post_Summary_By_SLI(int *sli,const char *label,const char *value);
extern   int Post_Summary_By_Sail(SAIL *sail,const char *label,const char *value);

// a helper for parsing
EXTERN_C int RXSUM_ParseStringHeader( string str, string &model, string &Sname, string &what) ;

EXTERN_C int DeCode_Global_Controls(const char *text);
EXTERN_C int Post_Global_Controls(void);

EXTERN_C int Justapply_one_summary_item(const char*header, const char*text,const int type, const int flag) ;

#endif  //#ifndef _HDR_SUMMARY_
