
 /* batpatch.h */
#ifndef BATPATCH_16NOV04
#define BATPATCH_16NOV04

EXTERN_C int Find_Panel_Opposite(Panelptr *pan,PSIDEPTR panps);
EXTERN_C int Bat_Sort_Fn(const void *va,const void *vb);
EXTERN_C int Put_Crosses_In_Poly(struct PC_INTERSECTION*is,int ni, VECTOR **p,int*c);

EXTERN_C int Compute_Patch(RXEntity_p e);

EXTERN_C int Compute_Batten(RXEntity_p e);	  
EXTERN_C int Compute_Next_Batpatch(SAIL *p_s);
EXTERN_C int Compute_All_Batpatches(void);

EXTERN_C int Insert_Patch(RXEntity_p e);

EXTERN_C int Compute_Patch_Geometry(RXEntity_p e);

EXTERN_C int Create_Bat_BasecurveForPatch(int k,struct PC_PATCH *p,struct PC_INTERSECTION*a,int ni);
EXTERN_C int Create_Bat_CurveForPatch( RXEntity_p site1,RXEntity_p site2,int k,struct PC_PATCH *p);
EXTERN_C int Find_All_CrossesForPatch(RXEntity_p e,struct PC_INTERSECTION**is,int *ni);

EXTERN_C double Batten_Curvature( double x,double*k);
EXTERN_C double Batten_Spline( double x,double*k);
EXTERN_C int  Find_Curves_Between(RXEntity_p e1,RXEntity_p e2,sc_ptr **l,int *n);
EXTERN_C int Make_Fanned_Batpatch(RXEntity_p e,char*flag,double *arc, double *a0,float chord, int *c, VECTOR **p, struct IMPOSED_ANGLE*ia,int Nin );


#endif //#ifndef BATPATCH_16NOV04
