

#include "StdAfx.h"
#include <iostream>
#include "unixver.h"
#include <ctype.h>
#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) &&!defined(AMGTAPER)
#include "global_declarations.h"
#include "RelaxWorld.h"
#endif
#include "stringutils.h"
using namespace std;

int PC_Strip_Leading_Chars(char *s,const char*c)
{
  char *B=s;
  if(!s)
    return 0;
  while(*B && strchr(c,*B))
    B++;
  if(B != s) {
    while(*B) {
      (*s) = (*B);
      s++;
      B++;
    }
    *s = '\0';
  }
return 1;
}

int PC_Strip_Leading(char *s)/* adams */
{
  char *B=s;
  if(!s)
    return 0;
  while(*B && isspace(*B))
    B++;
  if(B != s) {
    while(*B) {
      (*s) = (*B);
      s++;
      B++;
    }
    *s = '\0';
  }
return 1;
}
int PC_Strip_Trailing_Chars(char *s,const char *c){
  /* repeatedly, if the last character is a " ", strip it */
  char *p;

  if(!s)
    return 0;
  p = s;
  
  while(*p)
    p++;
  p--;
  while( (p >= s) && strchr(c,*p)) {
    *p = '\0';
    p--;
  }
return 1;
}
int PC_Strip_Trailing(char *s){
  /* repeatedly, if the last character is a " ", strip it */
  char *p;

  if(!s)
    return 0;
  p = s;
  
  while(*p)
    p++;
  p--;
  while( (p >= s) && 
	( isspace(*p) || ((*p) == 13) || ((*p) == 12)) ) {
    *p = '\0';
    p--;
  }
return 1;
}

 int PC_Replace_Char(char*s, char a, char b){
  char *p;

  if((!s) ||( !a))/* cant replace a zero with anything */
    return 1;
  p = s;
   
  do {
	if(*p==a) *p=b;
	p++;

  } while(*p);
 

return 1;
}


/**********************************************************/



bool str_islower( const char *s) {
 const char * lp=s;
 
  while(*lp) {
	  if(isupper(*lp)) {return false;}
    lp++;
  }
  return(true);
}
char *stripcomments(  char*p_lp){
/*if p_lp starts with a comment character, 
		return the ptr to the next \r or \n
else return p_lp */

char *rv = p_lp;

	if(*rv!='!') 
		return rv;
	int n = strcspn(rv,"\n\r");
	if(n) {rv +=(1+n); return stripcomments(rv);}
	return rv+strlen(rv);
}
#ifdef NEVER
char *compresstok(char *s,char *tok)
{
register char *lp = s;
register char *lp2 = s;


while(*lp) { 
  *lp2 = *lp;
  if(strchr(tok,*lp)) {
    lp++;
    while(strchr(tok,*lp))
      lp++;
  }
  else {
    lp++;
    lp2++;
  }
}
*lp2 = '\0';
return(s);
}


#endif
/* following discards everything outside the brackets */


char *unbracket(char *s,const char *tok)
{
register char *lp=s;
register char *lp2=s;
int done=0;

  if(lp) {
    while(*lp) {
      if((*lp == '(') ) {
	done=1;
	lp++;
	break;
      }
      lp++;
    }
    if(done) {
      while(*lp) {
	if(*lp == ')') {
	  done=2;
	  *lp2 = '\0';
	  break;
	}
	*lp2 = *lp;
	lp2++;
	lp++;
      }
    }
    if(done == 1) 
      cout<< "unbracket called with unmatched brackets"<<endl;

    lp2--;
    while(lp2 >= s && strchr(tok,*lp2)) {
      *lp2 = '\0';
      lp2--;
    }
  }

return(s);
}



char *strclend(char *s) 
{
  register  char *lp=s;
  if(lp) {
    while(*lp) {
      if((*lp == '!') || (*lp == '\r') || (*lp == '\n'))
	*lp = '\0';
      lp++;
    }
  }
  return(s);
}

char *EOL_To_Blank(char *s) 
{
  register  char *lp=s;
  if(lp) {
    while(*lp) {
      if( (*lp == '\r') || (*lp == '\n'))
		*lp = ' ';
      lp++;
    }
  }
  return(s);
}
int is_clean(const char *s,const int length)
{
char *lp;
int i =0;
if(!s)
  return(0);
lp = (char*) s;
while(*lp) {
	if((*lp)>127) 
		return(0);
	if(i>=length)
		return 0;
	lp++; i++;
}

return(1);
}

int rxIsEmpty(const char *s)
{
char *lp;
int retVal = 1;
if(!s)
  return(retVal);
lp = (char*) s;
while(*lp) {
  if(!isspace(*lp))
    return(0);
  lp++;
}

return(retVal);
}

//char *streos( char *s1, char *s2)
//{
//  char *s;
//  if(!(s=strstr(s1,s2))) return(NULL); /* s2 NOT in s1 */
//  return(&(s[strlen(s2)]));
  
//}
//const char *strieos( char *s1, char *s2)
//{
//  const char *s;
//  if(!(s=stristr(s1,s2))) return(NULL); /* s2 NOT in s1 */
//  return(&(s[strlen(s2)]));
  
//}


char *strrep(char *s1,char a,char b)
{
  char *s = s1;
  while(*s) {
    if(*s == a) *s = b;
    s++;
  }
  return(s1);
}


char *strtolower(char *s)
{
  char *lp=s;
  if(!s)
    return(s);
  while(*lp) {
    (*lp)=tolower(*lp);
    lp++;
  }
  return(s);
}

char *strtoupper(char *s)
{
  char *lp=s;
  if(!s)
    return(s);
  while(*lp) {
    (*lp)=toupper(*lp);
    lp++;
  }
  return(s);
}

#ifdef NEVER  
char *stristr( const char * p_szSource, const char * pcszSearch )
//
//	Return a pointer to the start of the search string
//	If pszSource does not contain pcszSearch then returns NULL.
{
	const char * pszSource =  p_szSource;

	if(!p_szSource) return NULL;
	if(!pcszSearch) return NULL;	
	if(!g_Janet) {
		assert(str_islower(pszSource)); // preparing to reduce use of case-independent
		assert(str_islower(pcszSearch)); 
	}
	return (char*) strstr(p_szSource,pcszSearch);

	const size_t nLength = strlen( pcszSearch );
	while( *pszSource )
	{
#ifndef MSVS2005
		if( !strncasecmp( pszSource, pcszSearch, nLength ) )
			break;
#else
		assert("in stringutils.cpp stristr not compatible with MSVStudio 2005"==NULL); 
		break;
#endif
		pszSource++;
	}

	if( !( *pszSource ) )
	{
		pszSource = NULL;
	}
	return pszSource;
}

#endif

char * stristr( const char * p_szSource, const char * pcszSearch )
//
//	Return a pointer to the start of the search string
//	If pszSource does not contain pcszSearch then returns NULL.
{
	char * pszSource = (char*) p_szSource;

	if(!p_szSource) return NULL;
	if(!pcszSearch) return NULL;	
	const size_t nLength = strlen( pcszSearch );
	while( *pszSource )
	{
#ifndef MSVS2005
		if( !strncasecmp( pszSource, pcszSearch, nLength ) )
			break;
#else
		assert("in stringutils.cpp stristr not compatible with MSVStudio 2005"==NULL); 
		break;
#endif
		pszSource++;
	}

	if( !( *pszSource ) )
	{
		pszSource = NULL;
	}
	return pszSource;
}


 int Separators_to_Colons(char *line) {
	 int k;
	 for(k=0;line[k];k++) {
	  if (line[k] ==',') line[k]=':';
	  if (line[k] =='/') line[k]=':';
	   }
	   return(1);
}
#ifdef linux
	char *STRDUP(const char *s) {
	char *s2;

	if(!s)  {
		g_World->OutputToClient (" STRDUP on NULL",1);
		return NULL; 
	}
	assert(s);
	
	if((s2= (char *)MALLOC(sizeof(char)*strlen(s) + 1)) == NULL) return(NULL);
	strcpy(s2,s);
	return(s2);
	}
#endif
