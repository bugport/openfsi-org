/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :

 *  7/3/96 inclu de akmutil.h for dell
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RX_FESite.h"
#include "RXSail.h"
#include "RXEntity.h"
#include "etypes.h"
#include "global_declarations.h" 

#include "redraw.h"


int ReDraw_Nodes(SAIL *sail)
{
	RX_FESite *s;
	ent_citer it;
    for (it = sail->MapStart(); it !=  sail->MapEnd(); ++it)  {
		RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
		if ((p->TYPE==SITE || p->TYPE==RELSITE ) && p->hoopskey){ 
			s = dynamic_cast<RX_FESite *> (p);
			ON_3dPoint pp = s->GetLastKnownGood(RX_MODELSPACE);
			Move_Text_And_Markers(p->hoopskey,pp.x,pp.y,pp.z);
		}	
	}
return(0);
}

int Move_Text_And_Markers(HC_KEY segkey,const float x,const float y, const float z)
{
int iret=0;
#ifdef HOOPS
char type[128];
HC_KEY key;

HC_Open_Segment_By_Key(segkey);
 HC_Begin_Contents_Search(".","marker,text"); /* look for markers and text only */
	while(HC_Find_Contents(type,&key)) {
		iret++;
		if(strieq(type,"text")) 		
			HC_Move_Text(key,x,y,z);		
		else  {	
			HC_Delete_By_Key(key);	assert(!g_Hoops_Error_Flag);}	
	}
 HC_End_Contents_Search();
 
 
HC_Close_Segment();
#endif
return(iret);
}
