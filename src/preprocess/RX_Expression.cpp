#include "StdAfx.h"
#include "MTParser.h"  
#include "MTParserPrivate.h"// 
#include "RX_Expression.h"



RXMTInternalVariable::RXMTInternalVariable(){
m_exp=0;
}
RXMTInternalVariable::~RXMTInternalVariable(){
	m_exp=0;
}

const MTCHAR* RXMTInternalVariable::getSymbol()
{ 
		return (m_name.Array());
}    
 MTDOUBLE RXMTInternalVariable::evaluate(unsigned int nbArgs, const MTDOUBLE *pArg)
 {
		  return m_exp->evaluate (); 
  }
MTVariableI* RXMTInternalVariable::spawn() throw(MTParserException)
  { 
	  return new RXMTInternalVariable(); 
  }
 
 MyVarFactory::MyVarFactory(RXExpressionList *l)
 {
	 m_rxel=l;
 }
 MyVarFactory::MyVarFactory()
 {
	 m_rxel=0; assert(0); // shouldnt call this constructor
 }
MTVariableI* MyVarFactory::create(const MTCHAR *symbol)
  {
	  RXMTInternalVariable *v = new RXMTInternalVariable(); 
	  // I think we search here, which means we need to know the expressionlist
	  RX_Expression*l_e;
	  if(m_rxel->FindExpression (symbol,&l_e)){ // remember FIndExpression looks in the parents of m_rxel
		v->SetExpression(l_e);
		v->SetName(symbol);
	  }
	else
		{//assert(0);
		delete v; v=0;
		MTTHROW(MTExcepData (	MTLOCEXCEP_MTParserInfoObjectNotFound ) );//throwParsingExcep(MTDEFEXCEP_ItemNotFound, symbol);	
		} 
      return v ;      
  }

MTVariableFactoryI* MyVarFactory::spawn()
  { 
	  return new MyVarFactory ();
  }





RX_Expression::RX_Expression(void)
: m_model(NULL)
{
	  puts("Dont use the default RX_Expression constructor"); assert( 0);
//	m_pVars=0;
	m_MessageboxRetVal = IDNO;
	m_text=ON_String("undefined");
}

RX_Expression::~RX_Expression(void)
{
	puts("clear exp");
	m_list->RemoveExpression(this->GetOName());

}
/*har * RX_Expression::GetClassID() const
{
	return RX_ExpressionCLASSID;
}
int RX_Expression::IsKindOf(const char * p_ClassID) const
{   // Please verify the strcm preturn value
	if (0==strcmp(p_ClassID,RX_ExpressionCLASSID))
		return TRUE;
	else
		return FALSE;
}

RX_Expression * RX_Expression::Cast( RXObject* p_pObj)
{
	return(RX_Expression *)Cast((const RX_Expression*)p_pObj);
}

const RX_Expression * RX_Expression::Cast( const RX_Expression* p_pObj)
{
	if (!p_pObj)
		return NULL;
	if (p_pObj->IsKindOf(RX_ExpressionCLASSID))
		return (const RX_Expression *)p_pObj;
	else
		return NULL;
} */





RX_Expression::RX_Expression(ON_wString name,const wchar_t *p_Buf,  SAIL *const p_sail)
: m_model(p_sail)
{
	m_MessageboxRetVal=IDIGNORE;


	ON_wString line, wbuf; 	ON_String buf; 
	ON_wString w1,w2,w3;
	SetOName(name);
// p_buf may contain macro definitions as in 
//	parser.defineMacro(_T("euc(x,y)"), _T("sqrt(x^2+y^2)") , _T("Euclidean distance"));
	//$macro$ euc(x,y)$sqrt(x^2+y^2)$ Euclidean distance
	// and the expression definition as in
	//$expression$ x+y+z 
int k, nw=0;
//	m_MessageboxRetVal=IDIGNORE;
	defineConst(_T("pi"), 3.14159265358979);
	defineConst(_T("e"), 2.71828182845905);
	defineMacro( _T("isnan(x)"), _T("isNaN(x)") ,_T("fix for lowercase"));
	defineMacro( _T("isfinite(x)"), _T("isFinite(x)") , _T("fix for lowercase"));
	defineFunc(new AtSegFct());


//	m_originalText=ON_wString(p_buf);  m_originalText.TrimLeftAndRight();

	line=ON_wString(p_Buf);  line.TrimLeftAndRight(); line.MakeLower();
	k=line.Replace(L"[",L"("); 
	k=line.Replace(L"]",L")");
	k=line.Replace (L"\r",L" "); 
	k=line.Replace (L"\n",L" "); 

	m_text=line;
} //  RX_Expression::RX_Expression

bool RX_Expression::ResolveExp() // shouldnt do it twice 

	 {
	m_MessageboxRetVal=IDIGNORE;

	ON_wString line, wbuf; 	ON_String buf; 
	ON_wString w1,w2,w3;
// p_buf may contain macro definitions as in 
//	parser.defineMacro(_T("euc(x,y)"), _T("sqrt(x^2+y^2)") , _T("Euclidean distance"));
	//$macro$ euc(x,y)$sqrt(x^2+y^2)$ Euclidean distance
	// and the expression definition as in
	//$expression$ x+y+z 
	int nw=0;

	line = m_text;
  do{
	w1=nextword(line,'$');
	if(!w1.Length()&&!line.Length())
		break;
	if(w1==ON_wString("macro")){
		w1=nextword(line,'$');
		w2=nextword(line,'$');
		w3=nextword(line,'$');
		try{
			enableAutoVarDefinition(true, new MyVarFactory());
			defineMacro( w1.Array(), w2.Array() , w3.Array());
		}
		 catch( MTParserException &e ) 
			{
			buf=ON_String(" macro failed to initialize \n") + w1+ON_String("\n") +w2+ON_String("\n") +w3 +ON_String("\n\n");
			buf=buf+ON_String(e.m_description.c_str());
			///m_MessageboxRetVal=rxerror(buf,3);
			return false;
			}// end catch
// start define undefs in macro
#ifdef NEVER
			unsigned int nundef = getNbUsedVars();
			if(nundef) {
				wbuf+= ON_String(" uses variables... \n");
				m_pVars = new double[nundef];
				for( unsigned int t=0; t <nundef ; t++ )
				{  
					wbuf +=ON_wString(getUsedVar(t).c_str()) + ON_wString("  \tset to 0.0\n");
					redefineVar(getUsedVar(t).c_str(), &m_pVars[t]);
					m_pVars[t]=0.0;
				}
			}// if nundef
			else
				m_pVars=0;
#endif
 			printf("%s\n",ON_String(wbuf).Array ()); fflush(stdout);
// end define undefined in macro

	}
	else if(w1==ON_wString("equation") || w1==ON_wString("expression")   ){
		m_text=nextword(line,'$');
	}
	//else
		//m_text=nextword(line,'$');
  } while( line.Length ());//do
  // her w1 will be the last word if m_text is empty
  if(m_text.IsEmpty ()) m_text=w1;
  if(m_text.IsEmpty ()) {
	 puts ("empty text in  Parser Expression "); 
	  return false;
  }
//	m_pVars=0;

	enableAutoVarDefinition(true, new MyVarFactory(this->m_list  )); // Why create another Factory??
			try{
				 compile(m_text.Array()); m_MessageboxRetVal =IDOK;
			 }
			 catch( MTParserException &e ) // this will identify any undefined function.
			 {
				MTSTRING msg = _T("while parsing... ");
				msg += e.m_description.c_str();
				buf=ON_String(msg.c_str()) +ON_String(" in\n") +m_text; 
#ifdef _WINDOWS				 
				m_MessageboxRetVal = AfxMessageBox(CString(buf),MB_ABORTRETRYIGNORE);
				if(m_MessageboxRetVal==IDABORT   ||m_MessageboxRetVal == IDNO  )
				{ puts("abort or no");   
				return false;
				}
#endif
#ifdef linux
			puts(buf.Array());
				m_MessageboxRetVal = IDOK;
				if(m_MessageboxRetVal==IDABORT   ||m_MessageboxRetVal == IDNO  )
				{ puts("abort or no");   
				return false;
				}
#endif

				for( unsigned int t=0; t < e.size(); t++ )
				{
					wbuf=ON_wString(e.getException(t)->getDescription() );

				}
				return false;
			 }// end catch
#ifdef NEVER
			unsigned int nundef = getNbUsedVars();
			if(false && nundef) {
				wbuf+= ON_String(" uses variables... \n");
				m_pVars = new double[nundef];
				for( unsigned int t=0; t <nundef ; t++ )
				{  
					wbuf +=ON_wString(getUsedVar(t).c_str()) + ON_wString("  \tset to 0.0\n");
					redefineVar(getUsedVar(t).c_str(), &m_pVars[t]);
					m_pVars[t]=0.0;
				}
			}// if nundef
#endif
	if(!wbuf.IsEmpty ())
 		printf("%s\n",ON_String(wbuf).Array ()); fflush(stdout);
	return true;
	}


ON_wString RX_Expression::nextword(ON_wString &line, const char delim) {

	int k1,nw=0;
	ON_wString w1;

	  line.TrimLeft();
	  if (line[0]==delim){
		  line=line.Mid(1);
		  k1 = line.Find(delim); 
		  if(k1<0)
		  { line.TrimLeftAndRight(); w1=line; line.Empty(); return w1;}
		  w1=line.Left(k1); //printf(" word %d is %s\n",nw, ON_String(w1).Array ());
		  line=line.Mid(k1);
		  w1.TrimLeftAndRight();
		  return w1;
		  nw++;
	  } // if

line.TrimLeftAndRight();w1=line; line.Empty(); return w1;
}

  
 
RXExpressionList::RXExpressionList() 
{
m_parent=0;
}
RXExpressionList::~RXExpressionList()
{	RXEXMAP::iterator it;
// why is m_map  invalid when this is called from ~RXSail???
#ifdef NEVER
	for(it=m_map.begin();it!=m_map.end(); it++){
		RX_Expression *e = it->second;
		delete e ; it->second=0;
}
#endif
	m_map.clear();
	puts(" Did we already look in this->m_owner and delete any exps which have m_model==this?? ");
}
int RXExpressionList::ClearExpList( const SAIL*p_owner){
	RXEXMAP::iterator it;
	bool changed;
	int rc=0;
// I cant remeber whether map.erase invalidates the iterator, so lets do one at a time.
	do{
		changed=false;
		for(it=m_map.begin();it!=m_map.end(); it++){
			if(it->second->m_model==p_owner) {
			m_map.erase(it);
			changed=true; rc++;  break;
			}
		}
	}while(changed);
	 if(m_parent) 
		 rc+=m_parent->ClearExpList(p_owner);
	 return rc;
}




RX_Expression* RXExpressionList::AddExpression( ON_wString name, ON_wString equation,  SAIL *const p_sail ) 
{
	//puts("dont forget to pass 'p_sail' to the new expressions m_owner. This is so write script can search 
	// up the tree to write all expressions with the same p_sail. 
	RXEXMAP::iterator it;
	pair<RXEXMAP::iterator,bool> ret;
	ret=m_map.insert (pair<ON_wString,RX_Expression* >(name,new RX_Expression(name, equation.Array(),p_sail))) ; 

	if (ret.second==false)
		printf("element '%S' already existed",name.Array ());

	RX_Expression* p=  ret.first->second ;
		p->m_list=this;
	return p;
} // RXExpressionList::AddExpression

bool RXExpressionList::RemoveExpression(ON_wString name)
{
	RXEXMAP::iterator it=m_map.find(name ) ;
	if(it ==GetMap().end()) 
			return false;
	m_map.erase(it);
	return true;

}
 bool RXExpressionList::FindExpression(ON_wString  text, RX_Expression **e)  //looks in this->explist and recursively in this->parent->explist. 
{	
	bool rc=true;
	*e=0;
	RXEXMAP::iterator it=m_map.find(text ) ;
	if(it ==GetMap().end())  {
		if(m_parent) 
			return m_parent->FindExpression (text,e);
		else 
			return false;
	}
	else
		*e=it->second;
	return true;
 }

 // writes the expressions in the list as SET entities
 int RXExpressionList::WriteToScript(FILE * fp,const wchar_t* p_where, const SAIL*p_model)
 {
	 int rc=0;
	 if(!fp)
		 return 0;
	 RXEXMAP::iterator it;
	 for(it=m_map.begin();it!=m_map.end(); it++){
		RX_Expression* p=it->second ;
		if(p_model && p->m_model!=p_model) continue;
		rc+=fprintf(fp,"set\t %S \t %S \t %S ! written from expression\n",p->GetOName().Array(), p->m_text.Array(),p_where);
	 }
	 if(m_parent) // usually m_parent is the world and this call writes our model's global variables.
		 m_parent->WriteToScript(fp,p_where, p_model);
	 return rc;
 }
MTDOUBLE AtSegFct::evaluate(unsigned int nbArgs, const MTDOUBLE *pArg)
{ 
	printf("( AtSegFnct) %f %f\n", pArg[0], pArg[1]);
	 return pArg[1];

}

