
//test _LIB because opennurbs_staticlibd defines _LIB


 #include "StdAfx.h" 
 
#include "pc_solvecubic.h"
/*
parking various project options
"C:\mydocs\Visual Studio 2005\Projects\FortranForwin32wid_static\Debug\FortranForwin32wid_static.lib"

*/

extern "C" int solvecubic(double *q,double *r,double *s,double *t,double *p_r1,double *p_r2,double *p_r3);

int  PF_solvecubic(double *q,double *r,double *s,double *t,double *p_r1,double *p_r2,double *p_r3) 
{

#ifdef FORTRANLINKED
		//cout<< " solvecubic commented out"<<endl;
 		  return solvecubic(q,r,s,t,p_r1,p_r2,p_r3);
#else
		assert("(no AMG2005) solve cubic is a fortran function"==NULL);
	  // return solvecubic(q,r,s,t,p_r1,p_r2,p_r3); // peter BUG april 2008 we weerent retreiving the retval
		return 0;
#endif

}

double PCON_Dot( double *pa, double *pb, const int n) {
	double retval=0;
	int i;
	double *a, *b;
	for(i=0,a=pa,b=pb;i<n;i++,a++,b++) retval += (*a) * (*b);
	return retval;
}
int PCON_Dot( ON_Matrix &a, ON_SimpleArray<double> &y ,ON_SimpleArray<double>*ret) {

	int i,j, nr,nc;
	double v;
	nc = a.ColCount (); 
	nr = a.RowCount ();
	assert(nc==y.Count());

	for( i=0;i<nr;i++) {
		v=0;
		for( j=0;j<nc;j++) {
			v+= a[i][j]*y[j];
		}
		ret->Append(v);
	}


	return nr;
}

int PC_SolveOverdetermined(ON_SimpleArray<double> &c, ON_Matrix &a, ON_SimpleArray<double> *x)  
{
	int rc=0;
/*   [c] has length n;  a is a mtrix [n x m] where m<=n  
	we find the least square solution for [x] to minimize [e].[e] 
	where [e] = [a] . [x] - [c] */
	int nc,na,m,i,j ;
	nc = c.Count();
	na = a.RowCount();
	m = a.ColCount();
	if(nc !=na)
		return 0;
	ON_SimpleArray<double> y; y.Empty();
	ON_Matrix theMat = ON_Matrix(m,m);

	for(i=0;i<m;i++) {
		y.Append( PCON_Dot(c,a[i],nc));
		for(j=0;j<m; j++) 
			theMat[i][j] = PCON_Dot(a[i],a[j],nc);
	}
	if(!theMat.Invert(1e-8)) 
		return 0; 
	x->Empty();
	return	PCON_Dot (theMat , y,x);

} //PC_SolveOverdetermined


	
