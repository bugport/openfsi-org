
#include "StdAfx.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXOffset.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "RXRelationDefs.h"

#include "etypes.h"
#include "hoopcall.h"
#include "arclngth.h"
#include "printall.h"
#include "griddefs.h"
#include "RXCurve.h"


#include "iangles.h"

RXEntity_p PCIA_Close_To_Another_Offset(RXEntity_p e,double tol) {
    // returns 1 if site e has offset close to another in IAlists of either of its SCs
    int i,k,found;
    double o1=0.0,o2,d;
    sc_ptr sc;
    RXSRelSite *p = (RXSRelSite  *)e;
    for(i=0;i<2;i++) {
        found = -1;
        if(!p->CUrve[i]) continue;
        sc = (sc_ptr  )p->CUrve[i];
        Sort_IA(sc->ia,sc->n_angles);
        for (k=0;k<sc->n_angles ;k++) {
            if(sc->ia[k].s==e) {
                o1=(sc->ia[k].m_Offset->Evaluate(NULL,1));
                found = k;
                break;
            }
        }
        d = sc->GetArc(1);
        if(found < 0) continue;
        for(k=0;k<sc->n_angles;k++) {
            if(k==found) continue;
            o2 = (sc->ia[k].m_Offset->Evaluate(NULL,1));
            if(fabs(o2-o1) < d)
                d=fabs(o2-o1);
            if(d < tol) return sc->ia[k].s;

        }
    }

    return NULL;
}
double Show_Offset_By_Entity(sc_ptr sc, RXEntity_p e,int *err){
    int k;

    for (k=0;k<sc->n_angles ;k++)
    {
        if(sc->ia[k].s==e)
        {
            *err=0;
            return(((sc->ia[k]).m_Offset->Evaluate(NULL,1)));
        }
    }
    *err=1;
    return 0.0;
}//double Show_Offset_By_Entity


int Remove_From_All_IA_Lists(RXEntity_p e) {

    int  iret=0,flag;
    sc_ptr sc;
    SAIL *l_sail = e->Esail ;

    ent_citer  it;
    for (it = l_sail->MapStart(); it != l_sail->MapEnd(); ++it)  {
       RXEntity_p p  =  it->second;
        if (p->TYPE == SEAMCURVE)  {
            sc = (sc_ptr  )p;
            flag = sc->Remove_From_IA_List( e,DO_SCS_TOO) ;
            iret += flag;
            if(sc->End1site == e )
                sc->End1site = NULL;
            if(sc->End2site == e )
                sc->End2site = NULL;
        }
    }
    return iret;
}

int Duplicate_Relsite(const sc_ptr sc , RXOffset*offset, RXEntity_p *e2,const double tol) 
{
    /* if a relsite already exists with the same curve and offset, return
    its name as name2
    this looks WRONG. it presumes that we should only look in Curve[0]
    */
    int i,which=0;
    class RXSRelSite *s;
    for(i=0;i<sc->n_angles;i++) {
        s	= dynamic_cast< class RXSRelSite *>( (sc->ia[i]).s);
        if(!s)
            continue;
        if( dynamic_cast<sc_ptr >(s->CUrve[0]) != sc)
            continue;


        if(PC_Same_Offsets((sc->ia[i]).m_Offset,offset,sc,which,tol))
        {
            *e2 =  s;//(RXEntity_p)
            return(1);
        }
    }
    return(0);
}//int Duplicate_Relsite


int Insert_Angle_References(RXEntity_p p1,RXEntity_p p2,sc_ptr p) {

    /* p1 and p2 are pointers to entities at either end of seam pointed to by pe
   */

    if (p1 && strieq((*p1).type(),"seamcurve")) {
        sc_ptr ccptr =(sc_ptr  )p1;
        int k=((*ccptr).n_angles)++;
        ccptr->ia = (struct IMPOSED_ANGLE*) REALLOC(ccptr->ia ,(k+1)*sizeof(struct IMPOSED_ANGLE));
        (ccptr->ia[k]).Angle = &(p->end1angle);
        (ccptr->ia[k]).m_Offset = p->End1dist;
        (ccptr->ia[k]).sign  = &(p->end1sign);
        (ccptr->ia[k]).s  = p;
        (ccptr->ia[k]).End= 1;
        ccptr->ia[k].IA_Owner = NULL;


    }

    if (p2&& strieq((*p2).type(),"seamcurve")) {
        sc_ptr ccptr =(sc_ptr  )p2;
        int k=((*ccptr).n_angles)++;
        ccptr->ia = (struct IMPOSED_ANGLE*) REALLOC(ccptr->ia ,(k+1)*sizeof(struct IMPOSED_ANGLE));
        (ccptr->ia[k]).Angle = &(p->end2angle);
        (ccptr->ia[k]).m_Offset = p->End2dist;
        (ccptr->ia[k]).sign  = &(p->end2sign);
        (ccptr->ia[k]).s  = p;
        (ccptr->ia[k]).End= 2;
        ccptr->ia[k].IA_Owner = NULL;
    }
    return(1);
}

int Compare_Angles( const void *va,const void *vb){

    /* this test has to sort the I_As according to the following rules:
     IF THE SAME RELNODES OR x1==x2
     we don-t care , so we sort by name to prevent ambiguity
     ELSE
     sort according to x1,x1
     ENDIF
     
     NOTE This routine flags if two coincident nodes are found*/

    struct IMPOSED_ANGLE *ia,  *ib;

    RXEntity_p ra=NULL, rb=NULL;  /* PC_ENTITIES at the inboard ends */ //gcc
    RXEntity_p ea, eb;  /* PC_ENTITIES of seams abutting */

    sc_ptr sa=NULL,sb=NULL;
    int enda,endb, which;
    char errs[256];

    double xa,xb;

    ia = (struct IMPOSED_ANGLE *) va;
    ib = (struct IMPOSED_ANGLE *) vb;

    ea = (*ia).s;                   eb = (*ib).s;
    enda = (*ia).End;               endb = (*ib).End;

    if(ea->TYPE==SEAMCURVE) {
        sa = (sc_ptr  )(ea);
        if (enda==1)          ra = sa->End1site;
        else if(enda ==2)     ra = sa->End2site;
        else ea->OutputToClient(" endA out of range",3);
    }

    else if(ea->TYPE== PCE_NODECURVE) {
        sa = (sc_ptr  )(ea);
        if (enda==1)          ra = sa->End1site;
        else if(enda ==2)     ra = sa->End2site;
        else ea->OutputToClient(" endA out of range",3);
    }

    else if (ea->TYPE==RELSITE) {
        ra = ea;
    }
    else if (ea->TYPE==SITE) {
        ra = ea;
    }
    else {
        sprintf(errs,"(Compare_Angles) type not known '%s' (%s)\n",ea->type(),ea->name());
        ea->OutputToClient(errs,2);
        return(0);
    }

    if(eb->TYPE==SEAMCURVE || eb->TYPE==PCE_NODECURVE) {
        sb =(sc_ptr) eb;
        if (endb==1)           rb = sb->End1site;
        else if(endb ==2)    rb =sb->End2site;
        else eb->OutputToClient(" endB out of range",3);
    }

    else if (eb->TYPE==RELSITE) {
        rb = eb;
    }
    else if (eb->TYPE==SITE) {
        rb = eb;
    }
    else {
        sprintf(errs,"Compare_Angles failed '%s' (%s)\n",eb->type(),eb->name());
        ea->OutputToClient(errs,2);
        return(0);
    }

    if (rb==NULL || ra==NULL) {
        char str[256];
        sprintf(str," problem with  ");
        if(ia&&ia->s&&ia->s->name()) strcat(str,ia->s->name());
        strcat(str,"  and  ");
        if(ib&&ib->s&&ib->s->name()) strcat(str, ib->s->name());
        ea->OutputToClient(str,3);

    }

    if (ra==rb)  {
        int iret;
        const char *na = ia->s->name();
        const char *nb = ib->s->name();
        iret = strcmp(na,nb);
        if(iret==0) {
            const char *na = ia->s->type();
            const char *nb = ib->s->type();
            iret = strcmp(na,nb);
        }

        if(!iret){
            xa=xb=0.0;
            if(ia && ia->m_Offset) xa = (ia->m_Offset->Evaluate(NULL,0));
            if(ib && ib->m_Offset) xb = (ib->m_Offset->Evaluate(NULL,0));
            sprintf(errs," cant compare <%s><%s> at %f with <%s><%s> at %f",ia->s->type(),ia->s->name(),xa,ib->s->type(),ib->s->name(),xb);

            ea->OutputToClient(errs,1); // Oct 2002 was 1 in production code
            return 1; // at a guess
        }
        return(iret);
    }

    /* we only get here if the two nodes are different or one is a cut. So we need a geometric test */
    which=0;
    if(!ia->m_Offset->GetSc()->m_pC[which]->IsValid() ){
        which=2;
        if(!ia->m_Offset->GetSc()->m_pC[which]->IsValid() ){
            which=1;
        }
    }

    //	That is a lashup for:
    //1) we often use side(0) to mean red-space but sometimes the SC hasn't a side 0
    //2) so we try side 2.  TPNs don't have valid 0 or 2, so regress to 1
    // 3) a lash-up to get past computing a geodesic curve the first time WRONG

    xa = (ia->m_Offset->Evaluate(NULL,which));   /* because arc points to sc->arcs[0] */

    which=0;
    if(!ib->m_Offset->GetSc()->m_pC[which]->IsValid() ){
        which=2;
        if(!ib->m_Offset->GetSc()->m_pC[which]->IsValid() ){
            which=1;
        }
    }

    xb = (ib->m_Offset->Evaluate(NULL,which));

    if (fabs(xa-xb)<FLOAT_TOLERANCE) {
        int iret;
        const char *na = ia->s->name();
        const char *nb = ib->s->name();
        if(strieq(ia->s->type(),ib->s->type() )) {
            sprintf(errs,"Coincident %ss %s (%f)  and %s (%f) ",ib->s->type(),na,xa,nb,xb  );
            ea->OutputToClient(errs,1);
        }
        iret = strcmp(na,nb);
        if(iret==0) {
            const char *na = ia->s->type();
            const char *nb = ib->s->type();
            iret = strcmp(na,nb);
        }
        if(!iret){
            sprintf(errs,"comparing offsets on %s and %s gave the same ( %f and %f)",	ia->s->name(),ib->s->name(),xa,xb);
            ea->OutputToClient(errs,1);
        }
        return(iret);
    }
    else
    {
        if (xa <xb)
            return(-1);
        if (xa >xb)
            return( 1);
    }
    return 0;
}
/************************************************************/

int Sort_IA( struct IMPOSED_ANGLE*ia,int N) {

    if (N >1) {
        qsort(ia,N,sizeof(struct IMPOSED_ANGLE),Compare_Angles);
    }

    return(1);
}

