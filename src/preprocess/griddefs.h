/* date:  Friday 12 November 1993 PH version
September 1997. sites have curve[2], cross-angle
SCs has Needs_Crossing flag

 april 1996  the SC has 0% and 100% offsets pre-defined
  march 1996 offset definitions altered 
end Feb 1996 SC changes to allow cutting
 aunts re-introduced, so that when we delete a niece
 we can tell the aunt about it
  23/2/96  	Gauss end refs added to SC
  17/12/95 Defined_Length  flag added to SC
 25/5/95 scaling flags added to Seamcurve to speed geosolve
 14/3/95 Gauss fields added to Seamcurve

   changed 9/12/94 
file: griddefs.h   panels have Geometry_Has_Changed flag
			this is set to 1 for all NEW panels
			and also set to 1 in Geo SOlve for all panels
			adjacent to sites which have moved 

Declarations for gridding and PCDRAW programs */

#ifndef _HDR_GRIDDEFS_
#define _HDR_GRIDDEFS_ 1
#ifdef AMGTAPER
	#error("please dont use griddefs in AMGTAPER");
#endif

#include "RXTriplet.h"
class  RXCurve;
class  RXPanel;
// class RXLayer;
#define LEFTOFSTART            0x1
#define RIGHTOFSTART           0x2
#define LEFTOFEND		0x4
#define RIGHTOFEND             0x8


// values for flags in  SC and Site
#define 	SC_SUB_SEGMENT		2
#define		MOULDED		     	8  // MUST MATCH RXSRelsite.h 
#define		FILAMENT            32
#define		PCF_SKETCH          64
#define		DONT_SEARCH         128
#define		CUT_FLAG            256
#define		SNAP_TO_NODES		512	// snap ends to anywhere on other SCs and to sites and relsites.
#define		SNAP_TO_ENDS_ONLY	1024	// only search the end pts of other SCs and the sites and relsites.
#define		TPN_SC              2048
#define		SC_STRIPE           4096
#define		PCF_EXPORT_DISP		8192
#define		SC_3DM_CUT          16384
#define		PCF_EXPORT_COORDS	32768

#define		PCF_ISMESHSITE 		65536   // set and used only in RXMesh, it marks a site as being in a triangulation. Dangerous if there are >1 meshes in the model.
#define		PCF_FIL_NO_BUCKLE	131072  
#define		PCF_ISFIELDNODE		524288 // set in various places. It marks the site as available for including in a triangulation.
#define		PCF_ISONEDGE		1048576
//#define     PCF_                2097152

/**********************************************************************/

struct SIDEREF {
	PSIDEPTR pp;
	int end;
	double m_srangle;
};  //struct SIDEREF 

struct SIDE_ANGLE {
	short int flag;  /* 0=ignore 1 = ref psidechord 2 ref SC chord 4 = refSC*/
	float angle;
	char *str;
};  //struct SIDE_ANGLE 

struct IMPOSED_ANGLE {  /* these are pointers to the original data
			 so they stay in synch */
	double *Angle;
    class RXOffset *m_Offset;

	int *sign;
	RXEntity_p s; /*the seam ending here */
	int End;                    /*1 or 2 depending on whether e1 or e2 of s lands here*/
	RXEntity_p IA_Owner; 	/* only the IAOwner may free it */ 
};//struct IMPOSED_ANGLE 
	
struct GPoint{
	double u,v,x,y,z;
	double GAngle, AbsAngle, Sw,Suv, GeodesicY, t; 
/* t is a given parameter along the curve in UV space
Suv is the measured dist along the curve in UV space  (0 - 1)
Sw is the measured dist along the curve in world space (0 - 1)
 We should try to move the knots to keep Suv the same as t
*/
	double AngleToNextNormal, SegLen,UVLen, spare;
	double UFac, VFac; /* the perpvector in UV space */
	ON_3dVector SegVec,du,dv,normal, ChordU; /* chord vector in UV space */
	struct GPoint *next, *before;
	int end,flag;
	RXEntity_p m_e;

	class RXOffset *o;
};//struct GPoint

/*********************************************************************
							PANEL STRUCTURES
**********************************************************************/
#define __SITE__ 1

#ifdef __cplusplus
	 typedef class RX_FEedge FortranEdge;
	 typedef class RX_FETri3 FortranTriangle;

#else
#error ("cant compile griddefs in C")
#endif 

struct PC_FILAMENT_ON_TRI  {  
		int j1,j2;
		double t1,t2;
		RXEntity_p m_sco;
		double m_theta; 
		double m_W;
		double m_strain;
		double m_Ti;
		double m_EA;
		struct PC_FILAMENT_ON_TRI *next;
};//struct PC_FILAMENT_ON_TRI 
	
struct PC_MATERIAL {	 /* card 'fabric' for linear, 'material' for nonlinear */
	double d[3][3];
	int creaseable;
	int linear;
	struct PC_TABLE *table;
	 double zk;

	int npn,seedx,seeds;
	double *es,*fs,*en,*fn;
	char *text;
	RXTriplet m_qprestress;
	#ifndef WIN32
		void *g; /* the graphic */   // MIGHT BE A PROBLEM in CPP thomas 15 11 04
	#endif  //#ifndef WIN32
};	 //struct PC_MATERIAL		  

typedef  class RXPanel* Panelptr;

#endif



