#ifndef _HDR_GEODESIC
#define _HDR_GEODESIC 1	

extern int Geodesic_Flag;
#define GEO_CANCEL 1
#define GEO_OK 0

//ON_Surface* s  replaces  RXEntity_p mould
 struct GPoint{
	double u,v,x,y,z;
	double GAngle, AbsAngle, Sw,Suv, GeodesicY, t; 
/* t is a given parameter along the curve in UV space
Suv is the measured dist along the curve in UV space  (0 - 1)
Sw is the measured dist along the curve in world space (0 - 1)
 We should try to move the knots to keep Suv the same as t
*/
	double AngleToNextNormal, SegLen,UVLen, spare;
	double UFac, VFac; /* the perpvector in UV space */
	ON_3dVector SegVec, du,dv,normal, ChordU; /* chord vector in UV space */
	struct GPoint *next, *before;
	int end,flag;
	void *oo;
};	
extern int Find_Geodesic_By_UV(const ON_Surface* s,double u1,double v1,double u2,double v2, ON_3dPoint **p,int*c);

extern int  rh_InitGlist(struct GPoint**GeodesicList,ON_3dPoint *e1, ON_3dPoint *e2,double u1,double v1,double u2,double v2,const ON_Surface* s);
extern int  CopyGlistToPline(struct GPoint*GeodesicList,ON_3dPoint**p,int*c);

extern int  GeodifyList (struct GPoint*GeodesicList,double Atol ,double Ltol,const ON_Surface* s);
extern int  Insert_Gknot_After(struct GPoint **g,const ON_Surface* s);
extern int  Remove_Gknot(struct GPoint **g,const ON_Surface* s);
extern int  GeodifyKnot (struct GPoint *g,const ON_Surface* s);

extern  int Get_Normalised_Geodesic_S(struct GPoint*GeodesicList);
//extern  int Calculate_Y(sc_ptr sc,ON_Surface* s)
extern int Calculate_Y(struct GPoint *GeodesicList,double Atol ,double Ltol,const ON_Surface* s);

extern int Calc_GKnotAngles(struct GPoint *g,const ON_Surface* s);
	/* all info derived from its relation with its neighbours */

extern int Calc_Gknot_Position(struct GPoint *g,const ON_Surface* s);
	/* all info derived from (u,v) alone */
//extern int UV_from_Sxy( sc_ptr sc, double sxy, double*u, double*v);
extern int  Print_Gknot(struct GPoint *g, FILE *fp);
extern int  Print_GList(struct GPoint *g, FILE *fp);

//extern int printmatrix(char*s, double L[3][3]) ;
extern int VerifyScale(struct GPoint *g, double zfac);
extern int CalcGeodesicScale(struct GPoint *g);
extern double Geodesic_Arc(struct GPoint *head);
//extern double Suv_from_Sxy( sc_ptr sc, double sxy, int*flag); 
//extern double Sxy_from_Suv( sc_ptr sc, double suv, int*flag); 
//extern int UV_from_Suv( sc_ptr sc, double suv, double*u, double*v) ;
//extern int Place_On_Geo(sc_ptr sc,double Suv, double*v1,double*v2,double*v3,double*U1,double*U2); 
extern int Delete_Geodesic_List(struct GPoint *head);

extern int  Geo_Test_Remove( struct GPoint *g,double Atol ,double Ltol);
extern int Geo_Test_Insert(struct GPoint *g,double Atol ,double Ltol,const ON_Surface *s );
//extern int GlistFirstAndLast(sc_ptr sc,Site *e1, Site *e2,ON_Surface* s) ;
double rh_True_Angle(ON_3dVector *a, ON_3dVector *b);
double rh_Apparent_Angle(ON_3dVector a, ON_3dVector b, ON_3dVector view);
int rh_Compute_Perp_Transform(ON_3dVector *n,double d[3][3]);
extern int rh_Compute_3Matrix_Product(double a[3][3], double b[3][3], double r[3][3]);

int rh_matinvd(int *isol,int *idsol,int nr,int nc,double *a,int mca,double *det);
#endif
