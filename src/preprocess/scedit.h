#ifndef _SCEDIT_H_
#define _SCEDIT_H_ 

#include "griddefs.h"

void Make_SeamCurve_Edits(SAIL *sail) ;
int Edit_SeamCurve_Card(RXEntity_p e, RXEntity_p *r);
int make_edit_seamcurve_length(RXEntity_p sEnt);

#endif  //#ifndef _SCEDIT_H_

