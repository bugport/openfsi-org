#ifndef _CONNLIST_H_
#define _CONNLIST_H_

extern "C"   int  cf_create_connect (const char*m, int*lm,const char*s, int*ls, void**cptr);   
extern "C"   int  cf_delete_connect ( void** cptr);
extern "C"  void  cf_resolve_connects (int*count);
extern "C"  void  cf_unresolve_connects (const char*name,int *l);
extern "C"  void  cf_unresolveOneConnect(const int n);
extern "C"  void  cf_typeconnects (void);
extern "C"  void  cf_get_first_three_connects (const char*,int*,const char*,int*, int*, int*,int*);

#endif  //#ifndef _CONNLIST_H_


