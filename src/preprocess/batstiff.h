 /* batstiff.h */

#ifndef BATSTIFF_16NOV04
#define BATSTIFF_16NOV04

EXTERN_C float KBatten_Stiffness(const RXEntity_p e, float x1,float x2,float x3);
EXTERN_C float Batten_Stiffness(const RXEntity_p e, float x);


#endif //#ifndef BATSTIFF_16NOV04
