#ifndef RXIMPORTENTITY_H
#define RXIMPORTENTITY_H
#include <QString>
#include "RXEntity.h"
#define RXIMPORT_SAILPACK   1

/// to read a NAS file we used to do 3 passes
/// but to translate a sailpack batten sequence we need to look at the blsegs and the bcomps
/// but we cannot resolve a bconp until we have resolved the  blsegs.
/// and we cannot resolve a batten/pocket blseg pair without knowing about the bconps.


class RXImportEntity : public RXEntity
{

public:
    RXImportEntity();
    RXImportEntity(class RXSail *sail);
    virtual ~RXImportEntity();
    int CClear();
    virtual int Dump(FILE *fp) const;
    virtual int Compute(void);
    virtual int Resolve(void);
    virtual int ReWriteLine(void);
    virtual int Finish(void);
     int SpecialTreatmentForLuffLeechFootHead();

private:
    QString m_filename;
    QString mAtts;
    int ReadBulkData_1(); // fixings,  nodes and materials
    int ReadBulkData_2();//// elements and the rest
    int ReadBulkData_3();//// BLSEGS and BCONPs
    int ResolveObjects();
    int FindOtherEndOfBCONP(const int n1);
    static  QString nasno(const QString s) ;
    static QStringList ChopString(const QString lLine,const int s) ;
    int m_rximportFlags;


    // now a lot of classes for the objects
    class NastranObject
    {
    public:
        NastranObject() ;
        virtual ~NastranObject(){}
        NastranObject(const int nn, const QString line);
        QString m_line;
        int m_nn;
        RXEntity_p m_ent;
        QString  word(const int i) {return m_line.mid(8*i,8);}
        int Compute( class RXImportEntity*model) { return 1;}
    };


    class NastranMAT
    {
    public:
        NastranMAT();
        ~NastranMAT();
        NastranMAT(const int nn, const QString line);
        NastranMAT(const int nn, const QStringList wds);
        QString Summary() const;
        QString m_line;
        int nn;
        double d[9];
        double exx() const;
        double g()const;
    private:
        double m_exx,m_nu,m_g;
    };
    class NastranPSHELL // see http://www.kxcad.net/Altair/HyperWorks/oshelp/pshell.htm
    {
    public:
        NastranPSHELL();
        ~NastranPSHELL();
        NastranPSHELL(const int n, const QString line);
        int pid,mMatid,mid2,mid3,mid4;
        double thk,bsp,tst,nsm,z1,z2,t0;
        QString m_line;
        class rxpshell*m_pan;
    };
    class NastranCQUAD8
    {
    public:
        NastranCQUAD8();
        ~NastranCQUAD8();
        NastranCQUAD8(const int n, NastranPSHELL&sh,
                      const double pressure,
                      const int nn[8]);
        int nn;
        int nodes[8];
        NastranPSHELL m_ps;
        double m_dp;
    };
    class NastranCTRIA3
    {
    public:
        NastranCTRIA3();
        ~NastranCTRIA3();
        NastranCTRIA3(const int n, NastranPSHELL&sh,
                      const double pressure,
                      const int nn[3],const double theta);
        int nn;
        int nodes[3];
        NastranPSHELL m_ps;
        double m_dp,m_th;
    };
    class NastranPBAR:
            public RXImportEntity::NastranObject
    {
    public:
        NastranPBAR(){}
        ~NastranPBAR(){}
        NastranPBAR(const int nn, const QString line):
            RXImportEntity::NastranObject(nn,line),
            matid(0), area(0), i1(0), i2(0),j (0),
            nsm(0), c1 (0), c2 (0),d1 (0),d2(0)
        {}
        int Compute( class RXImportEntity*model);
        int   matid;
        double area;
        double i1;
        double i2;
        double j ;
        double nsm;
        double c1 ;
        double c2 ;
        double d1  ;
        double d2;
    };

    class NastranCONROD :// a bar
            public RXImportEntity::NastranObject
    {
    public:
        NastranCONROD() {}
        ~NastranCONROD() {}
        NastranCONROD(const int nn, const QString line);

        int nodes[2];
        double m_ea,m_ti,m_zi;
        int Compute( class RXImportEntity*model);

    };
public:
    class NastranCBAR : // a beam
            public RXImportEntity::NastranObject
    {
    public:
        NastranCBAR(){
            m_cbnodes[0]=-1; m_cbnodes[1]=-1;
            for(int k=0;k<7;k++) eis[k]=0;
        }
        ~NastranCBAR(){;}
        NastranCBAR(const int nn, const QString line) :
            NastranObject(nn,line) {
            m_cbnodes[0]=-1; m_cbnodes[1]=-1;
            for(int k=0;k<7;k++) eis[k]=0;
        }
        int m_cbnodes[2];
        double eis[7];  //ea, ei2,ei3,gj,ti,beta,zi
        int Compute( class RXImportEntity*model) ;
    };

    class NastranBCONP:
            public RXImportEntity::NastranObject
    {
    public:
        NastranBCONP();
        ~NastranBCONP();
        NastranBCONP(const int nn, const QString line);
        int Resolve();
        int Compute( class RXImportEntity*model) ;
        //$ TYPE  ID      SLAVE   MASTER          SFAC    FRICID  PTYPE   CID
       // class NastranBLSEG *m_slave, *m_master;
    };
    class NastranBLSEG: public RXImportEntity::NastranObject
    {
    public:
        NastranBLSEG() {}
        ~NastranBLSEG(){}
        NastranBLSEG(const int nn, const QString line);
        int Resolve();
        std::list<int> m_nodes;
    };

    class NastranRBE2: public RXImportEntity::NastranObject
    {
    public:
        NastranRBE2() {}
        ~NastranRBE2(){}
        NastranRBE2(const int nn, const QString line);
        std::list<int> m_nodes;
    };

    class RXImportEntity::NastranBLSEG  FindBLSEGAcrossModels(const int id,QString&sailtype) ;
    class RXImportEntity::NastranCBAR Find_CBAR( const int n1, const int n2);

    std::map<int,RXImportEntity::NastranPSHELL> m_pshells;
    std::map<int,RXImportEntity::NastranMAT> m_materials;
    std::map<int,RXImportEntity::NastranPBAR> m_pbars;  // beam materials
    std::map<int,RXImportEntity::NastranBLSEG> m_blsegs; // strings of nodes
    std::map<int,RXImportEntity::NastranRBE2> m_rbe2s; // strings of nodes

    std::vector<RXImportEntity::NastranCQUAD8> m_cquad8;
    std::vector<RXImportEntity::NastranCTRIA3> m_ctria3;

    std::vector<RXImportEntity::NastranCONROD> m_conrods; // bar elements
    std::vector<RXImportEntity::NastranCBAR> m_cbars; // beam elements
    std::vector<RXImportEntity::NastranBCONP> m_bcomp; // connectors

};

#endif // RXIMPORTENTITY_H
