#include "StdAfx.h"
#include <QDebug>
#include <QMessageBox>
#ifndef RXEDEV
#include "RXEntityDefault.h"
#include "global_declarations.h"
#include "RXSail.h"
#include "RelaxWorld.h"
#endif
#include "MTParser.h"  
#include "MTParserPrivate.h" 
#include "RXMTEnquire.h"
#include "RXRelationDefs.h"
#include "RXENode.h"
#include "RXGraphicCalls.h"
#include "RXQuantity.h"
#include "RXExpression.h"
#ifdef RXQT
extern "C" int RXPrint(class QObject *q, const char*s,const int level);
extern "C" int rxWorkerDisplayMsgBox(class QObject *q,const QString s , const int level);
#endif

int MyVarFactory::m_VarIndex=0;

RXMTInternalVariable::RXMTInternalVariable(){
    m_exp=0;
}
RXMTInternalVariable::~RXMTInternalVariable(){
    m_exp=0;
}

const MTCHAR* RXMTInternalVariable::getSymbol()
{ 
    return (m_MTIVname.c_str());
}    
MTDOUBLE RXMTInternalVariable::evaluate(unsigned int nbArgs, const MTDOUBLE *pArg)
{
    return m_exp->evaluate ();
}
MTVariableI* RXMTInternalVariable::spawn() throw(MTParserException)
{
    return new RXMTInternalVariable();
}




MyVarFactory::MyVarFactory(RXExpression *p)
{
    m_rxexp=p;
    //m_rxel=l;
}
MyVarFactory::MyVarFactory():
    m_rxexp(0)
{
    assert(0); // shouldnt call this constructor
}
MTVariableI* MyVarFactory::create(const MTCHAR *symbol)
{
    RXMTInternalVariable *v = new RXMTInternalVariable();

    RXExpressionI *l_e=0;

    if(m_rxexp->GetOwner() && m_rxexp->GetOwner()->FindExpression (symbol,rxexpLocalAndTree,&l_e)){ // remember FindExpression walks up the parent tree of m_rxel
        if(l_e==m_rxexp )
        {
            RXEntity_p aaa = dynamic_cast<RXEntity_p> ( m_rxexp->GetOwner());
            QString buf("recursive definition in expression named `");
            buf+= QString::fromStdWString(m_rxexp->GetOName() );
            buf+= QString("`\n expressiontext is `");
            buf+= QString::fromStdWString(m_rxexp->GetText()  );
            buf+=  QString("`\n");
            if(aaa) {
                buf += " in object " ;
                buf+= aaa->type() ;
                buf+=" , " ;
                buf+= aaa->name();
            }
            cout<< qPrintable(buf)<<endl;
            m_rxexp->GetOwner()->OutputToClient(buf,99);

            delete v;
            MTTHROW(MTExcepData (	MTLOCEXCEP_MTParserInfoObjectNotFound ) );//throwParsingExcep(MTDEFEXCEP_ItemNotFound, symbol);

        }
        v->SetExpression(l_e);
        v->SetMTIVName(symbol);
        RXENode*lll = m_rxexp->GetOwner(); assert(lll);
        l_e->SetRelationOf(m_rxexp,(child|niece),RXO_FACTORYVARIABLE,  this->m_VarIndex++ );
        // getowner is looking at v
    }
    else
    {
        // if we get here during a resolve,  this->m_OwnerNode will point to the entity being resolved
        // this->m_model is the sail with its m_ReportForm
        // and we want a 'set' of name 'symbol'
        if(this->m_rxexp->Model()){
            RXEntity* pp = this->m_rxexp->Model()->Get_Key_With_Reporting("set",ToUtf8(symbol).c_str(),true);

            if(pp) { // the recursive GetKey found it and resolved it.
                if(m_rxexp->GetOwner() && m_rxexp->GetOwner()->FindExpression (symbol,rxexpLocalAndTree,&l_e))
                {
                    v->SetExpression(l_e);
                    v->SetMTIVName(symbol);
                    RXENode*lll = m_rxexp->GetOwner(); assert(lll);
                    l_e->SetRelationOf(m_rxexp,(child|niece),RXO_FACTORYVARIABLE,  this->m_VarIndex++ );
                    // getowner is looking at v
                    return v;
                }
            }
            else // no pp. we have to throw
            {
                assert(!pp);
                RXEntity_p aaa = dynamic_cast<RXEntity_p> ( m_rxexp->GetOwner());
                QString buf("cant find variable '");
                buf+=QString::fromStdWString(symbol);
                buf+=("'\nin expression named `");
                buf+= QString::fromStdWString(m_rxexp->GetOName() );
                buf+= QString("`\nexpressiontext is `");
                buf+= QString::fromStdWString(m_rxexp->GetText()  );
                buf+=  QString("`\n");
                if(aaa) {
                    buf += " in object `" ;
                    buf+= aaa->type() ;
                    buf+=" , " ;
                    buf+= aaa->name() ;
                    buf+="`";
                }
                int ret = m_rxexp->GetOwner()->OutputToClient(buf,99);
                switch (ret) {
                  case QMessageBox::Yes:
                      // Save was clicked
                      break;
                  case QMessageBox::No:
                      // Don't Save was clicked
                       break;
                  case QMessageBox::Cancel:
                      // Cancel was clicked
                      break;
                  default:
                      // should never be reached
                      break;
                }
            }
        }// if model
        delete v; v=0;
        MTTHROW(MTExcepData (	MTLOCEXCEP_MTParserInfoObjectNotFound ) );//throwParsingExcep(MTDEFEXCEP_ItemNotFound, symbol);
    }
    return v ;
}

MTVariableFactoryI* MyVarFactory::spawn()
{
    return new MyVarFactory ();
}

MyStringVarFactory::MyStringVarFactory(RXExpression *p) // why not pass the expression not its list??
{
    m_rxexp=p;
    //m_rxel=l;
}
MyStringVarFactory::MyStringVarFactory():
    m_rxexp(0)
{
}
MTVariableI* MyStringVarFactory::create(const MTCHAR *symbol)
{
    RXMTStringVariable *v = new RXMTStringVariable();

    RXExpressionI *l_e=0;

    if(!m_rxexp) {
        v->SetMTIVName(symbol);
        return v;
    }
    cout<< " inheritance with strings not tested"<<endl;
    if(m_rxexp->GetOwner() &&  m_rxexp->GetOwner()->FindExpression (symbol,rxexpLocalAndTree,&l_e)){ // remember FindExpression walks up the parent tree of m_rxel
        v->SetExpression(l_e);
        v->SetMTIVName(symbol);
        RXENode*lll = m_rxexp->GetOwner(); assert(lll);
        l_e->SetRelationOf(m_rxexp,(child|niece),RXO_FACTORYVARIABLE);
        // getowner is looking at v
    }
    else
    {
        delete v; v=0;
        MTTHROW(MTExcepData (	MTLOCEXCEP_MTParserInfoObjectNotFound ) );//throwParsingExcep(MTDEFEXCEP_ItemNotFound, symbol);
    }
    return v ;
}

MTVariableFactoryI* MyStringVarFactory::spawn()
{
    return new MyStringVarFactory ();
}









RXExpressionDble:: RXExpressionDble(void):x(0) { }
RXExpressionDble:: ~RXExpressionDble(void) { }
RXExpressionDble::RXExpressionDble(const RXSTRING &name,const RXSTRING &p_buf,  class RXSail *const p_sail)
    : RXExpressionI(name,p_buf,p_sail)
{
    wchar_t *eptr;
    x= wcstod(p_buf.c_str(),&eptr);
    size_t i = p_buf.length ();
    size_t j = eptr-p_buf.c_str();
    //	if(i!=j) cout<<" do you have the 'janet' flag turned on?"<<endl;
    this->SetAccessFlags(RXE_PRIVATE); // so they don't get exported

}

MTDOUBLE RXExpressionDble:: evaluate()  { return x;}

MTDOUBLE RXExpressionDble:: evaluate(const MTCHAR *expr) throw(MTParserException){
    MTDOUBLE rv=0;
    // if we can read a number from expr, return it
    // else fire an exception
    assert(0);
    return rv;

}
bool RXExpressionDble:: ResolveExp() {
    bool rc=false;
    assert(0);
    return rc;
}


// returns 1 if it did change and sets NeedsComputing on the owning entity
int RXExpressionDble::Change(const RXSTRING &s) {
    int rc=0;
    wchar_t *eptr;
    const wchar_t *start = s.c_str();
    MTDOUBLE y = wcstod(start,&eptr);
    size_t i = s.length();
    size_t j = eptr-start;
    assert(i==j);
    if(fabs(y-x) > ON_EPSILON) { x=y; return 1;}
    return 0;
}
MTSTRING RXExpressionDble::GetExpression(void)const{
    MTSTRING  s;
    //    wchar_t buf[256];
    //    swprintf(buf,256,L"%g",x);
    QString buf; buf=QString("%1").arg(x);// for minGW
    //s= buf.toStdWString();
    s=	RX_QSTRINGTOWSTRING(buf);
    return s;
}
int RXExpressionDble::Resolve(){
    return 1;
}
int RXExpressionDble::Kill() {
    assert(!this->IsBeingKilled());
    int rc= this->RXObject::Kill();
    delete this;
    return rc;
}
int RXExpressionDble::Compute()
{
    cout<<"(RXExpressionDble::Compute) we don't want this function to compute straight away "
          "we just want it to set 'needs_Computing' on any entity nieces. "
          "(and that includes its own parent if that parent is an entity"<<endl;
    int rc=0;

    return rc;
}

int RXExpressionDble::Finish()
{
    int rc=0;
    assert(0);
    return rc;
}


RXSTRING RXExpressionDble::GetText() const
{
    RXSTRING rv = TOSTRING(this->x);
    return rv;
}

int  RXExpressionDble::Print(FILE *fp) const {
    int rc=0;
    rc=fprintf(fp, " ConstExpression '%S', '%S'\n",this->GetOName().c_str(), this->GetText().c_str());
    RXObject::PrintOMap(fp);
    return rc;
}
QString  RXExpressionDble::Descriptor()const
{
    QString q("expdble_"); q+=this->GetQName();
    RXEntity_p e = dynamic_cast<  RXEntity_p>(this->GetOwner());
    if(e)
    {
        q.prepend(e->Descriptor());
    }

    if(this->Model())
        q.prepend(this->Model()->GetQType());
    return q;
}

QString  RXExpression::Descriptor()const
{ int i=0;
    QString q("exp_"); q+=this->GetQName();
    RXEntity_p e = dynamic_cast<  RXEntity_p>(this->GetOwner());
    if(e)
       {i++; q.prepend(e->Descriptor()+"$");}
    RelaxWorld *ww = dynamic_cast<RelaxWorld *>(this->GetOwner());
    if(ww)
       {i++; q.prepend("world$");}
    if(i)
        return q;
    if(this->Model())
        q.prepend(this->Model()->GetQType()+"_");
    return q;
}
RXExpression::RXExpression(void)
    : RXExpressionI(),
      m_needscompiling(true)

{
    assert("Dont use the default RXExpression constructor"==0);
    m_MessageboxRetVal = IDNO;
    m_Text=_T("undefined");
}

RXExpression::~RXExpression(void)
{	class RXENode *en ;
    if(m_debug) { RXTAB(dbgc)  ; printf("~RXExpression '%S'\n" , GetOName().c_str());fflush(stdout);}
    if(GetOwner() ) {
        en = dynamic_cast<class RXENode *>(this->GetOwner() );
        if(en) en->RemoveExpression(this->GetOName());
    }
    //this->OnTopologyChange ();
    m_Text.clear();
    this->CClear ();
}
int  RXExpression::CClear()  
{
    this->UnResolveExp();
    this->RXObject::CClear (); // sept 2010  trial
    return 0;
}
int RXExpression::Kill() {
    assert(!this->IsBeingKilled());
    int rc= this->RXObject::Kill();
    delete this;
    return rc;
}
// returns 1 if it did change and sets NeedsComputing on the owning entity
int RXExpression::Fill(const RXSTRING &s) // called from RXOffset::fill
{
    RXSTRING oldText=m_InputText;
    if(on_wcsicmp( s.c_str(),this->m_Text.c_str()) ==0)
        return 0;

    // dont do an unresolve

    if(m_debug) wcout << L"RXExp::Fill "<< this->GetOName () <<L" from <"<<oldText<<L"> to <"<<s<<L">" <<endl;
    m_Text= s; rxstriptrailing(m_Text); m_InputText=m_Text;
    if(!this->ResolveExp())
    {
        RXSTRING buf = _T("something missing in expression '")
                +s
                +_T("' \n so reverting to value '")
                +oldText
                +_T("'");
        Model()->OutputToClient (buf,2);
        m_Text= oldText; m_InputText=oldText;
        this->resetExpression();
        int kk=this->ResolveExp(); assert(kk);
    }

    this->SetNeedsComputing();

    return 1;
}

// p_buf may contain macro definitions as in 
//	parser.defineMacro(_T("euc(x,y)"), _T("sqrt(x^2+y^2)") , _T("Euclidean distance"));
//$macro$ euc(x,y)$sqrt(x^2+y^2)$ Euclidean distance
// and the expression definition as in
//$expression$ x+y+z 
// returns 1 if it did change and sets NeedsComputing on the owning object
int RXExpression::Change(const RXSTRING &s)
{
    int rc=0;
    RXSTRING oldText=m_InputText;
    if(on_wcsicmp( s.c_str(),this->m_Text.c_str()) ==0)
        return 0;

    this->undefineAllVars(); this->m_needscompiling =true;
    if( Model()) {
        if(!this->m_accessflags&RXE_READONLY)
        {
            Model()->SetFlag(FL_NEEDS_GAUSSING);
            Model()->SetFlag(FL_NEEDS_RUNNING);   // overkill, but trim changes dont provoke it later
        }
    }

    if(0)  {
        QString qName = QString::fromStdWString(this->GetOName() );
        {
            RXObject* o = dynamic_cast<RXObject*> (this->GetOwner() );
            //  It would be better if this were handled entirely by the nicee system

            if(o) {
                QString qOname = QString::fromStdWString(o->GetOName() );
                if(! this->FindInMap(o,niece))
                    cout<<" why isnt ownerNode "<<qPrintable(qOname) <<" a niece of "<< qPrintable(qName)<<" ??"<<endl; // if this is true we dont need to set needs comp
               // o->SetNeedsComputing()  ;
                cout<<" Are we sure ou need to set NC on the  expression's owner? Try without"<<endl;
            }
        }
        if(m_debug)  cout <<  "RXExp::Ch-nge "<< qPrintable (qName) << " from <"<<oldText.c_str()<<L"> to <" << ">" <<endl; // to s

    }
    m_Text= s; rxstriptrailing(m_Text); m_InputText=m_Text;
    if(!this->ResolveExp())
    {
        RXSTRING buf = _T("something missing in expression '")
                +s
                +_T("' \n so reverting to value '")
                +oldText
                +_T("'");
        Model()->OutputToClient(buf,2);
        m_Text= oldText; m_InputText=oldText;
        this->resetExpression();
        int kk=this->ResolveExp(); assert(kk);
    }
    this->SetNeedsComputing(1);// this is in  RXExpression::Change
    return 1;
}

// p_buf may contain macro definitions as in 
//	parser.defineMacro(_T("euc(x,y)"), _T("sqrt(x^2+y^2)") , _T("Euclidean distance"));
//$macro$ euc(x,y)$sqrt(x^2+y^2)$ Euclidean distance
// and the expression definition as in
//$expression$ x+y+z 

RXExpression::RXExpression(const RXSTRING &name,const RXSTRING &p_Buf,  class RXSail *const p_model)
    : RXExpressionI(name,p_Buf,p_model),
      m_needscompiling(true),
      m_MessageboxRetVal(IDIGNORE),
      #ifdef DEBUG
      m_debug(false) // true
    #else
      m_debug(false)
    #endif
{
    assert(GetOwner() ==0);
    SetOwner(0)  ;

    QString line;//, wbuf; 	ON_String buf;
    //ON_wString w1,w2,w3;
    SetOName(name);

    int k, nw=0;
    defineConst(_T("pi"), 3.14159265358979);
    defineConst(_T("e"), 2.71828182845905);

    defineFunc(new  RXDBFnc(p_model,this));
    defineFunc(new  RXCurveLength(p_model,this));
    defineFunc(new  RXSiteX(p_model,this));
    defineFunc(new  RXSiteY(p_model,this));
    defineFunc(new  RXSiteZ(p_model,this));

    line=QString::fromStdWString( p_Buf );

    line=line.trimmed() .toLower();


    line.replace ("[","(");
   line.replace ("]",")");
    line.replace ("\r"," ");
   line.replace ("\n"," ");


    m_Text=line.toStdWString();   m_InputText=line.toStdWString();
} // RXExpression::RXExpression

/** @brief RXExpression::Grandfather
 * enables string constants to be defined simply by enclosing in quotes
 *this removes the nasty original syntax like
 *            $stringconst$kkk$pt1$expression$x(kkk)
 *and lets you use something like
 *   x("pt1") + y("pt2")
 *instead
 *
  **/
int  RXExpression::Grandfather()
{
    int nw,i, rc=0;
    if(m_Text.find(L"""") ==wstring::npos)
        return 0;
    QString line, newhead,newexp,newline;
    QString vname;
    QStringList wds;
    line = QString::fromStdWString(m_Text);

    wds = line.split(QRegExp("\""));
    nw=wds.count();
    if(nw<3)
        return 0;
    if(!(nw&1))
    {
        qDebug()<<" quotes dont match in '"<<line<<"'";
        return -1;
    }
    for(i=1;i<nw;i+=2)
    {
        vname = QString("kkk%1").arg(i-1);
        newhead+= "$stringconst$"+vname+"$"+wds.at(i);
        newexp+=wds.at(i-1);
        newexp += vname;
    }
    newline = newhead + "$expression$"+newexp+wds.back();

    m_Text=newline.toStdWString();
    return rc;

}

bool RXExpression::ResolveExp() // shouldnt do it twice 
{
    if(!m_needscompiling)
        return true;
    m_MessageboxRetVal=IDIGNORE;

    QString line;
    QString w1,w2,w3;
    QStringList wds;
    int i ;  // index of current word

    // p_buf may contain macro definitions as in
    //	parser.defineMacro(_T("euc(x,y)"), _T("sqrt(x^2+y^2)") , _T("Euclidean distance"));
    //$macro$ euc(x,y)$sqrt(x^2+y^2)$ Euclidean distance
    // and the expression definition as in
    //$expression$ x+y+z
    //  int nw=0;
    // Feb 2014 If we detect a quoted string in m_Text we take it to be a string constant
    // so we convert
    //  x("ipoint")
    //to
    //           $stringconst$kkk$ipoint$expression$x(kkk)
    this->Grandfather();

    line = QString::fromStdWString(m_Text);
    wds = line.split("$"); i=0;
    do{
        w1=wds.at(i++);
        if(!w1.length()&&!line.length())
            break;
        if(w1== "macro"){
            w1=wds.at(i++);
            w2=wds.at(i++);
            w3=wds.at(i++);
            try{
                enableAutoVarDefinition(true, new MyVarFactory());
                defineMacro( w1.toStdWString(), w2.toStdWString() , w3.toStdWString());
            }
            catch( MTParserException &e )
            {
                QString buf(" macro failed to initialize \n") ;
                buf+=   w1   +QString("\n")
                        +w2 +QString("\n")
                        + w3 +QString("\n\n")
                        +QString::fromStdWString(e.m_description.c_str());
                qDebug()<<buf;
                ///m_MessageboxRetVal=rxerror(buf,3);
                return false;
            }// end catch
        } // end if macro

        else
            /*
            if(w1==ON_wString("stringconst")){
                w1=nextword(line,'$');
                w2=nextword(line,'$');

                try{
                    //	enableAutoVarDefinition(true, new MyVarFactory(this));
                    enableAutoVarDefinition(false);
                    MTSTRING *s= new MTSTRING(w2);
                    MTCHAR *c = w1.Array ();
                    defineVar    (c,s);
                }*/

            if(w1== "stringconst"){
                w1=wds.at(i++);
                w2=wds.at(i++);

                try{
                    //	enableAutoVarDefinition(true, new MyVarFactory(this));
                    enableAutoVarDefinition(false);
                    const std::wstring ww1 = w1.toStdWString();
                    const std::wstring ww2 = w2.toStdWString();
                    MTSTRING *s= new MTSTRING(ww2);
                    const  MTCHAR *c = ww1.c_str (); // peter added the 'const ' dodgy
                    defineVar    (c,s);
                }
                catch( MTParserException &e )
                {
                    QString buf(" macro failed to initialize \n");
                    buf+= w1+QString("\n")
                            +w2+QString("\n")
                            +w3 +QString("\n\n");
                    buf=buf+QString::fromStdWString(e.m_description.c_str());
                    ///m_MessageboxRetVal=rxerror(buf,3);
                    return false;
                }// end catch
            } // end if macro

            else if(w1=="equation" || w1=="expression"  ){
                m_Text=wds.at(i++).toStdWString();
            }

    } while(i<wds.length() && line.length ());//do
    // her w1 will be the last word if m_text is empty
    if(m_Text.empty()) m_Text=w1.toStdWString();
    if(m_Text.empty ()) {
        cout<< "empty text in  Parser Expression "<<endl;
        return false;
    }

    enableAutoVarDefinition(true, new MyVarFactory(this )); // Why create another Factory?? VG reports a LEAK
    try{
        compile(m_Text.c_str());
        m_MessageboxRetVal =IDOK;
        m_needscompiling=false;
    }
    catch( MTParserException &e ) // this will identify any undefined function.
    {
        QString msg("Failed to parSE '") ;//= _T("(We should handle undefined vars transparently and let the Resolve system report them)\n while parsing... ");
        QString buf= msg +QString("' in '") +QString::fromStdWString(m_Text) +QString("'");
#ifdef linux
        qDebug()<<qPrintable(buf) <<endl;
        m_MessageboxRetVal = IDABORT; // 17 oct 2013 was IDOK;
        if(m_MessageboxRetVal==IDABORT   ||m_MessageboxRetVal == IDNO  )
        {
            // cout<< "abort or no"<<endl;
            return false;
        }
#endif
        buf.clear();
        for( unsigned int t=0; t < e.size(); t++ )
        {
            buf+=QString::fromStdWString(e.getException(t)->getDescription() );
        }
        if(!buf.isEmpty() )
            cout<<qPrintable(buf)<<endl;
        return false;
    }// end catch e
    catch(...)
    {
        QString msg("Failed to parse(2)'") ;//= _T("(We should handle undefined vars transparently and let the Resolve system report them)\n while parsing... ");
        QString buf= msg +QString("' in '") +QString::fromStdWString(m_Text) +QString("'");
#ifdef linux
        cout<<qPrintable(buf) <<endl;
        m_MessageboxRetVal = IDOK;
        if(m_MessageboxRetVal==IDABORT   ||m_MessageboxRetVal == IDNO  )
        {
            //     cout<< "abort or no"<<endl;
            return false;
        }
#endif
        return false;
    }

    return true;
}

int  RXExpression::UnResolveExp(){ //strip back to m_text;
    int rc=0;

    if(m_debug){dbgc++; RXTAB(dbgc);  wcout << L"RXExp::UnR-solveExp "<< this->GetOName () <<L" exp= <"<<m_Text<<L">" <<endl;}
    this->undefineAllVars(); this->m_needscompiling =true;

    this->OnDelete();//   a bit strong.
    if(m_debug)dbgc--;
    return rc;
}

ON_wString RXExpression::nextword(ON_wString &line, const char delim) {

    int k1,nw=0;
    ON_wString w1;

    line.TrimLeft();
    if (line[0]==delim){
        line=line.Mid(1);
        k1 = line.Find(delim);
        if(k1<0)
        { line.TrimLeftAndRight(); w1=line; line.Empty(); return w1;}
        w1=line.Left(k1); ;
        line=line.Mid(k1);
        w1.TrimLeftAndRight();
        return w1;
        // nw++;
    } // if

    line.TrimLeftAndRight();w1=line; line.Empty(); return w1;
}
MTDOUBLE RXExpression::evaluate() {
    MTDOUBLE rc=0;
    bool bb;
    if(m_needscompiling) {
        bb=this->ResolveExp (); m_needscompiling=false;
    }
    try{
        rc=MTParser::evaluate ();
    }
    catch( MTParserException &e )
    {
        ON_String buf=ON_String("(Parser Exception) expression failed to evaluate :") ;
        buf=buf+ON_String(e.m_description.c_str());
        cout <<" parser: "<<buf.Array()<<endl;
        ///m_MessageboxRetVal=rxerror(buf,3);
        return 0;
    }// end catch
    catch(  RXException &e) {
        qDebug() << "(RXxception) evaluation failed on  '" <<  QString::fromStdWString(m_Text)<<"'" <<endl; e.Print(stdout);
    }
    catch(...) {
        qDebug() << "(general exception) evaluation failed on  '" << QString::fromStdWString(m_Text)<<"'" <<endl;
    }
    return rc;
}



int RXExpression::Print(FILE *fp)const {
    int rc;
    rc=fprintf(fp, "\n RX Expression '%S',\n Inputtext= '%S'\n",this->GetOName().c_str(), this->m_InputText.c_str());
    RXSTRING m_Text; // use m_Name from RXO,m_name;

    rc+=fprintf(fp, "m_needscompiling = %d, flags= %d\n",m_needscompiling, this->AccessFlags() );

    MTVariableI *vi;RXMTInternalVariable *v;
    rc=fprintf(fp, "\tVariables of '%S' \n",this->GetOName().c_str() );
    MTRegistrarI * r = getRegistrar();
    int n = r->getNbDefinedVars();
    for(int i=0; i<n;i++) {
        vi = r->getVar(i);
        v =dynamic_cast<RXMTInternalVariable *> (vi);
        if(v)
            rc+=fprintf(fp,"\t%d  %S  %S  \n",i, v->getSymbol(), v->GetExpressionPtr()->GetOName().c_str());
    }

    rc+=RXObject::PrintOMap(fp);

    return rc;
}
int RXExpression::Compute()
{
    //    wcout<<L" Compute Exp  "<< this->GetOName() <<endl;
    //        cout<<" (RXExpression::Compute )we don't want this function to compute straight away "
    //        <<	"we just want it to set 'needs_Computing' on any entity nieces. "
    //		<<"(and that includes its own parent if that parent is an entity"<<endl;
    int rc=0;

    return rc;
}
int RXExpression::Resolve()
{
    if (! this->GetOwner() )
        cout<<" Resolving an expression too early - inheritance not yet hooked up"<<endl;

    int rc=this->ResolveExp();

    return rc;
}
int RXExpression::Finish()
{
    int rc=0;
    assert(0);
    return rc;
}
int RXExpression::UnResolve()
{
    int rc=0;
    cout<<"RXExpression::UnResolve "<<endl;

    // here we have to reduce it to the state before REsolveExp()
    rc = this->UnResolveExp();
    // rc+=RXObject::CClear(); dont do this. its just an OnTopologyChange
    return rc;
}

RXSTRING RXExpression::GetText() const
{
    return m_InputText;
}

class RXExpressionI* RXENode::AddExpression( class RXExpressionI* p)
{
    //"dont forget to pass 'p_sail' to the new expressions m_owner. This is so write script can search
    // up the tree to write all expressions with the same p_sail.

    pair<RXEXMAP::iterator,bool> ret;
    ret=m_xmap.insert (pair<RXSTRING,RXExpressionI* >( p->GetOName(), p)) ;

    if (ret.second==false)
        printf("element '%S' already existed in this ExpList",p->GetOName().c_str());
#ifdef DEBUG
    RXExpressionI* p2=  ret.first->second ;
    assert(p==p2);
#endif
    p->SetOwner (this);
    // this was a bad idea because its better to decide explicitly what relationship to apply between each expression and its entity
    //      RXObject *thiso = dynamic_cast<RXObject *> (this);
    //        if(thiso)
    //               thiso->SetRelationOf(p,aunt,RXO_EXP_OF_ENT);
    return p;
} // RXENode::AddExpression

class RXExpressionI* RXENode::AddExpression(const QString &name,const QString &equation,  class RXSail *const p_sail)
{
    return AddExpression(name.toStdWString(), equation.toStdWString(),p_sail);
}

class RXExpressionI* RXENode::AddExpression( const RXSTRING & name, const RXSTRING &equation,  class RXSail *const p_sail) 
{
    //dont forget to pass 'p_sail' to the new expressions m_owner. This is so write script can search
    // up the tree to write all expressions with the same p_sail.

    //   RXEXMAP::iterator it;
    pair<RXEXMAP::iterator,bool> ret;
    ret=m_xmap.insert (pair<RXSTRING,RXExpression* >(name,new RXExpression(name, equation,p_sail))) ;
    //cout<< "RXENode::AddExpression is where we should catch an expression which can't find what it needs to resolve "<<endl;
    if (ret.second==false)
        wcout <<_T("element  already existed in this ExpList  :") <<name <<endl;

    RXExpressionI* p=  ret.first->second ;
    p->SetOwner(this);
    if(m_debug){RXTAB(dbgc); wcout << L"RXENode::AddExp  "<<L" exp= <"<<name<<L">=<" <<equation<<">"<<endl;}
    return p;
} // RXENode::AddExpression




RXENode::RXENode() :
    m_parent(0),
    m_client(0),
    m_GlcNode(0),
    m_xmapChangeCount(0),
    #ifdef RXEXP_DEBUG
    m_debug(true)
  #else
    m_debug(false)
  #endif

{

}
int RXENode::EvaluateAllReadOnly(){
    int rc=0;
    RXEXMAP::iterator itx;
    for (itx =  m_xmap.begin (); itx != m_xmap.end(); ++itx){//safe
        RXExpressionI* exp=itx->second ;
        class RXQuantityReadOnlyDble* qrod = dynamic_cast<class RXQuantityReadOnlyDble*>(exp);
        if(qrod )
            rc+= qrod->Compute(); // exp->evaluate();
        else {
            class RXQuantityWithPtr *qptr = dynamic_cast<class RXQuantityWithPtr *>(exp);
            if( qptr)
            {
                // exp->evaluate();
                rc+= qptr->Compute();
            }
        }
    }//for
    return rc;
}
void RXENode::SetParent(RXENode*p) { // call with null clears it from its p-rents m_children.
#ifdef USE_ENODECHILDREN
    if(p)
        p->m_enodechildren.insert(this);
    else if (m_parent)  // called with null
        m_parent->m_enodechildren.erase(this);
#endif
    m_parent = p;

} 
RXENode::~RXENode()
{
    //  delete m_GlcNode; except that SoGroup cant be deleted and whats more,
    // it lives in a different thread
    CClear();
}
class QObject*   RXENode::Client( ) const
{
    if(this->m_client ) return this->m_client;
    if(this->m_parent ) return m_parent->Client();
    return NULL;
}

RXGRAPHICSEGMENT  RXENode::GNode(const int flag) const
{
    if(m_GlcNode) return m_GlcNode;
    if(flag !=rxexpLocalAndTree) return 0;
    if(this->m_parent ) return m_parent->GNode();
    return NULL;

}
int RXENode::CClear() // delete its expressions and detach its children.
{	RXEXMAP::iterator it;
    if(m_debug){RXTAB(dbgc) ; wcout << "RXENode::CClear "<<endl; }
    if(m_xmap.size()){
        for(;m_xmap.size();){
            it=m_xmap.begin();
            RXExpressionI *e = it->second;

            if(e->IsBeingIterated())   // a method of RXObject. Messy but its needed.
            {
                m_xmap.erase(it);
            }
            else
                e->Kill();
            m_xmapChangeCount++;

        }
    }
#ifdef USE_ENODECHILDREN
    std::set<RXENode*>::iterator itc;
    for(;m_enodechildren.size();){
        itc=m_enodechildren.begin();
        RXENode *e = *itc;
        e->SetParent(0);
    }
#endif
    if(m_debug){RXTAB(dbgc) ;  wcout << "Leave RXENode::CClear "<<endl; }
    return 1;
}
// the idea of ClearExpList is to disconnect this ENode from any expressions which 
int RXENode::ClearExpList( const SAIL*p_owner){
    RXEXMAP::iterator it;
    int previouscount =0;
    int rc=0;
    if(m_debug) {
        wcout << "RXENode::ClearExpList ";
        RXObject * ep = dynamic_cast<RXObject *>(this); if(ep) wcout << ep->GetOName ();
        wcout <<  endl;
    }
    assert(p_owner);
    // I cant remember whether map.erase invalidates the iterator, so lets do one at a time.
    do{
        previouscount =m_xmapChangeCount;
        for(it=m_xmap.begin();it!=m_xmap.end(); it++){
            if(it->second->Model()==p_owner) {
                m_xmap.erase(it); m_xmapChangeCount++; if(m_debug) cout<<"deleting from XMAP"<<endl;
                rc++;   break;
            }
        }
    }while( previouscount !=m_xmapChangeCount );
    if(m_parent)
        rc+=m_parent->ClearExpList(p_owner);
    return rc;
}

bool RXENode::RemoveExpression(const RXSTRING & name)
{
    RXEXMAP::iterator it=m_xmap.find(name ) ;
    if(it ==m_xmap.end())
        return false;
    if(m_debug){RXTAB(dbgc); wcout << L"RXENode::RemoveExpression  "<<L" exp= <"<<name<<L"> (invalidate any iterator)"<<endl;}
    m_xmap.erase(it); m_xmapChangeCount++;
    return true;
}
RXExpressionI * RXENode::FindExpression(const RXSTRING& text,const enum  RXEPL_Flags  f,RXExpressionI **e)const //looks in this->explist and recursively in this->parent->explist. 
{	
    RXExpressionI *rc=0;
    if(e) *e=0;
    RXEXMAP::const_iterator it;
    switch(f) {
    case rxexpLocal:
        it=m_xmap.find(text ) ;
        if(it ==m_xmap.end())  {
            return 0;
        }
        else  {
            rc = it->second;
            if(e)
                *e=rc;
            return rc;
        }
    case rxexpLocalAndTree: // start in the current ENode. If not found, walk up the tree
        it=m_xmap.find(text ) ;
        if(it ==m_xmap.end())  {
            if(m_parent)
                return m_parent->FindExpression (text,rxexpLocalAndTree,e);
            else
                return 0;
        }
        else  {
            rc = it->second;
            if(e) *e=rc;
        }
        return rc;

    case rxexpJustTree: // start the search with the parent.
        if(m_parent)
            return m_parent->FindExpression (text,rxexpLocalAndTree,e);
        else
            return 0;
    };

    return 0;
}

RXExpressionI * RXENode::FindAndChange(const RXSTRING &exname,const RXSTRING &value, const enum RXEPL_Flags &f) // called from the DB, this searches down the tree not up it like FindExpression
{
    int k=0;
    RXExpressionI *e = this->FindExpression(exname,f);//rxexpLocal
    if(e) {
        k=e->Change (value); // this calls unresolve which calls OnValueChange
    }
    return e;
}

// writes the expressions in the list as SET entities
int RXENode::WriteToScript(FILE * fp,const wchar_t* p_where, const class RXSail *p_model)const  
{
    int rc=0;
    if(!fp)
        return 0;
    RXObject *o=0;
    RXEXMAP::const_iterator it;
    for(it=m_xmap.begin();it!=m_xmap.end(); it++){
        RXExpressionI* p=it->second ;
        if(p_model && p->Model()!=p_model) continue;
        // if its a spawn continue
        if(!p->FindInMap(o,frog))
            rc+=fprintf(fp,"set\t %S \t %S \t %S ! (no Frog) expression=%S\n",p->GetOName().c_str(), p->GetText().c_str(),p_where, p->GetExpression().c_str());
    }
    if(m_parent) // usually m_parent is the world and this call writes our model's global variables.
        rc+=m_parent->WriteToScript(fp,p_where, p_model);
    return rc;
}
int  RXENode::ExportDependencies(wostream &s,const wstring &head, const int recur, const int what ) const
{
    int rc=0;
    if(!s.good())
        return 0;
    // objects are found in:
    //  world/enode ::m_xmap
    //  sail/enode::xmap
    //  sail::emap
    // entity/Object::
    // entity/ENode
    // entity::


    RXEXMAP::const_iterator it;
    for(it=m_xmap.begin();it!=m_xmap.end(); it++){
        RXExpressionI* p=it->second ;
        //   if(p->AccessFlags()&RXE_PRIVATE)
        //       continue;
        s<<head;
        s<<L"$";
        s<<p->GetOName();
        s<<"=";
        wstring temp = p->GetText();
        //     rxreplacestring(temp,_T("*( _a_a_)"),L"%");
        s<<temp<<endl;
        rc++;
    }

    return rc;
}
int RXENode::Export(wostream &s,const wstring &head, const int recur ) const
{
    int rc=0;
    if(!s.good())
        return 0;
    RXEXMAP::const_iterator it;
    for(it=m_xmap.begin();it!=m_xmap.end(); it++){
        RXExpressionI* p=it->second ;
        if(p->AccessFlags()&RXE_PRIVATE)
            continue;
        s<<head;
        s<<L"$";
        s<<p->GetOName();
        s<<"=";
        wstring temp = p->GetText();
        rxreplacestring(temp,_T("*( _a_a_)"),L"%");
        s<<temp<<endl;
        rc++;
    }

    return rc;
}


int RXENode::List(wostream &s,const string &head, const int depth ,const int recur ) const

{
    int rc=0;

    return 0;
    FILE *fp=0;
    RXEXMAP::const_iterator it;
    for(it=m_xmap.begin();it!=m_xmap.end(); it++){
        RXExpressionI* p=it->second ;
        for(int i=0; i<depth;i++) rc+= fprintf(fp," \t");
        rc+=fprintf(fp,"Expression(%s)\t %S \t %S ",p->What(),p->GetOName().c_str(), p->GetText().c_str());
        double x = p->evaluate ();
        rc+= fprintf(fp, "\t value= \t %f\n", x);
        if(recur){
            p->Print(fp);
        }
    }
#ifdef USE_ENODECHILDREN
    for(int i=0; i<depth;i++) rc+= fprintf(fp," \t");

    if(!this->m_enodechildren.size()) return rc;
    rc+=fprintf(fp," this list has %d enode children\n", this->m_enodechildren.size());
    std::set<RXENode*>::const_iterator itc;
    for(itc=this->m_enodechildren.begin(); itc!= this->m_enodechildren .end(); ++itc) {
        RXENode*n= *itc;
        rc+=n->List(fp,depth+1);
    }
#endif
    return rc;
}
int RXENode::List(FILE * fp,const string &head, const int depth ,const int recur ) const

{
    int rc=0;
    if(!fp)
        return 0;
    RXEXMAP::const_iterator it;
    for(it=m_xmap.begin();it!=m_xmap.end(); it++){
        RXExpressionI* p=it->second ;
        for(int i=0; i<depth;i++) rc+= fprintf(fp," \t");
        rc+=fprintf(fp,"Expression(%s)\t %15.15S \t`%18S` ",p->What(),p->GetOName().c_str(), p->GetText().c_str());
        double x = p->evaluate ();
        rc+= fprintf(fp, "\t value= \t %f\n", x);
        if(recur){
            p->Print(fp);
        }
    }
#ifdef USE_ENODECHILDREN
    for(int i=0; i<depth;i++) rc+= fprintf(fp," \t");

    if(!this->m_enodechildren.size()) return rc;
    rc+=fprintf(fp," this list has %d enode children\n", this->m_enodechildren.size());
    std::set<RXENode*>::const_iterator itc;
    for(itc=this->m_enodechildren.begin(); itc!= this->m_enodechildren .end(); ++itc) {
        RXENode*n= *itc;
        rc+=n->List(fp,depth+1);
    }
#endif
    return rc;
}

//  these output fns are in Enode so that
// we can walk up the parent tree until we find a client. 
//  See debug.h  The idea is to define rxerror as OutputToClient.
// but  lets do that slowly because rxerror is often called from C-style global fns.

int RXENode::OutputToClient(const RXSTRING &s, const int level)const{
    std::string c = ToUtf8(s);
    return OutputToClient(c,level);
}
int RXENode::OutputToClient(const std::string &s, const int level)const{
    return OutputToClient(s.c_str(),level);
}
int RXENode::OutputToClient(const QString &s, const int level)const{
    return OutputToClient(qPrintable(s),level);
}
int RXENode::OutputToClient(const char *s, const int level) const{
    // this is the one which will direct the text towards the interested client.  W
    // for the World  we might override it with a broadcast.
    if(level>99)
        return  RXPrint(this->Client(), qPrintable(QString("(broadcast)") +QString(s)),level);
    if (level>1)
        return  rxWorkerDisplayMsgBox(this->Client(), QString(s) ,level ) ;
#ifdef MICROSOFT
    if(level>1) return AfxMessageBox(CString(s));
#endif
    if(level>0)
        if(s) cout<<"msg code="<<level<<":  "<<s<<endl;

#ifdef RXQT

#endif
}
