/* Custom menu data */

#ifndef CUSTOM_16NOV04
#define CUSTOM_16NOV04

//#include "Graphic Struct.h"

struct Custom_Button {
 int used;
char *label, *where;
char hotkey;
char*action;
Graphic *g;
}; 
#define MAXCUSTOM 100
EXTERN_C struct Custom_Button  g_buttons[MAXCUSTOM];
#ifdef _X
int Insert_Custom_Buttons(const char*what,Widget pulldownPane, Graphic *g);
#endif

int Create_Custom_Button(const char*line) ;

int Apply_Custom_Button(int i);


#endif //#ifndef CUSTOM_16NOV04
