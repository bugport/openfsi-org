/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Modified :
 jan 6 linux  silent option in repacking
 
  13 Feb 2004  use __wrap 
  
 8/97 RXFREE sets space to zero just before returning. This should make
	the code fall over more
  8/97  RXFOPEN creates a file called 'readfiles.txt' containing a list of all the files opened 
	This could be sorted and then 'uniq'ed  then used as input to tar.
	ideally, it would generate the tar script, including CD commands.

   7/97 added RXFOPEN FCLOSE  List_C_Files Close C Files at end
   10/96   simple malloc, calloc add 16 
  * 18.6.96 Peters memchecking version
  * 20.6.96 FUNCTIONAL CHANGE. MALLOC before did a calloc.
  * I took this out for speed but it could be dodgy

 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h" // was debug.h till June 2007
#include <QDebug>
#include "RelaxWorld.h"
#ifndef _WRAP
 	#define __real_free free
 	#define __real_malloc malloc
 	#define __real_calloc calloc
 	#define __real_realloc realloc
#endif
 


#ifdef _WRAP
	EXTERN_C void  __real_free(void*);
	EXTERN_C void* __real_malloc(long int);
	EXTERN_C void* __real_calloc(long int, long int);
	EXTERN_C void* __real_realloc(void*, long int);
 #endif

#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif
#include "words.h"
#ifdef RXQT
#include <memory.h>
#include <errno.h>
#endif
#ifdef WIN32 //this enables MS leak detection. VLD is better
//	#include <crtdbg.h>
#endif
#include "stringutils.h"
#include "akmutil.h" // for Replace_String

#include "global_declarations.h"



static long total_allocated=0;
static int BAD=0;


static char MD[64], LastMD[64];

#define LIST_SIZE 512000

static long MEMCOUNT = 0;
static long lastref=-1;
static long lowestfree=0;

#if(defined _RXMALLOC) 
	void* MALLOC(const size_t a) {void*p=malloc(a); if(!p) _rxthrow("malloc fails"); return p;}
	void* CALLOC(size_t a,size_t b) {void*p=calloc(a,b); if(!p)_rxthrow("calloc fails"); return p;}
	void* REALLOC(void *a,size_t b) {void*p=realloc(a,b); if(!p)_rxthrow("realloc fails"); return p;}

#endif



int Set_Previous_MALLOC_DATA() {
	strncpy(MD, LastMD,10);
	return 1;
}
int Set_MALLOC_DATA(const char*c) {
size_t count =strlen(c);
if(count>10) count=10;
 if(BAD ) 	 
	printf(" Set_MALLOC_DATA to <%s>\n",c);
	strncpy(MD,"##########",10);
	strncpy(MD, c,count);
	MD[9]=000;
	strncpy(LastMD, MD,10);
	LastMD[11]=0;
	return 1;
}

typedef struct MEM_BLOCK mem_block_t;
#define MAX_MEM_BLOCKS (10000)

/* comment out the next 3 lines for speed 
Leave them in for heap checking  */ 
#define     NO_MEMORY_CHECK  0    
#define SIMPLE_MEMORY_CHECK  0
#define PETERS_MEMORY_CHECK  1  
#define COMPLICATED_MEMORY_CHECK 0
#ifdef PETERS_MEMORY_CHECK
static void*MEMLIST[LIST_SIZE];
#ifdef MICROSOFT
static _CrtMemState * l_state;
static _CrtMemState * dp_state =NULL;
#endif

int Check_All_Heap(int p_flag) { // default is DBG_SILENT
long i,c=0;
/* DBG_FULLPRINT	1
DBG_SILENT	0
DBG_MARK	2
DBG_ALL 	4  */

// p_flag might be DBG_FULLPRINT or  DBG_SILENT
// p_flag might be DBG_MARK which means take a snapshot
// p_flag might be DBG_ALL which means dump since beginning
//	else check dump the last snapshot.
// so to test for a local leak, first call (DBG_SILENT+DBG_MARK)
// then call								(DBG_FULLPRINT+DBG_ALL)
int flag, bad=0;
int l_print = 0;
int l_mark = 0, l_all = 0;


if( p_flag& DBG_FULLPRINT ) l_print = 1;
if( p_flag& DBG_MARK ) l_mark = 1;
if( p_flag& DBG_ALL ) l_all = 1;

#ifdef MICROSOFT


	if(l_all)
		l_state = NULL;
	else
		l_state = dp_state;


	int OK = _CrtCheckMemory( );
	if(l_print) {
		_CrtMemDumpAllObjectsSince( l_state);
	}
//	if( OK)
		//cout<< "memory check OK\n"<<endl;
	if( !OK)
		cout<<" Memory Check failed "<<endl;

	if(l_mark) {
		if(dp_state) free(dp_state);
		dp_state = ( _CrtMemState *) malloc(sizeof(_CrtMemState ));
		_CrtMemCheckpoint( dp_state );
	}

	return 1;
#endif
for(i=0;i<MEMCOUNT;i++) {

	if(MEMLIST[i]) {
		if( l_print !=0) {
			printf(" %ld ",i); c++;
			Print_One_Heap(MEMLIST[i]); 
		}
		flag = MEMCHECK(MEMLIST[i]);
		bad=bad && !flag;
	}
}
if(l_print || bad) { // peter jan 2005 tidy
	printf(" total allocated  %ld\n",  total_allocated);
	printf(" Number of items%ld\n",  c);
	printf(" Last is               %ld\n",  MEMCOUNT-1);
	printf(" No of bad allocs=%d\n", bad);
}
Repack_Heap(l_print || bad);
BAD=0; 
return(!bad);
}

int Repack_Heap(int p_print) {
long i,c=MEMCOUNT;
void *p; 

	if(p_print) 
		cout<< "\nREPACKING (getchar)"<<endl;

	for(i=0;i<MEMCOUNT;i++) { 
		while(!MEMLIST[MEMCOUNT-1]) MEMCOUNT--;
		if(!MEMLIST[i]) {
			MEMCOUNT--;
			MEMLIST[i]= p = MEMLIST[MEMCOUNT];
			MEMLIST[MEMCOUNT]=NULL;
			RENUMBER(p,i);		
}
	}

	if(p_print) {
		printf(" repacking reduced MEMCOUNT from %ld to %ld\n",  c,MEMCOUNT );
		printf(" Last is               %ld\n",  MEMCOUNT-1);
	}
	lowestfree=MEMCOUNT;
	lastref= -1;
	STRDUP("\n JUST REPACKED\n");
	return(1);
}





long  NEXTBLANKMEM(){
long i;
	if(lastref>=0) { /* it was the last one to be freed */
 		i = lastref; lastref= -1; return i;
	}

/* last one was NOT at end  so search*/
	for(i=lowestfree;i<MEMCOUNT;i++) {
		if(!MEMLIST[i]) { lowestfree=i+1;return i;}
		}
 
/* We have to append to list  l ref already -1 */

	i = MEMCOUNT;
	lowestfree=i+1;
	
 	MEMCOUNT++;
  	if(MEMCOUNT >= LIST_SIZE) { 
		printf(" memcount now %ld / %d\n",MEMCOUNT,LIST_SIZE);
		g_World->OutputToClient("need bigger heap list",3);
		}

 return(i);

}

int Print_One_Heap(void *p) {

//long  i;
long  size;
char *s;
char *ip;
long *iptr;
char* lp ;
int k ,lim ;

s=(char*) p;

s-=32; 
lp = s;

ip = (char*) p;
ip-=12;
iptr = (long*) ip;
size = *iptr; 

lim = size+36;
if(lim > 80) lim=80;
k = 0;
 do {
	if(k==12){ printf("..index...%ld..",size); k=28; s +=16;}
	if((*s < 128) &&( *s >=32) ) 
		printf("%c",*s);
	else
		cout<< ".";
	s++;k++;
} while(k<lim);

printf(" thats all (count %d)\n",lim);
return 1;
}

int MEMCHECK(void*p) {
char buf[128];
//long  i;
long  size , bad=0;
char *s, *start;
char *ip;
long *iptr;
char* lp ;

#ifndef _WRAP 
	return 1;// this system isnt implemented in MSVC
#endif
if(BAD) printf("MCHECK on  suspicious pointer %p ",p);  
if(!p) { cout<< " thats a NULL"<<endl; return 1;}
s=(char*) p;
s-=32; start=s;
lp = s;


ip = (char*) p;
ip-=12;
iptr = (long*) ip;
size = *iptr; 

if(BAD) printf(" s is <%s>\n", s); 

if( (*lp !='C') && (*lp !='M')) bad=1;
else {
	lp++;
	if(*lp++ !='A') bad=1;

	s += 28;
	lp= s;
	if(*lp != 'A') bad=1;
	lp++;
	if(*lp != 'K') bad=1;
	lp++;
	if(*lp != 'M') bad=1;
	lp++;
	if(*lp != 'M') bad=1;

	ip = (char *)p;
	ip-=12;
	iptr = (long *) ip;
	size = *iptr;
	s = (char *)p ; s+=size;
	if(*s++ != 'M') bad=2;
	if(*s++ != 'K') bad=4;
	if(*s++ != 'A') bad=8;
	if(*s++ != 'A') bad=16;
}
if(!bad) return(1);
char *l_llp = buf;
sprintf(buf," CORRUPT MEMORY (%ld)",bad); g_World->OutputToClient(l_llp,2);
Print_One_Heap(p);

s=start;
memcpy(&(buf[0]), s,  24);
buf[48] = 0;
printf("(MEMCHECK) BAD  ALLOC '%s'\n",buf);
BAD=0 ; // nuisance
printf("current MD string is <%s>\n",MD); 

return 0;
}
long RENUMBER(void*p, long newindex) {
//char*s = p;
char *ip;
long *iptr,  oldindex ;

		ip = (char*) p;
		ip-=20;
		iptr = (long*) ip; 
		oldindex = *iptr; 
		memcpy(ip, &newindex,sizeof(long)); 

return newindex;
}

void __wrap_free(void *p) {
char*s = (char *)p;
char *ip;
long *iptr, size,Index;
	if(!p)  {
		g_World->OutputToClient("wrap_RXFREE called on NULL ptr ",2);
	}
	else{
		if(BAD) cout<< "RXFREE.. ";
		ip = (char*) p;
		ip-=12;
		iptr = (long*) ip;
		size = *iptr; 
		ip -=8;
		iptr = (long*) ip;
		Index = *iptr;
		if(Index < 0 || Index >=LIST_SIZE) {
			printf(" in RXFREE of %p index = %ld\n", p, Index);
			g_World->OutputToClient(" BAD INDEX",2);
		}
	
		s -=32;
 		MEMCHECK(p);
		memset(p,27,sizeof(char)*size); /* a silly value would be better than 0 */
	
		MEMLIST[Index]=NULL; lastref = Index;
		if(lastref < lowestfree) lowestfree=lastref;
		if(Index==MEMCOUNT-1) MEMCOUNT--;
		total_allocated = total_allocated -size;
	/* 	memset(s,0,32); */
		__real_free(s);
	}
}

void *__wrap_malloc(long int n)
{
char  *ptr, *s; //, *e;
long *ip,Index;

	if(n < 0) {g_World->OutputToClient(" malloc negative amount",2);  n=8;}
	total_allocated = total_allocated + n;

// this was a test to find a memory leak in find all strings
//	k  = ((n ==64) && (0==strncmp("AFTERPREP", MD,9)));	
//	assert (k==0);
	
	if(BAD) {
		printf(" MALLOC  of %ld total now %ld \n", n, total_allocated ); 
		printf("MD is <%s>\n", MD);
	}
	ptr= s = (char *)__real_malloc(n+48); /* 36 would do */
	Index = NEXTBLANKMEM();
	if(!ptr) 
		g_World->OutputToClient("MALLOC failed",3);
         		
	ptr+=32;
	memcpy(s,"MA",2);
	s+=2;		memcpy(s, MD,10);
	s +=10;
	ip = (long*) s;	 memcpy(ip,&Index, sizeof(long ));

	s=ptr; s-=12;
	ip = (long *) s;	memcpy(ip,&n, sizeof(long ));
 
	s+=8;		memcpy(s,"AKMM",4);
	s = ptr;
	s+=n;		memcpy(s,"MKAA",4);
	MEMLIST[Index] = ptr;
return(ptr);
}
void *__wrap_calloc(long int tot,long int size)
{
char buf[256];
char  *ptr, *s; //, *e;
 int no_of_bad;
 //int k;
 long allocated, in_use;
 long  *ip, n,totmod,nmod, Index;
	n = tot*size;
// this wwas a test to find a memory leak in PCT_Table
//	k  = ((n ==880) && (0==strncmp("leaveSetO", MD,9)));	
//	assert (k==0);


	for(totmod = tot + 42/size+1; totmod*size < ( n + 42);totmod++) {
		printf(" CALLOC  requested %ld  (mod to %ld)\n", n, totmod*size);
		printf(" original count is %ld current count %ld size is %ld\n", tot,totmod,size);
	}
	ptr =s =  (char *)__real_calloc(totmod,size); nmod = totmod*size;
	
	total_allocated = total_allocated + n;
	if(!ptr ) {	
		sprintf(buf,"Safe CALLOC failed. Request was %ld, (add 42->) %ld  Size=%ld ",tot,tot+42,size);
 		printf("%s\n",buf);
		printf("total allocated in RELAX is %ld\n", total_allocated);
#ifdef HOOPS
		HC_Show_Memory_Usage(&allocated,&in_use);
 		 printf("allocated Hoops memory = %ld, in use = %ld\n",allocated,in_use);
#endif	
		no_of_bad = Check_All_Heap();
	
		g_World->OutputToClient(buf,2);
		exit(1);
	}

	Index = NEXTBLANKMEM();
	ptr+=32;
	memcpy(s,"CA",2);
	s+=2;		memcpy(s, MD,10);

	s +=10;
	ip = (long*) s;	 memcpy(ip,&Index, sizeof(long ));


	s=ptr; s-=12;
	ip = (long*) s;	memcpy(ip,&n, sizeof(long));

	s+=8;		memcpy(s,"AKMM",4);
	s = ptr;
	s+=n;		memcpy(s,"MKAA",4);
	MEMLIST[Index] = ptr;
	return(ptr);
}


void *__wrap_realloc(void *dest, long size)
{
char *s,*ptr,*start;
char *ip; long *iptr;//, *indptr;
long   Index, oldsize = 0;

if(BAD) printf(" REALLOC requesting %ld ",size);  
if(!dest) {
if(BAD)	 cout<< " ON NULL\n"<<endl; 
	ptr = (char *)__wrap_malloc(size);
	return(ptr);
}

	if(!MEMCHECK(dest)) cout<< "that was in REALL OC"<<endl;


	ip= s = (char *) dest;

	 s-=32; ip-=12;
	start=s;
	iptr = (long*) ip;
	oldsize = *iptr ;

	s +=12;
	iptr = (long*) s;
	Index = *iptr;

	ptr = s = ip = (char *)__real_realloc(start,size+48);
	if(!s) 	g_World->OutputToClient("REALLOC failed",3);

	ptr+=32;   ip =  ptr;
	ip -=12; 
	iptr = (long*) ip;	memcpy(iptr,&size, sizeof(long));

	s = ptr;
	s +=size; 		memcpy(s,"MKAA",4);

	total_allocated = total_allocated + size - oldsize;

	MEMLIST[Index] = ptr;
	MEMCHECK(ptr);  

return(ptr);
}


long get_alloc_mem(void)
{
	return(total_allocated);
}
// end if Peters Memory Check

#elif  NO_MEMORY_CHECK

void __wrap_free(void *p) { free(p); }
void *__wrap_malloc(long int n) { return (malloc(n)); }

void *__wrap_calloc(long int tot,long int size) { return(calloc(tot,size));}

void *__wrap_realloc(void *dest,long int size) {  return( realloc(dest,size));}
long get_alloc_mem(void) { 	return(total_allocated); }


#elif  SIMPLE_MEMORY_CHECK

void __wrap_free(void *p)
{
	if(!p)  {   printf(" Malloc Data is <%s> ",MD);
		error("RXFREEcalled on NULL ptr ",2);
	/*	getchar(); */

	}
	else
		free(p);
}
void *__wrap_malloc(long int n)
{
void *ptr;
lin  allocated,in_use;
	if(n < 0) {error(" malloc negative amount",2);  n=8;}
	total_allocated = total_allocated + n;
ptr = malloc(n);
if(!ptr) {
	error("MALLOC failed",2);
               printf(" tried to MALLOC %ld\n",n);
	getchar();
}
return(ptr);
}
void *__wrap_calloc(long int tot,long int size)
{
void *ptr;
	total_allocated = total_allocated + tot*size;
ptr = calloc(tot,size);
if(!ptr ) {
	error("quick CALLOC failed",2);
 	HC_Show_Memory_Usage(&allocated,&in_use);
 	 printf("allocated Hoops memory = %d, in use = %d\n",allocated,in_use);

}
	return(ptr);
}
void *__wrap_realloc(void *dest,long int size)
{
void *ptr;
ptr = realloc(dest,size);

	total_allocated = total_allocated + size;
if(!ptr ) {
	error("REALLOC failed",2);
	getchar();

}
return(ptr);
}


long get_alloc_mem(void)
{
	return(total_allocated);
}
#elif COMPLICATED_MEMORY_CHECK
static  struct itnode *Mem_Tree_Root = NULL;

long get_alloc_mem(void)
{
return(total_allocated);
}

void RXFREE(void *ptr)
{
struct itnode *tn;
int key = (int) ptr;
mem_block_t *mb;
int n;
char *cptr;

if(!ptr) {
  error("RXFREE on NULL ptr",2);
  getchar();
  return;
}
tn = ifindnode(Mem_Tree_Root,key,4);
if(!tn) {
	error("called free on non-allocated memory !",2);
	getchar();
}
mb = (mem_block_t *) tn->data;
cptr = mb->ptr;
Mem_Tree_Root = ideltree(Mem_Tree_Root,key,NULL);
memcpy(&n,&(cptr[mb->size]),4);
if(n != mb->size) {
	error("sizes not equal in RXFREE",2);
}
total_allocated = total_allocated - mb->size;
free(mb);
free(ptr);
}



void *MALLOC(long int n)
{
struct itnode *tn;
int key;
mem_block_t *mb;
int i;
char *ptr;

ptr = calloc(n+4,sizeof(char));
if(!ptr) {
  error("MALLOC failed",2);
  getchar();
  return(NULL);
}
memcpy(&(ptr[n]),&n,4);
total_allocated = total_allocated +n;

mb = calloc(1,sizeof(mem_block_t));
mb->size = n;
mb->ptr = ptr;
key = (int) ptr;

Mem_Tree_Root = iaddtree(Mem_Tree_Root,key,(void *) mb);

return((void *) ptr);
}

void *CALLOC(long int tot,long int size)
{
return(MALLOC(tot*size));
}

void *REALLOC(void *dest,long int size)
{
char *ptr;
int oldn;
struct itnode *tn;
int key;
mem_block_t *mb;

if(size < 1) {
	cout<< "REALLOC with n < 1"<<endl;
	getchar();
}
ptr = CALLOC(size,sizeof(char));
if(!ptr) {
  error("REALLOC failed",2);
  printf("total mem alloc = %l\n",get_alloc_mem());
  getchar();
  return(NULL);
}

if(dest != NULL) {
key = (int) dest;
tn = ifindnode(Mem_Tree_Root,key,4);
if(!tn) {
	error("called realloc on non-allocated memory !",2);
	getchar();
}
mb = (mem_block_t *)tn->data;
oldn = mb->size;
if(size > oldn) 
	memcpy(ptr,dest,(oldn));
else
	memcpy(ptr,dest,(size));
RXFREE(dest);
}
return((void *) ptr);
}

#else
void *__wrap_calloc(long int tot,long int size)
{/* short version */

void *p = calloc(tot, size);
/*if(tot*size > 1000000) printf("CALLOC  %s %ld\n", MD, tot*size);
*/
if(!p && size != 0) {
  printf("Trying to Callocate %ld (%ld)\n",size,size);
  error("could not Callocate memory",2);
 
}
return(p);
}
void *__wrap_malloc(long int n)
{ /* short version */

       void *p ;

     	if(n < 0) {error(" malloc negative amount",2);  n=8;}
      	 p = malloc(n); 

	if(!p && n != 0) {


		long allocated, inUse;
 		 printf("Failed to Malloc  %ld \n Try Relinquish\n",n);
		HC_Show_Memory_Usage(&allocated, &inUse);
		printf(" BEFORE  allocated = %ld, in Use = %ld\n", allocated, inUse);
		HC_Relinquish_Memory();
 		printf(" AFTER    allocated = %ld, in Use = %ld\n Try again \n", allocated, inUse);
	
		p  = malloc(n);

		if(!p && n != 0) {
			  printf("Trying to Mallocate %ld (%ld)\n",n,n);
 			 error("could not Mallocate memory",2);
		}
	}
  return(p);
}
void *__wrap_REALLOC(void *dest,long int size)
{
	void *p;
	p  = realloc(dest,(size) );

	if(!p && size != 0) {
		long allocated, inUse;
 		 printf("Failedto REallocate %ld (%ld)\n Try Relinquish\n",size,size);
		HC_Show_Memory_Usage(&allocated, &inUse);
		printf(" BEFORE  allocated = %ld, in Use = %ld\n", allocated, inUse);
		HC_Relinquish_Memory();
 		printf(" AFTER    allocated = %ld, in Use = %ld\n Try again \n", allocated, inUse);
	
		p  = realloc(dest,(size) );

		if(!p && size != 0) {
			 printf("Failedto REallocate %ld (%ld)\n",size,size);
		 	 error("could not REallocate memory",2);
		}
	}
return(p);
}
void RXFREE(void *ptr) {
if(!ptr) {
  printf(" Malloc Data is <%s> ",MD);
  error("trying to free NULL ptr",2);
}
else
  free(ptr);
}

long get_alloc_mem(void)
{
	return(total_allocated);
}


int Check_All_Heap(int p_flag=0) {
return 1;
}
#endif

struct PHFILE {
	std::string name;
	std::string type;
	FILE *fp;
};

static struct PHFILE GlobalFileList[1024];
static int  FileCount=0;
int  RemoveFromFileList(FILE *stream){
	int i,count=0;
	char buf[128];
	for(i=0;i<FileCount;i++){
		if(GlobalFileList[i].fp==stream) {
			GlobalFileList[i].fp=NULL;
			GlobalFileList[i].name.clear();
			GlobalFileList[i].type.clear();
			count++;
		}
	}
	if(!count) {
		sprintf(buf," tried to close file %p when its not OPEN",stream);	
		g_World->OutputToClient(buf,2);
	}
	return 1;
}
int  Add_To_FileList(FILE *stream, char*n, const char*t){
	int i,ind;
	ind=FileCount;
	for(i=0;i<FileCount;i++){
		if(!(GlobalFileList[i].fp)) {
			ind=i; break;		
		}
	}
	if(ind==FileCount) {FileCount++; if(FileCount>=1024) g_World->OutputToClient("(PETER) more than 1024 files",3);}
	GlobalFileList[ind].fp=stream;
	GlobalFileList[ind].name=string(n);
	GlobalFileList[ind].type=string(t);
	return 1;
}

int  LookInFileList( char*n){
	int i;

	for(i=0;i<FileCount;i++){
		if(GlobalFileList[i].fp && (!strcmp(GlobalFileList[i].name.c_str(),n)) ){
			printf("Trying to OPEN %s (='%s') twice \n", n, GlobalFileList[i].name.c_str() );
		}
	}
	return 1;
}
#ifndef RXFOPEN
FILE *FOPEN_S ( const std::string &filename, const char *type)
{
    return RXFOPEN(filename.c_str(),type);
}

FILE *RXFOPEN ( const char *filenamein, const char *type){
    static FILE *inlist=NULL;
    FILE *fp;
    char filename[512];
    if( filenamein && (*filenamein)) { /* for debugging */
        filename_copy_local(filename,512,filenamein);
        fp = fopen( filename,type);
    }
    else {
        //	printf(" OPEN BLANK FILE!! '%s'\n",type);
        fp=NULL;
        return(fp); // sept 02
    }
    if(fp) {
        if(!inlist){
            char buf[256];
            sprintf(buf,"%s/readfiles.txt",g_RelaxRoot);
            inlist= fopen(buf,"w");
            if(!inlist) inlist = fopen("readfiles.txt","w");
            if(!inlist) {
                cout<< " you do not have permission to open readfiles.txt. crash."<<endl;
            }
            assert(inlist);
            if(!rxIsEmpty( g_CodeDir))
                fprintf(inlist,"export R3CODE=%s\n", g_CodeDir);
            if(!rxIsEmpty( g_RelaxRoot))
                fprintf(inlist,"export R3ROOT=%s\n", g_RelaxRoot);

            fprintf(inlist,"tar -cvPf thisjob.tar $R3CODE/makeexport\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/hardcopy\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/colours.dat\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/colours.HLS\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/cleanfile\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/before.mac\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/after.mac\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/RelaxII.defaults\n");
            fprintf(inlist,"tar -uvPf thisjob.tar $R3CODE/wind\n");
        }
        if(0!=strncmp(filename, "Rlx_",4) &&( 0!=strncmp(filename, "rxshape.",6) )  &&( 0!=strncmp(filename, "/tmp",4) ) ) {
            fprintf(inlist,"tar -uvPf thisjob.tar ");
            if(*filename != '/') {
                fprintf(inlist," %s",g_currentDir);
                if(g_currentDir[strlen(g_currentDir)-1]!='/')fprintf(inlist,"/");
            }
            fprintf(inlist,"%s\n",filename); 
            fflush(inlist);
        }
        //else printf(" SKIP write to archive of %s\n", filename);

        LookInFileList( filename);
        Add_To_FileList(fp,filename,type);

    }

    else	{			//sept 2002
        char buf[256];
        if(errno !=2) { // in linux you get 2 for trying to read a non-existant file
            sprintf(buf, "System refused to open %s %s\n errno = %d  %s",filename,type,errno,strerror(errno));
            g_World->OutputToClient(buf,2);
        }
    }
    return(fp);
}
#endif
#ifndef FCLOSE
int FCLOSE (FILE *stream){
	int iret;
	if(stream) RemoveFromFileList(stream);
	else {
		cout<< " FCLOSE on NULL"<<endl;
		return 0;
	}

	fflush(stream);
	iret = fclose(stream);
	if(iret) printf(" fclose on %p  returned %d\n",stream, iret);
	return(iret);
}

int List_C_Files(void){
	int i,count=0;
	for(i=0;i<FileCount;i++){
		if(GlobalFileList[i].fp) {
		count++;
		printf(" %d %p %s %s\n", i, GlobalFileList[i].fp,GlobalFileList[i].name.c_str(),GlobalFileList[i].type.c_str());
		}
	}
	printf(" %d C files of %d are open\n",count,FileCount);
	return count;
}
int Close_C_Files(void){
	int i,count=0;
	for(i=0;i<FileCount;i++){
		if(GlobalFileList[i].fp) {
		count++;
		printf(" CLOSE %d %p %s %s\n", i, GlobalFileList[i].fp,GlobalFileList[i].name.c_str(),GlobalFileList[i].type.c_str());
		FCLOSE( GlobalFileList[i].fp);
		}
	}
	printf(" %d C files were closed\n",count);
	return count;

}
	/*char  buf[1024]; sprintf(buf,"echo %s > scratchfile",in);
	system(buf);
	FILE *fp = fopen("scratchfile","r");
	if(fp) {
		if(fgets(out,p_length,fp)) {
			int k = strlen(out); if(k>0) out[k-1]=0;
			//printf("expanded <%s>\n to      <%s>\n", in,out);
			fclose(fp); 
			return 1;
		}
		else
			puts("cant read scratchfile");
	}
	else
		puts("cant open scratchfile"); 
*/
int filename_copy_local(char *out,int p_length,const char *in) {  
// by calling 'system echo (filename)' we expand out any environment variables. 
//THe trouble is that we also expand any wild-cards.
// so we have to do it word-by-word
#ifdef WIN32
#define SEPS "\\"
#else
#define SEPS "/"
#endif
int i;
	if ( ! strchr(in,'$') )
	{
		strncpy(out,in,p_length-2); return 0; 
	}
	int changed=0;
	vector<string> wds = rxparsestring(in,SEPS);
	for( i=0; i<wds.size ();i++) {
		string &w = wds[i];
		if(w.find('$') ==string::npos) 
			continue;
		char*lp = getenv(w.c_str());
		if(lp) { 
			cout<<" word `"<<w<<"`";
			w=lp;changed++;
			cout<<" becomes "<<w<<endl;
		}
	}
	string sout;
	if(changed) {
		for(i=0;i<wds.size(); i++) {
			sout+=wds[i];
			if(i !=wds.size()-1) sout +=SEPS;
		}
		cout<<"filename    `"<<in<<"`"<<endl;
		cout<<"expanded to `"<<sout.c_str()<<"`"<<endl;

		strncpy(out,sout.c_str(),p_length-2); 
		return changed; 
	}
	char *a = STRDUP(in);

	 Replace_String(&a,"$R2ROOT",g_RelaxRoot);
	 Replace_String(&a,"$R2CODE", g_CodeDir);
	 Replace_String(&a,"$R3ROOT",g_RelaxRoot);
	 Replace_String(&a,"$R3CODE", g_CodeDir);
	 Replace_String(&a,"$RWD", g_workingDir);

	 out[p_length-1]=0;
	strncpy(out,a,p_length-2);
	RXFREE(a);
return 1;

}//int filename_copy_local

#endif
#ifndef rxerror
#ifdef WIN32
		int rxerror(const char*a, int b) {
				if(b>1) return AfxMessageBox(CString(a));
                                return printf("msg code(debug.cpp)=%d '%s'\n",b,a);
		}
			int rxerror(ON_String a, int b) {
				return rxerror(a.Array(),b);
		}
#endif
		int rxerror(const RXSTRING &a, const int b) {
			std::string c = ToUtf8(a);
			return rxerror(c.c_str(),b);
		}
#endif
 


