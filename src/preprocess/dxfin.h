/* header file for dxfin function JIB 1995 */


#ifndef DXFIN_15NOV04
#define DXFIN_15NOV04

#include "vectors.h"
#include "nurbs.h"


#define DXF_FALSE 0
#define DXF_TRUE 1

/* section definitions for file pointer position */
#define DXF_HEADER 1
#define DXF_TABLES 2
#define DXF_LTYPE 3
#define DXF_LAYER 4
#define DXF_STYLE 5
#define DXF_VIEW 6
#define DXF_UCS 7
#define DXF_VPORT 8
#define DXF_DIMSTYLE 9
#define DXF_APPID 10
#define DXF_BLOCKS 11
#define DXF_ENTITIES 12

#define DXF_MINSTR 32
#define DXF_MAXSTR 255
#define DXF_MAXVERTEX 1000
struct s_point{float x; float y; float z;};
struct s_lstpts{struct s_point p; struct  lstpts *next;};
struct s_entdat{int group; char data[32];};

struct dxfentity1 {
char layer[DXF_MINSTR];char color[DXF_MINSTR]; char linetype[DXF_MINSTR];
 int flag; float elevation; float thickness; 
struct s_point normal; struct s_point p1; struct  s_point p2; struct  s_point p3; struct  s_point p4;
char name[DXF_MINSTR]; char value[DXF_MINSTR]; 
float g40; float g41; float g42; float g43; float g44; float g45; float g50; float  g51;
};

struct s_dxfgroup{char value[DXF_MINSTR];};
struct s_dxfgroup_long{char value[DXF_MAXSTR];};

struct s_dxfentity{
	struct s_dxfgroup group[240]; 
	struct s_dxfgroup plane[40]; 
	struct s_dxfgroup_long xtend[80]; 
	char comment[DXF_MAXSTR];
	SAIL *m_sail;
};

struct s_line {struct s_point p1; struct s_point p2; char layer[DXF_MINSTR]; int color;};
struct s_polygone {int nbpts; struct s_point p[DXF_MAXVERTEX]; char layer[DXF_MINSTR]; int color;};

struct layer {char name[32]; int color;int linetype; int visible; int thaw;int viewport;struct layer *next;};
struct header {char name[32]; int value;struct header *next;};


HC_KEY dxfin(SAIL *sail, const char *filename);
void dxfinit(void);// char*fname);
void dxfclear(struct s_dxfentity *what);
void getdxfheader(void);
void getdxflayers(void);
void getdxfblocks(SAIL *sail);
void getdxfentities(SAIL *sail);
//void setattributes(void);


int findsection(const char *sectionname);
int findtable(char *sectionname);

void dxftrace(struct s_dxfentity *dxfentity);

void dxferror(const char *stmp);

int checkdxffile(const char  *filename);
/* return 1 if it looks like a dxf file, else return 0 */

void dxfopensegment();
char *Create_DXF_Attributes(struct s_dxfentity *ent);
int getdxf_one_entity(struct s_dxfentity *dxfentity,int group);
void getdxfentity();
int creategeom(struct s_dxfentity *dxfentity);
void newline(struct s_dxfentity *dxfentity);
void newpoint(struct s_dxfentity *dxfentity);
void new3dface(struct s_dxfentity *dxfentity);
void newcircle(struct s_dxfentity *dxfentity);
void newarc(struct s_dxfentity *dxfentity);
void newinsert();
int newpolyline(struct s_dxfentity *dxfentity);
void newvertex(struct s_dxfentity *dxfentity);

int dxforientation(struct s_dxfentity *dxfent);
void newinsert(struct s_dxfentity *dxfentity);
int newdxfspline(struct s_dxfentity *dxfentity) ;
int dxf_place_group(struct s_dxfentity *dxfentity,int group,char *buf);
int dxf_read_group(struct s_dxfentity *dxfentity,int group);

int Create_Nurbs_From_Dxf(struct s_dxfentity *ent,int vc, VECTOR  *p);

struct TwinEntity{
	RXEntity_p e1,e2;
};

EXTERN_C struct TwinEntity Generate_Curve_From_Graphic(SAIL *sail,const char*atts,const char*handle,char*layer,const char *name, int c, VECTOR  *p, void*p_ono=0);

int Generate_NURBS_From_Dxf_Spline(struct s_dxfentity *ent,double *x,double *y,double *z,double *k,int ncp,int nkts,int order,struct PC_NURB *n);


#endif //#ifndef DXFIN_15NOV04





