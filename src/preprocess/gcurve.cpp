/* Relax  source code
 * Copyright Peter Heppel Associates 1995
 *
 * written by 	A K Molyneaux
######################################################

To do a form-find:
Gauss_All_Panels  collects the curvature onto the sc's gauss_pts
Make_Gauss_Curves (using make_gauss_curve) then creates the gauss BC

######################################################
December 2000.  Modified PH to enable extraction of fanned edges.  Possibly upsets 
formfinding by G Angle survey.

14 12 00.  It works, but doesnt seem very accurate. 
 Possible errors
A)  G angles correctly indexed on extraction
B)	G_Angles accurate	- if not there's little we can do
C)	skipping some g angles we should keep.
D)	g angles are in the wrong order.
E)	Integration (compute_gauss curve)  in rxerror.  

Testing has to be done in Unix because the Windows version used false get g angle
Tests  A) B).  Get a print of the G and S angles.  
Get a print of the slist (


######################################################
 11/3/97 time.h in HPUX
 3/7/96 a problem  addOneGaussPt_oncurve uses the offsets at the ends of the psides
 and then the ds values of the sites in its slist, to determine the <posn> values in the gauss list

This is wrong. The <posn> values should be calculated from the current edge lengths as resulting
from prestress analysis.

A partial work-around is to modify the Relsite offsets when we create the relsite edits
(in reledit.c) 
Doing a re-panel after making relsite edits would have almost the same effect
except that we'd lose the gvalues on the new sites due to topology changes



 *  25/5/96 gausscurve edits are multi-line. The new reader in entitles.c looks for balanced brackets
   or an EOR line.
 *   7/3/96  definitions of Fieldrec moved to gcurve.h
 *   so that it may contain a complete prototype list

 *	19/12/95	ifdefs for MSVC compatibility
 *	8/12/95 WARNING. (p,c) are NOW output values from Compute_Gausscurve
      the solid angles should be put in (pin,cin)

 *	4/12/95 PH added includ entities.h
 *
 *
 */

/* This contains routines to add all of the 
   gaussian curvature within a panel to the
   seamcurves. It essentially does the following :
   
   For each panel :
   For each internal point X in the panel :
   Find the distance to each edge and the nearest point P on the edge
   choose the nearest edge
   add a point at the appropriate distance along the base seamcurve corresponding to point P
   give this point P a value corresponding to the curvature at X
   give this point P the ID of the node at X so it can find it again
   Next Point
   Next Panel
   
   
   */

/* basic ideas :
   Generate a GaussCurve card
   Generate an Edit card to install it in the seamcurve
   */


#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXPolyline.h"
#include "RXOffset.h"
#include "RXQuantity.h"
#include "RXSeamcurve.h"
#include "RXPside.h"
#include "global_declarations.h"

#include "griddefs.h"
#include "rxbcurve.h"
#include "RXCurve.h"

#define GCDB (0)
#include "printall.h"
#include "stringutils.h"
#include "panel.h"
#include "polyline.h"
#include "arclngth.h"

#include "gcurve.h"
#include "polyline.h"
#include "reledit.h"
#include "script.h"
#include "scedit.h"
#include "drawing.h"
#include "f90_to_c.h"


#include "RXPanel.h"

#include "entities.h"
#include "hoopcall.h"
#include "akmutil.h"
#include "resolve.h"

#include "angcheck.h"
#include "words.h"
#include "etypes.h" 

#include "delete.h"
#include <time.h>
int dbg=0;
#include "edit.h"
#ifdef _X
#include  <OpenMat.h>
#endif
#ifdef WIN32
#define M_PI  3.141592654
#include <time.h>



//  get_svalue_ for the undeformed angle
void get_svalue_(int*a,int *b,float*c ,int *err){
    *c= (float) (M_PI * 0.99);
    cout<< "dummy get_svalue_"<<endl;
    *err=0;
}
void get_gvalue_(int*a,int *b,float*c ,int *err){
    *c= (float) (M_PI * 0.98);
    cout<< "dummy get_gvalue_"<<endl;
    *err=0;
}
#endif


float G_Factor = 1.0;		/* factor to apply to gvalues */


typedef enum {create=0,pairs,date} GaussCurveFields;

static  FieldRec  GCtable[]  = {
    {"create", create, 0, "yes"},
    {"pairs", pairs, 0,NULL},
    {"date", date, 0,NULL},
    {NULL,-1,-1,NULL}
};


FieldRec *check_table(FieldRec *t,char *s)
{
    while(t->string) {
        if(stristr(s,t->string)) {
            return(t);
        }
        t++;
    }
    return(NULL);
}


FieldRec *NewTable(FieldRec *fr)
{
    FieldRec *r,*t;
    int i=1;			/* note there is always a NULL at least */

    t = fr;
    if(t) {
        while(t->string) {
            t++;i++;
        }
    }

    r = (FieldRec *)CALLOC(i,sizeof(FieldRec));
    if(fr)
        memcpy(r,fr,sizeof(FieldRec)*i);
    return(r);
}


int Print_SC_GaussPoints(FILE *fp, sc_ptr np){

    linklist_t *ll = (LINKLIST *)np->gauss_pts;
    struct GAUSS_PT *gp;
    RX_FESite *s;
    SAIL *sail = np->Esail;

    fprintf(fp,"Gauss Points at :\n");
    fprintf(fp,"node\tx\ty\tz\tdist\tposn\tvalue\tadjlen\n");
    while(ll) {
        gp = (GAUSS_PT *)ll->data;
        if(gp->n>=0){// && gp->n<sail->GNsize) {
            s = sail->GetSiteByNumber( gp->n);
            fprintf(fp,"%d\t%f\t%f\t%f\t%f\t%f\t%f\n",
                    s->GetN(),s->x,s->y,s->z,gp->dist,gp->posn,gp->val);
        }
        else	{
            fprintf(fp, "node %d out of range\n", gp->n);
            printf("node %d out of range\n", gp->n);
        }

        ll = ll->next;
    }
    return 1;
}
int parse_attribute_items(const RXEntity_p e,char *part,FieldRec *table)
{
    int i=0,n;
    char **data,**dt;
    char *a,*v;
    FieldRec *f;

    n  = make_into_words(part,&data,";");
    dt = data;

    if(n > 0) {
        while(i<n) {
            a = strtok((*dt),"=");
            v = strtok(NULL,"=");
            if(!(f=check_table(table,(*dt)))) {
                char buf[256];
                sprintf(buf,"The resource '%s' is not supported by this edit",(*dt));
                e->OutputToClient (buf,2);
            }
            else {
                if(!v) {
                    f->value = STRDUP("yes");   /* default is always yes */
                    f->set = 1;
                }
                else {
                    f->value = STRDUP(v);
                    unbracket(f->value,":");
                    f->set = 1;
                }
            }
            i++;dt++;
        }
    }
    return(n);
}

int Draw_Edge_Curves(char*name, SAIL*sail) {


    RXEntity_p ge ;
    RXGaussCurve *ce;
    sc_ptr sc;
    float tempchord, xx,xn,yx,Yn,zx,zn;
    HC_KEY hoopskey =0;


    HC_Open_Segment("?Include Library/fanned");
    HC_Open_Segment( sail->GetType().c_str());
    G_Factor = 1.0;
    Gauss_Seamcurves_Only(PCG_DEFLECTED,name ,sail);

    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd();it=sail->MapIncrement(it)) {
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if(name && !strieq(p->name(), name))
            continue;
        if (strieq(p->type(),"seamcurve") ){
            sc = (sc_ptr  )p;
            if(sc->edge){
                hoopskey=HC_KOpen_Segment(p->name());
                HC_Open_Segment("loaded");
                HC_Flush_Contents(".","everything");
                HC_Set_Color("red");
                HC_Insert_Text (0.0, 0.0, 0.0, "Flying is red");

                ge = make_gauss_curve(p); // returns the edit entity.

                ce=NULL;
                assert(0);//	if(ge && (1==Resolve_Edit_Card(ge,&ce)))

                {  // should create an edit
                    ce->Resolve();
                    //		gc = (PC _BASE_CURVE *)ce->dataptr;
                    tempchord= (float) (sc->GetArc(0));
                    sail->m_Linear_Tol = sail->m_Linear_Tol/100.0;
                    ce-> Compute_GaussCurve(tempchord,0,sail->m_Linear_Tol);
                    sail->m_Linear_Tol = sail->m_Linear_Tol*100.0;
                    PolyPrint("gausscurve DEFLECTED OUT \n",ce->p,ce->c);
                    RX_Insert_Polyline(ce->c,(float*) ce->p,sc->GNode(),0);
                }

                Free_Gcurve_List(sc);

                if(ce) ce->Kill();
                if(ge) ge->Kill();
                HC_Close_Segment();
                HC_Close_Segment();

            }
        }
    }

    if(1) {
        // now repeat with PCG_UNDEFLECTED
        Gauss_Seamcurves_Only(PCG_UNDEFLECTED,name,sail );
        ent_citer iit;
        for (iit = sail->MapStart(); iit != sail->MapEnd();iit=sail->MapIncrement(iit))  {
            RXEntity_p p  = dynamic_cast<RXEntity_p>( iit->second);
            if(name && !strieq(p->name(), name))
                continue;
            if (strieq(p->type(),"seamcurve") ){
                sc = (sc_ptr  )p;

                if(sc->edge){

                    HC_Open_Segment(p->name());
                    HC_Open_Segment("unloaded");
                    HC_Flush_Contents(".","everything");
                    HC_Set_Color("green");
                    HC_Insert_Text (0.5, 0.01, 0.0, "Moulded is green");
                    ge = make_gauss_curve(p); // returns the edit entity.
                    ce=NULL;
                    assert(0);//          if(ge && (1==Resolve_Edit_Card(ge,&ce)))
                    {  // should create an edit
                        ce->Resolve();
                        //gc = (PC _BASE_CURVE *)ce->dataptr;
                        tempchord= (float) (sc->GetArc(0));
                        sail->m_Linear_Tol = sail->m_Linear_Tol/100.0;
                        ce-> Compute_GaussCurve(tempchord,0,sail->m_Linear_Tol);
                        sail->m_Linear_Tol = sail->m_Linear_Tol*100.0;
                        RX_Insert_Polyline(ce->c,(float*)ce->p,sc->GNode(),0);
                        PolyPrint("gausscurve UNDEFLECTED IN \n",ce->p,ce->c);
                    }
                    Free_Gcurve_List(sc);
                    if(ce) 	ce->Kill();
                    if(ge)  ge->Kill();
                    HC_Close_Segment();
#ifdef HOOPS
                    PC_Box_Size("*","polylines","object",&xx,&xn,&yx,&Yn,&zx,&zn);
#endif
                    HC_Close_Segment();

                }
            }
        }
    }
    HC_Close_Segment();
    HC_Close_Segment();
#ifdef _X
    if(hoopskey) {
        Graphic *g ;
        char buf[256];
        xn = xn*1.2 - xx * 0.2 ;
        xx = xx*1.1 - xn * 0.1 ;

        Yn = Yn*2.0 - yx * 1.0 ;
        yx = yx*1.5 - Yn * .5  ;
        xn = xn -.01;
        xx = xx +.01;
        Yn = Yn -.01;
        yx = yx +.01;
        Draw_Ruler(0.0,0.0,0.0, 2, Yn,yx);
        printf( "camera xmin xmax ymin ymax %f %f %f %f\n", xn,xx,Yn,yx);
        sprintf(buf,"Fanned %s (%s)",name,sail->GetType().c_str()  );
        g = OpenMatWindow(buf);
        HC_Open_Segment_By_Key(g->m_ViewSeg);
        HC_Include_Segment_By_Key(hoopskey);
        HC_Set_Camera_By_Volume("stretched",xn,xx,Yn,yx);
        HC_Close_Segment();
        Do_Resize(NULL,g);
    }
#endif

    return 1;

}

int Make_Gauss_Curves( SAIL *sail) {

    vector<RXEntity* >thelist;
    sail->MapExtractList(SEAMCURVE ,  thelist) ;
    vector<RXEntity* >::iterator it;
    for (it = thelist.begin(); it != thelist.end(); ++it) {
        RXEntity_p p  = *it;
        make_gauss_curve(p); // returns the edit entity.
    }
    return 1;
}

/* to create ...
   gausscurve : name : x ,y,x,y,.....
  need something like
    editword: <model> :gausscurve :<name> :2:x:y:x:y,....

*/
RXEntity_p make_gauss_curve(RXEntity_p e)
{
    sc_ptr sc = (sc_ptr  )e;
    RXEntity_p gee; // Ge,

    printf("Making gauss curves on '%s' \n",e->name());
    Print_SC_GaussPoints(stdout, sc);
    if(sc->gauss_pts) {
        linklist_t *ll = (LINKLIST *)sc->gauss_pts;
        double oldposn =-999.;
        struct GAUSS_PT *gp;
        QString buf, gcname,ewname ,dpair ;
        int depth ;
        char buf2[256],  *lp;
        struct tm *newtime;
        RXEntity* peold;
        time_t aclock;

        ewname = QString( "ewgc_") +QString(sc->name());
        gcname =QString( "gcpts_") +QString(sc->name());

        time( &aclock );                 /* Get time in seconds */
        newtime = localtime( &aclock );  /* Convert time to struct tm form  */
        // sprintf(buf,"edit:GaussCurve#%sPTS:%s:create=yes;pairs=(",e->name(),e->name());

        buf = QString("editword:%1 : gausscurve : " ).arg(ewname)  + gcname;
        buf += " : 2 ";
        while(ll) {
            gp = (GAUSS_PT *)ll->data;
            dpair = QString("\n %1 : %2 ").arg( gp->posn, 12,'f'  ).arg( gp->val,0,'f',9);
            buf+=dpair;
            ll = ll->next;
        }
        depth = 0;			/* want to keep it in the script file */

        // strcat(buf,")");    /* end of the data */
        sprintf(buf2,"!; date=%s ",asctime(newtime)); /* The time of the edit */
        for(lp=buf2; *lp; lp++) {if(*lp=='\n') *lp=' ';    if(*lp==':') *lp='.'; }
        buf+=buf2 ;
       // sprintf(buf2,"GaussCurve#%sPTS",e->name());
        if((gee=e->Esail->GetKeyWithAlias(  "editword",qPrintable(ewname))))
            gee->Kill( );

        gee = Just_Read_Card(e->Esail , qPrintable(buf),"Editword",qPrintable(ewname),&depth);

        // now tell the SC the name of its new GC (word 4)

        ewname.append("_SC_" );

        //  sprintf(buf,"edit:SeamCurve#%sGC:%s:basecurve=%s",e->name(),e->name(),e->name());

        buf = QString("editword:%1 : seamcurve : " ).arg(ewname) +QString(sc->name()) + " : 4 : " + gcname;

        if((peold=e->Esail->GetKeyWithAlias("editword",qPrintable(ewname))))
            peold->Kill();

        Just_Read_Card(e->Esail ,qPrintable(buf),"editword",qPrintable(ewname),&depth);

    }
    else {
        cout<< "No gausspts\n"<<endl;
        gee=NULL;
    }
    return(gee);
}

/* to create ...
   gausscurve : name : x ,y,x,y,.....
  need something like
    editword: <model> :gausscurve :<name> :2:x:y:x:y,....

*/
RXEntity_p make_gauss_curve_adam(RXEntity_p e)
{

    sc_ptr sc = (sc_ptr  )e;
    RXEntity_p gee; // Ge,

    printf("Making gauss curves on '%s' \n",e->name());
    Print_SC_GaussPoints(stdout, sc);
    if(sc->gauss_pts) {
        linklist_t *ll = (LINKLIST *)sc->gauss_pts;
        double oldposn =-999.;
        struct GAUSS_PT *gp;
        char *buf;
        char *t;
        char colon[4]=":";
        char eoln[4] = "\n";
        int depth,count=0;
        char buf2[256],dpair[256], *lp;
        struct tm *newtime;
        RXEntity* peold;
        time_t aclock;

        buf = (char *)MALLOC(32100*sizeof(char));

        time( &aclock );                 /* Get time in seconds */
        newtime = localtime( &aclock );  /* Convert time to struct tm form  */
        sprintf(buf,"edit:GaussCurve#%sPTS:%s:create=yes;pairs=(",e->name(),e->name());
        while(ll) {
            if(count) t=colon;
            else t = eoln;
            gp = (GAUSS_PT *)ll->data;
            sprintf(dpair,"%12.7g:%12.7g %s",gp->posn,gp->val,t);
            if(gp->posn != oldposn){
                strcat(buf,dpair);
                oldposn =gp->posn ;
                printf("cat pair '%s'\n", dpair);
            }
            else {
                printf("skipped gpt '%s' at %f\n", dpair, gp->posn );
            }
            ll = ll->next;
            count++;
            if(count >5) count=0;
        }
        depth = 0;			/* want to keep it in the script file */

        strcat(buf,")");    /* end of the data */
        sprintf(buf2,"; date=%s ",asctime(newtime)); /* The time of the edit */
        for(lp=buf2; *lp; lp++) {if(*lp=='\n') *lp=' ';    if(*lp==':') *lp='.'; }
        strcat(buf,buf2);
        sprintf(buf2,"GaussCurve#%sPTS",e->name());
        if((peold=e->Esail->GetKeyWithAlias(  "edit",buf2)))
            peold->Kill( );

        gee = Just_Read_Card(e->Esail , buf,"Edit",buf2,&depth);

        // now tell the SC the name of its new GC

        sprintf(buf,"edit:SeamCurve#%sGC:%s:basecurve=%s",e->name(),e->name(),e->name());
        sprintf(buf2,"SeamCurve#%sGC",e->name());
        if((peold=e->Esail->GetKeyWithAlias("edit",buf2)))
            peold->Kill();

        Just_Read_Card(e->Esail ,buf,"Edit",buf2,&depth);

        RXFREE(buf);
    }
    else {
        cout<< "No gausspts\n"<<endl;
        gee=NULL;
    }
    return(gee);
}





int Edit_Gauss_Curve_Card(RXEntity_p e, RXEntity_p *ge)
{
    /* takes a card of the form
     edit : type : name :create=yes | no, data = (x y x y .....)
     note spaces separate data pairs and commas separate items
     and replaces ALL the data
     */
    int cnt;
    char *lcopy,*lcopy2;
    char **data;
    class RXGaussCurve* gc;

    char *name;
    int retVal = 0;
    VECTOR *p;
    FieldRec  *Rtable,*t;
    char *buf;

    Rtable = NewTable(GCtable);
    buf = (char *)MALLOC(strlen(e->GetLine()) + 100);
    lcopy = STRDUP(e->GetLine());
    /* lcopy may have intermediate eols and we should replace them with spaces */
    EOL_To_Blank(lcopy);
    /* strclend(lcopy); clean up the end of the line  Peter Removed. This. eols must be accepted in the line
        and trailing comments have already been stripped*/

    cnt = make_into_words(lcopy,&data,":\t ");
    if(cnt < 4) {
        sprintf(buf,"There is not enough data in the "
                "Edit Gausscurve Card.\nMust have one "
                "point and the form:\nGaussCurve <tab> "
                "name <tab> distance <tab> value <tab> "
                "distance <tab> ...etc");
        e->OutputToClient   (buf,2);
    }

    name = data[2];
    lcopy2 = STRDUP(e->GetLine());
    EOL_To_Blank(lcopy2);

    strtok(lcopy2,":\t ");
    strtok(NULL,":\t ");
    data[3] = strtok(NULL,":\t ");

    data[3] = data[3] + strlen(data[3]) + 1;

    parse_attribute_items(e,data[3],Rtable);
    RXFREE(data);
    *ge =e->Esail->Entity_Exists( name,"GaussCurve");
    if((*ge) && (*ge)->Needs_Resolving) {
        retVal = 0;
        goto End;
    }
    if(!(*ge)) {
        if(strieq(Rtable[create].value,"yes")) {
            int depth;

            if(!Rtable[pairs].set) {
                e->OutputToClient ("Missing data to create a GaussCurve",2);
                retVal = -2;
                goto End;
            }

            sprintf(buf,"GaussCurve:%s:%s",name,Rtable[pairs].value);
            depth = 1;
            *ge = Just_Read_Card(e->Esail ,buf,"GaussCurve",name,&depth);

            retVal = 1;
            for(t = Rtable;t->string;t++) {
                if(t->set) {
                    RXFREE(t->value);
                }
            }
            goto End;
        }
        else {
            e->OutputToClient ("You asked to edit a non-existant GaussCurve without the create=yes option",1);
            retVal = -1;
            goto End;
        }
    }
    gc = (class RXGaussCurve*)(*ge) ;
    t = Rtable;
    while(t->string) {
        if(t->set) {
            switch (t->field) {
            case create:
                break;
            case date:
                break;

            case pairs:
                /* parse the data here */
                //cnt = extract_vector_pairs(t->value,&p,&cnt,":");
                assert("TODO: gcurve"==0);
                //if(gc->pin)
                //  RXFREE(gc->pin);
                //gc->pin = p;
                //gc->cin = cnt;
                retVal = 1;
                break;

            default:
                e->OutputToClient ("unknown resource in GaussCurve Edit Field\n",2);
                /* should never get here */
                break;
            }
            RXFREE(t->value);  /* only free if set flag is true hence it was strdup'd */
        }
        t++;
    }
End:
    RXFREE(buf);
    RXFREE(lcopy);
    RXFREE(lcopy2);
    RXFREE(Rtable);
    return(retVal);
}

int  Gauss_Seamcurves_Only(int what,char*name, SAIL *sail) {

    int fmodel=-1,flag=0;
    ent_citer it;
    cout<< "Gauss_Seamcurves_Only is retured ( dont know why) "<<endl; return 0;
    /*
   double total;
 calc_s_angles_ (&fmodel,&flag, &total);  // overkill to do them both.  So What.
 flag = 0; 				// just to be sure
 calc_g_angles_ (&fmodel,&flag,  &total);
*/


    //  for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it))  {
    //RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);

    //switch (p->TYPE) {
    //	case COMPOUND_CURVE:
    //	case SEAMCURVE :
    //		{ assert(0);
    //	  Gauss_One_SeamCurve(sail,p,what );
    //	  break;
    //	}
    //}
    // }
    //return 1;
}


int Gauss_All_Panels(int what,SAIL *sail) {

    Panelptr  np;
    int flag=1; double total=0;

    cout<< " Gauss_All_Panels "<<endl;

    cf_calc_s_angles (sail->SailListIndex(),flag, &total);  // overkill to do them both.  So What.
    cf_calc_g_angles (sail->SailListIndex(),flag, &total);

    vector<RXEntity* >::iterator it;
    vector<RXEntity* >thelist;
    sail->MapExtractList(PANEL ,  thelist) ;

    for (it = thelist.begin(); it != thelist.end(); ++it) {
        RXEntity_p p  = *it;
        np =(Panelptr) p;
        np->Gauss_One_Panel(what);
    }

    thelist.clear();
    sail->MapExtractList(SEAMCURVE ,  thelist) ;        sail->MapExtractList(COMPOUND_CURVE,  thelist) ;

    for (it = thelist.begin(); it != thelist.end(); ++it) {
        RXEntity_p p  = *it;
        Gauss_One_SeamCurve(sail,p,what );
    }
    return 1;
}

int RXPanel::Gauss_One_Panel(const int what)
{
    int nodeno;
    int i, retVal = 0;
    OutputToClient(" TODO :Panel Gaussing ",1);
    for (i = 0; i < this->m_t->numberofpoints; i++) {
        nodeno = this->m_t->pointmarkerlist[i];
        RX_FESite*s = this->Esail->GetSiteByNumber(nodeno);
        addOneGaussPt(s,this->m_pslist,this->m_psidecount,this->reversed,BLACKCURVE,what);
        retVal++;
    }
    return(retVal);
}

static int  oldN= -1;
int Gauss_One_SeamCurve(SAIL *sail,RXEntity_p e,int what)
{
    int i;

    int retVal = 0;
    Pside *ps;
    sc_ptr sc;

    oldN=-1;
    if(!sail) {
        e->OutputToClient ("no sail in gauss on panel",2);
        return(0);
    }

    sc = (sc_ptr  )e;
    assert(sc);
    if(!sc) return 0;
    //  dbg = strieq(e->name(),"luff");
    if(dbg) {
        printf("Gauss_One_SeamCurve %s\n", e->name());
        Print_SC_GaussPoints(stdout, sc);
    }
    for(i=0;i<sc->npss;i++) {
        ps = sc->pslist[i];
        addOneGaussPt_oncurve(ps,sc,BLACKCURVE,what);
        retVal++;
    }
    if(dbg) { cout<< "\n on leaving\n"<<endl;
        Print_SC_GaussPoints(stdout, sc);
        printf("LEAVE Gauss_One_SeamCurve %s\n", e->name());
        dbg=0;
    }
    return(retVal);
}

int addOneGaussPt_oncurve(Pside *ps,sc_ptr sc,int space,int what)
{
    static int rev[2][3] = {{0,1,2},{2,1,0}};
    double end,start,L;
    int wh;
    int i;
    RX_FESite *pt=NULL;
    struct GAUSS_PT *gp=NULL;
    linklist_t *ll=NULL;
    int retVal=0;
    int err=0;
    if(dbg) printf(" addOneGaussPt_oncurve %s\n", ps->name());

    wh = rev[0][space];
    /* for each point on the pside */
    end = (ps->End2off->Evaluate(sc,wh)); /* end of the pside */
    start = (ps->End1off->Evaluate(sc,wh)); /* end of the pside */

    RXQuantity*length = dynamic_cast<RXQuantity*>(sc->FindExpression(L"length",rxexpLocal));
    if(length)
    {if (length->evaluate() < 0.001)  length->Change(TOSTRING(sc->m_arcs[wh] ));}

    //	  PCQ_Set_Quantity_By_Value(&sc->length, sc->m_arcs[wh]);

    int sli = sc->Esail->SailListIndex();

    L = end-start;
    for(i=0;i<ps->m_sList.size();i++) {
        pt = dynamic_cast<RX_FESite *>(ps->m_sList[i].first);
        if(pt->GetN() != oldN) {
            ll = (LINKLIST *)CALLOC(1,sizeof(linklist_t));
            gp = (GAUSS_PT *)CALLOC(1,sizeof(struct GAUSS_PT));
            /*oldN =  WRONG ?? */ gp->n = pt->GetN();
            err=0;
            if(dbg) printf("\t%d ", gp->n);
            switch(what) {
            case PCG_DEFLECTED :
                cf_get_gvalue(sli,gp->n,&(gp->val),&err);
                break;
            case PCG_UNDEFLECTED :
                cf_get_svalue(sli,gp->n,&(gp->val),&err);// the undeformed angle
                break;
            default:
                assert(0);
            }
            if(err == 0 || err ==2 ) {
                if(GCDB) {
                    if(fabs(gp->val) <= 1e-1) {
                        printf("node %d has gp value = %f\n",gp->n,gp->val);
                    }
                }

                if(sc->edge) {
                    if(ps->leftpanel[0]) {
                        if(ps->leftpanel[1]) {
                            ps->OutputToClient("This edge seems to have panels onboth sides !",2);
                        }
                        gp->val = -G_Factor*(M_PI - gp->val); /* edge with panels on left */
                        if(GCDB) printf("considering point %d which has gval = %f(Panel on LEFT) err=%d\n",gp->n,gp->val,err);
                    }
                    else {
                        gp->val = G_Factor*(M_PI - gp->val); /* edge with panels on right */
                        if(GCDB) printf("considering point %d which has gval = %f(Panel on RIGHT) err=%d\n",gp->n,gp->val,err);
                    }
                }
                else { //not an edge
                    gp->val = M_PI + M_PI - gp->val;
                    gp->val = gp->val * G_Factor;
                    if(GCDB) printf("considering point %d which has gval = %f(INSIDE Seam) err = %d\n",gp->n,gp->val,err);
                }
                if(err != 0)
                    gp->val = 0.0;
                cout<<" addOneGaussPt are you sure DS is current?"<<endl;
                gp->posn = (start + (pt->getDs() * L))/sc->m_arcs[(wh)];//   arcs[wh]); /* length->evaluate();*/
                gp->dist = 0.0;
                ll->data = gp;

                if(sc->gauss_pts == NULL) {
                    sc->gauss_pts = ll;   /* first one */
                }
                else {
                    linklist_t *l2 = (LINKLIST *)sc->gauss_pts;
                    struct GAUSS_PT *gp2 = (GAUSS_PT *)l2->data;
                    int DONE=0;
                    if(gp->posn < gp2->posn) {		/*insert at head */
                        ll->next = (LINKLIST *)sc->gauss_pts;
                        sc->gauss_pts = ll;
                    }
                    else {
                        while(l2->next) {
                            gp2 = (GAUSS_PT *)(l2->next)->data;
                            if(gp->posn < gp2->posn) {
                                ll->next = l2->next;
                                l2->next = ll;
                                DONE=1;
                                break;
                            }
                            l2 = l2->next;
                        }//end while
                        if(!DONE)
                            l2->next = ll;
                    }
                }
            }
            else {
                printf(" get_?value err= %d\n", err);
                RXFREE(gp);
                RXFREE(ll);
            }
        }
        else
            printf("skip point %d oldN is %d\n",pt->GetN(),oldN);
    }
    retVal = 1;
    if(dbg) cout<< " \nleave\n"<<endl;
    if(dbg) Print_SC_GaussPoints(stdout, sc);
    return(retVal);
}
//
int addOneGaussPt(RX_FESite *pt,PSIDEPTR *plist,int n,int *isReversed,int space,int what)
{
    assert(pt);
    //  RXEntity_p e;
    sc_ptr  sc=0;        /* the seamcurve it is on */
    static int rev[2][3] = {{0,1,2},{2,1,0}};
    static double tol = (float) 0.01;

    double ss_start,ss_end, dist=0.0,ss=0.0, dist_best=0.0,ss_best=0.0; //gcc
    int wh, i;
    struct GAUSS_PT *gp;
    linklist_t *ll;
    int retVal=0;
    int best= -1;
    int sli, err=0;
    PSIDEPTR ps;
    ON_3dPoint r;
    ON_3dVector t;

    for(i=0;i<n;i++) {
        sc = (plist[i])->sc; sli = sc->Esail->SailListIndex();
        //  sc = (sc_ptr  )e;
        wh = rev[isReversed[i]][space];
        tol = (sc->GetArc(wh))/1000;	/* set tol to .1 % of arclength */

        ss = ss_start = ((plist[i])->End1off->Evaluate(sc,wh));
        ss_end = ((plist[i])->End2off->Evaluate(sc,wh));

        retVal = sc->m_pC[wh]->Drop_To_Curve(pt->ToONPoint(),&r, &t, tol,&ss,&dist);

        /*    GOAT retVal = PC_ Drop_ Perpe ndicular((VECTOR*)pt, sc->p[wh], sc->c[wh], tol,&ss,&dist,0);  */

        /* if retVal > 0 then converged OK
       if dist < tol then met the tolerance ???????
       if s > start of pside  && s < end of pside then the point is
       within the bounds of this panel
       */

        if(retVal>0 && (ss >= ss_start) && (ss <= ss_end) ) { /* OK to consider this pside */
            if(best < 0 || (dist < dist_best)) {
                dist_best = dist;
                ss_best = ss;
                best = i;
            }
        }
    } /* next pside */
    if(best < 0) {
        char buf[256];
        retVal = -1;
        sprintf(buf,"Could not find Perpendiculars on (%f,%f,%f)!!!\n",pt->x,pt->y,pt->z);
        sc->OutputToClient(buf,2);
    }
    else {
        sc = (plist[best])->sc;
        ps = plist[best];
        //  sc = (sc_ptr  )e;
        wh = rev[isReversed[best]][space];
        if(GCDB) printf("Looking at point at (%f,%f,%f)\n",pt->x,pt->y,pt->z);
        if(GCDB) printf("Found the closest seamcurve '%s' at %f along and %f away\n",
                        sc->name(),ss_best,dist_best);
        /* add the point to the seamcurve's gauss_pt data structure */
        ll = (LINKLIST *)CALLOC(1,sizeof(linklist_t));
        gp = (GAUSS_PT *)CALLOC(1,sizeof(struct GAUSS_PT));
        gp->n = pt->GetN();
        err=0;

        switch(what) {
        case PCG_DEFLECTED :
            cf_get_gvalue(sli,gp->n,&(gp->val),&err);
            break;
        case PCG_UNDEFLECTED :
            cf_get_svalue(sli,gp->n,&(gp->val),&err);
            break;
        default:
            assert(0);
        }


        if(err == 0) {
            if(fabs(gp->val) <= 1e-2) {
                if(GCDB) printf("node %d has gp value = %f\n",gp->n,gp->val);
                //getchar();
            }
            if(sc->edge) {
                if(ps->leftpanel[0]) {
                    if(ps->leftpanel[1]) {
                        ps->OutputToClient("This edge seems to have panels onboth sides !",2);
                    }
                    gp->val = -G_Factor*(M_PI+M_PI - gp->val); /* edge with panels on left */
                    if(GCDB) printf("considering point %d which has gval = %f(Panel on LEFT)err=%d\n",gp->n,gp->val,err);       	}
                else {
                    gp->val = G_Factor*(M_PI+M_PI - gp->val); /* edge with panels on right */
                    if(GCDB) printf("considering point %d which has gval = %f(Panel on RIGHT)err=%d\n",gp->n,gp->val,err);
                }
            }
            else {
                gp->val = G_Factor*  ( - gp->val);//  (M_PI + M_PI - gp->val);
                if(GCDB) printf("considering point %d which has gval = %f(Internal Point)err=%d\n",gp->n,gp->val,err);
            }
            if(err != 0)
                gp->val = 0.0;

            gp->posn = ss_best/(sc->GetArc(wh));
            gp->dist = dist_best;
            ll->data = gp;

            if(sc->gauss_pts == NULL) {
                sc->gauss_pts = ll;   /* first one */
            }
            else {
                linklist_t *l2 = (LINKLIST *)sc->gauss_pts;
                struct GAUSS_PT *gp2 = (GAUSS_PT *)l2->data;
                int DONE=0;
                if(gp->posn < gp2->posn) {
                    /*insert at head */
                    ll->next = (LINKLIST *)sc->gauss_pts;
                    sc->gauss_pts = ll;
                }
                else {
                    while(l2->next) {
                        gp2 = (GAUSS_PT *)(l2->next)->data;
                        if(gp->posn < gp2->posn) {
                            ll->next = l2->next;
                            l2->next = ll;
                            DONE=1;
                            break;
                        }
                        l2 = l2->next;
                    }
                    if(!DONE) {
                        l2->next = ll;
                    }
                }
            }
        }
        else {
            RXFREE(gp);
            RXFREE(ll);
        }
        retVal = 1;
    }
    return(retVal);
}


int Resolve_Edit_Card(RXEntity_p e, RXEntity_p *r)
{

    int retVal =0;
    *r = NULL;
    if(stristr(e->name(),"gausscurve")) {
        retVal = Edit_Gauss_Curve_Card(e, r);
    }
    else if(stristr(e->name(),"relsite")) {
        retVal = Edit_RelSite_Card(e,r);
    }
    else if(stristr(e->name(),"seamcurve")) {
        retVal = Edit_SeamCurve_Card(e,r);
    }
    else {
        char buf[256];
        sprintf(buf,"Edit card '%s' not yet implemented",e->name());
        e->OutputToClient(buf,2);
    }

    if(retVal >0) {
        e->Needs_Finishing = 0;
        e->SetNeedsComputing(0);
        e->Needs_Resolving= 0;
        retVal = 1;
    }
    return(retVal);
}
