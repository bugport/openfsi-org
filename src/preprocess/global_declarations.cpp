#include "StdAfx.h"
#include "RXSummaryTolerance.h"
#include "global_declarations.h"
#include "RXLogFile.h"
#include "ch_rowt.h"

#include "controlmenu.h"
#include "offset.h"  
#include "units.h"


int g_JPEG_x;
int g_JPEG_y;
int g_JPEG_Quality;

char g_Overlay_Text_Size[64];
int g_Gcurve_Count;


#ifdef HOOPS
int g_Colour_By_Face;
int g_Display_Materials;
int g_Display_Mould;
#ifdef NROWDIM
const char *g_pbname[NROWDIM] = {"faces","edges","lines","markers","text","orientation","Material Type","arrow", "interpolation surface","field" };
const char * g_Expname[NROWDIM] = {"jpg","hmf","pict","eps","3dm","dxf","iges","pdf","fullscreen","everything","dog" };
#endif //#ifdef NROWDIM


#endif
double g_Gauss_Cut_Dist;
double g_Gauss_Parallel;
char* g_Preprocessor_Heuristics = NULL;
struct _OUTPUT_UNITS g_Units[N_UNITS+1 ];


#ifdef _X
int l_ac;
Widget g_panelSolveButton;	//from sailmenu.h  /* kept to enable / disable items later */
Widget g_meshButton;
Widget g_MaterialWidget;

Widget g_PansailTextWindow;	//from text.cpp
Widget g_RelaxTextWindow;

Widget g_qdialog;
Widget g_resolveWidget;

Widget g_prestressWidget;
Widget g_AnalysisKillWidget;
Widget g_AnalysisCalcWidget;

Widget g_Active_VPP_Widget;
Widget g_Passive_VPP_Widget;

Widget g_visButton[NROWDIM];  //From radiomenus.cpp
#endif


SAIL *g_boat;

#ifndef RXQT
Graphic * g_selectGraph; 
#ifdef MAX_GRAPH
Graphic g_graph[MAX_GRAPH];
#endif
int g_hoops_count;	/* to allow for 0 being boat window */
#endif

/* Optimisation_Function A; */

#ifdef _X

int g_Select_Action=0;
Widget g_open_dialog;		/* moved 31Jan97 Adam */
#endif
#ifdef MDIM
#ifdef _X
Widget g_saveChildren[MDIM];
Widget g_loadinterpChildren[MDIM];
Widget g_dropChildren[MDIM];
#endif
//SAIL *g_sailObject[MDIM];
#else
#pragma error (" no MDIM (??)")
#endif //#ifdef MDIM

//int g_nCams=0;
//RXEntity_p g_camPlist[30];
int g_ArgDebug;
/* MenuCB.c */

int g_TrimExists = 0;

int AutoCalc = 0; /* auto calc turned off at start */


#ifdef _X
Widget g_options_dialog = (Widget) NULL;
Widget g_options_form = (Widget) NULL;
Widget g_options_ref_text = (Widget) NULL;
Widget g_options_h_text = (Widget) NULL;

Widget g_wake_dialog = (Widget) NULL;
Widget g_wake_form = (Widget) NULL;
Widget g_wake_label = (Widget) NULL;
Widget g_wake_name_text = (Widget) NULL;
Widget g_wake_file_button = (Widget) NULL;
Widget g_wake_ok_button = (Widget) NULL;
Widget g_wake_cancel_button = (Widget) NULL;


Widget g_wind_dialog = (Widget) NULL;
Widget g_wind_form = (Widget) NULL;
Widget g_wind_label = (Widget) NULL;
Widget g_wind_name_text = (Widget) NULL;
Widget g_wind_file_button = (Widget) NULL;
Widget g_wind_ok_button = (Widget) NULL;
Widget g_wind_cancel_button = (Widget) NULL;

Widget g_control_dialog = (Widget) NULL;
Widget g_ncycle_label = (Widget) NULL;
Widget g_method_label = (Widget) NULL;
Widget g_oldmx_label = (Widget) NULL;
Widget g_const_label = (Widget) NULL;
Widget g_choose_label = (Widget) NULL;
Widget g_panalysis_label = (Widget) NULL;
Widget g_phase_label = (Widget) NULL;
Widget g_phase_frame = (Widget) NULL;
Widget g_phase_rowcol = (Widget) NULL;
Widget g_press_frame = (Widget) NULL;
press_rowcol_p g_press_rowcol = (press_rowcol_p) NULL;
Widget g_ncycle_frame = (Widget) NULL;
Widget g_ncycle_rowcol = (Widget) NULL;
Widget g_ncycle_text = (Widget) NULL;
Widget g_ncycle2_text = (Widget) NULL;
Widget g_ncycle3_text = (Widget) NULL;
Widget g_ranalysis_label = (Widget) NULL;
Widget g_nonlin_label = (Widget) NULL;
Widget g_crease_label = (Widget) NULL;
Widget g_extra_label = (Widget) NULL;
Widget g_relax_frame = (Widget) NULL;
relax_rowcol_p g_relax_rowcol = (relax_rowcol_p) NULL;
#endif

//END FROM control.cpp


FILE *g_SP = NULL;

Optimisation_Structure g_Opti; // was O_Function


//from settrim.cpp
#ifdef _X
Widget g_control_text[CONTROL_COUNT];



XtAppContext        g_app_context;
Display         *g_display;
#endif  //#ifdef _X 


ch_data_t g_CH_Run_Data;  /* data structure used to store currently open*/


double g_Relax_Tolerance, g_Relax_R_Damp,g_Relax_V_Damp,g_diagonalfac;
//int g_iterations;

int g_Analysis_Flags=INITIAL_RUN;
control_data_t g_ControlData[3];  /* set in getglobal.c */

////  WInd geometry variables - now shifted to rxwindgeometry for ease of housekeeping.

#ifdef NEVER
// ncomp now an automatic variable of calcnow. 
int g_N[MAXCOM],g_M[MAXCOM],g_pModel[MAXCOM]; 

char g_pName[MAXCOM][16];

float g_zmdiam[MAXCOM][MAXROW];
float g_diam[MAXCOM][MAXROW];
float g_nmastp[MAXCOM];
float *g_Uv;
float *g_Vv;
float *g_Xv;
float *g_Yv;
float *g_Zv;
float *g_Xc;
float *g_Yc;
float *g_Zc;
float *g_Uc;
float *g_Vc;
float *g_Dcp;
float *g_CpU;
float *g_CpL;
float *g_LU;
float *g_HL;
float *g_phiU;
float *g_phiL;
float *g_CfU;
float *g_CfL;
int *g_iblstu;
int *g_iblstl;
float *g_LSD;
float *g_BLF;	
float * g_Thu;
float * g_Thl;
float * g_Hu;
float * g_Hl;
float * g_Cfu;
float * g_Cfl;
float * g_Singo;
int g_coffset[MAXCOM];
int g_goffset[MAXCOM];
int g_noffset[MAXCOM];
int g_moffset[MAXCOM];
int g_nwakeso,g_ntwako,g_ntoto,g_ncuto;
int g_mwko[MAXCOM],g_nwko[MAXCOM];
int g_irelxo[MAXCOM][MAXROW][MAXROW];		/* these are mwk(pModel) * nwk(pModel) in size 	*/
/* BUT mwk and nwk are returned to me !! 	*/
float g_xwakeo[MAXCOM][MAXROW][MAXROW];
float g_ywakeo[MAXCOM][MAXROW][MAXROW];  		/* defined to (MAXROW,MAXROW,MAXCOM)     	*/
float g_zwakeo[MAXCOM][MAXROW][MAXROW];
char g_namwako[MAXCOM][16];
char g_baseName[20];
char g_wakefile[128];
#endif


//From iges.cpp
int g_GlobalType[iges_NGLOBALS ]= { 
    IGS_STRING,
    IGS_STRING,
    IGS_STRING,
    IGS_STRING,
    IGS_STRING,
    IGS_STRING,
    IGS_INT,
    IGS_INT,
    IGS_INT,
    IGS_INT,
    IGS_INT,
    IGS_STRING,
    IGS_REAL,
    IGS_INT,
    IGS_STRING,
    IGS_INT,
    IGS_REAL,
    IGS_DATE,
    IGS_REAL,
    IGS_REAL,
    IGS_STRING,
    IGS_STRING,
    IGS_INT,
    IGS_INT,
    IGS_DATE  };
int g_IGS_DTYPE[18]= {	IGI,IGI,IGI,IGI,IGI,IGI,IGI,IGI,IGI, 
                        IGI,IGI,IGI,IGI,IGI,IGS,IGS,IGS,IGI};
//char g_igs_stmp[iges_MAXSTR];
//END from iges.cpp


#define MAX_TEXT_WINDOWS 2


const char g_PansailTextFileName[]={"pansail.cir"};
const char g_RelaxTextFileName[]={"shapeout.txt"};
#ifdef _X
Widget g_TextWindows[MAX_TEXT_WINDOWS];
#endif
const char *g_TextFiles[MAX_TEXT_WINDOWS];
////From text.cpp

//From question.cpp
struct QDATA *g_question_data = NULL;
//int  g_QDB = 0;
int g_UNRESOLVE_ALL=0;
//End from question.cpp


//from vppint.cpp
char g_VPP_Command[256];
char g_VPP_Address[256];
char g_VPP_Transfer_File[256];
class RXLogFile g_VPP_Sum;
//end from vppint.cpp



//from gridmain.cpp
int g_Check_Panels;
int  g_mydebug = 0;
//end from gridmain.cpp

//from debug.cpp
int g_errno;
//end from debug.cpp

//from delete.cpp
int g_Peter_Debug;
//end from delete.cpp

//from panel.cpp
long int g_RCOUNT;
double g_SOLVE_TOLERANCE=0.001;  
double g_ANGLE_TOL = 0.0002;
double g_GAUSS_FACTOR = 1.0;
double g_U1,g_U2; 
int g_TRACE=0;
int g_COLOR_ERROR = 0;
//end from panel.cpp

//from Check All.cpp
int g_Flag;
//end from CheckAll

int g_Gauss_Recursion ; 
int g_Spline_Division =33;
int g_Janet = 0;
int g_NPardisoIterations=2;

double g_Relaxation_Factor;
#ifdef HOOPS
int g_Mapcount;
char *g_Hoops_Driver;
int g_Hoops_Error_Flag;
char g_HNetServerIPAddress[256];
#endif

char g_RelaxRoot[256];
char g_CodeDir[256];
char g_workingDir[256];	
char traceDir[256];
char templateDir[256];
char g_currentDir[256];
char g_boatDir[256];
char g_defaultsFile[256];

char g_Export_Options[256];
//char g_After_Each_Run[256];
QString g_qAfterEachRun;
QString g_qAfter_Each_Cycle ;
QString g_qAfter_Each_Update;

//char g_Application_Name[256];

int g_Export_Reactions;

int g_Transform_On_Deflected;

int g_PanelInPlane;
int g_Use_ONCurve;

int g_NON_INTERACTIVE;

int g_Filament_pp;

float g_nSDs;

//const char*g_lpat[10]= {"----","- -", "....","-.-.","-..-..","-...","-- --","center","phantom","----"};
//const char*g_mpat[10]= {"/.\\","X","*","+","O","@", "(+)", "[]","[.]","\\*/"};

double g_VectorPlotScale =1.0;


#ifdef USE_PANSAIL
const char *g_pansailShell[N_PANSAIL_SHELLS] = {
    "panSailDp", 		/* 0 */
    "Singo", 		/* 1 */
    "Cfu", 			/* 2 */
    "Cfl" 			/* 3 */ };
//"BL Upper", 			/* 4 */
//"BL Lower" 			/* 5 */
//};
int g_pnslU[N_PANSAIL_SHELLS] = {
    CP_OP , 		/* 0 */
    DEGREE_OP, 		/* 1 */
    CP_OP, 			/* 2 */
    CP_OP };		/* 3 */

//OTHER_OP, 			/* 4 */
//OTHER_OP 			/* 5 */
#endif



const char *g_relaxShell[N_RELAX_SHELLS ] = {
    "None",  		/* 0 */
    "Stress X", 		/* 1 */
    "Stress Y", 		/* 2 */
    "Stress XY", 	/* 3 */
    "Strain X", 		/* 4 */
    "Strain Y", 		/* 5 */
    "Strain XY", 	/* 6 */
    "Stress up", 		/* 7 */
    "Pressure",		/* 8 */
    "stress lp", 		/* 9 */
    "Delta K", 	/* 10 */
    "crease", 		/* 11 */
    "S Angles",		/* 12 */
    "Err Ind", 		/* 13 */
    "Strain UP",             /* 14 */
    "Stiff UP"  ,        /* 15 */
    "Strain lp" ,             /* 16 */
    "amg thk",		// 17
    "amg ke",		// 18
    "amg kg",		// 19
    "amg nu" ,		// 20
    "ply min X",		// 21
    "ply max X",		// 22
    "ply min Y",		// 23
    "ply max Y",		// 24
    "ply max XY"		// 25
};
/*    case 21: //plyminX,
  case 22: // plymaxX,
  case 23:// plyminY,
  case 24:// plymaxY
  case 25:// plymaxXY */



int g_rlxU[N_RELAX_SHELLS] = {
    OTHER_OP ,  		/* 0 */
    STRESS_OP,		/* 1 */
    STRESS_OP, 		/* 2 */
    STRESS_OP, 		/* 3 */
    STRAIN_OP , 		/* 4 */
    STRAIN_OP , 		/* 5 */
    STRAIN_OP , 		/* 6 */
    STRESS_OP , 		/* 7 */
    PRESS_OP ,		/* 8 */
    STRESS_OP, 		/* 9 */
    SANGLE_OP,		/* 10 */
    STRAIN_OP, 		/* 11 */
    SANGLE_OP,		/* 12 */
    OTHER_OP,		/* 13 */
    STRAIN_OP,             	/* 14 */
    EA_OP ,         	/* 15  */
    STRAIN_OP,             	/* 16 */
    OTHER_OP ,  		// 17
    OTHER_OP ,  		// 18
    OTHER_OP ,  		// 19
    OTHER_OP   ,		// 20
    STRAIN_OP ,  		// 21
    STRAIN_OP ,  		// 22
    STRAIN_OP ,  		// 23
    STRAIN_OP ,  		// 24
    STRAIN_OP    		// 25
};



int init_global_declarations(void) {
    //    memset(&relaxflagcommon,0,sizeof(struct RELAX_FLAG_STRUCT  ));
 {
    relaxflagcommon.factor1=0;
    relaxflagcommon.factor2=0.01;
    relaxflagcommon.residualCap=-1;
    relaxflagcommon.DampingFactor=0;
    relaxflagcommon.admasmin=0.1;
    relaxflagcommon.g_UpdateTime = 1.0;
    relaxflagcommon.g_RKDamping=0;
    relaxflagcommon.g_RKEps=0;
    relaxflagcommon.g_RK_H1=0;
    relaxflagcommon.Stop_Running=0;
    relaxflagcommon.NompThreads=-1;
    relaxflagcommon.admasmin=0.1;
    relaxflagcommon.residualCap=-1;
    relaxflagcommon.ForceLinearDofoChildren= 1;
    relaxflagcommon.Mass_Set_Flag=1;
    relaxflagcommon.VPP_Connected=0;
    relaxflagcommon.ForceLinearDofoChildren=1;
    relaxflagcommon.Mass_By_Test=0;
    relaxflagcommon.SSDWrinklingAllowed=0;
    relaxflagcommon.K_on_Wrinkled_Matrix=0;
    relaxflagcommon.New_DD=0;
    relaxflagcommon.HardCopy=0;
    relaxflagcommon.dm=0;
    relaxflagcommon.LinkCompAllowedFlag=0;
    relaxflagcommon.StringCompAllowedFlag=0;
    relaxflagcommon.Debug1=0;
    relaxflagcommon.Debug2=0;
    relaxflagcommon.Force_Linear=0;
    relaxflagcommon.Use_DSTF_Old =0;
    relaxflagcommon.prestress=0;
    relaxflagcommon.form_2006=0;
    relaxflagcommon.BumperSticker=0;
    relaxflagcommon.EdgeCurveCorr=0;
    relaxflagcommon.SVD_Solver=0;
    relaxflagcommon.ElasticModifier[0][0] =1;
    relaxflagcommon.ElasticModifier[1][1] =1;
    relaxflagcommon.ElasticModifier[2][2] =1;

}
    *g_RelaxRoot				=(char)0;
    *g_CodeDir				=(char)0;
    *g_workingDir				=(char)0;
    *traceDir				=(char)0;
    *g_currentDir				=(char)0;
    *g_defaultsFile				=(char)0;
    *g_boatDir				=(char)0;
    *templateDir				=(char)0;

    *g_Export_Options			=(char)0;

    g_qAfter_Each_Cycle.clear();
    g_qAfterEachRun.clear();
    g_qAfter_Each_Update.clear();

    g_PanelInPlane = 0;
    g_Janet		 				=(int)0;
    g_Spline_Division 				=(int)33;
    g_Export_Reactions 				=(int)-2; // Flagged
    g_Transform_On_Deflected 			=(int)1;
    g_Use_ONCurve					=0;


#ifdef HOOPS
    g_Hoops_Error_Flag=0;  strcpy(g_HNetServerIPAddress,"192.168.1.65");
#endif
    g_Filament_pp=1;
#ifdef _X
    l_ac=0;
#endif
    //#ifdef MATHLINK
    //	memset(  &g_MathSession,0,sizeof(RX_MathSession ));
    //#endif
    //Nothing for now thomas 09 06 04

    g_Relax_Tolerance = 0.1;
    g_Relax_R_Damp=00.0;
    g_Relax_V_Damp=0.0;
    g_diagonalfac =0.005;
    return 1;

}//int init_globals_declarations




void ClearGlobalValues(void){
    if(g_Preprocessor_Heuristics )
        RXFREE (g_Preprocessor_Heuristics);
#ifdef HOOPS
    RXFREE (g_Hoops_Driver);
#endif
    g_CH_Run_Data.m_label.clear();
    if(g_CH_Run_Data.list) {
        for(int i=0; i<g_CH_Run_Data.N;i++)
            RXFREE(g_CH_Run_Data.list[i]);
        RXFREE(g_CH_Run_Data.list);g_CH_Run_Data.list=0;
    }


}
