#ifndef DOVTEMPTEST_AJN2006
#define DOVTEMPTEST_AJN2006

#include "opennurbs.h"

//extern const 	RXON_NurbsSurface *g_uvsurf; // uv as fn of IJ
//extern const	RXON_NurbsSurface *g_sa;  // angle sine ALL THESE ad fn of IJ
//extern const 	RXON_NurbsSurface *g_ca; // angle cosine
//extern const 	RXON_NurbsSurface *g_thk;	//thickness in number of plies
//extern const 	RXON_NurbsSurface *g_ky;	// eyy by exx 
//extern const 	RXON_NurbsSurface *g_kg;	// gxy by exx
//extern const 	RXON_NurbsSurface *g_nu;	// nu
extern const 	ON_NurbsSurface *g_2dmould;
extern const 	ON_NurbsSurface *g_3dmould;
extern struct	PC_3DM_Model *g_Last3dmModel;
#endif //DOVTEMPTEST_AJN2006
