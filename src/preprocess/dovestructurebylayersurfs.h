// dovestructurebylayersurfs.h: interface for the dovestructurebylayersurfs class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DOVESTRUCTUREBYLAYERSURFS_H__08AF2687_9317_4E78_8D23_69A480C45D90__INCLUDED_)
#define AFX_DOVESTRUCTUREBYLAYERSURFS_H__08AF2687_9317_4E78_8D23_69A480C45D90__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


//#ifndef BOOL
//#define BOOL ON_BOOL
//#endif
#include "AMGLevel.h"
#include "RXON_Surface.h"
#include "doveforstack.h"


class dovestructurebylayersurfs  : public doveforstack // WAS dovestructure 
{
public:
// traceLayer returns a ptr to a new ON_CurveOnSurface.
		AMGTape *GetOneTapeGeometry(
				ON_Curve *p_pthec,
				double t,
				bool LorRonCurve,
				double alphaerror,
				double MaxLateralOverlap,
				double minLength,
				double maxlength,
				double tapewidth,
				AMGLevel level);

	    ON_Curve *TraceLayer(const double u0,const double v0,  // initial coords.  In normalized UV space
		const AMGLevel level, 
		const double maxlenXYZ,			// trace will stop at this length.
		const ON_3dVector &p_startDirXYZ ,// the trace will start approximately in this direction.
		const double eps,			// an accurate trace needs about 1e-9
		const int p_TraceFlag= 0	// either 0 or RX_DOVE_TOBOUNDARY
		);

	int FibreTangentXYspace(const double u, const double v, const AMGLevel level, ON_3dVector &p_vec);
	ON_String GetSurfName(const int & p_ID) const;
	int CountSrfs() const;
	int Print(FILE *fp);

	dovestructurebylayersurfs();
	virtual ~dovestructurebylayersurfs();

	int SetDAlpha(const double & p_Dalpha); 

	virtual int HowManyLayers(const int p_flag);

virtual int AlphaFromRefDir(
		const double u, // in domain 0 to 1
		const double v, // in domain 0 to 1
		AMGLevel level,// lowest is level 0; If the level doesnt exist, the fn returns 0;
		double *a,	// the angle of this ply in radians from the ref vector.
		ON_2dVector *pref2d=0, // the reference vector in U,V space.
		ON_3dVector *pref3d=0// the reference vector in cartesian space, calculated from ON_Surface *m_mould
);

	
virtual int GradAlpha(
		const double u, // in domain 0 to 1
		const double v, // in domain 0 to 1
		const AMGLevel level,// lowest is level 0; If the level doesnt exist, the fn returns 0; const VC8
		ON_2dVector *gradAlpha,// the directional derivative {da/du,da/dv}
		int p_DALPHADUV = 0);  // set to true to take into account the dimensions of the ALPHA surface
	
	ON_String Serialize();
		char * Deserialize(char*lp);
		int Set_SurfacePtrs();
private:

		ON_ClassArray<ON_String> m_namesInput; // may be in any order
		ON_ClassArray<ON_String> m_namesSurf; // the same order as m_surfs
		ON_SimpleArray<RXON_NurbsSurface *>  m_Surfs;

		double m_DAlpha; //Dalpha from the Iso U in radian
protected:
	int DAlphaByDx(const ON_3dPoint &qxy ,
		const ON_2dPoint &quv, const AMGLevel level, ON_3dVector *dadx);

		int DAlphaByDx_LS(const ON_Surface *s ,
		const ON_3dPoint &quvOnS,ON_3dVector *dadx);
};

#endif // !defined(AFX_DOVESTRUCTUREBYLAYERSURFS_H__08AF2687_9317_4E78_8D23_69A480C45D90__INCLUDED_)
