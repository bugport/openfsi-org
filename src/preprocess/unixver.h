#ifndef _UNIXVER_HDR_
#define _UNIXVER_HDR_ 
#ifdef linux
	#define RX_SYSCOPY  "cp"
#else
	#define RX_SYSCOPY  "copy"
#endif

#ifndef __ICC
#ifdef linux
	#define _NOT_MSW
#endif

#ifdef _NOT_MSW
	#define strlwr (char *) strtolower
#endif

//#ifdef RX_ICC // was _GNU_SOURCE
//	#define _copysign(x,y) copysign(x,y)
//#endif
#endif
#ifdef WIN32
#define strncasecmp(a,b,c) _strnicmp(a,b,c)
#endif
#endif //#ifndef _UNIXVER_HDR_
