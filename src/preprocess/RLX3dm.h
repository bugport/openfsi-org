/* header file for 3DMin function Thomas Ricard, PH 2003*/
#ifndef _RLX3DM_H_
#define _RLX3DM_H_

#include "vectors.h"
#include "rlx3dmEntities.h"

#include "griddefs.h"


/* section definitions for file pointer position */

//#define iges_STDSTR 64
//#define iges_MAXSTR 255
//#define iges_NGLOBALS 25
//#define iges_blksize	8
//#define IGS_BUFFERSIZE	256 // sb 1024 at least

//#define IGI 1
//#define IGS 2
//With the exception of the fields numbered 10, 16, 17, 18, and 20, 
//entries in all fields in this section will be either integer constants
 //or pointer constants.

/*comented out by thomas 02 11 04
//!To allocate memory to a PC_3DM_Model
int alloc3DMstuff(struct PC_3DM_Model * l_stuff);
//!To allocate memory to a PC_3DM_Entity
int alloc3DMstuff(struct PC_3DM_Entity * l_stuff);

//!To free memory to a PC_3DM_Model
int free3DMstuff(struct PC_3DM_Model * l_stuff);
//!To free memory to a PC_3DM_Entity
int free3DMstuff(struct PC_3DM_Entity * l_stuff);
*/ //END comented out by thomas 02 11 04

//!Called when the user import an 3dm file, all the sail is created in this function 
/*!
	e: Enttity to point on the 3dm file
*/
EXTERN_C  int Resolve_3DM_Card(RXEntity_p e);

//!Read a 3dm file and fill a sail structure with its content
/*!
	p_3dm : pointer on a 3dm structure
	p_pSail : pointer on the sail structure to fill
*/
HC_KEY PC_3dm_Read(struct PC_3DM_Model *p_p3dm,
					 SAIL *p_pSail);

//!Write a 3dm file from a sail structure 
/*!
	p_3dm : pointer on a 3dm structure
	p_pSail : pointer on the sail structure to fill
*/

void PC_3dm_Initialize(rxONX_Model &model );


EXTERN_C int PC_3dm_Write(SAIL *p_pSail, const char * p_filename );//struct PC_3DM_Model *p_p3dm,
						//);

//!To write a RXEntity into a 3Dm file, 
/*!
	The file needs to be an openned 3dm file, no internal check for the kind of file. 
*/
int WriteEntityTo3dm(rxONX_Model * p_Model,RXEntity_p  p_e, ON_String & p_Notes);

HC_KEY PC_3dmin(struct PC_3DM_Model *p_p3dm, 
			  SAIL *p_pSail,
			  ON::unit_system  l_u = RLX_DEFAULT_UNITS );

//int iges_create_entity(char *line,struct PC_iges *p);

//!Read all the data in the 3dm file and set the sail with
int SetSailFrom3dm(struct PC_3dm_Model * p);

//!To read a generic 3dm object
/*!
	returns a pointer on the RXEntity which is created or NULL
*/
EXTERN_C RXEntity_p  Read3dmObject(struct PC_3DM_Entity * p_p3dmEnt,double scalefactor);


//!Point a point from a model
/*!
	p_pModel is the complete model, stores some data like layers... 
	p_pObj is the particular object to read
	returns a pointer on the RXEntity which is created or NULL
*/
EXTERN_C RXEntity_p  Read3dmPoint(struct PC_3DM_Entity * p_p3dmEnt);

EXTERN_C RXEntity_p  Read3dmCurve(struct PC_3DM_Entity * p_p3dmEnt);

EXTERN_C RXEntity_p  Read3dmSurface(struct PC_3DM_Entity * p_p3dmEnt);

EXTERN_C RXEntity_p  Read3dmBrep(struct PC_3DM_Entity * p_p3dmEnt);

EXTERN_C RXEntity_p  Read3dmMesh(struct PC_3DM_Entity * p_p3dmEnt);

EXTERN_C RXEntity_p  Read3dmPointCloud(struct PC_3DM_Entity * p_p3dmEnt);
 
EXTERN_C int Read3dmProperties(SAIL * p_e, struct PC_3DM_Entity * p_p3dmEnt);

int Read3dmNotes(SAIL* p_s, struct PC_3DM_Entity * p_p3dmEnt, const ON_3dmNotes & p_notes);

//To concatenate some attribute to a name according to layer's attributes (environment variables)
ON_String PC_3dm_create_attributes(struct PC_3DM_Entity * p_p3dmEnt);

ON_String PC_StripTail( ON_String & name,  char* tok);

int PCN_ON_NURBSCurve_To_Poly(struct PC_3DM_Entity *n,int *c,VECTOR **poly);

//! To the layer ID from an entity 
int GetONXObjLayer(rxONX_Model * p_Model,RXEntity_p  p_e);

int GetONXObjLayerByName(rxONX_Model * p_Model,ON_String p_name);


//!Check the 3DM model and transform the name of p_p3dmEnt.m_pObj to make it unique
/*!
	If p_LayerID>-1 process the check only in the layer related to p_LayerID 
*/
int Make_Name_Unique(struct PC_3DM_Entity * p_p3dmEnt, const int & p_LayerID);

//!to remove the coments from the rhino name
/*!
	the coment mark is "!"
*/
ON_String RemoveComents(const ON_String & p_line);
EXTERN_C struct TwinEntity Generate_Curve_From_ONO(SAIL *sail,const char*atts,const char*handle,char*layer,const char *name, ONX_Model_Object*p_ono );

#endif  //#ifndef _RLX3DM_H_





