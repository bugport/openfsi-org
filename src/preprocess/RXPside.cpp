
#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include "RX_FEEdge.h"
#include "RXCurve.h"
#include "RXOffset.h"
#include "RXPanel.h"
#include "RXSRelSite.h"
#include "RXRelationDefs.h"

#include "global_declarations.h"
#include "rxfixity.h"
#include "etypes.h"
#include "panel.h" // fo things like Get_Position_By_Double
#include "entities.h"
#include "printall.h" // for print_end
#include "nurbs.h"
#include "RXPside.h"


RXPside::RXPside(void)
{
    assert("dont use default pside constructor"==0);
}
RXPside::RXPside(SAIL *sail):RXEntity(sail) 
  ,m_psFlag(0)
{
    Init();
}

RXPside::~RXPside(void)
{
    this->CClear();  // we must assume that it has already been cleared
    // because when we delete we cannot know if its SC is still existing

}


int RXPside::Init() {

    m_sList.clear();
    m_eList.clear();

    leftpanel[0]=leftpanel[1]=0;
    leftZone[0]=leftZone[1]=0;

    //    e[0]=e[1]=0;
    m_ep[0]=m_ep[1]=0;

    el[0]=el[1]=0;	er[0]=er[1]=0;
    sc=0;
    End1off=0;
    End2off=0;

    Defined_Material[0]= Defined_Material[1]= Defined_Material[2]=0;

    Mat_Angle[0].flag=0;		Mat_Angle[0].angle=0;		Mat_Angle[0].str=0;
    Mat_Angle[1].flag=0;		Mat_Angle[1].angle=0;		Mat_Angle[1].str=0;
    Mat_Angle[2].flag=0;		Mat_Angle[2].angle=0;		Mat_Angle[2].str=0;

    Backwards=0;
    return 0;
}
int RXPside::Compute(void) {
#ifdef DEBUG
    //cout<<"RXPside::Compute "<<this->name()<<endl;
#endif
    if(!this->GetpsFlag())
        this->SetpsFlag(1);
    return 0;
}
int RXPside::Resolve (void) {
#ifdef DEBUG
    cout<<"RXPside::Resolve should not happen";
    assert(0);
#endif	
    return 0;
}
int RXPside::Finish(void) {

    /* given e1p,e2p it generates e1,e2.
     e1ptr,e2ptr, e1off,e2off,e1sign  have been retired. We can retrieve
     the end positions from e1p,e2p*/

    //    for(int e=0;e<2;e++)
    //       this->e[e] = this->Ep(e);

    return(1);

}
int RXPside::ReWriteLine(void) {
    return 0;
}
int  RXPside::CClear(){
    int rc=0;

    int k,l;
    /* remove from sc's pslist */

    if(sc) {
        sc->Make_Pside_Request = 1;

        for(k=0;k<sc->npss;k++) {
            if(sc->pslist[k]==this) {
                (sc->npss)--;
                for (l=k;l<sc->npss;l++) {
                    sc->pslist[l]= sc->pslist[l+1];
                }
                break;
            }
        }
    }
    if(!g_Janet &&this->m_sList.size()) this->ClearSlist(this->Esail);
    if(!g_Janet && this->m_eList.size())	this->ClearElist(this->Esail);

    /* clear references at end nodes */

    this->Remove_From_Node_Lists() ;
    /* el and er of adjacent psides still point to eo so the above rtine
  calls  Sort_One_Node at the ends. This resets el*/

    /* clear panel references. Delete(ie Kill) the panels */
    for (k=0;k<2;k++) {
        Panelptr  pan = this->leftpanel[k];
        if( pan) {
            for(l=0;l<pan->m_psidecount;l++) {
                PSIDEPTR pss = pan->m_pslist[l];
                for( int m=0;m<2;m++) {
                    if(pss->el[m]== this) pss->el[m]=NULL;
                    if(pss->er[m]== this) pss->er[m]=NULL;
                }
            }

            pan->Kill();
            this->leftpanel[k]=NULL;
        }
    }

    /* clear zone references. */
    for (k=0;k<2;k++) {
        Panelptr  pan = this->leftZone[k];
        if(pan) {
            for(l=0;l<pan->m_psidecount;l++) {
                int m;
                PSIDEPTR pss = pan->m_pslist[l];
                for(m=0;m<2;m++) {
                    if(pss->leftZone[m]==pan) pss->leftZone[m]=NULL;
                }
            }
            pan->Kill();
            this->leftZone[k]=NULL;
        }
    }

    this->End1off=  this->End2off=0;  // the expressionlist deals with this.
    this->sc=0;
    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}

extern int  RXPPRINT  ;// defined in RXPanel.cpp

//  strictly this is incomplete, because in R2 the grid density is controlled at SITE and Curve level as well
// as globally via  RXSail::MeshSize
// for R3 we havent decided if we keep the same or we provide an easy way of varying density 
// it will probably be via the accumulation of several functions in (uv). some user-defined and some from analysis
// error estimates. 

// ALSO this requires that the pside's end Sites are already in the same coordinate system as the grid density function.

vector<double> RXPside::GetOffsets( )const {

    vector<double> rc;
    double x,last=0,d;
    RXSitePt *p1, *p2, p;
    double L,dp1,off1,off2;

    p1 =  this->Ep(0) ;
    p2 =  this->Ep(1) ;

    off1 = this->End1off->EvaluateBySpace(sc,RX_REDSPACE);  // (dec 2008) must measure in redspace
    off2 = this->End2off->EvaluateBySpace(sc,RX_REDSPACE);

    p1->setDs(off1 ) ; 	p2->setDs(off2 ) ; // BEWARE.  ds is ephemeral.

    dp1 = this->sc->MeshSize(*p1 );

    L = off2 - off1;
    if(off2>off1) {
        last=off1;  d = dp1;
        do{
            rc.push_back(last);
            x = last+d/2.;
            p = (*p1) * (1. - x/(off2-off1)) + (*p2) * (x/(off2-off1));
            d = this->sc->MeshSize(p );
            last +=d;
        } while(last < off2);
    }
    else {
        cout<< " off2 small"<<endl;
    }
    // now scale all by (off2/last
    vector<double>::size_type i,sz = rc.size();
    for(i=1;i<sz;i++) {
        x = rc[i]-rc[0];
        rc[i] = rc[0] + x * L/(last-off1);
    }

    rc.push_back(off2);
    /* lets divide this by redlength and take care when using the value to multiply by
  the length of the curveon which we are doing  an EV Offset or GetPosition. */
    return rc;

}
int RXPside::ReMesh(void)   // ALL WRONG
{ int err,rc=0;
    // called when the pside already has m_sList but its geometry has changed.
    // first we re-scale the ds' to the new endnode ds'
    // next we reset the coords of the slist, (INTERNAL NODES ONLY)
    // then we reset the lengths of the edges.
    //  cout<<"TODO:RXPside::ReMesh  "<<endl;
    // return 0;
    double tNonDim, t;

    RXSitePt  xxx;
    SAIL *sail = this->Esail ;
    RXEntity_p mould = sail->GetMould();
    assert(this->m_sList.size()>0 );
    RXSitePt *p1, *p2;
    double  off1,off2,do1,do2;

    RX_FESite *pp ;
    p1=  this->Ep(0);
    p2 = this->Ep(1);
    // tried this oct 2012 but it didnt force all the panels to re-mesh
    if(this->leftpanel[0]) this->leftpanel[0]->Needs_ReGridding=1;
    if(this->leftpanel[1]) this->leftpanel[1]->Needs_ReGridding=1;

    off1 = this->End1off->EvaluateBySpace(sc,RX_REDSPACE);  // (dec 2008) must measure in redspace
    off2 = this->End2off->EvaluateBySpace(sc,RX_REDSPACE);

    p1->setDs(off1) ; 	p2->setDs( off2) ; // BEWARE.  ephemeral.

    off1 = this->End1off->EvaluateBySpace(sc,RX_REDSPACE);  // (dec 2008) must measure in redspace
    off2 = this->End2off->EvaluateBySpace(sc,RX_REDSPACE);
    do1=  off1 -  p1->getDs() ; 	do2 = off2- p2->getDs(); // must be WRONG. both are in RX_REDSPACE

    vector<double>::size_type i,sz = this->m_sList.size();
    for(i=1;i<sz-1;i++) {

        pp = this->m_sList[i].first; // 1,2,3.....n-1
        //        defl = pp->Deflection();
        //      err=pp->SetDeflection(ON_3dVector(0,0,0)); // not sure its necessary to carry over the deflection like this.
        tNonDim = this->m_sList[i].second;
        t = off1 *(1-tNonDim) + off2*tNonDim;
        pp->setDs(t); // intermediate pts in  m_sList
        t = t + do1*(1.-tNonDim) + do2*tNonDim;
        Get_Position_By_Double(this, t,1,xxx); //blackspace
        if(mould&&!(pp->m_Site_Flags&NOMOULD  ) ) { // shift x to the nearest place on the mould, using pp->(u,v) as a seed
            ON_2dPoint uv= sail ->CoordModelXToUV(pp,&err);
            xxx = *pp; xxx.m_u = uv.x; xxx.m_v=uv.y;
            if(err)
            {
                ON_String l_buf = ON_String("(pside::remesh) Find Nearest didnt return OK:  " );
                rxerror(l_buf,1);
            }
            else
            {
                Evaluate_NURBS(mould,xxx.m_u,xxx.m_v,&xxx);
                ON_3dVector dx = *pp-xxx;
                if(dx.Length()>0.0001)
                    qDebug()<< "(pside remesh) move pt"<<pp->GetN()<<dx.x<<dx.y<<dx.z;
            }
        }// if mould
        // x is now the blackspace position of the site pp. Lets add its deflections and set its world-space coords.
        pp->RXSitePt::Set(xxx);   //  just sets pp->(xyz)
        pp->RX_FESite::Set(xxx);
        //      pp->SetDeflection(defl);
    } // for i

    rc+=UpdateEdges();  // this fn doesnt do anything
    this->SetpsFlag(0);
    return rc;
}
/*
 TODO: a function that looks at the pside to see if the internal points are out-of-order
     cant trust dsEphemeral getDs();
     and m_sList is a hang-over from the last meshing
     so we have to evaluate the offsets of each site on the pside
     Slow, but...
*/
int RXPside::IsDisordered(void) const
{
    int na, rc=0;
    double  z2  ;
//    class RX_FESite*e;
    class RXSRelSite*p;

    std::vector< std::pair<class RX_FESite*,double> >::const_iterator it;

    std::vector<double> Z; Z.reserve( m_sList.size());
    std::vector< double> ::const_iterator i1,i2;
    if(m_sList.size()<1) return 0;
  //  qDebug()<<"second \toffset\tephemeral"<<"RXPside::orderTEst:"<<this->GetQName();

    sc_ptr  lsc=this->sc;
    na=lsc->n_angles-1;
    z2=lsc->ia[0].m_Offset->Evaluate(lsc,0);
    for(it =m_sList.begin();it !=m_sList.end() ;it++  )
    {
     //   e=( *it).first;
        p= (class RXSRelSite*)it->first;


        if(p->CUrve[1])
            if(p->CUrve[1]== this->sc)
                z2=p->Offsets[1]->Evaluate(sc,0);
        if(p->CUrve[0])
            if(p->CUrve[0]== this->sc)
                z2=p->Offsets[0]->Evaluate(sc,0);

        //  qDebug()<<z2;
        Z.push_back(z2);
        z2=9999999;
        if(na>0)
            z2=lsc->ia[na].m_Offset->Evaluate(lsc,0);
    }

    if(Z.size()<2) return 0;
    for(i2=Z.begin(),i1=i2++;i2!=Z.end();i2++)
    {
        if(*i2<= *i1)
        {   rc++;
            qDebug()<<*i2<<" <= "<<*i1; }
    }
    if(rc)
        qDebug()<<" FOUND DISORDERED!!!"<<this->GetQName();;
    return rc;
}

/*  the supposition here is that if there is a mould, the redspace shares a coordinate system with it.
 IE, the panel is on the mould and there is a small amount of shape in the pside
*/
int RXPside::Mesh(void) // generates points in BlackSpace along the pside from end0 to end1
{ int err,rc=0;
    //  Decide on the number to insert using redspace.
    // then working in blackspace, generate the points, as in SetSNode.
    // Use REdSpace to set the edgelengths.
    RX_FESite *lastPt;
    double f ,t, Trel ;
    ON_3dPoint x;
    SAIL *sail = this->Esail ;
    RXEntity_p mould = sail->GetMould();
    err=this->IsDisordered();

    bool nomould = false;
    if(this->m_sList.size()>0  ) {
        if(this->GetpsFlag())
        { int rc=  this->ReMesh();
            this->SetpsFlag(0);
            return rc;
        }
        return 0;
    }
    assert(this->Ep(0) && this->Ep(1) );
    assert(this->Ep(0)->GetN() > 0);		assert(this->Ep(0)->GetN() !=RXSITEINITIAL_N && this->Ep(0)->GetN() !=RXOBJ_INITIAL_N);
    assert( this->Ep(1)->GetN() > 0);		assert( this->Ep(1)->GetN()!=RXSITEINITIAL_N && this->Ep(1)->GetN() !=RXOBJ_INITIAL_N);
    if (this->Ep(0)->m_Site_Flags&NOMOULD )nomould=true;
    if ( this->Ep(1)->m_Site_Flags&NOMOULD )nomould=true;
    if (this->sc->FlagQuery(NOMOULD) )		nomould=true;
    double off1 = this->End1off->EvaluateBySpace(sc,RX_REDSPACE);  // (dec 2008) must measure in redspace
    double off2 = this->End2off->EvaluateBySpace(sc,RX_REDSPACE);
    // walk along the pside creating RXSiteRelSites and joining them up
    //	with edges.  The sites go into pside->sList and the edges into ps->eList.

    vector<double> l_o = this->GetOffsets( );
    vector<double>::size_type i,sz = l_o.size();
    this->m_sList.clear();
    lastPt = this->Ep(0);
    this->m_sList.push_back(make_pair(lastPt,0.0));
    /* The call 'Get Position' with the offset and the pside entity is the same as if in the SC
   if the SC is a CURVE
   But if the SC is a SEAM it will find the black-space position, and then translate it
   by the weighted mean of the red-to-black vectors of the ends of the pside.
    See  Draw_Cut_Curves in panel.c		*/


    for(i=1;i<sz-1;i++) {
        RXSRelSite * pp = new RXSRelSite(sail); // VG reports leak
        pp->SetOName(RXSTRING(L"PS_") + this->GetOName() + RXSTRING(L"_") +TOSTRING(i));
        //this->SetRelationOf(pp,spawn,RXO_SITE_OF_PSIDE ,i); // not spawn  we use the slist to delete it
        this->sc->SetRelationOf(pp,niece,RXO_MESHSITE_OF_SC ,i); // so the fixity gets computed
        if( nomould)
            pp->m_Site_Flags = pp->m_Site_Flags |NOMOULD;
        t = l_o[i] ;
        pp->setDs( t);
        pp->CUrve [0] = this->sc;

        pp->Offsets[0]=dynamic_cast<class RXOffset* > ( pp->AddExpression( new RXOffset(pp->GetOName() ,TOSTRING(t), this->sc,this->Esail) ));

        pp->SetRelationOf(pp->Offsets[0],aunt|frog,RXO_OFF_OF_PS);// LEAK, sould it be spawn??
        Get_Position_By_Double(this, t,1,x); //blackspace
        pp->RXSitePt::Set(x);
        pp->Set(x);

        // to catch fixities, etc.
        pp->ResolveCommon();  // generates the fixity object
        RXFixity *fff =  dynamic_cast< RXFixity *>( pp->GetOneRelativeByIndex(RXO_FIX_OF_SITE));
        if(fff)
        {fff->Finish(); fff->Needs_Finishing=0;}

        if(mould &&!(pp->m_Site_Flags&NOMOULD) ) {
            f = (t-l_o.front()) / (l_o.back()-l_o.front());
            pp->m_u =   this->Ep(0)->m_u *(1.-f) +  this->Ep(1)->m_u *f;
            pp->m_v =   this->Ep(0)->m_v *(1.-f) + this->Ep(1)->m_v *f;
            ON_2dPoint uv= this->Esail ->CoordModelXToUV(pp,&err);// uses tol=10^-10, sets pp->(u,v) to the value found
            if(err)
            {
                ON_String l_buf = ON_String("(pside::MeshOnePside) Find Nearest didnt return OK:  " );
                rxerror(l_buf,1);
            }
        }// if mould
        if(sc->edge)
            pp->m_Site_Flags = pp->m_Site_Flags|PCF_ISONEDGE;
        Trel = (t - off1)/ (off2-off1);
        this->m_sList.push_back(  make_pair(pp,Trel));

        lastPt=pp;
    } // for i
    this->m_sList.push_back(make_pair( this->Ep(1),1.0) );

    //    QString  buf;
    //    buf=QString("Mesh PSide: %1 ep0 is %2, %3 %4 %5 ").arg(this->GetQName()) .arg(Ep(0)->GetN()).arg(Ep(0)->x,20,'f',15).arg ( Ep(0)->y,20,'f',15).arg( Ep(0)->z,20,'f',15);
    //    qDebug ()<<buf;
    //    buf=QString("Mesh PSide: %1 ep1 is %2, %3 %4 %5 ").arg(this->GetQName()) .arg(Ep(1)->GetN()).arg(Ep(1)->x,20,'f',15).arg ( Ep(1)->y,20,'f',15).arg( Ep(1)->z,20,'f',15);
    //    qDebug ()<<buf;
    rc+=this->MakeEdges();
    return rc;
}


double RXPside::LengthUnstressed() const { // just the sum of the zlink0's
    double rv=0;
    class RX_FEedge*ee; int n;
    std::vector<class RX_FEedge*>::const_iterator it;
    for(it = this->m_eList.begin();it!=m_eList.end(); ++it){
        ee = *it;
        rv+=ee->GetLength();
    }
    return rv;
}
/*  the supposition here is that if there is a mould, the redspace shares a coordinate system with it.
 IE, the panel is on the mould and there is a small amount of shape in the pside
*/

int RXPside::UpdateEdges( )  // ONLY call from ReMeshOnePside. Doesnt seem right only to do
{  return 0;// WRONG
    int rc=0;
    class RX_FEedge *fe;
    class RX_FESite *s=0;
    double t0,t1;
    vector<double>::size_type i,sz = this->m_eList.size();
    for(i=0;i<sz;i++) {
        fe = this->m_eList[i];
        t0 = fe->GetLength();
        s = fe->GetNode(0);
        if( s->IsRedSpace()  )
            if( fe->GetNode(1)->IsRedSpace() )
            {
                t1=fe->GetNode(0)->DistanceTo(*(fe->GetNode(1))); //Redspace
                fe->SetLength(t1); rc++;
                if(fabs(t1-t0)> 1e-7)
                    cout<<"Pside edge " << fe->GetN()<< "   changed from "<<t0<<" to "<<t1<<endl;
            }

    } // for i
    return rc;
}

int RXPside::MakeEdges( )  // ONLY call from MeshOnePside
{
    int err;
    // This method relies on the pside's SList::ds values being current FOR THIS PSIDE
    // BEWARE the ::ds value is ephemeral.
    RXEntity_p mould = this->Esail->GetMould();
    sc_ptr sc = (sc_ptr  ) this->sc;
    int side=2;
    if(sc->m_pC[0]->IsValid()) side=0;
    else assert( sc->m_pC[2]->IsValid());

    ON_3dPoint lastx; RXSitePt x;
    class RX_FEedge *fe;
    double t,u,v;
    vector<double>::size_type i,sz = this->m_sList.size();
    for(i=0;i<sz;i++) {
        RX_FESite * pp = this->m_sList[i].first;
        t = pp->getDs();
        Get_Position_By_Double(this, t,side,x); //Redspace

        if(mould&&!(pp->m_Site_Flags&NOMOULD  ) ) { // shift x to the nearest place on the mould, using pp->(u,v) as a seed
            x.m_u =  pp->m_u; 					x.m_v =  pp->m_v;
            ON_2dPoint uv = this->Esail->CoordModelXToUV(&x,&err);
            if(!err)
            {  Evaluate_NURBS(mould,x.m_u,x.m_v,&x);
                ON_3dVector dx = *pp-x;
                if(dx.Length()>0.0001)
                    qDebug()<< "(pside mesh) would move pt"<<pp->GetN()<<dx.x<<dx.y<<dx.z;
                //             pp-> RXSitePt::Set(x); // remove apr 20 2014
                //              pp->Set(x);
            }
            else
            {
                QString l_buf = "(pside::MakeEdges) Find Nearest didnt return OK: " + this->GetQName()
                        +QString("  nodeno=%1 ( %2 %3 %4)").arg( pp->GetN()).arg(pp->x).arg(pp->y).arg(pp->z)
                        +QString("  u=%1,  v=%2").arg(x.m_u).arg(x.m_v);
                rxerror(l_buf,1);
            }
        }// if mould
        if(i) {		// insert an edge from last to pp
            fe = new RX_FEedge(this->Esail);
            //pp->SetOName(RXSTRING(L"spOfPS_") +TOSTRING(i));
            //this->SetRelationOf(pp,spawn,RXO_SITE_OF_PSIDE ,i); we use the slist to delete it
            fe->SetNodes( this->m_sList[i-1].first ,this->m_sList[i].first);
            fe->SetLength( lastx.DistanceTo (x)   ); // we are in redspace
            this->m_eList.push_back(fe);
        }
        lastx=x;
    } // for i

    return this->m_eList.size();
}

vector<int > RXPside::GetEdgeNumbers(const bool ReverseOrder) const
{
    vector<int> rc;
    class RX_FEedge *fe;
    unsigned int i ;
    for(i=0;i<this->m_eList.size();i++)
    {
        fe = ( this->m_eList[i]); 	assert(fe);
        if(ReverseOrder  )
            rc.insert (rc.begin(),fe->GetN()    );
        else
            rc.push_back (fe->GetN());
    } // for i;
    return(rc);
}// GetEdgeNumbers

vector<RXSitePt> RXPside::psGetCurveCoords(const int wh,const bool ReverseOrder) const
/* create nodal positions for meshing routines  wh is side (0,1,2)
This variant uses Get Position. It should handle CUT curves OK  */
/* February 2007  this isnt accurate enough because intersection sites don't get good enough 
values of OFFSET
If we can identify the cases where it's OK to interrogate the end site rather than do a place-on-curve
we should be better off.
 We cant use the endsite if its 'position ' is ambiguous.  This occurs if one of it's curves is a SEAM
  */
{
    vector<RXSitePt> rc;
    RXSitePt *p1, *p2;
    unsigned int i ;
    RXSitePt x;
    RXEntity_p mould = this->Esail->GetMould();
    double l_u,l_v;
    assert(wh>=0 && wh <=2);

    p1 =   this->Ep(0);
    p2 =   this->Ep(1);
    p1->setDs(  this->End1off->Evaluate(sc,wh));  // WRONG to do this here. Its a work-around for tpns.
    p2->setDs(  this->End2off->Evaluate(sc,wh)); // measure in redspace

    if (RXPPRINT>1) printf("GetCurveCoords %s (side %d) from %f to %f\n",this->name(),wh, p1->getDs(),p2->getDs());
    for(i=0;i<this->m_sList.size();i++)
    {
        RX_FESite *p =   ( RX_FESite *) this->m_sList[i].first;
        assert(p);
        /* The call 'Get Position' with the offset and the pside entity is the same as if in the SC
  if the SC is a SEAM
  But if the SC is a CURVE it will find the black-space position, and then translate it
  by the weighted mean of the red-to-black vectors of the ends of the pside.
   See  Draw_Cut_Curves in panel.c
 */


        if(! Get_Position_By_Double(this, p->getDs(),wh,&x)) {
            rxerror(" GPBD failed",8); 	x.x = x.y = x.z =0.0;
        }
        if (RXPPRINT>1)printf(" \t %u %d %f \t( %f %f %f )\n", i, p->GetN(),p->getDs(), x.x,x.y,x.z);

        //  Peter 20 april 2014 WHY setting u and v to red-space coordinates

        if(mould)	 {// now correct using code copied from Compute_Any_Site
            l_u=p->m_u; l_v= p->m_v;
            if(!ON_IsValid(l_u) || !ON_IsValid(l_v)  )
                if(Evaluate_NURBS_By_XYZ(mould,x.x, x.y,x.z,&l_u,&l_v)) {
                    p->m_u = l_u;
                    p->m_v = l_v;
                }
                else {p->m_u = ON_UNSET_VALUE; p->m_v = ON_UNSET_VALUE; }
        } // if mould


        if(ReverseOrder  )
            rc.insert (rc.begin(),RXSitePt(x.x, x.y, x.z ,p->m_u,p->m_v ,p->GetN()));
        else
            rc.push_back (RXSitePt(x.x, x.y, x.z ,p->m_u,p->m_v,p->GetN() ));

    } // for i;

    return(rc);
}
int RXPside::Dump(FILE *fp ) const{
    int i,side,L;
    for(L=0;L<2;L++){
        if(this->leftZone[L]) fprintf(fp,"   leftZone [%d]  %s\n",L,this->leftZone[L]->name());
    }
    for(L=0;L<2;L++){
        if((*this).leftpanel[L]) fprintf(fp,"   leftpanel[%d]  %s\n",L,this->leftpanel[L]->name());
    }
    //for(L=0;L<3;L+=2){
    //  fprintf(fp," material list %d\n",L);
    //  Print_Entity_List(this->m_psmats[L],fp);
    //}
    for(L=0;L<3;L+=2){
        if(this->Mat_Angle[L].flag & 1 )
            fprintf(fp,"  use as ref side  %d angle = %f \n",L,this->Mat_Angle[L].angle  );
        if(this->Mat_Angle[L].flag & 4  ) {
            if(this->Mat_Angle[L].str )
                fprintf(fp,"  use as ref side  %d sc= %s \n",L,this->Mat_Angle[L].str );
            else
                fprintf(fp,"  use as ref side  %d sc= %s \n",L, "NULL" );
        }
    }
    fprintf(fp," end1offset "); if(this->End1off)	this->End1off->Print(fp);
    fprintf(fp," end2offset ");if(this->End2off)	this->End2off->Print(fp);

    if ( this->sc !=NULL) {
        fprintf(fp,"    Curve:    %s\n",this->sc->name());
    }

    Print_End(this->er[0],"e1l",fp);
    Print_End(this->el[0],"e1r",fp);
    Print_End(this->el[1],"e2l",fp);
    Print_End(this->er[1],"e2r",fp);

    if ( this->Ep(0)){

        if (strieq((*this->Ep(0)).type(),"site")) {
            Site *sp = this->Ep(0);
            fprintf(fp,"    e1p Site     %10s x=%10.6f y=%10.6f z=%10.6f\n",this->Ep(0)->name(),
                    (*sp).x,(*sp).y,(*sp).z);
        }
        if (strieq((*this->Ep(0)).type(),"relsite")) {
            Site *sp = this->Ep(0);
            fprintf(fp,"    e1p RELSite  %10s x=%10.6f y=%10.6f z=%10.6f\n",(*this->Ep(0)).name(),
                    (*sp).x,(*sp).y,(*sp).z);
        }
    }
    else cout<< " e1p       NULL"<<endl;
    if (this->Ep(1)){

        if (strieq((*this->Ep(1)).type(),"site")) {
            Site *sp =this->Ep(1);
            fprintf(fp,"    e2p Site     %10s x=%10.6f y=%10.6f z=%10.6f\n",(*this->Ep(1)).name(),
                    (*sp).x,(*sp).y,(*sp).z);
        }
        if (strieq((*this->Ep(1)).type(),"relsite")) {
            Site *sp = this->Ep(1);
            fprintf(fp,"    e2p Relsite  %10s x=%10.6f y=%10.6f z=%10.6f\n",(*this->Ep(1)).name(),
                    (*sp).x,(*sp).y,(*sp).z);
        }
    }
    else cout<< " e2p       NULL"<<endl;

    for(side=0;side<3;side++) {
        ON_3dPoint v;

        if(sc->m_pC[side]->HasGeometry()) {
            this->Get_Position(this->End1off,side,v); /* Was o not p */
            fprintf(fp,"  side %4d  %f %f %f\n",side,v.x,v.y,v.z);
            this->Get_Position( this->End2off,side,v);
            fprintf(fp,"  side %4d  %f %f %f\n",side,v.x,v.y,v.z);
        }
    }

    fprintf(fp," end1off \t%f\n", (this->End1off->Evaluate(sc ,0)));
    fprintf(fp," end2off \t%f\n", (this->End2off->Evaluate(sc ,0)));

    for(i=0;i<this->m_sList.size();i++) {
        RX_FESite *pt = this->m_sList[i].first;
        fprintf(fp," %d (%f) %S\n",i,this->m_sList[i].second,  pt->TellMeAbout ().c_str());
    }

    for(i=0;i<this->m_eList.size();i++) {
        RX_FEedge *ee = this->m_eList[i];
        fprintf(fp," %d %S\n",i,ee->TellMeAbout().c_str());
    }
    return 1;
}
// RXPside::ClearSlist has been superceded by the Object dependencies
int RXPside::ClearSlist(SAIL *sail )
{
    int i;
    RXSRelSite *temp;
    RX_FESite  *s1;
    int count = this->m_sList.size();

    for(i=1;i<(count-1);i++)
    {
        if(temp=dynamic_cast<RXSRelSite*>( this->m_sList[i].first)) {
            assert(temp);

        }
        else
            cout<<"ClearSlist, doesnt cast to RXSRelSite"<<endl;
    }
    this->m_sList.clear ();

    return 0;
}

int  RXPside::ClearElist(SAIL *sail)
{ 
    int i;
    for(i=0;i< this->m_eList.size();i++)
    {
#ifndef NEVER
        class RX_FEedge *e =  this->m_eList[i];
        delete e;
#else
        this->m_eList[i]->ClearFEdge(sail);
        sail->EdgePush(this->m_eList[i]);
#endif
    }
    this->m_eList.clear ();
    return(0);
}
int RXPside::DrawDeflected(HC_KEY p_k){
    ON_3dPoint  x;

    vector<pair <class RX_FESite*,double> >::const_iterator it;
#ifdef HOOPS
    HC_Open_Segment(this->name()); HC_Flush_Segment(".");
    HC_Restart_Ink();
    for(it = this->m_sList.begin(); it!= this->m_sList .end(); ++it) {
        x = (*it).first->DeflectedPos();
        HC_Insert_Ink(x.x,x.y,x.z);
    }
    HC_Close_Segment();
#elif defined (RXQT)
    const char*color="blue";
    int count = this->m_sList.size();
    float *poly =new float[3*count];
    float *lp =poly;
    for(it = this->m_sList.begin(); it!= this->m_sList .end(); ++it) {
        x = (*it).first->DeflectedPos();
        *(lp++)=x.x; *(lp++)=x.y; *(lp++)=x.z;
    }
    RX_Insert_Polyline(count,poly,p_k,  color);
    delete[] poly;
#endif
    return (int) this->m_sList.size();
}
//************************************
// Method:    Remove_From_Node_Lists
// FullName:  RXPside::Remove_From_Node_Lists
// Access:    protected 
// Returns:   int
// Qualifier:
//************************************
int RXPside::Remove_From_Node_Lists()  {

    /*  For each end of entity ep,
      check the pslist
      remove the pside if it is there*/

    Site* se;
    int rc=0;

    for (int k=0;k<2;k++) {
        se = this->Ep(k); if(!se) continue;
        rc+=se->RemoveOnePside(this);
        this->m_ep[k]=0;
    }
    return(rc);
}
int RXPside::Get_Position(RXOffset *const offin,int side,ON_3dPoint &pt)const {

    /* the entity may be a SITE or RELSITE
 or it may be a SC, in which case <offset>  and <side> are needed*/
    /* It may be a seam, in which case <side>= 0,1,or 2 for (left, black, right)
   If its a PSIDE, offset refers to its SC.
   If the SC is a Seam thats fine.
   If the SC is a  Curve, there is a special meaning for sides 0 and 2;
   They are curve 1, but displaced by a weighted mean of the endsites of the pside.
*/

    double OffsetValue;

    if(this->Needs_Resolving)
        return(0);

    if(this->Needs_Finishing)
        return(0);
    assert (this->TYPE==PCE_PSIDE) ;

    OffsetValue=(offin->Evaluate(this->sc,side));

    return (Get_Position_By_Double ((RXEntity_p) this,OffsetValue, side,pt));
}
int RXPside::Add_To_EndNode_Lists( ) const {

    /*  For each end of the pside p,
      check the pslist
      add the pside if it is not already there */
    int k;

    for (k=0;k<2;k++) {
        Site *se = this->Ep(k);
        se->AddOnePside(this,k);
    }
    return(1);
}

/***********************************************/
int  RXPside::IsSpecial ( RXEntity_p EndSCO[2] ) const {

    /* returns 1 if end1 is special, plus  2 if end2 is special
  special means that the end is a CROSS site and the other SC of the cross
  is a seam.
 There are two cases;
  1) The PS owner is one of the cross SCs . If the other cross SC is a seam, count it;
  2)  The PS owner is neither cross SC. In this case we base the offset on curve 1 */


    RXEntity_p se;
    sc_ptr sc2;
    Site *s;
    int end,i, retval=0;

    for(end=0;end<2;end++) {
        EndSCO[end] =NULL;
        se = this->Ep(end);
        s = (Site *)se; assert(dynamic_cast<Site *>(se )  );
        if(!s->CUrve[0] || !s->CUrve[1] ) continue;

        /* Case 1. */
        for(i=0;i<2;i++) {
            if(s->CUrve[i] == this->sc) {
                sc2 = (sc_ptr  )s->CUrve[1-i];
                if(sc2->seam) {
                    retval = retval + (end+1);
                    EndSCO[end]  = sc2;
                    /*  (" studying PS %s got CASE 1 SC %s at end %d\n", ps->owner->name(), sc2->owner->name(), end); */
                }
            }
        }

        /* case 2  we get here if neither curve is ps->sc */

        sc2 = (sc_ptr  )s->CUrve[1];
        if( (s->CUrve[0] != this->sc)&& (s->CUrve[1] != this->sc) &&(sc2->seam)) {
            retval = retval + (end+1);
            EndSCO[end]  = sc2;
            printf(" studying PS %s got CASE 2 SC %s at end %d\n", this->name(), sc2->name(), end);
        }

    }
    return retval;
} 
int RXPside::Find_Special_Side(  RXEntity_p sco2,RXEntity_p se) const {
    /* sco2 is a SEAM that crosses ps at SITE  se .

 */
  //  const class RXPside * ps=this;
    sc_ptr sc;
    Site *s;
    ON_3dPoint v; ON_3dVector t[2],l_z;
    double off,z;
    int i;
    s = (Site *)se;
    for(i=0;i<2;i++) {
        sc = (sc_ptr  )s->CUrve[i];
        off = (s->Offsets[i]->Evaluate(sc,1));
        sc->m_pC[1]->Find_Tangent( off, &v, &(t[i]));
        t[i].Unitize ();
    }
    v = ON_CrossProduct(t[0],t[1]);

    l_z =se ->Normal(); // MAYBE  ps or sco2
    z= ON_DotProduct(v,l_z);

    if(sco2 == s->CUrve[0] ) { z=-z;}

    if(se == this->Ep(0)) { z=-z;}

    return (z >0);
}
