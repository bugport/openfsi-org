#pragma once
#include "opennurbs.h"
class CTensylProp
{
public:
	CTensylProp(void);
	CTensylProp(int p_p,double p_e,double p_sw,double p_rs);
	~CTensylProp(void);
	int Print(FILE *fp,const double p_scale);
	int m_prop_no;
	double m_ea;			//lb force
	double m_SelfWeight;   // lb/inch
	double m_RenderSize;
	ON_String s1,s2;
};
