#pragma once

//these objects are stored in g_World->m_tol_list

#include <list>
#include <string>

#define SCT_DISABLED  	0
#define SCT_UNITS  	'u'
#define SCT_PERCENT 	'%'

class RXSummaryTolerance
{
public:
	RXSummaryTolerance(const string &a, const double &b, const string &c);
	~RXSummaryTolerance(void);

	int HasNotChanged(const char*buf);
	std::string GetHeader() const {return m_header;}
	int FlushHistory();
        static QString  m_logtext;

protected:
	std::string m_header;
	double tol;
	char Tol_Flag;
	double value;
	std::list<double> m_oldvals;


};

