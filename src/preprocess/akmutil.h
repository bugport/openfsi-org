#ifndef _HDR_AKMUTIL_
#define _HDR_AKMUTIL_ 1
#include <QString>


int is_directory(const char *path);
EXTERN_C int Increment_String (QString &value);
EXTERN_C int VerifyFileName( QString fn,const QString prompt,const QString pattern);
int validfile(const char *fname, const char* dir =0);
int validfile(const std::string &fname, const char* dir =0);
int validfile(const QString &fname, const char* dir =0);
EXTERN_C void akm_just_update_display_();

EXTERN_C void AKM_FREE(void *s);
EXTERN_C void akm_insert_ink_(float *x,float *y,float *z);
EXTERN_C int akm_insert_marker_(float *x1, float *Y1, float *z1);
EXTERN_C int akm_restart_ink_(void);
EXTERN_C void akm_open_segment_(const char *name);
EXTERN_C void akm_close_segment_(void );
EXTERN_C int akm_delete_segment_(const char *seg);
EXTERN_C int akm_insert_line_(float *a,float *b,float *c,float *d,float *e,float *f,RXGRAPHICSEGMENT *k, double*h,double*s,double*v);
EXTERN_C int akm_set_color_(const char *col);
 
EXTERN_C void AKM_Insert_Multiline_Text(float x,float y, float z, const char *s_in);
extern int Akm_Set_Color_By_Value(const char*what,const char*type,const float a,const float b,const float c);// see hoopsinterface.f90
extern int Akm_Set_Color_By_Value(const char*what,const char*type,float * a,float*b,float *c);

EXTERN_C  int AKM_copy_file(char *s,char *d);


#define OK_TO_READ  1
#define OK_TO_WRITE  0
#define BAD_FILENAME  -1
#endif
