// layerobject.cpp: implementation of the layerobject class.
//
//////////////////////////////////////////////////////////////////////
 
#include "StdAfx.h"
#include "RXGraphicCalls.h"

#include "layerobject.h"
#include "layertst.h"
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

layerobject::layerobject()
{
	m_flag=0;
	m_n=-1;
	m_pRefAxis=0;
	m_lt=0;
	m_tz=0;
	m_hkey=0;
}

layerobject::~layerobject()
{
	m_n=-99;
	if(m_pRefAxis) 
		delete m_pRefAxis;
	m_pRefAxis=0;
	m_lt=0;
	m_tz=0;
#ifndef RHINO_PLUGIN
	if(m_hkey)
		HC_Delete_By_Key(m_hkey);
#endif
	m_hkey=0;
}

void layerobject::FlagSet  (const int p) {
	m_flag = m_flag| p;
}

void layerobject::FlagClear(const int p) {
	int j;
	j=m_flag&p; 
	m_flag = m_flag - j; 
}
 
int layerobject::FlagQuery(const int p) const{return(m_flag &p); }  

int layerobject::FlagClear(){
	int i = m_flag; m_flag=0; 
	return i; 
} 

int layerobject::FlagGetAll() const
{
 return m_flag;
}

ON_String layerobject::Serialize()// allocates and returns a ON_String image of the object
{
	ON_String s, rc = ON_String("layerobject:") ;
	s.Format(" %d\t flags =%d \t",m_n,m_flag);
	rc+= s;
	if(m_pRefAxis)
		s.Format(" %f\t %f\n",m_pRefAxis->x, m_pRefAxis->y);
	else
		s.Format(" %s\n","NULL");
	rc+= s;

// because the class is virtual we don't put a record terminator (char(27) ) on the end
return(rc );
}

char * layerobject::Deserialize(char *lp)// Fills out the object from p_s. returns the ptr to the next char
{
	int k,nc;
	double x=-99.,y=-99;
	k=sscanf(lp,"layerobject: %d\t flags =%d  \t%n",&m_n,&m_flag,&nc); 
	if(!k) 
			k=sscanf(lp," layerobject: %d\t flags =%d  \t%n",&m_n,&m_flag,&nc);

	if(!k) { 
		cout<< " layerobjectcant deserialize\n"<<endl;
		return 0;
	}


	lp+=nc;
	k=sscanf(lp," %lf\t %lf \t%n",&x,&y,&nc);
	if(!k) {
		k=sscanf(lp," NULL\t%n",&nc);  //Jan 3 I don't think we want the space in front of NULL
		m_pRefAxis=0;
	}
	else
		m_pRefAxis=new ON_2dVector(x,y);
	lp+=nc;


return lp;
}
#if !defined(RHINO_PLUGIN) && (defined(RX_USE_DOVE)||defined(AMGTAPER) )
ON_2dVector layerobject::GetRefVector()  // in layertst the method is overridden to stop the recursion.
{
	if(m_pRefAxis)
		return ( ON_2dVector(*m_pRefAxis));

	return ( ON_2dVector(m_lt->GetRefVector()));
}

int layerobject::SetRefAxis(ON_2dVector p_v)
{
	m_pRefAxis=new ON_2dVector(p_v);
	return 1;
}
#endif

int layerobject::Print(FILE *fp) 
{
	ON_String s = Serialize();
	return fprintf(fp," %s\n", s.Array());
}
