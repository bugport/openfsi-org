#ifndef RXPSHELL_H
#define RXPSHELL_H
#include "RXPanel.h"

class rxpshell : public RXPanel
{
public:
    rxpshell();
    rxpshell(SAIL *sail);
    ~rxpshell();
     int BuildTriMaterials();
public:
    double BuildOneTriMaterial(const int i);
};

#endif // RXPSHELL_H
