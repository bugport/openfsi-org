// RXTRObject.h: interface for the RXTRObject class.
//
//////////////////////////////////////////////////////////////////////

#ifndef RXTRObject_15NOV04
#define RXTRObject_15NOV04

#include <iostream>
#include <map>
using namespace std;

#ifdef __cplusplus

#include "opennurbs.h"

	#ifndef RXTRObjectCLASSID 
		#define RXTRObjectCLASSID "D2208343-C129-4171-A047-30BBF786C7B4" 
	#endif  //#ifndef RXTRObjectCLASSID 

	class RelaxWorld;


	/** @brief Base class for thomas' various classes RXCurve, etc

	


	*/

class RXTRObject
	{
	public:
		RXTRObject();
		RXTRObject(const RXTRObject & p_Obj);

		virtual ~RXTRObject();
		void Init();
		void Clear_SSD();
		RXTRObject& operator = (const RXTRObject & p_Obj);
		
        RXSTRING GetTROName() const;
		void SetTROName(const RXSTRING & p_Name);

		virtual char * GetClassID() const; 
		virtual int IsKindOf(const char * p_ClassID) const;

		static RXTRObject * Cast( RXTRObject* p_pObj); 
		static const RXTRObject * Cast( const RXTRObject* p_pObj);
	private:
		RXSTRING m_Name;
	};

#endif //#ifdef __cplusplus

#endif // #ifndef RXTRObject_15NOV04
