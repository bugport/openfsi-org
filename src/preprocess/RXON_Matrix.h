#ifndef _RXON_MATRIX_H_
#define _RXON_MATRIX_H_
#include "opennurbs.h"

class RXON_Matrix :
	public ON_Matrix
{
public:
	RXON_Matrix(void);
	RXON_Matrix(const int i, const int j);
	//RXON_Matrix(const ON_Matrix p_m ); this gives trouble
	ON_String Serialize();
	char * Deserialize(char*lp);
	int Print(FILE *fp);
public:
	~RXON_Matrix(void);
};
#endif
