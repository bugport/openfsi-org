#pragma once

#include "RX_FEObject.h"
#include "RX_FEEdge.h"
#include <vector>
using namespace std;


struct FEEDEGEREF {
 class RX_FEedge*e;
int rev;
}; //struct  FEEDEGEREF
typedef struct FEEDEGEREF FEEdgeRef;



class RX_FELinearObject :
	public RX_FEObject
{
public:
	RX_FELinearObject(void);
	virtual ~RX_FELinearObject(void);
	int CClear();
        std::vector<int> GetNodeList();
protected:
	int CountPsideEdges(double*p_length, PSIDEPTR *pslist,int npss) ;
	int SortPsideEdges(void);
	int SortPsideEdgesWithSeed(RXEntity_p seedsite);
	int printr (FEEdgeRef &r) ;
	int printelist(vector<FEEdgeRef> &p_elist) ;
	int ClearElist();

	vector<FEEdgeRef> m_elist;

};
