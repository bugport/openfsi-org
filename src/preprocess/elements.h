#ifndef _HDR_ELEMENTS_
#define _HDR_ELEMENTS_ 1


/* header for boat element data types
   u1,v1,u2,v2 for STRIPEDATA added
 */
 #ifdef linux
	#define _NOT_MSW
#endif

struct STRIPEDATA {
	int FULLY_DEFINED;
	int n;	/* id no. */
	char *name;	/* name */
	char *color;	/* color */
	char *type;	/* u or v  or "between"*/
	double *val;
	int step;
	double u1,v1,u2,v2;
		
	};
typedef struct STRIPEDATA StripeData;


#endif  //#ifndef _HDR_ELEMENTS_

