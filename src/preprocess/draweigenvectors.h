#ifndef _DRAW_EV_H__
#define  _DRAW_EV_H__

#define RX_EVS	1
#define RX_UPS	2
#define RX_EV_TENSORFORM	4
#ifdef OPENNURBS_INC_
extern ON_3dVector Principle_Stress(ON_3dVector &p_stress);
EXTERN_C double RXE_PrincipleAngle(ON_4dPoint s);
EXTERN_C int RXM_EigenSolution3x3 (const double *d, ON_4dPointArray &evec) ; // return value is ERR not OK

EXTERN_C HC_KEY RXM_Draw_One_Stress_Vector(const double length, const double angle,const ON_3dPoint &q,const ON_Xform&xf);
#endif

/// flag may be  RX_UPS for up stiff or RX_EVS (anded or not with RX_EV_TENSORFORM)
EXTERN_C HC_KEY  Draw_Material_Eigenvectors(SAIL *p_sail,const int flag);


#endif

