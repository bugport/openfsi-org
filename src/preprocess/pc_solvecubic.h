#ifndef _PC_SOLVECUBIC_H_
#define _PC_SOLVECUBIC_H_ 


#include "opennurbs.h"

extern int PF_solvecubic(double *q,double *r,double *s,double *t,double *p_r1,double *p_r2,double *p_r3); 

extern double PCON_Dot( double *pa, double *pb, const int n) ;

extern int PCON_Dot( ON_Matrix &a, ON_SimpleArray<double> &y, ON_SimpleArray<double>*ret);
EXTERN_C int PC_SolveOverdetermined(ON_SimpleArray<double> &c, ON_Matrix &a, ON_SimpleArray<double> *x) ; 

#endif

