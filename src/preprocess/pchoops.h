/* DATE:  Tuesday 26 January 1993
file : PCHOOPS.H 
Starting a file for PC_ hoops lookalike routines */

#ifndef PHOOPS_16NOV04
#define PHOOPS_16NOV04

#error(" dont use  pchoops.h")
EXTERN_C int PC_Edit_String_By_Key(HC_KEY tkey);

EXTERN_C void PC_Show_One_Net_User_Option(char *spec, char *value);
/* this is necessary because HC_Show_One_User_Option does not
search the style segments affecting ".", whereas HC_Show_User_Options() does. */


EXTERN_C int PC_Set_Camera_By_Volume(char *, float ,float ,float ,float );

EXTERN_C int PC_Show_Camera_By_Volume(char *, float *,float *,float *,float *);

EXTERN_C int PC_QSet_Camera_By_Volume(char *,char *, float ,float ,float ,float );


#endif //#ifndef PHOOPS_16NOV04
