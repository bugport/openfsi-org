#pragma once
#include "opennurbs.h"

#if !defined(  RXPHYSX) && !defined(RXRHINOPLUGIN )
#include "RXSRelSite.h"
#endif
class CTensylNode
{
public:
	CTensylNode(void);
	CTensylNode(int p_nn,ON_3dPoint p_pt, bool p_isbdy,int  p_fixity,int p_field,int p_AxSymm);
	~CTensylNode(void);
	int Print(FILE *fp=stdout);
	int m_nn;
	ON_3dPoint m_pt;
	bool m_isbdy;
	int m_fixity;
	int m_field;
	int m_AxSym;
	ON_wString m_atts;
#if ! defined( RXRHINOPLUGIN) && !defined(RXPHYSX )
	RXSRelSite *m_e;
#else
	void *m_e;
	double m_t;
#endif
};

