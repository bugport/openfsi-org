
/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
************************************************************************************
 *Element materials

The Mat_xxx functions have argument p_Tri but its only Mat_Filament which uses it.
Filaments are in blackspace.  Does P_Tri have vertices and are they in blackspace??

Mat_Filament uses p_Tri->m_panel and p_tri->Vertices() . p_Tri is passed to fld::m_FEtri
Then m_FE_Tri is

is referenced:  
to get the area
for filling out the tri's filament list. 
Now we find the direction of each filament in the elemnt's ref material  Xform,

A note about XForms.  
Post-multiplying ( v2 = X . V )  converts V in global to V2 in Xform.
Pre-multiplying ( v2 =V . X   )  converts V in XForm  to V2 in global..
***************************************************************************************
 *
 *
 * Modified :
  1/8/98  the REVERSED flag
    4/4/97	  The Field may now be radial as well. So it can be used for patches
    It would be better still if Fields could have limited scope.
     28/3/97 A geometric bug in fields. A point could be outside the field. FIXED
  There were three issues
  1) PC Drop Perp returned the penultimate point
  2) The tolerance was too loos at 2%
  3)  the test f between o and 1 isnt enough.
   needs an additional test d1<w, d2<w

 *  4/9/96   PC_FIELD changed as a preliminary to the double-curve field
 9/3/96 way of using offsets is standardised
 * 12/1/96 chage to Compute SC Trigraph call
 *  7/2/95       S value now iterated so no arc/chord=1 approximation
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

/* OpenFSI.org
 tracing the use of the pointer-to-tri passed into the MatFuncPtr functions
Mat_Field   - passes it to compute_filament_field  and to the never used  Compute_NURBS_ALPHA_Field

Compute_Filament_Field passes it the the FLD
its area is read
its m_refangle is read
its vertices are read to an array of 4 vectors, via Tri_Vertices.
RXTriangle has a constructor which copies its vertices.
its an argument to PCF_PushToTriFilamentList, which just adds it to its linklist 'fils'


Mat_Radial  doesnt use it
Mat_Parallel  doesnt use it
Mat_Uniform   doesnt use it
Mat_Curve doesnt use it
Compute_NURBS_ALPHA_Field uses it via Tri_Vertices

*/


#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXSeamcurve.h"
#include "RX_FETri3.h"
#include "RXLayer.h"
#include "RXPanel.h"
#include "RXTriangle.h"
#include "RXdovefile.h"
#include "RXRelationDefs.h"

#define MDB (0)
#ifdef _OPENMP
#include <omp.h>
#endif

#include "RXCurve.h"

#include "filament.h"
//#include "panel.h"
#include "etypes.h"

#include "slowcut.h"
#include "fielddef.h"

#include "global_declarations.h"


#include "peterfab.h"

int Compute_TwoCurve_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside){

    /*
   1)drop perpendiculars to the two curves.
  2) get the factor f assigning the proportion to 1 or 2
  2) get  direction depending on keyword Type
     from q drop a perp to s1 (p1,d1 ) and to s2	(p2,d2)
     the ratio is (d1/(d1+d2))
  */
 // Jan 2014   we allow sc1 == sc2 in which case
 //   thickness is 1 and direction is the tangent at the nearest pt
    ON_3dVector t1,t2,p1p2,p1q, ref;
    RXEntity_p sco1, sco2;
    sc_t * sc1, *sc2;
    ON_3dPoint p1,p2;
    ON_3dPoint ev1, ev2;
    double width,s1,s2,d1,d2,dot,tol,rev;
    int errflag;
    if (field->flag&REVERSED) rev=-1.0; else rev=1.0;

    sco1 = field->sc1;
    if(!sco1) {field->mat->OutputToClient("incomplete FIELD spec. Missing curve 1",1); return 0;}
    sc1 = (sc_ptr  )sco1;
    sco2 = field->sc2;
    if(!sco2) { field->mat->OutputToClient("incomplete FIELD spec. Missing curve 2 (or RADIAL?)",1); return 0;}
    sc2 = (sc_ptr  )sco2;
    tol = max(sc1->GetArc(1), sc2->GetArc(1))/200.0 ;

    ev1=sc1->Get_End_Position(SC_START);
    ev2=sc1->Get_End_Position(SC_END);
    s1 = q->DistanceTo (ON_3dPoint(ev1));
    s2 = q->DistanceTo (ON_3dPoint(ev2));
    s1=  s1/(s1+s2) *  sc1->GetArc(1);
    errflag=sc1->m_pC[1]->Drop_To_Curve( q->ToONPoint (),&p1,&t1, tol,&s1,&d1);
    if(errflag <=0 )
        return 0;
    s2 =  s1 *sc2->GetArc(1)/ sc1->GetArc(1);
    errflag=sc2->m_pC[1]->Drop_To_Curve(q->ToONPoint (),&p2,&t2, tol,&s2,&d2); //  -1 error  1 converged, 0 unconverged
    if(errflag<=0 )
        return 0;
    /* factor f = (P1Q dot p1p2) / (	|p1p2|^2 |)	 */

    p1q = q->ToONPoint () -p1;
    p1p2 =  p2-p1;
    width = p1.DistanceTo (p2);
    if(width < 0.0001) width = 0.0001;
    if(g_ArgDebug) printf(" width %f\n", width);
    dot = ON_DotProduct(p1q, p1p2);
    *f = dot/width/width;
    *inside = (*f <=1.0 && *f >=0.0 && d1<=width &&d2 <=width);

    ref.x = t1.x*(float)(1.0-(*f)) + t2.x * (float)((*f)* rev);
    ref.y = t1.y*(float)(1.0-(*f)) + t2.y * (float)((*f)* rev);
    ref.z = t1.z*(float)(1.0-(*f)) + t2.z * (float)((*f)* rev);

    bool ok = ref.Unitize (); assert(ok);
    for(int k=0;k<3;k++) xv[k]=ref[k];

    *thickness = 1.0/width;

    return 1;
}

int Compute_OneCurve_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside){

    /*
   1)drop perpendiculars to the two curves.
  2) get the factor f assigning the proportion to 1 or 2
  2) get  direction depending on keyword Type
     from q drop a perp to s1 (p1,d1 ) and to s2	(p2,d2)
     the ratio is (d1/(d1+d2))
  */
 // Jan 2014   we allow sc1 == sc2 in which case
 //   thickness is 1 and direction is the tangent at the nearest pt
    ON_3dVector t1;
    RXEntity_p sco1, sco2;
    sc_t * sc1 ;
    ON_3dPoint p1 ;
    ON_3dPoint ev1, ev2;
    double s1,s2,d1,tol,rev;
    int errflag;
    if (field->flag&REVERSED) rev=-1.0; else rev=1.0;

    sco1 = field->sc1;    sc1 = (sc_ptr  )sco1;
    sco2 = field->sc2;    assert(sco1 && sco1==sco2);
    tol = sc1->GetArc(1)/200.0 ;

    ev1=sc1->Get_End_Position(SC_START);    s1 = q->DistanceTo ( ev1);
    ev2=sc1->Get_End_Position(SC_END);      s2 = q->DistanceTo ( ev2);

    s1=  s1/(s1+s2) *  sc1->GetArc(1); // starting guess
    errflag=sc1->m_pC[1]->Drop_To_Curve( q->ToONPoint (),&p1,&t1, tol,&s1,&d1);
    if(errflag <=0 )
        return 0;

    for(int k=0;k<3;k++) xv[k]=t1[k];

    *thickness = 1.0;

    return 1;
}

int Compute_NURBS_ALPHA_Field(RXEntity_p p_fe,double *thickness,class RX_FETri3 *p_t,MatDataType &xv,double*mx, double *f, int*inside,double ps[3])
{
    // evaluate the NURBS (see Resolve_Field) at the (u,v) values for the triangle
    // the z component is the alpha angle (? degrees/rads)
    // evaluate the mould dr/du and normal vectors.
    // rotate drdu by alpha.  This is the material ref vector.


    p_fe->OutputToClient  ("Compute_NURBS_ALPHA_Field in development",5);
    assert(0);

    int ii1=0;
    double *mp;
    const RXSitePt *l_v[4]; // WIERD
    RXTriangle l_TriAngle;
    p_t->Vertices (l_v);
    //A Bit of an Initialisation (maybe some is not necessary)

    assert(sizeof(MatValType)==sizeof(double));

    mp=mx;
    memset(mp,0,9*sizeof(double));
    mp = ps;
    memset(mp,0,3*sizeof(double));


    return 0;
}//int Compute_NURBS_ALPHA_Field 


int Compute_Field_Dove(RXEntity_p p_fe,double *thickness,const class RXSitePt *q,MatDataType &xv,double*mx, double *f, int*inside,double ps[3])
{

    //The field may later contain a mask restricting the search .


    //SAIL *l_sail = p_fe->Esail;
    struct PC_FIELD *l_f = ( struct PC_FIELD *) p_fe->dataptr;
    RXEntity_p l_de = l_f->mat;
    RXdovefile *theDove = (RXdovefile *)l_de->dataptr ;
    ON_Matrix l_m=ON_Matrix(3,3);
    ON_3dVector l_x;
    double *mp;
    int i,j,retval=1;
    assert(sizeof(MatValType)==sizeof(double));

    mp = ps;
    memset(mp,0,3*sizeof(double));
#ifdef RX_USE_DOVE
    if(theDove->Evaluate(q->m_u,q->m_v,l_m,l_x)) {
        mp=mx;
        for(i=0; i<3;i++) {
            for(j=0; j<3;j++) {
                if(!_finite(l_m[i][j])) {
                    l_m[i][j]=0;
                    printf(" bad dove matrix at\n(xyz) %f %f %f (i,j) %d %d (u,v) %g %g\n",q->x,q->y,q->z, i,j,q->m_u,q->m_v);
                    retval=0;
                }
                *mp = l_m[i][j];
                mp++;
            }
        }
        xv[0]=l_x.x; xv[1]=l_x.y; xv[2]=l_x.z;
        //printf("at\t%f\t%f\t%f\t ref vector is\t %f\t%f\t%f\n",q->x,q->y,q->z,l_x.x,l_x.y,l_x.z );
        return retval;
    }
    else {
        printf(" DoveEvaluate failed at %f %f\n",q->m_u,q->m_v);
        memset(mx,0,9*sizeof(double));
        xv[0]=1; xv[1]=0; xv[2]=0;
    }
#endif

    return 0;
}//int Compute_Dove_Field 


MatValType *Mat_Filament(double *thickness,double *mx,const class RXSitePt *q,class RX_FETri3 *p_Tri,class RXLayer *p,MatDataType &p_xv,double ps[3])
{
    // Find the value of the material at given point
    // this model returns the 3by3 on its own
    //  the material ref vector (black space) is  the first 3 in XV for this functionn it is an OUTPUT, like tha other Mat_XX

    /*
The field may later contain a mask restricting the search for filaments.
For now, find all SCs with flag&filament 
Find intersections with the triangle boundary.
*/
    assert(sizeof(MatValType)==sizeof(double));
    RXEntity_p fe = p->matptr;  // the 'field' entity
    SAIL *l_sail = fe->Esail;
    rxON_BoundingBox l_bbt,l_bbc;
    RXEntity_p  l_pSco = NULL;
    sc_ptr l_pSc = NULL;

    class FILAMENT_LOCAL_DATA l_fld;
    double *mp;

    //A Bit of an Initialisation (maybe some is not necessary)
    mp=l_fld.DD_Tot=mx;			memset(mp,0,9*sizeof(double));
    mp = l_fld.ps=ps;			memset(mp,0,3*sizeof(double));
    *thickness = 1.0;


    l_fld.m_FE_Tri=p_Tri;
    p_Tri->Vertices (l_fld.v);

    l_fld.m_MatRefTrigraph = p_Tri->MaterialTrigraph (); // postmult converts (redorblack) to material axes.

    ON_3dVector a1(1,0,0),a2;
    a2 = a1* l_fld.m_MatRefTrigraph;
    p_xv[0]=a2.x; p_xv[1]=a2.y; p_xv[2]=a2.z;

    int l_MeshIn3D =  RX_MESH_3D; //Set l_MeshIn3D to RX_MESH_XYPLANE to mesh in the XY plane
    double l_EpsDot = 1.0e-6;


    l_fld.m_rxt.SetAllVertices(&l_fld);
    l_fld.m_rxt.GetTightBoundingBox(l_bbt); // this is really a GetVanillaBB not tight
    l_bbt.GrowAllRound (l_sail-> m_Linear_Tol );

    // it's overdoing it to have all of  this a critical section  but let's try to reduce it progressively

    RXPanel*ppp = p_Tri->m_panel;
    set<RXEntity_p>::iterator it;
#ifndef DEBUG
#pragma omp critical(filmaterials)	
#endif
    {
        for(it = ppp->m_fils.begin ();it!= ppp->m_fils.end();++it) { // we parallelize doing one panel per thread

            l_pSco  = ( *it);
            l_pSc = (sc_ptr  )l_pSco;
            rxON_BoundingBox l_bbtemp;
            if(l_pSc->m_pC[1]->GetTightBoundingBox(l_bbc,0,0)) {
                if(!l_bbtemp.Intersection(l_bbc,l_bbt))  { //a change in feb 2010 to verify
                    continue;
                }
                if(l_bbt.IsDisjoint(l_bbc)){
                    continue;
                }

            }


            // to parallelize this it's probably smarter to test all the sc's in parallel and push any intersections onto a local list, then once we've got the list
            // we do a create_filament_matrix on the list.

            int l_ret = l_fld.m_rxt.Create_Filament_Matrix(l_pSc,&l_fld, l_sail->m_Linear_Tol,  l_sail->m_Cut_Dist , l_EpsDot, l_MeshIn3D);

            //BEFORE the 7th OCt 2004 Create_Filament_Matrix() was ALLWay retruning 1
            //Thomas changes it to return RX_ELT_SEES_FILAMENT if the element can see the filament
            //					or return RX_ELT_CANNOT_SEES_FILAMENT if the element CANNOT see the filament
            if (l_ret==RX_ELT_SEES_FILAMENT)
            {
                l_pSc->SetRelationOf( fe,child,RXO_FLD_OF_SC);;
            }
        }//for  it
    }

    return mx;
}// Mat_Filament was Compute_Filament_Field 

int Compute_Radial_Field(struct PC_FIELD*field,double *thickness,const class RXSitePt *q,MatDataType &xv, double *f, int*inside)
{
    /* draw a line from focus to q
    Find an intersection with the SC curve
  If found, the factor t is 0 at focus, 1 at the SC
  We are interested in factor < 1.  In this case we
  add a layer, direction from focus to q,
  thickess according to the attributes of the Field
      */
    ON_3dVector p2q, ref; ON_3dPoint v1,v2  ;
    RXEntity_p sco1;
    sc_ptr  sc1;
    Site *focus;
    double s1,t;//width,s2,tol,d1,d2,dot,
    int idum;
    focus =  (Site *)field->focus;
    sco1 = field->sc1;
    if(!sco1) { assert(field->focus);field->focus->OutputToClient ("incomplete Radial FIELD spec. No curve 1",2); return 0;}
    sc1 = (sc_ptr  )sco1;

    s1=sc1->GetArc(1)/2.;

    ON_3dPoint vf =   focus->ToONPoint ();
    ON_3dPoint qv; assert("Compute_Radial_Field "==0);
    if(sc1->m_pC[1]->Curve_To_Line_Intersection(&s1,&v1,&v2,&vf,&qv,&t, &idum,(EXT_1_L+EXT_1_R+EXT_2_R))){
        // we are t from focus to q.  and s1 along sc1.
        assert("Compute_Radial_Field again "==0);	//q->Set(ON_3dPoint(qv.x,qv.y,qv.z) );
        if(t<=0.0) t=0.0001;
        *f =1.0/t;
        *inside = (*f <=1.0 && *f >=0.0);

        ON_3dVector p2qon   = (*focus-*q); cout<< "TODO:step this"<<endl;
        ref = p2q; if(ref.LengthSquared()>ON_EPSILON){ //  if(HC Compute_Normalized_Vector(&p2q,&ref)){
            xv[0]=ref.x; xv[1]=ref.y; xv[2]=ref.z;
        }
        else
            cout<< " cant do radial field at focus"<<endl;

        *thickness = 1.0;

        return 1;
    } // if intersect
    *thickness=0.;
    return 0;

}




MatValType *Mat_Field(double *thickness,double *mx,const class RXSitePt *q,class RX_FETri3 *p_Tri,class RXLayer *p,MatDataType &xv,double ps[3])
{
    /* Find the value of the material at given point */
    /* this model returns the 3by3 on its own
   the material ref vector (black space) is in the first 3
   floats in dummy */

    struct PC_FIELD*field = (PC_FIELD *)p->matptr->dataptr;
    struct PC_MATERIAL*mat = (PC_MATERIAL *)field->mat->dataptr;

    double f=0,tfac=0;
    int i,inside=0;

    *thickness = 1.0;
    xv[0] = (float)1.0;
    xv[1] = (float)0.0;
    xv[2] = (float)0.0;

    if (field->flag&FILAMENT_FIELD){   // there is now a Mat_Filament
        assert(0); //if(Compute_Filament_Field(p->matptr,thickness,p_tri,xv,mx,&f,&inside,ps))  return mx; // was field
        return(NULL);
    }
    if (field->flag&DOVE_FIELD){
        if(Compute_Field_Dove(p->matptr,thickness,q,xv,mx,&f,&inside,ps))  return mx; // was field
        return(NULL);
    }
    if (field->flag&NURBS_ALPHA){
        if(Compute_NURBS_ALPHA_Field(p->matptr,thickness,p_Tri,xv,mx,&f,&inside,ps))  return mx; // was field
        return(NULL);
    }
    if (field->flag&RADIAL){
        if(!Compute_Radial_Field(field,thickness,q,xv,&f,&inside))  return NULL;
    }

    else {
        if(field->sc1==field->sc2) {
        if  (!Compute_OneCurve_Field(field,thickness,q,xv,&f,&inside)) return NULL;
        }
        else
        {
          if  (!Compute_TwoCurve_Field(field,thickness,q,xv,&f,&inside)) return NULL;
        }
    }

    tfac = 0.0;
    if(*thickness ==0.0)
        return NULL;
    if(inside || (field->flag&EVERYWHERE) )  {
  //      if(field->flag&CURVE_FIELD)
 //           assert("Mat_Parallel( &thdum,mx,q,tri,p,xv,ps);"==0); // gets xv not thickness

        if(field->flag&UNIFORM) {
            tfac= tfac+1.0;
        }
        if(field->flag&TRIANGLE) {
            double g = 2.0 - 4.0* fabs(f-0.5);
            if(g> 0.0)
                tfac= tfac+g;
        }
        if(field->flag&RAMP) {
            if(f > 0.0)
                tfac=tfac+ f*2.0;
        }
        if(field->flag&COSINE) {
            double a = sin(f*ON_PI);
            if(a>0.0)
                tfac =  tfac + a*ON_PI/ 2.0;

        }
        if(field->flag&COSSQ) {
            double a = sin(f*ON_PI);
            if(a>0.0)
                tfac = tfac + a*a  * 2.0;
        }
        if(field->flag&INVERSE) {
            if(f > 0.00001)
                tfac = tfac + 1.0/f;
        }
    }

    *thickness= *thickness*tfac;

    memcpy(mx,mat->d,9*sizeof(MatValType));
    ON_3dPoint ppp = mat->m_qprestress.ToONPoint();
    for(i=0;i<3;i++)
        ps[i] =  ppp[i];

    return(mx);
}//MatValType *Mat_Field

MatValType *Mat_Radial(double *thickness,MatValType *mx,const class RXSitePt *q,class RX_FETri3 *p_Tri,class RXLayer *p,MatDataType &xv,double ps[3]) {
    /* Find the value of the material at given point */
    /* Data is expected as follows:
 0,1,2 material x-axis (black space) to be returned in xv( ptr to 3 floats)
  3,4,5 x,y,z of focus;
  6 thickness at focus
  7 radius at which material thickness is 1.0
  14/11/94 thickness now rad/r0,
  within limits (1->{thickness})
    */
    int i,j,n;
    ON_3dVector xc,xr;
    struct PC_MATERIAL *mat;
    double r,t;
    MatValType themat[3][3];
    /* 	extern FILE *matfp;*/



    n = p->matstruct.m_sizeofdata;
    assert (n==8);

    mat = (PC_MATERIAL *)p->matptr->dataptr;
    assert(p->matptr->TYPE==FABRIC);
    //	mem cpy(xv,p->matstruct.Data,n*sizeof(float));
    xv=p->matstruct.m_Data;

    /* the function
  1) get radius vector from q to xx[3] . Length is r, direction is xv
  2) thickness t is xx[6] *(1-r/xx[7] + 1.0*r/xx[7] with a min of 1.0
  3) multiply each element of p->d by t and put in mx
  */

    xc.x=xv[3]; xc.y=xv[4];xc.z=xv[5];
    xr.x=q->x-xc.x;
    xr.y=q->y-xc.y;
    xr.z=q->z-xc.z;
    r = xr.Length();
    if(r <FLOAT_TOLERANCE ) r=(float) FLOAT_TOLERANCE;
    xv[0]=  xr.x/r;
    xv[1]=  xr.y/r;
    xv[2]=  xr.z/r;
    /*	  t = xv[6] *((float)1.0-r/xv[7]) + (float)1.0*r/xv[7]; if(t<1.0) t=(float)1.0;	*/

    t = xv[7]/r;
    if(t<1.0) t=(float)1.0;
    if(t>xv[6]) t = xv[6];
    *thickness = t;
    /*	fprintf(matfp,"xv = %12.5f, %12.5f, %12.5f, %12.5f\n",xv[0],xv[1],xv[2],t); */
    for(i=0;i<3;i++) {
        for(j=0;j<3;j++) {
            themat[i][j]= mat->d[i][j];
        }
    }

    memcpy(mx,themat,9*sizeof(MatValType));
    ON_3dPoint ppp = mat->m_qprestress.ToONPoint();
    for(i=0;i<3;i++)
        ps[i] =  ppp[i];

    return(mx);
}



MatValType *Mat_Parallel(double *thickness,MatValType *mx,const class RXSitePt *q,class RX_FETri3 *p_Tri,class RXLayer *dummy,MatDataType &xv,double ps[3]) {
    /* Find the value of the material at given point */
    /* Data is the SC Entity  we are parallel to
 material x-axis (black space) is to be returned in xv( ptr to 3 floats)
 
    */


    struct PC_MATERIAL *mat;
    ON_3dPoint r; ON_3dVector t;
    double  s,d,tol;
    class RXLayer *p = dummy;
    RXEntity_p sce = p->matstruct.ref_sc;
    sc_ptr  sc;
    int i, errval;
    //assert(p->matstruct.sizeofdata==0); this fails on radial curve seams as the value is 3 (i think)

    if(sce){
        sc = (sc_ptr  )sce;
        *thickness = 1.0;
        /* the function
      Drop a perpendicular to SC->C[1] from where we are
      return the tangent as xv(0:2)
    */
        s=0.0;
        tol =  sc->GetArc(1) /50.0 ;
        assert(0);
        if((errval = sc->m_pC[1]->Drop_To_Curve(q->ToONPoint(),&r, &t,tol,&s,&d)) < 1)  {
            printf(" PC_Drop_To_Curve returned %d\n",errval);
            assert(p); p->OutputToClient ("cant DROP to curve",1);
        }
        xv[0]=t.x; xv[1]=t.y;   xv[2]=t.z;
    }
    else {
        *thickness = 0.0;	/* so dont care about vector */
        xv[0]=0.0; xv[1]=1.0;xv[2]=0.0;
    }
    switch (p->matptr->TYPE) {
    case FABRIC:
        mat = (PC_MATERIAL *)p->matptr->dataptr;
        break;
    case FIELD:{
        struct PC_FIELD *field = (PC_FIELD *)p->matptr->dataptr;
        mat  = (struct PC_MATERIAL *) field->mat->dataptr;
        break;
    }
    default:
        mat=NULL;
        assert(0);
    }

    memcpy(mx,mat->d,9*sizeof(MatValType));
    ON_3dPoint ppp = mat->m_qprestress.ToONPoint();
    for(i=0;i<3;i++)
        ps[i] =  ppp[i];
    return(mx);
}



MatValType * Mat_Uniform(double *thickness,double *mx,const class RXSitePt *q,class RX_FETri3 *p_Tri,class RXLayer *p,MatDataType &xv,double ps[3])
{
    /* Find the value of the material at given point */
    /* this model returns the 3by3 on its own
   the material ref vector (black space) is in the first 3
   floats in dummy */
    //float *ff;
    int i, n;

    struct PC_MATERIAL*mat = (PC_MATERIAL *)p->matptr->dataptr;
    *thickness = 1.0;
    assert(p->matptr->TYPE==FABRIC);

    n = p->matstruct.m_sizeofdata;
    assert (n==3) ;
    //ff =(float*) p->matstruct.Data;
    xv = p->matstruct.m_Data;
    memcpy(mx,mat->d,9*sizeof(MatValType));
    ON_3dPoint ppp = mat->m_qprestress.ToONPoint();
    for(i=0;i<3;i++)
        ps[i] =  ppp[i];

    if(MDB) printf("Uniform material (%f %f %f)\n", xv[0],xv[1],xv[2]);
    if(MDB) printf("matrix           (%f %f %f)\n", mx[0],mx[1],mx[2]);
    if(MDB) printf("                 (%f %f %f)\n", mx[3],mx[4],mx[5]);
    if(MDB) printf("                 (%f %f %f)\n", mx[6],mx[7],mx[8]);
    return(mx);
}

MatValType * Mat_Curve(double *thickness,double *mx,const class RXSitePt *q,class RX_FETri3 *p_Tri,class RXLayer *p,MatDataType &xv,double ps[3])
{
    /* Find the value of the material at given point */
    /* this model returns the 3by3 on its own

    */
    int i,n;
    struct PC_MATERIAL*mat = (PC_MATERIAL *)p->matptr->dataptr;
    sc_ptr sc;
    RXEntity_p e;
    struct LINKLIST *ll;

    ON_3dPoint  p1;
    ON_3dVector l_t1,t2 ;
    assert(p->matptr->TYPE==FABRIC);
    *thickness = 1.0;
    xv[0] = xv[1] =xv[2] = 0.0;
    n = p->matstruct.m_sizeofdata;
    assert(n==0);
    ll = p->matstruct.ll;
    while (ll) {
        e = (RXEntity_p )ll->data;
        if(!e) {  ll = ll->next; continue;}
        sc = (sc_ptr  )e;
        if(!sc) {  ll = ll->next; continue;}  //xv will be the tangent to sc->curves[1] at the nearest point

        if (sc->m_pC[1]->GetONCurve()){
            double s = sc->GetArc(1)/2.0;  // seed point
            ON_3dPoint l_p = ON_3dPoint(q->x,q->y,q->z);
            ON_Curve *l_c = sc->m_pC[1]->GetONCurve();

            if(l_c->GetClosestPoint(l_p,&s, 10. * s+100.0)) {// this is really WIERD
                l_t1=l_c->TangentAt(s);
                if( (l_t1.x *xv[0]  +  l_t1.y *xv[1]  +l_t1.z *xv[2]  ) < 0.0) {
                    xv[0] = xv[0] - l_t1.x; xv[1] = xv[1] - l_t1.y; xv[2] = xv[2] - l_t1.z;
                }
                else {
                    xv[0] = xv[0] + l_t1.x; xv[1] = xv[1] + l_t1.y; xv[2] = xv[2] + l_t1.z;
                }
            }
            else {
                ON_String l_buf;
                l_buf.Format("(Mat_Curve) GCP failed v = %f %f %f on %s\t setting ref to (1,0,0)\n",l_p.x,l_p.y,l_p.z,sc->name());
                p->matptr->OutputToClient  (l_buf.Array(),1);
                xv[0]=1.; xv[1]=0.; xv[2]=0.;
            }
        }
        else // not an ONC
        {
            double dist=-1.0;
            double s = 0.5;
            double max_distance = sc->GetArc(1) *.0001;
            max_distance = max(max_distance,0.00001);
            int kkk = sc->m_pC[1]->Drop_To_Curve(q->ToONPoint (),&p1,&t2, max_distance,&s,&dist);
            if (0 <kkk ){
                if(  t2.x*xv[0]+t2.y*xv[1]+t2.z*xv[2] < 0.0) {
                    xv[0] = xv[0] - t2.x; xv[1] = xv[1] - t2.y; xv[2] = xv[2] - t2.z;
                }
                else {
                    xv[0] = xv[0] + t2.x; xv[1] = xv[1] + t2.y; xv[2] = xv[2] + t2.z;
                }
            }
            else {
                ON_String l_buf,b2;
                l_buf.Format("nonONC drop to curve failed(%d)  v = %f %f %f on %s\t setting ref to (1,0,0)\n",kkk,q->x,q->y,q->z,sc->name());
                b2.Format("nearest pt =( %f %f %f ) s= %f  dist=%f\n",p1.x,p1.y,p1.z, s,dist);
                assert(p); p->OutputToClient  (l_buf+b2,1);
                xv[0]=1.; xv[1]=0.; xv[2]=0.;
            }
        }

        ll = ll->next;
    }// while ll
    // verify this!!
    ON_3dVector vv(xv[0],xv[1],xv[2]); vv.Unitize ();
    for(int k=0; k<3;k++) xv[k]=vv[k];

    memcpy(mx,mat->d,9*sizeof(MatValType));
    ON_3dPoint ppp = mat->m_qprestress.ToONPoint();
    for(i=0;i<3;i++)
        ps[i] =  ppp[i];

    return(mx);
}
// double *(*Get_Material_Function_Key(char *s))(double *,double *,Site *,class RX_FETri3 *,void *,float *){

MatFuncPtr Get_Material_Function_Key(const char*s) {  //gcc picked this up


    if(strieq(s,"parallel"))
        return((MatFuncPtr)Mat_Parallel);
    if(strieq(s,"uniform"))
        return((MatFuncPtr)Mat_Uniform);
    if(strieq(s,"radial"))
        return((MatFuncPtr)Mat_Radial);
    if(strieq(s,"filament"))
        return((MatFuncPtr)Mat_Filament);
    if(strieq(s,"field"))
        return((MatFuncPtr)Mat_Field);
    if(strieq(s,"curve"))
        return((MatFuncPtr)Mat_Curve);
    return(NULL);
} 
