#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXRelationDefs.h"
#include "RXCurve.h"
#include "RXSRelSite.h"
#include "RXPside.h"
#include "RXSail.h"
#include "RX_FEString.h"
#include "RXOffset.h"
//#include "RXQuantity.h"
#include "resolve.h" // for gcwr
#include "entities.h" // for push

#include "etypes.h"
#include "global_declarations.h"
#include "stringutils.h"
#include "arclngth.h"
#include "iangles.h"
#include "printall.h"

#include "words.h"

#include "RXCompoundCurve.h"

RXCompoundCurve::RXCompoundCurve()
{
    assert("dont use default SC constructor"==0);
}
RXCompoundCurve::RXCompoundCurve(class RXSail *p ):
    RXSeamcurve (p)

{
    this->m_ccCurves =0;
    this->m_ccRevflags=0;
    this->m_ccE1sites=0; this->m_ccE2sites=0;
    this->m_ccNcurves=0;
    Init();
}
RXCompoundCurve::~RXCompoundCurve() {
    this->CClear();
}

int RXCompoundCurve::Compute(void){
    return Compute_Compound_Curve();	// so its length may be used as a target
}

int RXCompoundCurve::Finish(void){assert(0); return 0;}
int RXCompoundCurve::CClear(){
    int rc=0;
    // clear stuff belonging to this object
    int k, m;

    if(this->m_ccRevflags) { RXFREE(this->m_ccRevflags); this->m_ccRevflags=NULL; }
    if(this->m_ccCurves) 	 { RXFREE(this->m_ccCurves);   this->m_ccCurves=NULL; }
    if(this->m_ccE1sites)  { RXFREE(this->m_ccE1sites);  this->m_ccE1sites=NULL; }
    if(this->m_ccE2sites)  { RXFREE(this->m_ccE2sites);  this->m_ccE2sites=NULL; }
    this->m_ccNcurves=0;



    for(k=0;k<3;k++) {
        if(this->p[k] && this->p[k]!=this->m_pC[k]->Get_p()) RXFREE(this->p[k]);
        this->p[k]=0;
        if(this->m_pC[k] ) delete this->m_pC[k];this->m_pC[k]=0;
    }
    /* the SCs at the ends are in the IAList.   We have to remove this SC from their IALists
That will work equally for intersecting SCs. 
 For safety, we also check the Curves of any relsites in the IAList.
 But these curves would normally always be in the IAlist themselves.
 We dont need to check EndNsite->dataptr->Curve because EndNSite will always be in the
 IA List.
 */
    if(this->ia){
        for(k=0;k<this->n_angles;k++) {
            if(this->ia[k].s->TYPE==SEAMCURVE) {
                sc_t  *sc = (sc_ptr  )this->ia[k].s;
                sc->Remove_From_IA_List(this,0) ;
            }
            else  if(this->ia[k].s->TYPE==RELSITE) {
                Site*es = (Site*) this->ia[k].s;
                for(m=0;m<2;m++) {
                    if(es->CUrve[m]) {
                        sc_t  *sc = (sc_ptr  )es->CUrve[m];
                        sc->Remove_From_IA_List(this,0) ;
                    }
                }
            }
        }
        if(g_Peter_Debug) cout<< "IAs freed"<<endl;
        if(!g_Janet && Remove_From_All_IA_Lists(this)) {
             char buf[256];
             sprintf(buf,"CC '%s' needed big remove",this->name());
             rxerror(buf,1);
        }
        // we may be left with some IA Offs that are owned by this->ia.  Let's free them
        for(k=0;k<this->n_angles ; )// dont increment k because the fn decrements n_angles
           this->Delete_One_IA(k);

        RXFREE(this->ia); this->ia=NULL; this->n_angles =0;
    }
    if(this->pslist)
    { RXFREE(this->pslist); this->pslist=NULL; } this->npss=0;

    for(k=0;k<3;k++) {
        if(this->Mat_Angle[k].str   ) {
            RXFREE(this->Mat_Angle [k].str); this->Mat_Angle [k].str=NULL;
        }
    }

    RXExpressionI *rxi;
    if(rxi=this->FindExpression (L"ea",rxexpLocal) ) delete rxi;
    if(rxi=this->FindExpression (L"ti",rxexpLocal) ) delete rxi;
    // call base class CClear();
    rc+= RXSeamcurve::CClear();
    return rc;
}

int RXCompoundCurve::Init() {
    RXSeamcurve::Init();

    this->m_ccCurves =0;
    this->m_ccRevflags=0;
    this->m_ccE1sites=0; this->m_ccE2sites=0;
    this->m_ccNcurves=0;
    return(1);
} 



int RXCompoundCurve::Resolve() {

    /* compound curve card is of type

     compound curve : <name> : n1: n2:...(maybeoneday [:$endlist[: atts]] ... */
    int k;
    const char *s,*name;
    sc_ptr ec;
    int retval = 0 ;
    std::string sline(this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline, RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw>1){
        name=wds[1].c_str();
        this->SeamCurve_Init();
        k = 2;
        for(k=2;k<nw;k++)  {
            s= wds[k].c_str();

            ec = (sc_ptr) this->Esail->Get_Key_With_Reporting("edge,seam,curve,seamcurve,nodecurve",s);
            if(ec) {
                /* one curve is enough to justify Finishing. but because Finishing zeros the plists,
     it must be done before the pushes */
                if(!this->m_ccNcurves)
                    if(!this->PC_Finish_Entity("compound curve",name,this,(long)0,NULL," ",this->GetLine()))break;
                this->m_ccCurves=(sc_ptr *)REALLOC(this->m_ccCurves,(this->m_ccNcurves+1)*sizeof(RXEntity_p ));
                this->m_ccCurves[this->m_ccNcurves]=ec;

                ec->SetRelationOf(this,child|niece,RXO_SC_OF_CC,this->m_ccNcurves);
                (this->m_ccNcurves)++;
            }
            else{
                cout<<" CClear on CompoundCurve resolve miss????"<<endl;
                return(0);// an SC was unresolved or non-existent
            }
        } // for k
        /* we get here with p->ncurves=NULL if there are no SCsin the card list . MAybe this is OK */
        if(!this->m_ccCurves) {
            this->m_ccCurves=(sc_ptr *)REALLOC(this->m_ccCurves,(this->m_ccNcurves+1)*sizeof(RXEntity_p ));
            printf(" finishing blank CC %s\n",this->name());
            this->PC_Finish_Entity("compound curve",name,this,(long)0,NULL," ",this->GetLine());
            this->m_ccRevflags=(int *)CALLOC(1,sizeof(int));

            this->Needs_Finishing=1;
            this->Needs_Resolving=0;
            return 1;
        }

        this->m_ccRevflags=(int *)CALLOC(this->m_ccNcurves,sizeof(int));
        if(this->m_ccNcurves) {
            Needs_Finishing=1;
            Needs_Resolving=0;
            retval = 1;
        }
        else { cout<< "RXFREE(p);"<<endl; }
    }

    return(retval);
}


int RXCompoundCurve::Dump( FILE *fp) const
{
    static int level = 0;
    if(level>0)
        return 0;

    level ++;

    if(this->Defined_Length) fprintf(fp,"   Fixed_Length.\n");

    if(this->m_pC[1]&&  this->m_pC[1]->IsValid())
        fprintf(fp," Arclength(C1)%f\n",this->GetArc(1));
    fprintf(fp,"    Ncurves   %d\n",this->m_ccNcurves);
    for (int k=0;k<this->m_ccNcurves;k++) {
        RXEntity_p e = this->m_ccCurves[k];
        fprintf(fp,"          %3d     %s   (rev=%d)",k,  e->name(),this->m_ccRevflags[k]);
        fprintf(fp,"\n");
    }
    fprintf(fp,"    N Psides   %d\n",this->npss);
    for (int k=0;k<this->npss;k++) {
        RXEntity_p e = this->pslist[k];
        fprintf(fp,"          %3d     %s\n",k, e->name());
    }
    level --;
    fprintf(fp,"Now print as a SC\n");
    return  1+RXSeamcurve::Dump(fp) ;
}


double RXCompoundCurve::GetArc( const int & p_ID,const double fractional_tolerance) const
{	
    double l_len = 0;

    //	if(p_ID !=1) assert(p_sc->C[p_ID]->IsValid());
    if (!&(this->m_pC[p_ID]))
        return l_len;

    //  Sept 2005  this fails for tpn and xyzabs models
    //	l_len =p_sc->C[p_ID]->Get_arc(fractional_tolerance);
    //	return l_len;
    // Sept 2005 if the above works, below here would be DEAD CODE


    if (this->m_pC[p_ID]->GetONCurve())
    {
        const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(this->m_pC[p_ID]->GetONCurve());

        if (!l_crv)
            rxerror("m_ONCurve is not a ON_Curve object, in seamcurves.cpp GetArc() ",2);
#ifdef SC_DEBUG

        if(ON_NurbsCurve::Cast(p_sc->C[p_ID]->GetONCurve()))
            rxerror(" Its a nurbscurve ",1);
        else if(ON_ArcCurve::Cast(p_sc->C[p_ID]->GetONCurve()))
            rxerror(" Its a ArcCurve ",1);
        else 	if(ON_PolyCurve::Cast(p_sc->C[p_ID]->GetONCurve()))
            rxerror(" Its a Polycurve ",1);
        else 	if(ON_PolylineCurve::Cast(p_sc->C[p_ID]->GetONCurve()))
            rxerror(" Its a polylinecurve ",1);
        else 	if(ON_LineCurve::Cast(p_sc->C[p_ID]->GetONCurve()))
            rxerror(" Its a linecurve ",1);
        else 	if(ON_CurveProxy::Cast(p_sc->C[p_ID]->GetONCurve()))
            rxerror(" Its a curveProxy ",1);
        else
            rxerror(" Cant cast at all ",3);
#endif
        if(! l_crv->GetLength(&l_len,fractional_tolerance) ){
            //for checking what its going on with l_crv->GetLength
            int l_test = l_crv->GetLength(&l_len,fractional_tolerance);
            // try as a polyline
            const ON_PolylineCurve * l_p = ON_PolylineCurve::Cast(this->m_pC[p_ID]->GetONCurve());
            if( !l_p)
            {	char str[256];
                sprintf(str,"ERROR: cannot cast '%s' to an ON_PolylineCurve; in seamcurves.cpp GetArc", this->name() );
                rxerror(str,1);
            }
            else
                if( ! l_p->GetLength(&l_len,fractional_tolerance) ){
                    char str[256];
                    sprintf(str,"getlength failed in seamcurves.cpp GetArc\n %s", this->name() );
                    rxerror(str,2);
                }
        }
    }
    else
    {
        if (this->c[p_ID]>1)
            l_len = PC_Polyline_Length(this->c[p_ID],this->p[p_ID]);
        else
            l_len = this->m_arcs[p_ID];  // usually only for compound curves
    }
    return l_len;
}//double GetArc

int RXCompoundCurve::Delete_Seamcurve(void ){

    /* may be part of a CC*/
    RXCompoundCurve *np=this;
    int k,j, m,fin=0;

    class RXCompoundCurve  *cc;

    ent_citer it;
    if(!g_Janet) { //cout<< " should use mapextract(CC)"<<endl;
        for (it = Esail->MapStart(); it != Esail->MapEnd(); it=Esail->MapIncrement(it)) {
            RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);


            if(this->TYPE==COMPOUND_CURVE) {
                cc = dynamic_cast<class RXCompoundCurve*> ( p) ;
                if(g_Peter_Debug){
                    printf(" FOUND CC %s %s dp=%p", this->type(), this->name(),cc);
                    if(cc) printf(" (ncurves = %d)\n",cc->m_ccNcurves);
                    else cout<< " NULL CC"<<endl;
                }
                if(cc){
                    for(j=0;j<cc->m_ccNcurves;j++) {
                        if(cc->m_ccCurves[j]==np) {
                            if(g_Peter_Debug) printf("removing %s from %s\n",this->name(),this->name());
                            (cc->m_ccNcurves)--;
                            this->SetNeedsComputing();
                            for( k=j;k<cc->m_ccNcurves;k++) {
                                cc->m_ccCurves[k] = cc->m_ccCurves[k+1];
                            }
                            fin=1;
                            break;
                        }
                    }
                }
            } // end if compound curve
            if(fin)break;
        } // compound curve trawl
    } //not janet
    if(g_Peter_Debug) cout<< "CC check ended"<<endl;


    for(k=0;k<3;k++) {
        if(this->p[k] && this->p[k]!=this->m_pC[k]->Get_p()) RXFREE(this->p[k]);
        this->p[k]=0;

        delete this->m_pC[k];
    }
    /* the SCs at the ends are in the IAList.   We have to remove this SC from their IALists
That will work equally for intersecting SCs. 
 For safety, we also check the Curves of any relsites in the IAList.
 But these curves would normally always be in the IAlist themselves.
 We dont need to check EndNsite->dataptr->Curve because EndNSite will always be in the
 IA List.
 */
    if(this->ia){
        for(k=0;k<this->n_angles;k++) {
            if(this->ia[k].s->TYPE==SEAMCURVE) {
                sc_t  *sc = (sc_ptr  )this->ia[k].s;
                sc-> Remove_From_IA_List( np,0) ;
            }
            else  if(this->ia[k].s->TYPE==RELSITE) {
                Site*es = (Site*) this->ia[k].s;
                for(m=0;m<2;m++) {
                    if(es->CUrve[m]) {
                        sc_t  *sc = (sc_ptr  )es->CUrve[m];
                        sc->Remove_From_IA_List( np,0) ;
                    }
                }
            }
        }
        if(g_Peter_Debug) cout<< "IAs freed"<<endl;
        if(!g_Janet && Remove_From_All_IA_Lists(this)) {
            char buf[256];
            sprintf(buf,"CC '%s' needed big remove",this->name());
            rxerror(buf,1);
        }
        // we may be left with some IA Offs that are owned by this->ia.  Let's free them
        for(k=0;k<this->n_angles ; )// dont increment k because the fn decrements n_angles
            this->Delete_One_IA(k);

        RXFREE(this->ia); this->ia=NULL; this->n_angles =0;
    }
    int count = this->npss *2 + 256;
    for(;this->npss>0;) { // this should be the first action
        PSIDEPTR pse = this->pslist[0];
        pse->Kill();
        count--;
        if(!count)
            break;
    }
    if(this->pslist)
    { RXFREE(this->pslist); this->pslist=NULL; } this->npss=0;

    cout<< "TODO:: Delete_Geodesic_List(this->GeodesicList);   Free_Gcurve_List(np); "<<endl;


    for(k=0;k<3;k++) {
        if(this->Mat_Angle[k].str   ) {
            RXFREE(this->Mat_Angle [k].str); this->Mat_Angle [k].str=NULL;
        }
    }

    RXExpressionI *rxi;
    if(rxi=this->FindExpression (L"ea",rxexpLocal) ) delete rxi;
    if(rxi=this->FindExpression (L"ti",rxexpLocal) ) delete rxi;
    if(g_Peter_Debug) cout<< "returning from Delete:SC"<<endl;
    return(1);
}



int RXCompoundCurve::ReWriteLine(void){
 //   sc_ptr  this = this;
    /* seam card is of type
      seam:  name  :  SITE1 : SITE2 : basecurve [length] [[[optional adjectives] leftmat] rightmat] !comment*/
    //	char nn[256];

    std::string sline( this->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();


    RXSTRING NewLine;
    NewLine=TOSTRING(L"compound curve");

    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );
    NewLine+=  TOSTRING(this->name()) ;
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );

    if(!g_Janet) {

        if(this->End1site && this->End1site->generated <= this->generated) {
            NewLine+=  TOSTRING( this->End1site->name());
        }
        else 	if (nw>2)
            NewLine+=  TOSTRING(wds[2].c_str());
    }
    else {
        if(this->End1site && (
                    (this->End1site->generated <= this->generated)
                    ||this->FlagQuery(SNAP_TO_NODES)
                    ||this->FlagQuery(SNAP_TO_ENDS_ONLY) )
                ) {
            NewLine+=  TOSTRING(this->End1site->name());
        }
        else 	if (nw>2)
            NewLine+=  TOSTRING(wds[2].c_str());
    }
    NewLine+=RXSTRING(_T(RXENTITYSEPSTOWRITE) );

    if(!g_Janet) {

        if(this->End2site && this->End2site->generated <= this->generated)
            NewLine+=  TOSTRING( this->End2site->name());
        else if (nw>3)
            NewLine+=  TOSTRING(wds[3].c_str());
    }
    else { // janet
        if(this->End2site && (
                    (this->End2site->generated <= this->generated)
                    ||this->FlagQuery(SNAP_TO_NODES)
                    ||this->FlagQuery(SNAP_TO_ENDS_ONLY) )
                ) {
            NewLine+=  TOSTRING( this->End2site->name());
        }
        else if (nw>3)
            NewLine+=  TOSTRING(wds[3].c_str());
    }
    this->SetLine(ToUtf8(NewLine).c_str());

    return 1;
}

int RXCompoundCurve::Compute_Compound_Curve() {
    /* Given curve list, generates ps list
     and arc length
     Its now called from R_Solve, so its length may be used as a target
        */
    PSIDEPTR ps;
    int i,k,tot;
    if(!strieq(this->type(),"compound curve")) return(0);
    if(this->Needs_Resolving || this->Needs_Finishing)
        return 0;

    this->m_arcs[0] =this->m_arcs[1] =this->m_arcs[2] =(float) 0.0;
    if (this->pslist) RXFREE(this->pslist);
    this->pslist=NULL;
    this->npss=0;

    for (i=0;i<this->m_ccNcurves;i++) {
        sc_ptr  sc;
        RXEntity_p cc = this->m_ccCurves[i];
        sc = (sc_ptr  )cc;

        for(k=0;k<3;k++) {
            if(sc->m_pC[k]->IsValid ())
                this->m_arcs[k] = this->m_arcs[k] + sc->GetArc(k);
            else
            {
                printf("(Compute_Compound_Curve ) cant tot up arc, side %d\n", k);
                assert(k !=1);
                this->m_arcs[k] = this->m_arcs[k] + sc->GetArc(2-k);
            }
        }
        tot = this->npss + sc->npss;
        this->pslist=(PSIDEPTR  *)REALLOC(this->pslist,(tot+1)*sizeof(PSIDEPTR ));

        if( !this->pslist) {rxerror("MALLOC!! (Compute_Compound_Curve)",3);}
        if(!(this->m_ccRevflags[i])) {
            for (k=0;k<sc->npss;k++) {
                ps = sc->pslist[k]; /* l114 */
                ps->Backwards=0;				// IS THIS EVER USED????????
                this->pslist[this->npss] = sc->pslist[k]; this->SetRelationOf(sc->pslist[k],parent,RXO_PSIDE_OF_CC,this->npss); // not sure about 'parent'
                (this->npss)++;
            }
        }
        else { /* reversed */
            for (k=sc->npss-1;k>=0;k--) {
                ps =  sc->pslist[k];
                ps->Backwards=1;
                this->pslist[this->npss] = sc->pslist[k];this->SetRelationOf(sc->pslist[k],parent,RXO_PSIDE_OF_CC,this->npss); // not sure about 'parent'
                (this->npss)++;
            }
        }
    }
#ifdef HOOPS
    HC_Open_Segment(this->type());
    this->hoopskey = HC_KOpen_Segment(this->name());		// Feb 2003 Use to be zero. so we can see where it places this value
    //	sprintf(buf,"length=%f,blacklength=%f", this->m_arcs[0], this->m_arcs[1]);
    //HC_Set_User_Options(buf);
    HC_Close_Segment();
    HC_Close_Segment();
#endif
    return(1);
}              
