// RXPattern.h: interface for the RXPattern class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RXPATTERN_H__150F1D61_A096_4937_863E_37ABB66B01D1__INCLUDED_)
#define AFX_RXPATTERN_H__150F1D61_A096_4937_863E_37ABB66B01D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifdef __cplusplus
// #include "debug.h"  NOT ALLOWED. debug.h must be the first include file
#include "RXObject.h"
#include "opennurbs.h"	// Added by ClassView


#include "opennurbs_xform.h"	// Added by ClassView
class RXPattern : public RXObject  
{
public:
	int DevelopOneCurve(  ON_CurveOnSurface *pC);
	double FanningStrain(const double p_u,const double p_v,int *err);
	ON_Object * Pattern(ON_ClassArray<ON_CurveOnSurface> * p_ca);
	RXPattern(ON_Surface *);
	virtual ~RXPattern();
	const double phiDashed(const double t);
	double Integral_One(const double d0,const double d1);
	double m_d0;
	const double sDashed(const double u);
	ON_3dVector GeodesicCurvature( const double t,const ON_CurveOnSurface *pc=0);
	double Integral_G1(const double t0,const double t1, const double tol, double*err);
	double m_tol;
private:

	ON_2dPoint TqToUV(const double t, const double q);
	double Integral_GSin(const double t0,const double t1, const double tol, double*err);
	double Integral_GCos(const double t0,const double t1, const double tol, double*err);

	ON_CurveOnSurface * m_cOs; // a temporary for the integrations.
	ON_Curve * IntegrationPath(const ON_2dPoint p1,const ON_2dPoint p2);
	ON_ClassArray<ON_CurveOnSurface>  * m_CoS_array;

	double Integral_Two(const double t0,const double t1, const double tol, double*err);

	double GxDa(double u,double v);
	int FannedEdgeCurvature(const double v, double*a0,double *a1);

protected:
	ON_Xform JFanned(const double p_u, const double p_v); 
	ON_Xform JTwoDee(const double p_u, const double p_v);

	ON_Surface * m_s;

};
// global declarations

EXTERN_C double  Integrand1 ( void* p, const double t);
EXTERN_C double Integrand2 ( void* p, const double t);

#endif //  __cplusplus
EXTERN_C int RXPatternTest(SAIL *sail) ;
#endif // !defined(AFX_RXPATTERN_H__150F1D61_A096_4937_863E_37ABB66B01D1__INCLUDED_)
