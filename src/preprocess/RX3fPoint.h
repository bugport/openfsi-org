// RX3fPoint.h: interface for the RXObject class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_RX3FPOINT_H__ABB3CEE2_47AF_454F_BA84_FE3393E3C12A__INCLUDED_)
#define AFX_RX3FPOINT_H__ABB3CEE2_47AF_454F_BA84_FE3393E3C12A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define RX3fPoint_OK 1

#include "RXObject.h"
 
#ifdef __cplusplus

#ifndef RX3FPOINTCLASSID //877FC37F 0EDF 428D B667 532A1E787B32
#define RX3FPOINTCLASSID "877FC37F-0EDF-428D-B667-532A1E787B32"
#endif

class RX3fPoint : public RXObject 
{
public:
	RX3fPoint();
	RX3fPoint(const RX3fPoint& p_Obj);
	RX3fPoint(float * p_x,float * p_y,float * p_z);

	virtual ~	RX3fPoint();
	void Init();
	void Clear();
	RX3fPoint& operator = (const RX3fPoint& p_Obj);
	
	virtual char * GetClassID() const; 
	virtual int IsKindOf(const char * p_ClassID) const;

	static RX3fPoint* Cast( RX3fPoint* p_pObj); 
	static const RX3fPoint* Cast( const RX3fPoint* p_pObj);

	const float * operator[](int i) const;
	int Set(int i, float * p_V);
	int Set(const ON_3fPoint & p_pt);
	
	ON_3dPoint operator*( float d ) const;
	ON_3dPoint operator/( float d ) const;
	ON_3dPoint operator+( const ON_3dPoint& p ) const;
	ON_3dPoint operator+( const ON_3dVector& v ) const;
	ON_3dVector operator-( const ON_3dPoint& p ) const;
	ON_3dPoint operator-( const ON_3dVector& v ) const;
	
	ON_3dPoint  operator-( const RX3fPoint& v ) const;

	float operator*(const ON_3dPoint& h) const;
	float operator*(const ON_3dVector& h) const;
	float operator*(const ON_4dPoint& h) const;
	bool operator==( const ON_3dPoint& p ) const;
	bool operator!=( const ON_3dPoint& p ) const;

protected:
	const float * m_x;  
	const float * m_y;
	const float * m_z;
};

#endif //#ifdef __cplusplus

#endif // !defined(AFX_RXOBJECT_H__ABB3CEE2_47AF_454F_BA84_FE3393E3C12A__INCLUDED_)
