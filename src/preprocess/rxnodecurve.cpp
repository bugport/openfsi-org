#include "StdAfx.h"
#include <QDebug>
#include "RXSRelSite.h"
#include "RXCurve.h"
#include "RXOffset.h"
#include "RXSail.h"
#include "RXRelationDefs.h"
#include "RXAttributes.h"

#include "rxnodecurve.h"

RXNodeCurve::RXNodeCurve()
{
    assert(strlen("dont use default SC constructor")==0);
}

RXNodeCurve::RXNodeCurve(class RXSail *s):
    RXSeamcurve(s)
{
}
RXNodeCurve::~RXNodeCurve()
{
}

int RXNodeCurve::CClear()
{
    int rc=0;
    RXSeamcurve::CClear();
    return rc;
}
int RXNodeCurve::Dump(FILE *fp) const
{
    int rc=fprintf(fp," A NodeCurve \n");
    rc += this->RXSeamcurve::Dump(fp);
    return rc;
}

int RXNodeCurve::Compute(void)
{

    // qDebug()<< " re-create the polyline from the nodes see seamcurve.cpp ll 1029 ";
    int rc=0;
    set<RXObject*> z= this->FindInMap(RXO_NODECURVE_NODE,RXO_ANYINDEX);
    unsigned int  nn = z.size();
    int oldc, k,  changed=0;

    oldc = this->c[1];
    if(oldc-nn) changed=1;
    if(changed ) {
        this->c[1]=nn;
        this->p[1] = (VECTOR *)REALLOC(this->p[1],(nn+1)*sizeof(VECTOR));
        //  memset(this->p[1],0,c[1]*sizeof(VECTOR));
    }
    // verify whether any of the sites differ from the polyline vertices
    for (k=0;k<nn;k++){
        RXSRelSite* s = dynamic_cast<RXSRelSite*>( this->GetOneRelativeByIndex(RXO_NODECURVE_NODE,k) );
        if(k<oldc){
            ON_3dPoint  ppp( (const float*) &(this->p[1][k]));
            if (s->DistanceTo(ppp)> this->Esail->m_Linear_Tol)
                changed++;
        }
        else
            changed++;
        this->p[1][k].x = s->x ;
        this->p[1][k].y = s->y ;
        this->p[1][k].z = s->z ;
    }
    for(k=0;k<3;k++) {
        this->m_pC[k]->Update(this->p[1] ,this->c[1]);
        this->m_arcs[k] = (float) this->m_pC[k]->Get_arc();
    }


    return rc || changed;
}

int RXNodeCurve::Resolve(void)
{
    int rc=0;
    // nodecurve : name : atts: node1 : node2:...
    // RXO_NODECURVE_NODE
    RXEntity_p ne;
    QString qline(this->GetLine());
    QStringList wds=rxParseLine(qline,RXENTITYSEPREGEXP );
    int i,j,k,nw = wds.size();
    int nn=0;
    if(nw<5) return 0;

    RXAttributes rxa(qPrintable(wds[2]));

    for (k=3;k<nw;k++)
    {
        ne =  this->Esail->Get_Key_With_Reporting("site,relsite",qPrintable(wds[k]),true);
        if(!ne){
            this->CClear();
            return(0);
        }
        ne->SetRelationOf(this,child|niece,RXO_NODECURVE_NODE,k-3); nn++;
    }

    if( PC_Finish_Entity("nodecurve",qPrintable(wds[1]),
                         this,0,0, qPrintable(rxa.ToQString()),
                         qPrintable(qline)))
    {
        this->SeamCurve_Init();// makes new rxcurves
        this->FlagSet( rxa);
        this->End1site = dynamic_cast<class RXSRelSite*> (this->GetOneRelativeByIndex(RXO_NODECURVE_NODE,0)) ;
        this->End2site = dynamic_cast<class RXSRelSite*> ( this->GetOneRelativeByIndex(RXO_NODECURVE_NODE,nn-1)) ;

        this->ResolveCommon(rxa);
        this->Needs_Resolving= 0;
        this->Needs_Finishing = 1;
        this->SetNeedsComputing();
        this->m_arcs[1]= this->End1site->DistanceTo(*(this->End2site));
        return 1;
    }// if PC_Finish...
    return 0;
}
int RXNodeCurve::ReWriteLine(void)
{
  QStringList newline;
  newline<<type()<<  name();
  newline <<this->AttributeString();
  //    set<RXObject*> z= this->FindInMap(RXO_NODECURVE_NODE,RXO_ANYINDEX);
  //    for (set<RXObject*> ::iterator  it =z.begin();it!=z.end();++it)
  //        newline<<  QString::fromStdWString((*it)-> GetOName());
  int k;
  for (k=0; ;k++) {
    class RXObject* s = this->GetOneRelativeByIndex(RXO_NODECURVE_NODE,k) ;
    if(!s) break;
    newline<<  QString::fromStdWString(s-> GetOName());
  }
  QString ss = newline.join(RXENTITYSEPSTOWRITE  );
  this->SetLine( qPrintable(ss) );
  return 1;
}
int RXNodeCurve::Finish(void)
{
    RXOffset*dummyo;
    class RXSRelSite *s, *lastS;
    RXSTRING buf;
    unsigned int  nn ;
    int oldc, k;
    double t =0.0;

    int rc= RXSeamcurve::Finish();
    if(!rc)
        return 0;
    // for each of the nodes, not counting the first and last,

    set<RXObject*> z= this->FindInMap(RXO_NODECURVE_NODE,RXO_ANYINDEX);
    nn = z.size();

    oldc = this->c[1];
    assert(oldc==nn) ;
    lastS=dynamic_cast<class RXSRelSite*> (this->GetOneRelativeByIndex(RXO_NODECURVE_NODE,0)) ;
    for (k=1;k<nn-1;k++) {
        s = dynamic_cast<class RXSRelSite*> (this->GetOneRelativeByIndex(RXO_NODECURVE_NODE,k)) ;
        t += s->DistanceTo(*lastS);
        buf = TOSTRING(t);
        RXSTRING iname=QString("i%1").arg(k).toStdWString();
        dummyo= dynamic_cast< RXOffset*>( this->AddExpression (new RXOffset( iname,buf,this,this->Esail )));// this->SetRelationOf(dummyo,aunt,RXO_EXP_OF_ENT); //done
        dummyo->SetAccessFlags(RXE_PRIVATE);
        dummyo->Evaluate(this ,1);
        this->Insert_One_IA(s,dummyo );
        lastS=s;
    }

    this->Needs_Finishing=0;
    this-> Compute();
    return 1;
}
