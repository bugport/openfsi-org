#ifndef RXRIGIDBODY_H
#define RXRIGIDBODY_H
#include "RXEntity.h"
//    else if	(strieq(Firstword,"rigidbody")) 	return(PCE_RIGIDBODY);
class RXRigidBody : public RXEntity
{
public:
    RXRigidBody();
    virtual ~RXRigidBody();
    RXRigidBody(class RXSail *s);

    int Dump(FILE *fp) const;
    int Compute(void); // return 1 if the object moved requiring compute of its nieces
    int Resolve(void);
    int Finish(void);
    int ReWriteLine(void);
	int Mesh();
	int m_Needs_Meshing;
protected:
        int TestAndMerge();
        int ExtractSites(    std::set<RXObject*> &siteset );
        int IsSixDOF() const;
        // The dofo will be in this' sail
        int m_dofono;
        int m_IsDead; // positive means it is dead. Negative means its being merged.
};

#endif // RXRIGIDBODY_H
