#include "StdAfx.h"
#include "rxON_Extensions.h"
#include <map>
#include "RXON_3dPoint.h"

RXON_3dPoint::RXON_3dPoint(void)
{
    x=y=z=ON_UNSET_VALUE;
}
RXON_3dPoint::RXON_3dPoint(ON_3dPoint p)
{
    x=p.x;	y=p.y;	z=p.z;
}
RXON_3dPoint::~RXON_3dPoint(void)
{
}
bool RXON_3dPoint::MapToSurface(const ON_Surface * p_From, 
                                const ON_Surface * p_To,
                                ON_3dPoint &       p_point) const
{
    if (!ON_Surface::Cast(p_From))
        return false;
    if (!ON_Surface::Cast(p_To))
        return false;

    double l_u,l_v;
    if (!p_From->GetLocalClosestPoint(*this,ON_UNSET_VALUE,ON_UNSET_VALUE,&l_u,&l_v))
        return false;

    p_point = p_To->PointAt(l_u,l_v);

    return true;
}

ON_3dPoint RXON_3dPoint::Flow(const ON_Curve * p_c1, const ON_Curve * p_c2)
{
    double seed,t1,t2,f;
    ON_3dPoint p1,p2,p1dash;
    ON_3dVector x1,x2,y12,z1,z2,y1,y2;
    ON_3dPoint rp(x,y,z);

    seed = p_c1->Domain().Mid();
    if(!p_c1->GetLocalClosestPoint(rp,seed,&t1))
    { cout<< "(Flow) GLCP fail"<<endl; return  rp;}

    // set up a trigraph xf1 (tangent1, (p2-p1), cross-product with origin p1
    // set up a trigraph xf2 (tangent2, (p2-p1), cross-product with origin p2
    p1=p_c1->PointAt(t1);

    f = (t1-p_c1->Domain().Min()) /  p_c1->Domain().Length() ;
    t2 = p_c2->Domain().Min() *(1.-f) + p_c2->Domain().Max() * f;

    p2=p_c2->PointAt(t2);
    x1 = p_c1->TangentAt(t1);
    x2 = p_c2->TangentAt(t2);
    y12= p2-p1; y12.Unitize ();
    z1=ON_CrossProduct(x1,y12);  if(!z1.Unitize () ) printf("(XON_3dPoint::Flow) cant unitize Z1 %f %f %f\n",z1.x,z1.y,z1.z) ;
    z2=ON_CrossProduct(x2,y12);  if(!z2.Unitize () ) printf("(XON_3dPoint::Flow) cant unitize  Z2 %f %f %f\n",z2.x,z2.y,z2.z) ;
    ON_Xform xf1, xf2;
    y1 = ON_CrossProduct(z1,x1);
    y2 = ON_CrossProduct(z2,x2);
    xf1 = ON_Xform(ON_3dPoint(0,0,0),x1,y1,z1);
    xf2 = ON_Xform(ON_3dPoint(0,0,0),x2,y2,z2);

    if(!xf1.Invert()) {
        cout<< " Flow : singular transformation"<<endl;
        return rp;
    }
    p1dash = xf1 *((*this)-p1) ; // pt in c1 coords (p1c1)
    rp = (xf2 * p1dash) + p2;		// flowed pt rp is xf2 * p1c1

    return  rp;
}
int RXON_3dPoint::Print(FILE * fp)
{
    return fprintf(fp," %f\t%f\t%f\n",x,y,z);
}
int RXONCurveDivide(const ON_Curve *p,	std::map<double,ON_3dPoint>&m,
                    std::map<double,ON_3dPoint>::iterator &lo,
                    std::map<double,ON_3dPoint>::iterator &hi,const double tolsq){

    std::map<double,ON_3dPoint>::iterator mid;
    ON_3dPoint p1,p2,p3;
    ON_3dVector c,a,t1,t2;
    double ysq=0.0 ,s;
    int r1,r2;
    bool curvy=false;
    p1 = lo->second ; p2=hi->second ;
    c = p2-p1;
    double angtolsq = tolsq/c.LengthSquared ();
    if(angtolsq>0.25)
        return 0;
    c.Unitize ();

    t1 = p->TangentAt (lo->first); t1.Unitize ();
    s = pow( ON_DotProduct(t1,c),2);
    if(s > angtolsq)
        curvy=true;
    else {
        t2 = -p->TangentAt (hi->first); t2.Unitize ();
        s = pow( ON_DotProduct(t2,c),2);
        if(s > angtolsq)
            curvy=true;
    }

    s = (lo->first +hi->first )/2.; // midpt
    p3= p->PointAt(s);
    a = p3-p1;
    if(!curvy) {
        ysq = ON_CrossProduct(a,c).LengthSquared();
        if(ysq< tolsq/1000.)
            return 0;
    }
    m[s]=p3;
    mid = m.find(s); assert(mid!=m.end());
    if(ysq< tolsq && !curvy)
        return 1;
    r1= RXONCurveDivide(p,m,lo,mid,  tolsq);
    r2= RXONCurveDivide(p,m, mid,hi, tolsq);
    return r1+r2;
}

ON_PolylineCurve* CurveToPolyline(const ON_Curve *c, const double tolsq,int &err)
{
    ON_PolylineCurve* pc=0; err=0;
    map<double,ON_3dPoint> mm;
    ON_SimpleArray<ON_3dPoint> pts;

    if(c->IsPolyline(&pts)) {
        pc = new ON_PolylineCurve(pts);
        return pc;
    }

    std::map<double,ON_3dPoint>::iterator lo,hi,it;
    ON_Interval i = c->Domain();
    mm[i.Min()]=c->PointAtStart();
    mm[i.Max()]=c->PointAtEnd();
    lo =mm.begin(); hi= mm.end(); hi--;
    int n = RXONCurveDivide(c,mm,lo,hi,tolsq);

    pc  = new ON_PolylineCurve(pts);
    for(it = mm.begin(); it!=mm.end(); ++it)
    {
        pc->m_pline.AppendNew()=it->second;
    }
    return pc;
}
