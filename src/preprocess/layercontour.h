// layercontour.h: interface for the layercontour class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERCONTOUR_H__F42C895A_CE31_46BE_B2E6_DC1A6F64E16C__INCLUDED_)
#define AFX_LAYERCONTOUR_H__F42C895A_CE31_46BE_B2E6_DC1A6F64E16C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "layerobject.h"
#include "rxON_Extensions.h"

#define CONTOUR_MISS		0
#define CONTOUR_HITSTART	1
#define CONTOUR_HITEND		2
#define CONTOUR_HITMID		4

class layercontour : public layerobject  
{
public:
	int GetContoursxyz(ON_SimpleArray<ON_Curve*> &pC); // returns in pC a copy of the curvelist.  You may destroy the list but not its contents.

	int GetContoursuv(ON_SimpleArray<ON_Curve*> &pC); // returns in pC a copy of the curvelist.  You may destroy the list but not its contents.
	ON_3dPoint m_x;
	int Trace();
	double GetZ() const;
	void SetZ(double z);
	layercontour();
	virtual ~layercontour();

private:
	int FindNextContourPoint( layerlink **p_e, int * revFlag, int *fromRight, ON_3dPointArray* pts);
	int Intersects(const layerlink *lnk) ;
	layerlink * FindStart();
	HC_KEY m_hk;
	double m_z;
	ON_SimpleArray<ON_Curve*> m_Curves;
};

#endif // !defined(AFX_LAYERCONTOUR_H__F42C895A_CE31_46BE_B2E6_DC1A6F64E16C__INCLUDED_)
