#include "StdAfx.h"
#include "RelaxWorld.h"
#include <set>
#include <QStringList>
#include <QRegExp>
#include <QDebug>

#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8
#else
#include "../../stringtools/stringtools.h"
#endif
#include "stringutils.h"
#include "RXAttributes.h"

#define OPENINGTEXT  L"("
#define CLOSINGTEXT  L"),"


QStringList RXAttributes::kwlist =RXAttributes::InitKWList();

QStringList RXAttributes::InitKWList()  // this is very dodgy
// we add a space, or a space-dollarm just before any words,
//this is because historically we just went 'xscaledyscaledets.'
// trouble is, many of these keywords appear as parts of other words, or of object names
// safer if we only allow atts with a keyword in this list.
// if we find one that isnt, we ask the user to edit the attstring.
{
    QStringList rv = QStringList()
            <<"3dm"
           <<"xabs"
          <<"xscaled"
         <<"yscaled"
        <<"flat"
       <<"draw"
      <<"fanned"
     <<"2dcurve"
    <<"base_mould"
    <<"someatts"
    <<"xyzabs"
    <<"stripe"
    <<"filament"
    <<"cut"
    <<"nocut"
    <<"snapendsonly"
    <<"snap"
    <<"nosnap"
    <<"sketch"
    <<"angle"
    <<"angle_tol"
    <<"basecurve"
    <<"beam"
    <<"buckle"
    <<"comment"
    <<"contact surface"
    <<"contactsurface"
    <<"cross"
    <<"cut_tol"
    <<"ea"
    <<"edge"
    <<"editable"
    <<"export_def"
    <<"export_coords"
    <<"export_reaction"
    <<"field"
    <<"fix"
    <<"fixed_length"
    <<"flength"
    <<"focus"
    <<"force_control"
    <<"geodesic"
    <<"gaussian_curvature"
    <<"grid"
    <<"Interpol"
    <<"layer"
    <<"linear_tol"
    <<"Make_Edit"
    <<"mass"
    <<"mat"
    <<"mat1"
    <<"mat2"
    <<"moulded"
    <<"nomold" <<"nomould"
    <<"n1"
    <<"n2"
    <<"name"
    <<"no_length_edit"
    <<"nobuckle"
    <<"nofilament"
    <<"noslide"
    <<"notrim"
    <<"onflag"
    <<"origin"
    <<"patch"
    <<"pocket"
    <<"psxx" <<"psyy"<<"psxy"
    <<"px"
    <<"py"
    <<"pz"
    <<"pressure_factor"
    <<"quadmap"
    <<"ref"
    <<"ref1"
    <<"ref2"
    <<"rlx"
    <<"rotfixity"
    <<"seed"
    <<"seg"
    <<"sksite"
    <<"slide"
    <<"slidecurve"
    <<"spline"
    <<"string"
    <<"ti"
    <<"tq"
    <<"trace"
    <<"transform"
    <<"trim"
    <<"trimmable"
    <<"uniform"
    <<"windfiles"
    <<"yabs"
    <<"yasx"
    <<"yscaled"
    <<"zi" <<"zt"
    <<"arclength"
    <<"e1w"
    <<"e2w"
    <<"intersect"
    <<"left"
    <<"right"
    <<"u"
    <<"v"
    <<"umax"
    <<"umin"
    <<"uv"
    <<"uvmap"
    <<"vmax"
    <<"vmin"
    <<"master"
    <<"slave"
    <<"layernames"
    <<"objnames"
    <<"noUVMap"
    <<"NoBeamPost"
    <<"post"
    <<"interp"
    <<"cspline"
    <<"spline"
    <<"seam"
    <<"curve"
    <<"noCompForce"
    <<"noTensileForce"
    <<"rigidbody"
    << "rank" // site connect rank

      ;
    return rv;
}


RXAttributes::RXAttributes(void)
{
}

RXAttributes::~RXAttributes(void)
{
}
RXAttributes:: RXAttributes(const RXAttributes &a)
{
    this->m_ww= a.m_ww;

}

RXAttributes::RXAttributes(const RXSTRING &s)
{
    QString q   = QString::fromStdWString(s).toLower();
    this->Splice (q);
}

RXAttributes:: RXAttributes(const char* s) 
{
    if(s) {
        QString q   =QString(s).toLower();
        this->Splice(q);
    }
}

RXSTRING RXAttributes::GetAll(const int stripBad) const
{
    QString q = this->Serialize(stripBad);
    return q.toStdWString();
}
void RXAttributes::Add(const QString &q)
{
    this->Splice( q );
}
void RXAttributes::Add(const RXSTRING &s)
{
    QString q = QString::fromStdWString( s);
    this->Splice( q );
}
//static
QString RXAttributes::Dollar(const QString & q)
{
    assert(!q.isNull());
    if(q.startsWith("$"))
        return q.trimmed();
    QString r = q.trimmed();
    return r.prepend("$");
}

bool RXAttributes::Remove (const QString &kw)
{
    // if the attribute contains the keyword and optionally any following '= value'
    // we remove it

    std::pair<QString,QString> qq= SplitAtt(kw);
    std::map<QString,QString >::iterator it;
    it = this->m_ww.find(Dollar(qq.first));
    if(it!=this->m_ww.end() ){
        this->m_ww.erase(it);
        return true;
    }
    return false;
}
QString   RXAttributes::Serialize(const int stripBad) const {
    QString rv, key,val;
    std::map<QString,QString>::const_iterator it;

    for(it=m_ww.begin(); it!=m_ww.end();++it)
    {
        key = it->first;
        val = it->second;
        if(!stripBad || kwlist.contains(key.mid(1) ,Qt::CaseInsensitive))
        {
            if(val.length())
                rv+=QString (",(")+key+QString("=")+val +QString(")");
            else
                rv+=QString (",")+key;
        }
        else {
            //cout<<"skip rewrite of att: `"<<qPrintable (key)<<"`"<<endl;
            continue;
        }
    }

    if(rv.startsWith(",")) rv.remove(0,1);
    return rv;
}
QString RXAttributes::GetPart(const QString what) const  // what is a comma-separated list of possibles
{
    QString rv, key,val;

    std::map<QString,QString>::const_iterator it;
    QStringList x = what.split(",");
    for(it=m_ww.begin(); it!=m_ww.end();++it)
    {
        key = it->first;
        val = it->second;
        if(!x.contains(key))
            continue;
        if(val.length())
            rv+=key +QString(" = ")+val +QString(",");
        else
            rv+=key +QString (",");
    }
    if(rv.endsWith(","))
        rv.chop(1);
    return rv;
}
QString RXAttributes::PrettyPrint()const
{
    QString rv, key,val;
    std::map<QString,QString>::const_iterator it;

    for(it=m_ww.begin(); it!=m_ww.end();++it)
    {
        key = it->first;
        val = it->second;

        if(val.length())
            rv+=QString("      %1").arg(key,-20) +QString(" = ")+val +QString("\n");
        else
            rv+=QString("      %1").arg(key,-20) +QString ("\n");

    }
    return rv;
}
// if the value contains $ or = or brackets
// there is a good chance that it was wrongly parsed.
int  RXAttributes::HeuristicCheck (QString &rv)const
{
    QString    key,val;
    int rc=0, bad=0;
    std::map<QString,QString>::const_iterator it;

    for(it=m_ww.begin(); it!=m_ww.end();++it)
    {
        key = it->first;
        val = it->second; bad=0;
        if(val.length())
        {
             if(val.contains( ")" ) && !val.contains( "(" ) )
            {
                bad=1; rc++;
                rv+=QString(">>>>>>      %1").arg(key,-20) +QString(" = ")+val +QString("<<<<<(bracket Match)\n");
            }
            else if(val.contains(QRegExp("[$\\=\\)\\,\\(]") )  )
            {
                bad=1; rc++;
                rv+=QString(">>>>>>      %1").arg(key,-20) +QString(" = ")+val +QString("<<<<<\n");
            }
            else
            {
                rv+=QString("            %1").arg(key,-20) +QString(" = ")+val +QString("\n");
            }
        }
        else
            rv+=QString("            %1").arg(key,-20) +QString ("\n");
    }
    return rc;
}
std::pair<QString,QString>  RXAttributes::SplitAtt(const QString & ps)
{
    QString s=ps;
    std::pair<QString,QString> rv;
    int k;
    if(s.startsWith("(") && s.endsWith(")"))
    {
        s.remove(s.count()-1,1);   s.remove(0,1);
    }
    s=s.trimmed();
    k = s.indexOf("=");
    if(k>0) {
        QString s2 = s.mid(k+1).trimmed();
        s=s.left(k).trimmed();
        rv.first=Dollar(s);
        rv.second=s2;
    }
    else
    {
        rv.first=Dollar(s);
        rv.second.clear();
    }
    return rv;
}

// generate the union of the existing atts and the string Q
// Q takes precedence. unless ThisHasPrecedence is true

bool RXAttributes::Splice(const QString &q, const int ThisHasPrecedence)//
{
    std::map<QString,QString> rv;  // OK its ugly to mix Qt and stdlib but icpc support for ISO C++ 2011 is 'experimental'
    std::map<QString,QString> ::iterator it;
    std::list<RXSTRING>::reverse_iterator rit;
    std::list<RXSTRING>b = Parse(q); /// parse is a static.

    if(ThisHasPrecedence)
    { //insert b first, then this on top of it
        for(rit=b.rbegin();rit!=b.rend();++rit)
        {
            QString x = QString::fromStdWString(*rit);
            rv.insert(SplitAtt(x ));
        }
        for(it=m_ww.begin();it!=m_ww.end();++it)
        {
            std::pair <QString,QString> p = std::make_pair(it->first, it->second );
            rv.insert(p);
        }
        m_ww=rv;
    }
    else // the nnew has precedence so throw it in on top.
    {
        for(rit=b.rbegin();rit!=b.rend();++rit)
        {
            QString x = QString::fromStdWString(*rit);
            m_ww.insert(SplitAtt(x));
        }
    }
    return true;
}

int RXAttributes::Grandfather(QString&q )
{
    int rc=0;
    if(!q.length())
        return 0;
    q=q.replace(QRegExp  ("\\)\\s*\\("), "),(");
    q=q.replace("n>>", " , ");
    q=q.replace("e>>", " , ");
    q=q.replace("e>", " , ");
    q=q.replace("n>", " , ");
    q=q.replace("$", " $");
    q=q.replace("$3dm", " , $3dm , ");
    q=q.replace(QRegExp  (",\\s*,"), ",");

    //   flat$xscaled$yscaled$string $nobuckle$draw xabs yabs yasx buckle, nobuckle fanned nomold
    //    for each KW in the list
    //            if we find it
    //            If preceded by '$' change to space KW (dont use commas because we may be in a value)
    //            else change to space $ KW
    //            end if
    //            end for

    return rc;
    int j,  nk = kwlist.count();
    for (int k=0; k<nk;k++)
    { QString &kw = kwlist[k];
        j = q.indexOf( kw);
        if(j<0)
            continue;

        if(j>0 && q[j-1]== '$')
            q.insert(j-1, " ");
        else
            q.insert(j, " $");
    }


    return rc;
}
bool RXAttributes::isSep(const int i, const  QChar* t, const int nc)
{
    // first, if we are inside brackets, return false.
    // second, if we are whitespace or comma, we are a separator
    // UNLESS we are whitespace and we can get to an '=' staying within whitespce.

    int j,hitl=0,hitr=0;
    int blcount=0; int brcount=0;
    const QChar *t1,*t2;

    // bracket test
    // 1) walk left, counting brackets until the score is negative .  '(' decrements, ')' increments
    // if final score is negative, we are in a bracket left so we need to check right
    // 2) walk right, counting brackets unti the score is negative .  ')' decrements, '(' increments
    // if final score is negative, we are in a bracket right too,  so we are iide brackets


    if(i>0 && i< nc -1 ){
        // if on the left, we find a left-bracket before we find an odd count of white-bracket
        for(j=i,t1=&t[i];j>=0;j--,t1--)
        {
            if(*t1== '('){
                blcount--;
                if(blcount<0) { hitl=true; break;}
            }
            else
                if(*t1== ')')
                    blcount++;
        }
        if(hitl)  {
            for(j=i,t2=&t[i];j<nc;j++,t2++)
            {

                if(*t2== ')'){
                    brcount--;
                    if(brcount<0) { hitr=true; break;}
                }
                else
                    if(*t2== '(')
                        brcount++;
            }
            if(hitr)
                return false;
        }
    } // if i...
    // we are not in brackets, so a `,` is a separator
    QChar c;
    c = t[i];
    if(c==',')
        return true;

    {
        bool isdiv =  c.isSpace();
        if(!isdiv) return false;
    }

    // here we are whitespace. We are NOT a sep if the first non-white char either fwd or back is an '='
    // walk forward
    //   nc=t.length();

    for (j=i;j<nc;j++) // walk forwards
    {
        if (t[j] == '=')
            return false;
        //  if(!isWhiteSpace(t[j]))   // need to check the other side
        if(!t[j].isSpace())
            break;
    }

    for (j=i;j>=0;j--) // walk backwards
    {
        if (t[j] == '=')
            return false;
        if(!t[j].isSpace())   // need to check the other side
            break;
    }
    // we get here if whitespace all the way to the end.
    return true;
}


// 2) we should verify the correctness of return ing false if the forward walk reveals an '='
// SPEED.  Parse/IsSep is slow.  Can we speed it up by
// A) working with the raw data not a std::wstring?
// B) staying with wstring but using iterators
// C) or even QString
bool RXAttributes::isSep(const int i,const RXSTRING t)
{
    // first, if we are inside brackets, return false.
    // second, if we are whitespace or comma, we are a separator
    // UNLESS we are whitespace and we can get to an '=' staying within whitespce.

    int j,hitl=0,hitr=0,isdiv,nc;
    int blcount=0; int brcount=0;

    nc=t.length();
    // bracket test
    // 1) walk left, counting brackets until the score is negative .  '(' decrements, ')' increments
    // if final score is negative, we are in a bracket left so we need to check right
    // 2) walk right, counting brackets unti the score is negative .  ')' decrements, '(' increments
    // if final score is negative, we are in a bracket right too,  so we are iide brackets


    if(i>0 && i< t.length()-1 ){
        // if on the left, we find a left-bracket before we find an odd count of white-bracket
        for(j=i;j>=0;j--)
        {
            const wchar_t &cc =t[j];
            if(cc== '('){
                blcount--;
                if(blcount<0) { hitl=true; break;}
            }
            else
                if(cc== ')')
                    blcount++;
        }
        if(hitl)  {
            for(j=i;j<nc;j++)
            {
                const wchar_t &cc =t[j];
                if(cc== ')'){
                    brcount--;
                    if(brcount<0) { hitr=true; break;}
                }
                else
                    if(cc== '(')
                        brcount++;
            }
            if(hitr)
                return false;
        }
    } // if i...
    // we are not in brackets, so a `,` is a separator
    wchar_t c;
    c = t[i];
    if(c==L',')
        return true;

    isdiv = ( isWhiteSpace(c));
    if(!isdiv) return false;
    // here we are whitespace. We are NOT a sep if the first non-white char either fwd or back is an '='
    // walk forward
    //   nc=t.length();

    for (j=i;j<nc;j++) // walk forwards
    {
        if (t[j] == '=')
            return false;
        if(!isWhiteSpace(t[j]))   // need to check the other side
            break;
    }

    for (j=i;j>=0;j--) // walk backwards
    {
        if (t[j] == '=')
            return false;
        if(!isWhiteSpace(t[j]))   // need to check the other side
            break;
    }
    // we get here if whitespace all the way to the end.
    return true;
}


std::list<RXSTRING> RXAttributes::Parse(const QString pq) // static
{
    // words are separated by comma, unless the comma is inside brackets.
    int i,j,k, i0,nc,inaSep;
    QString q = pq;

    Grandfather(q);
    const RXSTRING t =q.toStdWString();
    const QChar* cc = q.data();

    std::list<RXSTRING> ww;
    nc=t.length();
    k=0;inaSep=1;i0=0;
    for(i=0;i<nc;i++)
    {
#define    NEWSEPTEST
#ifdef NEWSEPTEST
        if (isSep(i,cc,nc))
#else
        if (isSep(i,t))
#endif

        {
            if(!inaSep) { // just finished a word
                ww.push_back( t.substr(i0,i-i0));
            }
            inaSep=1; continue;
        }
        // here the current char isnt a separator
        if(inaSep) // starting a new word;
        {
            i0=i; inaSep=0;
        }
    }

    if(!inaSep)
        ww.push_back( t.substr(i0 ));    // the last word

    return ww;
}

//static
int RXAttributes::Test(const RXSTRING &s){
    int rv=0;
    // testing of ChangeValue(L"$res", L"no");

#ifdef NEVER
    if(1){
        RXAttributes a("$res=yes"); a.ChangeValue(L"$res",L"notatall"); wcout<<a.m_rxatext.c_str()<<endl;
        RXAttributes b("($res=yes"); b.ChangeValue(L"$res",L"notatall"); wcout<<b.m_rxatext.c_str()<<endl;

        RXAttributes c("($res=yes)"); c.ChangeValue(L"$res",L"notatall"); wcout<<c.m_rxatext.c_str()<<endl;
        RXAttributes d("$res =yes"); d.ChangeValue(L"$res",L"notatall"); wcout<<d.m_rxatext.c_str()<<endl;
        RXAttributes e("$res  =yes"); e.ChangeValue(L"$res",L"notatall"); wcout<<e.m_rxatext.c_str()<<endl;
        RXAttributes f("$res= yes"); f.ChangeValue(L"$res",L"notatall"); wcout<<f.m_rxatext.c_str()<<endl;
        RXAttributes g("$res = yes"); g.ChangeValue(L"$res",L"notatall"); wcout<<g.m_rxatext.c_str()<<endl;
        RXAttributes h("($res=yes"); h.ChangeValue(L"$res",L"notatall"); wcout<<h.m_rxatext.c_str()<<endl;
        RXAttributes i("$res=yes)"); i.ChangeValue(L"$res",L"notatall"); wcout<<i.m_rxatext.c_str()<<endl;

        RXAttributes j("$res=yes,goat"); j.ChangeValue(L"$res",L"notatall"); wcout<<j.m_rxatext.c_str()<<endl;
        RXAttributes k("goat,($res=yes"); k.ChangeValue(L"$res",L"notatall"); wcout<<k.m_rxatext.c_str()<<endl;
        RXAttributes l("goat,($res=yes) goat"); l.ChangeValue(L"$res",L"notatall"); wcout<<l.m_rxatext.c_str()<<endl;
        RXAttributes m("goat,$res=yes)"); m.ChangeValue(L"$res",L"no"); wcout<<m.m_rxatext.c_str()<<endl;
        RXAttributes n("goat,$res=  yes,wombat"); n.ChangeValue(L"$res",L"notatall"); wcout<<n.m_rxatext.c_str()<<endl;

        RXAttributes o("goat,$res=  yes,wombat"); o.ChangeValue(L"$rEs",L"no"); wcout<<o.m_rxatext.c_str()<<endl;
        {RXAttributes a("$res="); a.ChangeValue(L"$res",L"no"); wcout<<a.m_rxatext.c_str()<<endl;}
        {RXAttributes a("$res"); a.ChangeValue(L"$res",L"notatall"); wcout<<a.m_rxatext.c_str()<<endl;}
        {RXAttributes a("$res,(next)"); a.ChangeValue(L"$res",L"notatall"); wcout<<a.m_rxatext.c_str()<<endl;}
        {RXAttributes a("$res,next"); a.ChangeValue(L"$res",L"notatall"); wcout<<a.m_rxatext.c_str()<<endl;}
    }
#endif
    return 1;
}

bool RXAttributes::ChangeValue(const QString& key, const QString & val)
{
    //  Returns True if anything changed
    std::map<QString,QString >::iterator it;
    it = this->m_ww.find(Dollar(key));
    if(it!=this->m_ww.end()  ){
        if(val==it->second)
            return false;
    }
    this->m_ww[Dollar(key)]=val;
    return true;
}
bool RXAttributes::ChangeValue(const RXSTRING & pkey, const RXSTRING & pvalue)
{
    // Returns True if anything changed
    QString key=QString::fromStdWString(pkey);
    QString val=QString::fromStdWString(pvalue);
    return this->ChangeValue(key,val);
}


bool RXAttributes::Extract_Word(const QString &qkey, QString &val) const
{
    std::map<QString,QString>::const_iterator it;
    it = this->m_ww.find(Dollar (qkey));
    if(it!=this->m_ww.end()  ){
        val = it->second;
        return true;
    }
    val.clear();
    return false;
}
QString RXAttributes::Extract_Word(const QString &qkey, bool &found ) const
{
    QString val;
    found = Extract_Word( qkey,val);
    return val;
}
bool RXAttributes::Extract_Word(const RXSTRING &key, RXSTRING &result) const
{
    QString r, qkey=QString::fromStdWString(key);
    std::map<QString,QString>::const_iterator it;
    it = this->m_ww.find(Dollar(qkey));
    if(it!=this->m_ww.end()  ){
        r = it->second;
        result=r.toStdWString();
        return true;
    }
    return false;
}

RXSTRING RXAttributes::Extract_Word(const RXSTRING &key, bool &found ) const
{
    QString qresult =  Extract_Word (QString::fromStdWString(key),found);
    return qresult.toStdWString();
}

int RXAttributes::Extract_Float (const RXSTRING &keyw,float*v)const {
    double d;
    int rval;
    rval = Extract_Double (keyw,&d);
    *v = (float) d;
    return rval;
}

int RXAttributes::Extract_Float(const char*keyw,float*v)const {
    double d;
    int rval;
    rval = Extract_Double ( keyw,&d);
    *v = (float) d;
    return rval;
}

int RXAttributes::Extract_Double  (const RXSTRING &keyw,double*v) const {
    RXSTRING w;
    if(!Extract_Word(keyw,w)) {
        *v=ON_UNSET_VALUE;
        return 0;
    }
    wchar_t *stop=0;
    const wchar_t *lp = w.c_str();
    *v = wcstod(lp,&stop);
    int nr = stop-lp;
    int nc = w.length();
    if(nr != nc){
        if(nr>0)
            qDebug()<<" Extract_Double on" <<QString::fromStdWString(w) <<", length="<<nc <<"nread="<<nr;
        return 0;
    }
    return 1;
}
int RXAttributes::Extract_Double (const char*keyw,double*v) const{
    int rc=0;
    rc=this->Extract_Double(TOSTRING(keyw),v);
    return rc;
}

int RXAttributes::Extract_Integer  (const RXSTRING &keyw,int*v) const {
    /* somewhere in <att> there may be  a sequence <keyw [whitespace]=[whitespace]<int>	*/
    RXSTRING w;
    if(!Extract_Word(keyw,w)) {
        *v=-1;
        return 0;
    }
    wchar_t *stop;
    const wchar_t *lp = w.c_str();
    *v = wcstol(lp,&stop,10);
    return 1;
}
int RXAttributes::Extract_Integer ( const char*keyw,int*v)const {
    return  this->Extract_Integer(TOSTRING(keyw),v);
}

bool RXAttributes::Find(const RXSTRING &key )const	{
    QString q = QString::fromStdWString(key);
    return  this->Find(q) ;
}

bool RXAttributes::Find(const QString &q )const
{
    return(this->m_ww.find(Dollar(q)) !=m_ww.end()) ;
}

RXAttributes::AttParserNode::AttParserNode(void)
    :m_op(' ')
    ,parent(0)
    ,level (0)
{

}
RXAttributes::AttParserNode::AttParserNode(class AttParserNode *p,const QChar op)
    :m_op(op)
    ,parent(p)
    ,level (0)
{
}
RXAttributes::AttParserNode::~AttParserNode(void)
{
    std::list<class AttParserNode *> ::iterator it;
    for( ; children.size();)
    {
        class AttParserNode *c= children.front();
        children.pop_front();
        delete c;
    }
    m_text.clear();

}

class RXAttributes::AttParserNode* RXAttributes::AttParserNode::Up()
{
    return parent;
}
class RXAttributes::AttParserNode* RXAttributes::AttParserNode::Down(const QChar op)
{
    class AttParserNode *c=new   AttParserNode(this,op) ;
    c->level= this->level+1;
    children.push_back(c);
    return c;
}
void  RXAttributes::AttParserNode::Append(const QChar c)
{
    this->m_text+=c;
}

QString  RXAttributes::AttParserNode::Serialize()
{
    QString rv;
    if(!m_text.isEmpty()){
        rv=QString("  {%1}").arg(level); rv+=m_op;
        rv+=  m_text;
    }
    std::list<class AttParserNode *> ::iterator it;
    for(it=children.begin() ; it!=children.end(); ++it)
    {
        class AttParserNode *c= *it;
        rv+= c->Serialize();
    }

    return rv;
}






