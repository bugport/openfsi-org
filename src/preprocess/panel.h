 /* this   is panel.h 27/5/94 
10.6.96 angletol increased from 0.0001 to 0.001 and back again to 0.00025
*/
#pragma once
 
#include "opennurbs.h"
#include "griddefs.h"
#include "vectors.h"

class RXOffset; 
class RXSail;

EXTERN_C int Update_Xabs_Offset(sc_ptr p);
EXTERN_C int Measure_End_Angles(sc_ptr  p,int seamflag,int Use_Gauss_EA ,double e1a,double e2a);

EXTERN_C int Make_Absolute_BaseCurve(VECTOR*p,int c,float Chord,double tol) ;

EXTERN_C int PC_Pull_Pline_To_Mould( RXEntity_p mould,VECTOR**p, int *c,int adaptive,double tol);
EXTERN_C int PC_Pull_SC_To_Mould(RXEntity_p sce);

int Scale_Base_Curve(VECTOR *Bin,VECTOR* base,int bc,float depth,int Xabs,int Yabs,int YasX,double length);  
int Make_Fanned_Experimental(sc_ptr  sc,int flag,float *arc, double *a0,float chord, int *c, VECTOR **p);
int Make_Fanned_(sc_ptr  sc,int flag,float *arc, double *a0,float chord, int *c, VECTOR **p);
int Sort_IA(struct IMPOSED_ANGLE*ia,int Nia);

int Sort_Nodes(SAIL*p_s);
int Get_Position_By_Double(const class RXEntity* ptr,const double offin,const int side,VECTOR *v);
int Get_Position_By_Double(const class RXEntity* ptr,const double offin,const int side,ON_3dPoint &v);
int Get_Position_By_Double(const class RXEntity* ptr,const double offin,const int side,class RXSitePt *v);

int Get_Position(const RXEntity_p ptr, RXOffset *offin,const int side,VECTOR *v);
int Get_Position(const RXEntity_p ptr, RXOffset *offin,const int side,ON_3dPoint *v);
int Get_Position(const RXEntity_p ptr, RXOffset *offin,const int side,class RXSitePt *v);

ON_3dPoint *Compute_SiteWhichSnap(Site *p,ON_3dPoint *v1,ON_3dPoint *v2);
 
int Compute_Sign(const ON_3dPoint &a, const ON_3dPoint &b, const ON_3dPoint &c, const ON_3dPoint &d,const ON_3dVector &z); 
int Compute_Entity_Endsign(RXEntity_p p,const ON_3dPoint & e1,const ON_3dPoint & e2);

int Crank_Polyline(VECTOR **p,int *c,float x[],double a[],int *crank,int Nin);

int rxdecrement(int j,int count); 
int rxincrement(int j,int count);
sc_ptr  Get_Compound_Curve_Element(const class RXEntity* ptr, RXOffset *offin,RXOffset **offout);
EXTERN_C int Make_All_Psides(SAIL *sail); 
EXTERN_C int Paneller(SAIL *sail); 


EXTERN_C int Grow_Panel(PSIDEPTR  enext,PSIDEPTR  p, Panelptr  panel,RXEntity_p estart);
EXTERN_C int Coalesce_Cranks(float x[],double a[],int *crank,int N);

