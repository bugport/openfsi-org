#ifndef _RXENTITY_HDR_8BD53B9D89444812B2D261814134BB8A
#define _RXENTITY_HDR_8BD53B9D89444812B2D261814134BB8A
#include "RXObject.h"
#include <QString>
#include "RXENode.h"
#include "linklist.h"
#include "RXAttributes.h"
#ifndef HC_KEY
#error("Need HC_KEY defined")
#endif
class RXSail;
using namespace std;

class RXEntity  :
        virtual public RXObject,public RXENode
{
public:
    RXEntity(void);
    RXEntity(class RXSail*p);
    virtual ~RXEntity(void);
    virtual int CClear();
    int UnResolve(void){	return  this->CClear (); }
    int Kill();
public:
    const char *type()const{return m_type;}
    const char *name()const;
    const char *GetLine()const{return line;}
    void SetLineLwr() ;
//private:
    char* line;// can be private except for string and bcurve grandfathering
public:
    void SetLine(const char*s ) { if(line) RXFREE(line); this->line=STRDUP(s); }
    void SetLine(const QString s ) {if(line) RXFREE(line); this->line=STRDUP(qPrintable(s)); }

    int Rename(const char*namein,const char*type);
    int SetTypeName(const char*p_type, const char*p_name) ;
    virtual int PC_Finish_Entity(const char *type,
                                 const char *name,void *dp,HC_KEY hk,
                                 struct LINKLIST *plist,const char *att,
                                 const char *line);

    virtual int PC_Finish_Entity(const char *type,
                                 const char *name,   const RXAttributes &att,
                                 const char *line);

    QString AttributeString(const char* prefix =0 ) const;
    QString AttributesPrettyPrint() const;
    int AttributeSet(const char*what, const char*val=0);
    int AttributeSet(const RXSTRING what, const RXSTRING val);
    int AttributeSet(const QString &what, const QString &val);

    virtual bool AttributeGet(const RXSTRING what,RXSTRING &value) const;
    virtual bool AttributeGet(const char*what,char*value=0) const;

    virtual bool AttributeGet(const QString  what, QString &value) const;
    virtual bool AttributeGetDble(const QString & what, double*value) const;
    virtual bool AttributeGetInt(const QString & what, int*value) const;

    virtual bool AttributeAdd(const QString &newAtt);
    virtual bool AttributeRemove(const QString &AttToKill);
    virtual bool AttributeFind(const QString &key )const;
    virtual int AttributeDestroy( );

    RXAttributes m_rxa;

    int Needs_Finishing; // only used as boolean
    int Needs_Resolving; // only used as boolean
    int m_CHANGED; /*   used  in UpdateActive, probably boolean but not confirmed*/

    int edited;		// looks like bool
    int generated; // integer. value is the depth of the entity( ie no of generations from the file entity)
    int TYPE;	// integer, but limited in range.  currently -99 to about 200.

    HC_KEY hoopskey;

    RXSail *Esail;

    ONX_Model_Object  * m_pONMO;

protected:
    void SetLine(const RXSTRING &s);
    void SetLine(const std::string &s);

private:
    char *m_type; char*m_name;
public:

    // override at least these five methods
    virtual int Dump(FILE *fp) const=0;
    virtual int Compute(void)=0; // return 1 if the object moved requiring compute of its nieces
    virtual int Resolve(void)=0;
    virtual int Finish(void)=0;
    virtual int ReWriteLine(void)=0;
    virtual QString Descriptor()const;
    virtual void* DataPtr(void) const{
        cout << " DataPtr on RXEntity " <<endl;
        assert(0); return 0;
    }
    virtual void  SetDataPtr(void* ptr) { assert(0); }
    virtual int Print_One_Entity( FILE *fp) const ;
    virtual void SetNeedsComputing(const int i=1);
    int Recursive_Solve(const int depth,const class RXSail *cSail);

    virtual ON_Xform Trigraph(void)const;
    virtual ON_3dVector Normal(void)const ;
};
#endif
