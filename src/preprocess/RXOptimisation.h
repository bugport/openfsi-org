#ifndef _RXOPTIMISATION_H_
#define _RXOPTIMISATION_H_

#include "RXOptVariable.h"

typedef RX_DP (*RXOptFunctionPtr)	(class RXOptimisation *p_Opt, void *p_user_data, ON_SimpleArray<double> &p_x);
typedef RX_DP(RXMinimand)			(class RXOptimisation *p_Opt, void *p_user_data, ON_SimpleArray<double> &p_x);

RXMinimand rxo_testfunc;



class RXOptimisation
{
//	friend RXOptimisationCaller;
public:
	RXOptimisation(void);
	RXOptimisation( void*p);
public:
	virtual ~RXOptimisation(void);

	int GetNCalls(){return m_nfuncCalls;};
	virtual int Print(FILE *fp);
	virtual bool FindMinimum();  // uses the solvers internal x0,scales etc and finds the values of x, which minimise the goal function. 
	virtual int ExtractResults(RXOptVarList *p_OptVarList); //Fills in the value,scale,x0  members of p_OptVarList by copying from the solvers m_X array. 
	virtual int SetValues(RXOptVarList *p_OptVarList,ON_SimpleArray<double> &p_x) ;  //Fills in the value members of p_OptVarList by copying from p_x. 
	virtual int SetStart (RXOptVarList *OptVarList);
	virtual void SetCaller(class RXOptimisationCaller *p);
	class RXOptimisationCaller *GetCaller(){return m_caller;}
	void SetGoalFunction (
			RXOptFunctionPtr p_new_function,
			void * p_user_data=0) {
			m_goal_function= p_new_function;
			m_goal_function_user_data=p_user_data;
		};

virtual	RX_DP func(ON_SimpleArray<double> p_x) ;
	 

protected:
	ON_SimpleArray<double> m_x, m_x0, m_scale;
	int m_nfuncCalls; 
 	RXOptFunctionPtr m_goal_function;
	void *m_goal_function_user_data;
	class RXOptimisationCaller *m_caller;
    RX_DP m_tol;
	int m_nmax;
public:
    void SetTolerance(const RX_DP  p_tol);
	void SetMaxCycles(const int  p_n);
};

#endif

