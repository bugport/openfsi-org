/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
   March 1997. CHeck whether Leading and Trailing are Computed before using
   Nov 96 xy to deflected removed in favour of get global coords because
   the latter handles moulded sails correctly
   15/3/96  lower_U,Upper U lower_V Upper V implemented
   Normally they would be 0.0 or 1.0, but if we want to clip off....
   the foot  we set Lower_V = 0.05
   the head  we set Upper_V = 0.9
   the Luff  we set Lower_U = 0.05
   the Leech we set Upper_U = 0.9
   THis provides a work-around for severe distortion in the foot shelf
   which can throw PANSAIL badly on mainsails with foot-wakes.

  9/3/96 ifdefs for MSVC
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include <QDebug>
#include "RXSail.h"
#include "RelaxWorld.h"
#include <sstream>
#include "RXEntityDefault.h"
#include "RX_FESite.h"
#include "RXSeamcurve.h"
#include "global_declarations.h"
#include "batdefs.h"
/* file pansin.c 23/4/94  
  routines for dealing with interpolation into pansail
*/			   
#include "panel.h"
#include "entities.h"
#include "interpln.h"
#include "finish.h"
#include "iges.h"
#include "etypes.h"

#include "pansin.h"

int interp_(int* const loadindex,float*const pu,float*const pv,float*x,float*y,float *z) {

    /* For PANSAIL ONLY. returns x and y given u and v. For USAERO may have to return velocities
Note. Called with Pansail Model No unlike the other interp_ routines*/

    SAIL *s;
    double u = *pu,v=*pv;
    if(*loadindex < 0)
        return(0);

    s = RXSail::LoadIndexToModel(*loadindex);
    if(!s) {
        char buf[256];
        (*x) =  u; (*y)=  0.0; (*z) = v;  /* insurance */
        sprintf(buf,"(pansail IN) index %d not FOUND",*loadindex);
        g_World->OutputToClient(buf,3);
        return(0);
    }
    ON_3dPoint srot;
    srot = s->CoordsAt(ON_2dPoint(*pu,*pv),RX_GLOBAL);
    double l_AWA = g_World->m_wind.Get(RXWindGeometry::AWA );
    *x = srot.x * cos(l_AWA) - srot.y*sin(l_AWA);
    *y = srot.y*cos(l_AWA) + srot.x * sin(l_AWA);
    *z = srot.z;
    //if(1)
    //cout<< "interp "<< u<<" "<<v<<" "<<*x<<" "<<*y<<" "<<*z<<endl;
    return(1);
}

int RXSail::Write_Iges(const char*fname,const int nu,const int nv,const int degree) {

    RXEntity_p e;
    ON_3dPoint srot;
    int i,j,ok=0;
    double u=0.5,v=0.5;
    stringstream b;
    b<<"Interpolation surface:dummy:nurbs:NULL: : :	nv="<<nu<<" nu="<<nv<<" ku="<<degree<<",kv="<<degree<<" head=0 tail=0 :table";
    qDebug()<<  "igesout   u   v   srot.x  srot.y  srot.z";
    for(i=0;i<nu;i++){
        u = (double)i/(double)(nu-1);
       // qDebug()<<" "<<u<<endl;
        for(j=0;j<nv;j++){
            v = (double)j/(double)(nv-1);
            srot =this->CoordsAt(ON_2dPoint(u,v),RX_GLOBAL);
            b<<endl<<srot.x<<" \t"<<srot.y<<" \t"<<srot.z;
           // qDebug()<<u<<v<<"  \t"<<srot.x <<srot.y <<srot.z;
        }
    }
    b<<"\n";
    i=1;
    e = Just_Read_Card(this,b.str().c_str (),"interpolation surface","dummy",&i);
    if(e){
        if(  Resolve_Interpolation_Card (e)){
            Finish_ISurface_Card (e);
            struct PC_INTERPOLATION *pi = (struct PC_INTERPOLATION *)e->dataptr;
            if(Compute_Interpolation_Card(e))
                ok = iges_out_128 (fname,this->GetType().c_str(), pi->NurbData);
            else
                rxerror("Compute_Interpolation failed",2);
            e->Kill();
        }
    }
    if(!ok)
        cout<<"failed to ";
    cout<<"write igs <"<<fname<<endl;

    return(ok);
}

