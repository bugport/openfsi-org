#pragma once


#include "opennurbs.h"
class  layernode;
extern int meyerBarycentric(const ON_2dPoint& p, const ON_2dPointArray &q,  ON_SimpleArray<double> *w);

extern int meyerBarycentric(const ON_2dPoint& p, 	ON_SimpleArray<layernode *> &q,  ON_SimpleArray<double> *w);
extern double cotangent(const ON_2dPoint& a, const ON_2dPoint& b, const ON_2dPoint& c);
 

