/* header file for nlm.c */
#ifndef _NLM_H_
#define _NLM_H_

#include "griddefs.h"
class RX_FETri3 ;
#ifdef FORTRANLINKED
	#define Polyline_Interp fortpolyline_interp_
#else
	#define Polyline_Interp Polyline_Interp_clanguage
#endif


EXTERN_C void Polyline_Interp(double* x,double*y,int*N,int*s,double *x0,double*yret,double *grad,int*err); // fortran

//EXTERN_C int print_triangle_data(FILE *fp,FortranTriangle *te,int ftrino,SAIL *p_sail);
EXTERN_C void Get_Tri_Ref_Angle(int *trino,double *angle);
#endif

