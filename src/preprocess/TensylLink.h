#pragma once
#include <QString>
 class RX_PCFile;
 class CTensylProp;

class CTensylLink
{
public:
	CTensylLink(void);
	CTensylLink(int p_nn,int p_n1,int p_n2,double p_Slacklength,int p_prop,
		int p_Control,int p_Boundary,
		int p_Field,const int pRow,const int pCol,
                class RX_PCFile*f,
                    QString att);

	~CTensylLink(void);
	int Print(FILE *fp);
	int m_nn;
	int m_fnn; // the fortran index
	int GetPropNo() const { return m_prop;}
 
#ifndef PCIO
	int MakeElementProperties(CTensylProp *p , class RX_PCFile *pcf, double &ea,double &zi,double &ti) const;
	double DeflectedLength() const;
        QString Atts() const {return m_atts;}
#endif
protected:
	// these are the original node indices read from the PC file. They are keys into the map m_nodes. 
	// After nodes have been merged. they will point to the wrong place.
	int m_nk1;
	int m_nk2;
	// to derefernce the  node we go f->m_nodes[m_nkx].m_nn.  and then MasterNodeNo on that (I think)
        QString m_atts;
public:
	double m_Slacklength;
	int m_prop;
	int m_Control; // 1 means m_slacklength is ZI in inches. 0 means m_slacklength is TI in lb
	int m_Boundary;
	int m_FieldForLink;
	int  m_Row;
	int  m_Col;
	int GetStartNode() const;	// to derefernce the  node we go f->m_nodes[m_nkx].m_nn.  and then MasterNodeNo on that (I think)
	int GetDestNode()const;
#if !defined(RXRHINOPLUGIN) && !defined(RXPHYSX) 

	class RXSeamcurve *m_e;
#else
	void *m_e;
#endif
protected:
	class RX_PCFile*m_f;
};
