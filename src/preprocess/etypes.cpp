/* etypes.c returns an integer based on a keyword
  return values determined by etypes.h
 4/12/95 gausscurve added

   */
#include "StdAfx.h"
#include "stringutils.h"
#include  "etypes.h"

int PC_Local_Entity_Type(const char*Firstword) {

    assert(	SEAM==	SEAMCURVE);
    assert(	SEAM==	EDGE);
    assert(	SEAM==	CURVE);

    if (rxIsEmpty(Firstword)) 					return(0);
    else if	(streq(Firstword,"seamcurve"))		return(	SEAMCURVE);
    else if	(streq(Firstword,"site"))			return(	SITE);

    else if	(strieq(Firstword,"seam"))			return(	SEAM);
    else if	(strieq(Firstword,"seamcurve"))		return(	SEAMCURVE);
    else if	(strieq(Firstword,"edge")) 			return(	EDGE);
    else if	(strieq(Firstword,"curve")) 		return(	CURVE);


    else if	(strieq(Firstword,"site"))			return(	SITE);
    else if	(strieq(Firstword,"relsite")) 		return(	RELSITE);
    else if (strieq(Firstword,"string")) 		return(PCE_STRING);

    else if	(strieq(Firstword,"2dcurve")) 		return(	TWODCURVE);
    else if	(strieq(Firstword,"basecurve")) 	return(	BASECURVE); //SSD
    else if	(strieq(Firstword,"3dcurve")) 		return(	THREEDCURVE);
    else if	(strieq(Firstword,"spline")) 		return(	PCE_SPLINE);
    else if	(strieq(Firstword,"uvcurve")) 		return(	PCE_UVCURVE);
    else if	(strieq(Firstword,"gausscurve"))	return(GAUSSCURVE);

    else if	(strieq(Firstword,"drawinglayer"))	return(PCE_DWGLAYER);
    else if	(strieq(Firstword,"scalar")) 		return(PCE_SCALAR);
    else if (strieq(Firstword,"3dm")) 			return(PCE_3DM);
    else if (strieq(Firstword,"iges")) 			return(PCE_IGES);
    else if (strieq(Firstword,"dxf")) 			return(DXF);
    else if (strieq(Firstword,"dovefile")) 		return(PCE_DOVE);
    else if (strieq(Firstword,"pcfile")) 		return(PCE_PCFILE);
    else if (strieq(Firstword,"nodecurve")) 	return(PCE_NODECURVE );

    else if (strieq(Firstword,"pansail")) 		return(PANSAIL);
    else if	(strieq(Firstword,"material"))      return(MATERIAL);
    else if	(strieq(Firstword,"fixity")) 	   	return(PCE_FIXITY);
    else if	(strieq(Firstword,"field"))	   	return( FIELD);
    else if	(strieq(Firstword,"interpolation surface")) 	return( INTERPOLATION_SURFACE);
    else if	(stristr(Firstword,"stripe")) 	 return(	STRIPE);
    else if	(strieq(Firstword,"connect")) 	return( CONNECT);
    else if	(strieq(Firstword,"name")) 	   	return(  PCE_NAME);

    else if	(strieq(Firstword,"gendp")) 		return(	 GENDP);
    else if	(strieq(Firstword,"grid density")) 	return(	 GENDP);
    else if	(strieq(Firstword,"input")) 		return(	 INPUT);

    else if	(strieq(Firstword,"rename"))		return(	 RENAME);
    else if	(strieq(Firstword,"alias")) 	  return(PCE_ALIAS);

    else if	(strieq(Firstword,"compound curve"))  return(COMPOUND_CURVE);


    else if	(strieq(Firstword,"regiondata")) 	return(	PC_PANELDATA);
    else if	(strieq(Firstword,"paneldata")) 	return(	PC_PANELDATA);  //  obsolete
    else if	(strieq(Firstword,"patch"))			return(	PATCH);
    else if	(strieq(Firstword,"pocket")) 		return(	POCKET);

    else if	(strieq(Firstword,"batten")) 		return(	BATTEN);
    else if	(strieq(Firstword,"fabric")) 		return(	FABRIC);

    else if	(strieq(Firstword,"panel")) 		return(	PANEL);
    else if	(strieq(Firstword,"pshell")) 		return(	PCE_PSHELL);
    else if	(strieq(Firstword,"rigidbody")) 	return(PCE_RIGIDBODY);

    else if	(strieq(Firstword,"set"))           return(	PCE_SETDATA);
    else if	(strieq(Firstword,"include")) 		return(	INCLUDE);
    else if	(strieq(Firstword,"block")) 		return(	INCLUDE);
    else if	(strieq(Firstword,"panelfab"))		return(	PANELFAB);

    else if	(strieq(Firstword,"mould"))  		return(	PCE_MOULD);

    else if	(strieq(Firstword,"nurbs"))           return(PCE_NURB );

    else if	(strieq(Firstword,"layer"))           return(PCE_LAYER);

    else if	(strieq(Firstword,"pside"))           return(PCE_PSIDE);
    else if	(strieq(Firstword,"sail"))            return(PCE_SAIL);

    else if	(strieq(Firstword,"cut"))           return(PCE_CUT);

    else if (strieq(Firstword,"edit"))			return(EDIT);
    else if (strieq(Firstword,"editword"))		return(EDITWORD);
    else if (strieq(Firstword,"linear substructure"))   return(PCE_GLE);
    else if (strieq(Firstword,"delete"))		return(PCE_DELETE_CARD);
    else if (strieq(Firstword,"pressure"))		return(PCE_PRESSURE);
    else if (strieq(Firstword,"contact surface"))	return(PCE_CONSURF);
    else if (strieq(Firstword,"beamstring"))		return(PCE_BEAMSTRING);
    else if (strieq(Firstword,"beammaterial"))		return(PCE_BEAMMATERIAL);
    else if (strieq(Firstword,"vector field"))		return(PCE_VECTORFIELD);
    else if (strieq(Firstword,"import"))    		return(PCE_IMPORT);
    else if (strieq(Firstword,"submodel"))    		return(PCE_SUBMODEL);

// below are the really rare ones - not to say obsolete

    else if (strieq(Firstword,"do_boat"))		return(DO_BOAT);
    else if	(strieq(Firstword,"TARGET"))		return(PCE_GOAL);
    else if	(strieq(Firstword,"VARIABLE"))		return(PCE_GOAL);
    else if	(strieq(Firstword,"MEASURE"))		return(PCE_GOAL);
    else if	(strieq(Firstword,"ZONE"))          return(PCE_ZONE);
    else if	(strieq(Firstword,"question"))		return(PC_QUESTION);

    else if (strieq(Firstword,"datfile"))		return(DATFILE);
    else if (strieq(Firstword,"mshfile"))		return(MSHFILE);
    else if (strieq(Firstword,"infile"))		return(INFILE);
    else if	(strieq(Firstword,"coordinate system"))  	  return(	PCE_COORDS);
    else if	(strieq(Firstword,"pansail wake"))  	  return(PCE_WAKE);
    else if	(strieq(Firstword,"streamline"))  	  return(PCE_STREAMLINE);
    else if	(strieq(Firstword,"windvane"))  	  return(PCE_VELOCITY);
    else if	(strieq(Firstword,"batpatch")) 		assert(0); //return(	BATPATCH);
    else if	(strieq(Firstword,"camera")) 	return(   PCE_CAMERA);
    else if	(strieq(Firstword,"display")) 	return(	 DISPLAY);

    else if (strieq(Firstword,"deleted")) 	{assert(0);	return(PCE_DELETED);}
    else if	(strieq(Firstword,"summary"))	return(	  PCE_SUMMARY);
    else if	(strieq(Firstword,"boundary")) 	return(	   BOUNDARY);
    else if	(strieq(Firstword,"aeromik")) 		return(	AEROMIK);
    else if	(strieq(Firstword,"aero")) 	  		return(PCE_AERO);

    else if	(strieq(Firstword,"rlx"))             return(RLX);
    else if	(strieq(Firstword,"tpn"))           return(	PCE_TPN);


    return(0);
}

int INTEGER_TYPE(const char*Firstword) {
    int i = PC_Local_Entity_Type(Firstword);
    assert(i <RX_MAX_TYPES);
    return i;
}

