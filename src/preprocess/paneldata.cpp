
/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Part of meshlib.a
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
#include "StdAfx.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include "RXPanel.h"
#include "RXPside.h"
#include "words.h"
#include "panel.h"
#include "etypes.h"
#include "entities.h"
#include "resolve.h"
#include "stringutils.h"

#include "paneldata.h"


int Resolve_RegionData_Card(RXEntity_p e){
    //regiondata:name: s1:s2:s3:... s(n)  : atts
    char *atts, *line=STRDUP(e->GetLine());
    char **wd=NULL;
    QString  qmatstr,qrefstr;
    RXEntity_p sco, *List, mat=NULL,ref=NULL;
    struct PANELDATA *np;
    int i,bad=0, iret =0;
    int nw=make_into_words(line,&wd,":");
    if(nw<1) { printf(" bad regiondata card %s\n",e->name()); return(0);}
    atts = wd[nw-1]; strtolower(atts);
    RXAttributes rxa(wd[nw-1]);
    List = (RXEntity_p *)CALLOC((nw-1),sizeof(void*));
    for(i=2;i<nw-1;i++) {
        sco = e->Esail->Get_Key_With_Reporting( "curve,seamcurve,seam,edge,nodecurve",wd[i]);
        if(!sco) { bad=1; break;}
        List[i-2]=sco;
    }
    if(rxa.Extract_Word( "$mat",qmatstr)){
        mat= e->Esail->Get_Key_With_Reporting("fabric,material",qPrintable( qmatstr));
        if(!mat) bad=1;
    }
    if(rxa.Extract_Word( "$ref",qrefstr)){
        ref = e->Esail->Get_Key_With_Reporting( "curve,seamcurve,seam,edge,nodecurve",qPrintable( qrefstr));
        if(!ref) bad=1;
    }

    if(!bad){
        np = (PANELDATA *)CALLOC(1,sizeof(struct PANELDATA));
        np->list=List;
        np->c=nw-3;
        np->owner=e;
        np->refSC=ref;
        np->mat=mat;

        if(e->PC_Finish_Entity("regiondata",wd[1],np,(long)0,NULL,atts,e->GetLine())){
            e->Needs_Finishing=1;
            e->Needs_Resolving=0;
            iret = 1;
        }
    }
    else
        RXFREE(List);
    RXFREE(wd);
    RXFREE(line);
    return iret;
}
int Create_PanelData_Entity(Panelptr   pan, RXEntity_p mat, RXEntity_p ref){
    //regiondata:name: s1:s2:s3:... s(n)  : atts
    int depth=0;
    int l, k;

    char str[256],name[256];
    char *line = (char *)MALLOC(64+strlen(pan->name()));
    sprintf(name,"pdata_for_%s", pan->name());
    sprintf(line,"regiondata : %s : ",name);
#ifdef DEBUG
    if(!pan->m_psidecount)
        cout<<"Wierd. create a panelData when the panel  has no edges"<<endl;
#endif


    for (l=0;l<pan->m_psidecount;l++) {
        PSIDEPTR pss = pan->m_pslist[l];
        k = strlen(line) +strlen(pss->sc->name() ) + 4+2;
        line = (char *)REALLOC(line,k);
        strcat(line,pss->sc->name());	strcat(line," : ");
        k = strlen(line);
    }
    if(ref) {
        sprintf(str,"($ref=%s)",ref->name());
        k=strlen(line)+strlen(str);
        line = (char *)REALLOC(line,k+2+2);
        strcat(line,str);
        k = strlen(line);
    }
    if(mat) {
        sprintf(str,"($mat=%s)",mat->name());
        k=strlen(line)+strlen(str);
        line = (char *)REALLOC(line,k+2+2);
        strcat(line,str);
        k = strlen(line);
    }
    RXEntity_p rde = Just_Read_Card(pan->Esail ,line,"regiondata",name,&depth);
    //rde->SetRelationOf(pan,frog|parent,RXO_RD_OF_PANEL);
    // the RD is created by the user not by the panel
    pan->Esail->SetFlag(FL_NEEDS_SAVING);
    RXFREE(line);
    return 1;
}
RXEntity_p RXPanel::Get_PanelData_Material(){
    /*
1)  See if any paneldata entities have an SC list corresponding to  this one.
2)	If it does, return matkey. AND ATTACH THE PANELDATA TO THE PANEL		
        */

    RXEntity_p matkey=0,  *scList, pde=NULL;
    struct PANELDATA *pd=NULL;
    int l,found=0;
    l = this->m_psidecount;
    scList= new RXEntity_p[l+1]; // this trashes 'this' when we debug the release build
    for (l=0;l<this->m_psidecount;l++)
        scList[l]=this->m_pslist[l]->sc;

    for (ent_citer   it =  this->Esail->MapStart(); it !=  this->Esail->MapEnd(); ++it) {
        pde  = dynamic_cast<RXEntity_p>( it->second);
        if(pde->TYPE !=PC_PANELDATA) continue;

        pd=(PANELDATA *)pde->dataptr;
        if(!pd) continue;
        if(PCE_Cyclic_Permutation(pd->list,pd->c,scList,this->m_psidecount)){
            found=1;
            break;
        }
        PCE_Reverse_List(scList,this->m_psidecount);
        if(PCE_Cyclic_Permutation(pd->list,pd->c,scList,this->m_psidecount)){
            found=1;
            break;
        }
    } // for i
    if(found){
        this->m_PanelData = pd;
        matkey=pd->mat; // may be null
    }
    delete [] scList;
    return matkey;
}

RXEntity_p RXPanel::Get_PanelData_Ref_SC()  {
    /* if the panels paneldata has a ref SC - or several - , return it.*/
    if(!this->m_PanelData) return NULL;
    return ( this->m_PanelData->refSC);
}

//int Get_PanelData_Ref_Vector(  Panelptr  panel, VECTOR *v)  {
//	// if the panels paneldata has a ref vector, return it. 
//return 0;
//}
int PCE_Cyclic_Permutation(RXEntity_p *l1,int c1,RXEntity_p *l2,int c2){
    // returns 1 if l1 is a cyclic permutation of l2, else 0

    int i,j,s1,good;
    if(c1 != c2)  return 0;
    for(s1=0;s1<c1;s1++) { // start index
        good=1;
        for(i=s1,j=0;j<c1;j++,i=rxincrement(i,c1)){
            if(l2[j]!=l1[i]) {
                good=0; break;
            }
        }
        if(good) return 1;
    }

    return 0;
}
int PCE_Reverse_List(RXEntity_p *l, int c){
    int i,j;
    RXEntity_p b;
    i=0; j=c-1;
    while (i<j){
        b=l[i];
        l[i]=l[j];
        l[j]=b; i++; j--;
    }
    return 1;

}
