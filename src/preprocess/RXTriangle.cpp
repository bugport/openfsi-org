//RXTriangle.ccp : implementation of the class RXTriangle
//
//////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "RXGraphicCalls.h"

#include "RX_FETri3.h"
#include "RXCurve.h"
#include "RXPanel.h"
#include "RXSeamcurve.h"

#include "curveintersection.h"
#include "curveplaneintersection2.h"

#include "griddefs.h"
#include "filament.h"
#include "RXQuantity.h"

#include "RXTriangle.h"

#ifndef RHINO_DEBUG_PLUGIN
#include "printall.h"
#include "global_declarations.h"
#endif 

//RXOBJECT_IMPLEMENT( RXTriangle, RXTRObject, "084A7B93-9DFC-473D-B1CA-99E0911BBD36" );

RXTriangle::RXTriangle()
{
    Init();
}//RXTriangle::RXTriangle

//Copy constructor
RXTriangle::RXTriangle(const RXTriangle & p_Obj)
{
    Init();
    this->operator =(p_Obj);
}//RXTriangle::RXTriangle(const RXTriangle & p_Obj)

RXTriangle::RXTriangle( class RX_FETri3 &p_t)
{
    Init();
    const RXSitePt *v[4];
    p_t.Vertices (v);
    int i;
    for (i=0;i<3;i++)
    {
        SetV(i,v[i]->ToONPoint()   );
    }
}

RXTriangle::~RXTriangle()
{
    Clear();
}//RXTriangle::~RXTriangle

void RXTriangle::Init()
//meme alloc + init val to 0 0 0
{
    RXTRObject::Init();
    int i;
    for (i=0;i<3;i++)
    {
        m_v[i] = ON_3dPoint(ON_UNSET_VALUE,ON_UNSET_VALUE,ON_UNSET_VALUE);
    }
    ClearCC();
}//void RXTriangle::Init

int RXTriangle::Clear()
{
    for (int i=0;i<3;i++)
    {
        m_v[i] = ON_3dPoint(0,0,0);
    }
    ClearCC();
    RXTRObject::Clear_SSD();

    return RXTRIANGLEOK;

}//int RXTriangle::Clear

RXTriangle& RXTriangle::operator = (const RXTriangle & p_Obj)
{
    //Make a copy
    Init();

    RXTRObject::operator =(p_Obj);

    int i;
    for (i=0;i<3;i++)
    {
        m_v[i] = p_Obj.m_v[i];
        //		m_Edges[i] = p_Obj.m_Edges[i];
    }
    ClearCC();
    return *this;
}//void RXTriangle::operator =

char * RXTriangle::GetClassID() const
{
    return RXTRIANGLECLASSID;
}//char * GetClassID

int RXTriangle::IsKindOf(const char * p_ClassID) const
{
    if (streq(p_ClassID,RXTRIANGLECLASSID))
        return true;
    else
        return RXTRObject::IsKindOf(p_ClassID);
}//int RXTriangle::IsKindOf

RXTriangle * RXTriangle::Cast( RXTRObject* p_pObj) 
{
    return(RXTriangle *)Cast((const RXTriangle*)p_pObj);
}//RXTRObject * RXTriangle::Cast

const RXTriangle * RXTriangle::Cast( const RXTRObject * p_pObj) 
{
    if (!p_pObj)
        return NULL;
    if (p_pObj->IsKindOf(RXTRIANGLECLASSID))
        return (const RXTriangle *)p_pObj;
    else
        return NULL;
}//const RXTriangle * RXTRObject::Cast

ON_3dPoint RXTriangle::operator[](const int & p_ID) const
{
    assert((p_ID>-1)&&(p_ID<3));
    //RX3fPoint l_pt = m_v[p_ID];
    return m_v[p_ID];
}//ON_3dPoint RXTriangle::operator[]

//ON_3dPoint& RXTriangle::operator[](const int & p_ID)
//{
//	assert((p_ID>-1)&&(p_ID<3));
//	return m_v[p_ID];
//}//ON_3dPoint& RXTriangle::operator[]


int RXTriangle::SetV(const int & p_ID,const ON_3dPoint & p_pt)
{
    assert((p_ID>-1)&&(p_ID<3));

    m_v[p_ID] = p_pt; ClearCC();
    return RXTRIANGLEOK;
}//int RXTriangle::SetV

const ON_3dPoint RXTriangle::GetV(const int & p_ID) const
{
    assert((p_ID>-1)&&(p_ID<3));
    return m_v[p_ID];
}//ON_3dPoint RXTriangle::GetV

ON_3dPoint RXTriangle::PointAt(const double & p_t) const 
{
    ON_3fPoint l_pt(0.0,0.0,0.0);
    assert((p_t>=0)&&(p_t<=3));
    int i;
    ON_3fPoint l_v[3];
    for (i=0;i<3;i++)
        l_v[i] = ON_3fPoint(m_v[i][0],m_v[i][1],m_v[i][2]);

    ON_3fPoint l_T;
    if (p_t<1.0)
    {
        l_T= l_v[1]-l_v[0];
        l_pt = (p_t-0.0)*l_T;
        l_pt = l_v[0]+l_pt;
    }
    else
        if (p_t<2.0)
        {
            l_T= l_v[2]-l_v[1];
            l_pt = (p_t-1.0)*l_T;
            l_pt = l_v[1]+l_pt;
        }
        else
        {
            l_T= l_v[0]-l_v[2];
            l_pt = (p_t-2.0)*l_T;
            l_pt = l_v[2]+l_pt;
        }
    ON_3dPoint l_ptf(l_pt);
    return l_ptf;
}//ON_3dPoint RXTriangle::PointAt


ON_3dVector RXTriangle::TangentAt(const double & p_t) const 
//Evaluate unit tangent vector at a parameter.
{
    ON_3dPoint l_p(0.0,0.0,0.0);

    assert((p_t>=0)&&(p_t<=3));
    if (p_t<1.0)
        l_p = m_v[1]-m_v[0];
    else
        if (p_t<2.0)
            l_p = m_v[2]-m_v[1];
        else
            l_p = m_v[0]-m_v[2];
    ON_3dVector l_T  = l_p;
    l_T.Unitize();

    return l_T;
}//ON_3dPoint RXTriangle::PointAt


int RXTriangle::FindIntersections(const RXCurve * p_crv,
                                  ON_ClassArray<RXCrossing2> & p_Intersections,
                                  const double     & p_Tolerence,
                                  const double     & p_CutDistance,
                                  const double     & p_EpsDot)
{

    ON_LineCurve l_line;

    int i,j;
    int l_ret = 0;
    //int l_DoNOTMergeEnds = 0;
    //to be able to catch 2 intersections on 2 different edes which close to each other, we are using a temporary intersection
    //which is copy to p_Intersections after each edge and then we empty it before the next edge
    ON_ClassArray<RXCrossing2>  l_TempInter;
    assert(  l_TempInter.Count()==0); l_TempInter.Empty();
    //int l_nbtemp = 0;
    ON_3dVector	l_EltNormal;

    if(!GetUnitNormal(l_EltNormal))
    {
        char l_buff[256];
        sprintf(l_buff,"Pb in Normal of triangle: (%f,%f,%f)(%f,%f,%f)(%f,%f,%f)",m_v[0][0],m_v[0][1],m_v[0][2],m_v[1][0],m_v[1][1],m_v[1][2],m_v[2][0],m_v[2][1],m_v[2][2]);
        p_crv->Get_Ent()->OutputToClient(l_buff,1);
    }

    ON_Plane l_plane;
    ON_3dPoint	l_origin;
    ON_3dPoint l_point_on_x;
    ON_3dPoint l_point_on;
    ON_ClassArray<RXUPoint> l_ptsOnplane;

    ON_3dPoint l_V,l_V1;
    ON_Interval l_domain;
    int l_SomewhereNear = 1;
    const RXCurve * l_rxcrv = RXCurve::Cast(p_crv);
    ON_Curve * l_ONcrv = dynamic_cast<ON_Curve*>(l_rxcrv->GetONCurve());
    ON_NurbsCurve *l_ON_nc = ON_NurbsCurve::Cast(l_ONcrv  );
    // check whether the circumsphere of the triangle intersects the curve and create a seed value for the curve parameter
    // if it doesnt, don't go any further.
    double *l_P_Seed = NULL;
    double radius;
    ON_3dPoint cen;
    int l_DontTry= 0;
    if(l_ON_nc && CircumCircle(cen, &radius)) { // could do something similar for polylines
        double t0 , t1;
        double tperp;
        t0= l_ON_nc->Domain().Min();
        t1= l_ON_nc->Domain().Max();
        double l1 = cen.DistanceTo(l_ON_nc->PointAtStart());
        double l2 = cen.DistanceTo(l_ON_nc->PointAtEnd());
        tperp = t0 + (t1-t0) * l1/(l1+l2); // we used use GPA (V slow) to verify tperp is reasonable.

        // drop a perpendicular from cen to p_crv
        // We needn't go further if the result is off end or if the distance is > radius
        const ON_Interval l_int = ON_Interval(t0,t1);

        // this call is VERY SLOW.  can we find a cheaper way of seeing if we are close?
        // We could at least reject curves by seeing if cen is beyond 'radius'  of the convex hull.

        int l_rc = l_ON_nc->GetClosestPoint(cen,&tperp,radius, NULL);// &l_int);

        if (l_rc)
            l_P_Seed = &tperp;
        else {
            l_DontTry=1;
            l_SomewhereNear=0; // means we won't continue the loop
        }

        // if dist < radius) pass the T value found as l_P_Seed
        // else  dont call CPI
    } // if circumcircle

    for (i=0;i<3 && l_SomewhereNear ;i++)  //FOR EACH Edge
    {
        l_ptsOnplane.Empty();

        l_V.x = m_v[i][0];
        l_V.y = m_v[i][1];
        l_V.z = m_v[i][2];
        if (i<2)
        {
            l_V1.x = m_v[i+1][0];
            l_V1.y = m_v[i+1][1];
            l_V1.z = m_v[i+1][2];
        }
        else
        {
            l_V1.x = m_v[0][0];
            l_V1.y = m_v[0][1];
            l_V1.z = m_v[0][2];
        }
        l_domain = ON_Interval(i,i+1.0);

        //to get a local copy of the current edge temporary
        l_line.m_line.from = l_V;
        l_line.m_line.to = l_V1;
        l_line.m_t = l_domain;


        ON_3dPoint l_ThirdPt;
        if (i==0)
        {	l_ThirdPt.x = m_v[2][0];l_ThirdPt.y = m_v[2][1];l_ThirdPt.z = m_v[2][2];}
        if (i==1)
        {	l_ThirdPt.x = m_v[0][0];l_ThirdPt.y = m_v[0][1];l_ThirdPt.z = m_v[0][2];}
        if (i==2)
        {	l_ThirdPt.x = m_v[1][1];l_ThirdPt.y = m_v[1][1];l_ThirdPt.z = m_v[1][2];}

        //to define a plane normal to the element going thrue the edge

        l_origin = l_line.m_line.from;
        l_point_on_x = l_line.m_line.to;
        l_point_on = l_point_on_x+l_EltNormal;

        l_plane.CreateFromPoints(l_origin,l_point_on_x,l_point_on);

        //to find the intersectino between the plane and the curve .l_PersistentCurve is the rxObject holding l_line

        int l_TempRet=999;
        if(! l_DontTry)
            l_TempRet = CurvePlaneIntersections(p_crv,		//RXCurve
                                                &l_plane,	//ON_Plane
                                                l_line,		//ON_LineCurve
                                                l_ptsOnplane,
                                                p_EpsDot,
                                                p_CutDistance,
                                                l_P_Seed);

        int l_nbtemp = l_ptsOnplane.Count();

        for (j=0;j<l_nbtemp;j++) // July 2005 l_ptsOnplane member m_sense is filled in but not used after here.
        {
            ON_3dPoint l_Pt = l_ptsOnplane[j].GetPos();
            double l_t = 0.0;
            if (!l_line.m_line.ClosestPointTo( l_Pt, &l_t))//return a value in l_line.m_line domain
                p_crv->Get_Ent()->OutputToClient("ERROR: In RXTriangle::FindIntersections() l_line.m_line.ClosestPointTo() failled ", 2);

            //to change the doamin
            ON_Interval l_domain = l_line.Domain();


            l_t = l_domain.Min()+l_t*l_domain.Length();

            if (!l_domain.Includes(l_t))
                continue;

            RXUPoint l_PtOnEdge;

            if (!l_PtOnEdge.Set(l_t,this)) //	if (!l_PtOnEdge.Set(l_t,&l_line))
                p_crv->Get_Ent()->OutputToClient("ERROR: In RXTriangle::FindIntersections() l_PtOnEdge.Set failed",5);

            RXCrossing2 l_Inter;
            l_Inter.SetPoint1(l_ptsOnplane[j]);
            //REMOVE BY THOMAS 23 NOV 04			l_Inter.SetJ1(i);  // Peter Oct 2004
            l_Inter.SetPoint2(l_PtOnEdge);
            p_Intersections.AppendNew() = l_Inter;
            l_ret++;
        }//for (j=0;j<l_nbtemp;j++)

    }//for (i=0;i<3;i++)  //FOR EACH Edge

    int l_nbinter = p_Intersections.Count();
    assert(l_nbinter>=0);

    RXUPoint l_PtOnEdge;
    double l_t1,l_t2;

    // to REMOVE the intersection on the triangle edge are OFF the edge
    for (i=0;i<l_nbinter;i++)
    {
        l_PtOnEdge = *(p_Intersections[i].GetPoint2());
        ON_Interval l_EdgeDomain = l_PtOnEdge.Domain();
        l_t1 = l_PtOnEdge.Gett();
        //int l_haschanged = 0;
        if (l_t1>l_EdgeDomain.Max())
        {
            assert(0); //SHOULD NEVER happend because we detect this case before now!
            p_Intersections.Remove(i);
            i--;
            l_nbinter--;
            l_ret--;
        }
        if (l_t1<l_EdgeDomain.Min())
        {
            assert(0); ////SHOULD NEVER happend because we detect this case before now!
            p_Intersections.Remove(i);
            i--;
            l_nbinter--;
            l_ret--;
        }
        l_PtOnEdge.Clear();//housekeeping
    }

    int l_RemovePoint = 0;

    assert(l_nbinter>=0);

    //We are looking for the interstion which are ON a vertices
    //If 1 intersection is found on a vertices, we aree looking for the other one and we remove it
    for (i=0;i<l_nbinter;i++)
    {
        assert(i<p_Intersections.Count());
        l_PtOnEdge = *(p_Intersections[i].GetPoint2());
        //Test if the point is on a vertex
        l_t1 = l_PtOnEdge.Gett();
        if ((fabs(l_t1-0)<p_Tolerence)||(fabs(l_t1-1)<p_Tolerence)||(fabs(l_t1-2)<p_Tolerence)||(fabs(l_t1-3)<p_Tolerence))
        {
            //int l_DoublePointFound = 0;
            //if the point is on a vertex we look for the other intersection to find a point at the same t
            for (j=i+1;j<l_nbinter;j++)
            {
                assert(j<p_Intersections.Count());

                //Condition for a double point:
                //        - l_PtOnEdge.m_t==l_PtOnEdge2.m_t
                //      or      - l_PtOnEdge.m_t == 0 and l_PtOnEdge2.m_t = 3
                //      or      - l_PtOnEdge.m_t == 3 and l_PtOnEdge2.m_t = 0

                l_t2 = p_Intersections[j].GetPoint2()->Gett();

                if (fabs(l_t2-l_t1)<p_Tolerence)
                    l_RemovePoint = 1;
                if ((fabs(l_t1-0.0)<p_Tolerence)&&(fabs(l_t2-3.0)<p_Tolerence))
                    l_RemovePoint = 1;
                if ((fabs(l_t1-3.0)<p_Tolerence)&&(fabs(l_t2-0.0)<p_Tolerence))
                    l_RemovePoint = 1;

                if (l_RemovePoint)
                {
                    p_Intersections.Remove(j);
                    j--;
                    l_nbinter--;
                    l_ret--;
                }//if (l_RemovePoint)
                l_RemovePoint=0;
            }//for (j=i+1;j<l_nbinter;j++)

        }//if ((fabs(l_t1-0)<p_Tolerence)||(fabs(l_t1-1)<p_Tolerence)||(fabs(l_t1-2)<p_Tolerence)||(fabs(l_t1-3)<p_Tolerence))

    }//for (i=0;i<l_nbinter;i++)

    return l_ret;
}//int RXTriangle::FindIntersections


int RXTriangle::FindFilamentIntersection(const RXCurve * p_crv,   // processes them after finding them
                                         ON_ClassArray<RXCrossing2> & p_Intersections,
                                         const double     & p_Tolerence,
                                         const double     & p_CutDistance,
                                         const double	  & p_EpsDot)
{
    int l_ret = FindIntersections(p_crv,p_Intersections,p_Tolerence,p_CutDistance,p_EpsDot);
    int l_n = p_Intersections.Count();
    if (l_n==0)
    {
        return RX_FILELT_INTER_NONE;
    }//if (l_n==0)

    //	if (l_ret>2)
    //		int toto = 3456;

    //TO sort the intersection by increasing t along the curve
    SortCrossingOnCurve(p_Intersections,0) ;

    int i;
    //if their is more than 2 intersections and if they are colinear we only keep the more further appart
    if (l_n>2)
    {
        //search for the intersection on the vertices
        ON_SimpleArray<int> l_intOnvertex;
        ON_SimpleArray<int> l_vertexwithintersection;
        l_intOnvertex.Empty();
        for (i=0;i<l_n;i++)   //to catch all the intersection which ar on a vertex
        {
            int l_vID = -1;
            //double l_t =
            p_Intersections[i].GetPoint(1)->Gett();
            int l_Int1OnVertex = IsOnaVertex(*(p_Intersections[i].GetPoint(1)),l_vID,p_EpsDot);
            if (l_Int1OnVertex)
            {
                l_intOnvertex.AppendNew() = i;
                l_vertexwithintersection.AppendNew() = l_vID;
            }
        }

        //to catch all the intersection which are beetween 2 intersection on a 2 vertices
        ON_SimpleArray<int> l_toremove;
        int j;
        for (i=0;i<l_intOnvertex.Count()-1;i++)
        {
            double l_t1 = p_Intersections[l_intOnvertex[i]].GetPoint(1)->Gett();
            double l_t2 = p_Intersections[l_intOnvertex[i+1]].GetPoint(1)->Gett();

            if ((powf(l_t1-2.0,2)<p_EpsDot)&&(powf(l_t2-0.0,2)<p_EpsDot))    //to deal with the third edge of the triangle
                l_t2 = 3.0;
            if ((powf(l_t2-2.0,2)<p_EpsDot)&&(powf(l_t1-0.0,2)<p_EpsDot))    //to deal with the third edge of the triangle
            {
                l_t1 = l_t2;
                l_t2 = 3.0;
            }

            if ((powf(l_t1-3.0,2)<p_EpsDot)&&(powf(l_t2-1.0,2)<p_EpsDot))    //to deal with the third edge of the triangle
                l_t1 = 0.0;
            if ((powf(l_t2-3.0,2)<p_EpsDot)&&(powf(l_t1-1.0,2)<p_EpsDot))    //to deal with the third edge of the triangle
            {
                l_t2 = l_t1;
                l_t1 = 0.0;
            }

            for (j=0;j<l_n;j++)
            {
                double l_t = p_Intersections[j].GetPoint(1)->Gett();
                if ((l_t>l_t1)&&(l_t<l_t2))
                    l_toremove.AppendNew() = j;
            }
        }
        //Remove all the intersections beetwen the intersections which are on a vertex
        int l_nRemove = l_toremove.Count();
        for (i=0;i<l_nRemove;i++)
        {
            int l_IDrem = l_toremove[i]-i;
            p_Intersections.Remove(l_IDrem);
            l_n--;
            l_ret--;
        }//for (i=0;i<l_toremove.Count();i++)
        p_Intersections.Shrink();
    }//if (l_nbinter>2)


    //if the curve intersects the triangle on a vertex. and if the intersection is not at the end of the curve
    //this inetesction is ducplicated
    /*

 int l_PointOnVertex = 0;
 double l_t = 0.0;
 for (i=0;i<l_n;i++)
 {
  l_PointOnVertex=0;
  l_t = p_Intersections[i].GetPoint(1)->Gett();  //Point On the line (triangle edge)
  assert((l_t>=0)&&(l_t<=3));

  if (fabs(l_t-0.0)<ON_Min(1.0E-6,p_Tolerence))
   l_PointOnVertex = 1;
  if (fabs(l_t-1.0)<ON_Min(1.0E-6,p_Tolerence))
   l_PointOnVertex = 1;
  if (fabs(l_t-2.0)<ON_Min(1.0E-6,p_Tolerence))
   l_PointOnVertex = 1;
  if (fabs(l_t-3.0)<ON_Min(1.0E-6,p_Tolerence))
   l_PointOnVertex = 1;
  double l_tcurve = p_Intersections[i].GetPoint(0)->Gett();
  ON_Interval l_domain = p_Intersections[i].GetPoint(0)->Domain();
  //We do not want to duplicate if this intersection is at the end of the curve
  if (fabs(l_tcurve-l_domain.Min())<ON_Min(1.0E-6,p_Tolerence))
   l_PointOnVertex = 0;
  if (fabs(l_tcurve-l_domain.Max())<ON_Min(1.0E-6,p_Tolerence))
   l_PointOnVertex = 0;

  if (l_PointOnVertex)
  {//we duplicate the node
   RXCrossing2 l_Cross = p_Intersections[i];
//We do not duplicate the intersection anymore , the logic is a bit different in RXTriangle::Create_Filament_Matrix
//			p_Intersections.Insert(i+1,l_Cross);
//			i++;
//			l_n++;
  }
 }//for (i=0;i<l_n;i++)
 */

    //Setting the return FLAG
    /*
 RX_FILELT_INTER_MORETHAN_2  --> their is more than 2 intersection betweent he element and the filament
 RX_FILELT_INTER_NONE      --> no intersection beetween the filament and the element
 RX_FILELT_INTER_VERTEX    --> the element does not see the filament
 RX_FILELT_INTER_EDGE      --> the element does not see the filament
 RX_FILELT_INTER_VERTEX_EDGE   --> the element does see the filament
 RX_FILELT_INTER_EDGE_VERTEX   --> the element does see the filament
 RX_FILELT_INTER_VERTEX_VERTEX  --> the element does see the filament (the filement is on an edge)
 RX_FILELT_INTER_EDGE_EDGE      --> the element does see the filament
 RXTRIANGLEFALSE
 */

    if (l_n>2)
        return RX_FILELT_INTER_MORETHAN_2;
    else//if (l_n>2)
    {
        if (l_n==2)
        {
            int l_Int1OnVertex = 0;
            int l_Int2OnVertex = 0;
            double l_t1 = p_Intersections[0].GetPoint(1)->Gett();
            double l_t2 = p_Intersections[1].GetPoint(1)->Gett();
            if (l_t1==int(l_t1))
                l_Int1OnVertex = 1;
            if (l_t2==int(l_t2))
                l_Int2OnVertex = 1;

            //DEBUG TEST PETER 20 NOV 04
            assert(	RX_FILELT_INTER_VERTEX_VERTEX != RX_FILELT_INTER_EDGE_EDGE);
            //END DEBUG TEST PETER 20 NOV 04

            if ((l_Int1OnVertex)&&(l_Int2OnVertex))
                return RX_FILELT_INTER_VERTEX_VERTEX;
            if ((l_Int1OnVertex)&&(!l_Int2OnVertex))
                return RX_FILELT_INTER_VERTEX_EDGE;
            if ((!l_Int1OnVertex)&&(l_Int2OnVertex))
                return RX_FILELT_INTER_EDGE_VERTEX;
            if ((!l_Int1OnVertex)&&(!l_Int2OnVertex))
                return RX_FILELT_INTER_EDGE_EDGE;


        }//if (l_n==2)
        else //if (l_n==2)
        {
            if (l_n==1)
            {
                int l_vID = -1;
                int l_Int1OnVertex = IsOnaVertex(*(p_Intersections[0].GetPoint(1)),l_vID,p_EpsDot);
                if (l_Int1OnVertex==RXTRIANGLEOK)
                    return RX_FILELT_INTER_VERTEX;
                else
                    return RX_FILELT_INTER_EDGE;
            }//if (l_n==1)
            else  //if (l_n==1)
            {
                if (l_n==0)
                {
                    return RX_FILELT_INTER_NONE;
                }//if (l_n==0)
                else //if (l_n==0)
                {
                    if (l_n<0)
                    {
                        return RXTRIANGLEFALSE;
                    }//if (l_n<0)
                }//else //if (l_n==0)
            }//else  //if (l_n==1)
        }//else //if (l_n==2)
    }//else if (l_n>2)

    return RXTRIANGLEOK;
}//int RXTriangle::FindFilamentIntersection

#ifdef _GLOBAL_DECLARATIONS_16NOV04
int RXTriangle::Create_Filament_Matrix(sc_ptr p_sc,
                                       class FILAMENT_LOCAL_DATA * p_Fld,
                                       const double & p_Tolerence,
                                       const double & p_CutDistance,
                                       const double & p_EpsDot,
                                       const int & p_IsIn3d)
{

    /*       PCF_DISREGARD
//PCF_SMALL
//PCF_ONE
RX_FILELT_INTER_MORETHAN_2  --> their is more than 2 intersection betweent he element and the filament
RX_FILELT_INTER_NONE      --> no intersection beetween the filament and the element
RX_FILELT_INTER_VERTEX    --> the element does not see the filament
RX_FILELT_INTER_EDGE      --> the element does not see the filament
RX_FILELT_INTER_VERTEX_EDGE   --> the element does see the filament
RX_FILELT_INTER_EDGE_VERTEX   --> the element does see the filament
RX_FILELT_INTER_VERTEX_VERTEX  --> the element does see the filament (the filement is on an edge)
RX_FILELT_INTER_EDGE_EDGE      --> the element does see the filament

PCF_Inc(a)  */

    // list is sorted is by flag.  PC_OKs at the top
    //  then  by s1.  This divides the Xs into pairs in the event that a
    //  SC crosses the tri more than once.
    //  assume duplicates have been trimmed

    int i,j;
    assert(p_Fld);

    assert(p_IsIn3d != RX_MESH_XYPLANE);

    sc_ptr  l_pSc;
    l_pSc = p_sc;
    assert((sc_ptr  )l_pSc);

    int res = -1;
    ON_ClassArray<RXCrossing2> l_Inter;
    assert(l_Inter.Count()==0);

    RXCurve * l_crv = p_sc->m_pC[1];

    res = FindFilamentIntersection(l_crv,l_Inter,p_Tolerence,p_CutDistance,p_EpsDot);// at this point, general for all types of curve
    if (l_crv)
        l_crv=NULL;
    if(0 && g_mydebug) {
        printf(" return from FFI with %d ",res );
        switch(res) {
        case RX_FILELT_INTER_MORETHAN_2 :		cout<< "MORETHAN_2  "<<endl; break;
        case RX_FILELT_INTER_NONE:  			cout<< "NONE "<<endl; break;
        case RX_FILELT_INTER_VERTEX  :			cout<< "_VERTEX "<<endl; break;
        case RX_FILELT_INTER_EDGE   : 			cout<< " EDGE "<<endl; break;
        case RX_FILELT_INTER_VERTEX_EDGE : 		cout<< "_VERTEX_EDGE "<<endl; break;
        case RX_FILELT_INTER_EDGE_VERTEX  : 	cout<< " _EDGE_VERTEX "<<endl; break;
        case RX_FILELT_INTER_VERTEX_VERTEX  :	cout<< "VERTEX_VERTEX  "<<endl; break;
        case RX_FILELT_INTER_EDGE_EDGE	:		cout<< "  usual crossing type. no vertices"<<endl; break;
        default:								assert(0);
        }
        fflush(stdout);
    }
    switch (res) {
    case RX_FILELT_INTER_NONE :
        return RX_ELT_CANNOT_SEES_FILAMENT; //	//the elt has NO intersection with the filament, the elt does not see the filament

    case RX_FILELT_INTER_VERTEX :
    case RX_FILELT_INTER_EDGE:
        return RX_ELT_CANNOT_SEES_FILAMENT; //	l_type = PCF_C;   //the elt has only 1 intersection with the filament, the elt does not see the filament

    case RX_FILELT_INTER_MORETHAN_2 :
    {
        char buf[256];
        HC_Open_Segment("hilite");
        HC_Set_Color("magenta");
        HC_Set_Line_Weight(4.0);
        HC_Restart_Ink();
        for(i=0;i<3;i++)
            HC_Insert_Ink(m_v[i][0],m_v[i][1],m_v[i][2]);
        HC_Insert_Ink(m_v[0][0],m_v[0][1],m_v[0][2]);
        HC_Close_Segment();

        sprintf(buf," filament cross >2");
        p_sc->OutputToClient(buf, 1);
        if(g_mydebug)
        {
            int k;
            for(k=0;k<l_Inter.Count();k++) {
                RXCrossing2 l_Cross1 = l_Inter[k];
                ON_3dPoint a = l_Cross1.EvalPt(0);
                ON_3dPoint b = l_Cross1.EvalPt(1);
                printf(" %d (%f %f %f) (%f %f %f)\n",k, a.x,a.y,a.z,b.x,b.y,b.z);
                //	this shows (see triangle.3dm) that there is an accuracy problem
            }
        }
        return RX_ELT_CANNOT_SEES_FILAMENT;
    } // more than two
    default:
        break;
    } // end select

    //POST PROCESS THE INTERSECTIONS
    int l_n = l_Inter.Count();
    assert(l_n>1);

    int l_type = PCF_DISREGARD;

    if (l_n==0)
        l_type = PCF_DISREGARD;

    RXCrossing2 l_Cross1;
    RXCrossing2 l_Cross2;

    double l_w = 0.0;
    ON_3dVector l_vdash;
    double this_fil_to_matref_angle = 0.0;
    MatValType l_mx[9];
    RXExpressionI *q ;
    double l_ea, *l_pMp = NULL;
    q=l_pSc->FindExpression(L"ea",rxexpLocal);
    if(q)   l_ea =  q->evaluate() ;
    else l_ea=0.0;

    double   ps, l_ti[3];
    q = l_pSc->FindExpression(L"ti",rxexpLocal);
    if(q) ps= l_ti[0] =  q->evaluate() ;//PC_Value_Of_Q(l_pSc->ti);
    else ps= l_ti[0] = 0.0;
    l_ti[1]=l_ti[2]=0.0;

    if ((res==RX_FILELT_INTER_VERTEX_EDGE)||(res==RX_FILELT_INTER_EDGE_VERTEX)||(res==RX_FILELT_INTER_EDGE_EDGE)||(res==RX_FILELT_INTER_VERTEX_VERTEX))
    {
        l_Cross1 = l_Inter[0];  //the current intersection, m_t is a parameter OR an arclength.
        l_Cross2 = l_Inter[1]; //the next intersection,



        ON_3dVector l_X = p_Fld->Create_Material_Axes(l_Cross1,l_Cross2); // no side-effects

        ON_3dPoint l_pt0_cross1 = l_Cross1.EvalPt(0);
        //ON_3dPoint l_pt1_cross1 = l_Cross1.EvalPt(1);
        ON_3dPoint l_pt0_cross2 = l_Cross2.EvalPt(0);
        //ON_3dPoint l_pt1_cross2 = l_Cross2.EvalPt(1);


        //to get the length of the filament which is in the element
        double l_length = l_pt0_cross1.DistanceTo(l_pt0_cross2); //l_Cross1.EvalPt(0).DistanceTo(l_Cross2.EvalPt(0));
        //To get the area of the element
        double l_area = p_Fld->m_FE_Tri->m_TriArea;


        //debug
        if (l_Cross1.GetPoint1()->Gett()!=l_Inter[0].GetPoint1()->Gett())
            l_Cross1 = l_Inter[0];
        if (l_Cross1.GetPoint2()->Gett()!=l_Inter[0].GetPoint2()->Gett())
            l_Cross1 = l_Inter[0];

        if (l_Cross2.GetPoint1()->Gett()!=l_Inter[1].GetPoint1()->Gett())
            l_Cross2 = l_Inter[1];
        if (l_Cross2.GetPoint1()->Gett()!=l_Inter[1].GetPoint1()->Gett())
            l_Cross2 = l_Inter[1];
        //end debug


        //l_w is  "area of triangle"/"Length of the filament" (Peter 22 Nov 04)

        l_w = l_area / l_length;

        l_vdash = p_Fld->m_MatRefTrigraph * l_X;
        this_fil_to_matref_angle = -(atan2(l_vdash.y,l_vdash.x));   /* angle from this filament to reference filament */

        memset(l_mx,0, 9*sizeof(MatValType));

        if (res==RX_FILELT_INTER_VERTEX_VERTEX)  //--> the element does see the filament (the filement is on an edge)
        {
            l_ea=l_ea/2.0;  //if the filament is along the edge, half the ea and the ti goes to this element and the other half goes to the adjacent element
            l_ti[0] = l_ti[0]/2.0;
        }

        l_mx[0] = l_ea/l_w;								//compute the filament matrix in the axis system of the element
        if(g_mydebug)
            printf(" EA/W = %f\n", l_mx[0]);

        RXPanel::rotate_material(l_mx,this_fil_to_matref_angle); //compute the filament matrix in the axis system of the element

        assert(p_Fld->DD_Tot);
        l_pMp = p_Fld->DD_Tot;
        for(j=0;j<9;j++,l_pMp++){
            (*l_pMp) += l_mx[j];  //to add to p_fld->DD_Tot  the contribution of the filament (using l_pMp and temporary pointer to p_fld->DD_Tot)
            if( g_mydebug) printf(" %f\t", (*l_pMp) );
        }
        if( g_mydebug) cout<< "\n"<<endl;
        l_ti[0] = l_ti[0]/l_w;
        RXPanel::rotate_stress(l_ti,this_fil_to_matref_angle);
        for(j=0;j<3;j++)
            p_Fld->ps[j] += l_ti[j];

        //Onto the tri we push a string intersection structure.
        //The two coordinates, the angle to the material axis and a pointer to the string SC.


        //WE actually do not need J1 and J2 becauce the intersections are set on 3 differnt line of domain [0,1], [1,2], [2,3]
        //therefore the RXUPoint objects knows on which element it is
        //Thomas 28 10 04

        //to get the indexes of the edge of the triangle where the intersection is
        double l_t_onTR1 = l_Cross1.Gett(1);
        double l_t_onTR2 = l_Cross2.Gett(1);
        assert((l_t_onTR1>=0.0)&&(l_t_onTR1<=3.0));
        assert((l_t_onTR2>=0.0)&&(l_t_onTR2<=3.0));

        int l_j1 = int(floor(l_t_onTR1));
        int l_j2 = int(floor(l_t_onTR2));

        double	l_t1 = l_Cross1.Gett(1) - (double) l_j1;
        double	l_t2 = l_Cross2.Gett(1) - (double) l_j2;

        assert(l_j1>=0);	assert(l_j2>=0);

        PCF_PushToTriFilamentList(p_Fld->m_FE_Tri,
                                  l_j1,l_t1,
                                  l_j2,l_t2,
                                  l_pSc,-this_fil_to_matref_angle ,l_w,l_ea,ps); // the angle here should be the angle from TRI REf Vector not the Filed Layer Ref Vector

        //Clean up
        l_Cross1.Clear();
        l_Cross2.Clear();
        l_Inter.Empty();
        l_Inter.Shrink();
        return RX_ELT_SEES_FILAMENT; //	l_type = PCF_C;   //the elt has only 1 intersection with the filament, the elt does not see the filament
    }
    return RX_ELT_CANNOT_SEES_FILAMENT;

}//int RXTriangle::Create_Filament_Matrix

#endif //#ifdef _GLOBAL_DECLARATIONS_16NOV04		

int RXTriangle::IsOnaVertex(const RXUPoint &p_pt, 
                            int &p_VIDn,
                            const double &p_EpsDot) const
{
    double l_t1 = p_pt.Gett();
    double l_floor= floor(l_t1);
    double l_ceil = ceil(l_t1);
    double l_round;
    //to get the integer
    if (fabs(l_t1-l_floor)<fabs(l_t1-l_ceil))
        l_round = l_floor;
    else
        l_round = l_ceil;

    assert((l_round>-1)&&(l_round<4));

    if (powf(l_t1-l_round,2)<p_EpsDot)
    {
        if (l_round<3)
            p_VIDn = int(l_round);
        else
            p_VIDn = 0;
    }
    else
        return RXTRIANGLEFALSE;

    return RXTRIANGLEOK;
}//int RXTriangle::IsOnaVertex

int RXTriangle::SetAllVertices(class FILAMENT_LOCAL_DATA *p_fld)
{
    //To set the triangle from the field data
    int i;
    for (i=0;i<3;i++)
    {
        assert(p_fld->v[i]);
        ON_3dPoint l_tempV;

        l_tempV.x = p_fld->v[i]->x;
        l_tempV.y = p_fld->v[i]->y;
        l_tempV.z = p_fld->v[i]->z;

        SetV(i,l_tempV);
    }
    return 1;
}

int RXTriangle::SomewhereNear(const RXCurve &p_c, const double p_tol)
{

    cout<<" RXTriangle::SomewhereNear is always true"<<endl;
    return 1;
    //	const ON_3dPoint &l_v = m_v[0];
    //	int i = p_c.Somewhere_Near(l_v,0.1);

    //	if(p_c.Somewhere_Near( l_p,0.1)
    //		||p_c.Somewhere_Near(&m_v[1],p_tol)
    //		||p_c.Somewhere_Near(&m_v[2],p_tol) )
    //				return 1;
    //	else
    //		return 0;
}

int RXTriangle::GetUnitNormal(ON_3dVector &p_Eltnormal) const
{

    ON_3dVector l_edg1; //(m_v[1]-m_v[0]);
    ON_3dVector l_edg2; //(m_v[2]-m_v[0]);
    l_edg1.x = (m_v[1][0])-(m_v[0][0]);
    l_edg1.y = (m_v[1][1])-(m_v[0][1]);
    l_edg1.z = (m_v[1][2])-(m_v[0][2]);
    l_edg2.x = (m_v[2][0])-(m_v[0][0]);
    l_edg2.y = (m_v[2][1])-(m_v[0][1]);
    l_edg2.z = (m_v[2][2])-(m_v[0][2]);

    p_Eltnormal = ON_CrossProduct(l_edg1,l_edg2);
    if(!p_Eltnormal.Unitize ()) return false;
    return true;
} 
int RXTriangle::ClearCC(){
    m_radius=-1;
    return 1;
}
int RXTriangle::CircumCircle(ON_3dPoint &p_centre, double *p_r) 
{
    // get r1,r2 ,r3 thevertices

    // get the edgelength a (r2->r3)
    // get the Y axis normalised

    if(m_radius>=0) {
        *p_r = m_radius;
        p_centre = this->m_centre;
        return 1;
    }
    double a,b,c,A,C;
    ON_3dVector Ynorm ;
    const ON_3dPoint *r1 = &m_v[0];
    const ON_3dPoint *r2 = &m_v[1];
    const ON_3dPoint *r3= &m_v[2];

    a = r2->DistanceTo (*r3);
    b = r3->DistanceTo (*r1);
    c = r1->DistanceTo (*r2);
    A =  (-a*a+ b*b + c*c)/(2*b*c);
    if(A>=1.0) A=1.0;
    if(A <= -1.0) A=-1.0;
    A = acos(A);
    C = c/a *sin(A);
    C = asin(C);
    m_radius = a/(2.0 *sin (A));
    Ynorm = Yaxis();
    p_centre = (*r1 + *r2)/2.0 + Ynorm *m_radius *sin(ON_PI/2.0 - C);
    m_centre = p_centre;
    *p_r = m_radius;
    return 1;
}

ON_3dVector RXTriangle::Yaxis() const
{
    ON_3dVector e1,e2,n,y;

    e1 = m_v[1] - m_v[0];
    e2 = m_v[2]- m_v[0];
    n = ON_CrossProduct(e1,e2);
    y = ON_CrossProduct(n,e1);
    y.Unitize();
    return y;
}

int RXTriangle::GetTightBoundingBox(rxON_BoundingBox &bbox, bool pbGrowBox, const ON_Plane *p_onbb) const
{
    int i;
    ON_SimpleArray<ON_3dPoint> pt;
    ON_3dPoint pp;
    bbox.m_min = bbox.m_max = m_v[0];
    for(i=1;i<3;i++) {
        pp = m_v[i];
        bbox.m_min.x = min(pp.x,bbox.m_min.x);
        bbox.m_min.y = min(pp.y,bbox.m_min.y);
        bbox.m_min.z = min(pp.z,bbox.m_min.z);

        bbox.m_max.x = max(pp.x,bbox.m_max.x);
        bbox.m_max.y = max(pp.y,bbox.m_max.y);
        bbox.m_max.z = max(pp.z,bbox.m_max.z);


    }
    return 1;

    for(i=0;i<3;i++)
        pt.AppendNew() = m_v[i];
#ifndef ON_V3
    i=bbox.Set(pt); //?? return cvalue
    assert(i); // because we havent stepped it
#else
    i=ON_GetTightBoundingBox(  pt, bbox, bGrowBox,onb) ; // ON V3 only
#endif
    pt.Destroy();
    return i;
}

ON_Xform RXTriangle::Trigraph()
{
    ON_3dVector l_Eltnormal,l_Y;
    //	assert(0);
    ON_3dVector l_edg1; //(m_v[1]-m_v[0]);
    ON_3dVector l_edg2; //(m_v[2]-m_v[0]);
    l_edg1.x = (m_v[1][0])-(m_v[0][0]);
    l_edg1.y = (m_v[1][1])-(m_v[0][1]);
    l_edg1.z = (m_v[1][2])-(m_v[0][2]);
    l_edg2.x = (m_v[2][0])-(m_v[0][0]);
    l_edg2.y = (m_v[2][1])-(m_v[0][1]);
    l_edg2.z = (m_v[2][2])-(m_v[0][2]);

    l_Eltnormal = ON_CrossProduct(l_edg1,l_edg2);
    l_Eltnormal.Unitize ();
    l_edg1.Unitize();
    l_Y = ON_CrossProduct(	l_Eltnormal,l_edg1);
    ON_Xform l_xf = ON_Xform(m_v[0],l_edg1,l_Y,l_Eltnormal);
    l_xf.Transpose();
    return l_xf;

}

