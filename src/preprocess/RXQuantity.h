
#ifndef _HDR_RXQUANTITY_
#define _HDR_RXQUANTITY_

#include "RXExpression.h"

#define RXDECIMALPT '.' 

class RXQuantity:
        public RXExpression
{
public:
    RXQuantity();
    virtual ~RXQuantity();
    RXQuantity(const RXSTRING &name,const RXSTRING &p_buf,const RXSTRING &units, class RXSail *const p_sail);
    virtual MTDOUBLE evaluate();
    void  SetNeedsComputing(const int i);
    virtual const char* What()const {return "Q";}
    RXSTRING GetLine(void) const{ return GetExpression()+m_unitstring;}
    static double OneTimeEvaluate(const RXSTRING &p_buf, const wchar_t*type,class RXENode *const context,int *err);
    virtual QString Descriptor()const;
private:
    int m_m,m_l, m_t; // powers of mass, length and time
    double m_scalefaCTor;
    RXSTRING m_unitstring;
    void SetInternalUnits(const RXSTRING &units);
    RXSTRING UnitString(RXSTRING &s);
    double SetScaleFactor();
};

// statics; should use RXQuantity::OneTimeEvaluate
double Evaluate_Quantity(SAIL *const p_Sail, const char* offin,const char *type);
double Evaluate_Quantity(SAIL *const p_Sail, const RXSTRING &offin, const wchar_t*type);

double Evaluate_Length_String(SAIL *const p_sail,char *s_in);

// The two 'With Pointer' subclasses.
// RXQuantityWithPtr maintains a double's value equal to the evaluation if its expression.  which is its slave:
// the double  is a slave
// RXQuantityReadOnlyDble  associates a name to a dble  so the dble can be referenced by other expressions

class RXQuantityWithPtr:
        public  RXQuantity
{
public:
    RXQuantityWithPtr();
    virtual ~RXQuantityWithPtr();
    RXQuantityWithPtr(const RXSTRING &name,const RXSTRING &p_buf,const RXSTRING &units, class RXSail *const p_sail, double*dptr);
    virtual MTDOUBLE evaluate();
    virtual int Compute() ;
    virtual const char* What() const{return "QP";}
    virtual QString Descriptor()const;
private:
    double*m_dblPtr;
};
//
class  RXQuantityReadOnlyDble:
        public  RXQuantity
{
public:
    RXQuantityReadOnlyDble();
    virtual ~RXQuantityReadOnlyDble();
    RXQuantityReadOnlyDble(const RXSTRING &name,const RXSTRING &p_buf,const RXSTRING &units, class RXSail *const p_sail, double*dptr);
    virtual MTDOUBLE evaluate();
    virtual int Compute() ;
    virtual const char* What()const {return "RO";}
    virtual QString Descriptor()const;
    virtual RXSTRING GetText() const;
private:
    double*m_dblPtr;
    double m_oldValue;
};


#endif
