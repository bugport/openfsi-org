#include "StdAfx.h"
#include "RXEntityDefault.h"
#include "RXPolyline.h"
#include "rxspline.h"
#include "uvcurve.h"
#include "nurbs.h"
#include "etypes.h"

int Compute_UV_Curve( RXEntity_p e, RXEntity_p mould, double tol) {
/* e is a basecurve its (pin,cin) are in UV space
We should build up a  polyline adaptively. (sagitta < tol) but WRONG we just sample 101 pts */

class RXBCurve* p;

 double f,u=0,v=0;
 int i;
printf(" In Compute_UV_Curve on e='%s'\n ",e->name());
if(!e || !mould) { cout<< " no mould "<<endl; return 0;}

p = (class RXBCurve*) e;

 if(p->BCtype !=BCUVCURVE) { printf(" return with bad type %d ", p->BCtype); return 0;}
 if(mould->Needs_Resolving) { 
	 cout<< "mould->Needs_Resolving\n"<<endl; 
	 return 0;}
  if(mould->Needs_Finishing) { 
	  cout<< "mould->Needs_Finishing\n"<<endl;
	  return 0;}
 if(mould->NeedsComputing()) { 
	 cout<< "mould->Needs_ Computing\n"<<endl; 
	 return 0;}

    p->p = (VECTOR *)CALLOC(102, sizeof(VECTOR));
   for(i=0,f=0.0 ;i<101;i++) {
	   assert("TODO :: Compute_UV_Curve"==0);
	//u = p->pin[0].x * (1.-f) + p->pin[1].x * f;
	//v = p->pin[0].y * (1.-f) + p->pin[1].y * f;
	//Evaluate_NURBS(mould,u,v,&(p->p[i] )); 
	//printf("%d %f %f (%f %f %f)\n", i,u,v,p->p[i].x,p->p[i].y,p->p[i].z);
	f =f + 0.01;
  }
	 p->c=101;
  return 1;
}
