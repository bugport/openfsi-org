/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 * 22/12/95 printf
 *  16/10/95 debugging. doesnt always get the Z axis
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"

#include "RXSail.h"
#include "RXAttributes.h"
#include "global_declarations.h"
#include "words.h"

#include "ReadName.h"

int Parse_Name_Attributes(char *text, SAIL *s){
  // Peter Add Oct 2003 . parse text 
// text might be "$pressure_Factor=<double>  etc" 
	int i;
	s->m_Pressure_Factor=1.0;
	s->m_Linear_Tol = g_SOLVE_TOLERANCE ;
	s->m_Cut_Dist= g_Gauss_Cut_Dist;
	s->m_Angle_Tol= g_ANGLE_TOL ;
	i = g_Use_ONCurve;
	s->SetFlag(FL_ONFLAG,i ); 

	if(!text) return 0;
	RXAttributes rxa (text);
 
 
	if(!rxa.Extract_Double ("$pressure_factor" ,&(s->m_Pressure_Factor) ) ) 
		s->m_Pressure_Factor=1.0 ;

	if(!rxa.Extract_Double ("$linear_tol" ,&(s->m_Linear_Tol) ) ) 
		s->m_Linear_Tol = g_SOLVE_TOLERANCE ; 
	
	if(!rxa.Extract_Double ("$cut_tol" ,&(s->m_Cut_Dist) ) ) 
		s->m_Cut_Dist= g_Gauss_Cut_Dist;

	if(!rxa.Extract_Double ( "$angle_tol" ,&(s->m_Angle_Tol) ) ) 
		s->m_Angle_Tol= g_ANGLE_TOL ;

	if(!rxa.Extract_Integer( "$onflag" ,&(i) ) ) 
		i = g_Use_ONCurve;
		
    s->SetFlag(FL_ONFLAG,i );
    if(rxa.Find("$nobeampost"))
        s->FlagSet(NO_BEAM_POST );
    if(rxa.Find("$nouvmap"))
        s->FlagSet(NO_UV_MAP );

 return 1;
	}


