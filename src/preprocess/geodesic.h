#ifndef _HDR_GEODESIC
#define _HDR_GEODESIC 1	

#include "entities.h"
#include "vectors.h"	

EXTERN_C int Find_Geodesic(sc_ptr sc, VECTOR**p,int*c, float*chord);

EXTERN_C int Create_Displaced_Polyline(sc_ptr sc,RXEntity_p mould,int which,int c,double*x,double*y,double*z,VECTOR *base);
EXTERN_C int  InitGlist(sc_ptr sc,Site *e1, Site*e2,RXEntity_p mould);
EXTERN_C int  CopyGlistToPline(sc_ptr sc,VECTOR**p,int*c);
EXTERN_C int  GeodifyList (sc_ptr sc,RXEntity_p mould);
EXTERN_C int  Insert_Gknot_After(struct GPoint **g,RXEntity_p mould);
EXTERN_C int  Remove_Gknot(struct GPoint **g,RXEntity_p mould);
EXTERN_C int  GeodifyKnot (struct GPoint *g,RXEntity_p mould);

EXTERN_C  int Get_Normalised_Geodesic_S(sc_ptr sc);
EXTERN_C  int Calculate_Y(sc_ptr sc,RXEntity_p mould)
;
EXTERN_C int Calc_GKnotAngles(struct GPoint *g,RXEntity_p mould);
	/* all info derived from its relation with its neighbours */

EXTERN_C int Calc_Gknot_Position(struct GPoint *g,RXEntity_p mould);
	/* all info derived from (u,v) alone */
EXTERN_C int UV_from_Sxy( sc_ptr sc, double sxy, double*u, double*v);
EXTERN_C int  Print_Gknot(struct GPoint *g, FILE *fp);
EXTERN_C int  Print_GList(struct GPoint *g, FILE *fp);

EXTERN_C int printmatrix(const char*s, double L[3][3]) ;
EXTERN_C int VerifyScale(struct GPoint *g, double zfac);
EXTERN_C int CalcGeodesicScale(struct GPoint *g);
 EXTERN_C double Geodesic_Arc(struct GPoint *head);
EXTERN_C double Suv_from_Sxy( sc_ptr sc, double sxy, int*flag); 
EXTERN_C double Sxy_from_Suv( sc_ptr sc, double suv, int*flag); 
EXTERN_C int UV_from_Suv( sc_ptr sc, double suv, double*u, double*v) ;
EXTERN_C int Place_On_Geo(sc_ptr sc,double Suv, double*v1,double*v2,double*v3,double*U1,double*U2); 
EXTERN_C int Delete_Geodesic_List(struct GPoint *head);
EXTERN_C int Geo_Test_Remove( struct GPoint *g); 
EXTERN_C int Geo_Test_Insert(struct GPoint *g );
EXTERN_C int GlistFirstAndLast(sc_ptr sc,Site *e1, Site *e2,RXEntity_p mould);
#endif

