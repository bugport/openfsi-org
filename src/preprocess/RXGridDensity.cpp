#include "StdAfx.h"
#include "RXRelationDefs.h"

#include "etypes.h"
#include "ReadGenDp.h"
#ifndef linux
#include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8

#else
#include "../../stringtools/stringtools.h"
#endif
#include "RXSail.h"
#include "RXQuantity.h"

#include "RXGridDensity.h"
/*
Maintains an expression for  target element edge length as a function of
some of {u,v,x y z} or a constant. 
It should place the expression into the model's expressionlist. 
The expression can be accessed by 
 GetModel()->FindExpression("grid");
As a speed optimisation we place a pointer to the expression in the 
ShewchukTriangle class.

For more generality we might make a 'distributedProperty' class which
has methods evaluate(x,y,z) and evaluate(u,v) 
It has sub-classes for scalar, vector(and/or Point) or tensor. 
Which might be further sub-classed into Algebraic, Nurbs, tri-spline, etc. 

Or the end-user classes might be derived from (eg) distributedProperty'->Tensor
and Algebraic.
Note that this is getting quite close to VTK's Field object. 

*/


RXGridDensity::RXGridDensity(void)
{
    assert("dont use default RXEntity constructors"==0);
}
RXGridDensity::RXGridDensity(SAIL *s):
    RXEntity (s)
{
}

RXGridDensity::~RXGridDensity(void)
{ 
    this->CClear();
}

int RXGridDensity::Compute(void)
{
    int rc=0;
    return rc;
}
int RXGridDensity::Resolve(void) // if successful it places an expression named "grid" on the sail.
{
    /* Gendp card is of type
 Grid Density 	 GridDenName 	 0.8 ! comment */
    vector<RXSTRING> words= rxparsestring( FromUtf8(this->GetLine()),RXSTRING( RXENTITYSEPS_W));
    RXObject*eo;
    const wchar_t *lp =words[2].c_str();
    //if the old one exists, change it.
    // something like
    RXExpressionI *olde;
    if(olde= this->Esail->FindExpression(L"grid",rxexpLocal ))		 //looks in this->explist and recursively in this->parent->explist.
    {
        delete olde;
    }
    RXQuantity *q = new RXQuantity(L"grid",lp,L"m",this->Esail);
    RXExpressionI *ee =this->Esail->AddExpression (q ); 	//	this->SetRelationOf(q,aunt,RXO_EXP_OF_ENT); //done
    if(!ee) return 0;

    if(!q->Resolve ()) {
        delete q;
        return 0;
    }

    eo = dynamic_cast<RXObject*>(q);
    if(eo)
        this->SetRelationOf(eo,aunt|spawn,RXO_EXP_OF_GRID);
    double v = q->evaluate ();

    this->Needs_Resolving=false;
    this->Esail ->SetMeshExpression(q );
    return 1;
} 
int RXGridDensity::ReWriteLine(void)
{
    int rc= 0;
    RXExpressionI *e;
    MTSTRING  w;
    QString  NewLine ( this->type());
    NewLine += RXENTITYSEPSTOWRITE;
    NewLine += this->name();
    NewLine += RXENTITYSEPSTOWRITE;
    e=this->Esail->FindExpression(L"grid",rxexpLocalAndTree);
    if(e) {
        w= e-> GetText();
        NewLine += QString::fromStdWString(w);
        NewLine +=this->AttributeString();

        this->SetLine(qPrintable(NewLine));
    }


    return rc;
}
int RXGridDensity::Finish(void) 
{
    int rc=0;
    return rc;
}
int  RXGridDensity::CClear(){
    int rc=0;
    // clear stuff belonging to this object

    // call base class CClear();
    rc+=RXEntity::CClear ();
    return rc;
}

int RXGridDensity::Dump(FILE *fp) const 
{
    int rc=0;
    RXExpressionI *e;
    std::wstring  w;
    std::string s, NewLine =  this->type();
    NewLine += RXENTITYSEPSTOWRITE;
    NewLine += this->name();
    NewLine += RXENTITYSEPSTOWRITE;
    e = this->Esail->FindExpression(L"grid",rxexpLocalAndTree);
    if(e) {
        w= e->GetText();
        s = ToUtf8(w);
        NewLine += s;
    }
    else NewLine+= " (no expression found)";
    NewLine += "  ";
    NewLine +=this->AttributesPrettyPrint().toStdString();
    fprintf(fp,"%s\n", NewLine.c_str ());
    return rc;
}
RXSTRING RXGridDensity::TellMeAbout() const 
{
    RXSTRING rc(L"Grid Density");
    return rc;
}
