/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 June 2002 tries NOT forcing the delete of the boats DAT file
 March 1997 compare file dates ifdefffed out for HP
 * also, dont read nodes. The numbering may be wrong
 *   old DAT file always deleted. If you change a kite for a genoa, for instance
   it places the wrong displacements on the boat nodes.

 should do a Load State instead
 * no commas in default atts.
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* read bar data card from a panel.in style file */

#include "StdAfx.h" 
#include "RelaxWorld.h"
#include <QFileInfo>
#include <QDir>
#undef strieq
#include "RXSail.h"
#include "rxsubwindowhelper.h"
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "RXGraphicCalls.h"
#include "delete.h"
#include "stringutils.h"

#include "entities.h"
#ifdef _X
#include "menuCB.h"
#endif
#include "trimmenu_s.h"
#include "summary.h"
#include "redraw.h"
#include "resolve.h"

#include "drop.h"

#include "script.h"
#include "f90_to_c.h"
#include "connect_f.h"
#include "global_declarations.h"

#include "ReadBoat.h" 

#define DEBUG0
#define SUCCESS 1
#define READBOAT_FAILED 0

int findString(FILE *fp,const char *str)
{
    char line[256];
    long len;
// see  http://msdn.microsoft.com/en-us/library/75yw9bf3%28v=vs.71%29.aspx
	// fseek(SEEK_CUR) is not guaranteed to work on MSW for text streams.
    while(fgets(line,255,fp)) {
        if(stristr(line,str)) {
            len = strlen(line);
			if(0 !=fseek(fp,-len,SEEK_CUR)){ // reset to beginning of line
				cout<<"reset to beginning of file "<<endl;
				if(0 !=fseek(fp,0,SEEK_SET)){ // reset to beginning of file
					cout<<".....but cant even do that "<<endl;
					return 0;
				}
			}
            return(1);
        }
    }
    return(0);
}//int findString

int check_is_boat(const QString qfilename) {
    FILE *fp;
    int retVal = 0;
    char line[256],*lp;

    fp = RXFOPEN(qPrintable(qfilename),"r");
    if(fp && findString(fp,"name")) {
        if(fgets(line,255,fp)) {
            line[254] = '\0';
            lp = strchr(line,'!');
            if(lp)
                *lp = '\0';
            if(strstr(line,"boat"))
                retVal = 1;
        }
        FCLOSE(fp);
    }

    return(retVal);
}

// pfilename is *.in. May be an absolute path or relative to g_currentDir
// remember Graphic is typedef'd class RXSubWindowHelper   ;
int ReadBoat(const  QString pfilename,Graphic *gIn)
{  
    QString qfilename;
    int ret, 	Hoisted ,sli ,err;	char buf[256];

    QFileInfo baseInfo (g_currentDir );
    assert(baseInfo.isAbsolute () );
    QDir base = QDir(g_currentDir);

    qfilename= base.absoluteFilePath ( pfilename ) ;
    qfilename=  QDir::cleanPath ( qfilename  ) ;


    if(!check_is_boat(qfilename)) {
        cout<<"File does not contain a name = boat card"<<endl;
        return(READBOAT_FAILED);
    }
    RXSubWindowHelper::DropAll();
    RXGRAPHICSEGMENT w_old =0;
    if(g_boat){
        w_old = g_boat->GNode(); // or something
        if (g_boat->IsHoisted())
            g_boat->Drop();
        delete g_boat;
    }
    g_boat = new RXSail( g_World );

    g_boat->SetFileIN(qfilename);    // absolute path
     if(gIn) g_boat->SetGraphic(gIn);
    g_boat->SetFlag(FL_ISBOAT,1);
    if(!w_old){
#ifdef RXQT
    #ifdef USE_GLC
        class GLC_StructOccurence*w = RXNewWindow("boat", g_boat);
        g_boat->setGNode(w);
#elif defined(COIN3D)
        RXGRAPHICSEGMENT w = RXNewWindow("boatwindow", g_boat);
        g_boat->setGNode(w);
        g_boat->m_viewFrame =(class RXViewFrame*) w->getUserData() ; // Jan 2014 is it threadsafe?
    #endif
#else

#endif
    }
    else
    g_boat->setGNode(w_old);

#ifdef HOOPS
    HC_Open_Segment_By_Key(g->m_ModelSeg);
    HC_Flush_Contents(".","subsegment,geometry,style");
    HC_Open_Segment("data");
    HC_Set_User_Options("adjectives=flat$xscaled$yscaled$draw  ");
#endif
    ret = g_boat->Pre_Process(qPrintable(qfilename));
#ifdef HOOPS
    HC_Set_Visibility("text=off");
    HC_Close_Segment();
    HC_Close_Segment();
#endif

    if(ret <= 0){
        g_boat->OutputToClient("boat has failed to build !",2);
        return(READBOAT_FAILED);
    }
#ifdef _X
    SetCameraList();
#endif
    if(!g_boat->IsOK()) {
        g_boat->OutputToClient("Read boat has failed !",2);
        return(READBOAT_FAILED);
    }
    g_boat->m_fileBAG = g_boat->ScriptFile();
    g_boat->SetFlag(FL_NEEDS_POSTP );

    g_boat->Mesh();
#ifdef _X_
    l_ac=0;
    sprintf(buf,"%s Boat: %s ",RELAX_VERSION,sd->fileBAG);
    XtSetArg(al[l_ac],XmNtitle,buf); l_ac++;
    XtSetValues(g->topLevel,al,l_ac);
#endif
//    sprintf(buf,"file$%s",g->GSail->GetType().c_str());
    Post_Summary_By_Sail(g_boat,"file$",qPrintable(gIn->GSail->m_fileBAG));

#ifdef FORTRANLINKED
    Hoisted = 1;
    sli = g_boat->SailListIndex();
    cf_sethoisted	(sli,(Hoisted),	&err);
    cf_setactive	(sli,(Hoisted),	&err); // May 2003
#endif
    g_boat->AddToSummary();
    g_World->SetUpAllTransforms(0);
    g_World->PostProcessAllSails(3);
#ifdef _X
    UpdateMenu();
    SetCameraList();
#endif
    // for postprocessing put the boat into
#ifdef HOOPS
    HC_Open_Segment_By_Key(g->m_ModelSeg);
    HC_KEY datakey = HC_KOpen_Segment("data");
    HC_Close_Segment();
    HC_Open_Segment_By_Key(g->GSail->PostProcExclusive());
    HC_Open_Segment("none");
    HC_Flush_Contents(".","everything");
    HC_Include_Segment_By_Key(datakey);
    HC_Style_Segment_By_Key(g->GSail->PostProcTransformSeg());

    HC_Close_Segment();
    HC_Close_Segment();
    ReDraw_Nodes(g->GSail);
#endif
#ifdef _X
    set_up_whole_trimmenu ();  /* re-create trim menu even if still closed */
#endif
    int c;
    cf_resolve_connects( &c);  // wierd. this has already been called by SetUpAllTransforms
    return(SUCCESS);
}



