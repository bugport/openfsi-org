#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXOffset.h"
#include "RXRelationDefs.h"
#include "RXCompoundCurve.h"
#include "RXPside.h"
#include "RX_FEBeam.h"
#include "rxspline.h"

#include "stringutils.h"

#include "entities.h"
#include "etypes.h"
#include "f90_to_c.h"

#include "batdefs.h"
#include "batstiff.h"
#include "polyline.h"
#include "read.h"

#include "panel.h"

#include "batpatch.h"
#include "RXPanel.h"

#include "resolve.h"

#include "RX_FEPocket.h"
#define PDB (0)

RX_FEPocket::RX_FEPocket(void)
    : m_ZiLessTrim(0)
    , m_Trim(0)
{
    assert(0); // dont use the default constructor
    Init();

}

RX_FEPocket::RX_FEPocket(SAIL *sail)
    :RXEntity(sail)
    ,m_ZiLessTrim(0)
    , m_Trim(0)
{

    Init();
    m_sail=sail;
}
int RX_FEPocket::Compute(void){
    return 0;
}
int RX_FEPocket::Finish(void){assert(0); return 0;}


int  RX_FEPocket::CClear(){
    int rc=0;
    // clear stuff belonging to this object
    assert(!m_pEnd1dist);
    assert(!m_pEnd2dist);
    cc=0; // its  caught by the dependency system. no need here.
    if(cc) {
        for (int k=0;k<this->cc->npss;k++) {
            PSIDEPTR e = this->cc->pslist[k];
            e->Kill( );
        }
        for (int k=0;k<this->cc->m_ccNcurves;k++) {
            RXEntity_p e = this->cc->m_ccCurves[k];
            if(e->generated==30000) e->Kill(); /* dangerous.  Only if its a 'cut' batpatch*/
        }
    }
    if(this->m_p) RXFREE(this->m_p);  this->m_p=NULL;
    // call base class CClear();
    rc+=RX_FELinearObject::CClear();
    rc+=RXEntity::CClear ();
    return rc;
}

RX_FEPocket::~RX_FEPocket(void) // See Clear-Entity
{
    ClearFEA();
    this->CClear ();
}


int RX_FEPocket::Init(void)
{
    d=0;            /* depth.  May be zero */

    m_p=0;  /* must be malloced*/
    c=0;
    //	int type;  MUST be PATCH
    inserted=0;
    N_Angles=0;
    a=0;
    basecurveptr=0;
    partial=0;

    this->m_pEnd1dist=0;
    this->m_pEnd2dist=0;

    end1ptr=0;
    end1sign=0;
    end2ptr=0;  /* ptr to RXEntity of end2 {SITE|seam|curve} */
    end2sign=0;

    m_arc=0;      /* current arclength */

    m_Trim=0.; /* imposed length change on the batten in this pocket */
    strt_A=0;   /* an estimate of the initial slope of the IA curve */

    end1angle=0; /* ready for export */
    end2angle=0;

    e[0]=0; e[1]=0;/* the SITES or relsites at the ends. */

    mat=0;
    corner=0;
    cc=0;


    return 0;
}

//bool RX_FEPocket::SetOwner(RXEntity_p  e)
//{
//	if(!e) // also if its not a proper entity
//	return false;
//	m_owner=e;
//	return true;
//}

int RX_FEPocket::Dump(FILE * fp) const
{
    int k;
    const RX_FEPocket *np = this;
    class RXCompoundCurve *   cc = (class RXCompoundCurve *   )np->cc;
    int count = np->N_Angles;

    fprintf(fp,"    depth     %f\n",(*np).d );
    if((np->basecurveptr)!=NULL) {
        fprintf(fp,"    basecurve %s\n",(*np).basecurveptr->name() );
    }
    else
        fprintf(fp,"     basecurve NULL\n");

    {
        if((np->corner)!=NULL) {
            fprintf(fp,"    corner site %s\n",(*(*np).corner).name() );
        }
        else fprintf(fp,"     Corner NULL\n");
    }
    if(np->mat) {
        fprintf(fp,"    material %s\n",np->mat->name() );
    }
    else
        fprintf(fp,"     basecurve NULL\n");

    /* e1,e2 point to the entities containing the end sites */
    if ((*np).e[0] !=NULL) fprintf(fp,"    e1    %10s   %10s\n",(*(*np).e[0]).type(), (*(*np).e[0]).name());
    else fprintf(fp," e1 NULL\n");
    if ((*np).e[1] !=NULL) fprintf(fp,"    e2    %10s   %10s\n",(*(*np).e[1]).type(), (*(*np).e[1]).name());
    else fprintf(fp," e2 NULL\n");

    if(np->end1ptr)
        fprintf(fp,"    End1ptr   %10s\n",(*(*np).end1ptr).name() );
    else fprintf(fp,"    End1ptr  NULL\n");
    if(np->end2ptr)
        fprintf(fp,"    End2ptr   %10s\n",(*(*np).end2ptr).name() );
    else fprintf(fp,"    End2ptr  NULL\n");
    fprintf(fp,"    e1sign    %d\n",(*np).end1sign );
    fprintf(fp,"    e2sign    %d\n",(*np).end2sign );
    assert(!this->m_pEnd1dist);
    assert(!this->m_pEnd2dist);

    fprintf(fp,"    e1angle   %f\n",(*np).end1angle );
    fprintf(fp,"    e2angle   %f\n",(*np).end2angle );
    fprintf(fp,"    ArcLength %f\n",np->m_arc);
    fprintf(fp,"    strt_A    %f\n",(*np).strt_A );

    fprintf(fp,"    Npsides   %d\n",cc->npss);
    for (k=0;k<cc->npss;k++) {
        PSIDEPTR e = cc->pslist[k];
        fprintf(fp,"          %d     %s\n",k,  (*e).name());
    }
    fprintf(fp,"    Ncurves   %d\n",cc->m_ccNcurves);
    for (k=0;k<cc->m_ccNcurves;k++) {
        RXEntity_p e = cc->m_ccCurves[k];
        fprintf(fp,"          %3d     %s   (rev=%d)",k,  e->name(),cc->m_ccRevflags[k]);
        if(np->partial){
            if(cc->m_ccE1sites&&cc->m_ccE1sites[k]) fprintf(fp," between %s ",cc->m_ccE1sites[k]->name());
            if(cc->m_ccE2sites&&cc->m_ccE2sites[k]) fprintf(fp,"  and    %s ",cc->m_ccE2sites[k]->name());
        }
        else fprintf(fp," whole length ");
        fprintf(fp,"\n");
    }

    fprintf(fp,"    n_angles  %d\n\n",(*np).N_Angles );
    float sum=(float)0.0;

    for (k=0;k<count;k++) {

        if(np->a[k].Angle){
            sum=sum+(float) *(np->a[k].Angle);
            fprintf(fp,"    %f  ",*(np->a[k].Angle));
        }
        else
            fprintf(fp,"    angle NULL  ");
        fprintf(fp,"    %8S ",np->a[k].m_Offset->GetText().c_str());
        if(np->a[k].sign )
            fprintf(fp,"    %2d ", *(np->a[k].sign));
        else
            fprintf(fp,"    sign NULL  ");
        fprintf(fp,"    %10s", np->a[k].s->name());
        fprintf(fp," end%d\n",np->a[k].End);
    }
    fprintf(fp,"    Sum %f\n",sum);

    count = (*np).c;

    for (k=0;k<count;k++) {
        VECTOR v= np->m_p[k];
        fprintf(fp,"\t\t\t%d %f\t%f\t%f\n",k, v.x,v.y,v.z);
    }
    return 1;
}
void RX_FEPocket::SetTrim( const double x) {

    cout << "IN RX_FEPocket::SetTrim "<<endl;
    int sli,  n;
    sli = this->Esail->SailListIndex ();
    n = this->GetN();
    this->m_Trim = x;
    cf_setbattentrim( sli,  n,  x);
}


int RX_FEPocket::Mesh(void) // if ever we want it to create its own psides and intermediate nodes.
{//	printf("\n meshing Pocket  %s  doesnt do anything\n",m_owner->name    );
    int N = 0;
    return N;
}


// places the string into the FEA database
// The FEA database shares  memory allocated in C
// prerequisite is that the P structure is already prepared. 
int RX_FEPocket::AddFEA()
{ 
    int err=0, N=-1;
    SAIL * sail = GetSail();
    // first translate the attributes as for PrintStrings

    if(!IsInDatabase()) { //here we add a fortan FNODE to nodelist
#ifdef FORTRANLINKED
        N = cf_createfortranbatten(sail->SailListIndex(),&err);

#else
        assert(0);
#endif
        SetIsInDatabase(true);

        SetN(N);
    }
    return GetN();
}

// removes  from the FEA database
int RX_FEPocket::ClearFEA(void)
{ 
    int err=1;
    if(IsInDatabase()) {
#ifdef FORTRANLINKED
        cf_removefortranbatten(GetSail()->SailListIndex(),GetN(),&err);
#else
        assert(0);
#endif
        SetIsInDatabase(false);
    }
    return(!err);
}


// collects into the P structure. 
int RX_FEPocket::PostMesh(void) // essentially the same as PrintBattens
{	
    int k ;
    if(this->Needs_Resolving)
        return 0;
    sc_ptr scc;
    char text[33];  memset (text,' ',32);text[32]=0;

    int count,err;

    int sli=GetSail()->SailListIndex();

    if(PDB)	printf("\nPostMesh Pocket %s\n",this->name());
    strncpy(text,this->name(),strlen(this->name()  ));

    scc = (sc_ptr )cc;
    m_elist.clear(); m_ZiLessTrim=0;

    int l_ned =  CountPsideEdges(&(m_ZiLessTrim), scc->pslist,scc->npss );
    if(PDB)printf(" it has  %d edges\n",l_ned);
    if(l_ned<2) {
        // cout<< " short batten NOT inserted to FEA \nUse a finer mesh"<<endl;
        ClearFEA();
        return 0;
    }

    // its safe multiple times but this is usually the first call
    // now sort the edgelist into order.Fill in the Rev flags.
    if(!e[0])
        count = SortPsideEdges(); //cout<< " on return"<<endl; printelist(l_elist);
    else
        count = SortPsideEdgesWithSeed(e[0]); //cout<< " on return"<<endl; printelist(l_elist);
    assert(count==l_ned);
    if(count<2) {
        cout<< "non-contiguous batten skipped\n"<<endl;
        ClearFEA();
        return 0;
    }

    AddFEA();
    int n = GetN();
    if(PDB)printf("Call to  with count=%d\n", count);
#ifdef FORTRANLINKED
    cf_initialisebatten( sli, n,count,this,text, &err);// sets count and geom(hardcoded

#else
    assert(0);
#endif
    /* we have to provide
  character (len=32)	::text=b_name
zis,trim,,eas, Slide_Flag
96      DO J=1,count
         read(u,*,err=98) se(j),(ei(K,j),K=1,5),b0(j),(xib(K,j),K=1,3)
 */
    double *ei =( double *)CALLOC(count*6 +8 ,sizeof(double));
    double *dp =ei;
    double EI;
    int Islide=1;

    double PocketLength=0., elength ,KB,x1,x2,x3,curlength , l_ea;

    x1 = x2 = x3 = 0.0;
    class RXBeamMaterial *bm;
    double TestLength,l_force,diasq;
    RXExpressionI *q ;
    vector<FEEdgeRef>::iterator ii, inext;
    // get the overall length

    for(ii=	m_elist.begin();ii!= m_elist.end ();ii++) {
        FEEdgeRef r = *ii; inext=ii; inext++;
        FortranEdge *e= r.e;
        PocketLength += e->GetLength();
    }


    switch(mat->TYPE) {
    case BATTEN:
        curlength = 0.0;
        q = (mat->FindExpression (L"length",rxexpLocal));
        TestLength=q->evaluate () ;
        q = (mat->FindExpression (L"force",rxexpLocal));
        l_force=q->evaluate () ;

        diasq = sqrt(64.0 * TestLength*TestLength*l_force/(3.142*3.142*3.142*41.0E9 ) );// equivalent round GRP
        l_ea = (float) (41.0E9 * 3.142 * diasq / 4.0);

        for(ii=	m_elist.begin();ii!= m_elist.end ();ii++) {
            FEEdgeRef r = *ii; inext=ii; inext++;
            FortranEdge *e= r.e;
            elength = GetSail()->Get_Init_Edge_Length(e->GetN());
            x1 = curlength/PocketLength;
            curlength += elength;
            x2 = curlength/PocketLength;

            if(inext != m_elist.end ()) {
                x3 = (curlength + GetSail()->Get_Init_Edge_Length((*inext).e->GetN()))/PocketLength;
                KB = KBatten_Stiffness(mat,(float)x1,(float)x2,(float)x3);
            }
            else
                KB = 0.0;

            EI  = Batten_Stiffness(this->mat,(float)((curlength-elength/2.)/PocketLength));

            *dp=EI; dp++; *dp=EI;dp++;*dp=EI/2.; dp++;
            *dp=l_ea; dp++; *dp=KB; dp++; *dp=0.0; dp++;// dp is a pointer into array ei
        }


        break;
    case PCE_BEAMMATERIAL: // NOTE eiYY is the one that counts in KBattens. TO match Laurent's battens

        bm =(  class RXBeamMaterial *)  (this->mat);
        assert(bm);
        curlength = 0.0;
        for(ii=	m_elist.begin();ii!= m_elist.end ();ii++) {
            FEEdgeRef r = *ii; inext=ii; inext++;
            FortranEdge *e= r.e;
            elength = GetSail()->Get_Init_Edge_Length(e->GetN());
            x1 = curlength;
            curlength += elength;
            x2 = curlength;

            double emp[6]; // ea,ei2,ei3,gj,ti,beta
            bm->Evaluate((x1+x2)/2.0/PocketLength  ,  emp);
            EI  =  emp[2] ;
            l_ea = emp[0];

            if(inext != m_elist.end ()) { //else x3 would be off the end.
                x3 = curlength + GetSail()->Get_Init_Edge_Length((*inext).e->GetN()) ;
                KB = min(emp[1],emp[2])/fabs(x3-x1) ;
            }
            else
                KB = 0.0;

            *dp=emp[1]; dp++; *dp=emp[2];dp++;*dp=emp[3]; dp++;
            *dp=emp[0]; dp++; *dp=KB; dp++; *dp=0.0; dp++;// dp is a pointer into array ei
        }  // for ii
        break;
    default:
        assert(strlen ("this type of batten material isnt coded")==0);

    };//switch
    int *se = (int * ) CALLOC (count+8,sizeof(int) );
    int *rev= (int * ) CALLOC (count+8,sizeof(int) );

    for(k=0,ii=	m_elist.begin();ii!= m_elist.end ();ii++,k++) {
        FEEdgeRef r = *ii; inext=ii; inext++;
        FortranEdge *e= r.e;
        se[k] = e->GetN();
        rev[k] = r.rev;
    }

#ifdef FORTRANLINKED
    cf_fillbatten(sli,n,m_ZiLessTrim ,m_Trim,l_ea,Islide,se,rev,ei); // sets bo and xib to zero
#else
    assert(0);
#endif
    RXFREE (se);
    RXFREE (rev);
    RXFREE (ei);
    return 0;
}

// // really post-mesh.  Pockets are assumed to share edges and nods with the membrane
int RX_FEPocket::MeshAll( SAIL *sail)
{
    int l_count;
    l_count=0;
    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it)) {
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if (p->TYPE==POCKET ){
            class RX_FEPocket *st = (class RX_FEPocket*) p;
            l_count+=st->Mesh ();
            l_count+=st->PostMesh();
        }/* if a pocket */
    } /* for i */
    return(l_count);
}



// form batpatch

int  RX_FEPocket::Compute_Pocket() { // builds the pslist for the cc
    sc_ptr  sc;
    /* creates the list of psides. A batpatch is a list of curves
 A 'basecurve zero'	batten is between nodes
 It Uses the e1sites as it consists of a partial seamcurve
  All other batpatches consist of entire seamcurves
  */
    int i,j,OK;


    if(this->cc->TYPE ==PCE_DELETED) {
        cout<< "Compute_batpatch on deleted seamcurve\n"<<endl;
        RXSTRING buf = TOSTRING(L"Deleting Pocket because its SC is deleted : '") + TOSTRING(this->name()) + TOSTRING(L"'");
        rxerror(buf,2);
        this->Kill( );
        return(0);
    }
    class RXCompoundCurve * cc=(class RXCompoundCurve *   )this->cc;
    if(cc->pslist) {RXFREE (cc->pslist);	 cc->pslist=NULL; cc->npss=0;}
    // reconstruct the cc's  pslist. Not complete if the BP is partial.
    for(i=0;i<cc->m_ccNcurves;i++) {
        sc= cc->m_ccCurves[i];
        OK=1;
        if(this->partial) OK=0;
        for(j=0;j<sc->npss;j++) {
            PSIDEPTR ps;
            RXEntity_p node;
            ps = sc->pslist[j];
            node = ps->Ep(0);
            if(this->partial && node==cc->m_ccE1sites[i] ) OK=1;
            if(OK) {
                cc->pslist=(PSIDEPTR  *)REALLOC(cc->pslist,(cc->npss+1)*sizeof(PSIDEPTR));
                cc->pslist[cc->npss]=ps; cc->SetRelationOf(ps,parent,RXO_PSIDE_OF_CC,cc->npss); // not sure about 'parent'
                (cc->npss)++;
            }
            node = ps->Ep(1);
            if(this->partial && node==cc->m_ccE2sites[i]) OK=0;
        }
    }



    return(1);
}



int RX_FEPocket::Find_All_CrossesForPocket(struct PC_INTERSECTION**is,int *ni) {

    /*
  A more intelligent search routine which makes use of panel connectivity
 1)  To find the 'current' panel
  the first point of the batpatch.p->e[0]. call this e1p
  look at e1p->dataptr->pslist[0-psno]. called pse
   look at pse->dataptr->leftpanel[0-1]
   if it is not NULL, use it to seed the search.

   if the search gets going, fine.
   If it fails to find a cross on the first panel,
   try the next pside in the pslist

  2) Seed with the 'current' panel.
   search all the psides on that panel to look for a cross
   If a cross is found, change the current panel to be the one on the other side of the pside
   Keep going while the current panel is not NULL. */
    double s1,s2 = 0.0,s1max,s2max;
    VECTOR v;
    int found,iret,start;
    Site *e1;
    int kseed,lseed,panside;
    sc_ptr  sc;
    PSIDEPTR panps=NULL;

    Panelptr  pan, panseed;

    s1=  this->Esail->m_Linear_Tol;/* not close to batten start  */
    s1max= this->m_arc;
    start=1;
    v.x=v.y=v.z=0;
    e1 = (Site *)(this->e[0]);
    for(kseed=0;kseed<e1->PSCount();kseed++) {
        PSIDEPTR ps =  e1->PSList(kseed).pp;
        for(lseed=0;lseed<2;lseed++) {
            panseed = ps->leftpanel[lseed];
            if(panseed) {
                pan=panseed;
                do {
                    found=0;
                    for (panside=0;panside< pan->m_psidecount;panside++) {
                        panps=pan-> m_pslist[panside];
                        sc = (sc_ptr  ) panps->sc;
                        if(sc->edge)
                            iret=0;
                        else {
                            s2 = (panps->End1off->Evaluate( sc,1));
                            s2max = (panps->End2off->Evaluate( sc,1));
                            if(s2max<=s2){
                                char str[256];
                                sprintf(str,"s2max (%f) < s2 (%f) in %s side %d\n",s2max,s2, pan->name(),panside);
                                rxerror(str,1);
                            }
                            iret=Find_Polyline_Intersection(this->m_p,sc->p[1],this->c,sc->c[1],&s1,&s2,&s1max,&s2max,&v);
                        }
                        /* 1 for cross, 2 for parallel 0 for none */

                        if(iret==1) {
                            if(s1< this->m_arc- this->Esail->m_Linear_Tol)  {	  /* not close to batten end */
                                found=1;
                                start=0;

                                *is = (PC_INTERSECTION *)REALLOC (*is,((*ni)+1 )*sizeof(struct PC_INTERSECTION));
                                if(!(*is)) {
                                    rxerror("Realloc in batpatch failed ",2);
                                    continue;
                                }
                                ((*is)[*ni]).s1 = s1;
                                ((*is)[*ni]).flag =1;
                                ((*is)[*ni]).s2 = s2;
                                ((*is)[*ni]).seam = sc;
                                PC_Vector_Copy(v,&(((*is)[*ni]).v));
                                (*ni)++;
                            }
                            else {
                                // cout<< "discard because at end\n"<<endl;
                            }
                            s1 = s1 + (float) this->Esail->m_Linear_Tol;/* not close to last one */
                            break;
                        }

                    }
                    if(found) {	/* a match found */
                        Find_Panel_Opposite(&pan,panps);
                    }
                    else {
                        if(!start)return(*ni);
                        break;	/* out of do while */
                    }
                }
                while(pan);
            }	/* end of (if pan) */
        }   /* end of (for lseed) */
    }	/* end of (for kseed) */
    return(*ni);
}

int  RX_FEPocket::Create_Bat_BasecurveForPocket(int k,struct PC_INTERSECTION*a,int ni) {
    char bcname[256];
    int j,i1,bc;
    ON_3dVector l_z;
    class RXBCurve* bcp =0;//  (PC BASE_CURVE *)CALLOC(1,sizeof(struct PC BASE_CURVE));
    rxerror("please step RX_FEPocket::Create_Bat_BasecurveForPocket  ",3);
    assert(this->cc);
    sprintf(bcname,"%s%d",  this->cc->name(), k);assert(strlen(bcname)<255);

    /* the basecurve is from knot a[k].i to knot a[k+1].i
 unless k is 0, in which case first is 0
 or k is  ni, in which case last is p->c*/
    if(ni==0) {
        i1=0;
        bc = this->c;
    }
    else if(k==ni) {
        i1=a[k-1].i;
        bc = this->c - i1;
    }
    else if(k==0) {
        i1=0;
        bc = a[0].i + 1;
    }
    else {
        i1=a[k-1].i;
        bc = a[k].i - i1 + 1;
    }
    bcp->p = (VECTOR *)MALLOC((bc+1)*sizeof(VECTOR));
    bcp->c=bc;
    for (j=0;j<bc;j++) {
        PC_Vector_Copy(this->m_p[j+i1],&(bcp->p[j]));
    }
    l_z =this->cc->Normal();
    PC_Normalise_Polyline(bcp->p,bcp->c,l_z );
    this->SetRelationOf(this->cc->Esail->Insert_Entity( "basecurve",bcname,bcp,(long)0," "," ! basecurve made by batpatch"),spawn,RXO_BC_OF_POCKET);
    return(1);
}


int RX_FEPocket::Create_Bat_CurveForPocket( RXEntity_p site1,RXEntity_p site2,const int k) {
    char line[1256];
    char bcname[256];
    class RXCompoundCurve *   cc=(class RXCompoundCurve *   )this->cc;
    SAIL *sail = this->GetSail();
    sprintf(bcname,"%s%d",  this->cc->name(), k);
    if(sail->GetMould())
        sprintf(line,"curve: %s%d : %s : %s : %s :0.0 :fanned Xscaled YasX Yabs draw moulded: : : :!made by batpatch", cc->name(), k,site1->name(), site2->name(),bcname);
    else
        sprintf(line,"curve: %s%d : %s : %s : %s :0.0 :fanned Xscaled YasX Yabs draw : : : :!made by batpatch", cc->name(), k,site1->name(), site2->name(),bcname);

    cc->m_ccCurves[k]=(sc_ptr) Read_SeamCurve_Card(sail,line);
    cc->m_ccCurves[k]->Needs_Finishing=!cc->m_ccCurves[k]->Finish();
    cc->m_ccCurves[k]->generated = 30000; /* for the delete routine */
    return(1);
}

int RX_FEPocket::Compute_Pocket_Geometry() {

    /*  We have two end points. They may be SITEs or offsets on curves or seams.
First we find them. Then we call up the basecurve. We rotate it to fit
between the endpoints taking the adjectives into account.
We insert a HOOPS polyline and return the key of the containing segment. 
If the attribute contains the word 'moulded' we pull the line to the mould
  */
    //RXEntity_p e=this;
    char flag[10];

    ON_3dVector xl,yl; float chord;
    int k, Xabs,Yabs,YasX,iret=1;
    //  char*att=this->attributes;
    ON_3dPoint e1,e2;
    RXEntity_p bceptr = this->basecurveptr;
    RXEntity_p mould = this->Esail->GetMould();

    switch (bceptr->TYPE) {
    case SEAMCURVE:{	// copy its curve[1] into p->p;
        sc_ptr sc = (sc_ptr  )bceptr;

        if(this->m_p) RXFREE(this->m_p);
        this->m_p=(VECTOR *)MALLOC(sc->c[1]*sizeof(VECTOR));
        memcpy(this->m_p, sc->p[1], sc->c[1]*sizeof(VECTOR));
        this->c = sc->c[1];
        this->end1ptr = this->e[0] = sc->End1site;
        this->end2ptr=	 this->e[1] = sc->End2site;
        break;
    }

    case TWODCURVE :
    case BASECURVE :
    case THREEDCURVE :
    {
        class RXBCurve* bcptr = (class RXBCurve*) bceptr;
        ON_3dVector l_z;
        float depth=this->d;

        int bc=(*bcptr).c;
        int err = 0;
        VECTOR* base = (VECTOR *)MALLOC(sizeof(VECTOR)*(bc+1));

        HC_Open_Segment(this->type());
        this->hoopskey = HC_KOpen_Segment(this->name());
        HC_Flush_Geometry(".");
        assert(!this->m_pEnd1dist);
        assert(!this->m_pEnd2dist); err=1;
        if(err) { iret= -1; goto End;}
        xl=e2-e1;
        chord= xl.Length();

        if (chord<0.00001) {
            char string[256];
            sprintf(string,"Zero length between %sand %s",(*(*this).end1ptr).name(),(*(*this).end2ptr).name());
            rxerror(string,2); iret= -1; goto End;;
        }
        xl.Unitize ();
        l_z =this->Normal();
        yl=ON_CrossProduct(l_z,xl) ;

        /* attribute options are:
 flat/fanned.        Flat means ignore imposed angles.
 Xabs/Xscaled        Xabs means trim curve to fit (eg preserve radius)
 Yabs/Yscaled       Yabs means use Y offsets. scaled = make d=depth
 YasX/Yscaled 	    YasX means preserve aspect ratio during scaling
  */
        Xabs=0;Yabs = 0; YasX=0;
        if(this->AttributeFind("$yscaled")) {
            Yabs=0;
            YasX=0;
        }
        else if (this->AttributeFind("yasx")) {
            Yabs= 0;
            YasX=1;
        }
        else if (this->AttributeFind("yabs")) {
            Yabs=1;
            YasX=0;
        }
        /* extract the base curve offsets */

        Scale_Base_Curve((*bcptr).p, base,bc,depth,Xabs,Yabs,YasX,(double)0.0);

        /* base is the base-curve. Y in m and x from 0 to 1 */

        strcpy(flag,"curve");

        Make_Fanned_Batpatch(this,flag,&this->m_arc,&(*this).strt_A,chord,&bc,&base,(*this).a,0 );	 /* sets arc */

        this->c=bc;
        this->m_p = (VECTOR *)REALLOC(this->m_p,(bc+1)*sizeof(VECTOR));
        for (k=0;k<bc;k++){
            ((this->m_p)[k]).x = e1.x + xl.x*base[k].x +  yl.x*base[k].y ;
            ((this->m_p)[k]).y = e1.y + xl.y*base[k].x +  yl.y*base[k].y ;
            ((this->m_p)[k]).z = e1.z + xl.z*base[k].x +  yl.z*base[k].y ;
        }
        (this->m_p[bc-1]).x  =(float) e2.x;
        (this->m_p[bc-1]).y  =(float) e2.y;
        (this->m_p[bc-1]).z  =(float) e2.z;
        this->end1angle=0.0;
        this->end2angle=0.0;
        if(this->AttributeGet("moulded")) {
            PC_Pull_Pline_To_Mould(mould,&(this->m_p), &bc,1,this->Esail->m_Linear_Tol);
        }

        HC_Set_Color("lines=blue");

        RX_Insert_Polyline(bc,&(this->m_p[0].x), this->GNode(),0);

End:	 HC_Close_Segment();
        HC_Close_Segment();

        RXFREE(base);
        break;
    }
    default:
        rxerror("this pocket has bad basecurve type",3);

    }; // end switch

    return(iret);
}

int RX_FEPocket::Insert_Pocket() { // a CUT pocket.  Very old.
    /* First find the curve position
 Then get the crosses
 and the parallels
 generate the seamcurves
 Build up the curve list
 set the material on the psides generated
 */

    /* First find the curve position*/
    RX_FEPocket *p=this;
    RXEntity_p site1, site2;
    struct PC_INTERSECTION*is=NULL; int ni;
    sc_ptr  sc;
    class RXCompoundCurve *cc = (class RXCompoundCurve *)p->cc;
    Site *s1,*s2;

    char line[256];
    double foffset,arc;
    int k, Need_Crosses;

    /* the following section requires ps lists on nodes */

    if (!p->basecurveptr) {
        if(p->partial){

            Find_Curves_Between(p->e[0],p->e[1],&(cc->m_ccCurves),&(cc->m_ccNcurves));

            if(!cc->m_ccNcurves) {
                return(1);
            }
            if(cc->m_ccNcurves>1) {rxerror("too many Ncurves",1);  return(1);}
            cc->m_ccE1sites=(RXEntity_p *)CALLOC(2, sizeof(RXEntity_p ));
            cc->m_ccE1sites[0]=p->e[0];
            cc->m_ccE2sites=(RXEntity_p *)CALLOC(2, sizeof(RXEntity_p ));
            cc->m_ccE2sites[0]=p->e[1];
            cc->m_ccRevflags=(int *)CALLOC(1,sizeof(int)); // Jan 2006
        }
    }

    if (!p->basecurveptr) { p->inserted=1 ;return(0);}

    /* There is a bug in inserting batpatches in TPN designs.
    Sometimes the red-space coords can be taken from the wrong scurve
  This is the trap. It deletes offending batpatches */
    s1 = (Site *)p->e[0];
    s2 = (Site *)p->e[1];
    if(s1->PSCount()>2 ||s2->PSCount()>2  )	{
        if (this->Esail->GetFlag(FL_ISTPN)){
            char s[256];
            sprintf(s,"(TPN) cannot insert batten/patch\n %s\n It ends on a panel corner" ,this->name());
            rxerror(s,2);
            p->cc->Kill();
            this->Kill();
            return(0);
        }
    }

    p->Compute_Pocket_Geometry();  /* REALLOCs p */
    /*Then get the crosses   and the parallels	*/
    Need_Crosses = 1;

    if(Need_Crosses){ // if its NOT a PATCH SC-draw type
        ni=0;
        p-> Find_All_CrossesForPocket(&is,&ni) ; // uses p->p to make (is,ni)

        for(k=0;k<ni;k++) {
            sc = (sc_ptr  )is[k].seam;
            arc = sc->GetArc(1);
            foffset = is[k].s2/arc* 100.0;
            sprintf(line,"relSite: %s%d : %s : %f%% :! made by batpatch",cc->name(), k,is[k].seam->name(),foffset);
            is[k].site=Read_Relsite_Card(this->Esail ,line);
            is[k].site->Needs_Finishing=!is[k].site->Finish();
            this->SetRelationOf(is[k].site,child,RXO_SITE_OF_POCKET);
            is[k].seam->Break_Curve();   /* EXPERIMENT MAYBE WRONG 12/1/95 */
        }
        /* sort */

        qsort (is,ni,sizeof(struct PC_INTERSECTION),Bat_Sort_Fn);


        Put_Crosses_In_Poly(is,ni, &(p->m_p),&(p->c));

        /* is[k].i is now the polyline knot index of intersection k */
        /* a note on deps. THe SCs are deps of the sites, so they
     dont need to be pushed onto the batpatch deplist */

        p->partial=0;
        cc->m_ccCurves = (sc_ptr *)CALLOC((ni+2),sizeof(RXEntity_p ));

        cc->m_ccRevflags = (int*)CALLOC((ni+2),sizeof(int));
        site1=p->e[0];
        for(k=0;k<ni;k++) {
            site2=is[k].site;
            p->Create_Bat_BasecurveForPocket(k,is,ni);
            p->Create_Bat_CurveForPocket(site1,site2,k);
            cc->m_ccCurves[k]->Break_Curve();
            site1=site2;
        }
        site2 = p->e[1];
        p->Create_Bat_BasecurveForPocket(ni,is,ni);
        p->Create_Bat_CurveForPocket(site1,p->e[1],ni);
        cc->m_ccCurves[ni]->Break_Curve();
        cc->m_ccNcurves=ni+1;

        if(cc->m_ccNcurves==1) {
            Site*sp;
            /* 	error(" fixup for no crosses",1);   */
            sp = (Site*)site1;
            if(sp) {
                sc_ptr scp= (sc_ptr) sp->CUrve[0];
                if(scp){
                    scp->Make_Pside_Request=2;
                }
            }
        }

        if (is)
            RXFREE(is);
    }

    return(1);
}
int RX_FEPocket::ReWriteLine(void)
{
    /* Pocket card is of type
   Pocket:  name  :  SITE1 : SITE2 : batten [optional thickness] : basecurve : [depth :[optional adjectives]] !comment

   if the basecurve field is empty, it lies on any curve between site1 and site2
   if SITE1 and SITE2 are empty, the basecurve field must apply to a curve or compound curve*/
    cout<< "RX_FEPocket::ReWriteLine() not implemented \n" ;

    return 0;
}

int RX_FEPocket::Resolve() {

    /* Pocket card is of type
   Pocket:  name  :  SITE1 : SITE2 : batten [optional thickness] : basecurve : [depth :[optional adjectives]] !comment

   if the basecurve field is empty, it lies on any curve between site1 and site2
   if SITE1 and SITE2 are empty, the basecurve field must apply to a curve or compound curve*/

    char name[256],nName[256],n1[256],n2[256],basecurve[256];
    char att[256],type[256],material[256],mattype[256]; *mattype=0;
    float depth,thickness;
    RXEntity_p p1,p2,p3=NULL;

    HC_KEY key=0;
    int k;
    char line[1024]; strcpy(line,  this->GetLine());
    int retval=0;
    class RXCompoundCurve *l_ccd;
    *nName=0;
    k = Parse_BP_Line(this->Esail,line,type,name,nName,n1,n2,material,&thickness,basecurve,&depth,att);
    if(k) {

        /* a curve-style batten */
        if(rxIsEmpty(n1) && rxIsEmpty(n2)) {
            if(!strieq(type,"pocket")) return(0);

            this->cc = dynamic_cast<class RXCompoundCurve * > (this->Esail->Get_Key_With_Reporting("compound curve",basecurve));
            if(!this->cc){
                return(0);
            }
            this->cc->SetRelationOf(this,child,RXO_CC_OF_POCKET);;

            l_ccd = (class RXCompoundCurve *  )this->cc;

            this->end1ptr = this->e[0] = p1 =  l_ccd->End1site;
            this->end2ptr = this->e[1] = p2 =  l_ccd->End2site;
            this->d=depth;

            this->mat= this->Esail->Get_Key_With_Reporting("batten,beammaterial",material,true);
            if (!this->mat ) 	{
                this->Init();
                return(0);
            }
            this->mat->SetRelationOf(this,child,RXO_BAT_OF_POCKET);
            this->mat->SetNeedsComputing();
        }
        else 	   // n1,n2 are non-zero
        {
            p1= this->Esail->Get_Key_With_Reporting("site,relsite",n1);
            p2= this->Esail->Get_Key_With_Reporting("site,relsite",n2);

            if (p1==NULL ||p2==NULL ) {
                return(0);}

            p1->SetRelationOf(this,child,RXO_N1_OF_POCKET);
            p2->SetRelationOf(this,child,RXO_N2_OF_POCKET );

            this->e[0] = p1; this->e[1] = p2;
            this->end1ptr=p1;this->end2ptr=p2;
            this->d=depth;


            strcpy(mattype,"batten");
            if (rxIsEmpty(basecurve) || strieq(basecurve,"null")) {
                p3=NULL;
                this->basecurveptr=NULL;
                this->partial=1;
            }
            else {
                this->partial=0;
                p3 = this->Esail->Get_Key_With_Reporting("basecurve",basecurve);
                if (p3==NULL ) 	{
                    cout<< "??re-init a pocket for no BC??"<<endl;
                    return(0);
                }
                p3->SetRelationOf(this,child,RXO_BC_OF_POCKET);

            }

            this->mat= this->Esail->Get_Key_With_Reporting(mattype,material);
            if (!this->mat) 	{
                cout<< "??re-init a pocket for no MAT??"<<endl;
                return(0);
            }
            this->mat->SetRelationOf(this,child,RXO_MAT_OF_POCKET);;
            this->mat->SetNeedsComputing();

            this->basecurveptr=p3;
            int depth = this->generated+1;
            class RXCompoundCurve *ccd=dynamic_cast<class RXCompoundCurve *> (Just_Read_Card(this->Esail ,0,"compound curve",name,&depth));
            this->SetRelationOf (ccd,spawn,RXO_CC_OF_POCKET);
            this->cc = ccd;

            this->cc ->PC_Finish_Entity("compound curve",name,ccd,(long)0,NULL,NULL,NULL);
            ccd->Needs_Resolving=0;
            ccd->Needs_Finishing=0;
            this->SetRelationOf(this->cc,child,RXO_CC_OF_POCKET);
            this->SetRelationOf(ccd,spawn|child,RXO_CC_OF_POCKET);
        } // n1,n2 are non-zero

        if(this->PC_Finish_Entity("pocket",name,this,key,NULL,att,line)) {
            this->Needs_Resolving=0;
            this->Needs_Finishing=1;
            retval = 1;
        }
    }
    return(retval);
}
