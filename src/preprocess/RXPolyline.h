#ifndef _RXPolyline_HDR__
#define _RXPolyline_HDR__
#include <vector>
#include "vectors.h"
#include "RXTriplet.h"

class RXPolyline 
{
public:
    RXPolyline(void);
    RXPolyline(ON_3dPointArray &p);
    ~RXPolyline(void);
    void clear(void) { m_p.clear();   }
    // copy constructor
    RXPolyline&  operator=(const RXPolyline& src);

    int Init(void);

    RXTriplet& operator[]( int i );
    RXPolyline& operator-=(const RXPolyline& p);

    RXPolyline operator*(const  double d ) const;
    RXPolyline operator/(const double d ) const;

    RXPolyline operator+( const RXPolyline& p ) const;

    RXSTRING TellMeAbout() const;
    void push_back( const RXTriplet p) {m_p.push_back(p);}
    size_t size()const {return m_p.size();}
    int ToVectorArray( VECTOR **p, int*c);
    int Group(const int nmax, const double tol);
    int Dump(FILE *fp) const;
private:
    std::vector<RXTriplet> m_p;
}; //class RXPolyline

class RXExpPolyline : public  RXPolyline
{
public:
    RXExpPolyline(void);
    RXExpPolyline(ON_3dPointArray &p);
    ~RXExpPolyline(void);
    void clear(void) { m_p.clear();   }
    // copy constructor
    RXExpPolyline&  operator=(const RXExpPolyline& src);

    int Init(void);

    RXTriplet& operator[]( int i );
    RXExpPolyline& operator-=(const RXExpPolyline& p);

    RXExpPolyline operator*(const  double d ) const;
    RXExpPolyline operator/(const double d ) const;

    RXExpPolyline operator+( const RXExpPolyline& p ) const;

    RXSTRING TellMeAbout() const;
    void push_back( const RXExpTriplet p) {m_p.push_back(p);}
    size_t size()const {return m_p.size();}
    int ToVectorArray( VECTOR **p, int*c);
    int Group(const int nmax, const double tol);
    int Dump(FILE *fp) const;
private:
    std::vector<RXExpTriplet> m_p;
}; //class RXPolyline


#endif

