// layervertex.h: interface for the layervertex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYERVERTEX_H__3D10180F_2471_4902_AFEE_6C12BAD34E80__INCLUDED_)
#define AFX_LAYERVERTEX_H__3D10180F_2471_4902_AFEE_6C12BAD34E80__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "layerobject.h"
#include "layernode.h"
#include "layerlayer.h"
class layervertex : public layerobject  
{
public:
	int Print(FILE *fp) const;
	layervertex();
	layervertex(layernode *n, const int p_nodeno,   Lply  *p_ply);
	virtual ~layervertex();
	layernode* m_node;
private:
	Lply * m_ply;

};

#endif // !defined(AFX_LAYERVERTEX_H__3D10180F_2471_4902_AFEE_6C12BAD34E80__INCLUDED_)
