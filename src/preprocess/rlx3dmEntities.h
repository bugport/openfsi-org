/* header file for rlx3dmEntities.h function Thomas Ricard, PH 2003*/
#pragma once
#include <QString>
// #inc lude "rxON_Extensions.h" // needed for AMGTAPER

#ifndef ON3DM_NAME_SEPARATOR
//#define ON3DM_NAME_SEPARATOR  "()/$[]{},:;�<>&"
#define ON3DM_NAME_SEPARATOR  "()/$[]{},:;<>&"
#endif //#ifndef ON3DM_NAME_SEPARATOR

struct PC_3DM_Model {
    char * m_name;
    //	char * m_filename;
    QString m_qFilename;
    char *m_segname;
    RXEntity_p m_pOwner; // the command that calls the import
    class rxONX_Model * m_pONXModel; // The model
}; //struct PC_3DM_Model

struct PC_3DM_Entity {
    class rxONX_Model * m_pModel; // The mode which the object belongs to
    ONX_Model_Object * m_pObj; //The 3dmObject (Point Curve surface....)
    class RXSail *m_sail;
};  //struct PC_3DM_Entity


