/*
C**********************************************************************
      
      SUBROUTINE Prepare_NL_Mat(ex,fx,fy,f45,N,ref,nua,zka,SW
     & , enout,fnout,esout,fsout,errout)
      IMPLICIT NONE

C**********************************************************************

C arguments
      REAL*8 ex(*),fx(*),fy(*),f45(*)  ! test data
      INTEGER N			       ! point count
      REAL*8 ref		       ! strain at whic nu defined
      REAL*8 nua			       ! poissons, INPUT & OUTPUT
	  REAL*8 SW					! strip width
C returns
      REAL*8 zka		    	       ! 90 deg factor
      REAL*8 enout(*),fnout(*),esout(*),fsout(*)   ! the interpolaing functions
       INTEGER errout
*/

#ifndef NONLIN_16NOV04
#define NONLIN_16NOV04


extern "C" void	
fc_prepare_nl_mat(double*ex,double*fx,double*fy,double*f45,int*N,
		double*f,double*nua, double*zka, double*sw,
		double*enout, double*fnout, double* esout,  double*fsout,
		int* er
		);

#endif //#ifndef NONLIN_16NOV04
