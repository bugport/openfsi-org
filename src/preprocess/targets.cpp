/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	
 * 		P Heppel  MArch 1996
 *
  *  9/97   put_v sets needs_computing on the nieces. Increases the chance that the solve
	 will kick off.
	But the real problem is that there are errors order (solve tol) 
	and so a dimension change of the same size will have its result swamped by errors
	One technique would be to kick UP small proposals by a factor of 5 or so
	to get the matrix, and not to update the matrix when the changes were small
 *   6/5/96 'measure' keyword added. Its a passive thing. Just prints the value Whatfor is 3
 *   25/3/96 int function scale factor allowed
 *   21/3/96 length or redlength refers to arcs[2] to work with CUT curves
 * 	 Generates a matrix of influence coeffs between inputs and targets from successive tests
 *   If two successive tests yield the same results, This causes a matrix inversion error
 *   Once this happens, it means we are close. So do not continue to update the matrix

 	 Shimmer is introduced into successive tests by halving all the delta inputs except
 	 one (K) which is rotated. 

	 Once the matrix fails to invert, we might (but dont) turn off this shimmering 
	 on the grounds that the matrix is good enough

	 It would be good to have a global variable controlling, eg by adjusting the no of cycles
	 and whether to turn off shimmering
 *
 */


/* targets.c */

#include "StdAfx.h" 
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RXSeamcurve.h"
#include "RXSRelSite.h"
#include "RXQuantity.h"
#include "RXOffset.h"

#include "panel.h"
#include "resolve.h"
#include "entities.h"
#include "etypes.h" 
 
#include "matinvd.h"
#include "polyline.h"
#include "interpln.h"

#include "targets.h"

int Get_Target_Ptr(struct PC_GOALSEEK *p,RXEntity_p ec,const char *object){
  if(ec->TYPE==SEAMCURVE) {
  	sc_ptr  np = (sc_ptr  )ec;
		if(strieq(object,"depth")) {
				p->Type=PCT_QUANTITY;
				p->ptr = ec->FindExpression(L"depth",rxexpLocal);
		}
		else if((strieq(object,"redlength"))|| (strieq(object,"length")) ){
				p->Type=PCT_FLOAT;
				p->ptr = &(np->m_arcs[2]); /* a CUT curve uses arcs[2] specially */
		}
		else if(strieq(object,"blacklength")) {
				p->Type=PCT_FLOAT;
				p->ptr = &(np->m_arcs[1]);
		}
		else return 0;

  }
  else   if(ec->TYPE==SITE) {
  	RXSRelSite*np = (Site *)ec;
		if(strieq(object,"x")) {
				p->Type=PCT_RXEXPRESSION;
				p->ptr = np->FindExpression(L"a",rxexpLocal);
		}
		else if(strieq(object,"y")) {
				p->Type=PCT_RXEXPRESSION;
				p->ptr = np->FindExpression(L"b",rxexpLocal);
		}
		else if(strieq(object,"z")) {
				p->Type=PCT_RXEXPRESSION;
				p->ptr = np->FindExpression(L"c",rxexpLocal);
		}
		else return 0;

  }
   else if(ec->TYPE==RELSITE) {
   RXSRelSite*np = (Site *)ec;
		if(strieq(object,"offset")) {
				p->Type=PCT_OFFSET;
				p->ptr = &(np->Offsets[0]);
		}
		else return 0;
   }
    else if(ec->TYPE==INTERPOLATION_SURFACE) {
    struct PC_INTERPOLATION *np = (PC_INTERPOLATION *)ec->dataptr;
		if(strieq(object,"Factor")) {
				p->Type=PCT_QUANTITY;
				p->ptr =ec->FindExpression(L"factor",rxexpLocal);// &(np->Factor);
		}
		else return 0;
   } 
      else if(ec->TYPE==PCE_SETDATA) {
 		   ec->OutputToClient(" DATA not supported as goalseek",2);
		 return 0;
   }  
   else {
	 printf("Get Target Ptr cant find %s %s %s  \n",ec->type(), ec->name(),object);
	 return 0;

}



return(1);
}


int Resolve_Goalseek_Card(RXEntity_p e){

/* responds to keywords 'variable' or ' target' 
  compound curve card is of type
     
     variable : <type> : <name> : key  : cap [: tol] 
 or  target   : <type> : <name> : key  :targetvalue [: tolerance]    
     */
 // char key[256], ename[256],type[256], name[256],object[256],vstr[256];

  RXEntity_p ec;
   struct PC_GOALSEEK *p;
     const char*line = e->GetLine(); 
  int retval = 0;
	std::string sline(e->GetLine());
	rxstriptrailing(sline);
	std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
	int nw = wds.size();
	const char*key, *ename,*type,*name,*object,*vstr,*w6;
	if(nw<  6) return 0;

	key=wds[0].c_str(); {
	ename=wds[1].c_str();  {//  if(HC_Par se_String(line,":",1,ename)) { 
	type=wds[2].c_str();  {   //if(HC_Par se_String(line,":",2,type)) {
	name=wds[3].c_str();  { //if(HC_Par se_String(line,":",3,name)) {
	object=wds[4].c_str(); {// if(HC_Pa rse_String(line,":",4,object)) {
	vstr=wds[5].c_str(); {   //	if(HC_ Parse_String(line,":",5,vstr)) {
 					      ec = e->Esail->Get_Key_With_Reporting(type,name);
					      if(!ec) return(0);
						  p = (PC_GOALSEEK *)CALLOC(1,sizeof(struct PC_GOALSEEK));

    					  p->e=ec;
						  if strieq(key,"variable")	p->whatfor=2;
						  if strieq(key,"target")	p->whatfor=1;
						  if strieq(key,"measure")	p->whatfor=3;
						  if(!Get_Target_Ptr(p,ec,object)) {RXFREE(p); cout<< "get targetptr failed\n"<<endl; return 0;}
 						 // p->value = Evaluate_Quantity(e->Esail,vstr,"length");
						  sscanf(vstr,"%lf",&(p->value ));

						  p->tol = 0.001;
						  if(nw>6)
								 p->tol = atof(wds[6].c_str());

					      if(e->PC_Finish_Entity(key,ename,p,(long)0,NULL," ",line)) {
								e->Needs_Finishing=1;
								e->Needs_Resolving=0;
								retval = 1;
						}
						}
      				}  
    			}
    		}
		}
  	}
  
  return(retval);
}
int Print_Target_Summary(char *filename,RXEntity_p *meas,int nm){
FILE *fp;
   struct PC_GOALSEEK*g;
   double v;

int k;
if(!nm) return 1;
fp = RXFOPEN(filename,"w"); if(!fp) {  (*meas)->OutputToClient("couldnt open measurement file",2); return 0; }

	fprintf(fp, " MEASURE \n name \t  value \t target \t error \n" );
	 for(k=0;k<nm;k++) {
			 g = (PC_GOALSEEK *)meas[k]->dataptr;
			 v = Target_Value(g);		
			fprintf(fp, " %s \t  %f \t %f \t %f\n", g->e->name(), v, g->value,g->delta );
		 }
FCLOSE(fp);
return 1;
}

int RXSail::GoalSeek(  int *RE_PANEL_FLAG) {
	SAIL *sail=this;
 /* 
  count the number of variables and targets 
  Check they are the same as before. If not, we start from scratch
  ELSE

  We start assuming the jacobian is Diag[0.5]
  We make a series of tests.  vary an input and measure the change in the OP
  
  This give a matrix 
  
 return 1 if targets have been met */
  int N = 0;
  int i,j,k,l,nt=0,nv=0,nm=0, OKde;
  int K1,Still_Going = 30;
//  RXEntity_p *oldins =NULL; april 2003
//   RXEntity_p *oldouts=NULL;  april 2003 
   RXEntity_p *ins=NULL,*outs=NULL,*meas=NULL;
   struct PC_GOALSEEK*g;
   double v;

   int isol, idsol, nr, nc; int mca=11;double det;
   double*A=NULL;
    double*de=NULL;  
    double*dv=NULL;
    int K=0;
   double *a=NULL; /* a[10][11]; */
  	double*ecurr=NULL;
	double*eold=NULL;
	double*deltas=NULL;
	N=0; /* WRONG DEBUG */

	ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it)) {
		RXEntity_p e  = dynamic_cast<RXEntity_p>( it->second);
	   if (e->TYPE==PCE_GOAL && e->Needs_Resolving==0) {
		  g = (PC_GOALSEEK *)e->dataptr;
		  if (g->whatfor==2){
 			ins = (RXEntity_p *)REALLOC(ins,(nt+1)*sizeof(RXEntity_p ));
			ins[nt++]= e;
		
		  }
		  else if (g->whatfor==1){
		  	outs = (RXEntity_p *)REALLOC(outs,(nv+1)*sizeof(RXEntity_p ));
			outs[nv++]= e;
			 v = Target_Value(g);
			 printf(" TARGET %s %s   %f %f error=%f\n",e->name(), g->e->name(), v, g->value, v- g->value );
		  } 
		 else if (g->whatfor==3){ /* just print */
			meas = (RXEntity_p *)REALLOC(meas,(nm+1)*sizeof(RXEntity_p ));
			meas[nm++]= e;
			 v = Target_Value(g);
		
		  } 
		else printf(" unknown whatfor value %d\n", g->whatfor);
	   }
	}
	Print_Target_Summary("measure.out",meas,nm) ; 
	if(nt !=nv) {rxerror("targets must match variables",1); return(1);}
	if(!nt) return 1; 
	if(N !=nt || N !=nv) {/* new array */
	N=nt;
	mca = N;
	  A = (double *)REALLOC(A,N*N*sizeof(double));
	  de = (double *)REALLOC (de,N*N*sizeof(double));
	  dv = (double *)REALLOC (dv,N*N*sizeof(double));
	  a = (double *)REALLOC(a,(mca*mca+2+2*mca)*sizeof(double));
	  ecurr = (double *)REALLOC (ecurr,N*sizeof(double));
	  eold = (double *)REALLOC (eold,N*sizeof(double));
	  deltas = (double *)REALLOC (deltas,N*sizeof(double));

	
	  l=0;
	   for(k=0;k<nv;k++) {
		 for(j=0;j<nv;j++) {
			 de[l]=0.0; dv[l++]=0.0;
	   }
	   }
	    l=0;
	   for(k=0;k<nv;k++) {
		 de[l]=1.0; 
		 dv[l]=0.05;
		 l+=(1+nv);
	   }
	 
	 for(k=0;k<nv;k++) {
		 g = (PC_GOALSEEK *)outs[k]->dataptr;
		 v = Target_Value(g);
		 g->delta =  v - g->value;
	 printf(" TARGET %s %s   %f %f error=%f\n",outs[k]->name(), g->e->name(), v, g->value,g->delta );
	 }

	 for(k=0;k<nt;k++) {
	 g = (PC_GOALSEEK *)ins[k]->dataptr;
	 v = Target_Value(g);
	 printf(" VARIABLE %s %s   %f %f\n", ins[k]->name(), g->e->name(), v, g->value);
	 }
	 } /* end of (if new array ) */


 	 if(!get_ecurr(ecurr,outs,N)) {
		 cout<< " goalseek targets already met\n"<<endl;
		return(1); 
		}

	 do{
	 	 l=0;
		 for(i=0;i<N;i++) {
		 	for(j=0;j<N;j++){
			  	a[j+mca*i] = de[l++];
	 	}	}
		
	 	nr=N; nc = nr; 
		matinvd(&isol,&idsol, nr, nc,a,mca,&det)	;
		if(isol ==1)
			matmult(dv,a,A,N);
		else  {
			cout<< " Inversion failed\n"<<endl;
			/*K--; */
			}

 		/*	cout<< " dv "<<endl; for(i=0;i<N;i++) {
			 for(j=0;j<N;j++){   k=i+N*j; printf(" %f  ",dv[k]); }  cout<< "\n"<<endl;}
			 cout<< " de "<<endl; for(i=0;i<N;i++) {
			 for(j=0;j<N;j++){   k=i+N*j; printf(" %f  ",de[k]); }  cout<< "\n"<<endl;}
			cout<< " de inv"<<endl; 	for(i=0;i<N;i++) {
			 for(j=0;j<N;j++){  k=i+N*j; printf(" %f  ",a[k]); }  cout<< "\n"<<endl; } */
			cout<< " A "<<endl;  	for(i=0;i<N;i++) {
			 for(j=0;j<N;j++){  k=i+N*j; printf(" %f  ",A[k]);  }   cout<< "\n"<<endl;}
 
	 
		 vmult(deltas,A,ecurr,N); 

		 
		 K1 = K+1; if(K1>=N) K1=0;	 /* shimmer, so A doesnt become singular as we get successive*/
	 	 for(k=0;k<N;k++) {		/*  similar experiments */
		  	if(k!=K1){
		  		deltas[k]=deltas[k]/2.0;
		  		printf(" halving  %d \n",k);
		   	}
		   }
		 	
		  Put_v(deltas,ins,N); /* does the capping */
		  
		  memcpy(eold,ecurr,N*sizeof(double));
		  cout<< " solving to give...\n..."<<endl;
	  
/*		  R_Solve();   */
		 for(i=0;i<2000;i++) {
  			 k = sail->R_Solve();   
 		
  			  if(k) 
     				*RE_PANEL_FLAG = 1;
  			  l= Make_All_Psides(sail); /* WHY SLOW? */
			    if(!k) break;
 		 } 
		HC_Update_Display();
		 if(i> 1998) rxerror(" solve may not be converged",2);
		 printf(" Preproc loop in GoalSeek %d\n",i);

	
		 for(k=0;k<nm;k++) {
			 g = (PC_GOALSEEK *)meas[k]->dataptr;
			 v = Target_Value(g);
			g->delta=v- g->value;
			 printf(" MEASURE %s   %f %f error=%f\n", g->e->name(), v, g->value,g->delta );
		 }
		Print_Target_Summary("measure.out",meas,nm) ; 



		  if(!get_ecurr(ecurr,outs,N)) break;
		OKde=1;
 		for(k=0;k<N;k++) {
			 if(fabs(eold[k] - ecurr[k] ) < 0.0001) OKde=0; 
 		   }

		if(OKde) {
		   for(k=0;k<N;k++) {
			   l = N*K + k;
			   de[l] = eold[k] - ecurr[k];  /* movement last time */
			   dv[l] = deltas[k];
			   printf("filling in column K=%d  de= %f  dv= %f\n", K, de[l],dv[l]);
 		   }
		}


		   K++; if(K==N) K=0;
	
 	  }	while (Still_Going--);
	if(Still_Going <=0) rxerror(" Goal seek not converged",2);
return(1);
}
int matmult(double*a1,double*a2,double*res,int N){
/* multiply two packed matrices The packing is the transpose of the standard form
wher first row is (0,0), (0,1)  (0,2).. etc */
   int i,j,k,i1,i2,ir;
   k=0;
   for(i=0;i<N*N;i++) {
   	res[i]=0.0;
	}
   for(i=0;i<N;i++){
	    for(j=0;j<N;j++){
		 for(k=0;k<N;k++){
		 ir = i+N*k;
		 i1 = i+N*j;
		 i2 = j+N*k;
	   	res[ir] =  res[ir] + a1[i1] * a2[i2];
	  }
	  }
   }
 return(1);
 }

int Put_v(double *delta,RXEntity_p *ins, int N){

 /* increment the values in the ins by delta */ 
struct PC_GOALSEEK *g;
  struct LINKLIST *dep;
  struct LINKLIST **adep;
  RXEntity_p nextentity;
int k;

  	 for(k=0;k<N;k++) {
		 g = (PC_GOALSEEK *)ins[k]->dataptr;
		 printf(" proposed movement on %s is %f\n",g->e->name(),delta[k]);
		  if(fabs(delta[k]) > fabs(g->value))  {
					 delta[k] = delta[k] * fabs(g->value)/ fabs(delta[k]);
					 printf(" trimming to %f\n",delta[k]);
					 }
		 if(g->Type==PCT_FLOAT) {
		 		float v;
				 v = *((float*) g->ptr);
				 v = v + (float) delta[k];
				printf("setting %s to %f\n", g->e->name(), v);
				assert(0); memcpy(g->ptr,&v,sizeof(float));
 			 }
		 else if(g->Type==PCT_DOUBLE ) {
		 		double v;
				 v = *((double*) g->ptr);
				 v = v + delta[k];
				printf("setting %s to %f\n", g->e->name(), v);
				assert(0); memcpy(g->ptr,&v,sizeof(double));
 			 }
		else if(g->Type==PCT_OFFSET) {
				double v;
				 RXOffset*o =  (RXOffset*)g->ptr;
				  v = (o->Evaluate(NULL,1));
				  v = v + delta[k];
				/*	 Update_Offset(o,v); */
				  g->e->OutputToClient(" offset targets not implemented",3);
 			 }
		else if(g->Type==PCT_QUANTITY) {
				double v;
				 RXQuantity *q =  (RXQuantity*)g->ptr;
				  v = q->evaluate();
				  v = v + delta[k]; 
				printf("setting %s to %f\n", g->e->name(), v);
				  q->Change(TOSTRING(v));
 			 }
		else  g->e->OutputToClient("setting invalid GOALSEEK TYPE",3);
  		g->e->SetNeedsComputing();

#ifdef USE_NIECES
 	adep = &(g->e->NIECES);
   	dep = *adep;
 	 while(dep) {
		nextentity = (RXEntity_p )dep->data;
		nextentity->SetNeedsComputing();	
		adep = &(dep->next);
	 	dep = *adep;
	  }
#else
                g->e->OnValueChange();
#endif


	 }

	 return 1; /* if finished */
}
double Target_Value(struct PC_GOALSEEK *g) {
double v=0.0;//gcc
   	if(g->Type==PCT_FLOAT ) {
		 v = *((float*) g->ptr);
 			 }
	else if(g->Type==PCT_DOUBLE) {
		 v = *((double*) g->ptr);
 			 }
	else if(g->Type==PCT_OFFSET) {
		 RXOffset*o =  (RXOffset*)g->ptr;
		 v = (o->Evaluate(NULL,1));
 		 }
	else if(g->Type==PCT_QUANTITY) {
		 Quantity_p q =  (Quantity_p)(g->ptr);
		 v = q->evaluate();
 			 }
	else  g->e->OutputToClient("invalid GOALSEEK TYPE",3);

  return(v);
}	 
int get_ecurr(double *ecurr,RXEntity_p *outs, int N){
struct PC_GOALSEEK *g;
int k,iret=0;
double v;

  	 for(k=0;k<N;k++) {
		 g = (PC_GOALSEEK *)outs[k]->dataptr;
		 v  = Target_Value(g);
		 

	 
	 ecurr[k] =	 g->delta =  g->value - v;

	 if(fabs(g->delta) > g->tol) iret=1;
	 printf(" error on %s is %f\n",g->e->name(), ecurr[k]);
	 }
	   if(!iret) cout<< " SMALL ENOUGH\n"<<endl;
	 return iret; /* if finished */
}

 int vmult(double *v,double*A,double*ecurr,int N){
 register int i,j;
 double *vp =v;
  double *ap = A;
  double *ep; 
	 for(i=0;i<N;i++,vp++) {
	 	*vp=0.0;
		ap = &(A[i]);
		ep = ecurr;
		 for(j=0;j<N;  j++, ap+=N, ep++) 
			 (*vp) +=  (*ap) * (*ep) ; 	 
 	 }

 return(1);
 }


