
#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "interpln.h"
#include "resolve.h"
#include "printall.h"
#include "polyline.h"
#include "words.h"
#include "RXAttributes.h"
#include "stringutils.h"
#include "targets.h"

#include "global_declarations.h"
#include "rlx3dmbuffer.h"

#include "nurbs.h"

//  PCN_OneD_NURBS_To_Poly produces a polyline with c = LimitU if KvalU ==1

/* #define NURB_SMALL  (double)0.0000001 */
#define NURB_SMALL  (double)1e-5


/* 21/4/00 a note
Slow Evaluate_xx_Tangent is called when the derivatives cant be found because we are on an edge.

  11 Sept 98.
  1) Loop in Evaluate_Nurbs is from 0 to limitU.
  2)  In fn Bspline there are additional lines to allow extrapolation beyond 0 and  1
  3) In Eva by XY we allow UV outside o and 1

Nov 17 97. First attempt. Seems OK  for linear splines. 
OK for parab and cubic in the middle, but dx/du goes to zero at the edge , as well as upsetting the
nice uniform uv->xy transformation, this stuffs up the differentiation needed to find the normal.
We cannot use the phantom vertex method because we'd miss the edges.
It would  Extrapolate the head and tail vertices away from the
edge linearly. Then we can get dx/du to stay uniform at u=0; 
ensures that we hit the edge vertex exactly 


*/
int NURBDBG=0;

/* DANGER. When we free the nurb control points, be careful not to 
double-free the duplicated rows */

int PCN_Nurbs_Alloc(struct PC_NURB *n) { // requires LU,LV, Ku,Kv to be filled in.
    int i;
    int nu = n->LimitU + 2 * n->KValU + 2;
    int nv =  n->LimitV + 2 * n->KValV + 2;
    n->iP = n->cP = (VECTOR **)CALLOC( (n->LimitU),sizeof(void *));
    n->m_W = (float**)CALLOC(n->LimitU, sizeof(void*));
    for(i=0;i<  n->LimitU;i++) {
        n->iP[i]= n->cP[i]= (VECTOR *)CALLOC(n->LimitV,sizeof(VECTOR));
        n->m_W[i]=(float*)CALLOC(n->LimitV,sizeof(float));
    }
    n->m_u = (double*)MALLOC(nu*sizeof(double)) ;
    n->m_v = (double*)MALLOC(nv*sizeof(double));
    n->m_KnotVectorU= (float*)CALLOC(nu,sizeof(float)) ;
    n->m_KnotVectorV= (float*)CALLOC(nv,sizeof(float));
    return 1;
}


int PCN_Free_Nurb(struct PC_NURB *n){
    /* supposed to do everything to get rid of the PC_NURB*/
    int i, CPToo;
    CPToo = (n->cP != n->iP);

    if(n->iP) {
        for(i=0; i< n->LimitU ; i++) 	{
            if((i != n->LimitU-1) && (n->iP[i] != n->iP[i+1] ))
                RXFREE(n->iP[i]); // leaks the last row
            n->iP[i] = NULL;
        }
        if(n->iP)
            RXFREE(n->iP);
        n->iP=NULL;
    }
    if(CPToo )
        g_World->OutputToClient(" Leak on free Nurbs",2);
    if(n->m_W) {
        for(i=0;i<  n->LimitU;i++) {
            if(n->m_W[i]) RXFREE(n->m_W[i]);
        }
        RXFREE(n->m_W);
    }
    if(n->m_u)				RXFREE(n->m_u);
    if(n->m_v)				RXFREE(n->m_v);
    if(n->m_KnotVectorU)	RXFREE(n->m_KnotVectorU);
    if(n->m_KnotVectorV)	RXFREE(n->m_KnotVectorV);
    RXFREE(n);
    return 0;
}
int mattranspose(double *A,int N){
    int i,j,k=0;
    double*t=(double *)MALLOC(N*N*sizeof(double));
    memcpy(t,A,N*N*sizeof(double));
    for(i=0;i<N;i++) {
        for(j=0;j<N;j++) {
            A[k] = t[j*N+i];
            k++;

        }

    }



    RXFREE(t);
    return 1;
}

int Draw_Nurb_Labels(RXEntity_p e, int nu,int nv){
#ifdef HOOPS
    double u,v,h, du,dv;
    ON_3dPoint r; char buf[64];

    int ku,kv;

    du = 1.0/(double)(nu-1) ;
    dv = 1.0/(double)(nv-1) ;
    u = .0;
    HC_Set_Text_Font("no rotation, no transforms");
    HC_Set_Color("text=black");
    HC_Set_Visibility("text=on");
    HC_Open_Segment(" ues");
    for(ku=0 ;ku< nu ; ku++, u=u+du) {
        v = 0.5;
        h =Evaluate_NURBS(e, u , v,&r);
        sprintf(buf,"u=%4.2f",u);
        HC_Insert_Text(r.x,r.y,(float)h,buf);
    }
    v=0.0;
    for(kv=0 ;kv< nv ;kv++, v=v+dv) {
        u=0.5;
        sprintf(buf,"v=%4.2f",v);
        h =Evaluate_NURBS(e, u , v,&r);
        HC_Insert_Text(r.x,r.y,r.z,buf);
    }

    HC_Close_Segment();

    return 1;
#endif
    return 0;
}
int CreateUByArc(struct PC_NURB *n){/* probably all WRONG */

    VECTOR *p1,*p2;
    int i,j,nu,nv;
    double a;
    g_World->OutputToClient("CreateUByArc WRONG ",3);
    nu = n->LimitU;
    nv = n->LimitV;
    n->m_u[0] =0.0;
    for(i=1;i<nu;i++) {
        a=0.0;
        for(j=0;j<nv;j++) {
            p1 = &( n->cP[i][j]);
            p2 = &( n->cP[i+1][j]);
            a = a + distV(p1,p2);
        }
        n->m_u[i] =n->m_u[i-1]  + a/(double)(nv) ;
    }
    for(i=1;i<nu;i++)
        n->m_u[i] = n->m_u[i] / n->m_u[nu-1];

    n->m_v[0] =0.0;
    for(j=1;j<nv;j++) {
        a=0.0;
        for(i=0;i<nu;i++) {
            p1 = &( n->cP[i][j]);
            p2 = &( n->cP[i][j+1]);
            a = a + distV(p1,p2);
        }
        n->m_v[j] =n->m_v[j-1]  + a/(double)(nu);
    }
    for(j=1;j<nv;j++)
        n->m_v[j] = n->m_v[j] / n->m_v[nv-1] ;

    for(i=0;i<nu;i++)
        printf(" u[%d] is %f\n", i, n->m_u[i] );
    for(j=0;j<nv;j++)
        printf(" v[%d] is %f\n", j, n->m_v[j] );

    return 1;
}
int Set_Mesh_Visibility(HC_KEY key){
    HC_Set_Visibility("faces=off,edges=off,edges=(mesh quads=on)");
    /*	HC_Open_Geometry (key);
 char s[512];
 int r,c;
 HC_Show_Mesh_Size (key, &r, &c);
 for(i=0;i<r*(c-1);i++) {
   o1 = i;
  o2 = o1+ r+1; {
    HC_Open_Edge(o1,o2);
     HC_Show_One_Net_Visibility("edges",s);
     printf(" r=%d  o1=%d o2=%d '%s'\n", i,o1,o2,s);
    HC_Close_Edge();
  }
 }

 HC_Close_Geometry(); */
    return 1;
}
int Draw_Control_Graph_Mesh(struct PC_NURB *n){
#ifdef HOOPS
    int i,j,nu,nv, k=0;
    VECTOR *pts;
    HC_KEY meshkey;

    nu = n->LimitU;
    nv = n->LimitV;
    if(!nu ||  !nv)
        return 0;
    pts = (VECTOR *)MALLOC(((nu+1)*(nv+1)+2) *sizeof(VECTOR));
    HC_Open_Segment("GAUSS_MOULD");

    for(i=0 ;i< nu ; i++) {
        for(j=0 ;j< nv ;j++) {
            memcpy(&(pts[k]), &(n->iP[i][j]), sizeof(VECTOR));
            k++;
        }
    }

    HC_Set_Color("edges=periwinkle");
    meshkey = HC_KInsert_Mesh(nu,nv,pts);
    Set_Mesh_Visibility(meshkey);

    HC_Close_Segment();
    RXFREE(pts);
    return 1;
#else
    return 0;
#endif

}
int Draw_Control_Graph(struct PC_NURB *n){

    VECTOR *p1,*p2;
    int i,j,nu,nv;
    nu = n->LimitU;
    nv = n->LimitV;
    if(!nu ||  !nv)
        return 0;
    HC_Open_Segment("Control Graph");
    HC_Set_Color("lines=red");
    for(i=0;i<nu;i++) {
        for(j=0;j<nv-1;j++) {
            p1 = &( n->iP[i][j]);
            p2 = &( n->iP[i][j+1]);
            RX_Insert_Line(p1->x, p1->y,p1->z,p2->x,p2->y,p2->z,g_World->GNode());
        }
    }

    for(j=0;j<nv;j++) {
        for(i=0;i<nu-1;i++) {
            p1 = &( n->iP[i][j]);
            p2 = &( n->iP[i+1][j]);
            RX_Insert_Line(p1->x, p1->y,p1->z,p2->x,p2->y,p2->z,g_World->GNode());
        }
    }
    HC_Close_Segment();
    return 1;
}

int Draw_Surface_Trigraphs(RXEntity_p e){

#ifndef HOOPS

    return 0;
#else
    double u,v,dd;
    float F = (float) 0.2;
    ON_3dPoint r; ON_3dVector du,dv,nor;
    int i,j,kret, nn=13;

    HC_Open_Segment("trigraphs");
    HC_Set_Line_Weight(2.0);
    HC_Open_Segment("x");
    HC_Set_Color("green");
    HC_Close_Segment();
    HC_Open_Segment("y");
    HC_Set_Color("red");
    HC_Close_Segment();
    HC_Open_Segment("z");
    HC_Set_Color("black");
    HC_Close_Segment();

    u=0.0; dd = 1.0/(double)(nn-1-1*2*0); u = - dd*0;
    F = (float) (2.0*dd);
    HC_Set_Marker_Symbol("(+)");
    for(i=0; i<nn; i++ ,u=u+dd) {
        v=0.0 - dd*0;
        for(j=0; j<nn; j++, v=v+dd) {
            r.x=r.y=r.z=0.0;
            du.x=du.y=du.z=0.0;
            dv.x=dv.y=dv.z=0.0;
            nor.x=nor.y=nor.z=0.0;
            //Evaluate_NURBS(e,  u,  v,&r);
            kret = PCN_Evaluate_NURBS_Tangents(e,u, v,&r,&du, &dv);
            /* 	if(3 != kret && 24 !=kret) {
    printf("Ev tangents failed (%d) at %f %f \n",k, u,v);
    } */

            if(!du.Unitize () ) printf("at %f %f NO du (%f %f %f)..  ",u,v, du.x,du.y,du.z);
            else
                HC_QInsert_Line("x", r.x,r.y,r.z,   r.x+F*du.x,    r.y+F*du.y,    r.z+F*du.z);
            if(!dv.Unitize ()) printf("at %f %f NO dv (%f %f %f)..   ..",u,v, dv.x,dv.y,dv.z);
            else
                HC_QInsert_Line("y", r.x,r.y,r.z,   r.x+F*dv.x,    r.y+F*dv.y,    r.z+F*dv.z);

            nor=ON_CrossProduct(dv,du);
            if(!nor.Unitize ()) {
                printf("NO Normal(%f %f %f)\n", nor.x,nor.y,nor.z);
                g_World->OutputToClient("Ev tangents failed ",2);
            }
            else
                HC_QInsert_Line("z", r.x,r.y,r.z,   r.x+F*nor.x,   r.y+F*nor.y,   r.z+F*nor.z);
        }
    }
    HC_Close_Segment();
    return 1;
#endif
}

int PCN_Create_One_Knot_Vector(struct PC_NURB *n, int Lim,int  head,int tail,int order,float*KV) {
    int  k, i=0;
    // this version (3/2/01) requires Knot to offset the index by (order)
    float dk;
    assert(head==0); assert(tail==0);
    if(0) { //IGES style
        n->KoffU = n->KValU;
        n->KoffV= n->KValV;
        for(k=0;k<order;k++)
            KV[i++] = 0.0;

        dk = (float) 1.0/(float) ( Lim-order); //not  sure about the (2)
        for(k=0; k <= Lim-order;k++,i++) {
            KV[i] = (float) k  *  dk;
        }

        i--;
        for(;i<Lim;i++)  // the Knot Vectors now calloced to limit{U,V}
            KV[i] =  1.0;
        return 1;
    }
    // normal style
    n->KoffU = 0;
    n->KoffV=  0 ;
    for(k=0;k<order;k++)
        KV[i++] = 0.0;

    dk = (float) (1.0/(double) ( Lim-order));
    for(k=1; k <= Lim-order;k++,i++) {
        KV[i] = (float) k  *  dk;
    }

    i--;
    for(;i<Lim;i++)
        KV[i] =  (float) 1.0;
    return 1;


}

/* int PCN_Create_One_Knot_Vector_Safe(int Lim,int  head,int tail,int order,double*KV) {
int rc, k, i=0;	

double kv=0.0;
 rc = Lim  - head -tail- 1;
 for(k=0;k<=head;k++)
  KV[i++] = 0.0;
//	for(k=1;k<=rc;k++) 

 for(k=1; kv<1.0 ;k++,i++) {
  kv  = ((double) k - 1.0*(double)(order-1) / 2.0) / (double) rc;

  KV[i] =kv;

 }

 i--;
 for(k=0;k<=(2*tail+2) && i<NURBMAX;k++)
  KV[i++] =  1.0;

return 1;
}

*/

int PCN_Create_Uniform_Knot_Vectors(struct PC_NURB *n){
    PCN_Create_One_Knot_Vector(n,n->LimitU ,n->head,n->tail,n->KValU, n->m_KnotVectorU);
    PCN_Create_One_Knot_Vector(n,n->LimitV ,n->head,n->tail,n->KValV,  n->m_KnotVectorV);
    n->Knot_Type =NUNIFORM;
    return 1;
}

int Remove_Duplicate_Nurbs_Edges(struct PC_NURB *n){
    int i,j,i1,j1, ci=0,cj=0;
    VECTOR *p1, *p2;
    /*
 If two rows or columns are the same copy down the rows above
 */
    //printf(" Remove_Duplicate_Nurbs_Edges with n->LimitU=%d  n->LimitV=%d\n",n->LimitU,n->LimitV);
    i = n->LimitU; if(n->LimitV>i) i = n->LimitV;
    p1 = (VECTOR *)MALLOC(i*sizeof(VECTOR));
    p2 = (VECTOR *)MALLOC(i*sizeof(VECTOR));

    for(j=0;j<n->LimitV-1;j++) {
        for(i=0;i<n->LimitU;i++) {
            p1[i].x = n->iP[i][j].x ;
            p1[i].y = n->iP[i][j].y ;
            p1[i].z = n->iP[i][j].z;
            p2[i].x = n->iP[i][j+1].x ;
            p2[i].y = n->iP[i][j+1].y ;
            p2[i].z = n->iP[i][j+1].z;
        }
        if(0 == PC_Polyline_Compare(p1,n->LimitU,p2,n->LimitU,n->tol)){
            // j and j+1 are the same  printf("remove j = %d\n", j);
            for(j1=j;j1<n->LimitV-1;j1++) {
                for(i=0;i<n->LimitU;i++) {
                    n->iP[i][j1].x = n->iP[i][j1+1].x ;
                    n->iP[i][j1].y = n->iP[i][j1+1].y ;
                    n->iP[i][j1].z = n->iP[i][j1+1].z;
                }
            }
            n->LimitV--; j--; cj++;
        }
    }
    for(i=0;i<n->LimitU-1;i++) {
        for(j=0;j<n->LimitV;j++) {
            p1[j].x = n->iP[i][j].x ;
            p1[j].y = n->iP[i][j].y ;
            p1[j].z = n->iP[i][j].z;
            p2[j].x = n->iP[i+1][j].x ;
            p2[j].y = n->iP[i+1][j].y ;
            p2[j].z = n->iP[i+1][j].z;
        }
        if(0 == PC_Polyline_Compare(p1,n->LimitV,p2,n->LimitV,n->tol)){
            // i and i+1 are the same  printf("remove i = %d\n", i);
            for(i1=i;i1<n->LimitU-1;i1++) {
                for(j=0;j<n->LimitV;j++) {
                    n->iP[i1][j].x = n->iP[i1+1][j].x ;
                    n->iP[i1][j].y = n->iP[i1+1][j].y ;
                    n->iP[i1][j].z = n->iP[i1+1][j].z;
                }
            }
            n->LimitU--; i--; ci++;
        }
    }

    if(NURBDBG && (ci||cj) ) printf(" removed %d in U and %d in V\n", ci,cj);
    RXFREE(p1); RXFREE(p2);

    return (ci+cj);
}
int PCN_Scan_Nurbs(RXEntity_p e) {

    ON_3dPoint r,dt; ON_3dVector  tu,tv,xp,t;
    double zz,lt,dot,a;
    double u,v;
    float step= (float) 0.1;
    cout<< "u\tv\t r.x\t r.y\tr.z\t tu.x\t tu.y\t tu.z\t tv.x\t tv.y\t tv.z"<<endl;
    for(u=0;u<=1.01;u+=0.125) {
        for(v=0;v<=1.01;v+=0.125) {
            printf("%f\t%f\t",u,v);
            zz = Evaluate_NURBS(e,u,v,&dt);
            PCN_Evaluate_NURBS_Tangents(e, u, v, &r,&tu, &tv);
            // dt and R THE SAME??
            assert(dt.x==r.x);
            assert(dt.y==r.y);
            printf("%f\t%f\t%f\t",r.x,r.y,r.z);
            printf("%f\t%f\t%f\t",tu.x,tu.y,tu.z);
            printf("%f\t%f\t%f\n",tv.x,tv.y,tv.z);
        }
    }
    cout<< "vvUvv TV Length g_World->OutputToClient %% VV>>>\n \t ";
    for(u=0;u<=1.01;u+=step)
        printf("%f\t",u); cout<<endl;
    for(u=0;u<=1.01;u+=step) {
        printf("%f\t",u);
        xp.x=xp.y=xp.z=0;
        for(v=0;v<=1.01;v+=step) {
            //
            PCN_Evaluate_NURBS_Tangents(e, u, v,&r, &tu, &tv);
            zz = Evaluate_NURBS(e,u,v,&r);
            t.x=(r.x-xp.x)/step; t.y=(r.y-xp.y)/step; t.z=(r.z-xp.z)/step; // cf tv;
            dt = t-tv;
            zz=sqrt(t.x*t.x + t.y*t.y + t.z*t.z);
            printf("%f\t",sqrt(tv.x*tv.x + tv.y*tv.y + tv.z*tv.z)/zz*100);
            xp=r;


        } cout<<endl;
    }
    cout<< "vvUvv TV angle g_World->OutputToClient  VV>>>\n \t ";
    for(u=0;u<=1.01;u+=step)
        printf("%f\t",u); cout<<endl;
    for(u=0;u<=1.01;u+=step) {
        printf("%f\t",u);
        xp.x=xp.y=xp.z=0;
        for(v=0;v<=1.01;v+=step) {
            //
            PCN_Evaluate_NURBS_Tangents(e, u, v,&r, &tu, &tv);
            zz = Evaluate_NURBS(e,u,v,&r);
            t.x=(r.x-xp.x)/step; t.y=(r.y-xp.y)/step; t.z=(r.z-xp.z)/step; // cf tv;

            zz=sqrt(t.x*t.x + t.y*t.y + t.z*t.z);
            lt = sqrt(tv.x*tv.x + tv.y*tv.y + tv.z*tv.z);
            dot = (t.x*tv.x + t.y*tv.y + t.z*tv.z);
            a = acos(dot/zz/lt) *57.273;
            printf("%f\t",a);

            xp.x=r.x;xp.y=r.y;xp.z=r.z;
        } cout<<endl;
    }




    cout<< "vvVvv TU Length g_World->OutputToClient %%  UU>>>\n \t ";
    for(u=0;u<=1.01;u+=step)
        printf("%f\t",u); cout<<endl;
    for(v=0;v<=1.01;v+=step) {
        printf("%f\t",v);
        xp.x=xp.y=xp.z=0;
        for(u=0;u<=1.01;u+=step) {
            //zz = Evaluate_NURBS(e,u,v,&r);
            PCN_Evaluate_NURBS_Tangents(e, u, v,&r, &tu, &tv);
            t.x=(r.x-xp.x)/step; t.y=(r.y-xp.y)/step; t.z=(r.z-xp.z)/step; // cf tv;
            zz=sqrt(t.x*t.x + t.y*t.y + t.z*t.z);
            printf("%f\t",sqrt(tu.x*tu.x + tu.y*tu.y + tu.z*tu.z)/zz*100);
            memcpy(&xp,&r,sizeof(VECTOR));
        } cout<<endl;
    }

    cout<< "vvVvv TU ANGLE  UU>>>\n \t ";
    for(u=0;u<=1.01;u+=step)
        printf("%f\t",u); cout<<endl;
    for(v=0;v<=1.01;v+=step) {
        printf("%f\t",v);
        xp.x=xp.y=xp.z=0;
        for(u=0;u<=1.01;u+=step) {
            //zz = Evaluate_NURBS(e,u,v,&r);
            PCN_Evaluate_NURBS_Tangents(e, u, v,&r, &tu, &tv);
            t.x=(r.x-xp.x)/step; t.y=(r.y-xp.y)/step; t.z=(r.z-xp.z)/step; // cf tv;

            zz=sqrt(t.x*t.x + t.y*t.y + t.z*t.z);
            lt = sqrt(tu.x*tu.x + tu.y*tu.y + tu.z*tu.z);
            dot = (t.x*tu.x + t.y*tu.y + t.z*tu.z);
            a = acos(dot/zz/lt) *57.273;
            printf("%f\t",a);
            memcpy(&xp,&r,sizeof(VECTOR));
        } cout<< ""<<endl;
    }

    cout<< "vvVvv IRET  UU>>>\n \t ";
    for(u=0;u<=1.01;u+=step)
        printf("%f\t",u); cout<<endl;
    for(v=0;v<=1.01;v+=step) {
        printf("%f\t",v);
        xp.x=xp.y=xp.z=0;
        for(u=0;u<=1.01;u+=step) {

            printf("%d\t", PCN_Evaluate_NURBS_Tangents(e, u, v,&r, &tu, &tv) );
        } cout<<endl;
    }



    return 1;
}
int Compute_NURBS(RXEntity_p e) { /* prepares a NURBS by reading the line */
    /* First parse the line into a square array of vectors.
 Then prepare the spline by the IGES method. */
    struct PC_INTERPOLATION *p;
    struct PC_NURB *nurb ;
    int i,j,c;
    char **w;
    char **words=NULL;

    p =  (struct PC_INTERPOLATION*) e->dataptr;
    assert(p);

    if(p->NurbData) {
        nurb=p->NurbData;
        if((nurb->flags & PCN_IGES)||(nurb->m_pONobject)) {
            e->SetNeedsComputing(0);
            return 1;
        }
        PCN_Free_Nurb(p->NurbData);
    }

    p->NurbData = (PC_NURB *)CALLOC(1, sizeof(struct PC_NURB));
    char *ll;
    char *l_line = STRDUP(e->GetLine());
    nurb= p->NurbData;

    e->AttributeGetInt("debug",&NURBDBG);
    e->AttributeGetInt("nu",&(nurb->LimitU)) ;
    e->AttributeGetInt( "nv",&(nurb->LimitV)) ;
    if(!e->AttributeGetInt( "ku",&(nurb->KValU)))		nurb->KValU=3;
    if(!e->AttributeGetInt( "kv",&(nurb->KValV)))		nurb->KValV=3;
    if(!e->AttributeGetInt( "head",&(nurb->head  )))	nurb->head= 0 * nurb->KValU;
    if(!e->AttributeGetInt("tail",&(nurb->tail  )))	nurb->tail =0 * nurb->KValU;
    if(!e->AttributeGetDble( "tolerance",&(nurb->tol)))	nurb->tol=0.001;
    if(nurb->tol< (100.0*NURB_SMALL) ) nurb->tol = 100.0*NURB_SMALL;
    nurb->Interpolatory = e->AttributeGet("Interpol")  ;

    if(nurb->LimitU < (1+nurb->KValU) || nurb->LimitV <(1+ nurb->KValV)) {
        g_World->OutputToClient("insufficient rows or cols in NURBS",2);
        RXFREE(l_line);
        return 0;
    }
    if(!PCN_Nurbs_Alloc(nurb)) return 0;

    ll = strstr(l_line,"table");
    if(ll)
        ll +=6;

    c = make_into_words(ll,&words," \t\n");
    i = nurb->LimitU*nurb->LimitV*3;
    if(c < i) {
        char buf[256];
        sprintf(buf,"in table '%s' there are %d words\n but %d are needed\n because %d rows and %d cols are defined ",e->name(),c, i, nurb->LimitU,nurb->LimitV);
        RXFREE(words); RXFREE(l_line);RXFREE(p->NurbData); p->NurbData=NULL;
        g_World->OutputToClient(buf,2); return 0;
    }
    w=words;

    for(i=0;i<nurb->LimitU;i++) {
        for(j=0;j<nurb->LimitV;j++,w++) {
            nurb->iP[i][j].x = (float) atof(*(w++));
            nurb->iP[i][j].y = (float) atof(*(w++));
            nurb->iP[i][j].z = (float) atof(*w);
        }
    }
    //PrintNurbTable(nurb,stdout);

    if(Remove_Duplicate_Nurbs_Edges(nurb)) {
        //PrintNurbTable(nurb,stdout);
    }

    PCN_Set_Weights(nurb);

    if(nurb->head) i = AddHeadRows(nurb ,nurb->head);
    if(i && nurb->tail) i = AddTailRows(nurb, nurb->tail);
    if(!i) g_World->OutputToClient("insufficient rows or cols in NURBS",2);

    if(Set_Knot_Vectors(e, nurb) ==UNKNOWN) g_World->OutputToClient(" no knot vectors",3);

    RXFREE(words); RXFREE(l_line);
    e->SetNeedsComputing(0);
    return 1;
}
int PCN_Print_Nurb_Table(struct PC_NURB *nurb,FILE*fp){
    int i,j;

    fprintf(fp," \n Nu=%d Nv=%d\n", nurb->LimitU, nurb->LimitV);
    fprintf(fp," ku=%d kv=%d\n", nurb->KValU, nurb->KValV);
    if(!nurb->iP) {  fprintf(fp, " ip NULL\n"); return 1;}
    fprintf(fp, " cp array \n");
    for(i=0;i<nurb->LimitU;i++) {
        fprintf(fp,"i=%2.2d",i);
        for(j=0;j<nurb->LimitV;j++) {
            fprintf(fp,"\t(%5.2f", 	nurb->cP[i][j].x );
            fprintf(fp," %5.2f", 	nurb->cP[i][j].y );
            fprintf(fp," %5.3f)", 	nurb->cP[i][j].z );
        }
        fprintf(fp,"\n");
    }

    if(nurb->cP!= nurb->iP) {
        fprintf(fp, " ip \n");
        for(i=0;i<nurb->LimitU;i++) {
            fprintf(fp,"i=%2.0d",i);
            for(j=0;j<nurb->LimitV;j++) {
                fprintf(fp," (%5.1f",   nurb->iP[i][j].x );
                fprintf(fp," %5.1f", 	nurb->iP[i][j].y );
                fprintf(fp," %5.2f)", 	nurb->iP[i][j].z );
            }
            fprintf(fp,"\n");
        }
    }
    return 1;

}
int  Set_Knot_Vectors(RXEntity_p e, struct PC_NURB *n) {
    int i;
    char buf[64];
    n->Knot_Type =UNKNOWN;
    assert(e);
 //   RXAttributes rxa(e->Attributes());
    if(n->LimitU !=n->LimitV)
        if(e) e->OutputToClient(" this kind of NURBS needs to be square",1);
        else  g_World->OutputToClient(" this kind of NURBS needs to be square",1);
    n->LimitU = max(n->LimitV,n->LimitU) ; n->LimitV=n->LimitU;
//    for(i=0;i<n->LimitU;i++) {
//        sprintf(buf,"k%2.2d",i);
//        if(!rxa.Extract_Float(buf,&(n->m_KnotVectorU[i]))) break;
//        printf(" FROMATTS %d %s %f\n", i, buf, n->m_KnotVectorU[i]);
//        n->m_KnotVectorV[i] = n->m_KnotVectorU[i];
//        n->Knot_Type =FROMATTS;
//    }
    if(e->AttributeGet("arclength") ) {
        CreateUByArc(n);
        n->Knot_Type =ARCLENGTH;
        return ARCLENGTH;
    }
    if(n->Knot_Type==UNKNOWN ) PCN_Create_Uniform_Knot_Vectors(n);
    //	if(n->Knot_Type==UNKNOWN ) n->Knot_Type=ALGORITHM ;
    //printf(" Knot type %d\n" , n->Knot_Type);
    return n->Knot_Type;
}

int PCN_Set_Weights(struct PC_NURB *n) {  // non-unity weights stuff up the derivatives
    int i,j;
    //float f;

    //	for (i=0;i<NURBMAX;i++) { n->Wu[i]= n->Wv[i]=  1.0;  }
    for(i=0;i<  n->LimitU;i++) {
        for(j=0;j<n->LimitV;j++)
            n->m_W[i][j]=1.0;
    }
    return 2;

    /*	if(!(n->flags & PCN_IGES) && !n->head && !n->tail){
  f = (float) (1.0/(2*(1 + n->head)));
  for(i=0;i<=n->head;i++) {
   n->Wu[i]= n->Wv[i]= f;
  }
  f = (float) (1.0/(2*(1 + n->tail)));
  for(i=n->LimitU - n->tail-1;i<NURBMAX;i++)
   n->Wu[i]= f;
  for(i=n->LimitV - n->tail-1;i<NURBMAX;i++)
   n->Wv[i]= f;
 }
 return 1;*/
}
int  AddHeadRows(struct PC_NURB *nurb,int howmany){
    /* so far, LimitU, nurb->LimitV are the no of rows and cols in the  cp list
We multiply the points at the edge, so that there are (k) copies. 
 void *memmove (Target, Source, N)
 void *memcpy (Target, Source, N)
*/
    int i,k, ku,kv,nu,nv,flag;
    if(nurb->LimitU < 1) return 0;
    if(nurb->LimitV < 1) return 0;
    nu = nurb->LimitU + howmany;
    nv = nurb->LimitV + howmany;
    if(NURBDBG) PCN_Print_Nurb_Table(nurb,stdout);
    nurb->iP = nurb->cP =  (VECTOR **)REALLOC(nurb->iP, (nu+2)* sizeof(void *));
    ku =howmany;
    kv =howmany;

    /* extend each row at front */
    flag=    -1;
    if(kv>0) {
        printf(" Adding %d cols at head \n", kv);
        for(i= 0 ; i< nurb->LimitU ; i++) {
            /* old 	nurb->ip[i] =  REALLOC(nurb->ip[i], (nv+2) * sizeof(VECTOR)); */
            /* The switch is to avoid allocing things that werent themselves allocated */
            switch ( flag) {
            case -1:  {/* starting */
                if( i != nurb->LimitU-1 && (nurb->iP[i] ==nurb->iP[i+1]))  break;
                nurb->iP[i] =  (VECTOR *)REALLOC(nurb->iP[i], (nv+2) * sizeof(VECTOR));
                flag=0;
                break;
            }
            case 0 : {
                if( i  && (nurb->iP[i] ==nurb->iP[i-1])) {  flag=1; continue;}
                nurb->iP[i] =  (VECTOR *)REALLOC(nurb->iP[i], (nv+2) * sizeof(VECTOR));
                break;
            }
            case 1:  {

            }
            }



            memmove(&( nurb->iP[i] [kv]), &(nurb->iP[i][0]), (nurb->LimitV)*sizeof(VECTOR));
            for(k=0;k<kv;k++)
                memcpy(&(nurb->iP[i][k]) , &( nurb->iP[i][kv]),sizeof(VECTOR));
        }
        nurb->LimitV+=(kv);
    }

    if(ku>0) {
        printf(" Adding %d rows at head\n", ku);
        /* Duplicate the rows by pointers not copy*/
        memmove( &(nurb->iP [ku]) , nurb->iP, (nurb->LimitU) * sizeof(void *));
        for (k=0;k<ku;k++)  nurb->iP [k] = nurb->iP [ku];
        nurb->LimitU+=(ku);
    }
    //printf(" %d head rows added\n", howmany);
    if(NURBDBG) PCN_Print_Nurb_Table(nurb,stdout);
    return 1;

}


int  AddTailRows(struct PC_NURB *nurb,int howmany){
    /* so far, LimitU, nurb->LimitV are the no of rows and cols in the  ip list
We multiply the points at the edge, so that there are (k) copies. 
 void *memmove (Target, Source, N)
 void *memcpy (Target, Source, N)
*/
    int i,k, ku,kv,nu,nv;
    int flag , ACopy, lastgood=   -1;
    nu = nurb->LimitU + howmany;
    nv = nurb->LimitV + howmany;

    if(!howmany) return 0;

    nurb->cP = nurb->iP =(VECTOR **)REALLOC(nurb->iP, (nu+2)* sizeof(void *));
    ku = kv = howmany;
    if(!kv) return 0;
    printf(" Adding %d cols at tail \n", kv);

    flag = -1;
    for(i= 0 ; i< nurb->LimitU ; i++) {   ACopy=0;
        /* The switch is to avoid allocing things that werent themselves allocated */
        switch ( flag) {
        case -1:  {/* starting */
            if( i != nurb->LimitU-1 && (nurb->iP[i] ==nurb->iP[i+1])) {  ACopy=1; break; }
            nurb->iP[i] =  (VECTOR *)REALLOC(nurb->iP[i], (nv+2) * sizeof(VECTOR));
            for(k=0;k<i;k++) nurb->iP[k] = nurb->iP[i] ;
            lastgood=i;
            flag=0;
            break;
        }
        case 0 : {
            if( i  && (nurb->iP[i] ==nurb->iP[i-1])) {  flag=1; continue;}
            nurb->iP[i] =  (VECTOR *)REALLOC(nurb->iP[i], (nv+2) * sizeof(VECTOR));
            lastgood=i;
            break;
        }
        case 1:  {
            if(lastgood >=0) nurb->iP[i] = nurb->iP[lastgood ];
            ACopy=1;
        }
        }
        if(! ACopy) {assert(0);
            for(k=nurb->LimitV; k< nurb->LimitV+kv; k++)
                memcpy(&(nurb->iP[i][k]), &(nurb->iP[i][nurb->LimitV-1]),sizeof(VECTOR));
        }

    }
    nurb->LimitV+=(kv);

    /* Duplicate the rows by pointers not copy */
    for (k=nurb->LimitU ;k<nurb->LimitU+ ku; k++)  nurb->iP[k] = nurb->iP[nurb->LimitU-1];

    nurb->LimitU+=(ku);
    //printf(" %d tail  rows added\n", howmany);
    if(NURBDBG) PCN_Print_Nurb_Table(nurb,stdout);
    return 1;
}

int PCN_Print_Nurb_Knots ( struct PC_NURB *n,FILE *fp) {
    int i,k;
    Set_Nurb_Globals(n, n->KValU ,3, n->LimitU,NURB_U) ;
    fprintf(fp,"    Nu=%d    Nv=%d\n", n->LimitU, n->LimitV);
    fprintf(fp,"    ku=%d    kv=%d\n", n->KValU, n->KValV);
    fprintf(fp,"  head=%d  tail=%d\n", n->head, n->tail);
    fprintf(fp," KoffU=%d KoffV=%d\n", n->KoffU, n->KoffV);

    switch (n->Knot_Type )  {
    case UNKNOWN:
        fprintf(fp," Knot type unknown \n");
        break;
    case FROMFILE:
        fprintf(fp," Knots taken from file \n");
        break;
    case NUNIFORM:
        fprintf(fp," Knots generated uniform \n");
        break;
    case ARCLENGTH:
        fprintf(fp," Knots from arclength \n");
        break;
    case ALGORITHM:
        fprintf(fp," Knots by algorithm \n");
        break;
    case PCN_IGES:
        fprintf(fp," Knots from IGES\n");
        break;
    case SSD:
        fprintf(fp," Knots by SSD method \n");
        break;
    default:
        fprintf(fp," Knot type  %d not recognised\n",n->Knot_Type);
    }

    i =  n-> LimitU + 3* n->KValU + n->head + n->tail;
    fprintf(fp, "  i \t U Knot\n");
    for(k=0;(k<i );k++) fprintf(fp," %d \t%f \n", k,Knot(n,k));

    Set_Nurb_Globals(n, n->KValV ,3, n-> LimitV,NURB_V) ;
    i =  n-> LimitV + 3*n->KValV + n->head + n->tail;
    fprintf(fp, "  i \t V Knot\n");
    for(k=0;(k<i);k++) fprintf(fp," %d \t%f \n", k,Knot(n,k));
    return 1;

}

int Print_Basis_Functions( RXEntity_p e,double u) {
    double b0, d;
    int i;
    struct PC_INTERPOLATION*p = (PC_INTERPOLATION*)e->dataptr;
    struct PC_NURB *n = p->NurbData ;

    d=0.0;
    printf(" basis functions (weighted) for u=%f\n", u);
    for(i=0;i<n->LimitU; i++)  {
        b0= PCN_Calc_Bval_nw(n, n->KValU, i, n-> LimitU ,u,NURB_U);
        printf(" %d  %f\n", i, b0);
        d += b0;
    }
    printf(" divide the above by %f\n", d);
    return 1;

} 
int Set_Nurb_Globals(struct PC_NURB *nu, int k,int i,int Limituv,int uv) {

    nu->knotK = k; /* only for algorithmic Knot values */
    nu->knotN = Limituv - nu->tail -1;
    nu->Limit = Limituv;
    nu->uv = uv;
    return 1;
}

double  PCN_Calc_Bval_nw(struct PC_NURB *nu, int k,int i,int Limituv,double u,int uv) {
    /* K is order
i is which spline */

    assert(k <= Limituv+1);

    Set_Nurb_Globals(nu, k,i, Limituv,uv) ;
#ifndef _DEBUG
    if(u<1.0) {
        if(u > Knot(nu,i +k + nu->KValU)) return 0.0;
        if(u < Knot(nu,i - k -nu->KValU)) return 0.0;
    }
#else
    //	printf("\nU\t%f\tuv\t%d\tKnotK\t%d\tI\t%d\tLimit\t%d\tknotN\t%d\t",u,uv,k,i,Limituv,nu->knotN);
#endif
    switch (uv){
    case NURB_U:
        return(Bspline(nu, i,k,u,i) );// *  nu->Wu[i] );

    case NURB_V:
        return(Bspline(nu, i,k,u,i));// *  nu->Wv[i] );
    }
    assert(0);
    return 0.0;


}
double  Calc_Bval_1(struct PC_NURB *nu, int k,int i,int Limituv,double u,int uv) {
    /* K is order
i is which spline */


    Set_Nurb_Globals(nu, k,i, Limituv,uv) ;
    // 	if(u<1.0) {  /* maybe WRONG 10/4/00 Peter removed the commenting here. */
    //		if(u > Knot(nu,i +k + 1)) return 0.0;
    //		if(u < Knot(nu,i - k -1)) return 0.0;
    //	}
    return(DBspline(nu, i,k,u,i,k));
}

double  Calc_Bval_2(struct PC_NURB *nu, int k,int i,int Limituv,double u,int uv) {
    /* K is order
i is which spline */


    Set_Nurb_Globals(nu, k,i, Limituv,uv) ;
    /* 	if(u<1.0) {
  if(u > Knot(nu,i +k + 1)) return 0.0;
  if(u < Knot(nu,i - k -1)) return 0.0;
 } */
    return(DDBspline(nu, i,k,u,i));
}



float Knot(struct PC_NURB *n,int i) {// index offset by K to match IGES vector style
    float KnotVal = 0.0; //gcc
    int Index;
    Index=i;
    if(n->Knot_Type != ALGORITHM) {

        if(i<0)  i=0;
        if(n->uv==NURB_U){
            Index =i+n->KoffU;
            if(Index<0) Index=0;
            KnotVal=  n->m_KnotVectorU[Index];
            return(KnotVal);
        }
        else if(n->uv==NURB_V) {
            Index =i+n->KoffV;
            if(Index<0) Index=0;
            KnotVal=  n->m_KnotVectorV[Index];
            return(KnotVal);
        }
    }
    else {
        if(i < n->knotK) /* this assumes head=tail=k */
            return(0.0); // KnotVal=0.0;
        else if(i > n->knotN+ n->knotK-2)
            return(1.0);// KnotVal = 1.0;
        else
            KnotVal = (float) (( i - n->knotK +1)/ ( n->knotN - 1));
    }

    return(KnotVal );
}



double DBspline(struct PC_NURB *nu, int i,int k, double u,int im,int k0) {
    double d,  t = 0.0;
    d = Knot(nu, i+k-1) - Knot(nu, i-1);
    //was   im-1 guess 31/1/01
    if(d !=0) t =  Bspline(nu, i, k-1,u,im) /d;

    d = Knot(nu, i+k) - Knot(nu, i);
    if(d !=0) t =  t - Bspline(nu,i+1 ,k-1,u,im ) /d;

    return(t*(double)k);
}


double DDBspline(struct PC_NURB *nu, int i,int k, double u,int im ) { /* WRONG. */
    double d,  t = 0.0;
    d = Knot(nu, i+k-1) - Knot(nu, i-1);
    if(d !=0) t =  DBspline(nu, i, k-1,u,im-1,k-1) /d;

    d = Knot(nu, i+k) - Knot(nu,i);
    if(d !=0) t =  t + DBspline(nu,i+1 ,k-1,u,im,k-1) /d;
    return(t*(double)k);
}

double Bspline(struct PC_NURB *n, int i,int k, double u,int im) { /* from farin */
    double t,v,kh,kl;
    float kn1,kn,fu;

    if(k==0){ /* farin p154 */
        fu = (float) u;
        kn1 =Knot(n,i-1) ;
        kn = Knot(n,i);
        if(( kn1 <=fu) && (fu < kn) )
            return 1.0 ;
        if(u > 1. - NURB_SMALL) {// slightly more generous test
            if((kn1 <=fu) && (fu <= kn+NURB_SMALL) ) // NURB_SMALL added sept 02 because of change to float Knots
                return 1.0 ;
        }
        /* Peter addition */
        if(u== 0.0  && im ==n->head  && n->knotK == i  ){ // WHY use the HEAD???
            return 1.0;
        }
        return 0.0;
    }

    v=0.0;
    kh = (double) Knot(n, i+k-1);
    kl = (double) Knot(n,i-1);
    t= kh - kl;
    if( t > NURB_SMALL) {
        v = (u- kl ) * Bspline(n,i,(k-1),u,im)/t;
    }
    kh = Knot(n,i+k);
    kl = Knot(n,i);
    t = kh - kl ;
    if( t > NURB_SMALL)
        v = v + (kh - u) * Bspline(n,i+1,(k-1),u,im)/t;

    return(v);
}
double Evaluate_NURBS(RXEntity_p e, double u, double v,ON_3dPoint *r){
    /* returns vector at (u,v); */

    double h;
    struct PC_INTERPOLATION*p = (PC_INTERPOLATION*)e->dataptr;
    struct PC_NURB *n ;

    assert( p)  ;
    n = p->NurbData ;
    if(!n){ cout<< "mould is Not a NURBS"<<endl;  return 0;}

    //Thomas 27 11 03
    if (n->m_pONobject){
#ifndef _NO_ON
        h =  EvaluateONNurbsSurface(n, u,v,r);
        return (double)r->z;
#else
        return(0.0);
#endif
    }
    else
        return (Evaluate_NURBS_By_N(n, u,v,r));
    //End Thomas 27 11 03

}
/*
double Evaluate_NURBS(RXEntity_p e, double u, double v,VECTOR*p_r){
// returns vector at (u,v);  

double h;
struct PC_INTERPOLATION*p = (PC_INTERPOLATION*)e->dataptr;
struct PC_NURB *n ;
ON_3dPoint r;

if(!p) return 0;
n = p->NurbData ; 
 if(!n){ cout<< "mould is Not a NURBS"<<endl;  return 0;}

//Thomas 27 11 03
 if (n->m_pONobject){
#ifndef _NO_ON
  h =  EvaluateONNurbsSurface(n, u,v,&r); assert("here is the EVN bug"==0);
  return r.z;
#else
  return(0.0);
#endif
  }
 else {
  h= Evaluate_NURBS_By_N(n, u,v,&r);
  p_r->x=r.x; p_r->y=r.y; p_r->z=r.z;
return h;
 }
//End Thomas 27 11 03
} */

double Evaluate_NURBS_By_N(struct PC_NURB *n, double u, double v,ON_3dPoint*r)  {
    /* returns vector at (u,v); */
    int i,j;
    double b,b0, d;

    double *Jval= (double*)MALLOC(n->LimitV*sizeof(double));

    assert(n);
    r->x= r->y= r->z=0.0;
    d=0.0;
    if(u<0.) u=0.;
    if(v<0.) v=0.;
    if(u>= 1.) u=1.;
    if(v>=1.) v=1. ;

    for(j=0;j<n->LimitV; j++)
        Jval[j] = PCN_Calc_Bval_nw(n, n->KValV,j,n->LimitV,v,NURB_V);

    for(i=0;i<n->LimitU; i++)  {
        b0= PCN_Calc_Bval_nw(n, n->KValU, i, n-> LimitU ,u,NURB_U);

        if(fabs(b0)  < NURB_SMALL) continue;
        for(j=0;j<n->LimitV; j++)  {
            if(fabs(Jval[j])< NURB_SMALL) continue;
            b=b0*Jval[j] * n->m_W[i][j] ;
            d =d + b ;
            r->x  =   r->x +  n->cP[i][j].x* (float) b;
            r->y  =   r->y +  n->cP[i][j].y* (float) b;
            r->z  =   r->z +  n->cP[i][j].z* (float) b;
        }
    }
    if(d >0.0) {
        r->x =  r->x/ (float) d;
        r->y =  r->y/(float) d;
        r->z =  r->z/(float) d;
    }

    b = (double) r->z ;

    RXFREE(Jval);
    return  ( b);
}

int PCN_Slow_Evaluate_U_Tangent(RXEntity_p e, double u, double v,VECTOR*tu){
    /* returns vector dfbydu  at (u,v);
Doesnt normalise the vector
This is an approximate method that samples the surface a small distance away.
 Its OK for direction but not for magnitude.  DODGY WRONG
*/
    ON_3dPoint r1,r2;
    double u_;

    if(u<0.) u=0.;
    else if(u>1.) u=1.;
    if(v<0.) v=0. ;
    else if(v>1.) v=1.;
    if(fabs(u-0.5) > 0.01)
        u_ = u * .95 + 0.5 * .05;
    else
        u_=u+0.001;
    Evaluate_NURBS(e,u,  v, &r1);
    Evaluate_NURBS(e,u_,  v, &r2);

    tu->x = (r2.x - r1.x)/ (float)(u_ - u);
    tu->y = (r2.y - r1.y)/ (float)(u_ - u);
    tu->z = (r2.z - r1.z)/ (float)(u_ - u);
    if( (fabs(tu->x) < NURB_SMALL) &&  (fabs(tu->y) < NURB_SMALL) && (fabs(tu->z) < NURB_SMALL)) return 0;
    return 8;
}

int PCN_Slow_Evaluate_V_Tangent(RXEntity_p e, double u, double v,VECTOR*tu){
    /* returns vector dfbydu  at (u,v);
Doesnt normalise the vector
This is an approximate method that samples the surface a small distance away (2.5%)
 Its OK for direction but not for magnitude.  DODGY WRONG*/

    ON_3dPoint r1,r2;

    double v_;

    if(u<0.) u=0. ;
    else if(u>1.) u=1.;
    if(v<0.) v=0. ;
    else if(v>1.) v=1.;


    if(fabs(v-0.5) > 0.001)
        v_ = v * .99 + 0.5 * .01;
    else
        v_=v+0.001;
    Evaluate_NURBS(e,u,  v, &r1);
    Evaluate_NURBS(e,u,  v_, &r2);

    tu->x = (r2.x - r1.x)/(float) (v_ - v)  ;
    tu->y = (r2.y - r1.y)/(float) (v_ - v) ;
    tu->z = (r2.z - r1.z)/(float) (v_ - v) ;
    if( (fabs(tu->x) < NURB_SMALL) &&  (fabs(tu->y) < NURB_SMALL) && (fabs(tu->z) < NURB_SMALL)) return 0;
    return 8;
}



int PCN_Evaluate_NURBS_Tangents(RXEntity_p e,const double uin,const double vin,ON_3dPoint *r,
                                ON_3dVector*tu, ON_3dVector*tv){
    /* returns vectors dfbydu and dFbyDv at (u,v);
Doesnt normalise the vectors
return values;;;
 1	r OK
 2	tu ok symbolically
 4	tv OK symbolically
  8	tu slow way
  16	tv slow way.
*/
    int iret;
    ON_3dVector  r11,r20,r02;
    double u,v;
    u = max(uin,0.); u=min(uin,1.);
    v = max(vin,0.); v=min(vin,1.);


    // i1 =0.0;// Evaluate_U_Tangent(e,  u, v,&tu0);
    // i2 = 0.0; // Evaluate_V_Tangent(e,  u, v,&tv0);
    iret = Evaluate_NURBS_Derivatives(e, u,v,r, tu, tv, &r11, &r20, &r02);
    if(iret)
        return iret;
    cout<< "  Evaluate_NURBS_Derivatives !!!!!!!!!!!"<<endl;

    assert(0); //	iret = iret +PCN_Slow_Evaluate_U_Tangent(e,  u, v,tu);// ie add 8

    //	if(!(iret&4))
    assert(0);//	 iret =  iret + 2 * PCN_Slow_Evaluate_V_Tangent(e,  u, v,tv);// ie add 16


    return ( iret) ;
}

int Evaluate_U_TangentSSD(RXEntity_p e, double u, double v,VECTOR*tu){
    /* returns vector dfbydu  at (u,v);
Doesnt normalise the vector
Based on Farin P168 ' Derivative of  a BSpline curve'
*/
    int i,j,iret;
    double bu0,bu,du;
    struct PC_INTERPOLATION*p = (PC_INTERPOLATION *)e->dataptr;
    struct PC_NURB *n;
    double *Ival;
    if(!p) return 0;
    n = p->NurbData ;
    if(!n) return 0;
    Ival = (double*)MALLOC(n->LimitU*sizeof(double));
    iret=1;
    du= tu->x=tu->y=tu->z= 0.0;
    for(i=1;i<n->LimitU; i++)
        Ival[i] = PCN_Calc_Bval_nw(n, (n->KValU-1),i,n->LimitU,u,NURB_U);

    for(j=0;j<n->LimitV; j++)  {
        bu0= PCN_Calc_Bval_nw(n, n->KValV, j, n-> LimitV,v,NURB_V);
        if( fabs(bu0) > NURB_SMALL){
            for(i=1;i<n->LimitU; i++)  {
                if(Ival[i]<NURB_SMALL) continue;
                bu=bu0* Ival[i] * n->m_W[i][j]; // a WRONG GUESS when the weights arent equal
                du = du + bu;
                tu->x  =   tu->x  + (n->cP[i][j].x - n->cP[i-1][j].x)*bu * (float) (n->LimitU-2*n->KValU);
                tu->y  =   tu->y  + (n->cP[i][j].y - n->cP[i-1][j].y)*bu * (float) (n->LimitU-2*n->KValU);
                tu->z  =   tu->z  + (n->cP[i][j].z - n->cP[i-1][j].z)*bu * (float) (n->LimitU-2*n->KValU);
            }
        }
    }
    if(fabs(du) >0.0) {
        tu->x =  tu->x/(float) du;
        tu->y =  tu->y/(float) du;
        tu->z =  tu->z/(float) du;
    }
    else
        iret=0;
    RXFREE(Ival);
    return iret;
}

int Evaluate_V_TangentSSD(RXEntity_p e, double u, double v,VECTOR*tv){
    /* returns vector dFbyDv at (u,v);
Doesnt normalise the vector
Based on Farin P168 ' Derivative of  a BSpline curve'
*/
    int i,j;
    double  bv0,bv, dv;
    struct PC_INTERPOLATION*p = (PC_INTERPOLATION *)e->dataptr;
    struct PC_NURB *n;
    double f, *Jval;
    if(!p) return 0;
    n = p->NurbData ;
    if(!n) return 0;
    Jval = (double*)MALLOC(n->LimitV*sizeof(double));
    dv= tv->x = tv->y = tv->z =0.0;
    f= (double) (n->LimitV-2*n->KValV);
    for(j=1;j<n->LimitV; j++) {
        Jval[j] =  PCN_Calc_Bval_nw(n, (n->KValV-1),j,n->LimitV,v,NURB_V);
        //	dk= Knot(n,j+n->KValV -1) - Knot(n, j-1);
        //	if( dk> NURB_SMALL)				//this is  a GUESS as farin 168
        //		 Jval[j] =	 Jval[j] /dk;
        //	else
        //			 Jval[j] =0.0;
    }

    for(i=0;i<n->LimitU; i++)  {
        bv0=   PCN_Calc_Bval_nw(n, n->KValU, i, n-> LimitU,u,NURB_U) ;
        if ( fabs(bv0 ) > NURB_SMALL) {
            for(j=1;j<n->LimitV; j++)  {
                bv=bv0*Jval[j] * n->m_W[i][j] ; // a WRONG GUESS Oct 2003
                dv = dv + bv;
                tv->x  =  tv->x + ( n->cP[i][j].x -  n->cP[i][j-1].x ) *(float)bv;
                tv->y  =  tv->y + ( n->cP[i][j].y  - n->cP[i][j-1].y ) *(float)bv;
                tv->z  =  tv->z + ( n->cP[i][j].z  - n->cP[i][j-1].z ) *(float)bv;
            }
        }
    }
    if(fabs(dv) >0.0) {
        tv->x =  tv->x/(float)  (dv / f);
        tv->y =  tv->y/(float)  (dv / f);
        tv->z =  tv->z/(float)  (dv / f);
    }
    else
        return 0;
    RXFREE(Jval);
    return 1;
}
int Evaluate_NURBS_By_XYZ(RXEntity_p e,const double xx,const double yy,const double zz
                          ,double*p_pu,double* p_pv){

    // this is the wrapper for GetLocalClosest
    // it expects the u,v parameters to to be in domain {0,1} or else not ON_IsFinite.

    struct PC_INTERPOLATION*p;
    struct PC_NURB *l_n ;
    assert(e) ;
    p = ( struct PC_INTERPOLATION*)e->dataptr;

    l_n = p->NurbData;

    if ( l_n && l_n->m_pONobject){
        ON_Surface *l_s = ON_Surface::Cast (l_n->m_pONobject); // is this RXON_Surface??
        RXON_NurbsSurface *l_rxs = (RXON_NurbsSurface *) l_s;
        if(l_s) {
            ON_3dPoint l_p = ON_3dPoint(xx,yy,zz);
            double m0,r0,m1,r1,u,v;
            m0 = l_s->Domain(0).Min(); r0 = l_s->Domain(0).Length();
            m1 = l_s->Domain(1).Min(); r1 = l_s->Domain(1).Length();

            if(ON_IsValid(*p_pu))
                u = (*p_pu) * r0 + m0;
            else
                u = ON_UNSET_VALUE ;
            if(ON_IsValid(*p_pv))
                v = (*p_pv) * r1 + m1;
            else
                v = ON_UNSET_VALUE;

            if(l_rxs->GetLocalClosestPoint(l_p,u,v,&u,&v)) { // typically ON_NurbsSurface try l_rcxs not l_s
                (*p_pu) = (u-m0 )/ r0;
                (*p_pv) = (v-m1 )/ r1;
            }
            else {
                ON_String l_buf; l_buf.Format(" site (%f %f %f) fails GetCClosest",xx,yy,zz);
                g_World->OutputToClient(l_buf,1);
                return 0;
            }
            return 1;
        } // l_s
    }
    // we get here if its not anON-style nurbs.
    int l_err;
    assert(0);double zzz=Evaluate_NURBS_By_XY( e, xx, yy, p_pu, p_pv,&l_err,0) ;
    return (l_err==0);
}

double Evaluate_NURBS_By_XY(RXEntity_p e,const double xx,const double yy,double*uout,double*vout, int*p_err ,const ON_Xform *tr ){

    /* returns z  at (xx,yy);  finds (u and V)  iteratively by testing the derivatives at the current best guess.
As an optimisation, we keep the previous value of u&v.  Most times, the surface will be sampled at a sequence of
points along a line.
This method has numerical problems. If we are too close to an edge, the length of du and dv decreases to 0

 We could use a taylor expansion to get a better estimate of the step
  Or we could take another look at the spline to prevent this happening

 We limit delta to 0.5 Or something to stop it bouncing around. Seems to work.
 If we fail, we try just once more from (.5,.5) No point in more than that. Unless there were a singularity at (.5,.5)
 */
    struct PC_INTERPOLATION*p;
    struct PC_NURB *nurb ;
#ifndef _OPENMP
    static double u=.5,v=.5; // its this static which makes the function not reentrant
#else
    double u=.5,v=.5;
#endif
    ON_3dPoint r,q;
    ON_3dVector du,dv,verr;
    double zz, deltaU, deltaV,tol,det, Unext, Vnext, relfac=1.0;
    //int trace=0;
    int iret,  count,c2=1; /* c2 lets us go a second time (from .5,.5) after failing. */
    assert(e) ;
    p = ( struct PC_INTERPOLATION*)e->dataptr;

    nurb = p->NurbData;
    tol = nurb->tol; // muddled units. tol is in parameter space,  nurb->tol is in cartesian.
    // so we should scale tol down by (characteristic dimension)
    q.x=  xx; q.y=  yy; q.z=0.0;
    *p_err=99;
#ifndef WIN32
    if(!finite(u)) u=.5;if(u<=0.0) u =0.1;  if(u>=1. ) u=0.9;
    if(!finite(v)) v=.5; if(v<=0.0) v=0.1; if(v>=1. ) v=0.9;
#endif

    do {
        count=100;
        do {
            iret = PCN_Evaluate_NURBS_Tangents(e, u, v, &r,&du, &dv);  // use |TR
            iret=iret;
            zz = Evaluate_NURBS(e,u,v,&r);  // USE TR
            if(tr) {
                du = (*tr) *du; dv = (*tr)*dv; r = (*tr) * r; zz = r.z;
            }

            verr=r -q;
            if((fabs(verr.x) < tol) &&(  fabs(verr.y) < tol))  {
                *uout=u; *vout=v; *p_err=0; return(zz);
            }

/*
 1	r OK
 2	tu ok symbolically
 4	tv OK symbolically
  8	tu slow way
  16	tv slow way.
*/
            switch ( iret) {

            case 8:
            case 9: 		/* du only are OK  so leave deltaV zero*/
            case 3 :
                deltaU = deltaV = 0.0;
                if(fabs(du.y) > NURB_SMALL  && fabs(du.x) < NURB_SMALL )
                    deltaU = verr.y/du.y;
                else if(fabs(du.x) >NURB_SMALL && fabs(du.y) < NURB_SMALL )
                    deltaU = verr.x/du.x;
                else {
                    if(fabs(verr.x/du.x) > fabs(verr.y/du.y))
                        deltaU = verr.y/du.y;
                    else
                        deltaU = verr.x/du.x;
                }
                //deltaU =deltaU / 2.0 ; /* cheat */
                break;
            case 16:
            case 17:		/* dv only are OK   so leave deltaU zero*/
            case 5 :
                deltaV = deltaU = 0.0;
                if(fabs(dv.y) > NURB_SMALL  && fabs(dv.x) < NURB_SMALL )
                    deltaV = verr.y/dv.y;
                else if(fabs(dv.x) >NURB_SMALL && fabs(dv.y) < NURB_SMALL )
                    deltaV = verr.x/dv.x;
                else {
                    if(fabs(verr.x/dv.x) > fabs(verr.y/dv.y))
                        deltaV = verr.y/dv.y;
                    else
                        deltaV = verr.x/dv.x;
                }
                //deltaV =deltaV / 2.0; /* cheat */
                break;
            case 1: // added this case jan 2009 because we are ON only
            case 25:    //because we are getting the derivatives by sampling
            case 24:
                det = - du.x*dv.y +  du.y*dv.x;
                deltaU = (verr.x*dv.y - verr.y*dv.x )/det;
                deltaV = (-verr.x*du.y + verr.y*du.x )/det;
                break;
            default:{
                char str[256];
                sprintf(str," DEBUG\nEvaluate_NURBS_Tangents g_World->OutputToClient \n %d at %f %f\n (%f %f %f)\n", iret,u,v,r.x,r.y,r.z);

                g_World->OutputToClient(str,2);
                deltaV = deltaU = 0.0;
            }
            }//switch

            Unext = u + deltaU * relfac; Vnext = v + deltaV *relfac;
            /* Prevent going outside domain */
            if(Unext>=1.0) {
                if(fabs(deltaV ) < 1e-8) {
                    //printf(" off edge at U=%f, v=%f\n",Unext,Vnext);
                    break; // Peter Nov 2003
                    *p_err=1; return zz;
                }
                deltaU = (1.0-u)/2.; // the (over 2) slows things a lot.
            }
            else if (Unext <= 0.0) {
                if(fabs(deltaV ) < 1e-8) {
                    //printf(" off edge at U=%f, v=%f\n",Unext,Vnext);
                    break; // Peter Nov 2003
                    *p_err=1;return zz;
                }
                deltaU = -u/2.;

            }
            if(Vnext>=1.0) {
                if(fabs(deltaU ) < 1e-8) {
                    //printf(" off edge at u=%f, v=%f\n",Unext,Vnext);
                    break; // Peter Nov 2003
                    *p_err=1;return zz;
                }
                deltaV = (1.0-v)/2.;

            }
            else if (Vnext <= 0.0) {
                if(fabs(deltaU ) < 1e-8) {
                    //printf(" off edge at u=%f, v=%f\n",Unext,Vnext);
                    break; // Peter Nov 2003
                    *p_err=1; return zz;
                }
                deltaV = -v/2.;
            }
            if (fabs(deltaU ) < 1e-10 && fabs(deltaV ) < 1e-10) {
                printf("Pull-to-mould STOPPED in %d\n", 101-count);
                printf(" iret %d u %f v %f xx %f yy %f  r (%f %f %f) \n", iret,  u, v,xx, yy,r.x,r.y,r.z);
                *uout=u; *vout=v;  *p_err=1; return zz;
            }
            u = u  +  deltaU *relfac;
            v = v  + deltaV  *relfac;
            //	if(u<0.0 || v<0.0  ||u >1.0 || v>1.0 ) printf(" OUTSIDE  new (%f %f)  old (%f %f)  \n",u,v, u-deltaU*relfac, v-deltaV*relfac);
        } while (count--);
        u=v=.5;
        relfac = relfac/2.; // Peter Nov 2003
    } while (c2--);
    *uout=u; *vout=v; relfac = relfac/2.;
    char buf[256];
    sprintf(buf,"Pull-to-mould not converged. pos is %f %f %f \n", q.x,q.y,q.z);

    g_World->OutputToClient(buf,1);

    *p_err=2;
    return zz;
}


int DxDy_to_DuDv (RXEntity_p e, double dx, double dy, double*uarg, double*varg){

    /* Given a current {u,v} and the desire to move by {dx,dy}, find the
new  {u,v} that gets close */
    int iret;
    double u,v;
    ON_3dPoint  r;ON_3dVector r01,r10,r11,r02,r20;

    u = *uarg; v= *varg;
    iret = Evaluate_NURBS_Derivatives(e, u,v,&r, &r10, &r01, &r11, &r20, &r02);
    if(iret != 63) printf(" didnt get all derivs %d\n",iret);
    else cout<< " all derivs found "<<endl;
    printf(" u,v = (%f %f\n", u,v);
    //	vprint(" r      ", &r,1) ;
    //	vprint(" r10   ", &r10,1) ;
    //	vprint(" r01   ", &r01,1) ;
    //	vprint(" r11   ", &r11,1) ;
    //	vprint(" r02   ", &r02,1) ;
    //vprint(" r20   ", &r20,1) ;
    //getchar();


    return 1;
}

int Ev_NU_r (struct PC_NURB *n , double u, double v, double *Ival, double *Jval, VECTOR*r,int normalise) {
    int i,j, ok=0;
    double b,b0,d=0.0;
    r->x= r->y= r->z=0.0;
    for(i=0;i<n->LimitU; i++)  {
        b0= Ival[i];
        if( fabs(b0 ) < NURB_SMALL) continue;
        for(j=0;j<n->LimitV; j++)  {
            if(fabs(Jval[j] )< NURB_SMALL) continue;
            b=b0*Jval[j]*n->m_W[i][j] ;

            d =d+ b;
            r->x  =   r->x  +  n->cP[i][j].x * (float) b;
            r->y  =   r->y  +  n->cP[i][j].y * (float) b;
            r->z  =   r->z  +  n->cP[i][j].z * (float) b;
            ok=1;
        }
    }
    if(NURBDBG) 	printf(" \n Ev_NU_r  gave ok=%d d=%f cf %f\n", ok, d, NURB_SMALL);

    if( (fabs(r->x) < NURB_SMALL) &&  (fabs(r->y) < NURB_SMALL) && (fabs(r->z) < NURB_SMALL) &&  normalise != NURBS_NORM )  ok=0;

    /* the above line flags short tangent vectors. It works by chance ( WRONG) because we don't normalise derivative vectors */
    if(!ok) { if(NURBDBG) cout<< "not OK ret 0\n"<<endl;  return 0;}
    if(normalise != NURBS_NORM) { if(NURBDBG) cout<< " NONORM ret 1\n"<<endl; return 1;}
    if(fabs(d) < NURB_SMALL) { if(NURBDBG) cout<< " small D ret 0\n"<<endl;  return 0;}
    r->x =  r->x/(float) d;
    r->y =  r->y/(float) d;
    r->z =  r->z/(float) d;
    if(NURBDBG) cout<< " NORMALISED OK ret=1\n"<<endl;
    return 1;
}

int Evaluate_NURBS_Derivatives(RXEntity_p e,const double u,const double v,
                               ON_3dPoint*r,
                               ON_3dVector*r10, ON_3dVector*r01,
                               ON_3dVector*r11, ON_3dVector*r20, ON_3dVector*r02)
{
    /* returns vector at (u,v); */
    int i,j;

    struct PC_INTERPOLATION*p = (PC_INTERPOLATION *)e->dataptr;
    struct PC_NURB *n ;
    double *Jval, *Ival;// ,  Ival_1[NURBMAX],   Jval_1[NURBMAX];
    //  Ival_2[NURBMAX],   Jval_2[NURBMAX];
    int l_ret = 0;

    if(!p) return 0;
    n = p->NurbData ;
    assert(n);

    //Thomas 27 11 03

    if (n->m_pONobject){
#ifndef _NO_ON
        l_ret = EvaluateONNurbsDerivatives(n,u, v,r, r10, r01, r11, r20, r02); //return 1 is succeeded or 0
#endif
    }
    else
    {
        //End Thomas 27 11 03
        Jval = (double*)MALLOC(n->LimitV*sizeof(double));
        Ival = (double*)MALLOC(n->LimitU*sizeof(double));

        for(j=0;j<n->LimitV; j++)  {
            Jval[j]   = PCN_Calc_Bval_nw(n, n->KValV,j,n->LimitV,v,NURB_V);
            /*	Jval_1[j] = Calc_Bval_1  (n, n->KValV ,j,n->LimitV,v,NURB_V) *n->Wv[j];
   Jval_2[j] = Calc_Bval_2  (n, n->KValV ,j,n->LimitV,v,NURB_V) *n->Wv[j]; */
        }
        for(i=0;i<n->LimitU; i++)   {
            Ival[i]  = PCN_Calc_Bval_nw(n, n->KValU, i, n-> LimitU ,u,NURB_U);
            /*	Ival_1[i]= Calc_Bval_1  (n, n->KValU, i, n-> LimitU ,u,NURB_U) *n->Wu[i];
    Ival_2[i]= Calc_Bval_2  (n, n->KValU, i, n-> LimitU ,u,NURB_U) *n->Wu[i]; */
        }
        RXFREE(Ival); RXFREE(Jval);
        VECTOR vr;
        l_ret +=  		Ev_NU_r (n, u,v, Ival,   Jval,    &vr,    NURBS_NORM);
        r->x=vr.x; r->y=vr.y; r->z=vr.z;
        //	iret +=	 2* 	Ev_NU_r (n, u,v, Ival_1, Jval,    r10,  NURBS_NORM);  // Guess not NONORM 31/1/01
        //	iret +=  	4 *	Ev_NU_r (n, u,v, Ival,   Jval_1,  r01,  NURBS_NORM);
        /* 	iret +=  	8*	Ev_NU_r (n, u,v, Ival_1, Jval_1,  r11,  NURBS_NONORM);
  iret +=  	16*	Ev_NU_r (n, u,v, Ival_2, Jval,    r20,  NURBS_NONORM);
  iret +=  	32*	Ev_NU_r (n, u,v, Ival,   Jval_2,  r02,  NURBS_NONORM); */
    }

    if(NURBDBG) 	printf(" Evaluate_NURBS_Derivatives returns %d\n", l_ret);

    return  ( l_ret);
}


int PCN_Print_Nurbs_Samples(const RXEntity_p e, FILE *fp) {

    double u,v,du,dv;
    struct PC_INTERPOLATION *p = (PC_INTERPOLATION *)e->dataptr;
    int ku,kv;
    int nu=11,nv=9;
    ON_3dPoint res;


    if(!p->NurbFlag)
        return 0;
    du = 1.0/(float)(nu-1*1)  ;
    dv = 1.0/(float)(nv-1*1) ;
    for(kv=0,v=0.0 ;kv< nv ;kv++, v=v+dv) {
        for(ku=0,u=0.0 ;ku< nu ; ku++, u=u+du) {

            Evaluate_NURBS(e, u , v,&res);
            fprintf(fp,"\t(%f %f %f)", res.x,res.y,res.z);
        }
        fprintf(fp,"\n");
    }

    return(1);
}
int PCN_OneD_NURBS_To_Poly(struct PC_NURB *n,int *c,VECTOR **poly){
    double u=0.0,v=0.0;
    int i;
    ON_3dPoint r;
    *c = g_Spline_Division;
    assert(!n->m_pONobject);
    if(n->KValU == 1) {
        *c=n->LimitU;		//must be a polyline. June 2003 for Janet
        *poly = (VECTOR *)MALLOC((*c)*sizeof(VECTOR));
        for(i=0;i<(*c);i++)
            PC_Vector_Copy(n->iP[i][0], &((*poly)[i]));
    }
    else {
        *poly = (VECTOR *)MALLOC((*c)*sizeof(VECTOR));

        for(i=0;i<(*c);i++) {
            u = (double)i/(double) (*c -1);
            Evaluate_NURBS_By_N(n, u , v,&r);
            ((*poly)[i]).x=r.x;
            ((*poly)[i]).y=r.y;
            ((*poly)[i]).z=r.z;
        }
    }
    return 1;
}


