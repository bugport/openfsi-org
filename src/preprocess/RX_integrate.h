// RX_integrate.h
#ifndef _RX_INTEGRATE_H_
#define _RX_INTEGRATE_H_

	#ifdef __cplusplus
		#define EXTERN_C extern "C"
	#else
		#define EXTERN_C extern
	#endif  //#ifdef __cplusplus

typedef  double (*IFuncPtr)( void *p, const double t);

EXTERN_C double  RX_Integrate(IFuncPtr fp ,void*p, const double d0,const double d1, const double tol=1.0e-6, double *err=0);

EXTERN_C double  RX_RIntegrate(
	IFuncPtr fp ,
	void *p,
	const double x0,
	const double x1, 
	const double y0,
	const double y1, 
	const double ymid, 
	const double tol, 
	int depth,
	double*err
	);

#endif

