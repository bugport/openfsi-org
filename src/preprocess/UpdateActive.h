#ifndef _UPDATEACTIVE_H_
#define _UPDATEACTIVE_H_

//EXTERN_C int UpdateSolvedFlag(SAIL *sail,const char *seedtype);
EXTERN_C int Set_As_Changed_High(RXEntity_p p);

EXTERN_C int Set_As_Changed_Low(const char *spec,struct LINKLIST *plist);

#endif  //#ifndef _UPDATEACTIVE_H_
