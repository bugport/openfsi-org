
/* boxsearch c.  Routines to enable X-testing by cartesian boxes.
	Peter Heppel  24.02.01
13 Feb 2004  ONObject 
18 jan 03  insulate line 45 against entities with null dataptr.  Presumably this means an unresolved SC

  */
  
#include "StdAfx.h" 
#include "RXEntityDefault.h"
#include "RXSitePt.h"
#include "RXSeamcurve.h"
#include "filament.h"
#include "etypes.h"
#include "entities.h"

//#include "seamcurve.h"
#include "RXSail.h"
#include "boxsearch.h"


struct PC_QCD* PCX_Begin_Box_Search(SAIL *p_sail,class FILAMENT_LOCAL_DATA *fld){
	// ALLOCS a Pc_QCD,clears all SC 'Dont Search' flags.
struct PC_QCD*q = (PC_QCD*)CALLOC(1,sizeof(struct PC_QCD));

	q->l=q->r=fld->v[0]->x;
	q->t=q->b=fld->v[0]->y;
	for(int i=1;i<3;i++){
		if(fld->v[i]->x > q->r) 
			q->r=fld->v[i]->x ;
		else if(fld->v[i]->x < q->l) 
			q->l=fld->v[i]->x;

		if(fld->v[i]->y > q->t) 
			q->t=fld->v[i]->y ;
		else if(fld->v[i]->y < q->b) 
			q->b=fld->v[i]->y;
	}
	q->flags=PCX_TRIANGLE;

// Now set the SC flags

		ent_citer it;
    for (it = p_sail->MapStart(); it != p_sail->MapEnd(); ++it) {
		RXEntity_p e  =dynamic_cast<RXEntity_p>( it->second);
		if(e->TYPE!=SEAMCURVE) continue;
		if(!(e)) 	continue;
		((sc_ptr) e)->FlagClear(DONT_SEARCH);
	}
	q->m_sail = p_sail;
return q;
}

EXTERN_C int PCX_IsPointInside(struct PC_QCD *q, ON_3dPoint p_t,double tol){

	if (p_t.x   > q->r + tol) return 0;  //what about Z
	if (p_t.x < q->l - tol) return 0;

	if (p_t.y > q->t + tol) return 0;
	if (p_t.y < q->b - tol) return 0; 

 return 1;
}
int PCX_Compare_Boxes(struct PC_QCD *q1,struct PC_QCD *q2){
//return PCX_ALL_INSIDE or PCX_ALL_OUTSIDE	or PCX_INTERSECTING	
	// depending on where q2 is wrt q1;

	assert(q1->r >= q1->l);
	assert(q1->t >= q1->b);
	assert(q2->r >= q2->l);
	assert(q2->t >= q2->b);
	if(q1->l <= q2->l && q1->r >= q2->r && q1->b<= q2->b && q1->t >= q2->t)
		return (PCX_ALL_INSIDE);  

	if(q2->l < q1->l && q2->r > q1->r && q2->b< q1->b && q2->t > q1->t)
		return (PCX_SURROUNDS); 

// q2 is off to the right
	if(q2->l >q1->r)
			return PCX_ALL_OUTSIDE;

// q2 is off to the left
	if(q2->r < q1->l)
			return PCX_ALL_OUTSIDE;

// q2 is off to the bottom
	if(q2->t < q1->b)
			return PCX_ALL_OUTSIDE;

// q2 is off to the top
	if(q2->b > q1->t)
			return PCX_ALL_OUTSIDE;

// b l corner of q2 inside  
	if(q1->l < q2->l && q1->r > q2->l && q1->b< q2->b && q1->t > q2->b)
		return (PCX_INTERSECTING);  

// t l corner of q2 inside  
	if(q1->l < q2->l && q1->r > q2->l && q1->b< q2->t && q1->t > q2->t)
		return (PCX_INTERSECTING); 

// b r corner of q2 inside  
	if(q1->l < q2->r && q1->r > q2->r && q1->b< q2->b && q1->t > q2->b)
		return (PCX_INTERSECTING); 

// t r corner of q2 inside  

	if(q1->l < q2->r && q1->r > q2->r && q1->b< q2->t && q1->t > q2->t)
		return (PCX_INTERSECTING);  

return PCX_DONT_KNOW;
}

int PCX_End_Box_Search(  struct PC_QCD *q){
	// RXFREES q,clears SC 'dont_search'flags. Leaves their inside lists.

	SAIL *sail = q->m_sail ;
	if (q) RXFREE(q);

	ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it){
		RXEntity_p e  = dynamic_cast<RXEntity_p>( it->second);
		if(e->TYPE!=SEAMCURVE) continue;
		((sc_ptr) e)->FlagClear(DONT_SEARCH);
	}
return 0;
}
int PCX_Clear_QCD( struct PC_QCD* q){
	// RXFREES q. clears all inside_lists.
	// call from Update_Curve

	sc_ptr sc;

	if(!q)
		return 0;
	ent_citer it;
    for (it = q->m_sail->MapStart(); it != q->m_sail->MapEnd(); ++it) {
		RXEntity_p e  = dynamic_cast<RXEntity_p>( it->second);
		if(e->TYPE!=SEAMCURVE) continue;
		sc=(sc_ptr  ) e;
		Free_Entity_List(&(sc->Inside_List));
	}
	RXFREE(q);
	return 0;
}


