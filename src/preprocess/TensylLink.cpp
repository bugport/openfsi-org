#include "StdAfx.h"
#if defined(RXRHINOPLUGIN) ||defined(PHYSX)
class RXEntity;
#else
#include "RXSRelSite.h"
#endif
#include "RX_PCFile.h"
#include "TensylLink.h"

CTensylLink::CTensylLink(void)
    : m_nn(0)
    , m_nk1(0)
    , m_nk2(0)
    , m_Slacklength(0)
    , m_prop(0)
    , m_Control(0)
    , m_Boundary(0)
    , m_FieldForLink(0)
    ,m_fnn(0)
    ,m_f(0)
    ,m_e(0)
    , m_Row(0)
    , m_Col(0)
{
    //the default constructor is used by stl
}
CTensylLink::CTensylLink(int p_nn, int p_n1,int p_n2, double p_Slacklength, int p_prop,
                         int p_Control,int p_Boundary,
                         const int p_Field,const int pRow,const int pCol,
                         class RX_PCFile*f,
                         QString att)
    : m_nn(p_nn)
    , m_nk1(p_n1)
    , m_nk2(p_n2)
    , m_Slacklength(p_Slacklength)
    , m_prop(p_prop)
    , m_Control(p_Control)
    , m_Boundary(p_Boundary)
    , m_FieldForLink(p_Field)
    ,m_Row(pRow)
    ,m_Col(pCol)
    ,m_fnn(0)
    ,m_f(f)
    ,m_e(0)
    ,m_atts(att)
{
}

CTensylLink::~CTensylLink(void)
{
}
int CTensylLink::GetStartNode() const{// to derefernce the  node we go f->m_nodes[m_nkx].m_nn.  and then MasterNodeNo on that (I think)
    if(!m_f)
        return m_nk1;
    int n =m_f->m_nodes[m_nk1].m_nn ;
    n = m_f->MasterNodeNo(n);
    return n;
}	
int CTensylLink::GetDestNode() const{
    if(!m_f)
        return m_nk2;
    int n  =m_f->m_nodes[m_nk2].m_nn ;
    n = m_f->MasterNodeNo(n);
    return n;
}

int CTensylLink::Print(FILE *fp){
    int rc=0;
    int n1= GetStartNode();
    int n2= GetDestNode();
    //   Number, StartNode, EndNode, SlackLength, PropNo, Control, Boundary, Field
    fprintf(fp,"%d %d %d %17.12f %d %d %d %d  %d  %d '%s' !  original indices %d %d\n",
            m_nn,n1,n2,m_Slacklength,m_prop,
            m_Control,m_Boundary,m_FieldForLink,m_Row,m_Col,qPrintable(m_atts), m_nk1,m_nk2 );
    return rc;
}
#if !defined( PHYSX) && !defined(RXRHINOPLUGIN)
#ifndef PCIO
double  CTensylLink::DeflectedLength() const
{
    CTensylNode &p1 = m_f->m_nodes[this->GetStartNode()];
    CTensylNode &p2 = m_f->m_nodes[this->GetDestNode ()];

    Site *s1=(Site *) p1.m_e;
    Site *s2=(Site *) p2.m_e;

    ON_3dPoint x1 = s1->DeflectedPos();
    ON_3dPoint x2 = s2->DeflectedPos();
    return  x1.DistanceTo (x2);
}

//MakeElementProperties requires l.m_control l.m_Slacklength, to be already set
int CTensylLink::MakeElementProperties(CTensylProp *p , class RX_PCFile *pcf, double &ea,double &zi,double &ti) const
{
    double l_sf = ON::UnitScale(ON::inches,ON::meters);
    if(m_Control==1) {
        ea = p->m_ea* FORCE_UNIT_FACTOR;
        zi = m_Slacklength*l_sf;
        ti=0;
    }
    else {
        CTensylNode &p1 =  pcf->m_nodes[GetStartNode()];
        CTensylNode &p2 = pcf->m_nodes[ GetDestNode ()];
        ea=0.0;
        ea = p->m_ea* FORCE_UNIT_FACTOR; // new august 2011 for Janet formfinding
        ti = m_Slacklength* FORCE_UNIT_FACTOR;
        zi = p1.m_pt.DistanceTo (p2.m_pt)*l_sf;
    }
    return 1;
}
#endif
#endif
