
// curveplaneintersection2.cpp: 
//
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#include "global_declarations.h"

#include "rlxNearestPoint.h"

#include "curveplaneintersection2.h"

int CurvePlaneIntersections(const ON_LineCurve * p_pLine,
                            const ON_Plane * p_Plane,
                            RXUPoint & p_pt,
                            const double & p_EpsDot,
                            const RXTRObject * p_PersistentCurve)
{

    //!Finds the intersection beetween a line and a Plane
    /*!
 and intersection is found if the line crosses the plane
 if an intersection is founf set p_pt on the intersection
 returns RX_CRVPL_CROSS if an intersection is found between the end and the start of the curve
 returns RX_CRVPL_NOTCROSS if NO intersection is found between the end and the start of the curve
 returns RX_CRVPL_PARALLEL if the line is parallel to the plane
 returns RX_CRVPL_CRVINPLANE if the line is in the plane
*/

    assert(ON_LineCurve::Cast(p_pLine));
    assert(p_Plane);
    assert(p_EpsDot>0);

    ON_3dVector l_N = p_Plane->zaxis;
    l_N.Unitize();

    ON_3dPoint l_start = p_pLine->PointAtStart();
    ON_3dPoint l_end = p_pLine->PointAtEnd();
    ON_3dVector l_V1 = l_start-p_Plane->origin;
    ON_3dVector l_V2 = l_end-p_Plane->origin;
    ON_3dVector l_D = l_end-l_start;

    if (  (fabs(ON_DotProduct(l_N,l_V1))<p_EpsDot)&&( fabs(ON_DotProduct(l_N,l_V2))<p_EpsDot) )
        return RX_CRVPL_CRVINPLANE;

    if (fabs(ON_DotProduct(l_D,l_N))<p_EpsDot)
        return RX_CRVPL_PARALLEL;

    double l_k = (l_N.x*(-l_start.x+p_Plane->origin.x)+l_N.y*(-l_start.y+p_Plane->origin.y)+l_N.z*(-l_start.z+p_Plane->origin.z))/(l_N.x*l_D.x+l_N.y*l_D.y+l_N.z*l_D.z);

    if ((l_k<=0-p_EpsDot)||(l_k>=1+p_EpsDot))
        return RX_CRVPL_N0TCROSS;

    ON_Interval l_domain = p_pLine->Domain();
    if (p_PersistentCurve)
    {
        assert(RXTRObject::Cast(p_PersistentCurve));
        p_pt.Set(l_domain.Min()+l_k*l_domain.Length(),p_PersistentCurve);
    }
    else
        assert(0); //for now 08 11 04   p_pt.Set(l_domain.Min()+l_k*l_domain.Length(),p_pLine);

    return RX_CRVPL_CROSS;
}//int CurvePlaneIntersections

//!Finds all the intersections between a Polyline and a Plane
/*!
 and intersection is found if the Polyline crosses the plane
 if an intersection is found set p_pt on the intersection
 returns RX_CRVPL_CROSS if an intersection is found beetwene hte end and the start of the curve
 returns RX_CRVPL_N0TCROSS if NO intersection is found beetwene hte end and the start of the curve
 returns RX_CRVPL_COPLANAR if the line is coplanar to the plane
 returns RX_CRVPL_CRVINPLANE if the line is in the plane
*/
int CurvePlaneIntersections(const ON_PolylineCurve * p_Crv,
                            const ON_Plane * p_Plane,
                            ON_ClassArray<RXUPoint> & p_pts, // appends the crosses here
                            const double & p_EpsDot,
                            const RXTRObject * p_pPersistentCurve) // need a similar routine which does ON_Curve to ON_Plane
{

    assert(ON_PolylineCurve::Cast(p_Crv));
    assert(p_Plane);
    assert(p_EpsDot>0);

    p_pts.Empty();
    int i;
    int l_nbline = p_Crv->m_pline.Count();

    int l_n=0;

    ON_LineCurve l_line;
    ON_3dPoint l_temp;

    RXUPoint l_pt;
    int l_tempret = RX_CRVPL_N0TCROSS;
    int l_ret = RX_CRVPL_N0TCROSS;
    for (i=0;i<l_nbline-1;i++)
    {
        l_line.SetDomain(p_Crv->m_t[i],p_Crv->m_t[i+1]);
        l_line.m_line.from = p_Crv->m_pline[i];
        l_line.m_line.to = p_Crv->m_pline[i+1];
        //	assert(l_line.IsValid()); slow

        l_tempret = CurvePlaneIntersections(&l_line, // here l_line is a segment of the polyline
                                            p_Plane,
                                            l_pt,
                                            p_EpsDot,
                                            p_pPersistentCurve);

        if (l_tempret==RX_CRVPL_CROSS)
        {
            int l_nbInter = p_pts.Count();
            if (l_nbInter>0)
            {
                if (p_pts[l_nbInter-1].Gett()!=l_pt.Gett())
                    p_pts.AppendNew() = l_pt;
                else
                    cout<< " skip this cross. Its the same as the last"<<endl;
            }
            else
                p_pts.AppendNew() = l_pt;
            l_ret = RX_CRVPL_CROSS;
        }//if (l_tempret==RX_CRVPL_CROSS)

    }//for (i=0;i<l_nbline1;i++)

    return l_ret;
}//int CurvePlaneIntersections


int CurvePlaneIntersections(const ON_Curve * p_Crv,
                            const ON_Plane * p_Plane,
                            ON_ClassArray<RXUPoint> & p_pts,
                            const double & p_EpsDot,
                            const RXTRObject * p_pPersistentCurve,
                            double*p_Pseed) // need a similar routine which does ON_Curve to ON_Plane
{
    p_pts.Empty();
    int i, l_n=0;

    ON_LineCurve l_line;
    ON_3dPoint l_temp;
    ON_3dPointArray l_results;


    bool rc = ONCurveIntersect(p_Crv,*p_Plane,l_results,p_EpsDot); //,NULL,NULL ); // disable p_Pseed WASTE);	// only gets one intersection

    if(!rc) {
        return RX_CRVPL_N0TCROSS;  // either parallel or off the end
    }
    RXUPoint l_pt;
    int l_ret = RX_CRVPL_N0TCROSS;
    //	int l_ncrosses = l_results.Count();

    for (i=0;i<l_results.Count() ;i++) // fill in the RXUpoint Array
    {

        l_temp = l_results[i];
        int GoingUp =(int) l_temp.y;
        l_pt.Set(l_temp.x,p_pPersistentCurve ,GoingUp );

        int l_nbInter = p_pts.Count();
        if (l_nbInter>0)
        {
            if (p_pts[l_nbInter-1].Gett()!=l_pt.Gett())// beware comparing floats
                p_pts.AppendNew() = l_pt;
            else
                cout<< " skip this cross. Its the same as the last"<<endl;
        }
        else
            p_pts.AppendNew() = l_pt;
        l_ret = RX_CRVPL_CROSS;
    }//for (i=0;i<l_nbline1;i++)

    return l_ret;
}//int CurvePlaneIntersections(ON_C)
int CurvePlaneIntersections(const RXTRObject * p_Crv,
                            const ON_Plane * p_Plane,
                            ON_LineCurve p_Line,
                            ON_ClassArray<RXUPoint> & p_pts,
                            const double & p_EpsDot,
                            const double & p_MaxDistance,
                            double*p_Pseed)

//Finds all the intersections between a CURVE and a Plane (ONLY WORKS IF THE CURVE IS A POLYLINE)
/*
 and intersection is found if the CROSS crosses the plane
 if an intersection is found set p_pt on the intersection
 returns RX_CRVPL_CROSS if an intersection is found beetwene hte end and the start of the curve
 returns RX_CRVPL_N0TCROSS if NO intersection is found beetwene hte end and the start of the curve
 returns RX_CRVPL_COPLANAR if the line is coplanar to the plane
 returns RX_CRVPL_CRVINPLANE if the line is in the plane
*/
{
    int rc=0;

    const RXCurve * l_rxcrv = RXCurve::Cast(p_Crv);
    const ON_Curve * l_ONcrv = dynamic_cast<const ON_Curve*>(l_rxcrv->GetONCurve());

    if(g_mydebug) {
        if(!l_rxcrv)  { cout<< "p_crv wont cast to RXCurve"<<endl; rc++;}
        if(!l_ONcrv) { cout<< "can't dereference the ON_Curve"<<endl; rc++;}
        if(!p_Plane) { cout<< "p_plane is NULL"<<endl; rc++;}
        if(!(p_EpsDot>0))  { printf ("p_Epsdot is %f",p_EpsDot); rc++;}

        if(rc){
            printf ("rc is %d",rc); fflush(stdout); exit(12);
        }
    }

    ON_3dPointArray l_pline_points;
    ON_SimpleArray<double> l_pline_t;

    if (l_ONcrv->IsPolyline(&l_pline_points,&l_pline_t))
    {//The 2 curves can be seen as Polylines, NOTE: A line is seen as a polyline with only 2 points.
        // peter safer to cast as ON_POLYLINECurve
        ON_PolylineCurve l_PL;

        l_PL.m_pline = l_pline_points;
        l_PL.m_t = l_pline_t;

        const int l_res =  CurvePlaneIntersections(&l_PL,
                                                   p_Plane,
                                                   p_pts,
                                                   p_EpsDot,
                                                   p_Crv);
        return l_res;
    }
    else { // not a polyline.

        const int l_res =  CurvePlaneIntersections(l_ONcrv,
                                                   p_Plane,
                                                   p_pts,
                                                   p_EpsDot,
                                                   p_Crv,
                                                   p_Pseed);
        return l_res;


#ifdef NEVER
        ON_2dPointArray results;
        results.Empty();
        ON_LineCurve lc = ON_LineCurve(p_Line);
        const ON_Curve *cc =  dynamic_cast<const ON_Curve*>(&lc);
        if(g_mydebug) {
            cout<< "in CurvePlaneIntersections on Other"<<endl;
            fflush(stdout);
        }
        double tol = 0.00001; // HORRIBLE
        int old_rc=results.Count(); cout<<"TODO: verify thiS"<<endl;
        rc =ONCurv eIntersect( l_ONcrv,cc,results,p_MaxDistance,tol);  // use the line not the plane.
        //what is the correct return value
        if(rc) {
            ON_2dPoint *theP; int k;
            RXUPoint up;
            for (k=0;k<results.Count(); k++) {
                theP = results.At(k); // x is t on crv.  y is t on line
                up.Set(theP->x,p_Crv);  // a point on the curve. LAter we will get the closest pt on the line
                p_pts.AppendNew() = up;
            }
            return RX_CRVPL_CROSS;
        }// if rc
#endif
    }

    //#ifndef RHINO_DEBUG_PLUGIN
    //	error("CurvePlaneIntersections2 Only works if the curve is a polyline!! ",2 );
    //#endif  //#ifdef RHINO_DEBUG_PLUGIN

    return RX_CRVPL_N0TCROSS;
}//int CurvePlaneIntersections


