
#ifndef _UTILITY_HDR_
#define _UTILITY_HDR_ 1

// #include "Graphic Struct.h" // for Point

#ifdef _X
	#include <Xm/Xm.h>
#endif

#define FABS(a)         ((a) < 0.0 ? -(a) : (a))
#define local           static



#ifdef   _X
	EXTERN_C Widget	Get_Top_Shell (Widget);
	EXTERN_C boolean parse_args (const char *, int, char **);
#else
#error (" utility.h is SSD")
        EXTERN_C bool parse_args (const char *, int, char **);
#endif
	
EXTERN_C void	fit_segment (const char *);
EXTERN_C void	clamp_point_to_window (Point *);
EXTERN_C void 	insert_bbox (const char *, Point *, Point *);

#endif  //#ifndef _UTILITY_HDR_
