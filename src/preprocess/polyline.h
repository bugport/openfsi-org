/* polyline.h */


#ifndef POLYLINE_16NOV04
#define POLYLINE_16NOV04

#include "vectors.h"
#ifdef __cplusplus
#include "griddefs.h"
 class RXOffset; 
#endif
EXTERN_C int SeamCurve_Find_Straight(const ON_3dPoint e1,const ON_3dPoint e2, VECTOR**p,int*c,float*chord);
EXTERN_C int PC_Compute_Polyline_Mean(VECTOR *p, int c, VECTOR *v);
EXTERN_C int PCP_Polyline_Remove_Close_Points(VECTOR*p, int*c, double tol);
EXTERN_C int 	PC_Polyline_Extend_Ends(VECTOR*p2,int c2,double tol);
EXTERN_C int PC_Polyline_Compare(VECTOR*p1, int c1, VECTOR*p2, int c2, double tol);/* return 1 if different */
EXTERN_C int PC_Polyline_Copy(VECTOR*dest, VECTOR*source, int count); 
double AKM_Compute_Dot_Product(VECTOR *v1,VECTOR *v2);
int Append_To_Poly(VECTOR**pptr, int *count,VECTOR *pin,int cin);

int PC_Intersection(const ON_3dPoint a1,const ON_3dPoint  b1,const ON_3dPoint  a2,const ON_3dPoint  b2, ON_3dPoint &v,float*s1r,float*s2r);

int PC_Dist_Between_Lines(VECTOR a1,VECTOR b1,VECTOR a2,VECTOR b2,float *s1,float *s2,VECTOR *v,float *dis);
 
int Find_Polyline_Intersection(VECTOR*p1,VECTOR *p2,int c1,int c2,double*s1,double*s2,double* s1max,double *s2max,VECTOR *v);
int Place_On_Poly(float offset,VECTOR *p,int c,VECTOR*v);
int Place_On_Poly(const float offset,VECTOR *const p,const int c, ON_3dPoint &v);
EXTERN_C int Place_On_Poly_By_X(float offset,VECTOR *p,int c,VECTOR*v);

//! find the tangent on a seamcurve/seam 
/*! the entity must be a seamcurve, in which case <offset> is needed
It may be a seam, in which case <side> is used 
ONLY WORKS FOR SEAMCURVE Does not support SEAM
p_ptr : Seam curve or sema entity (INPUT)
p_offin : offset along the seamcurve, needed if seamcurve (INPUT)  
p_side : side of the seam, needed if seam (INPUT)
p_v : tangent vector (unit vector)(OUTPUT)
*/
int Find_Poly_Tangent(RXEntity_p p_ptr,RXOffset *p_offin,int p_side,ON_3dVector *p_v);

int Insert_Into_Poly(VECTOR **p,int *c,VECTOR v,int ka);

int PC_Normalise_Polyline(VECTOR *p,const int c, const ON_3dVector &Zaxis);
int Polygon_Centroid( VECTOR p[],int c, ON_3dPoint *cen); 

int Shrink_Poly(int c,VECTOR *p,double f);
int PCP_Poly_Planar(int c, VECTOR *poly);
EXTERN_C int Group_Polyline( VECTOR *p, int *c, int n, double p_tol);
EXTERN_C int PolyPrint(const char*a,const  VECTOR*p,const int c);
#endif //#ifndef POLYLINE_16NOV04
