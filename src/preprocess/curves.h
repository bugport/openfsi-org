/* curves.h declarations and prototypes for the PC_CURVE (RXCurve)

 which is intended to be a general curve description coded in such
 a way that the formulation can be changed later */
#ifndef _CURVES_H_
#define _CURVES_H_

#include "griddefs.h"

#include "RXCurve.h"

//EXTERN_C int Create_Curve(VECTOR*pxin,int cxin,RXCurve **C);
//EXTERN_C int Free_Curve(RXCurve **c);

//EXTERN_C int Find_Curve_Int(RXCurve*,double,double,double*,VECTOR*,VECTOR*,RXCurve*,double,double,double*,VECTOR*,VECTOR*);

//! Find all the interesection beetween 2 RXCurves and return the result in a SlowCut Like format
/*!
returns 0 if no intersection find \n
	    PCC_OK if at least 1 interesection is found  \n
		PCC_PARALLEL if the curves are aparallel  (NOT IMPLEMENTED YET)  \n
p_Crv1, p_Crv2      //Define the 2 curve to intersect
p_ss1, p_ss2, p_nc, //Define the interesections (OUTPUT), p_ss1 position of the interesections on polyline 1, parameter 0<<1 
														, p_ss2 position of the interesections on polyline 2, parameter 0<<1	
														, p_nc number of intersections
*/

#error("dont use curves.h")


//#ifndef PCC_OK
	#define  PCC_OK     		(1)
//#endif 
//#ifndef PCC_IFOUND_OK
	#define  PCC_IFOUND_OK		(1)
//#endif 
//#ifndef PCC_NOT_CONVERGED
	#define  PCC_NOT_CONVERGED	(-1)
//#endif
//#ifndef PCC_OUT_OF_RANGE
	#define  PCC_OUT_OF_RANGE   	(-2)
//#endif 
//#ifndef PCC_PARALLEL
	#define  PCC_PARALLEL		(-4)
//#endif
//#ifndef PCC_SMALL_ANG
	#define  PCC_SMALL_ANG		(-8)
//#endif 

#endif //#ifndef _CURVES_H_


