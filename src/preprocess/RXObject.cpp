// this is the RXObject. It manages relationships between Objects
// it would be neater if it also managed the enode and its children it thats possible


#include "StdAfx.h"
#include <QDebug>

#include "RXEntity.h"

#include "RXRelationDefs.h"

int g_DBGCount=0; // use  to count some breakpoints.

#include "RXObject.h"
#include "RXExpression.h"
int dbgc=0; // count for tabs in debug o/p


RXObject::RXObject():
    m_ObeingKilled(false),
    m_NeedsComputing(0),
    In_Stack(0),
    m_listhaschanged(false),
    m_olistChangeCount(0)
  ,m_ComputeCount(0)
{
    Init();
}

RXObject::RXObject(const RXObject & p_Obj):
    m_ObeingKilled(false),
    m_NeedsComputing(0),
    In_Stack(0),
    m_listhaschanged(false),
    m_olistChangeCount(0)
  ,m_ComputeCount(0)
{
    Init();
    this->operator =(p_Obj);
}

RXObject::~RXObject()
{
    dbgc++;
    if(RXODBG) {
        RXTAB(dbgc);
        wcout << L"enter ~RXObject: "<<this<< L" name=<"<<GetOName().c_str() <<L">, mapsize="<< this->OMapN() <<endl;
    }
    assert(!this->m_OBeingIterated ) ;
    OnDelete();


    while(this->OMapN()) {
        obiter it = m_omap.begin ();
        RXObject* p  = it->second;
        this->UnHookCompletely (p);
    }//while

    if(RXODBG) RXTAB(dbgc);
    if(RXODBG)wcout <<L"leave ~RXObject "<<this<< L" "<< GetOName().c_str() <<L" mapsize="<< this->OMapN() <<endl;
    dbgc--;
}

void RXObject::Init()
{
    m_OBeingIterated=false;
    m_listhaschanged=false;
#ifndef NDEBUG
    m_dbgval= RXO_DBG;
#endif
}



RXObject& RXObject::operator = (const RXObject & p_Obj)
{
    //Make a copy
    Init();
    m_Name= p_Obj.m_Name;
    //	m_IsUpToDate = p_Obj.m_IsUpToDate;
    assert(" RXObject Copy is incomplete"==0);
    return *this;
}//RXObject::RXObject& operator = (const RXObject & p_Obj)


RXSTRING RXObject::GetOName() const
{
    assert(m_dbgval== RXO_DBG);
    return m_Name;
}
void RXObject::SetOName(const RXSTRING& p_Name)
{
    m_Name = p_Name;
}
RXSTRING RXObject::GetExpressionText(const int what, const int index,const int type) const
{
    RXSTRING rv;
    set<RXObject*>objs;  RXExpressionI* exp;RXObject*o;
    objs= FindInMap(what,index,type);   // unless there's a bug objs will always have size 1 and
    assert(objs.size()==1);
    if(objs.size())
    {
        o =*(  objs.begin() );
        exp = dynamic_cast <RXExpressionI*>(o);
        if(exp)
            rv= exp->GetText();
    }
    return rv;
}

RXOKey::RXOKey(){
    assert(0);
}
RXOKey::RXOKey(const int w,const int i,const int t){
    what=w;
    index=i;
    type=t;
}
bool RXOKey::operator < (const RXOKey & p) const{

    if(this->what< p.what ) return true;
    if(this->what> p.what ) return false;
    if(this->index == RX_AUTOINCREMENT)
        return false;
    if(   p.index == RX_AUTOINCREMENT)
        return false;
    return ( this->index< p.index )  ;

}


RXOKey::~RXOKey(){

}



int RXOKey::InvertType(const int  p  ){ 
    int rc=0;
    rc = (p&parent )/parent  *child
            +(p&aunt)/aunt   *niece
            +(p&frog)/frog   *spawn
            +(p&child)/child *parent
            +(p&niece)/niece *aunt
            +(p&spawn)/spawn *frog;
    return rc;

}
int RXOKey::AndType(const int  t1, const int  t2 ){  
    int i1 = (int) t1;
    int i2 = (int) t2;
    int rc = i1&i2;
    return rc ;
}
int RXObject::Kill() {
    this->m_ObeingKilled=true;
    int rc = this->CClear();
    this->m_ObeingKilled=false;
    return rc;
}

int RXObject::CClear() {
    assert(m_dbgval== RXO_DBG);
    return OnTopologyChange();
}
int RXObject::UnResolve(){
    assert(m_dbgval== RXO_DBG);
    if(RXODBG) {RXTAB(dbgc); wcout <<L"enter UnR-solve \t "<<this<<L" "<< GetOName().c_str() <<L" mapsize="<< this->OMapN() <<endl;}

    int rc=this->OnTopologyChange();

    if(RXODBG) { RXTAB(dbgc); wcout << L"leave UnR-solve \t "<<this<<L" "<< GetOName().c_str() <<" Omapsize="<< this->OMapN() <<endl;}
    return rc;
}
// March 2013 problem identified with int RXObject::OnTopologyChange
// in the case where we edit an entity which is spawn, we should detach the entity from its frog
// so that it lives independently.
// BUT - often the frog is a CAD file.
// How are we going to remove the object from the CAD file? We would have to rewrite it;

int RXObject::OnTopologyChange(){
    int rc=0;
    RXObject* p;
    assert(m_dbgval== RXO_DBG);
    if(m_OBeingIterated)  {
        if(RXODBG){
            RXTAB(dbgc); wcout << L"SKIP  RXO:OnTopology  \t "<<this<<L" "<< GetOName().c_str() <<L" Omapsize=" << this->OMapN() <<endl;
        }
        return 0;
    }

    if(!m_omap.size()) return 0;
    int ChangeCount,cc;
    dbgc++;
    if(RXODBG) { RXTAB(dbgc);wcout << L"enter RXO:OnTopology \t"<<this<<L" "<< GetOName().c_str() <<L" Omapsize=" << this->OMapN() <<endl;}
    m_OBeingIterated=true;
    do{
        m_listhaschanged=false;ChangeCount=0;  cc=m_olistChangeCount;
        for (obiter it = m_omap.begin ();it!=m_omap.end() ;++it ){ // changecount-protected
            p  = it->second;
            if(p->m_OBeingIterated) continue; // no point in working on p twice
            if (  (it->first.type) & spawn) { // p is spawn of this
                if(RXODBG) {RXTAB(dbgc); printf("go to delete spawn '%S'\n",p->GetOName ().c_str ());}
                this->UnHookCompletely(p);
                p->OnTopologyChange();
                p->Kill();// NOTE this destroys RXExpressions
                m_listhaschanged=true;  // its wrong that this is needed.
                ChangeCount++;
                rc++;
            }

            else if ((it->first.type )& frog) {  // this is spawn of p. Remove this from p's object map.
                if(RXODBG) {
                    RXTAB(dbgc);
                    wcout<<L"go to CClear Frog '"<<p->GetOName().c_str() <<L"' of "<<this->GetOName().c_str()<<endl;
                }
                this->UnHookCompletely(p);
                rc++;
                this->m_listhaschanged=true; ChangeCount++;
            }
            else if ((it->first.type) & child)
            {
                this->UnHook(it); ChangeCount++;
                g_DBGCount++; // as an aid to setting breakpoints
                rc+=p->CClear();
            }
            if(ChangeCount || cc!=m_olistChangeCount)
                break;
        }//for
    }while(ChangeCount || cc!=m_olistChangeCount);
    m_OBeingIterated=false;
    assert(m_dbgval== RXO_DBG);
    if(RXODBG){
        RXTAB(dbgc); wcout << L"leave RXO:OnTopology \t"<<this<<L" "<<  GetOName().c_str() <<L" mapsize="<< this->OMapN() <<endl;
    }
 //   this->m_Name.append(L"_Killed");
    dbgc--;
    return rc;
}
// there are two ways to do this:
// the first is better if there are few loops in the niece-tree
//  First we compute the aunts recursively,
// then we compute the nieces recursively

// if there are many loops we may be better to go:
// First, compute nieces NON-recursively
// then compute ieces recursively
#ifdef NEVER
int RXObject::Recursive_Solve(const int depth, SAIL *cSail) {
    RXObject* nextentity;
    int cc, flag=0;	// 1 if the entity moved, else 0

    if(!this->NeedsComputing() )
        return(flag);

    if(In_Stack)
        return(flag);
    In_Stack = 1;
    // first compute   all aunts which need it.
    // then compute this
    // then compute recursively all nieces that need it.
    // attention. An object map can change at any time.

    if(0){ // we need this aunt walk to catch any upstream expressions that may have changed.
        m_OBeingIterated=true;            // But there is a nasty side-effect:
        do{
            m_listhaschanged=false; cc=m_olistChangeCount;
            for (obciter it = m_omap.begin (); it !=m_omap.end(); ++it){ // change-protected
                nextentity  = it->second;
                if ((it->first.type)&aunt)
                    nextentity->Recursive_Solve(depth+1,cSail);
                if(m_listhaschanged ||cc!=m_olistChangeCount) break;
            }//for
        }while(m_listhaschanged ||cc!=m_olistChangeCount);
        m_OBeingIterated=false;
    }
    this->m_ComputeCount++;
    flag =  this->Compute();
    this->SetNeedsComputing(0);   // it been done so wait for an aunt to turn it on

    // then compute recursively all nieces that need it. They 'need it' if
    // the compute of this resulted in a change
    // or they already have their needs_computing flag set

    // Speculation: we could save a lot if we consider separately nieces which are leafs (ie have no nieces)
    // and those which have nieces.
    // the logic could be:
    // if 'this' changed (flag !=0) then set NC on all the nieces but we only compute nieces which have nieces

    // if 'this' didnt change (flag==0), that means the preprocessor is converging. That is a good time to compute all the
    // remaining nieces (those nieces which are leafs as well as those with NC flag already set.

    // first non-recursive
    if(1){
        m_OBeingIterated=true;
        do{
            m_listhaschanged=false;  cc=m_olistChangeCount;
            for (obiter it = m_omap.begin (); it !=m_omap.end(); ++it){ // protected
                nextentity=  (it->second );
                if ( ( it->first.type) &niece){
                    if(flag>0)
                    {
                        bool mobi = nextentity->m_OBeingIterated;
                        if(mobi)
                            qDebug()<<" MOBI(1) on " << nextentity->GetQName();
                        nextentity->m_OBeingIterated=false;
                        nextentity->SetNeedsComputing();
                        nextentity->m_OBeingIterated=mobi;
                    }
                    if(nextentity->Compute()>0)
                        nextentity->SetNeedsComputing();
                }
                if(m_listhaschanged||cc!=m_olistChangeCount ) break;
            } //for
        }while(m_listhaschanged ||cc!=m_olistChangeCount );
        m_OBeingIterated=false;
    }

    // now recursive
    m_OBeingIterated=true;
    do{
        m_listhaschanged=false;  cc=m_olistChangeCount;
        for (obiter it = m_omap.begin (); it !=m_omap.end(); ++it){ // protected
            nextentity=  (it->second );
            if ( ( it->first.type) &niece){
                if(flag>0)
                {
                    bool mobi = nextentity->m_OBeingIterated;
                    //                    if(mobi)
                    //                        qDebug()<<" MOBI(2) on " << nextentity->GetQName();
                    nextentity->m_OBeingIterated=false;
                    nextentity->SetNeedsComputing();
                    nextentity->m_OBeingIterated=mobi;
                }
                nextentity->Recursive_Solve(depth+1,cSail);
            }
            if(m_listhaschanged||cc!=m_olistChangeCount ) break;
        } //for
    }while(m_listhaschanged ||cc!=m_olistChangeCount );
    m_OBeingIterated=false;
    In_Stack = 0;
    if(flag==2) flag=0;  // only for the pansail entity
    if(flag==-1) flag=0; // only when a SC fails to compute
    return(flag);
}
#else

int RXObject::Recursive_Solve(const int depth,const SAIL *cSail) { // NOTE: entity overrides this
    RXObject* nextObject;
    set<RXObject*> TheNieces,TheAunts;
    set<RXObject*>::const_iterator cit;
    bool mobi;
    int  flag=0;	// 1 if the entity moved, else 0
    if(!this->NeedsComputing() )
        return(flag);

    if(In_Stack)
        return(flag);
    In_Stack = 1;
    // first compute   all aunts which need it.
    // then compute this
    // then compute recursively all nieces that need it.
    // attention. An object map can change at any time.

    if(0){ // we need this aunt walk to catch any upstream expressions that may have changed.
        TheAunts  = FindInMap(RXO_ANYTYPE,RXO_ANYINDEX,aunt);
        for (cit = TheAunts.begin (); cit !=TheAunts.end(); ++cit){ // change-protected
            nextObject  = *cit;
            nextObject->Recursive_Solve(depth+1,cSail);
        }
        this->SetNeedsComputing(1);  // may have been turned off by an aunt
    }

    this->m_ComputeCount++;
    flag =  this->Compute();
    this->SetNeedsComputing(0);   // it been done so wait for an aunt to turn it on

    // then compute recursively all nieces that need it. They 'need it' if
    // the compute of this resulted in a change
    // or they already have their needs_computing flag set

    // Speculation: we could save a lot if we consider separately nieces which are leafs (ie have no nieces)
    // and those which have nieces.
    // the logic could be:
    // if 'this' changed (flag !=0) then set NC on all the nieces but we only compute nieces which have nieces

    // if 'this' didnt change (flag==0), that means the preprocessor is converging. That is a good time to compute all the
    // remaining nieces (those nieces which are leafs as well as those with NC flag already set.

    // first non-recursive
    TheNieces  = FindInMap(RXO_ANYTYPE,RXO_ANYINDEX,niece);
    if(0){

        for (cit = TheNieces.begin (); cit !=TheNieces.end(); ++cit){ // change-protected
            nextObject  = *cit;
            if(flag>0)
            {
                mobi = nextObject->m_OBeingIterated;
                if(mobi)
                    qDebug()<<" MOBI(1) on " << nextObject->GetQName();
                nextObject->m_OBeingIterated=false;
                nextObject->SetNeedsComputing();
                nextObject->m_OBeingIterated=mobi;
            }
            if(nextObject->Compute()>0)
                nextObject->SetNeedsComputing();
        }
    }

    // now recursive

    //   TheNieces  = FindInMap(RXO_ANYTYPE,RXO_ANYINDEX,niece);
    for (cit = TheNieces.begin (); cit !=TheNieces.end(); ++cit){ // change-protected
        nextObject  = *cit;
        if(flag>0)
        {
            mobi = nextObject->m_OBeingIterated;
            if(mobi)  qDebug()<<" MOBI(2) on " << nextObject->GetQName();
            nextObject->m_OBeingIterated=false;
            nextObject->SetNeedsComputing();
            nextObject->m_OBeingIterated=mobi;
        }
        nextObject->Recursive_Solve(depth+1,cSail);
    } //for

    In_Stack = 0;
    if(flag==2) flag=0; // only for the pansail entity
    if(flag==-1) flag=0; // only when a SC fails to compute
    return(flag);
}

#endif
void  RXObject::SetNeedsComputing(const int i){
    if(i) OnValueChange();
    else
        m_NeedsComputing=0;
}
/*   RXObject::OnValueChange, being recursive, is slow but safe.
 *   RXExpression::OnValueChange needs to be called recursively to ensure that the entities that depend on it get their NC flags set
 *   that is because the Recursive_Solve seeds on entities not on expressions.
 *   as a speed optimisation, RXEntity::SetNeedsComputing  overrides this with a non-recursive version.
 *   That's OK because R-Solve will recur into any niece Expressions.
 **/
int RXObject::OnValueChange(){ // called after 'this' has had one of its numbers changed. 
    assert(m_dbgval== RXO_DBG);
    this->m_NeedsComputing=1;

    if(m_OBeingIterated) return 0;
    if(RXODBG){
        RXTAB(dbgc); wcout << L"OnVCh \t"<<this<<L" "<< GetOName().c_str() <<L" mapsize=" << this->OMapN() <<endl;
    }
    int  rc=0;
    m_OBeingIterated=true;
    obciter ee = m_omap.end();
    do{
        m_listhaschanged=false;

        for (obciter it = m_omap.begin (); it !=ee; ++it){ //safe - doesnt need change-protection

            if ( ( it->first.type) &niece)  {
                RXObject* p  = it->second;
                //    if(!p->NeedsComputing()){  // 2 march 2013 tried this condition because vtune says this call is v slow
                dbgc++; rc++;
                p->SetNeedsComputing();
                dbgc--;
                //    }
            }
            assert(!m_listhaschanged); if(m_listhaschanged) break;
        }//for
    }while(m_listhaschanged);
    m_OBeingIterated=false;
    assert(m_dbgval== RXO_DBG);
    return rc;
}
int RXObject::OnDelete(){

    /*  anything looking at this needs to be unhooked*/
    return OnTopologyChange();
}

int RXObject::PrintOMap(FILE *fp,const QString &space, const int pwhat, const bool recur) const  {
    int rc=0, c=0;
    if( this->m_OBeingIterated) return 0;
    assert(!m_OBeingIterated  );
    //  this->m_OBeingIterated=true; //THREADSAFETY maybe we use equal_range
    assert(m_dbgval== RXO_DBG);
    for (obciter it = m_omap.begin (); it !=m_omap.end(); ++it){//safe
        if(it==m_omap.begin()) {
            rc+=fprintf(fp,"%sObject map of `%S` RXO%p\n",qPrintable(space), GetOName().c_str(),this);
            rc+=fprintf(fp,"%s i\t                  Name   \t(enttype) \ttype\t what \t index \tPAF CNS\n",qPrintable(space) );
        }
        RXObject* p  = it->second;
        rc+=fprintf(fp,"%s \t%d \t %12S\t  ",qPrintable(space), c++, p->GetOName().c_str());
        if(RXEntity_p e=dynamic_cast<RXEntity_p>(p)) fprintf(fp,"%17s \t",e->type());
        else fprintf(fp,"RXO%p \t",p);
        rc+=fprintf(fp," %d  \t %d \t   %d  \t", it->first.type,it->first.what,it->first.index);
        if((it->first.type) &parent )	rc+=fprintf(fp,"p");   else rc+=fprintf(fp," ");
        if((it->first.type )&aunt   )	rc+=fprintf(fp,"a");   else rc+=fprintf(fp," ");
        if((it->first.type )&frog   )	rc+=fprintf(fp,"f ");  else rc+=fprintf(fp,"  ");
        if((it->first.type) &child  )	rc+=fprintf(fp,"c");   else rc+=fprintf(fp," ");
        if((it->first.type) &niece  )	rc+=fprintf(fp,"n");   else rc+=fprintf(fp," ");
        if((it->first.type) &spawn  )	rc+=fprintf(fp,"s\n"); else rc+=fprintf(fp," \n");
        if(recur &&((it->first.type) & pwhat ) && space.length()<50)
        {
            fprintf(fp,"%s recur from %S into (%d)  %S\n",
                    qPrintable(space),this->GetOName().c_str(),
                    pwhat, p->GetOName().c_str());
            p->PrintOMap (fp,space+ " . ",pwhat,recur); fflush(fp);
        }
    }//for

    fflush(fp);
    return rc;
}
QString RXObject::Descriptor()const
{
    QString q("O_"); q+= QString::fromStdWString(this->GetOName());
    return q;

}
int RXObject::PrintOMapAsGraph(wostream &s, const int what, const bool recur)
{
    int rc=0;
    if( this->m_OBeingIterated) return 0;
    RXExpressionI*ii = dynamic_cast<RXExpressionI*>( this); // Yuch
    if(ii)
        if(ii->AccessFlags()&RXE_PRIVATE)
            return 0;
    assert(!m_OBeingIterated  );

    QString d= this->Descriptor();
    QString line;
    for (obciter it = m_omap.begin (); it !=m_omap.end(); ++it){//safe
        if((it->first.type) &what  )
        {
            RXObject* p  = it->second;
            ii = dynamic_cast<RXExpressionI*>(p);
            if(ii)
                if(ii->AccessFlags()&RXE_PRIVATE)
                    continue;
            line = "Style[Labeled[\""+d+"\" \\[DirectedEdge] \""+p->Descriptor() + "\" ,None],Blue],";
            s<<line.toStdWString()<<endl;
            rc++;
            if(recur)
            {
                this->m_OBeingIterated=1;
                rc+= p->PrintOMapAsGraph(s,what,recur);
                this->m_OBeingIterated=0;
            }
        }
    }//for
    return rc;
}

int RXObject::UnHook(const obiter &it){
    int rc=0;
    // #pragma omp critical (rxobjectOmap)
    {
        const RXOKey &k = it->first ;
        class RXObject*p = it->second;
        pair<obiter,obiter> ret;
        ret= p->m_omap.equal_range(RXOKey(k.what,k.index,RXOKey::InvertType(k.type )));

        obiter it2;
        for(it2=ret.first;it2!=ret.second;++it2){
            if(it2->second==this){
                p->m_omap.erase(it2); p->m_listhaschanged=true; m_olistChangeCount++;
                rc++;break;
            }
        }
        assert(rc);
        this->m_omap.erase(it); this->m_listhaschanged=true;m_olistChangeCount++;
    }
    return rc;
}

bool RXObject::FindInMap(RXObject*ooo, const int type)const
{ // finds the first. There may be more than one entry.
    bool rc = false;
    // #pragma omp critical (rxobjectOmap)
    {
        assert(m_dbgval== RXO_DBG);
        if(!ooo){ // ooo not specified. return true if any entries have 'type'
            for (obciter it = m_omap.begin (); it !=m_omap.end(); ++it){ //safe
                RXObject* p  = it->second;
                if (  (it->first.type) &type)
                {
                    rc=  true;
                    break;
                }
            }//for
        }
        else { // ooo has a value. return true if it is in the map with 'type'
            for (obciter it = m_omap.begin (); it !=m_omap.end(); ++it){ //safe
                RXObject* p  = it->second;
                if(p!=ooo) continue;
                if ( ( it->first.type )&type)
                {
                    rc=  true;
                    break;
                }
            }//for
        }
    }
    return rc;
}


// FindInMap. returns a subset of the objects this object is connected to.
// there are two useages:

// if the parameter 'what' is RX_ANY
// it returns all objects matching 'type',  ignoring 'index' , regardless of their 'what'
// example:  
//set<RXObject*> myset= FindInMap(RXO_ANY,(garbage),spawn); will return all spawn objects

// if 'what' is specific (ie defined in  RXRelationDefs.h) 
// it returns all the objects matching 'what' AND 'index'. It ignores 'type'
// example: 
//		FindInMap(RXO_N1_OF_SC, 0); will retrieve a SCs N1 (because you know it has inde 0) 
// or
// to retrieve all the layers a panel knows about, get the panel to call
// FindInMap(RXO_LYR_OF_PANEL,RX_ANY);
// To retrieve the Nth slave node of a panel, go
//// FindInMap(RXO_SITE_OF_PANEL,N);


set<RXObject*> RXObject::FindInMap(const int what, const int index,const int type) const{
    set<RXObject*> rc;

    // first form: retrieve all matching 'type'
    if(what==RXO_ANYTYPE){
        assert(type != RXO_NOTYPE);
        obciter ie =m_omap.end();
        {
            for (obciter it = m_omap.begin (); it !=ie; ++it){ //safe
                if(!((it->first.type)&type))
                    continue;
                rc.insert(it->second );
            }
        }
        return rc;
    }
    // 2nd form: retrieve all matching 'what'
    if(index==RXO_ANYINDEX) // the default for index
    {
        // #pragma omp critical (rxobjectOmap)
        {
            obciter lwr =   this->m_omap.lower_bound(RXOKey(what-1,RXO_MAXINDEX,type));
            obciter upr =   this->m_omap.upper_bound(RXOKey(what ,RXO_MAXINDEX,type));
            for(obciter it=lwr ;it!=upr;++it)
            {
                const RXOKey &k = it->first;
                if(k.what ==what)
                    rc.insert(it->second );
                else {
                    qDebug()<< "k.what = "<<k.what<<"isnt what=" <<what;
                    assert(0);
                }
            }
        }
        return rc;
    }

    // 3rd form: retrieve specific (what,index) pair
    // #pragma omp critical (rxobjectOmap)
    {
        pair<obciter,obciter> r =this->m_omap.equal_range(RXOKey(what,index,type));
        for(obciter it=r.first ;it!=r.second;++it)
            rc.insert(it->second );
    }

    return rc;
}

int RXObject::SetRelationOf(RXObject* p_o,const int type,const int what,const int index){
    int rev,rc;

    if(type==notype){ // not sure if this SB permanent. Its a work-around during panel-layer-paside development
        //if(RXODBG) 		wcout<<L"skip setting relation "<<this->GetOName()<<L" and "<<p_o->GetOName()<<endl;
        return 0;
    }
    rc= __SetRelation(p_o,type,what,index);
    rev =    RXOKey::InvertType(type);
    rc += p_o->__SetRelation(this,rev,what,index);

    assert( RXOKey::InvertType(rev) ==type);

    return rc;
}

int RXObject::__SetRelation(RXObject* p_o,const int type,const int what,const int pindex){
    int rc=1;
    // THIS IS only OK for entities.  we AND the types if we find an object with same key and NAME

    /*
If no {what,index } exists, we insert a record of p_o in this' map
if a {what,index } does exist, then it is
A)	pointing to the same ent. we And the keys
B)	Pointing to a different ent.  we insert a new record.
*/
    assert(m_dbgval== RXO_DBG);
    int iret;
    int sindex = pindex;
    if(sindex==RX_AUTOINCREMENT) sindex=0;
    obiter it,ita,itb;
    iret = m_omap.count(RXOKey(what,sindex));
    if(!iret)				// didnt find, so insert a new.
    {
        m_omap.insert ( orectype(RXOKey(what,sindex,type),p_o ) ); m_olistChangeCount++;
        m_listhaschanged=true;
        return rc;
    }
    if(pindex==(RX_AUTOINCREMENT)) // IN WORK. the idea is to get
    {
        iret = m_omap.count(RXOKey(what,RX_AUTOINCREMENT));
        int newindex = iret;
        m_omap.insert ( orectype(RXOKey(what,newindex,type),p_o ) ); m_olistChangeCount++;
        m_listhaschanged=true;
        return rc;
    }

    // found one. does it point to p_o?? This is SLOW. try to use RX_AUTOINCREMENT where possible
    pair<obiter,obiter> ret;
    ret = m_omap.equal_range(RXOKey(what,pindex));		// 1) search for an entry with same {what,index,ent}
    int found=0;
    ita= ret.first; itb = ret.second;
    for (it=ita; it!=itb; ++it){  // this is the slow line
        if(it->second==p_o) { found=1;break;}
    }
    // if there is an entry with the same entity....

    if(found) {		 // erase and re-insert  with ANDED type
        int t = RXOKey::AndType(it->first.type,type);
        if(t != type){
            // There's an error here:
            // it manifests itself when an RXExpression with a bad text  contains a symbol
            // that exists in the RXExp's owner. (eg when we make a recurring reference)
            // in the call from MTVariableI* MyVarFactory::create,
            // This results in uneven hooking and assert failure on CClear.
            // The  BIG QUESTION:  WHY  do we do this here?
            // and if we must do it,?
            RXObject*ooo = it->second;
            if(ooo)
                qDebug()<<"how to treat the other side of old Relation??: "<<ooo->GetQName() <<" and ", this->GetQName();
            else
                qDebug()<<"how to treat the other side of old Relation??: "<<  this->GetQName();
            m_omap.erase(it);
            m_omap.insert ( orectype(RXOKey(what,pindex,t),p_o ) ); m_olistChangeCount++;
            m_listhaschanged=true;
        }
        else {
            assert(it->first.what==what && it->first.index == pindex );
        }
    }
    else{// the entry has a different entity, so just insert
        m_omap.insert ( orectype(RXOKey(what,pindex,type),p_o ) );m_olistChangeCount++;
        m_listhaschanged=true;
    }


    return rc;
}

RXObject* RXObject::GetOneRelativeByIndex( const int what,const int index) const
{
    RXObject* rc=0;
    // #pragma omp critical (rxobjectOmap)
    {
        assert(m_dbgval== RXO_DBG);
        obciter it= m_omap.find(RXOKey(what,index));
        if(it!= m_omap.end()){
            rc=it->second;	//assert(m_omap.count(RXOKey(what,index)) <=1);
        }
    }
    return rc;
}
// the omap comparitor doesnt look at type - only what and index
// so to return something with a given type (ie aunt) we have to first find
// all with (what,anyIndex) and then trawl  for type

RXObject* RXObject::GetOneRelativeByLien( const int what,const int type) const
{
    RXObject* rc=0;
    assert(m_dbgval== RXO_DBG);
    // #pragma omp critical (rxobjectOmap)
    {
        obciter lwr =   this->m_omap.lower_bound(RXOKey(what-1,RXO_MAXINDEX,type));
        obciter upr =   this->m_omap.upper_bound(RXOKey(what ,RXO_MAXINDEX,type));
        for(obciter it=lwr ;it!=upr;++it)
        {
            const RXOKey &k = it->first;
            if(k.what !=what){
                qDebug()<< "k.what = "<<k.what<<"isnt what=" <<what;
                assert(0);
            }
            if(k.type&type )
                return rc= it->second;
        }
    }
    return rc;
}

int RXObject::Compute(){ cout<< " virtual RXObject::Compute"<<endl; return 0;}
int RXObject::Resolve(){ cout<< " virtual RXObject::Resolve "<<endl; return 0;}

int RXObject::RemoveFromMap(class RXObject*o ){ // horribly slow
    int rc=0;
    {
        int changing=1;this->m_listhaschanged=false;
        while(changing) {
            changing=0;
            obiter itend = m_omap.end();
            for (obiter it = m_omap.begin (); it !=itend; ++it){ // safe- doesnt need protecting
                RXObject* p  = it->second;
                if(p==o) {
                    if(RXODBG){
                        RXTAB(dbgc);printf("( RXObject::RemoveFromMap)remove '%S' from oMap of '%S'\n", it->second->GetOName().c_str(),this->GetOName().c_str());
                    }
                    m_omap.erase(it); this->m_listhaschanged=true; rc++; m_olistChangeCount++;
                    changing++; break;
                }
            }
        }
        assert(m_dbgval== RXO_DBG);
    }
    return rc;
}
int RXObject::UnHookCompletely(class RXObject*o){ 
    int rc=0;
    while (this->RemoveFromMap(o)  ) rc++;
    while (o->RemoveFromMap(this)  ) rc++;
    assert(!(rc&1));
    return rc;
}
int RXObject::UnHook(class RXObject*o, const int what){
    obiter it1,it2, ie1 =m_omap.end();

    for (it1 = m_omap.begin (); it1 !=ie1; ++it1){ //safe
        if(it1->first.what==what)
        {
            m_omap.erase(it1);
            obiter ie2 =o->m_omap.end();
            for (it2 = o->m_omap.begin (); it2 !=ie2; ++it2){ //safe
                if(it2->first.what==what)
                {
                    o->m_omap.erase(it2);
                    return 2;
                }
            }
            return 1;
        }
    }
    return 0;
}

