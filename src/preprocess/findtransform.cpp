/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 * 	4 June 2004 check for determinant before inverting
 *           12/1/95     some printfs commented out PH
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h"
#include "RelaxWorld.h"

#include "findtransform.h"

/* changed 26 April 94 A K Molyneaux. 
   Added alpha, beta, gamma as rotations about x,y,z of the matrix.
 */

 bool FrameFromPoints(const ON_3dPoint &s0,const ON_3dPoint &s1,const ON_3dPoint &s2,
							  ON_3dPoint&   P0,  // initial frame center
							  ON_3dVector&  X0, // initial frame X
							  ON_3dVector&  Y0, // initial frame Y
							  ON_3dVector&  Z0)
 {
	 bool bad=false;
	  P0= s0;
	  X0 = s1-s0; if(! X0.Unitize()) bad=true;
	Z0 = ON_CrossProduct(X0,(s2-s0)); 
	if(! Z0.Unitize()) bad=true;
	Y0 = ON_CrossProduct(Z0,X0);  
	if(bad) {
		g_World->OutputToClient(" Degenerate axis system ",1);
		X0=ON_3dVector(1.,0.,0.);
		Y0=ON_3dVector(0.,1.,0.);
		Z0=ON_3dVector(0.,0.,1.);
		return false;}

	 return true;
 }

ON_Xform FindTransform(const ON_3dPoint &s0,const ON_3dPoint &s1,const ON_3dPoint &s2,
				const ON_3dPoint &v0,const ON_3dPoint &v1,const ON_3dPoint &v2,
				ON_Xform &Mt,float Alpha,float Beta,float Gamma)
{
	ON_3dPoint    P0;  // initial frame center
	ON_3dVector   X0; // initial frame X
	ON_3dVector   Y0; // initial frame Y
	ON_3dVector   Z0; // initial frame Z
	ON_3dPoint    P1;  // final frame center
	ON_3dVector   X1;; // final frame X
	ON_3dVector   Y1;; // final frame Y
	ON_3dVector   Z1;  // final frame Z

	FrameFromPoints(s0,s1,s2,P0,X0,Y0,Z0);
	FrameFromPoints(v0,v1,v2,P1,X1,Y1,Z1);

 	Mt.Rotation(P1,X1,Y1,Z1,P0,X0,Y0,Z0);

/* add effect of the heel angle */
	ApplyRotation(Mt,Alpha,Beta,Gamma);
	return(Mt);

}
 int ApplyRotation(ON_Xform &xx,float alpha, float beta, float Gamma) 
 {
// premultiply xx by the XForm due to the 3 rotations
	ON_Xform a; a.Rotation(ON_DEGREES_TO_RADIANS*alpha,ON_3dVector(1,0,0),ON_3dPoint(0,0,0));
	xx=a*xx;
	return 1;
 }
