#include "StdAfx.h"
#include "RXSail.h"
#include "RelaxWorld.h"
#include <stack>
#include <QtCore>
#include "RXEntity.h"
#include "RX_FESite.h"
#include "RX_FEEdge.h"
#include "RX_FETri3.h"
#include "RX_FEString.h"
#include "RXSRelSite.h"
#include "RXQuantity.h"
#include "rxvectorfield.h"
#include "rxpshell.h"
#include "etypes.h"
#include "RXRelationDefs.h"
#include "entities.h"
#include "f90_to_c.h"
#include "getfilename.h"
#include "rxqtdialogs.h"
#include "global_declarations.h"
#include "rximportentity.h"

//  for Laurent we have ( in fixed-8 format)
// GRID RBE2  PBAR  CBAR  MAT1 CONROD BLSEG/BCONP  MAT2  PSHELL CTRIA3

/////////////// Special identifiers for Sailpack NAS file heuristics

#define SITEQSTRING(i) QString("%1_nas_%2").arg(QString::fromStdWString(g_theIE->Esail->GetName()).at(0)).arg(i,3,10,QChar('0')) // 4 or 5 for fieldwidth should be OK
#define SITEQSTRING2(s,i) QString("%1_nas_%2").arg(s.at(0)).arg(i,3,10,QChar('0')) // 4 or 5 for fieldwidth should be OK


#define RXSAILPACKPOCKETENDID ">PocketEnd" // disable pockets as a test
#define RXSAILPACKPOCKETID ">Pocket"

#define RXSAILPACKBATTENENDID ">BattenEnd"
#define RXSAILPACKBATTENID ">Batten"
#define RXSAILPACKLUFFID  ">Luff" // for end with
#define RXSAILPACKLEECHID ">Leech" // for end with
#define RXSAILPACKFOOTID  ">Foot"// for end with
#define RXSAILPACKHEADID  ">Head"// for end with

// keyword is 'import'
class RXImportEntity* g_theIE;
RXImportEntity::RXImportEntity():
    m_rximportFlags(0)
{
    g_theIE=this;
}
RXImportEntity::RXImportEntity(class RXSail *sail)
    : RXEntity(sail),
      m_rximportFlags(0)
{
    g_theIE=this;
}

RXImportEntity::~RXImportEntity()
{
    g_theIE=0;
}
int RXImportEntity::CClear()
{
    return 0;
}

QStringList RXImportEntity::ChopString(const QString lLine,const int s)
{
    QStringList wds;
    int j,n = lLine.size();
    for (j=0;j<n;j+=s)
        wds<<lLine.mid(j,s).simplified();
    return wds;
}
class RXImportEntity::NastranBLSEG RXImportEntity::FindBLSEGAcrossModels(const int id,QString&sailtype)
{
    class RXImportEntity::NastranBLSEG  rv;

    if(this->m_blsegs.find(id) != this->m_blsegs.end()) {
        class RXImportEntity::NastranBLSEG  &b=this->m_blsegs[id];
        rv=b;
        sailtype = QString::fromStdString( this->Esail->GetType());
    }
    else  // look in all models for this blseg id
    {
        vector<RXSail*> ss;
        RXSail *s;
        RXImportEntity * ie;
        ss= g_World->ListModels(RXSAIL_ALL);
        int i;

        for (i=0;i<ss.size(); i++) {
            s = ss[i];
            ie = dynamic_cast<RXImportEntity *>(s->GetFirst("import") ) ;
            if(!ie)
                continue;
            if(ie->m_blsegs.find(id) != ie->m_blsegs.end()) {
                class RXImportEntity::NastranBLSEG  &b=ie->m_blsegs[id];
                rv=b;
                sailtype = QString::fromStdString(s->GetType());
                return rv;
            }
        }
    }
    return rv;
}
class RXImportEntity::NastranCBAR RXImportEntity::Find_CBAR( const int n1, const int n2)
{
    class RXImportEntity::NastranCBAR  rv;
    std::vector<RXImportEntity::NastranCBAR> ::iterator  it;
    for(it= m_cbars.begin(); it!=m_cbars.end(); ++it)
    {

        if( it->m_cbnodes[0] == n1 && it->m_cbnodes[1] == n2 )
            return *it;
        if( it->m_cbnodes[0] == n2 &&  it->m_cbnodes[1] == n1)
            return *it;
    }

    return rv;
}

int RXImportEntity::Dump(FILE *fp) const
{
    fprintf(fp,"\t\t Nastran bulk data file: %s\n" ,qPrintable(this->m_filename) );
    fprintf(fp,"\t\t CQUAD8 count = %u\n", (unsigned int) m_cquad8.size()  );

    fprintf(fp,"\t\t PSHELL count = %u\n", (unsigned int)  m_pshells.size()  );
    fprintf(fp,"\t\t MAT1   count = %u\n",  (unsigned int)   m_materials.size()  );
    fprintf(fp, "\t\t partial import only - there may be many skipped objects \n ");
    std::map<int,RXImportEntity::NastranMAT>::const_iterator itm;
    for( itm=m_materials.begin(); itm!=m_materials.end();++itm)
    {
        fprintf(fp,"\t\t %d %s\n", itm->first, qPrintable(itm->second.m_line));
        fprintf(fp,"\t\t \t%s\n",  qPrintable(itm->second.Summary()));
    }
    std::map<int,RXImportEntity::NastranPSHELL >::const_iterator itp;
    for( itp=m_pshells .begin(); itp!=m_pshells .end();++itp)
    {
        fprintf(fp,"\t\t %d %s\n", itp->first, qPrintable( itp->second.m_line));
    }
    fprintf(fp, "\t\t and many other errors. \n ");
    fprintf(fp, "\t\t The parser does NOT handle blank fields and continuation lines correctly\n\n");
    fprintf(fp, "\t\t and (YUCH) it assumes that the node numbers are consecutive from 1\n\n");
    return 0;
}
int RXImportEntity::ResolveObjects(void)
{
    int rc=0;
    std::map<int,RXImportEntity::NastranBLSEG>::iterator its; // strings of nodes
    std::vector<RXImportEntity::NastranBCONP>::iterator itc; // connectors

    for (its = m_blsegs.begin();its!=m_blsegs.end(); ++its)
        rc+= its->second.Resolve();
    for (itc = m_bcomp.begin();itc!=m_bcomp.end(); ++itc)
        rc+= itc->Resolve();
    return rc;
}

int RXImportEntity::FindOtherEndOfBCONP(const int n)
{
    int rc=-1;
    std::vector<RXImportEntity::NastranBCONP>::iterator itc; // connectors
    for (itc = m_bcomp.begin();itc!=m_bcomp.end(); ++itc)
    {
        NastranBCONP &b = *itc;
        int k=2;
        int g2 = b.word(k++).toInt();
        int g1 = b.word(k++).toInt();
        if(g2==n  ) return g1;
        if(g1==n) return g2;
    }
    return rc;
}


int RXImportEntity::Compute(void) // the operations to do at model Compute stage rather than at resolve stage
{
    cout<<" creating els from NAS: " <<  this->m_cquad8.size()<< " CQUAD8s and "<< m_ctria3.size()  << " TRI3s" <<endl;

    std::vector<RXImportEntity::NastranCQUAD8>::iterator it8;
    class RX_FETri3 * t;
    class RX_FEedge *fe , *e1,*e2,*e3;
    class RX_FESite *p1,*p2,*p3; p1=p2=p3=0;
    RXEntity_p  s1, s2, s3; s1=s2=s3=0;
    std::map<pair<int,int>,class RX_FEedge *> edges;
    std::map<pair<int,int>,class RX_FEedge *>::iterator ite;
    int j, k,n1,n2,n3 ;
    pair<int,int> ep;

    for(it8=m_cquad8.begin(); it8!=m_cquad8.end(); ++it8)
    {
        int e[9]= {0,4,1,5,2,6,3,7,0};
        int nd[18] = {0,4,7,4,1,5,2,6,5,6,3,7,4,6,7,6,4,5} ;
        int ie[6] = {4,5,6,7,4,6}  ;
        NastranCQUAD8 & q = *it8;
        for (k=0;k<8;k++){ p1=p2=p3=0;
            n1 = q.nodes[e[k]]; n2 = q.nodes[e[k+1]];
            ep = make_pair(n1,n2);
            if(edges.find(ep)==edges.end())
                if(edges.find(make_pair(n2,n1)) == edges.end()){
                    fe = new RX_FEedge(this->Esail);
                    edges[ep] = fe;
                    p1 = this->Esail->GetSiteByNumber(n1);
                    p2 = this->Esail->GetSiteByNumber(n2); assert(p1&&p2);
                    fe->SetLength( p1->DistanceTo (*p2)   ); //
                    fe->SetNodes(p1,p2);
                }
        }
        // now the internals
        for(k=0;k<5;k++){p1=p2=p3=0;
            n1 = q.nodes[ie[k]]; n2 = q.nodes[ie[k+1]];
            ep = make_pair(n1,n2);
            if(edges.find(ep)==edges.end())
                if(edges.find(make_pair(n2,n1)) == edges.end()){
                    fe = new RX_FEedge(this->Esail);
                    edges[ep] = fe;
                    p1 = this->Esail->GetSiteByNumber(n1);
                    p2 = this->Esail->GetSiteByNumber(n2);assert(p1&&p2);
                    fe->SetLength( p1->DistanceTo (*p2)   ); //
                    fe->SetNodes(p1,p2);
                }
        }
    }

    for(it8=m_cquad8.begin(); it8!=m_cquad8.end(); ++it8)
    { // one quad makes 6 els
        int e[9]= {0,4,1,5,2,6,3,7,0};
        int nd[18] = {0,4,7,4,1,5,2,6,5,6,3,7,4,6,7,6,4,5} ;
        int ie[6] = {4,5,6,7,4,6}  ;
        NastranCQUAD8 & q = *it8;
        for(k=0;k<6;k++) { p1=p2=p3=0;
            n1 = q.nodes[nd[3*k]];
            n2 = q.nodes[nd[3*k+1]];
            n3 = q.nodes[nd[3*k+2]];
            s1 = this->Esail->Entity_Exists( qPrintable(SITEQSTRING(n1) ), "site"  );
            s2 = this->Esail->Entity_Exists( qPrintable(SITEQSTRING(n2) ), "site"  );
            s3 = this->Esail->Entity_Exists( qPrintable(SITEQSTRING(n3) ), "site"  );

            p1 = dynamic_cast<class RX_FESite *>(s1); p2 = dynamic_cast<class RX_FESite *>(s2);p3 = dynamic_cast<class RX_FESite *>(s3);

            t = new RX_FETri3(this->Esail);
            t->m_panel =0;
            t->SetRefAngle(0.0);
            t->SetNode(0,p1);     t->SetNode(1,p2);     t->SetNode(2,p3);

            ite = edges.find(make_pair(n1,n2));
            if(ite ==edges.end())
                ite= edges.find(make_pair(n2,n1)); assert(ite !=edges.end());
            e1 = ite->second;

            ite = edges.find(make_pair(n2,n3));
            if(ite ==edges.end())
                ite= edges.find(make_pair(n3,n2)); assert(ite !=edges.end());
            e2 = ite->second;

            ite = edges.find(make_pair(n3,n1));
            if(ite ==edges.end())
                ite= edges.find(make_pair(n1,n3)); assert(ite !=edges.end());
            e3 = ite->second;

            t->m_iEd[0]=  e1->GetN();  t->m_iEd[1]=  e2->GetN(); t->m_iEd[2]=  e3->GetN();
            NastranPSHELL &ps = q.m_ps;
            NastranMAT &mat = m_materials[ ps.mMatid] ;
            for(j=0;j<9;j++)
                t->mx[j] =  mat.d[j] * ps.thk;

            memset(t->prestress,0,sizeof(double)*3);
            t->m_TriArea = t->Area();
            t->creaseable=1;
            t->SetPressure(q.m_dp);//by chance our sign convention is the same as abaqus
            t->SetProperties();
        }
    }

    // the tria3s
    std::vector<RXImportEntity::NastranCTRIA3>::iterator it3;
    for(it3=m_ctria3.begin(); it3!=m_ctria3.end(); ++it3)
    {
        int e[4]= {0,1,2,0};
        NastranCTRIA3 &q = *it3;
        for (k=0;k<3;k++)
        {
            p1=p2=p3=0;
            n1 = q.nodes[e[k]]; n2 = q.nodes[e[k+1]];
            ep = make_pair(n1,n2);
            if(edges.find(ep)==edges.end())
                if(edges.find(make_pair(n2,n1)) == edges.end()){
                    fe = new RX_FEedge(this->Esail);
                    edges[ep] = fe;
                    p1 = this->Esail->GetSiteByNumber(n1);
                    p2 = this->Esail->GetSiteByNumber(n2); assert(p1&&p2);
                    fe->SetLength( p1->DistanceTo (*p2)   ); //
                    fe->SetNodes(p1,p2);
                }
        }
    }

    for(it3=m_ctria3.begin(); it3!=m_ctria3.end(); ++it3)
    {
        NastranCTRIA3 & q = *it3;
        p1=p2=p3=0;
        n1 = q.nodes[0];
        n2 = q.nodes[1];
        n3 = q.nodes[2];
        s1 = this->Esail->GetKeyNoAlias(L"site", SITEQSTRING(n1).toStdWString() );
        s2 = this->Esail->GetKeyNoAlias(L"site", SITEQSTRING(n2).toStdWString());
        s3 = this->Esail->GetKeyNoAlias(L"site", SITEQSTRING(n3).toStdWString());

        p1 = dynamic_cast<class RX_FESite *>(s1); p2 = dynamic_cast<class RX_FESite *>(s2);p3 = dynamic_cast<class RX_FESite *>(s3);

        t = new RX_FETri3(this->Esail);
        q.m_ps.m_pan->AddTri3(t);

        t->SetNode(0,p1);     t->SetNode(1,p2);     t->SetNode(2,p3);

        ite = edges.find(make_pair(n1,n2));
        if(ite ==edges.end())
            ite= edges.find(make_pair(n2,n1)); assert(ite !=edges.end());
        e1 = ite->second;

        ite = edges.find(make_pair(n2,n3));
        if(ite ==edges.end())
            ite= edges.find(make_pair(n3,n2)); assert(ite !=edges.end());
        e2 = ite->second;

        ite = edges.find(make_pair(n3,n1));
        if(ite ==edges.end())
            ite= edges.find(make_pair(n1,n3)); assert(ite !=edges.end());
        e3 = ite->second;

        t->m_iEd[0]=  e1->GetN();  t->m_iEd[1]=  e2->GetN(); t->m_iEd[2]=  e3->GetN();
        NastranPSHELL &ps = q.m_ps; int mid =ps.mMatid ;
        NastranMAT &mat = m_materials[mid ] ;
        for(j=0;j<9;j++)
            t->mx[j] =  mat.d[j] * ps.thk;

        memset(t->prestress,0,sizeof(double)*3);
        t->m_TriArea = t->Area();
        t->creaseable=1;
        t->SetPressure(q.m_dp);//by chance our sign convention is the same as abaqus
        t->SetProperties(); // GARBAGE
        t->SetRefAngle(q.m_th);
    } // ctria3
    // now     ;
    {    // beam properties
        std::map<int,RXImportEntity::NastranPBAR> ::iterator it, itb=m_pbars.begin(),ite =  m_pbars.end();
        for(it=itb;it!=ite;++it)
            (*it).second.Compute(this);
    }

    {    // bar elements
        std::vector<RXImportEntity::NastranCONROD>::iterator it, itb=m_conrods.begin(),ite =  m_conrods.end();
        for(it=itb;it!=ite;++it)
            (*it).Compute(this);
    }

    // beam elements
    //    {
    //        std::vector<RXImportEntity::NastranCBAR>::iterator it, itb=m_cbars.begin(),ite =  m_cbars.end();
    //        for(it=itb;it!=ite;++it)
    //            (*it).Compute(this);
    //    }

    return 0;    // connectors dont need computing
}

int RXImportEntity::ReWriteLine(void) // type : shorfilename (relative to scriptfile)
{

    QString s =  this->Esail->RelativeFileName(this->m_filename );
    QString NewLine = QString(type()) + QString(RXENTITYSEPS ) + QString(name()) + QString(RXENTITYSEPS ) +s;
    if(!mAtts.isEmpty()) NewLine += QString(RXENTITYSEPS ) +mAtts;

    this->SetLine(qPrintable(NewLine));

    return 0;
}

int RXImportEntity::Finish(void)
{
    return 0;
}
int RXImportEntity::Resolve(void)
{
    int rc=0;
    // import : name : filename : [atts] :[prompt]
    // get the filename if we can
    // if its 'nas' or 'ndf'
    QString qline(this->GetLine());
    QStringList wds=rxParseLine(qline,RXENTITYSEPREGEXP );

    m_filename.clear();
    QString pattern("*.nas,*.bdf,*.igs,*.3dm"),prompt ("Which file");

    if(wds.size()>2)       { m_filename = wds[2]; pattern = m_filename;}
    if(wds.size()>3)        mAtts = wds[3];
    if(wds.size()>4)       { m_filename = wds[2]; pattern = wds[4];}
    if(wds.size()>5)        prompt = wds[5];

    QFileInfo sfi(this->Esail->ScriptFile() );
    QDir modelDir = sfi.absoluteDir() ;

    QFileInfo fi(modelDir,m_filename);

    if (!fi.exists()) {
        m_filename = getfilename(NULL,pattern  ,prompt ,QString(g_currentDir),PC_FILE_READ);
        this->ReWriteLine();
        fi.setFile(m_filename);
        if (!fi.exists())
            return 0;
    }
    m_filename = fi.absoluteFilePath();

    g_CurrentIncludeFile = this; Push_Entity( this, &g_parList);


    QString ext = fi.suffix();
    if(ext.compare("nas",Qt::CaseInsensitive) || ext.compare("bdf",Qt::CaseInsensitive)) {
        //   g_Janet = 1;  cout<<" remember Nastran import sets g_janet"<<endl;
        rc= ReadBulkData_1();
        rc+=ReadBulkData_2();
        rc+=ReadBulkData_3();
        rc+= ResolveObjects();
        // generate a field entity for the filaments
        QStringList fsl; fsl<< "field " << "nasfilaments"<<" "<< " "<<" "<<"$filament"<<"<atts>   " ;
        QString eline = fsl.join(RXENTITYSEPS);
        this->Esail->Insert_Entity( "field","nasfilaments",0,(long) 0,0,qPrintable(eline));

    }
    if(!Pop_Entity( (&g_CurrentIncludeFile), &g_parList))
        g_CurrentIncludeFile = NULL;
    if(g_parList) g_CurrentIncludeFile = (RXEntity_p ) g_parList->data;
    else  g_CurrentIncludeFile = NULL;

    this->SetNeedsComputing();
    this->Needs_Resolving=0;

    return 0;
}


QString RXImportEntity::nasno(const QString s)
{
    double rv;
    QString rc = s.simplified();
    // if there's a '-' thats not  the first character, replace by a 'E-'
    // same for a '+'
    int j = rc.lastIndexOf("-");
    if(j>0)
        rc.replace(j,1,"E-");
    j = rc.lastIndexOf("+");
    if(j>0)
        rc.replace(j,1,"E+");
    // remove floating point notation - our parser cannot handle it.
    rv = rc.toDouble();
    if(fabs(rv) < 1e-12)
    {
        return QString("0");
    }
    if(fabs(rv) < 1e-6)
    {
        rc=QString("%0").arg(rv,16,'f',13);
    }
    else     if(fabs(rv) < 1)
    {
        rc=QString("%0").arg(rv,16,'f',12);
    }
    else if(fabs(rv) < 1e6 )
    {
        rc=QString("%0").arg(rv,16,'f',6);
    }
    else if(fabs(rv) < 1e15 )
    {
        rc=QString("%0").arg(rv,16,'f',1);
    }
    else
        rc=QString("%0").arg(rv,16,'E',7);

    rc=rc.trimmed();
    while(rc.endsWith("0")&& rc.size()>1)
        rc.chop(1);

    return rc.trimmed();
}
//#error(" rotational fixities give Subscript #1 of the array SAILLIST has value -1 which is less than the lower bound of 1")
int RXImportEntity::ReadBulkData_1() //SPC*"  GRID*  GRID PSHELL*   MAT1*" "MAT8*"
{
    int rc=0;
    int InBulkSection=0;
    int depth = this->generated +1;
    std::map<QString,QString>  l_fixityStack;

    std::map<QString,QString>::iterator  it;
    std::stack<QString> l_stack;
    RXEntity_p l_new;
    std::map<QString,QString>  l_fixityTable;
    l_fixityTable[QString("1")] = QString(" $fix= 1 0 0 1");
    l_fixityTable[QString("2")] = QString(" $fix= 0 1 0 1");
    l_fixityTable[QString("3")] = QString(" $fix= 0 0 1 1");
    l_fixityTable[QString("123")] = QString("($fix=full)");
    l_fixityTable[QString("12")] =  QString(" $fix= 0 0 1 ");
    l_fixityTable[QString("13")] =  QString(" $fix= 0 1 0 ");
    l_fixityTable[QString("23")] =  QString(" $fix= 1 0 0 ");
    l_fixityTable[QString("156")] = QString(" ($fix= 1 0 0 1) ,($rotfixity= 1 0 0 ) ");
    l_fixityTable[QString("4")] = QString(" $rotfixity= 1 0 0 1");
    l_fixityTable[QString("5")] = QString(" $rotfixity= 0 1 0 1");
    l_fixityTable[QString("6")] = QString(" $rotfixity= 0 0 1 1");
    l_fixityTable[QString("123456")] = QString(" ($fix= full) ,($rotfixity= full)");
    l_fixityTable[QString("1234")] = QString(" ($fix= full),($rotfixity= 1 0 0 1)");
    l_fixityTable[QString("1235")] = QString(" ($fix= full),($rotfixity= 0 1 0 1)");
    l_fixityTable[QString("1236")] = QString(" ($fix= full),($rotfixity= 0 0 1 1)");
    QFile file(this->m_filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;
    QString lastline,lLine,nextLine;
    QTextStream in(&file);
    while (!in.atEnd()) {
        lastline=lLine;
        if(nextLine.isEmpty())
            lLine = in.readLine();
        else
            lLine=nextLine;
        nextLine.clear();

        if(lLine.startsWith('$')  )
        {
            if(lLine.startsWith("$  SailPack ")  )
                m_rximportFlags = m_rximportFlags|RXIMPORT_SAILPACK;
            else if(lLine.startsWith("$")  )  // specialfor Laurent so we can pop the comment before a BLSEG
                l_stack.push(lLine);
            continue;
        }
        if(lLine=="END BULK")
            InBulkSection=0;
        if(lLine=="BEGIN BULK")
            InBulkSection=1;
        if(!InBulkSection )
            continue;
        if(lLine.startsWith("SPC*"))
        {
            //KWord            no idea              nodename       fixity  ?displacement
            //SPC*                2001               3             123   0.000000e+000

            QStringList wds = lLine.split(QRegExp("\\s+")); //OK to spl it - its free-form
            if(l_fixityTable.find( wds[3]) !=l_fixityTable.end() )
                l_fixityStack[wds[2]]= l_fixityTable[wds[3]];// or something
            else
                cout<<" no translation coded for NASTRAN fixity: "<<qPrintable(wds[3])<<endl;
            continue;
        }

        if(lLine.startsWith("GRID*")) // abaqus style grid point
        {
            lLine.append(in.readLine()); //  a '*'
            lLine.replace("*","");

            QStringList wds = lLine.split(QRegExp("\\s+"));  //OK to spl it - its free-form
            QString SiteName = SITEQSTRING(wds[1].toInt());
            // GRID*   26   5.000000e-001  -6.940983e+000*  0.000000e+000
            QString siteline = QString("site") + QString(RXENTITYSEPS )+ SiteName
                    + QString(RXENTITYSEPS )+  wds[2]
                    + QString(RXENTITYSEPS )+  wds[3]
                    + QString(RXENTITYSEPS )+  wds[4] ;
            // + QString(RXENTITYSEPS ) +QString(" ") ;

            it= l_fixityStack.find(wds[1]);
            if(it !=  l_fixityStack.end() )
                siteline +=  QString(RXENTITYSEPS )+  it->second;// +QString("!") + line;


            l_new = Just_Read_Card(this->Esail, qPrintable(siteline), "site",qPrintable(wds[1]),&depth);
            if(l_new->Resolve())  // not good to change the ONC after resolving.
                l_new->Needs_Finishing=!l_new->Finish();

            continue;
        }
        if(lLine.startsWith("GRID")) // sailpack style fixed-format grid point
        {
            QStringList sentences = lLine.split(QRegExp("//"));
            // fixed format 8s
            QStringList wds= ChopString(sentences[0],8);
            QString SiteName = SITEQSTRING(wds[1].toInt());
            // GRID   26   5.000000e-001  -6.940983e+000*  0.000000e+000
            // reverse the sense of X
            //  wds[3] = QString("%0").arg(-( nasno(wds[3]).toDouble()  )    );
            QString siteline = QString("site") + QString(RXENTITYSEPS )+ SiteName
                    + QString(RXENTITYSEPS )+ nasno(wds[3])
                    + QString(RXENTITYSEPS )+ nasno(wds[4])
                    + QString(RXENTITYSEPS )+ nasno(wds[5]);
            siteline += QString(RXENTITYSEPS )+ this->mAtts;
            sentences.removeFirst();
            siteline+= sentences.join(",");

            it= l_fixityTable.find(wds[7].simplified());
            if(it !=  l_fixityTable.end() )
                siteline +=  QString(",") + it->second;

            if(wds.size()>10)
                siteline +=  wds[10]; //peter removed the '!'
            l_new = Just_Read_Card(this->Esail,qPrintable (siteline) , "site",qPrintable (wds[1])  ,&depth);
            if(l_new->Resolve())  // not good to change the ONC after resolving.
                l_new->Needs_Finishing=!l_new->Finish();

            continue;
        }
        if(lLine.startsWith("PSHELL*")) //PSHELL*                2               1   1.500000e-003               1
        {
            lLine.append(in.readLine()); //  a '*'

            QStringList wds = lLine.split(QRegExp("\\s+"));  //OK to spl it - its free-form
            int pid = wds[1].toInt();
            m_pshells[pid] = NastranPSHELL(pid,lLine);
            QString ename=QString("PSHELL_%1").arg(pid);
            QString eline("panel: "); eline += ename;
            m_pshells[pid].m_pan= (class rxpshell*) this->Esail->Insert_Entity("pshell",qPrintable(ename),NULL,0,NULL,qPrintable(eline),true);
            m_pshells[pid].m_pan->SetRelationOf(this,spawn,RXO_PANEL_OF_NAS);
            continue;
        }
        if(lLine.startsWith("PSHELL"))
        {
            //$ TYPE  ID      MatID   Thick   MID2    12I/T3  MID3    TS/T    NSM
            //PSHELL  1       1       0.02    1                               0
            int pid = lLine.mid(8,8).toInt();
            m_pshells[pid] = NastranPSHELL(pid,lLine);

            QString ename=QString("PSHELL_%1").arg(pid);
            QString eline("panel: "); eline += ename;
            QString atts = lLine.mid(80);
            m_pshells[pid].m_pan= (class rxpshell*) this->Esail->Insert_Entity("pshell",qPrintable(ename),NULL,0,qPrintable(atts),qPrintable(eline),true);
            m_pshells[pid].m_pan->SetRelationOf(this,spawn,RXO_PANEL_OF_NAS);
            continue;
        }
        if(lLine.startsWith("MAT1*") || lLine.startsWith("MAT8*"))
        {
            lLine.append(in.readLine()); //  a '*'
            lLine.replace("*"," ");
            QString ll = lLine+QString(" ") + lastline;
            QStringList wds = lLine.split(QRegExp("\\s+"));  //OK to spl it - its free-form
            int m = wds[1].toInt();
            // m_materials[m]=NastranMAT(m,ll);
            m_materials.insert(pair<int,NastranMAT>(m,NastranMAT(m,ll))) ;
            continue;
        }
        if(lLine.startsWith("MAT1"))
        {
            QStringList wds = ChopString(lLine,8);
            int pid =  wds.value(1).toInt();
            m_materials.insert(pair<int,NastranMAT>(pid,NastranMAT(pid,wds))) ;
            continue;
        }
        if(lLine.startsWith("MAT2"))
        {
            //            Mat data
            //            $ *********************************************************
            //            $ TYPE  MID     G11     G12     G13     G22     G23     G33     Rho
            //            $       A1      A2      A12     TREF    GE      ST      SC      SS
            //            MAT2    1       1691484 304945.10       1691484 0       355769.20.3     +MT 1
            //            +MT 1   0       0       0       0
            //            If the material is orthotropic material model, a orthotropic material model will be used;
            //            if the material is anisotropic, no material properties will be translated
            QString ll = lLine; ll.truncate(72);

            QString tail = lLine.mid(80);
            QString l2 = in.readLine(); l2.remove(0,8);
            ll=ll+l2+tail;

            QStringList wds = ChopString(ll,8);
            int pid = wds[1].toInt();

            m_materials.insert(pair<int,NastranMAT>(pid,NastranMAT(pid,wds))) ;
            continue;
        }

        if(lLine.startsWith("PBAR"))  // beam materials Laurent fixed format 8 char
        {
            //$ TYPE  ID      MatID   A       I1      I2      J       NSM
            //$       C1      C2      D1      D2      E1      E2      F1      F2
            // $       K1      K2      I12
            // PBAR    4       4       0.2     0.667   0.1667  0.458                   +PB1      //MAST
            //   +PB1    1       0.5
            // 1 find the continuation word (begins with +)  ( at 54) and strip the line.
            // 2  read the next line and strip the leadi8 chars and append
            // 3 parse into 8-char words
            QString ll = lLine; ll.truncate(72);

            QString tail = lLine.mid(80);
            QString l2 = in.readLine(); l2.remove(0,8);
            ll=ll+l2+tail;
            int m = ll.mid(8,8).toInt();
            m_pbars[m]=NastranPBAR(m,ll);
            m_pbars[m].Compute(this) ;
            continue;
        }

        if(lLine.startsWith("RBE2 ")) //rigid body element: list of nodes
        {
            QString ll = lLine.left(80);
            while (ll.endsWith("        ") )
                ll.chop(8);
            do {
                QString l2 = in.readLine();
                if(l2.startsWith("        ")){
                    l2.remove(0,8);
                    ll+=l2.left(80-8);
                    while (ll.endsWith("        "))
                        ll.chop(8);
                }
                else {
                    nextLine=l2;
                    break;
                }
            } while(true);
            QString w2 = ll.mid(8,8);
            int m = w2.toInt();
            m_rbe2s[m]=NastranRBE2(m,ll);
            continue;
        }

        if(lLine.startsWith("       "))
            continue;
        if(lLine.startsWith("CTRIA3"))
            continue;
        if(lLine.startsWith("CBAR"))
            continue;
        if(lLine.startsWith("CONROD"))
            continue;

    }
    file.close();
    return rc;
}
int RXImportEntity::ReadBulkData_2() // PLOAD4* CQUAD8*
{
    int rc=0;
    int InBulkSection=0; double pressure;
    std::map<int,double>  l_LoadStack;
    std::map<QString,QString>::iterator  it;
    std::stack<QString> l_stack;
    QFile file(this->m_filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;
    QString lastline,lLine;
    QTextStream in(&file);
    while (!in.atEnd()) {
        lastline=lLine;
        lLine = in.readLine();

        if(lLine.startsWith('$')  )
        {
            if(lLine.startsWith("$ next")  )  // specialfor abaqus
                l_stack.push(lLine);
            continue;
        }
        if(lLine=="END BULK")
            InBulkSection=0;
        if(lLine=="BEGIN BULK")
            InBulkSection=1;
        if(!InBulkSection )
            continue;
        if(lLine.startsWith("PLOAD4*")) // pressure load
        {
            lLine+=in.readLine(); //  a '*'
            lLine.replace("*","");
            //PLOAD4*             3001             240  -5.000000e+002*
            QStringList wds = lLine.split(QRegExp("\\s+"));  //OK to spl it - its free-form
            l_LoadStack[wds[2].toInt()]=  wds[3].toDouble();
            continue;
        }

        if(lLine.startsWith("CQUAD8*"))
        {
            int nn[8]; int k;
            // CQUAD8*              299               1             251             254
            //  *                    253             252             918             911
            //  *                    919             896
            //kw n mat,8 nodes
            lLine.append(in.readLine()); //  a '*'
            lLine.append(in.readLine()); //  a '*'
            lLine.append(in.readLine()); //  a '*'
            lLine.replace("*","");

            QStringList wds = lLine.split(QRegExp("\\s+"));  //OK to spl it - its free-form
            int elno = wds[1].toInt();
            int pshno = wds[2].toInt();
            pressure=0.0;
            if(l_LoadStack.find(elno) != l_LoadStack.end())
                pressure = l_LoadStack.find(elno)->second;
            for(k=0;k<8;k++)
                nn[k]=wds[k+3].toInt();
            RXImportEntity::NastranPSHELL& psh = this->m_pshells  [pshno];

            m_cquad8.push_back(NastranCQUAD8 (elno,psh,pressure, nn) );
            continue;
        }
        if(lLine.startsWith("CTRIA3"))
        {
            int nn[3]; int k;
            //$ TYPE  ID      SHELL   G1      G2      G3      Theta   MCID
            //CTRIA3  119     1       735     729     624     -0.03064
            //kw n mat,3 nodes theta

            QStringList wds;
            if(lLine.startsWith("CTRIA3*",Qt::CaseInsensitive))
                wds = lLine.split(QRegExp("\\s+"));
            else
                wds= ChopString(lLine  ,8) ;
            int elno = wds[1].toInt();
            int pshno = wds[2].toInt();
            double th = wds[6].toDouble();
            pressure=0.0;
            if(l_LoadStack.find(elno) != l_LoadStack.end())
                pressure = l_LoadStack.find(elno)->second;
            for(k=0;k<3;k++)
                nn[k]=wds[k+3].toInt();
            RXImportEntity::NastranPSHELL& psh = this->m_pshells  [pshno];

            m_ctria3.push_back(NastranCTRIA3 (elno,psh,pressure, nn,th) );
            continue;
        }
        if(lLine.startsWith("CONROD*"))
        {
            qDebug()<<" Free-form conrods not supported";
            continue;
        }
        if(lLine.startsWith("CONROD"))
        {
            //$ TYPE  EID     G1      G2      MID     A       J       C       NSM
            //CONROD  5909    221     222     271     7.854-5 0.3     0.3     0.1               //ETAGEBDF1 / ETAGEBDF1
            //  CleanLine(lLine);
            QString &ll = lLine;
            QString w2 = ll.mid(8,8);
            int m = w2.toInt();
            m_conrods.push_back(NastranCONROD(m,ll));
            continue;
        }

        if(lLine.startsWith("CBAR"))
        {
            //$ TYPE  ID      PID     GA      GB      X1      X2      X3      OFFT
            //$       PA      PB      W1A     W2A     W3A     W1B     W2B     W3B
            //CBAR    5642    4       1       2       0       1       0                         //MAST
            QString &ll = lLine;
            QString w2 = ll.mid(8,8);
            int m = w2.toInt();
            m_cbars.push_back(NastranCBAR(m,ll));
            m_cbars.back().Compute(this);
            continue;
        }
        // just to silence the print;
        if(lLine.startsWith("RBE2"))
            continue;
        if(lLine.startsWith("GRID"))
            continue;
        if(lLine.startsWith("MAT1"))
            continue;
        if(lLine.startsWith("       "))
            continue;
        if(lLine.startsWith("+"))
            continue;
        if(lLine.startsWith("CTRIA3"))
            continue;
        if(lLine.startsWith("PBAR"))
            continue;
        if(lLine.startsWith("BLSEG"))
            continue;

    }
    file.close();
    return rc;
}
int RXImportEntity::ReadBulkData_3() // PLOAD4* CQUAD8*
{
    int rc=0;
    int InBulkSection=0;
    std::stack<QString> l_stack;
    QFile file(this->m_filename);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return 0;
    QString lastline,lLine;
    QTextStream in(&file);
    while (!in.atEnd()) {
        lastline=lLine;
        lLine = in.readLine();

        if(lLine.startsWith('$')  )
        {
            if(lLine.startsWith("$")  )  // specialfor abaqus
                l_stack.push(lLine);
            continue;
        }
        if(lLine=="END BULK")
            InBulkSection=0;
        if(lLine=="BEGIN BULK")
            InBulkSection=1;
        if(!InBulkSection )
            continue;

        if(lLine.startsWith("BCONP"))
        {
            //$ TYPE  ID      SLAVE   MASTER          SFAC    FRICID  PTYPE   CID
            //BCONP   1       4       2               1               2
            QString &ll = lLine;
            QString w2 = ll.mid(8,8);
            int m = w2.toInt();
            m_bcomp.push_back(NastranBCONP(m,ll));
            continue;
        }

        if(lLine.startsWith("BLSEG")) // strings of nodes
        {
            QString LaurentHdr = l_stack.top();  l_stack.pop();
            QString ll = lLine.mid(8);
            do {
                QString l2 = in.readLine();
                if(l2.startsWith("$")){
                    l_stack.push(l2);
                    break;
                }
                l2.remove(0,8);
                ll+=l2;

            } while(true);
            ll+=LaurentHdr;
            QString w2 = ll.mid(0,8);
            int m = w2.toInt();
            m_blsegs[m]=NastranBLSEG(m,ll);
            continue;
        }


    }
    file.close();
    return rc;
}
RXImportEntity::NastranCQUAD8::NastranCQUAD8()
{
}
RXImportEntity::NastranCQUAD8::~NastranCQUAD8(){
}
RXImportEntity::NastranCQUAD8::NastranCQUAD8(const int n,
                                             RXImportEntity::NastranPSHELL&ps,
                                             const double pressure,
                                             const int nds[8]):
    nn(n),
    m_ps(ps),
    m_dp(pressure)

{
    for(int k=0;k<8;k++) nodes[k]=nds[k];

}
RXImportEntity::NastranCTRIA3::NastranCTRIA3()
{
}
RXImportEntity::NastranCTRIA3::~NastranCTRIA3(){
}
RXImportEntity::NastranCTRIA3::NastranCTRIA3(const int n,
                                             RXImportEntity::NastranPSHELL&ps,
                                             const double pressure,
                                             const int nds[3], const double th):
    nn(n),
    m_ps(ps),
    m_dp(pressure),
    m_th(th)

{
    for(int k=0;k<3;k++) nodes[k]=nds[k];

}

RXImportEntity::NastranMAT::NastranMAT():
    nn(-1),
    m_exx(0),
    m_nu(0),
    m_g(0)
{
    memset(d,0,9*sizeof(double));
}
RXImportEntity::NastranMAT::~NastranMAT() {}
RXImportEntity::NastranMAT::NastranMAT(const int n, const QString line):
    nn(n)
  , m_exx(0),m_nu(0),m_g(0)
  ,m_line(line)
{
    //    $ MAT1 from material SANGLE
    //    MAT1*                  1   9.000000e+009                   3.000000e-001
    //    *          0.000000e+000   0.000000e+000   0.000000e+000   0.000000e+000 $ MAT1 from material SANGLE
    //    $
    //    $ MAT8 from material TEXTILE
    //    MAT8*                  2   5.000000e+007   5.000000e+007   2.000000e-001
    //    *          5.000000e+006   0.000000e+000   0.000000e+000   0.000000e+000 $ MAT1 from material SANGLE
    memset (d,0,9*sizeof(double));
    QStringList wds = line.split(QRegExp("\\s+")); assert(line.contains("*")); // we can only use split on free-form

    if(wds[0].startsWith( "MAT1") ) {//isotropic
        m_exx = nasno(wds[2]).toDouble();
        m_nu = nasno(wds[4]).toDouble();

        m_g = nasno(wds[3]).toDouble();
        m_nu = max(m_nu,0.0); m_nu = min(m_nu,0.5) ;
        double exx = m_exx/(1-m_nu*m_nu);
        d[0]  = d[4]  = exx;
        d[1]  = d[3]  = exx * m_nu;
        d[8]    = m_g;
    }
    else if(wds[0].startsWith("MAT8" )  ) {
        cout<<"MAT8 not coded - use MAT1"<<endl;
    }
    else {
        cout<<qPrintable(wds[0])<<" not coded - use MAT1"<<endl;
    }

}
RXImportEntity::NastranMAT::NastranMAT(const int n, const QStringList wds):
    nn(n)
  , m_exx(0),m_nu(0),m_g(0)
{
    //    $ MAT1 from material SANGLE
    //    MAT1*                  1   9.000000e+009                   3.000000e-001
    //    *          0.000000e+000   0.000000e+000   0.000000e+000   0.000000e+000 $ MAT1 from material SANGLE
    //    $
    //    $ MAT8 from material TEXTILE
    //    MAT8*                  2   5.000000e+007   5.000000e+007   2.000000e-001
    //    *          5.000000e+006   0.000000e+000   0.000000e+000   0.000000e+000 $ MAT1 from material SANGLE
    //    $ TYPE  MID     G11     G12     G13     G22     G23     G33     Rho
    //    $       A1      A2      A12     TREF    GE      ST      SC      SS
    //    MAT2    1       1691484 304945.10       1691484 0       355769.20.3     +MT 1
    //    +MT 1   0       0       0       0

    m_line =wds.join("");
    memset (d,0,9*sizeof(double));

    if(wds[0].startsWith( "MAT1") ) {//isotropic
        m_exx = nasno(wds[2]).toDouble();
        m_nu = nasno(wds[4]).toDouble();
        m_g = nasno(wds[3]).toDouble();
        m_nu = max(m_nu,0.0); m_nu = min(m_nu,0.5) ;
        double e1111 = m_exx/(1-m_nu*m_nu);
        d[0]  = d[4]  = e1111;
        d[1]  = d[3]  = e1111 * m_nu;
        d[8]  = m_g;
    }
    else
        if(wds[0].startsWith( "MAT2") ) {
            //     G11=2     G12=3     G13=4
            //     G12=3     G22=5     G23=6
            //     G13=4     G23=6     G33 =7
            d[0]  =nasno(wds[2]).toDouble();
            d[1]  =nasno(wds[3]).toDouble();
            d[2]  =nasno(wds[4]).toDouble();
            d[3]  =nasno(wds[3]).toDouble();
            d[4]  =nasno(wds[5]).toDouble();
            d[5]  =nasno(wds[6]).toDouble();
            d[6]  =nasno(wds[4]).toDouble();
            d[7]  =nasno(wds[6]).toDouble();
            d[8]  =nasno(wds[7]).toDouble();

        }
        else if(wds[0].startsWith("MAT8" )  ) {
            cout<<"MAT8 not coded - use MAT1"<<endl;
        }
        else {
            cout<<qPrintable(wds[0])<<" not coded - use MAT1"<<endl;
        }

}

QString RXImportEntity::NastranMAT::Summary() const
{
    QString rv;
    int r,c,k;
    rv += "{";
    for(r=0,k=0;r<3;r++) {
        rv += "{";
        for(c=0;c<3;c++,k++) {
            rv+= " " + QString::number(d[k]);

        }
        rv += "}";
    }
    rv += "}";
    return rv;
}
double RXImportEntity::NastranMAT::exx()const
{
    return m_exx;
}
double RXImportEntity::NastranMAT::g() const
{
    return m_g;
}

RXImportEntity:: NastranPSHELL::NastranPSHELL()
    : pid(0),mMatid(0),mid2(0),mid3(0),mid4(0),
      thk(0),bsp(0),tst(1.),nsm(0.),z1(0),z2(0),t0(0),m_pan(0)
{
}
RXImportEntity:: NastranPSHELL::~NastranPSHELL()
{

}

RXImportEntity:: NastranPSHELL::NastranPSHELL(const int n,const QString lineIn)
    : pid(0),mMatid(0),mid2(0),mid3(0),mid4(0),
      thk(0),bsp(0),tst(1.),nsm(0.),z1(0),z2(0),t0(0),m_pan(0)
{
    //PID
    //MID1
    //T
    //MID2
    // 12I/T3 bsp
    //MID3
    //TST
    //NSM
    // Z1,Z2
    //MID4
    // T0
    //PSHELL*                2               1   1.500000e-003
    m_line=lineIn;
    QStringList wds ;
    if(m_line.startsWith( "PSHELL*",Qt::CaseInsensitive)) {
        m_line.replace("*","");
        wds = m_line.split(QRegExp("\\s+"));
    }
    else
        wds=ChopString(m_line,8);

    pid = wds[1].toInt();
    mMatid= wds[2].toInt();
    thk = wds[3].toDouble();
    mid2 =wds[4].toInt();

}
////////////////////////////////////////////


RXImportEntity:: NastranObject::NastranObject( ) :
    m_line("(NEW)"),
    m_nn(-1),
    m_ent(0)
{

}
RXImportEntity:: NastranObject::NastranObject(const int pnn, const QString pline) :
    m_line(pline),
    m_nn(pnn),
    m_ent(0)
{

}
RXImportEntity::NastranBLSEG::NastranBLSEG(const int nn, const QString pline):
    RXImportEntity::NastranObject(nn,pline)
{

}

int RXImportEntity::NastranBLSEG::Resolve()
{
    // the line has a trailing '$ // comment'
    // the test for k non-zero is a clumsy way to catch this

    int i, k;
    double z=0, f=0.5;
    int depth = g_theIE->generated; depth++;
    QString battenName, ename=QString("nasblseg_%1").arg(this->m_nn);
    QString  eline=QString("string :")+QString(ename)+QString(" : %node :");
    QString w,tail;
    NastranCBAR b ;
    for(i=1; ;i++) { //first word is the object's ID
        k= word(i).toInt();
        if(!k)
            break;
        this->m_nodes.push_back(k);
        w = SITEQSTRING(k)+ QString(RXENTITYSEPS );
        eline+=w;
    }
    if(m_nodes.size()<2)
        return 0;
    if(m_line.contains(RXSAILPACKPOCKETID) &&m_nodes.size()>1 && (g_theIE->m_rximportFlags&RXIMPORT_SAILPACK) )  //make a  nodecurve";
    {
        QStringList  ncline; ncline<<"nodecurve"<<ename<<tail+QString("$trim=0,$ea=.00,$ti=0,$mass=0,$xabs,$flat,$sketch,$pocket,$string");
        // $string so it gets psides

        for(std::list<int>::iterator it=m_nodes.begin();it!=m_nodes.end();++it)
            ncline+= SITEQSTRING(*it);

        eline = ncline.join(RXENTITYSEPS ); ename+=QString("_nc");
        m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(eline),"nodecurve",qPrintable(ename),&depth,true);
    //    qDebug()<<" SKIP generating batten nodecurve" <<eline;
    }

    else if(m_line.contains(RXSAILPACKBATTENID)  &&m_nodes.size()>1 && (g_theIE->m_rximportFlags&RXIMPORT_SAILPACK) )
    {
        int n1,n2;
        std::vector<NastranCBAR> cbars;
        QString  bline;
        // we have a list of NODES
        //We have to find the CBARS that go with them,
        // and so get the eixx at midpoints and create a 'beammaterial' with the name of the next BLSEG
        // this is very specific to Sailpack.
        // we DONT create beam elements for those CBARS.
        int otherEnd = g_theIE->FindOtherEndOfBCONP(m_nn);
        if(otherEnd<0)
            return 0;
        battenName=QString("nasblseg_%1_bat").arg(otherEnd);
        // the batten name here should is based on the 'other end' of the bconp concerning   this blsg.

        std::list<int>::iterator itend = m_nodes.end(); itend--;
        for(std::list<int>::iterator it=m_nodes.begin();it!=itend;++it) {
            n1= *it; it++; n2=*it; it--;
            cbars.push_back( g_theIE->Find_CBAR(  n1,  n2) );
        }
        std::vector<NastranCBAR>::iterator itb;
        double blen =0;
        for(itb=cbars.begin();itb!=cbars.end();++itb) {
            b = *itb;
            blen+=b.eis[6];
            // qDebug()<<b.eis[0]<<b.eis[1]<<b.eis[2]<<b.eis[3]<<b.eis[4]<<b.eis[5];
        }
        bline=QString("beammaterial :")+ battenName +QString(" : (from blseg id= %1) :$table\ns\t 0 \t").arg(this->word(0));
        for(itb=cbars.begin();itb!=cbars.end();++itb) {
            z = z + f * itb->eis[6]; f=1.0;
            bline+= QString("%1 \t").arg(z/blen );
        }
        bline+= "1\neixx\t";  bline+= QString("%1 \t").arg(cbars.begin()->eis[1] );
        for(itb=cbars.begin();itb!=cbars.end();++itb)
        {z=itb->eis[1]; bline+= QString("%1 \t").arg(z);}
        bline+= QString("%1\n").arg(z);

        bline+= "eiyy\t";  bline+= QString("%1 \t").arg(cbars.begin()->eis[2] );
        for(itb=cbars.begin();itb!=cbars.end();++itb)
        {z=itb->eis[2]; bline+= QString("%1 \t").arg(z);}
        bline+= QString("%1\n").arg(z);

        bline+= "gj  \t";  bline+= QString("%1 \t").arg(cbars.begin()->eis[3] );
        for(itb=cbars.begin();itb!=cbars.end();++itb)
        {z=itb->eis[3]; bline+= QString("%1 \t").arg(z);}
        bline+= QString("%1\n").arg(z);

        bline+= "ea  \t";  bline+= QString("%1 \t").arg(cbars.begin()->eis[0] );
        for(itb=cbars.begin();itb!=cbars.end();++itb)
        {z=itb->eis[0]; bline+= QString("%1 \t").arg(z);}
        bline+= QString("%1\n").arg(z);
        bline+= "ti  \t";  bline+= QString("%1 \t").arg(cbars.begin()->eis[4] );
        for(itb=cbars.begin();itb!=cbars.end();++itb)
        {z=itb->eis[4]; bline+= QString("%1 \t").arg(z);}
        bline+= QString("%1\n").arg(z);
        bline+= "beta\t";  bline+= QString("%1 \t").arg(cbars.begin()->eis[5] );
        for(itb=cbars.begin();itb!=cbars.end();++itb)
        {z=itb->eis[5]; bline+= QString("%1 \t").arg(z);}
        bline+= QString("%1\n\n").arg(z);



        /*        beammaterial 	 bb 	 someatts 	 $table
        s	0.	1
        eixx	.1e9	.1e9
        eiyy	.1e9	.1e9
        gj	10e5	10e5
        ea	80000000	80000000
        beta	0.0000	0.00
        ti	0.0000	0.000
        */

        m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(bline),"beammaterial",qPrintable(battenName),&depth,true);
    }
    else if (m_nodes.size()>1){// default is to make a string for use in a connector

      tail="%endlist : 0: 0: 0: ($trim=0),($ea=0),($ti=0),($mass=0)," ;
        eline+=tail;
        tail = m_line.mid( 8*i);
        eline += tail;
        m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(eline),"string",qPrintable(ename),&depth,true);
    }
    else
        m_ent=0;
    return 1;
}

/////////////////////////////

//Special Treatment For LuffLeechFootHead
//  This needs to be called AFTER the strings are meshed. so CALL THIS FROM Compute not from constructor)
//The luff is $u=0
//        foot $v=0
//        head $v=1
//        leech $u = 1 to (headlength+.1)/(footlength+.1)

//        So for each edge blseg we extract its nodelist (std::vector<int> RX_FELinearObject::GetNodeList())
// we set $u,$v and PCF_ISONEDGE on the edge nodes


/////////////////////////////


int RXImportEntity::SpecialTreatmentForLuffLeechFootHead(){
    int i,j, rc=0;
    QStringList ss;
    ss<< RXSAILPACKLUFFID
      << RXSAILPACKLEECHID
      <<RXSAILPACKFOOTID
     <<RXSAILPACKHEADID;

    double u1[4] ={0,1,0,1};
    double v1[4] ={1,0,0,1}  ;
    double u2[4] ={0,1,1,0};
    double v2[4] ={0,1,0,1} ;
    double zi[4]={1,1,1,1} ;
    // luff goes top to bottom
    // leech goes bottom to top
    // foot goes front to back
    // head goes back to front

    double f,u,v;
    QString  str;
    RX_FEString * st;
    RXSRelSite *n, *n1;
    std::vector<int> thenodes;
    std::vector<double> thefacs;
    std::vector<int> ::iterator in,i2;
    int k;
    std::map<int,RXImportEntity::NastranBLSEG> ::iterator  it;

    str = QString::fromStdString(this->Esail->GetType() );
    qDebug()<<"SpecialTreatmentForLuffLeechFootHead  " <<str;



    u2[1] = u1[3]  = 0.025; // sb length(head)/length(foot)

    // lets measure the edges
    for(it=this->m_blsegs.begin();it!=this->m_blsegs.end(); ++it )
    {
        const RXImportEntity::NastranBLSEG &t= it->second;
        for(i=0;i<ss.count();i++)
            if(t.m_line.endsWith (ss[i],  Qt::CaseSensitive) )
            {
                st = dynamic_cast< RX_FEString * >(t.m_ent);
                if(!st)
                    continue;
                zi[i] = st->d.m_zi_LessTrim;
            }
    }

    double  ufac = (zi[3]+0.05)/(zi[2]+0.05); //CHECKED
    qDebug()<< ss << zi[0]<<zi[1]<<zi[2]<<zi[3]<<"  ufac="<<ufac;
    QString newexp = QString( "(u/(1-v*(1-%1 ) ))").arg(ufac,15,'f');
    qDebug()<<" new expression  "<<newexp;
    u2[1] = u1[3]  = ufac; // sb length(head)/length(foot)
    RXVectorField *fld = dynamic_cast< RXVectorField *>(this->Esail->m_QuadToUnitSquare   );
    if(fld)
    {
        RXQuantity *exp=dynamic_cast< RXQuantity *>(fld->FindExpression(L"a",rxexpLocal));
        if(exp){
            exp->Change(TOSTRING(newexp.toStdWString()));

        }
    }
    // change  >m_QuadToUnitSquare  to be
    //vector field 	 myquad 	 (u/(1-v*(1-ufac ) )) 	 (v) 	 0
    // neater if we place m_QuadToUnitSquare on the Sail, not on the Mesh.
    //     RXExpressionI * FindExpression(const RXSTRING &text,rxexpLocalAndTree , RXExpressionI **e=0) const ; //looks in this->explist and recursively in this->parent->explist.

    for(it=this->m_blsegs.begin();it!=this->m_blsegs.end(); ++it )
    {
        const RXImportEntity::NastranBLSEG &t= it->second;
        for(i=0;i<ss.count();i++)
            if(t.m_line.endsWith(ss[i],  Qt::CaseSensitive) )
            {
                st = dynamic_cast< RX_FEString * >(t.m_ent);
                if(!st)
                    continue;
                thenodes = st->GetNodeList();
                if(thenodes.size()<2)
                    continue;
                thefacs.clear(); thefacs.push_back(0.0);
                for(k=0,in=thenodes.begin();in!=thenodes.end();++in,++k)
                { i2=in; i2++;
                    if(i2==thenodes.end())
                        break;
                    n=dynamic_cast<RXSRelSite *>(this->Esail->GetSiteByNumber(*in)) ;
                    n1=dynamic_cast<RXSRelSite *>(this->Esail->GetSiteByNumber(*i2)) ;
                    f = n->DistanceTo(*n1);
                    thefacs.push_back(f + thefacs.back());
                }
                assert(thenodes.size()==   thefacs.size());
                for(k=0,in=thenodes.begin();in!=thenodes.end();++in,++k)
                {
                    f = thefacs[k]/thefacs.back();
                    u = u1[i] *(1.-f) + u2[i]*f;
                    v = v1[i] *(1.-f) + v2[i]*f;

                    n=dynamic_cast<RXSRelSite *>(this->Esail->GetSiteByNumber(*in)) ;
                    str=QString("%2,$u=%0,$v=%1,").arg(u).arg(v).arg(n->AttributeString());
                    // qDebug()<< n->x<<n->y<<n->z<<str;
                     n->AttributeDestroy();
                     n->AttributeAdd(str);
                    n->m_Site_Flags = n->m_Site_Flags|PCF_ISONEDGE;
                    rc++;
                }
                // should be able to break here
            }
    }
    return rc;
}

RXImportEntity::NastranRBE2::NastranRBE2(const int nn, const QString pline):
    RXImportEntity::NastranObject(nn,pline)
{
    //    $ TYPE  EID     GN      Cn      GM1     GM2     GM3...
    //    RBE2    1       1       123456  106     116     117
    //    RBE2    2       105     123456  107     115     239     240     241
    //            242     243     244     245
    // translate to:
    // rigidbody:name: n1,n2,n3,... %end, atts (will have something like $fix=123456
    int k,nw = m_line.length();
    nw=nw/8;

    int depth = g_theIE->generated; depth++;
    QString ename=QString("rbe2_%1").arg(nn,3,10,QChar('0'));
    QString  eline=QString("rigidbody :")+QString(ename)+QString(" : ");
    QString w,atts;
    for(int i=2; i<nw;i++) { // first word is the objects ID
        if(i==3) { atts =   word(i); continue;}
        k= word(i).toInt();

        if(!k)
            break;
        this->m_nodes.push_back(k);
        w = SITEQSTRING(k)+  QString(RXENTITYSEPS );
        eline+=w;
    }
    if(m_nodes.size()<2)
        return;
    QString tail("%end : $fix=");
    eline+=tail+atts;
    m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(eline),"rigidbody",qPrintable(ename),&depth,true);
}
RXImportEntity::NastranBCONP::NastranBCONP()
{

}
RXImportEntity::NastranBCONP::~NastranBCONP()
{

}
RXImportEntity::NastranBCONP::NastranBCONP(const int nn, const QString line):
    NastranObject(nn,line)
{

}

int RXImportEntity::NastranBCONP::Resolve( )

{
    //$ TYPE  ID      SLAVE   MASTER          SFAC    FRICID  PTYPE   CID
    //BCONP   1       4       2               1               2

    int depth = g_theIE->generated; depth++;
    QString sailname;
    int k=1;
    int id = word(k++).toInt();
    int g2 = word(k++).toInt();
    int g1 = word(k++).toInt();
    //connect	name 	string 	track 	boat 	string 	luff 	main 	$slide$spline$interp


    NastranBLSEG   s1 = g_theIE->FindBLSEGAcrossModels(g1 ,sailname)   ;
    if(s1.m_nn<0)
        return 0;

    QString sname, eName;
   // 26 dec 2013 tried prepending n1.size() so that NNCs get resolved before SSDs - NO CHANGE to SPVANILLA
    eName=QString("%1_nasbconp_%2").arg((int)s1.m_nodes.size(),4,10,QLatin1Char ( '0')).arg(id);
    QString  eline=QString("connect:")+QString(eName);

    if(!s1.m_nodes.size())
    {
      this->m_ent->OutputToClient(
          QString( "(NastranBCONP::Resolve) blseg with line '")+
          s1.m_line+QString ("' has no nodes "),3);
    }
    int node =  s1.m_nodes.front() ;
    if(s1.m_nodes.size()>1)
        sname=QString(" :string :nasblseg_%1:%2").arg(g1).arg(sailname);
    else // 	site 	<name>	<model>
        sname=QString(" :site:%1:%2").arg(SITEQSTRING2(sailname,node)).arg(sailname);
    eline+=sname;

    NastranBLSEG  s2 = g_theIE->FindBLSEGAcrossModels(g2 ,sailname) ; // slave
    if(s2.m_nn<0)
        return 0;
    if(!s2.m_nodes.size())
    {
      this->m_ent->OutputToClient(
          QString( "(NastranBCONP::Resolve) blseg with line '")+
          s2.m_line+QString ("' has no nodes "),3);
    }
    node =  s2.m_nodes.front() ;
    if(s2.m_nodes.size()>1)
        sname=QString(" :string :nasblseg_%1:%2").arg(g2).arg(sailname);
    else
        sname=QString(" :site :%1:%2").arg(SITEQSTRING2(sailname,node)).arg(sailname);

    eline+=sname; eline +=":";
    if(s1.m_nodes.size()>1)
        eline+= "$slide$spline$interp,$cspline ";
    eline +=" :";
// a  Sailpack heuristic:  Skip the pocketEnds while we are using old-style battens.
    if(s1.m_line.contains(RXSAILPACKPOCKETENDID ))
        return 1 ;
    if(s2.m_line.contains(RXSAILPACKPOCKETENDID ))
        return 1 ;
    if(s1.m_line.contains(RXSAILPACKBATTENENDID ))
        return 1 ;
    if(s2.m_line.contains(RXSAILPACKBATTENENDID ))
        return 1 ;

    m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(eline),"connect",qPrintable(eName),&depth,true);
    return 1;
}
int RXImportEntity::NastranPBAR::Compute( class RXImportEntity*model)
{
    //$ TYPE  ID      MatID   A       I1      I2      J       NSM      C1      C2      D1      D2      E1      E2      F1      F2       K1      K2      I12
    //PBAR    4       4       0.2     0.667   0.1667  0.458                   1       0.5      //MAST

    int rc=0;
    // id (m_nn) is already given
    int k =2;
    matid = word(k++).toInt();
    area = nasno(word(k++)).toDouble();
    i1   = nasno(word(k++)).toDouble();
    i2   = nasno(word(k++)).toDouble();
    j    = nasno(word(k++)).toDouble();
    nsm  = nasno(word(k++)).toDouble();
    c1   = nasno(word(k++)).toDouble();
    c2   = nasno(word(k++)).toDouble();
    d1   = nasno(word(k++)).toDouble();
    d2   = nasno(word(k++)).toDouble();

    return rc;
}

int RXImportEntity::NastranBCONP::Compute(class RXImportEntity*model)
{
    return 0;
    QString sitename,stringname,msailname, ssailname, s1,s2,s3("");
    int islave = word(2).toInt();
    int imaster = word(3).toInt();
    const  RXImportEntity::NastranBLSEG& s = model->FindBLSEGAcrossModels(islave,ssailname);//model->m_blsegs[islave];
    RXImportEntity::NastranBLSEG m =  model->FindBLSEGAcrossModels(imaster,msailname);//model->m_blsegs[imaster]; //checked

    // do s and m have m_ent??

    //  Our sliding connection works between STRINGS ( sequences of edges - not sequences of nodes0
    //  Compute BLSEG creates a string and compute BCONP creates a connect.
    //   Both pass though the standard preprocessor.
    // connect	m_b_luffok 	string 	track 	boat 	string 	luff 	main 	$slide$spline$interp
    //   connect	m_b_tack 	site 	trackbase 	boat 	site 	cun_base 	main 	 	2


    if(m.m_nodes.size()==1)
        s1 = QString("site \t %1 \t %2").arg(sitename).arg(msailname);
    else{
        s1 = QString("string\t%1 \t %2").arg(stringname).arg(msailname);
        s3 = QString(" $slide$spline$interp,$cspline ");
    }

    if(s.m_nodes.size()==1)
        s2 = QString("site \t %1 \t %2").arg(sitename).arg(ssailname);
    else
        s2 = QString("string\t%1 \t %2").arg(stringname).arg(ssailname);


    QString line = QString("connect: %1 : %2  :%3") .arg(s1).arg(s2).arg(s3);
    qDebug()<<"create a sliding connect between master.m_nodes and slave.m_nodes";
    qDebug()<<line;
    return 1;
}
RXImportEntity::NastranCONROD::NastranCONROD(const int nn, const QString line):
    RXImportEntity::NastranObject(nn,line)
{
    // $ TYPE  EID     G1      G2      MID     A       J       C       NSM
    //CONROD  5909    221     222     271     7.854-5 0.3     0.3     0.1               //ETAGEBDF1 / ETAGEBDF1
    class RX_FESite *p1,*p2; p1=p2=0;
    RXEntity_p  s1, s2; s1=s2=0;

    int eid,g1,g2,imat;
    double  area,jay,c,nsm;

    eid = word(1).toInt();
    g1= word(2).toInt();
    g2= word(3).toInt();
    imat= word(4).toInt();
    NastranMAT &mat = g_theIE->m_materials[imat] ;
    area=nasno( word(5)).toDouble();
    jay= nasno(word(6)).toDouble();
    c= nasno(word(7)).toDouble();
    nsm= nasno(word(8)).toDouble();

    double ea = area * mat.exx();
    double ti=0;
    double masspermetre=0;

    //ms>Batten 0 trimming
    if ( this->m_line.contains(RXSAILPACKBATTENID ) &&  this->m_line.contains("trimming"))
    {
        qDebug() <<"Skip sailpack batten trimmer \n>>>" << m_line;
        return;
    }
    s1 = g_theIE->Esail->GetKeyNoAlias(L"site", SITEQSTRING(g1).toStdWString() );
    s2 = g_theIE->Esail->GetKeyNoAlias(L"site", SITEQSTRING(g2).toStdWString());

    class RXSRelSite *r1 = dynamic_cast< class RXSRelSite *>(s1);
    class RXSRelSite *r2 = dynamic_cast< class RXSRelSite *>(s2);
    double zi=r1->DistanceTo(*r2);
    p1 = dynamic_cast<class RX_FESite *>(s1); p2 = dynamic_cast<class RX_FESite *>(s2);
    // see RX_PCFile line 875

    if(0) // make flinks = efficient but no trim
    {
        int linkno= cf_flinkcount(g_theIE->Esail->SailListIndex ());
        int fnn = cf_insert_flink (g_theIE->Esail->SailListIndex (), ++linkno ,p1->GetN()   ,p2->GetN(),
                                   ea,zi, ti,masspermetre,this) ;
        //return value is index of the fortran object
        //double tension = cf_getflinktension(l.fptr);

        cf_setflinklength(g_theIE->Esail->SailListIndex (),fnn, zi);
    }
    else   // to make a string, something like
    {
        // the tail starts at line 80.
        // we split it at the first ','
        // the entity name is generated from the text before the split
        // the attributes get the text after the split.
        int depth = g_theIE->generated; depth++;
        QString tail, atts, ename;
        tail = line.mid(80);
        int kk = tail.indexOf(",") ;
        if(kk>=0) {
            ename = tail.left(kk );
            atts = tail.mid(kk+1);
        }
        else
            ename=tail;
        ename=ename.replace("//","");
        ename=ename.simplified();
        ename=ename.replace(" / ","-");
        ename +=QString("_cr_%1").arg(eid);

        QString  eline=QString("string :")+QString(ename)+QString(" : %node :");
        eline+=QString(r1->name()) +QString(" : ");
        eline+=QString(r2->name()) +QString(" : ");

        tail=QString("%endlist : 0: 0: 0: %4,$noslide,$trim=0,$ea=%1,$ti=%2,$mass=%3,").arg(ea,0,'f').arg(ti,0,'f').arg(masspermetre,0,'f').arg(atts);
        eline+=tail ;
        //        qDebug()<<eline;
        m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(eline),"string",qPrintable(ename),&depth,true);
    }
}

int RXImportEntity::NastranCONROD::Compute( class RXImportEntity*model)
{
    // $ TYPE  EID     G1      G2      MID     A       J       C       NSM
    //CONROD  5909    221     222     271     7.854-5 0.3     0.3     0.1               //ETAGEBDF1 / ETAGEBDF1
    class RX_FESite *p1,*p2; p1=p2=0;
    RXEntity_p  s1, s2; s1=s2=0;
    return 0;
    int eid,g1,g2,imat;
    double  area,jay,c,nsm;
    eid = word(1).toInt(); // careful word() implies fixed-form
    g1= word(2).toInt();
    g2= word(3).toInt();
    imat= word(4).toInt();
    NastranMAT &mat = model->m_materials[imat] ;
    area=nasno( word(5)).toDouble();
    jay= nasno(word(6)).toDouble();
    c= nasno(word(7)).toDouble();
    nsm= nasno(word(8)).toDouble();

    double ea = area * mat.exx() ;
    double ti=0;
    double masspermetre=0;

    //ms>Batten 0 trimming
    if ( this->m_line.contains(RXSAILPACKBATTENID ) &&  this->m_line.contains("trimming"))
        return 1;

    s1 = model->Esail->GetKeyNoAlias(L"site", SITEQSTRING(g1).toStdWString() );
    s2 = model->Esail->GetKeyNoAlias(L"site", SITEQSTRING(g2).toStdWString());

    class RXSRelSite *r1 = dynamic_cast< class RXSRelSite *>(s1);
    class RXSRelSite *r2 = dynamic_cast< class RXSRelSite *>(s2);
    double zi=r1->DistanceTo(*r2);
    p1 = dynamic_cast<class RX_FESite *>(s1); p2 = dynamic_cast<class RX_FESite *>(s2);
    // see RX_PCFile line 875



    if(0) // make flinks = efficient but no trim
    {
        int linkno= cf_flinkcount(model->Esail->SailListIndex ());
        int fnn = cf_insert_flink (model->Esail->SailListIndex (), ++linkno ,p1->GetN()   ,p2->GetN(),
                                   ea,zi, ti,masspermetre,this) ;
        //return value is index of the fortran object
        //double tension = cf_getflinktension(l.fptr);

        cf_setflinklength(model->Esail->SailListIndex (),fnn, zi);
    }
    else   // to make a string, something like
    {
        int depth = g_theIE->generated; depth++;
        QString ename=QString("nasconrod_%1").arg(eid);
        QString  eline=QString("string :")+QString(ename)+QString(" : %node :");
        eline+=QString(r1->name()) +QString(" : ");
        eline+=QString(r2->name()) +QString(" : ");

        QString tail=QString("%endlist : 0: 0: 0: $trim=0,$ea=%1,$ti=%2,$mass=%3").arg(ea).arg(ti).arg(masspermetre);
        eline+=tail; qDebug()<<eline;
        m_ent = Just_Read_Card(g_theIE->Esail,qPrintable(eline),"string",qPrintable(ename),&depth,true);
    }
    return 1;
}
int RXImportEntity::NastranCBAR::Compute( class RXImportEntity*model)//beam
{

    //$ TYPE  ID      PID     GA      GB      X1      X2      X3      OFFT
    //$       PA      PB      W1A     W2A     W3A     W1B     W2B     W3B
    //CBAR    5642    4       1       2       0       1       0                         //MAST
    class RX_FESite *p1,*p2; p1=p2=0;
    RXEntity_p  s1, s2; s1=s2=0;
    double  beta=0., zi,ti=0, mass=0;
    double s[3],e[3]; ON_3dPoint q;
    double emp[5]; //ea, ei2,ei3,gj,ti,zi
    char name[64]; strcpy(name,"nasnode");
    int j,kk, eid,isect,n1,n2,err=0;//,g1,g2
    double  x1,x2,x3;

    eid = word(1).toInt();
    isect= word(2).toInt();
    m_cbnodes[0]= word(3).toInt();
    m_cbnodes[1]= word(4).toInt();

    NastranPBAR  &p  = model->m_pbars[isect] ;
    NastranMAT &mat = model->m_materials[p.matid] ;
    x1= nasno(word(5)).toDouble();
    x2= nasno(word(6)).toDouble();
    x3= nasno(word(7)).toDouble();

    eis[0]= emp[0] = p.area *  mat.exx() ;  // ea
    eis[1]= emp[1] = p.i1   *  mat.exx() ;  //ei2
    eis[2]= emp[2] = p.i2   *  mat.exx() ;  //ei3
    eis[3]= emp[3] = p.j    *  mat.g()   ;  //gj
    eis[4]= emp[4] = 0 ;                    //ti
    //eis[5] , beta should come from (x1,x2,x3)

    s1 = model->Esail->GetKeyNoAlias(L"site", SITEQSTRING(m_cbnodes[0]).toStdWString() );
    s2 = model->Esail->GetKeyNoAlias(L"site", SITEQSTRING(m_cbnodes[1]).toStdWString());

    p1 = dynamic_cast<class RX_FESite *>(s1); p2 = dynamic_cast<class RX_FESite *>(s2);
    eis[6]=zi=p1->DistanceTo(*p2);

    if ( this->m_line.contains(RXSAILPACKBATTENID ))
        return 1;

    n1 = p1->GetN(); p1->MakeSixNode();
    n2 = p2->GetN(); p2->MakeSixNode();
    q = p1->ToONPoint(); for(kk=0;kk<3;kk++) s[kk]=q[kk];
    q = p2->ToONPoint(); for(kk=0;kk<3;kk++) e[kk]=q[kk];
    j= cf_createbeamelement( model->Esail->SailListIndex(), n1, n2,
                             beta, zi,ti, mass,
                             emp,0,
                             name, 64,
                             &err); // returns the index (ie from nextfree
    if(err) { cout<< " Create beam error"<<endl; _rxthrow( " Create beam error");}

    kk = cf_one_initial_beamaxismatrix(model->Esail->SailListIndex(),j,s,e);// Are we sure this is OK in model coordinates
    // do a 'beta from X'

    return 1;
}
