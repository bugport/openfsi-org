 

#include "StdAfx.h" 
#include "rxON_Extensions.h"

#include "boundingbox.h"
#ifdef linux
	#define max(x,y)  ((x) > (y) ? (x) : (y))
	#define min(x,y)  ((x) < (y) ? (x) : (y))
#endif

int	RXON_GetLocalBB(const rxON_NurbsCurve *c, rxON_BoundingBox *bb, const int n, const double tol )// a BB around the relevant CVs, grown by max_dist
{
// segment between KNot(n) and Knot(n+1) will be inside the convex hull of CVs (n-order+1) to (n+1)
// we create the BB of these CVs
	int k,order;
	ON_3dPoint p0,   *m, *x;
	double *q;

	m = &(bb->m_min);
	x =  &(bb->m_max);
	assert(c->m_cv_stride>=3);
	order = c->Order(); 

	q = c->m_cv + ( n-order+2) *c->m_cv_stride;
	*m = ON_3dPoint(q); 
	*x=*m; 

 // int     m_cv_stride;      // The pointer to start of "CV[i]" is
                            //   m_cv + i*m_cv_stride.
	q+=c->m_cv_stride;
	for(k=1;k<order;k++) {

		m->x=min(m->x,*q); 		x->x=max(x->x,*q); q++;
		m->y=min(m->y,*q);		x->y=max(x->y,*q); q++;
		m->z=min(m->z,*q);		x->z=max(x->z,*q); q++;
		q+=(c->m_cv_stride-3);
	}
	bb->GrowAllRound(tol);
//	ONC_Grow_BoundingBox(*bb, tol);
return true;
}

