// RXCurveIntersection.cpp: implementation of the RXCurveIntersection class.
//
//////////////////////////////////////////////////////////////////////

 
#include "StdAfx.h"
 
#include "RXCurveIntersection.h"

#ifdef _DEBUG_NEVER
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

RXCurveIntersection::RXCurveIntersection()
{
	Init();
}//RXCurveIntersection::RXCurveIntersection

//Copy constructor
RXCurveIntersection::RXCurveIntersection(const RXCurveIntersection & p_Obj)
{
	Init();
	this->operator =(p_Obj);
}//RXCurveIntersection::RXCurveIntersection(double p_x,double p_y,double p_z)

RXCurveIntersection::~RXCurveIntersection()
{
	Clear();
}//RXCurveIntersection::~RXCurveIntersection

void RXCurveIntersection::Init()
//meme alloc + init val to 0 0 0
{
	m_Point1.Init();  
	m_Point2.Init();
}//void RXCurveIntersection::Init

int RXCurveIntersection::Clear()
{
	m_Point1.Clear();
	m_Point2.Clear();
	return(1);
}//int RXCurveIntersection::Clear

RXCurveIntersection& RXCurveIntersection::operator = (const RXCurveIntersection & p_Obj)
{
	//Make a copy
	Clear();

	m_Point1 = p_Obj.m_Point1;
	m_Point2 = p_Obj.m_Point2;
	
	return *this;
}//void RXCurveIntersection::operator =

int RXCurveIntersection::SetPoint1(const RXPointOnCurve & p_pt)
{
	m_Point1 = p_pt;
	return 1;
}//int RXCurveIntersection::SetPoint1

int RXCurveIntersection::SetPoint2(const RXPointOnCurve & p_pt)
{
	m_Point2 = p_pt;
	return 1;
}//int RXCurveIntersection::SetPoint2

ON_3dPoint RXCurveIntersection::EvalPt(const int &p_ID) const
{
	assert((p_ID==0)||(p_ID==1));
	ON_3dPoint l_pt(0.0,0.0,0.0);
	
	if(p_ID==0)
		l_pt = m_Point1.GetPos();
	if(p_ID==1)
		l_pt = m_Point2.GetPos();
	return l_pt;
}//ON_3dPoint RXCurveIntersection::EvalPt

double RXCurveIntersection::Gett(const int &p_ID) const
{
	assert((p_ID==0)||(p_ID==1));
	double l_t=0.0;
	if(p_ID==0)
		l_t = m_Point1.Gett();
	if(p_ID==1)
		l_t = m_Point2.Gett();
	return l_t;
}//double RXCurveIntersection::Gett

RXPointOnCurve * RXCurveIntersection::GetPoint1()
{
	return &m_Point1;
}//RXPointOnCurve * RXCurveIntersection::GetPoint1

RXPointOnCurve * RXCurveIntersection::GetPoint2()
{
	return &m_Point2;
}//RXPointOnCurve * RXCurveIntersection::GetPoint2

double RXCurveIntersection::Distance() const
{
//	returns the distance between the 2 points
	ON_3dPoint l_pt1 = EvalPt(0);
	ON_3dPoint l_pt2 = EvalPt(1);
	double l_dist = l_pt1.DistanceTo(l_pt2);
	return l_dist;
}//double RXCurveIntersection::Distance

double RXCurveIntersection::GetCrvlength(const int &p_ID,double fractional_tolerance) const
{
	assert((p_ID==0)||(p_ID==1));
	double l_len = 0.0;
	if(p_ID==0)
		return m_Point1.GetLength(fractional_tolerance);
	if(p_ID==1)
		return m_Point2.GetLength(fractional_tolerance);
	return 0.0;
}//double RXCurveIntersection::GetCrvlength
