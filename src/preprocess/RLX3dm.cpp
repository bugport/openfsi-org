/* revision history
April 2008. Writes V3 files ifdef ON_V3 else V4 files.
Previously took the version from the sail's input version which is probably a bug.
April 2008. Some default values for the model being written are provided in  PC_3dm_Initialize

*/

#include "StdAfx.h"
#include <QDebug>
#include <QMessageBox>
#include <QFileInfo>
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXSeamcurve.h"
#include "rxON_Extensions.h"
#include "RelaxWorld.h"

#ifdef WIN32
#include <io.h>
#endif

#include "RX_FESite.h"

#include "entities.h"
#include "resolve.h"
#include "interpln.h"
#include "script.h"

#include "dxfin.h"
#include "finish.h"
#include "words.h"

#include "global_declarations.h"
#include "akmutil.h"

#include "etypes.h"
#include "RXCurve.h"
#ifdef HOOPS
#include "hoopsToON.h"
#endif
#include "RX_UI_types.h"
#include "RXAttributes.h"
#include "RLX3dm.h"
using namespace std;

#define RLX3DMDBG (0)


#define ASSUME_UNIQUE_NAMES

#ifndef streq
#define streq strieq
#endif 
#ifdef ON_V3
#define m_ModelUnitsAndTolerances  m_UnitsAndTolerances
#endif
char stmp[1024];




int Resolve_3DM_Card(RXEntity_p e) {// was   _cpp
    struct PC_3DM_Model * l_p3dmM = new PC_3DM_Model;
    l_p3dmM->m_name = NULL;
    l_p3dmM->m_qFilename.clear();
    l_p3dmM->m_segname = NULL;
    l_p3dmM->m_pOwner = NULL;
    l_p3dmM->m_pONXModel = new rxONX_Model;


    int retval = 0;
    QString qline = e->GetLine();
    QStringList wds= rxParseLine(qline,RXENTITYSEPREGEXP );
    if(wds.size()<3)  wds<<"*.3dm";

    QString & qname=  wds[1];
    QString &qfname = wds[2];
    QString qAtts =  wds.value(3);
    QString qCommand = wds.value(4);

    qfname = e->Esail->AbsolutePathName(qfname);

    qAtts=qAtts.toLower();
    if (qAtts!="out")
    {
        if(!VerifyFileName(qfname,qname,"*.3dm"))  //To make sure we are trying to read a 3dm file
            return 0;
    }
    else
    {
        if (!validfile(qfname))  //To make sure we are trying to write with a correct path
            return 0;
    }
    qfname = e->Esail->RelativeFileName(qfname);
    qline = wds.join(RXENTITYSEPS);
    e->edited = !(qline ==e->GetLine());
    if(e->edited){
        e->SetLine( qPrintable(qline));
    }

    l_p3dmM->m_pOwner = e;
    l_p3dmM->m_name = STRDUP(qPrintable(qname));
    l_p3dmM->m_qFilename = e->Esail->AbsolutePathName(qfname);
    l_p3dmM->m_segname = STRDUP(qPrintable(qname));
    if(l_p3dmM->m_qFilename != "already done") {
#ifdef HOOPS
        HC_Open_Segment("?Include Library");
        if(HC_QShow_Existence(name,"self"))
            HC_Delete_Segment(name);
        HC_Close_Segment();
#endif

        QStringList skipList = qAtts.split(QRegExp(",?\s*$skip\s*="));
        qDebug()<<"skipList "<<skipList;



        e->hoopskey = PC_3dm_Read(l_p3dmM,e->Esail);
        e->Esail->m_p3dmModel = l_p3dmM;  //Thomas 18 02 04 . to set the opennurbs model to the sail
        // Peter adds: but what if there are several 3dm files in this model??
    }

    e->SetDataPtr (l_p3dmM); l_p3dmM->m_pOwner = e;
    e->Needs_Resolving=0;
    e->Needs_Finishing=0;
    e->SetNeedsComputing();

    retval = 1;

    return(retval);
}//int Resolve_3DM_Card_cpp

HC_KEY PC_3dm_Read(struct PC_3DM_Model *p_p3dm,
                   SAIL *p_pSail)
{
    int NOpen_Start,NOpen_End;
    char seg1[256],seg2[256];

    HC_KEY dkey;
    if (RLX3DMDBG) NOpen_Start = List_Open_Segments();


    QFileInfo fi(p_p3dm->m_qFilename);
    if(fi.suffix()!="3dm")
        p_p3dm->m_qFilename=fi.completeBaseName()+".3dm";

    HC_KEY key;
    HC_Show_Pathname_Expansion(".", seg2);
    if(RLX3DMDBG) {List_Open_Segments(); cout<< " that was before 3dmin"<<endl;}

    key = dkey = PC_3dmin(p_p3dm,p_pSail, RLX_DEFAULT_UNITS );   //<<--  here is the reading process

    if(RLX3DMDBG) {List_Open_Segments(); cout<< " that was after 3dmin"<<endl;	}
    if(key){
        HC_Open_Segment_By_Key(key);
        HC_Set_Selectability("off");
        HC_Close_Segment();

        HC_Show_Segment(key,seg1);
        //   if(RLX3DMDBG) printf(" %s was read into %s\n", p_p3dm->m_filename,seg1);

        HC_Open_Segment(p_p3dm->m_name);
        if(RLX3DMDBG) {
            HC_Show_Pathname_Expansion(".", seg2);
            printf("including %s \ninto %s\n", seg1,seg2);
        }
        HC_Include_Segment_By_Key(key);
        HC_Close_Segment();


    }
    if (RLX3DMDBG) {
        cout<< " Leave Read_3dm"<<endl;
        do{
            NOpen_End = List_Open_Segments(); if (RLX3DMDBG) printf(" at end there are %d open\n", NOpen_End);
            if (NOpen_End > NOpen_Start) {
                HC_Close_Segment();
                cout<< "CLOSING THE TOP ONE"<<endl;
            }
        } while(NOpen_End > NOpen_Start) ;
    } // dbg

    return dkey;

}// PC_3dm_Read



HC_KEY PC_3dmin(struct PC_3DM_Model *p_p3dm, SAIL *p_pSail, ON::unit_system  p_u)
{
    HC_KEY key=0;
    QString namebuf,dir;

    QString l_3dmlibrary;

    ON::Begin();
    FILE * fp = ON::OpenFile(qPrintable(p_p3dm->m_qFilename),"rb");
    if(!fp) return 0;

    //TO Open an Archive from a the 3dm file and to read the archive in the model
    ON_BinaryFile l_archive(ON::read3dm,fp);
    ON_TextLog l_dump_tostdout;
    ON_TextLog * l_dump = &l_dump_tostdout;

    bool l_rc = p_p3dm->m_pONXModel->Read(l_archive,l_dump);
    if (!l_rc)
    {
        printf("Unable to read %s, file corrupted\n",qPrintable(p_p3dm->m_qFilename ) );
        return 0; // SB-1
    }
    // dir includes its trailing sep. ext excludes its leading dot.
    //rxqfileparse isnt the same as rxfileparse
    QString ext;
    rxqfileparse(p_p3dm->m_qFilename ,dir,namebuf,ext);

    Make_Valid_Segname(namebuf);

    l_3dmlibrary=QString( "?Incl ude_his toric/3dm/%1_%2").arg  (namebuf).arg((qulonglong)p_pSail)  ;

#ifdef linux	
    HC_Open_Segment_By_Key(p_pSail->PostProcNonExclusive());
#else
    HC_Open_Segment("?Include library");
#endif
#ifdef HOOPS
    char hbuf[1024];
    HC_Open_Segment("3dm");
    HC_Show_Pathname_Expansion(".",hbuf ); l_3dmlibrary=QString(hbuf);
    HC_Flush_Contents(".","everything");
    HC_Close_Segment();
    HC_Close_Segment();
    key=HC_KOpen_Segment(qPrintable (l_3dmlibrary)); // was 3dmsegment till sept 02
#endif

    int i;
    struct PC_3DM_Entity l_3dmEnt;
    l_3dmEnt.m_pModel = NULL;
    l_3dmEnt.m_pObj   = NULL;
    l_3dmEnt.m_sail   = NULL;

    l_3dmEnt.m_pModel  = p_p3dm->m_pONXModel; // To point on a model
    ON_3dmSettings   l_settings = p_p3dm->m_pONXModel->m_settings;
    double scalefactor=1.0;
    if(p_u != ON::no_unit_system)
        scalefactor=  l_settings.m_ModelUnitsAndTolerances.Scale( p_u );

    scalefactor=1/scalefactor;

    int l_n = p_p3dm->m_pONXModel->m_object_table.Count(); //Number of object in the model

#ifndef linux
    cout<< "start MakeNamesUnique "<<endl;
    p_p3dm->m_pONXModel->MakeNamesUnique ();
#else
    //    cout<< "MakeNamesUnique isnt implemented in linux"<<endl;
#endif

    //    cout<< "start sort of 3dm model"<<endl;

    p_p3dm->m_pONXModel->SortEntities(rxONX_Model::comparebytype);

    cout<< "start load of 3dm model: "<<qPrintable(p_p3dm->m_qFilename)<<endl;


    for (i=0;i<l_n;i++)
    {
        l_3dmEnt.m_pObj = &(p_p3dm->m_pONXModel->m_object_table[i]); //To point on the current object

        if (l_3dmEnt.m_pObj->m_attributes.IsVisible()) //to make sure only the selected object are added to the model
        {
            int l_layerID = l_3dmEnt.m_pObj->m_attributes.m_layer_index;
            if (l_3dmEnt.m_pModel->m_layer_table[l_layerID].IsVisible())
            {
#ifndef ASSUME_UNIQUE_NAMES
                GOAT			Make_Name_Unique(&l_3dmEnt,l_3dmEnt.m_pObj->m_attributes.m_layer_index);
#endif
                l_3dmEnt.m_sail = p_pSail;
                RXEntity_p l_new  = Read3dmObject(&l_3dmEnt,scalefactor);
            }
        }
    } // for I
#ifdef _DEBUG
    cout<<" finished loading the 3dm model"<<endl;
#endif
    Read3dmProperties(p_pSail, &l_3dmEnt);

    l_settings.m_ModelUnitsAndTolerances.m_unit_system=RLX_DEFAULT_UNITS ; // hopefully when we write the model it is now in metres.
    //Clean up
    //	if (l_3dmEnt.m_pONXModel) l_3dmEnt.m_pONXModel = NULL;
    if (l_3dmEnt.m_pObj) l_3dmEnt.m_pObj = NULL;
    if (l_3dmEnt.m_sail) l_3dmEnt.m_sail = NULL;

    HC_Close_Segment();

    // close the file
    ON::CloseFile( fp );

    ON::End();

    return(key);

}//  PC_3dmin


RXEntity_p  Read3dmPoint(struct PC_3DM_Entity * p_p3dmEnt)
{
    //old ONX_Model_Object l_obj(*(p_p3dmEnt->m_pObj));
    assert((PC_3DM_Entity*)p_p3dmEnt);
    assert(p_p3dmEnt->m_pObj);
    ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;
    const ON_Point* l_point = ON_Point::Cast(l_pObj->m_object);
    assert (l_point );

    RXEntity_p  l_new = NULL;

    ON_String l_strname = l_pObj->m_attributes.m_name;
    if (l_strname.IsEmpty())
        l_strname = ON_String("point");
    l_strname.MakeLower();
    l_strname = RemoveComents(l_strname);
    ON_String l_Name = l_strname;

    //Before oppenening the HOOPS segment we have to take care of the name syntaxe
    //PART of code copied from dxfin.c line 203->216
    size_t q = l_strname.Length() ; assert(q < 256); //fails for 'headfwd,oie'
    ON_String tail = PC_StripTail(l_Name,IGS_NAME_SEPARATOR); // to just before the first $. () OK for entity name but NOT Hoops name
    ON_String l_Atts;
    ON_String l_NewAtts;

    if(!tail.IsEmpty()) {
        l_Atts=ON_String(" N>");// put the tail of name in atts
        l_Atts+=tail;
    }
    if (l_Name.IsEmpty())
        l_Name = ON_String("pt");
    else
        Make_Valid_Segname(l_Name);
#ifndef ASSUME_UNIQUE_NAMES
    if(stristr(l_atts,"$3dm"))
        Make_Name_Unique( (SAIL*)p_p3dmEnt->m_sail,  "site",l_Name);
#endif

    HC_Open_Segment(l_Name.Array());

    //At this stage everything is in the same segment
    HC_Set_Marker_Symbol("(.)");

    ON_Layer l_layer;
    ON_Material l_material;
    if (l_pObj->m_attributes.m_layer_index>0)
        l_layer = p_p3dmEnt->m_pModel->m_layer_table[l_pObj->m_attributes.m_layer_index];
    if (l_pObj->m_attributes.m_material_index>0)
        l_material = p_p3dmEnt->m_pModel->m_material_table[l_pObj->m_attributes.m_material_index];
    ON_Color l_c;
    //Set the color from the 3DM model
    switch (l_pObj->m_attributes.ColorSource())
    {
    case 0:   //color_from_layer
        l_c = l_layer.Color();
        break;
    case 1:   //color_from_object
        l_c = l_pObj->m_attributes.m_color;
        break;
    case 2:   //color_from_material
        l_c = l_material.Ambient();
        break;
    }
    HC_Set_Color_By_Value ("markers", "RGB", float(l_c.Red()/255.),float(l_c.Green()/255.),float(l_c.Blue()/255.));

    RX_Insert_Marker(l_point->point.x,l_point->point.y,l_point->point.z,p_p3dmEnt->m_sail->GNode());
    HC_Close_Segment();

    //At this stage the point have been inserted in the HOOPS data base
    //we want to insert it in the RELAL data base only if the layer name starts with GAUSS
    int l_LayerID = l_pObj->m_attributes.m_layer_index;
    ON_String l_LayerName = p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();//gets freed
    //    ON_String tt = l_LayerName.Left(5);
    if(0!=l_LayerName.Left(5).CompareNoCase("GAUSS") )
        return l_new;


    l_Atts+=" ,($layer=";// put the tail of name in atts
    l_Atts+=l_LayerName ;// put the tail of name in atts
    l_Atts+="),";// put the tail of name in atts

    //If we are here that means that we are in a layer which starts with GAUSS
    char line[512];
    l_NewAtts= PC_3dm_create_attributes( p_p3dmEnt);
    if( l_NewAtts== "UserCancel" )
           RXTHROW( "UserCancel"   );

    sprintf(line,"site : %s: %20.12f: %20.12f: %20.12f: %s",l_Name.Array(),l_point->point.x,l_point->point.y,l_point->point.z,l_NewAtts.Array());

    int i=1;
    if(!(p_p3dmEnt->m_sail->Entity_Exists(l_Name,"site")  )){
        l_new = Just_Read_Card((SAIL*)p_p3dmEnt->m_sail ,line,"site",l_Name,&i);
        //to connect to the ON Object
        l_new->m_pONMO =  l_pObj;
        if(l_new->Resolve())  // not good to change the ONC after resolving.
            l_new->Needs_Finishing=!l_new->Finish();
    }
    return l_new;
}//extern int Read_3dm_Point

RXEntity_p  Read3dmCurve(struct PC_3DM_Entity * p_p3dmEnt)
{
    assert((PC_3DM_Entity*)p_p3dmEnt);
    assert(p_p3dmEnt->m_pObj);


    ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;
    if (!l_pObj)
        return 0;

    const ON_Curve* l_curve = dynamic_cast<const ON_Curve*>(l_pObj->m_object);
    assert(l_curve);


    int l_LayerID = l_pObj->m_attributes.m_layer_index;
    ON_String l_Layer_Name = p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();
    ON_String l_strname = l_pObj->m_attributes.m_name;
    if (l_strname.IsEmpty() )
        l_strname = ON_String("curve_")+l_Layer_Name;
    l_strname.MakeLower();
    l_strname = RemoveComents(l_strname);
    ON_String l_Name = l_strname;
    QString qname = l_Name.Array();

    l_Name=qPrintable(qname);
    l_strname = l_Name;

    //SOME CHECKINGS ON THE CURVE
    //IF the curve is a polycurve we want to make sure all the subc urve are in the same direction
    const ON_PolyCurve* l_pPl = ON_PolyCurve::Cast(l_curve);
    if (l_pPl)
    {
        QString qbuf;
        qbuf=QString("`%1` is a polycurve").arg(qname);
        p_p3dmEnt->m_sail->OutputToClient(qbuf,1);
        int l_count = l_pPl->Count();
        int i;
        //int l_NotContinuousDomain = 0;

        const ON_Curve * l_prvCrv = NULL;
        ON_Curve * l_crtCrv = NULL;
        ON_3dPoint l_PtprvcrvEnd,l_PtcrtcrvStart;
        ON_Interval l_prvcrvDomain,l_crtcrvDomain;
        for (i=1;i<l_count;i++)
        {
            l_prvCrv = l_pPl->operator [](i-1);
            l_crtCrv = l_pPl->operator [](i);
            l_prvcrvDomain = l_prvCrv->Domain();
            l_crtcrvDomain = l_crtCrv->Domain();
            l_PtprvcrvEnd = l_prvCrv->PointAtEnd();
            l_PtcrtcrvStart = l_crtCrv->PointAtStart();

            if (l_PtprvcrvEnd.DistanceTo(l_PtcrtcrvStart) > 1e-9) // Peter April 2005
            {
                //Peter added a dist check
                double length = l_PtprvcrvEnd.DistanceTo (l_PtcrtcrvStart);
 qbuf=QString("<%1> is a polycurve with sub curves separated by %2, please split some of these sub curves.").arg(l_Name.Array()).arg(length);

                p_p3dmEnt->m_sail->OutputToClient(qbuf,2);

                break;
            }

            if (fabs(l_prvcrvDomain.Max()-l_crtcrvDomain.Min())<0.000001 )
            {
                ON_3dPoint l_p = l_prvCrv->PointAtEnd();
                qbuf=QString("(read3DM advisory) '%1' is a polycurve made with sub curves with not continuous domain"
                             "Relax is changing the domain of some of these sub curves.").arg(l_Name.Array());// could free
//                PC_Insert_Arrow(l_p,45,l_buff);

                p_p3dmEnt->m_sail->OutputToClient(qbuf,1);

                l_crtCrv->SetDomain(l_prvcrvDomain.Max(),l_prvcrvDomain.Max()+l_crtCrv->Domain().Length());
                ON_Interval l_testD = l_crtCrv->Domain();
                break;
            }
        }
        if (l_prvCrv) l_prvCrv = NULL;
        if (l_crtCrv) l_crtCrv = NULL;
    }//	if (l_pPl)
    if (l_pPl) l_pPl=NULL;



    //END SOME CHECKINGS ON THE CURVE

    RXEntity_p  l_new = NULL;
    if( l_Layer_Name && (!strncasecmp( l_Layer_Name.Array()  , "gauss",5 )))
    {
        ON_String l_atts = PC_3dm_create_attributes(p_p3dmEnt);
       if(ON_ArcCurve::Cast(l_curve))
           g_World->OutputToClient(" DANGER! Opennurbs 4 is inaccurate with arcs and circles ",2);

        if( l_atts== "UserCancel" )
            RXTHROW( "UserCancel"   );

        l_atts.MakeLower();
        struct TwinEntity  theEnts;
        theEnts= Generate_Curve_From_ONO((SAIL*)p_p3dmEnt->m_sail,l_atts.Array(),
                                         l_Name,l_Layer_Name.Array(),l_Name ,l_pObj);
        l_new = theEnts.e1; // the basecurve
    }
    return l_new;// l_new is the basecurve
}//RXEntity_p  Read3dmCurve

RXEntity_p  Read3dmSurface(struct PC_3DM_Entity * p_p3dmEnt)
{
    // the argument points to a Brep.  We will take its first surface
    // (ON_Object*)(&(ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object))->m_F[i]);
    assert((PC_3DM_Entity*)p_p3dmEnt);
    assert(p_p3dmEnt->m_pObj);

    ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;

    const ON_Surface* l_psurface;
    l_psurface = ON_Surface::Cast(p_p3dmEnt->m_pObj->m_object);
    if (!l_psurface) { // it could be a Brep . take its first surface
        l_psurface = ON_Surface::Cast((ON_Object*)(&(ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object))->m_F[0]));
        if (!l_psurface)
            return 0;
    }



    int l_LayerID = l_pObj->m_attributes.m_layer_index;
    ON_String l_LayerName = p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();

    ON_String l_strname = l_pObj->m_attributes.m_name;
    l_strname = RemoveComents(l_strname);
    l_strname.MakeLower();
    if (l_strname.IsEmpty())
        l_strname = ON_String("surface")+l_LayerName;

    // now parse l_name into name and atts
    // then sprintf(line,"interpolation surface: %s : NURBS: NULL: 1 : 1 : %s From 3DM \n"name,atts); //,ie->Dwords[IGS_ELABL]);
    // if atts contains the word '$mould' {
    //	if a mould entity exists, delete it.
    //  	Resolve_Mould_Card(Just_Read_Card "mould:themould:<name>:xy:<atts>" ));
    // NOTE this call is after successfully esolving the Interpolation card
    //
    RXEntity_p l_new = NULL;
    if(l_LayerName.Left(5).CompareNoCase("gauss")==0) {
        PC_NURB *l_pRLXnurbs = (PC_NURB*)CALLOC(1,sizeof(struct PC_NURB ));
        l_pRLXnurbs->flags = PCN_ON_FLAG;
        l_pRLXnurbs->m_pONobject = (ON_Object*)l_psurface;
        l_pRLXnurbs->tol = 0.001;

        char line[256];
        sprintf(line,"interpolation surface: %s : NURBS: NULL: 1 : 1 :3DM\n",l_strname.Array() ); //,ie->Dwords[IGS_ELABL]);
        int i=1;

        l_new = Just_Read_Card((SAIL*)p_p3dmEnt->m_sail ,line,"interpolation surface",l_strname.Array(),&i); //ie->Dwords[IGS_ELABL],&i); //p->owner->Esail,
        l_new->m_pONMO =  p_p3dmEnt->m_pObj;

        if(  Resolve_Interpolation_Card (l_new)){
            struct PC_INTERPOLATION *pi = (struct PC_INTERPOLATION *)l_new->dataptr;
            l_new->SetNeedsComputing(0);
            pi->NurbData=l_pRLXnurbs;
            // create mould card here
            if(l_new->AttributeFind("mould"))
            {
                //mould	BTheMould 	spherical 	xy 	<reserved>
                QString qline("mould: ");
                qline+=l_new->name();
                qline+= ":";
                qline+=l_new->name();
                qline+= ":xy: ";
                RXEntity_p ment = Just_Read_Card((SAIL*)p_p3dmEnt->m_sail ,
                                                 qPrintable(line),
                                                 "interpolation surface",
                                                 l_new->name(),
                                                 &i);
                qDebug()<<" made a mould object from ISurface object "<< l_new->GetQName();
            }
        }//	if(  Resolve_Interpolation_Card (l_new))
        return l_new;
    } // is GAUSS
    else { // just draw it
        //   AMGDraw_3dmObject( p_p3dmEnt,1.0);
        l_new=0;

    }

    l_LayerName.Destroy ();
    return l_new;
}//RXEntity_p  Read3dmSurface

RXEntity_p  Read3dmBrep(struct PC_3DM_Entity * p_p3dmEnt)
{	
    const ON_Brep* l_Brep=0;
    l_Brep = ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object);
    if (!l_Brep)
        return 0;

    RXEntity_p l_new = NULL;

    int l_n,i;
    //The Vertexis
    l_n = l_Brep->m_V.Count();
    for (i=0;i<l_n;i++)
    {
        //DO NOT WANT TO ADD THE VERTEXIS INTO THE RELAX MODEL FOR
        //l_Result = Read3dmPoint(&(l_Brep->m_V[i]));
        //if (l_Result!=InputOK)
        ///	return l_Result;
    }

    //The edges
    l_n = l_Brep->m_E.Count();
    for (i=0;i<l_n;i++)
    {
        //DO NOT WANT TO ADD THE EDGES INTO THE RELAX MODEL FOR
        //l_Result = Read3dmCurve(&(l_Brep->m_E[i]));
        //if (l_Result!=InputOK)
        //	return l_Result;
    }

    //The faces
    l_n = l_Brep->m_F.Count();
    if(l_n>1) printf(" only the first surface in a Brep is translated (this brep has %d)\n",l_n);
    for (i=0;i<l_n;i++)
    {
        //To reset the ON_Object associated to the entity on a surface and then read the surface

        //		p_p3dmEnt->m_pObj->m_object = (ON_Object*)(&(ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object))->m_F[i]);

        l_new = Read3dmSurface(p_p3dmEnt);
        if (!l_new){
            cout<<"maKe a rxON_Surface here "<<endl;
            return l_new;
        }
    }
    return l_new;
}//int Read3dmBrep

RXEntity_p  Read3dmMesh(struct PC_3DM_Entity * p_p3dmEnt)
{
    RXEntity_p  l_new = NULL;
    ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;

    ON_String l_strname = l_pObj->m_attributes.m_name;

    if (l_strname.IsEmpty())
        l_strname = ON_String("amesh");
    l_strname.MakeLower();

    //Before oppenening the HOOPS segment we have to take care of the name syntaxe
    //PART of code copied form dxfin.c line 203->216
    int q = l_strname.Length();
    assert(q < 256);
    char * lp = strpbrk(l_strname.Array(),IGS_NAME_SEPARATOR); // to just before the first $. () OK for entity name but NOT Hoops name
    char * l_atts = NULL;
    char * l_NewAtts = NULL;


    int l_k = 32;
    l_atts = (char*)CALLOC(1,1024);
    if(lp) {
        strcat(l_atts," N>");// put the tail of name in atts
        strcat(l_atts,lp);
        assert(strlen(l_atts) < 1023);
        *lp=0;
    }


    const rxON_Mesh* l_msh= dynamic_cast<const rxON_Mesh*>(l_pObj->m_object);
    if ( !l_msh )
    {
        if (l_atts) RXFREE(l_atts);
        if (l_NewAtts) RXFREE(l_NewAtts);
        return 0;
    }
    HC_Open_Segment(l_strname.Array());
    //At this stage everything is in the same segment
    ON_Layer l_layer;
    ON_Material l_material;
    if (l_pObj->m_attributes.m_layer_index>0)
        l_layer = p_p3dmEnt->m_pModel->m_layer_table[l_pObj->m_attributes.m_layer_index];
    if (l_pObj->m_attributes.m_material_index>0)
        l_material = p_p3dmEnt->m_pModel->m_material_table[l_pObj->m_attributes.m_material_index];
    ON_Color l_c;
    //Set the color from the 3DM model
    switch (l_pObj->m_attributes.ColorSource())
    {
    case 0:   //color_from_layer
        l_c = l_layer.Color();
        break;
    case 1:   //color_from_object
        l_c = l_pObj->m_attributes.m_color;
        break;
    case 2:   //color_from_material
        l_c = l_material.Ambient();
        break;
    }
    HC_Set_Color_By_Value ("markers", "RGB", float(l_c.Red()/255.),float(l_c.Green()/255.),float(l_c.Blue()/255.));

    l_msh->DrawHoops ();
    HC_Close_Segment();

    if (l_pObj) l_pObj= NULL;

    if (l_atts) RXFREE(l_atts);
    if (l_NewAtts) RXFREE(l_NewAtts);
    return l_new;
}//RXEntity_p  Read3dmMesh

RXEntity_p  Read3dmPointCloud(struct PC_3DM_Entity * p_p3dmEnt)
{
    RXEntity_p  l_new = NULL;
    return l_new;
}//RXEntity_p  Read3dmPointCloud

int Read3dmProperties(SAIL* p_s, struct PC_3DM_Entity * p_p3dmEnt)
{
    int l_Result = 0;
    ON_3dmProperties l_prop;
    l_prop = p_p3dmEnt->m_pModel->m_properties;

    //Application
    ON_3dmApplication l_app(l_prop.m_Application);
    ON_String l_appURL(l_app.m_application_URL);
    ON_String l_appDetails(l_app.m_application_details);
    ON_String l_appName(l_app.m_application_name);

    //Notes
    ON_3dmNotes l_notes(l_prop.m_Notes);
    Read3dmNotes(p_s,p_p3dmEnt,l_notes);

    //Preview image
    ON_WindowsBitmap l_WndBmp(l_prop.m_PreviewImage);
    //that is just an image of the model

    //Revision history
    ON_3dmRevisionHistory l_History(l_prop.m_RevisionHistory);
    //	l_History.m_create_time
    //	l_History.m_last_edit_time
    //	int l_revisionCount = l_History.m_revision_count;
    ON_String l_CreatedBy(l_History.m_sCreatedBy);
    ON_String l_LastEditedBy(l_History.m_sLastEditedBy);

    return l_Result;
}//int Read3dmProperties

int Read3dmNotes(SAIL * p_sail, struct PC_3DM_Entity * p_p3dmEnt, const ON_3dmNotes & p_notes)
{
    ON_String l_Notes(p_notes.m_notes);

    //int l_Result = 0;
    int iret=0;
    int l_pos = l_Notes.Find("\r");   // \r <--> ACII 13
    while (l_pos!=-1)
    {
        //        l_Notes.SetAt(l_pos,char(10));  //10 is is th ASCII value for "new line"
        l_Notes.SetAt(l_pos,'\n');  // Oct 2013  char 10 didnt work lets guess
        l_pos = l_Notes.Find("\r");
    }

    char tmpname[256];
    static FILE *fp = NULL; //, *l_3dm_two;
    strcpy(tmpname,"RLX_3DM_XXXXXX");
    qDebug()<<" TODO this can be a qTemp";
#ifdef WIN32 
    if(!_mktemp(tmpname)) {
        p_sail->OutputToClient("tmpname",3);
        return 0;
    }
#else
    if(-1 ==mkstemp(tmpname)) {
        p_sail->OutputToClient("tmpname",3);
        return 0;
    }
#endif

    fp = fopen(tmpname,"w");
    if (!fp)
        return -1;

    if (!l_Notes.IsEmpty())
        fprintf(fp,"%s",l_Notes.Array());

    fclose(fp);

    FILE *stream;
    char line[256];

    if( (stream = fopen( tmpname, "r" )) != NULL )
    {
        if( fgets( line, 256, stream ) == NULL)
            cout<< "no notes in this 3dm file"<<endl;
        else
            cout<< "NOTES firstline is <"<<line<<">\n";
        fclose( stream );
    }
    int l_depth = 0;

    if (p_sail ->startmacro(tmpname,&l_depth)==-1)
        iret = 0;
    else
        iret = 1;
    if(fp) {
#ifdef WIN32
        sprintf(line,"del %s\n",tmpname);
        system(line);
#else
        sprintf(line,"rm -r %s\n",tmpname);
        system(line);
#endif
    }


    return iret;
}//int Read3dmNotes
//
//char * ONStringToChar(const ON_wString & p_str)
//{
//	char * l_buff = NULL;
//	int l_L = p_str.Length();
//	assert(0);
//	l_buff = (char*)malloc(sizeof(char)*(l_L+1));
//	assert(l_buff);
//
//	int i;
//	for (i=0;i<l_L+1;i++) l_buff[i] = (char)0;
//	for (i=0;i<l_L;i++) l_buff[i] = p_str[i];
//	
//	return l_buff;
//}//int ONStringToChar

ON_String PC_StripTail( ON_String & name,  char* tok)
{
    // In name, find the first of the characters in tok .
    // copy the tail into tail and truncate 'name'
    ON_String tail;
    char *lp = tok; int  kmin = name.Length() + 2;
    int k=-1, k0=kmin;
    for(lp=tok;*lp;lp++){
        k = name.Find (*lp);
        if(k<0) continue;
        kmin=min(kmin,k);
    }
    if(kmin!=k0) { // found
        tail = name.Mid(kmin+1);
        name = name.Left(kmin);
    }
    return tail;
}

ON_String PC_3dm_create_attributes(struct PC_3DM_Entity *p_p3dmEnt) 
{// July 2003 now alters the name and splits it into atts
    QString envqstring ;
    char *lp;
    ON_String  a,en;
    int retVal=0;
    int l_LayerID = p_p3dmEnt->m_pObj->m_attributes.m_layer_index;
    ON_String l_LayerName = p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();
    l_LayerName.MakeUpper();
    ON_String l_strname = p_p3dmEnt->m_pObj->m_attributes.m_name;
    l_strname.MakeLower();
    l_strname = RemoveComents(l_strname);
    ON_String  l_name = l_strname;

    class RXSail *sail = p_p3dmEnt->m_sail;
    RXEntity_p atte =sail->Entity_Exists(l_LayerName.Array(),"drawinglayer");
    if(atte) {
        envqstring = atte->AttributeString();
    }
    else {
        envqstring = getenv(l_LayerName.Array());
        if(envqstring.isEmpty()) {
            char buff[1256];
            sprintf(buff," envEdit:GAUSS: %s" ,(l_LayerName.Array()));
#ifdef _X
            Execute_Script_Line(buff,NULL);
#else
            retVal=sail->OutputToClient(QString( "Please define a drawingLayer for " ) + QString(l_LayerName.Array() ),3);


            switch (retVal) {
              case QMessageBox::Yes:
                  // Save was clicked
                  break;
              case QMessageBox::No:
                  // Don't Save was clicked
                   return ON_String("UserCancel");
              case QMessageBox::Cancel:
                  // Cancel was clicked
                  return ON_String("UserCancel");
              default:
                  // should never be reached
                 return ON_String("UserCancel");
                  break;
            }



#endif
            envqstring = getenv(l_LayerName.Array ());
        }
        if(!envqstring.isEmpty()) {
            char buff[1256]; int depth=0;
            sprintf(buff,"drawinglayer:%s:%s" ,l_LayerName.Array (),qPrintable(envqstring));
            atte = Just_Read_Card(sail,buff,"drawinglayer",l_LayerName.Array () ,&depth );
            atte->Resolve();
        }
    }

    // trim name off at the first occurrence of a character in ON3DM_NAME_SEPARATOR
    // the first part stays in l_name. The second part goes into en.
    { //October 2008 the strpbrk didnt like
        int kkkk = l_name.Find(ON3DM_NAME_SEPARATOR);
        if(l_name.Length()){ // to just before the first $. () OK for entity name but NOT Hoops name
            if((lp = strpbrk(l_name.Array(),ON3DM_NAME_SEPARATOR))){ // to just before the first $. () OK for entity name but NOT Hoops name
                kkkk = (lp-l_name.Array());
                en = l_name.Mid(kkkk+1);
                l_name = l_name.Left(kkkk);
            }
        }
    }

    a=ON_String(",$3dm ,");
    if(l_name.Length ()) {a=a+ON_String("($name=")+l_name+ON_String("),");}
    if(l_LayerName.Length ()) {a=a+ON_String("($layer=")+l_LayerName +ON_String("),");}
    if(en.Length()) {a=a+ON_String(", N>> ,")+en; 	}

    RXAttributes rxa(a); // attributes from the object
    QString ss(envqstring);

    // Splice the drawing-layer atts to those from the object,
    //  the '1' means giving priority  =to those originating on the object

    rxa.Splice(ss, g_World->m_DrawingLayerHasPrecedence);
    return ON_String(rxa.GetAll().c_str());

}//char *PC_3dm_create_attributes

RXEntity_p  Read3dmObject(struct PC_3DM_Entity * p_p3dmEnt,double p_scalefactor)
{
    const ON_Object* l_obj=0;
    RXEntity_p  l_new = NULL;

    ON_Geometry *l_g = ( ON_Geometry *) ON_Geometry::Cast(p_p3dmEnt->m_pObj->m_object);

    if(l_g)
        l_g->Scale (p_scalefactor);

    {
        ON_Brep *l_brepobj = ( ON_Brep *) ON_Brep::Cast(p_p3dmEnt->m_pObj->m_object);
        if (l_brepobj) {
            p_p3dmEnt->m_pObj ->m_bDeleteObject=true;
            l_new = Read3dmBrep(p_p3dmEnt);
            p_p3dmEnt->m_pObj ->m_bDeleteObject=true;// here p_p3dmEnt->m_pObj is the Brep
        }
    }

    l_obj = (ON_Point * )ON_Point::Cast(p_p3dmEnt->m_pObj->m_object);
    if (l_obj)
        l_new = Read3dmPoint(p_p3dmEnt);
    else{
        l_obj = rxON_Mesh::Cast(p_p3dmEnt->m_pObj->m_object);
        if (l_obj)
            l_new = Read3dmMesh(p_p3dmEnt);
        else{
            l_obj = dynamic_cast<const ON_Curve*>(p_p3dmEnt->m_pObj->m_object);
            if (l_obj)
                l_new = Read3dmCurve(p_p3dmEnt);
            else{
                l_obj = ON_Surface::Cast(p_p3dmEnt->m_pObj->m_object);
                if (l_obj)
                    l_new = NULL ; //WE HAVE ALLREADY READ THE SURFACE FROM THE BREP; Read3dmSurface(p_p3dmEnt);
                else{
                    l_obj = ON_PointCloud::Cast(p_p3dmEnt->m_pObj->m_object);
                    if (l_obj)
                        l_new = Read3dmPointCloud(p_p3dmEnt);
                }
            }
        }
    }
    if (!l_new)
    {
#ifdef FULLDEBUG

        int l_LayerID = p_p3dmEnt->m_pObj->m_attributes.m_layer_index;
        ON_String l_LayerName = p_p3dmEnt->m_pModel->m_layer_table[l_LayerID].LayerName();

        ON_String l_w1 = PC_StripTail(l_LayerName,ON3DM_NAME_SEPARATOR);
        if (!l_w1.IsEmpty()) l_LayerName=l_w1;
        {
            ON_String l_nname = p_p3dmEnt->m_pObj->m_attributes.m_name;
            l_w1 = PC_StripTail(l_nname,ON3DM_NAME_SEPARATOR);
            if (!l_w1.IsEmpty())  l_nname=l_w1;
        }

        printf(" cannot read object %s in Layer %s \n", l_name, l_LayerName);
#endif
    }
    else { // 1 July 2004

        l_new->m_pONMO =rxONConvertObject(  p_p3dmEnt->m_pObj);  //to connect the ONX_Model_Object
    }
    return l_new;
}//int Read3dmObject


int PCN_ON_NURBSCurve_To_Poly(struct PC_3DM_Entity * p_p3dmEnt,int *c,VECTOR **poly) //WHY NURBSFORM??
{
    const ON_Object * l_obj = p_p3dmEnt->m_pObj->m_object;
    const ON_Curve * l_crv = dynamic_cast<const ON_Curve*>(l_obj);
    if (!l_crv)
        return 0;
    const ON_PolylineCurve* l_plc =  ON_PolylineCurve::Cast(l_crv);
    if(l_plc) {
        const ON_Polyline *pl = &(l_plc->m_pline);
        int i, np = *c = pl->Count();
        *poly = (VECTOR*)MALLOC((*c)*sizeof(VECTOR));
        for(i=0;i<(*c);i++)
        {
            (*poly)[i].x = (float) (*pl)[i].x;
            (*poly)[i].y = (float) (*pl)[i].y;
            (*poly)[i].z = (float) (*pl)[i].z;
        }
        //cout<<endl;
        return (*c>0);
    }
    *c = g_Spline_Division;
    if (*c>1){

        ON_Interval l_domain = l_crv->Domain();
        double l_u=l_domain.Min();
        double l_e = 1.0;

        l_e = l_domain.Length()/((double) (*c -1));
        ON_3dPoint l_pt(0.0,0.0,0.0);
        int i;
        *poly = (VECTOR*)MALLOC((*c)*sizeof(VECTOR));

        for(i=0;i<(*c)-1;i++)
        {
            if (l_crv->EvPoint(l_u,l_pt))
            {
                (*poly)[i].x =(float) l_pt.x; //    poly[i]->x = l_pt.x;
                (*poly)[i].y = (float)l_pt.y;
                (*poly)[i].z = (float)l_pt.z;
                l_u+=l_e;
            }
        }
        if (l_crv->EvPoint(l_domain.Max(),l_pt))  // To prevent of numerical error
        {
            (*poly)[*c-1].x = (float)l_pt.x;
            (*poly)[*c-1].y = (float)l_pt.y;
            (*poly)[*c-1].z =(float) l_pt.z;
        }
    }
    else {// c zero
        *poly=0; *c=0;
    }
    return 1;

}//int PCN_ON_NURBSCurve_To_Poly

void PC_3dm_Initialize(rxONX_Model &model ) {

    // file properties (notes, preview image, revision history, ...)
#ifndef ON_V3
    model.m_3dm_file_version= 4;		//l_ONXModel->m_3dm_file_version; April 2008
    model.m_3dm_opennurbs_version= 4;
#else
    l_modelToWrite.m_version     = 3;// l_ONXModel->m_version;
#endif
    // set revision history information
    model.m_properties.m_RevisionHistory.NewRevision();
    
    // set application information
    model.m_properties.m_Application.m_application_name = "Relax suite of programs";
    model.m_properties.m_Application.m_application_URL = "http://www.peterheppel.com";
    model.m_properties.m_Application.m_application_details = "Based on Example program in OpenNURBS V4 toolkit.";

    model.m_properties.m_Notes.m_bVisible = true;

    // file settings (units, tolerances, views, ...)
    model.m_settings.m_ModelUnitsAndTolerances.m_unit_system = RLX_DEFAULT_UNITS;
    model.m_settings.m_ModelUnitsAndTolerances.m_absolute_tolerance = 0.001;
    model.m_settings.m_ModelUnitsAndTolerances.m_angle_tolerance = ON_PI/1800.0; // radians
    model.m_settings.m_ModelUnitsAndTolerances.m_relative_tolerance = 0.001; // 1%

} //PC_3dm_Initialize


/* Writing a 3dm
1) if the sail has non-null m_p3dmModel, this model gets copied into the output file
2) For all  entities which have no ON_Object, append their Line into the notes, except for 
    case INTERPOLATION_SURFACE:
    case DXF :
    case PCE_TPN :
    case PCE_IGES :
    case PCE_3DM:
 which we skip
 and
    case SEAMCURVE
    case SITE:
    case RELSITE:
 whose geometry we write.
theres a confusion with layer indices. 

Finally, any other hoops objects which arent RXEntities get written into layer 0
but it's only nurbs surfaces and polylines.   which is a shame because hoops2on knows about

NurbsSurface ,Marker,Polyline,Polygon,Line, NurbsCurve
*/
int PC_3dm_Write(SAIL *p_pSail, const char * p_filename )//struct PC_3DM_Model *p_p3dm,
{
    return p_pSail->Write_3dm(p_filename);
}


int WriteEntityTo3dm(rxONX_Model * p_Model,RXEntity_p  p_e, ON_String & p_Notes)
{
    if (p_e->m_pONMO)
    {
        ONX_Model_Object& mo = p_Model->m_object_table.AppendNew();

        if (ON_Surface::Cast(p_e->m_pONMO->m_object))
            mo.m_object = ((ON_Surface*)p_e->m_pONMO->m_object)->NurbsSurface();
        else
            mo.m_object = p_e->m_pONMO->m_object;
        mo.m_bDeleteObject = false;
        mo.m_attributes.m_layer_index = GetONXObjLayer(p_Model,p_e);
        mo.m_attributes.m_name = ON_String(ON_String(p_e->name()) + ON_String("\t") + ON_String(p_e->GetLine()));
    }
    else
    {
        p_Notes += ON_String(p_e->name()) + ON_String("\t") + ON_String(p_e->GetLine()) + ON_String("\n");
        //int notanONObj=1;
    }
    return 1;
}//int WriteEntityTo3dm

int GetONXObjLayerByName(rxONX_Model * p_Model,ON_String p_name)
{
    ON_String l_layer(p_name);
    l_layer.MakeUpper();
    ON_String l_temp;
    int i,l_n = p_Model->m_layer_table.Count();
    for (i=0;i<l_n;i++)
    {
        l_temp = p_Model->m_layer_table[i].LayerName();
        l_temp.MakeUpper();
        if (l_layer==l_temp)
            return i;
    }
    return -1;
}//int GetONXObjLayer
int GetONXObjLayer(rxONX_Model * p_Model,RXEntity_p  p_e)
{
    char l_w[128];  wchar_t c=0;
    p_e->AttributeGet("$layer",l_w);

    ON_wString l_layer(l_w);
    if(!l_layer.IsEmpty ()) c = l_layer[0];
    if(!ONX_IsValidNameFirstChar( c))
        l_layer=ON_wString("L_") + l_layer;
    l_layer.MakeUpper();
    ON_String l_temp;
    int i,l_n = p_Model->m_layer_table.Count();
    for (i=0;i<l_n;i++)
    {
        l_temp = p_Model->m_layer_table[i].LayerName();
        l_temp.MakeUpper();
        if (l_layer==l_temp)
            return i;
    }
    return 0;
}//int GetONXObjLayer

int Make_Name_Unique(struct PC_3DM_Entity * p_p3dmEnt, const int & p_LayerID)
{
    static int l_id = 0;
    rxONX_Model *        l_pModel = p_p3dmEnt->m_pModel;
    ONX_Model_Object * l_pObj = p_p3dmEnt->m_pObj;

    ON_String l_strname(l_pObj->m_attributes.m_name);
    //To stop before the comented part
    l_strname = RemoveComents(l_strname);
    //TO make sure the name is not ""
    if (l_pObj->m_attributes.m_name == ON_String(""))
        l_pObj->m_attributes.m_name = ON_String("_3dm")+l_pModel->m_layer_table[l_pObj->m_attributes.m_layer_index].LayerName();

    int l_n = l_pModel->m_object_table.Count(); //m_count;
    int i=0;
    ON_String l_temp;
    char l_name[256];
    char l_tmpname[256];
    char l_others[256];

    Get_First_Words(l_strname.Array(),l_name,l_others);	/* assumes they are both MALLOCed already */

    for (i=0;i<l_n;i++)
    {
        if (l_pObj->m_object==l_pModel->m_object_table[i].m_object)
            continue;
        if (p_LayerID>-1)
            if (p_LayerID!=l_pModel->m_object_table[i].m_attributes.m_layer_index)
                continue;
        l_strname = l_pModel->m_object_table[i].m_attributes.m_name;
        Get_First_Words(l_strname.Array(),l_tmpname,l_others);	/* assumes they are both MALLOCed already */
        if (ON_String(l_name)==ON_String(l_tmpname))
        {
            l_id++;
            char l_buff[10];
            sprintf(l_buff,"%d",l_id);
            l_pObj->m_attributes.m_name = ON_String(l_tmpname) + ON_String(l_buff) + l_others;
            return Make_Name_Unique(p_p3dmEnt, p_LayerID);
        }
    }

    return 0;
}//int Make_Name_Unique

ON_String RemoveComents(const ON_String & p_line)
{
    //To stop before the comented part
    int l_coment = p_line.Find("!");
    if (l_coment>=0)
    {
        ON_String l_line = p_line.Left(l_coment);
        l_line.TrimRight();
        l_line.TrimLeft();
        return l_line;
    }
    return p_line;
}//ON_String RemoveComents

struct TwinEntity Generate_Curve_From_ONO(SAIL *p_sail,const char*attsin,
                                          const char*handle,char*layer,const char *namein,ONX_Model_Object*p_ono){

    // p_ono is ONX_Model_Object*
    //May 2003  Finishes it too
    // Sept 2002. Modified for IGES input.  In this case, the Handle is the entity number.  This is NOT persistent!
    // June 2001.  the keyword '$basecurve' means just generate a 3dcurve
    // the curve is shifted to start at the origin
    // its name doesnt have 'c' appended

    // Feb 2001  the curve is given depth=1 so it is NOT put in the script

    // so edits will be lost.
    // but this is a work-around for a partial-atts bug
    // We tried again with depth 0 and no transfer of atts. IE rely on the partial-atts
    // system.  A test

    /* Generate a 3dcurve entity and a cut curve
3dcurve	x1c  -1	1	0  6	4	0
curve	  x1	 	 	x1c 	0 	flat XYZabs draw cut
The Curve is made with depth=0 so it will be put in the script file for editing.
The 3DCurve is not.
Hence the 3DCurve may be edited in ACAD and picked up again.
The CURVE may be edited in the scriptfile and will not be overwritten.

*/

    int depth=1 ;
    int SC_Too=1;
    unsigned int l_k;
    RXSTRING name,n1,n2,extra;
    char buf[256];


    RXAttributes atts;
    RXSTRING stype;
    RXEntity_p  bce=NULL;
    class RXSeamcurve *sce=NULL;
    char Bcurve_Append = ' ';

    int qq;
    struct TwinEntity  rc = {NULL,NULL};

    //	printf(" Generate a Curve  in layer %s with %d points\n ",ent->group[8].value, c);
    l_k=32;
    if(attsin) l_k += strlen(attsin); // already contains the tail of the 3dm object nam
    if(namein) l_k += strlen(namein); // still has a tail

    if(attsin) atts.Add(attsin);

    if(atts.Find("edge"))
        stype = L"edge";
    else
        stype = L"curve";

    if(atts.Find("$basecurve"))
        SC_Too=0;
    if(namein) {
        qq = strlen(namein);
        name=TOSTRING(namein) ;
    }
    else if(!atts.Extract_Word(TOSTRING("name"),name))
        name= TOSTRING(layer)+TOSTRING("_")+ TOSTRING(handle);// ;sprintf(name,"%s_%s",  layer, handle);

    int ncount=0;
    if(atts.Extract_Word(L"$n1",n1)&&!n1.empty())
        ncount++;
    if(atts.Extract_Word(L"$n2",n2)&&!n2.empty())
        ncount++;
    if(ncount==2)
        extra=L"$nocut,$nosnap,";
    qq = name.length ();
    size_t  q = name.find_first_of(TOSTRING(IGS_NAME_SEPARATOR) );
    if(q != string::npos) name.erase(q);


    if(extra.empty ()) extra=L" ";
#ifdef HOOPS
    Make_Valid_Segname(name);
#endif

    char * cline =0;
    string nnn = ToUtf8(name);
    if(SC_Too){
        Bcurve_Append = 'c';

        cline =(char*)CALLOC(144+atts.Length () + 2*name.length() + 3*stype.length () + n1.length() +  n2.length() + extra.length()+12, 1);

        sprintf(cline,"%S :%S : %S : %S :%Sc :0 :$%S%S %S ",L"seamcurve",name.c_str(),n1.c_str(),n2.c_str(),name.c_str(),stype.c_str(),extra.c_str(),atts.GetAll().c_str () );
        RXSTRING sline;

        sline =L"seamcurve"; sline +=RXE_LSEPSTOWRITE;
        sline += name;
        sline +=RXE_LSEPSTOWRITE+ n1
                +RXE_LSEPSTOWRITE+ n2
                +RXE_LSEPSTOWRITE+ name+L"c"
                +RXE_LSEPSTOWRITE+L"0"
                +RXE_LSEPSTOWRITE +L"$"
                +stype  + L" " + extra  + L" " +atts.GetAll();

        sprintf(buf,"seamcurve");

        if(  !(p_sail->Entity_Exists(nnn.c_str(),to_string(stype.c_str()).c_str())|| p_sail->Entity_Exists(nnn.c_str(),"seamcurve")   ))
            sce = (sc_ptr)Just_Read_Card(p_sail,cline,"seamcurve",nnn.c_str(),&depth);  // was type
        else
            cout<< stype.c_str()<<", '"<< nnn.c_str() <<"' already exists \n";

        if(p_ono  && sce) sce->m_pONMO = p_ono; // dxf read usually has p_ono null.
    }

    // the basecurve - an empty line. just type,name,atts

    cline = (char*)REALLOC (cline, (128 + 48  + atts.Length())*sizeof(char));
    sprintf(cline,"3dcurve : %S%c : ", name.c_str(),Bcurve_Append);

    if(atts.Find("base_mould"))
        strcat(cline,"  $moulded");

    sprintf(buf,"%S%c",  name.c_str(),Bcurve_Append);
    assert(strlen(buf) < 255);
    depth=1;
    // May 20 2008 try inserting it directly as a 'basecurve' to avoid the rename was "3dcurve"
    // slow to call twice
    if( !(p_sail->Entity_Exists(buf, "3dcurve") || p_sail->Entity_Exists(buf, "basecurve"))  )
    {
        bce = Just_Read_Card(p_sail,cline,"3dcurve",buf,&depth);
        //thomas 01.03.04
        bce->m_pONMO = p_ono;//sce->m_pONMO; //peter - Jan 2014

    }
    else
        cout<<" 3dcurve "<<buf<<" already exists "<< name.c_str()<<endl;


    if(bce && bce->Resolve() )
        bce->Needs_Finishing=!bce->Finish();


    // It would be nice to finish here so the ent becomes visible and selectable,
    // but this brings an order-dependency. Esp. when we want to snap to sites
    // which are further down this file.  We cant do it.
    // so instead we just resolve it...
    // It would be much faster if instead of resolving we pushed a task to a TBB parallel_do
    //

  //  if(sce) sce->Needs_Resolving= !sce->Resolve(bce);
    if(sce)
    {int kk= !sce->Resolve(bce) ;
        assert( sce->Needs_Resolving==kk);
    }
    RXFREE(cline);
    rc.e1 =bce; rc.e2=sce;
    return rc; // was sce till July 1 2004
}

