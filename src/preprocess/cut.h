  
  /* cut.h */

 #ifndef _HDR_CUT

#include "griddefs.h"

struct CUT_SEGMENT {
   	double s1min,s1max;	  /* arc lengths of curve1 , each end */
   	double s2min,s2max;	  /* arc lengths of  curve2 , each end */
	VECTOR e1b,e1r,e2b,e2r;		/* black and red space, each end*/
	RXEntity_p scl, *scr;
	RXEntity_p p1, *p2;
	RXEntity_p childSC;
	double crankRight;
	struct CUT_SEGMENT *left,*right;
	};

EXTERN_C int PCC_Find_Curve_Int(RXCurve *C1,
				   double s1s,double s1f,double*s1,
				   ON_3dPoint *v1,ON_3dVector*t1,
				   RXCurve *C2,double s2s,double s2f,double*s2,
				   ON_3dPoint*v2,ON_3dVector*t2);

EXTERN_C int PCC_Check_Cut_Parameters(sc_ptr sc);
EXTERN_C int PCCut_Set_Cut_Limits(sc_ptr sc,double *upper, double *lower);

RXEntity_p Crossed(sc_ptr  ,sc_ptr  , double*, double * );
EXTERN_C int Compute_Cut_Intersections(sc_ptr sc );
EXTERN_C  int Compute_Snap_Intersections(RXEntity_p );
EXTERN_C int Cut_Whole_Curve(RXEntity_p e);

//EXTERN_C int Remove_Distant_Sites(sc_ptr sc,double tol);
EXTERN_C int SearchForCloseSites(sc_ptr sc,double tol);
EXTERN_C int PCC_Create_EndSites_If_Required(sc_ptr sc,double tol);
EXTERN_C int Update_Cut_IAs(RXEntity_p e,struct CUT_SEGMENT **L,int nsegs);
EXTERN_C int Manage_Cut_SCs(RXEntity_p e,struct CUT_SEGMENT **L,int nsegs,int Make_SCs);
EXTERN_C int Delete_Cut_CC(RXEntity_p e);
EXTERN_C int Compute_Cut_SC(RXEntity_p e);
EXTERN_C int Compute_Flat_Cut_Geometry(RXEntity_p e); 

EXTERN_C int Extract_Cranks(struct CUT_SEGMENT* Cut_Tree,struct CUT_SEGMENT***Cut_List,int *ncuts,int* );
EXTERN_C double Cut_Crank(struct CUT_SEGMENT *left,struct CUT_SEGMENT * rite,int sign);
EXTERN_C int Make_End_Ptrs(sc_ptr  sc);

#define  _HDR_CUT 1
#endif
