#ifndef _INTERPXYZ_H_
#define  _INTERPXYZ_H_


EXTERN_C edge *xyToPressure(edge *eseed,double *x,double*y,double *P);
EXTERN_C edge *itriSail(SAIL *sail,int what); //retirefd 2009
EXTERN_C edge *itriSites(RXSitePt **s,int n,const ON_Xform *p_xf);
EXTERN_C RXSitePt **copySailsites( SAIL *sail,int *pn);
EXTERN_C edge *find_dx_at_xy(edge *eseed,double *x,double*y,double *dx,double *dy,double *dz,double *z);
EXTERN_C RXSitePt **copy_site_list(RXSitePt **src,int sn,int *pn);
EXTERN_C double DoubleRegenOneValue(double zeta[3],double in[3]);

EXTERN_C edge *Do_Make_Interp_Surface(RXSitePt ***sl,int nsites,float backg);

namespace ancientmesh{
	EXTERN_C  double recalc(double *zeta,double *tv);
	EXTERN_C 	void *find_dx_at_xy(void *eseed,double *x,double*y,double *dx,double *dy,double *dz,double *z); // the void*s are edge*
	////////////////////////////
 
 
	EXTERN_C  int SameSiteRough(RXSitePt *s1, RXSitePt *s2);
	EXTERN_C  void *LocateRough(RXSitePt *x, void *e, const ON_Xform *p_xf); // the void*s are edge*
	EXTERN_C  int ccw(RXSitePt* p1,RXSitePt* p2,RXSitePt* p3,const ON_Xform *p_xf ) ;
	EXTERN_C  double Double_ccw(const VECTOR &p1,const VECTOR &p2,const VECTOR &p3,const ON_Xform *p_xf);

}
#endif //#ifndef _INTERPXYZ_H_



