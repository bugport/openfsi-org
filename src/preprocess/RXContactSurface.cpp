#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXCurve.h"
#include "RXSeamcurve.h"
#include "RXRelationDefs.h"

#include "entities.h"
#include "resolve.h"
#include "RXContactSurface.h"

RXContactSurface::RXContactSurface(void)
: m_s1(NULL)
, m_c1(NULL)
, m_c2(NULL)
, m_owner(NULL)
{
}

RXContactSurface::~RXContactSurface(void)
{
	cout<< "BUG  \n on deleting a contact surface we must flush references to it from the nodes\n"<<endl;
}
#define DELTA 0.01 // this must be bigger than about 0.005 because GLCP seems to return a small value when the pt is on the edge.

// returns   C_GOING_UP   or  C_GOING_DOWN  depending on the sense of the line, or 0 if no intersection
// DONT test for the vale of t. Any value is OK
	int RXContactSurface::TestPathIntersects( double oldpt[3], double proposedpt[3], 
		double newptxyz[3], double uvw[3],
		double *t)
	{
		int rc=0;
		double u1,u2,l_k;
		ON_Line theLine = ON_Line(ON_3dPoint(oldpt),ON_3dPoint(proposedpt));
		if(m_s2.Intersect(theLine,l_k,u1,u2,1.e-7)) {
			*t=l_k;
			if(u1<m_s2.Domain(0).Min()+DELTA)
				{//cout<< " U1 low"<<endl; 
				return 0;}
			if(u1>m_s2.Domain(0).Max()-DELTA)
				{//cout<< " U1 hi"<<endl; 
				return 0;}
			if(u2<m_s2.Domain(1).Min()+DELTA)
				{//cout<< " U2 low"<<endl; 
				return 0;}
			if(u2>m_s2.Domain(1).Max()-DELTA)
				{//cout<< " U2 hi"<<endl; 
				return 0;}

			ON_3dVector n = m_s2.NormalAt(u1,u2);
			if(ON_DotProduct(n,theLine.Direction ()) > 0 )
				rc =  C_GOING_UP;
			else
			rc = C_GOING_DOWN;
			uvw[0]=u1; uvw[1]=u2; uvw[2]=0.;
		}
		return rc;
	}

	int RXContactSurface::Jacobian(double u, double v, double j[9])
		{
		ON_3dVector du,dv,nn; ON_3dPoint p;
		if(!m_s2.Ev1Der (u,v,p,du,dv))
			return 0;
	
		nn = m_s2.NormalAt(u,v);
		j[0] = du.x; 		j[1] = du.y; 		j[2] = du.z; 
		j[3] = dv.x; 		j[4] = dv.y; 		j[5] = dv.z; 
		j[6] = nn.x; 		j[7] = nn.y; 		j[8] = nn.z; 
		return 1;
	}
int RXContactSurface::Evaluate(const double u1, const double u2, double p_p[3])
{
		ON_3dVector du,dv; ON_3dPoint p;
		if(!m_s2.Ev1Der (u1,u2,p,du,dv))
			return 0;
		p_p[0] = p.x; 		p_p[1] = p.y; 		p_p[2] = p.z; 
		return 1;
}
int RXContactSurface::TestPassEdge(const double u1, const double v1, const double u2, const double v2, double *k){
/*  on the line in uvspace from {u1,v1} to {u2,v2} 
is (u2,v2) outside the domain and if so, by what proportion.
return in *k the proportion from {u1,v1} to {u2,v2} where we first hit the edge
return non-zero if uv2 is past the edge

first, assert that (u1,v1) is within the domain
second.  return 0 if (u2,v2) is inside the space
third if(u2>u1 we test domain(0).Max() else  domain(0).Max() giving K1 as the factor along the line
fourth   if(v2>v1 we test domain(1).Max() else  domain(1).Max() giving K2 as the factor along the line
Then *k is min(k2,k1) and retval is non-zero if K < 1 else 0 
*/	double ku,kv;
	assert((u1>=m_s2.Domain(0).Min()+DELTA && u1<=m_s2.Domain(0).Max()-DELTA));
	assert((v1>=m_s2.Domain(1).Min()+DELTA && v1<=m_s2.Domain(1).Max()-DELTA));

	if( 		u2>=m_s2.Domain(0).Min()+DELTA 
		&&  	u2<=m_s2.Domain(0).Max()-DELTA
		&& 	v2>=m_s2.Domain(1).Min()+DELTA 
		&& 	v2<=m_s2.Domain(1).Max()-DELTA)
		return 0;
// the following will fail is u1==u2, but as we've asserted that u1 is inside and tested that u2 is outside
// we  are insulated 	

	if(u2>u1)	ku = ( m_s2.Domain(0).Max()-DELTA - u1) / ( u2-u1);
	else		ku = ( m_s2.Domain(0).Min()+DELTA - u1) / ( u2-u1);

	if(v2>v1)	kv = ( m_s2.Domain(1).Max()-DELTA - v1) / ( v2-v1);
	else		kv = ( m_s2.Domain(1).Min()+DELTA - v1) / ( v2-v1);

	*k = min(ku,kv);
	assert(*k>=0.);
	if(*k<1.) return 1;

	return 0;
}
int RXContactSurface::TestIsBelow( double p_proposedpt[3], // coordinates are in this model's space.
						   double p_newptuvw[3])  // return 1 if proposed_Point is below the surface
{
	double dz,u1,u2;
	double u1in=ON_UNSET_VALUE;
	double u2in=ON_UNSET_VALUE;
	ON_3dPoint prop (p_proposedpt); 
	
	int rc= m_s2.GetLocalClosestPoint(prop,u1in,u2in,&u1,&u2);
	if(!rc) 
		{
		printf(" GLCP failed on pt %f %f %f\n",prop.x,prop.y,prop.z);
		return 0;
		}
/*		 printf(" GLCP gave (%f %f) on domain ( %f %f ) and ( %f %f) \n", u1,u2,
		 m_s2.Domain(0).Min(),  
		 m_s2.Domain(0).Max(), 
		 m_s2.Domain(1).Min(), 
		 m_s2.Domain(1).Max());*/
	if(u1<m_s2.Domain(0).Min()+DELTA)
		{ //printf(" U1 %f lo %f\n" ,u1, m_s2.Domain(0).Min()); 
		return 0;}
	if(u1>m_s2.Domain(0).Max()-DELTA)
		{ //printf(" U1 %f  hi %f\n" ,u1, m_s2.Domain(0).Max()); 
		return 0;}
	if(u2<m_s2.Domain(1).Min()+DELTA)
		{ //printf(" U2 %f  lo  %f\n" ,u2, m_s2.Domain(1).Min()); 
		return 0;}
	if(u2>m_s2.Domain(1).Max()-DELTA)
		{ //printf(" U2 %f  hi %f\n" ,u2, m_s2.Domain(1).Max()); 
		return 0;}
	ON_3dPoint l_newpt = m_s2.PointAt(u1,u2);

	ON_3dVector nn = m_s2.NormalAt(u1,u2);
	dz = ON_DotProduct(nn,prop-l_newpt);

	if(dz>  -1.e-16) {		// we're on or above the surface.
		return 0; }
	p_newptuvw[0]=u1;	
	p_newptuvw[1]=u2;	
	p_newptuvw[2]=dz;
	//cout<< " touch"<<endl;
	return 1;
#undef DELTA
}

//   maps the surf from c1 to c2, similar to Rhino Flow command
int RXContactSurface::Compute(void)
{
	int rc=0;
// Get an ONCurve for the two SCs,  m_c1 and m_c2;
// m_s2 is a RXON_NurbsSurface, with the FLow method implemented.
// Then we Flow m_s2 from c1 to c2 and draw it.


         ON_Curve *c1,*c2;
	 rxON_PolylineCurve pc1,pc2;
	sc_ptr sc1, sc2;

	if(m_c1->m_pONMO)
                c1= (ON_Curve*)m_c1->m_pONMO->m_object;
	else {
		sc1= (sc_ptr ) m_c1;
		sc1->m_pC[1]->GetONPolyLineCurveFromRelaxPL(pc1);
		c1=&pc1;
	}

	if(m_c2->m_pONMO)
                c2= (ON_Curve*)m_c2->m_pONMO->m_object;
	else {
		sc2= (sc_ptr ) m_c2;
		sc2->m_pC[1]->GetONPolyLineCurveFromRelaxPL(pc2);
		c2=&pc2;
	}
	const ON_Surface *ls = m_s1; 
	ON_NurbsSurface * l_s1 = (ON_NurbsSurface *) ON_NurbsSurface::Cast( m_s1); 
 	ON_TextLog l_dump_tostdout;
	if(l_s1){
		l_dump_tostdout.Print ("This is S1 ");
		 l_s1->Dump(l_dump_tostdout);
		}
	//fflush(stdout);
	cout<< " that was m_s1"<<endl;
	//m_s2.~RXON_NurbsSurface();
	ls->GetNurbForm(m_s2); 
	m_s2.SetDomain(0,0.,1.); // dec 6 2006
	m_s2.SetDomain(1,0.,1.);

	m_s2.Flow(c1,c2);
	HC_Open_Segment_By_Key(m_owner->hoopskey);
		HC_Flush_Contents(".","geometry,subsegments");
		m_s2.DrawHoops("s2");
		m_s2.DrawNormals("s2/normals");
		HC_QSet_Color("s2","faces=red");
	HC_Close_Segment();

	return rc;
}

int RXContactSurface::Print(FILE * fp)
{
	int rc=0;

	rc+=fprintf(fp,"      \t%p",m_c1); 
	if(m_c1  )rc+=fprintf(fp,"\t%s \t%s\n", m_c1->type(),m_c1->name());
	rc+=fprintf(fp,"      \t%p",m_c2); 
	if(m_c2 )rc+=fprintf(fp,"\t%s \t%s\n", m_c2->type(),m_c2->name());
	rc+=fprintf(fp,"      \t%p",m_owner); 
	if(m_owner  )rc+=fprintf(fp,"\t%s \t%s\n", m_owner->type(),m_owner->name());


	ON_TextLog l_tl(fp);
	if(m_s1) {
		fprintf(fp,"m_s1\n");
		m_s1->Dump(l_tl);
	}
	fprintf(fp," m_s2\n");
	m_s2.Dump(l_tl);

	return rc;
}

int RXContactSurface::Resolve(RXEntity_p e){

// kw	name	isurf	sc1  	sc2  	atts
	int rc=1;
	ON_String l_line = e->GetLine();
	int k,k0=0;
	ON_ClassArray<ON_String> words;
	while( (k=l_line.Find(':'))  >=0) {
		ON_String w = l_line.Mid(k0,k-k0);
		words.AppendNew()=w;
		l_line=l_line.Mid(k+1);
	}
	words.AppendNew()=l_line;
	if(words.Count() < 5) return 0;
	ON_String n,s,c1,c2,atts;
	n =words[1];	n.TrimLeft();	n.TrimRight();
	s =words[2];	s.TrimLeft();	s.TrimRight();
	c1=words[3];	c1.TrimLeft();	c1.TrimRight();
	c2=words[4];	c2.TrimLeft();	c2.TrimRight();
	atts=words[5];
	RXEntity_p es, ec1, ec2;
	es  = e->Esail->Get_Key_With_Reporting("interpolation surface",s.Array());
	ec1 = e->Esail->Get_Key_With_Reporting("curve,seam,seamcurve",c1.Array());
	ec2 = e->Esail->Get_Key_With_Reporting("curve,seam,seamcurve",c2.Array());
	if(!es || !ec1 || ! ec2)
		return 0;

	ON_Surface *l_surf;
	RXContactSurface* c = new RXContactSurface();
	c->m_owner =e;
	e->SetDataPtr(  c);
	c->m_c1 = ec1;
	c->m_c2 = ec2;
 	l_surf = (ON_Surface* ) ON_Surface::Cast(es->m_pONMO->m_object);
	c->m_s1 = l_surf;	
	l_surf->SetDomain(0,0.,1.); // dec 6 2006
	l_surf->SetDomain(1,0.,1.);
	
	assert(	c->m_s1 );
	
	ON_TextLog l_tl(stdout);



	c->m_s1->GetNurbForm((c->m_s2));
	c->m_s2.SetDomain(0,0.,1.); // dec 6 2006
	c->m_s2.SetDomain(1,0.,1.);
	//c->m_s2.Dump(l_tl); cout<< "that was m_s2"<<endl;


     if(e->PC_Finish_Entity("contact surface",n.Array(),c,(long)0 ,NULL,atts.Array(),e->GetLine())) {
			rc = 1;
			e->Needs_Resolving= 0;
			e->Needs_Finishing = 0;
			e->SetNeedsComputing();
	     }

		es->SetRelationOf(e,child,RXO_ISURF_OF_CS);;
		ec1->SetRelationOf(e,child|niece,RXO_C1_OF_CS); 
		ec2->SetRelationOf(e,child|niece,RXO_C2_OF_CS); 
#ifdef USE_NIECES
		Push_Entity(e ,&(ec1->NIECES));
		Push_Entity(ec1 ,&(e->aunts));
		Push_Entity(e ,&(ec2->NIECES));
		Push_Entity(ec2 ,&(e->aunts));
#endif
	return rc;
}
