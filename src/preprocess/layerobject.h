// layerobject.h: interface for the layerobject class.
//  Dec 2006 renamed 'n' to 'm_n' because gcc showed it being shadowed
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LAYEROBJECT_H__3F50FF98_90B9_4590_8FEA_819608C6721D__INCLUDED_)
#define AFX_LAYEROBJECT_H__3F50FF98_90B9_4590_8FEA_819608C6721D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "opennurbs.h"

typedef class layernode Lnode; 
typedef class layerply Lply; 
typedef class layervertex Lvertex; 
typedef class layerlink  Llink; 
class layertst; 
class AMGTapingZone;

#define LLNK_NODAL_TOL 1.0e-12		

#define LLNK_NODE_MAX		1		
#define LLNK_OPEN			2  // ONLY on links.
#define LLNK_VISITED		4 // for nodes
#define LLNK_IN_STACK		8
#define LLNK_EMPTY  		16	// only on nodestack members. 
#define LLNK_NODE_DA  		32	
#define LLNK_NODE_DDA  		64	

#define LLNK_INTERESTING	128
		
#define LLNK_LINKFWDS		256			//used temporarily in layernodesort.
#define LLNK_MARK			512
#define LLNK_IS_PLYEDGE		1024 // for a layervertex, means it is on the edge of its ply.
#define LLNK_FIRST_TRAP_BOUNDARIES	2048
#define LLNK_TRACED			4096   // could be merged with VISITED
#define LLNK_LEFT			8192
#define LLNK_IS_WORLDEDGE	16384  // For layernode and layerlink, it's on the edge of the world
#define LLNK_RIGHT			32768   // used to flag stack members left, right or South ahead.
#define LLNK_DEAD			65536
#define LLNK_DIVIDEALL		131072
#define LLNK_NODE_MAXSQ		262144	 
#define LLNK_DESERIALIZE	524288 	
#define LLNK_FROM_SURFACES	1048576 	
#define LLNK_NORTH			2097152 
#define LLNK_EAST			4194304 
#define LLNK_SOUTH			8388608 
#define LLNK_WEST			16777216 		
#define LLNK_RESAMPLE		33554432
// #define LLNK_  SPARE    	67108864
#define LLNK_IS_POLE		134217728
#define LLNK_NO_CALCULATE	268435456

//#define LLN K_DRAW WITHS CRIM		536870912

/*

268435456
536870912
1073741824
2147483648

  */


#define increment(i,c) (i==c-1) ? 0 : i+1;
#define decrement(i,c) (i==0) ? c-1 : i-1; 


class layerobject  
{
public:
	void SetTZ(AMGTapingZone  *p) { m_tz=p;}
	AMGTapingZone *GetTZ()const { return m_tz;}
	virtual int Print(FILE *fp);
		ON_2dVector GetRefVector() ;
		virtual	char * Deserialize(char *p_s);// Fills out the object from p_s. returns the ptr to the next char
		virtual	ON_String Serialize(); // allocates and returns a character array image of the object
		int FlagGetAll() const;
		int FlagClear();
		int FlagQuery(const int) const;
		void FlagClear(const int);
		void FlagSet(const int);
		layerobject();
		virtual ~layerobject();

		int m_n;
		layertst * m_lt;
protected:
	int m_flag;
	int SetRefAxis(ON_2dVector p_v);
	ON_2dVector* m_pRefAxis;
	HC_KEY m_hkey;
	AMGTapingZone *m_tz;
private:


};

#endif // !defined(AFX_LAYEROBJECT_H__3F50FF98_90B9_4590_8FEA_819608C6721D__INCLUDED_)
