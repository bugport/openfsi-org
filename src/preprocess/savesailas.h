
#ifndef _SAVESAILAS_H_
#define  _SAVESAILAS_H_


#ifdef _X

int SaveSailAs(Widget parent, Graphic *g,int type);
void SaveSailAsOKCB(
	Widget w,	
	XtPointer client_data,
	XtPointer call_data);
void SaveSailAsCancelCB(
	Widget w,		/*  widget id		*/
	XtPointer client_data,	/*  data from application   */
	XtPointer call_data	/*  data from widget class  */);
#endif
EXTERN_C Widget OpenBuildFile(char *filename);

#endif  //#ifndef _SAVESAILAS_H_

