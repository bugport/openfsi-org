#include "StdAfx.h"
#include <QDebug>
#include <RXSail.h>
#include <fstream>
#include "griddefs.h"
#include "RXGraphicCalls.h"
#include "RXSRelSite.h"
#include "RX_FESite.h"
#include "rxvectorfield.h"
#include "rxON_Extensions.h"
#include "RXAttributes.h"
#include "RXShapeInterpolation.h"
///////////////////////////////////////////////////////////
//RXShapeInterpolationI , the virtual interface for various Shape INterpolation classes
// The goal of all these classes is to generate texture coordinates  and so permit the
// surface to be navigated in (uv) space
///////////////////////////////////////

RXShapeInterpolationI::RXShapeInterpolationI(class RXSail *s, vector<RX_FESite*> &p,const vector<RX_FEedge*> &e )
    :RXInterpolationI(s)
{
    RXAttributes rxa(s->m_attributes );
    QString mappername;

    if(rxa.Extract_Word("$quadmap",mappername)) {
        RXEntity_p mapper = s->Entity_Exists(qPrintable (mappername), "vector field" ) ;
        if (mapper)
            this->m_sail->m_QuadToUnitSquare  =mapper;
    }
}
RXShapeInterpolationI::RXShapeInterpolationI(class RXSail *s)

    :RXInterpolationI(s)
{
    RXAttributes rxa(s->m_attributes );
   QString mappername;
    //    if(rxa.Extract_Word("$uvmap",mappername)) {
    //        RXEntity_p mapper = s->Entity_Exists(mappername.c_str(),"vector field") ;
    //        if (mapper){
    //            this->m_unitSquareToQuad=mapper;
    //        }
    //    }
    if(rxa.Extract_Word("$quadmap",mappername)) {
        RXEntity_p mapper = s->Entity_Exists(qPrintable(mappername), "vector field" ) ;
        if (mapper)
            this->m_sail->m_QuadToUnitSquare  =mapper;
    }
}
RXShapeInterpolationI::~RXShapeInterpolationI()
{

}

//////////////////////////////////////////////////////////////////////////////
//  RXShapeInterpolation. The traditional approach.
// requires the model to project uniquely onto a plane defined by the model's normal vector(user defined)
// , which is a HUGE RESTRICTION
// Construct a Shewchuk triangulation from the list of points and edges.

RXShapeInterpolation::RXShapeInterpolation(class RXSail *s, vector<RX_FESite*> &p,const vector<RX_FEedge*> &e )
    :RXShapeInterpolationI(s,p,e)
    ,pts(p)
    ,Fedges(e)
{
    // scan for zeroes
    int cc=0;
    if(pts.size()>1){
        vector<RX_FESite*> ::const_iterator its = pts.begin(); its++;
        for( ; its!=pts.end(); ++its)
        {
            if(*its==NULL)
                cc++;
        }
    }
    else cout<<" pts small(1)"<<endl;
    if(Fedges.size()>1){
        vector<RX_FEedge*> ::const_iterator ite= Fedges.begin(); ite++;
        for( ; ite!=Fedges.end(); ++ite)
        {
            if(*ite==NULL)
                cc++;
        }
    }
    if(cc){
        cout<<"DANGER RXShapeInterpolation found "<<cc<<"  spaces "<< endl;
    }


}

RXShapeInterpolation::~RXShapeInterpolation(void)
{
}

int RXShapeInterpolation::Dump(const char* fname)
{
    ofstream f(fname);
    if(!f) return 0;
    int nn,i=0;
    vector<RX_FESite*>::const_iterator it,itend;
    RX_FESite* pp;
    f<<"line nn x y z u v nodeNo ShewkIndex "<<endl;
    itend = Points().end();
    for(it=Points().begin(); it!=itend;++it) {
        if(!(*it))
            continue;
        pp = *it;
        nn=pp->GetN();
        f<<i<<" "<< (*it)->x<<" "<< (*it)->y<<" "<< (*it)->z
        <<" \t"<< (*it)->m_u<<" \t"<< (*it)->m_v<<" \t";

        if(this->msh.ShewkIndex.find(nn) != this->msh.ShewkIndex.end() ) f<<" "<<nn<<"  "<<this->msh.ShewkIndex[nn];
        else
            f<< " no ShewkIndex ";
        f<<endl;
        i++;
    }
    f.close();
    return i;

}

int RXShapeInterpolation:: Prepare(const double lambda, const int flag)  // lambda was .75, flag was 0
{
    //  if the sail's atts include   $uvmap=(something)  we look for a vector field named myuvtouv
    // which returns the coordinates in flat space given uv in the unit square.

    if(!Triangulate(0))// XYZ space - but WHY not just use existing triangulation?
        return 0;
    int ok;
    ok=Flatten(lambda ); //calculate UV values in trapezoid spac and places them on the site m_u,m_v
    if(!ok)
    {
        QString s(": flatten not OK (often because you have self-intersecting panels) ");
        s.prepend( this->m_sail->GetQType());
        this->m_sail->OutputToClient(s,3);
        return 0;
    }
    ToUnitSquare(); // places (0-1) values onto the model's nodes using m_QuadToUnitSquare

  if(!Triangulate(1)) // UV space- create a new triangulation on a copy of the UV points
  {
      QString s(": UV-space triangulation returned 0) ");
      s.prepend( this->m_sail->GetQType());
      this->m_sail->OutputToClient(s,1);
  }
    msh.LocateInitialize ();

    //	this->m_xyz2uv=this->m_uv2xyz->Invert(); a nice idea one day- dream on!
    return 1;
}


int  RXShapeInterpolation::Flatten(const double lambda)
{
    this->msh.SetLambda (lambda);
    int ok=  this->msh.Flatten (this->Points());
#ifdef _DEBUG
    cout<<"RXShapeInterpolation ::flatten done."<<endl;
#endif

    return ok;
}
double RXShapeInterpolation::_ValueAt(const ON_2dPoint& p){
    double rc=0;
    assert("Dont use  RXShapeInterpolation::_ValueAt(p ) "==0);
    return rc;
}
int  RXShapeInterpolation::_ValueAt(const ON_2dPoint& p, double*rv)
{
    double rc=0;
    assert("Dont use  RXShapeInterpolation::_ValueAt(p,rv) "==0);
    return rc;
}

int RXShapeInterpolation::Triangulate(const int flag ){ // flag 1 is UV space flag 0 XYZ
    this->msh.Init();
    int rc=0;
     // see http://www.cs.cmu.edu/~quake/triangle.switch.html
    // produce constrained delaunay triangulations
#ifdef _DEBUG
    const char*f1 = "gOpenzCYYa99999";
    const char*f2="gOpenzcCYYa99999";
#else
    const char*f1 ="gOQpenzYYCa99999"; //XYZ
    const char*f2 ="gOQpenzcCYYa99999"; //UV
#endif
    const char*tristring;
    if(!flag) // XYZ
        tristring=f1;
    else
        tristring=f2;
    if(ONLY_EDGEEDGES){ assert(strlen("Only Edge Edges is experimental!!!" )==0);
        if(!flag)
         rc= this->msh.TriangulateForUV(this->m_sail, this->Points(),this->Edges(),"pOenzYY" ,flag);
        else
          rc= this->msh.TriangulateForUV(this->m_sail, this->Points(),this->Edges(),"pOcenzYY" ,flag);
    }
    else
        rc= this->msh.TriangulateForUV(this->m_sail, this->Points(),this->Edges(),tristring,flag);


    return rc;
}


int RXShapeInterpolation::ToUnitSquare() // a bad idea. We cant change pts because it stuffs up the triangulation
{
    int rc=0;
    double rv[3];
    RX_FESite *pp;
    vector<RX_FESite*>::iterator it;
    RXVectorField *f = dynamic_cast< RXVectorField *>(this->m_sail->m_QuadToUnitSquare   );// in ToUnitSquare
    if(f){

        for(it=this->pts.begin(); it!=pts.end(); ++it)
        {
            pp = *it; if(!pp) continue;
            if(pp->m_Site_Flags&PCF_ISMESHSITE){
                if( f->ValueAt(*pp,rv)  ){
                    pp->m_u = rv[0];
                    pp->m_v = rv[1]; rc++;
                }
            }
            //            else {
            //                class RXSRelSite * sss = dynamic_cast<class RXSRelSite * >(pp);
            //                if(sss) qDebug()<<" ToUnitSquare not a MESHSITE "<<sss->name() << sss->Attributes();
            //            }
        }
    }
    if(rc) this->Dump("freshMesh.txt");
    return rc;
}

ON_3dPoint RXShapeInterpolation::CoordsAt(const ON_2dPoint& pin,const RX_COORDSPACE space){// to get XYZ from UV
    ON_3dPoint rc(0,0,0);
    ON_2dPoint p(pin);
    RXVectorField *f = 0;// dynamic_cast< RXVectorField *>(this->m_unitSquareToQuad);
    if(f){
        RXSitePt pp(0.,0.,0.,p.x,p.y,-1);
        double rv[3];

        f->ValueAt(pp,rv);
        p.x=rv[0]; p.y=rv[1];
    }
    int np, kk, ii[3]; double ww[3];
    np=this->Points().size();
    kk= this->msh.Locate(p,ii,ww); // in coordsAt
    if(kk==BAD )
        qDebug()<<" RXMesh::Locate returned BAD  u= "<<p.x<<" v= " << p.y<<"ii = "<<ii[0]<<ii[1]<<ii[2] <<"/"<< np  ;
    // here ii may be beyond the end of pts
    for(int k=0;k<3;k++) {
        if(ii[k]<1 || ii[k]>= np)
        {
         //   qDebug()<<" non-positive ii[k] u= "<<p.x<<" v= " << p.y<<"ii = "<<ii[0]<<ii[1]<<ii[2] <<"w = "<<ww[0]<<ww[1]<<ww[2] ;
            continue;
        }
         rc+=this->Pt(ii[k]).DeflectedPos (space) * ww[k];
    }
    return rc;
}

    class RX_FESite& RXShapeInterpolation::Pt(const int n)
    {
        if(n<0 ||n>= pts.size()) {
            qDebug()<<" RXShapeInterpolation::Pt index OOR"<<n<<" of "<<pts.size();
        }
        return *(pts[n]);
}
//////////////////////////////////////////////////////////////////////////////////////////////////
//
//  RXShapeInterpolationP is a similar class which  accepts a vector of points not ptr-to-point
//
///////////////////////////////////////////////////////////////////////////////////////////////////

RXShapeInterpolationP::RXShapeInterpolationP(class RXSail *s)
    :RXInterpolationI(s)
    ,m_ndata(0)
{
}

RXShapeInterpolationP::~RXShapeInterpolationP(void)
{
    for (vector<RXSitePtWithData*>::iterator it= pts.begin(); it!=pts.end(); ++it){
        RXSitePtWithData *pp = *it;
        delete pp;
    }
}
int RXShapeInterpolationP::Dump(const char* fname)
{
    ofstream f(fname);
    if(!f) return 0;
    int i=0;
    vector<RXSitePtWithData*>::const_iterator it,itend;
    f<<" x y z u v "<<endl;
    itend = Points().end();
    for(it=Points().begin(); it!=itend;++it) {
        f<<i<<" "<< (*it)->x<<" "<< (*it)->y<<" "<< (*it)->z
        <<" "<< (*it)->m_u<<" "<< (*it)->m_v<<endl;
        i++;
    }
    f.close();
    return i;

}
int RXShapeInterpolationP::Read(const RXSTRING &filename, const int skiplines, const int ndata){
    int c=0;
    int i;
    char buf [2048];
    filename_copy_local(buf,2048,ToUtf8(filename).c_str() );	ifstream input(buf);
    if(!input) { wcout<<L" cant open "<<filename.c_str()<<L"should return -1"<<endl;  return -1;}
    double u,v,dp;
    string sbuf;
    double dbuf[5]; assert(ndata<6); m_ndata=ndata;
    for (i=0;i<skiplines;i++){
        getline (input, sbuf);
    }
    do{
        ON_2dPoint uv;
        input>>uv.x>>uv.y; if(! input.good()) break;
        for(i=0;i<ndata;i++) {
            input>>dbuf[i]; if(! input.good()) break;
        }
        if(! input.good()) break;
        RXSitePtWithData *mypt=new RXSitePtWithData(uv ,ndata,dbuf,c++);
        pts.push_back(mypt);

    } while(true);
    input.close ();
    cout<<"read "<< c<<" points"<<endl;


    return c;
}
int RXShapeInterpolationP::Triangulate(const int flag ){
    this->msh.Init();
    if(pts.size()<2) cout<<" pts small(3)"<<endl;
    int rc=this->msh.Triangulate(this->pts,"QgOpenzcYYa99999");
    return rc;
}

double RXShapeInterpolationP::_ValueAt(const ON_2dPoint& p){
    double rc=0;
    cout << "Dont use double RXShapeInterpolationP::_ValueAt(const ON_2dPoint& p) "<<endl;  assert(0);
    return rc;
}
int RXShapeInterpolationP::_ValueAt(const ON_2dPoint& p,double*r){// to get XYZ from UV // used to return 1
    int rv, k,j;
    int ii[3]; double ww[3];
   rv=this->msh.Locate(p,ii,ww);
    if(rv==BAD)
      { for(j=0;j<this->m_ndata ;j++)
            r[j]=0;
        return BAD;
    }
    for(j=0;j<this->m_ndata ;j++) {
        r[j]=0;
        for(k=0;k<3;k++) {
            if(ii[k]<=0 ||rv==BAD)
                continue;
            class RXSitePtWithData *q = this->pts[ii[k]];
            r[j]	+= q->GetDataHead()[j] * ww[k];
        }
    }
    return rv;
}
int RXShapeInterpolation::DrawHoops(rxONX_Model*pmod,const int n ) //  to test locate
{
    int rc=0;
    double u,v;
    double du = 1./((double)n);
    LocateInitialize ();
    ON_3dPointArray pa;
    HC_Open_Segment("u"); HC_Set_Visibility("lines=on");HC_Set_Color("red");
    for(u=0.;u<1.+du/10.;u+=du) {
        HC_Open_Segment("");
        pa.Empty();
        for(v=0.;v<1.001;v+=0.01) {
            ON_3dPoint p =  this->CoordsAt (ON_2dPoint(u,v),RX_GLOBAL );
            pa.AppendNew()=p;
            HC_Insert_Ink(p.x,p.y,p.z);
        }
        ON_PolylineCurve *pc=new ON_PolylineCurve(pa);
        if(pmod) {
            ONX_Model_Object& mu = pmod->m_object_table.AppendNew();
            mu.m_object = pc;
            mu.m_bDeleteObject = true;
            mu.m_attributes.m_layer_index = 3;
            mu.m_attributes.m_name.Format("vdir_%f",u);
        }
        HC_Close_Segment();
    }
    HC_Close_Segment();
    HC_Open_Segment("v");HC_Set_Visibility("lines=on"); HC_Set_Color("blue");
    for(v=0.;v<1.+ du/10.;v+=du) {
        HC_Open_Segment("");
        pa.Empty();
        for(u=0.;u<1.001;u+=0.01) {
            ON_3dPoint p =CoordsAt (ON_2dPoint(u,v),RX_GLOBAL);
            pa.AppendNew()=p;
            HC_Insert_Ink(p.x,p.y,p.z);
        }
        ON_PolylineCurve *pc=new ON_PolylineCurve(pa);
        if(pmod){
            ONX_Model_Object& mu = pmod->m_object_table.AppendNew();
            mu.m_object = pc;
            mu.m_bDeleteObject = true;
            mu.m_attributes.m_layer_index = 4;
            mu.m_attributes.m_name.Format("udir_%f",v);
        }
        HC_Close_Segment();
    }
    HC_Close_Segment();
    return rc;
}
class RXShapeInterpolation *RXShapeInterpolation::Invert(){
    cout<<"TODO: RXShapeInterpolation::Invert" <<endl;
    return NULL;
}
