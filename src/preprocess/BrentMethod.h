// BrentMethod.h: interface for the BrentMethod class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BRENTMETHOD_H__800835C4_1169_41F3_BD83_05C038D85180__INCLUDED_)
#define AFX_BRENTMETHOD_H__800835C4_1169_41F3_BD83_05C038D85180__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "stdio.h"

#define BRENT_ROOT_NOT_BRACKETED	2
#define	BRENT_NOT_CONVERGED     	4
#define BRENT_FAILED_1				8
#define BRENT_FAILED_2				16
#define BRENT_FAILED_3				32

class BrentMethod  
{
public:

	int DBG_Listing( FILE *fp);
	BrentMethod();
	virtual ~BrentMethod();
	int	Bracket(double p_seed, double &xmin, double & xmax); 
	int zpbrent( const double x1, const double x2);
	double GetResult() const;
	int ReportError()const;
	int ReportFunctionCount()const;
	int ReportIterationCount()const;

protected:
	int m_FuncCount;
	double m_xmax;
	double m_xmin;
	double m_tol;
    virtual double Func(const double x); //  function  whose root we wish to find
	int m_ErrorFlag;
	double m_result;
	int m_ITMAX;
	double m_EPS;
private:
	int m_iter;

};

extern "C" int BrentTest();
#endif // !defined(AFX_BRENTMETHOD_H__800835C4_1169_41F3_BD83_05C038D85180__INCLUDED_)
