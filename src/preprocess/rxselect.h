//select.h : header for select.cpp
//Thomas 10 06 04

#ifndef RX_SELECT_H
#define RX_SELECT_H

#include "griddefs.h"
#include "uselect.h"

#ifndef WIN32

struct SELECTDATA {
	const char *m_text;	/* name */
	Graphic *m_g;
		
	};
typedef struct SELECTDATA SelectData;

  static void SelectOK_CB(
		Widget w,	
		XtPointer client_data,
		XtPointer call_data);
	static void SelectCancelCB(
		Widget w,		/*  widget id		*/
		XtPointer client_data,	/*  data from application   */
		XtPointer call_data	/*  data from widget class  */
	);
	static void SelectDestroyCB(
		Widget w,		/*  widget id		*/
		XtPointer client_data,	/*  data from application   */
		XtPointer call_data	/*  data from widget class  */
	);
	static void SelectUnMapCB(
		Widget w,		/*  widget id		*/
		XtPointer client_data,	/*  data from application   */
		XtPointer call_data	/*  data from widget class  */
	);

#endif //#ifndef WIN32

#endif //#ifndef RX_SELECT_H
