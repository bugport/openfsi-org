/* started 13/12/94

Sept 98  The drawing routines
Sept 97. The keyword 'local' in the last word will cause this field to have local scope
 The default is global scope.

 3/9/96

The card is

field:name:sc1:sc2 :material:type  ! sc2 may be blank 

  29/3/97  added Set_Field_Type so the thickness setting is quicker
  22/2/95 matkey->Needs _Computing

  A FIELD is a rule for placing material.
  The ref direction is with respect to a seamcurve  Y(x)
  and is found by linear interpolation between the chord and the sc

   The thickness is f(x/arc) * g (y/Y(x))  {x,y in SC axes)
   where f(x/arc) is Y(x)/depth

  there are several field types
    A	fish - shape.  g = 1 if abs(y) < Y(x) else 0
    B	uniform			g = 1
    C	ramp			g = (1-abs(y/Y(x)) with min 0

The card is

field:name:sc:material:type */
#include "StdAfx.h"
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RX_FESite.h"
#include "RXLayer.h"
#include "RXRelationDefs.h"
#include "RXSeamcurve.h"

#include "entities.h"
#include "peterfab.h"
#include "panel.h"

#include "words.h"
#include "etypes.h"
#include "resolve.h"
#include "stringutils.h"
#include "akmutil.h"

#include "RX_UI_types.h"

#include "fields.h"

int Set_Field_Type(struct PC_FIELD *field){

    field->flag=0;
    const char*lp = field->m_TyPe;
    if(stristr(lp,"everywhere")) 	field->flag+=EVERYWHERE;

    if(stristr(lp,"uniform")
            || stristr(lp,"step")) 	field->flag+=UNIFORM;
    /* 'step' is SSD */
    if(stristr(lp,"triangle"))		field->flag+=TRIANGLE;

    if(stristr(lp,"ramp"))			field->flag+=RAMP;

    if(stristr(lp,"cosine")) 		field->flag+=COSINE;

    if(stristr(lp,"cossq"))		field->flag+=COSSQ;
    if(stristr(lp,"radial")
            || stristr(lp,"$seeded"))	field->flag+=RADIAL;
    if(stristr(lp,"inverse"))		field->flag+=INVERSE;
    if(stristr(lp,"reversed"))		field->flag+=REVERSED;
    if(stristr(lp,"filament"))		field->flag+=FILAMENT_FIELD;
    if(stristr(lp,"$dove"))		field->flag+=DOVE_FIELD;
    if(stristr(lp,"$nurbsalpha"))		field->flag+=NURBS_ALPHA;

    return (field->flag);
}

int Resolve_Field_Card(RXEntity_p e) {
    //	 FIELD record now requires TWO Sc,s\nThe second may be blank


    RXEntity_p layer_e, se=NULL;
    struct PC_FIELD *p;
    class RXLayer *fabdata;

    int retval = 0;
    int goodsc2=0;
    std::string sline( e->GetLine() );
    std::vector<std::string> wds = rxparsestring(sline,RXENTITYSEPS,false);
    int nw = wds.size();
    if(nw<6) return 0;
    QString s;
    const char *name,*sc1,*sc2,*mat,*type,*atts;
    name=wds[1].c_str();
    sc1 =wds[2].c_str();
    sc2 =wds[3].c_str();
    mat =wds[4].c_str();
    type=wds[5].c_str();
    RXAttributes typeatt(type);
    atts=wds[nw-1].c_str();

    p = (PC_FIELD *)CALLOC( 1,sizeof(struct PC_FIELD));
    p->m_TyPe= STRDUP(wds[5].c_str());
    Set_Field_Type(p);
    if(p->flag&FILAMENT_FIELD)
    {
        p->mat=0; p->sc1=0; p->sc2=0; // and its OK
    }
    else if(p->flag&DOVE_FIELD){
        p->mat = e->Esail->Get_Key_With_Reporting("dovefile",mat);
        if(!p->mat  ) {
            if( p->m_TyPe ) RXFREE(p->m_TyPe); p->m_TyPe=0;
            RXFREE(p);
            return(retval);
        }
        p->mat->SetNeedsComputing();
    }
    else { // not a dove
        p->sc1 = e->Esail->Get_Key_With_Reporting("curve,seam,seamcurve,compound curve",sc1);

        if(p->flag&RADIAL) {    /* focus is required. sc2 isnt */
            if(nw>6) { type = wds[6].c_str();
                p->focus = e->Esail->Get_Key_With_Reporting("site,relsite",type);
            }
        }
        else {
            if(!rxIsEmpty(sc2))  {
                goodsc2=1;
                p->sc2 = e->Esail->Get_Key_With_Reporting("curve,seam,seamcurve,compound curve",sc2);
            }
        }
        if(p->flag&NURBS_ALPHA) {
            // extract the name of the ISurface
        }

        p->mat = e->Esail->Get_Key_With_Reporting("fabric,material",mat);// Feb 2015 P

        if(!p->sc1 || !p->mat ||  (  (p->flag&RADIAL)&&(!p->focus) )  ||( (goodsc2)&&(!p->sc2) ) ) {
            if( p->m_TyPe ) RXFREE(p->m_TyPe); p->m_TyPe=0;
            RXFREE(p);
            return(retval);
        }
        p->mat->SetNeedsComputing();
    }

    p->Local = (NULL !=stristr(atts,"local")); // TODO  via rxatt system

    // Sept 2002 for a CURVE field
    if( typeatt.Extract_Word("$ref",s)){
        if(!(se = e->Esail->Get_Key_With_Reporting("seamcurve",qPrintable (s)))){
            if( p->m_TyPe ) RXFREE(p->m_TyPe); p->m_TyPe=0;
            RXFREE(p);
            return retval;
        }
        p->flag+=CURVE_FIELD;
    }


    layer_e = e->Esail->Insert_Entity( "layer",name,0,(long) 0,atts,e->GetLine());
    fabdata= (class RXLayer*) layer_e;
    fabdata->matptr=e;
    fabdata->matstruct.m_Data.resize(8);
    fabdata->matstruct.m_sizeofdata=8;
    if(p->flag&CURVE_FIELD  )
        fabdata->matstruct.ref_sc=se;

    e->SetRelationOf( layer_e,spawn|child|niece,RXO_LAYER_OF_FIELD); //Niece Jan 2014
    if(p->flag&FILAMENT_FIELD)
        fabdata->matstruct.function = Get_Material_Function_Key("filament");
    else
        fabdata->matstruct.function = Get_Material_Function_Key("field");


    fabdata->Use_Black_Space=1;
    fabdata->Use_Me_As_Ref=0;

    e->SetDataPtr( p);
    e->Needs_Resolving=0;
    e->Needs_Finishing=0;

    retval = 1;
    if(p->sc1)
        p->sc1->SetRelationOf(e,child|niece,RXO_SC1_OF_FIELD);
    if(p->sc2)
        p->sc2->SetRelationOf(e,child|niece,RXO_SC2_OF_FIELD);
    if(p->focus)
        p->focus->SetRelationOf(e,child|niece,RXO_FOCUS_OF_FLD);
    if(p->mat)
        p->mat->SetRelationOf(e,child|niece,RXO_MAT_OF_FIELD); // niece Jan 2014
    if(se)
        se->SetRelationOf(e,child|niece,RXO_REF_OF_FIELD);
    return(retval);
}  			

int Draw_All_Fields(SAIL *sail){
    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); ++it)  {
        RXEntity_p ep  = dynamic_cast<RXEntity_p>( it->second);
        if(ep->TYPE==FIELD&&! ep->Needs_Resolving)
            Draw_Field(ep);

    }
    return(1);
}

int  Draw_Field(RXEntity_p e) {
    assert(e);
#ifdef HOOPS
    HC_KEY key;
    double u,v;
    int ku,kv, k,nu=11,nv=9;
    VECTOR *pts;
    ON_3dPoint v1,v2;
    RXSitePt q;
    double thickness , du,dv ;
    float *colors;

    MatValType mx[9]; double ps[3];
    sc_ptr sc;
    RXEntity_p le = e->Esail->GetKeyWithAlias("layer",e->name());
    assert(le);
    class RXLayer *layer  = (class RXLayer *)le;

    MatDataType xv; xv.resize(20); //float xv[20];

    if (!e->dataptr)
    {
        char l_buff[100];
        sprintf(l_buff,"ERROR : in Draw_Field, %s->dataptr == NULL ",e->name());
        e->OutputToClient(l_buff,2);
        return 0;
    }

    struct PC_FIELD *p= (PC_FIELD *)e->dataptr;
    if (!p)
    {
        ON_String l_buff;
        l_buff.Format("ERROR : in Draw_Field, %s->dataptr is not a a PC_FIELD",e->name());
        e->OutputToClient(l_buff.Array(),2);
        return 0;
    }
    if(p->flag& FILAMENT_FIELD) return 0;
    if(!le)
        return 0;


    if(!p->sc1)
        return 0;

    if( !(p->flag&RADIAL) && !p->sc2)
        return 0;


    HC_Open_Segment(e->type());
    e->hoopskey = HC_KOpen_Segment(e->name());
    HC_Flush_Contents(".","everything");
    HC_Set_User_Index(RXCLASS_ENTITY, e);

    k=0;
    pts = (VECTOR *)MALLOC(((nu+1)*(nv+1)+2) *sizeof(VECTOR));
    colors  = (float *)MALLOC(((nu+1)*(nv+1)+2) *sizeof(float));
    du = 1.0/(double)(nu) ;
    dv = 1.0/(double)(nv-1) ;
    u = du/2;

    for(ku=0 ;ku< nu ; ku++, u=u+du) { // u is along the field. V is across it
        if(p->flag&RADIAL) {
            cout<< " draw radial field not coded"<<endl;
            assert(0); memcpy(&v1,  p->focus->dataptr, 3*sizeof(float)); }
        else 		{ // v1 is on sc2
            sc = (sc_ptr  )p->sc2;
            Get_Position_By_Double(p->sc2,sc->GetArc(1)*u ,1,v1);
        }
        /* v2 is on sc1 */
        sc = (sc_ptr  )p->sc1;
        if(p->flag&REVERSED)
            Get_Position_By_Double(p->sc1,sc->GetArc(1)*(1.-u) ,1,v2);
        else
            Get_Position_By_Double(p->sc1,sc->GetArc(1)*u ,1,v2);

        v = .0;
        for(kv=0 ;kv< nv ;kv++, v=v+dv) {
            q.x=v2.x*v + v1.x*(1.-v);
            q.y=v2.y*v + v1.y*(1.-v);
            q.z=v2.z*v + v1.z*(1.-v);


            Mat_Field(&thickness,mx,&q,NULL, layer, xv,ps);

            colors[k]=(float) thickness;
            pts[k].x =(float)q.x;
            pts[k].y =(float)q.y;
            pts[k].z =(float) q.z;
            k++;
        }
    }
    HC_Set_Color("edges=blue");
    key = HC_KInsert_Mesh(nu,nv,pts);
    HC_MSet_Vertex_Colors_By_FIndex(key,"faces",0,k,colors);

    HC_Close_Segment();
    HC_Close_Segment();
    RXFREE(pts);
    RXFREE(colors );
    return 1;
#else
    return 0;
#endif
}
