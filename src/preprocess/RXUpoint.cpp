// RXUPoint.cpp: implementation of the RXUPoint class.
//
//////////////////////////////////////////////////////////////////////


#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXCurve.h"
#include "RXTriangle.h"
#include "RXUPoint.h"


RXUPoint::RXUPoint()
{
	Init();
}//RXUPoint::RXUPoint

//Copy constructor
RXUPoint::RXUPoint(const RXUPoint & p_Obj)
{
	Init();
	this->operator =(p_Obj);
}//RXUPoint::RXUPoint(double p_x,double p_y,double p_z)

RXUPoint::~RXUPoint()
{
	Clear();
}//RXUPoint::~RXUPoint

void RXUPoint::Init()
//meme alloc + init val to 0 0 0
{
	m_pCurve=NULL;
	m_t = 0.0;
	m_sense=0;
}//void RXUPoint::Init

int RXUPoint::Clear()
{ 
	m_t = 0.0;
	m_sense=0;
	m_pCurve=NULL;
	return(1);
}//int RXUPoint::Clear

RXUPoint& RXUPoint::operator = (const RXUPoint & p_Obj)
{
	//Make a copy
	Init();

	m_t = p_Obj.m_t;
	m_sense= p_Obj.m_sense;

	m_pCurve = p_Obj.m_pCurve; 
	return *this;
}//void RXUPoint::operator =

double RXUPoint::Gett() const
{
	return m_t;
}//int RXUPoint::Sett

ON_3dPoint RXUPoint::GetPos() const
{
	int l_totot = 456;
	const RXCurve * l_pCurve = RXCurve::Cast(m_pCurve);
	const RXTriangle * l_pTri = RXTriangle::Cast(m_pCurve); 
	
	assert(l_pCurve||l_pTri);
	assert(!(l_pCurve&&l_pTri));

	ON_3dPoint l_pt(0.0,0.0,0.0);
	if (l_pCurve)
	{
		l_pt = l_pCurve->PointAt(m_t); 
	}
	else
	{
		if (l_pTri)
			l_pt = l_pTri->PointAt(m_t); 
		else
			assert(0);
	}
	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;
	return l_pt;
}//ON_3dPoint RXUPoint::GetPos

ON_3dVector RXUPoint::GetTangent() const
{
	const RXCurve   * l_pCurve = RXCurve::Cast(m_pCurve); 
	const RXTriangle * l_pTri = RXTriangle::Cast((RXTriangle*)m_pCurve); 
	
	assert(l_pCurve||l_pCurve);

	ON_3dVector l_tan(0.0,0.0,0.0);
	if (l_pCurve)
	{
		l_tan = l_pCurve->TangentAt(m_t);
	}
	
	if (l_pTri)
	{
		assert(0);  //fow now I do not what to code it!!
		l_tan = l_pTri->TangentAt(m_t); 
	}

	//clean up 
//	if (l_pCurve) l_pCurve = NULL;
//	if (l_pTri)   l_pTri = NULL;
		
	return l_tan; 
}//ON_3dPoint RXUPoint::GetTangent

int RXUPoint::SetToMin()
{
	const RXCurve   * l_pCurve = RXCurve::Cast(m_pCurve); 
	const RXTriangle * l_pTri = RXTriangle::Cast(m_pCurve); 
	
	assert(l_pCurve||l_pTri);
	assert(!(l_pCurve&&l_pTri));

	if (l_pCurve)
	{
		m_t = l_pCurve->GetONCurve()->Domain().Min(); 
	}
	
	if (l_pTri)
	{
		assert(0);  //fow now I do not what to code it!!
		//l_pt = m_pCurve->PointAt(m_t); 
	}

	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;

	return 1;
}//int RXUPoint::SetToMin

int RXUPoint::SetToMax()
{
	const RXCurve   * l_pCurve = RXCurve::Cast(m_pCurve); 
	const RXTriangle * l_pTri = RXTriangle::Cast(m_pCurve); 
	
	assert(l_pCurve||l_pTri);
	assert(!(l_pCurve&&l_pTri));

	if (l_pCurve)
	{
		m_t = l_pCurve->GetONCurve()->Domain().Max(); 
	}
	
	if (l_pTri)
	{
		assert(0);  //fow now I do not what to code it!!
		//l_pt = m_pCurve->PointAt(m_t); 
	}

	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;

	return 1;
}//int RXUPoint::SetToMin

int RXUPoint::SetToMid()
{
	const RXCurve   * l_pCurve = RXCurve::Cast(m_pCurve); 
	const RXTriangle * l_pTri  = RXTriangle::Cast(m_pCurve); 
	
	assert(l_pCurve||l_pTri);
	assert(!(l_pCurve&&l_pTri));

	if (l_pCurve)
	{
		m_t = l_pCurve->GetONCurve()->Domain().Mid(); 
	}
	
	if (l_pTri)
	{
		assert(0);  //fow now I do not what to code it!! 
	}

	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;

	return 1;
}//int RXUPoint::SetToMid

ON_Interval RXUPoint::Domain() const
{
	ON_Interval l_domain(0.0,1.0);

	const RXCurve   * l_pCurve = RXCurve::Cast(m_pCurve); 
	const RXTriangle * l_pTri = RXTriangle::Cast(m_pCurve); 
	
	assert(l_pCurve||l_pTri);
	assert(!(l_pCurve&&l_pTri));

	if (l_pCurve)
	{
		l_domain = l_pCurve->GetONCurve()->Domain(); 
	}
	
	if (l_pTri)
	{
		assert((m_t>=0.0)&&(m_t<=3.0));
		if (m_t<1.0)  //SHOULD MAYBE MIGRATE TO RXTRIANGLE
			l_domain.Set(0.0,1.0);
		else
			if (m_t<2.0)
				l_domain.Set(1.0,2.0);
			else
				l_domain.Set(2.0,3.0);
	}

	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;

	return l_domain;
}//ON_Interval RXUPoint::Domain

double RXUPoint::GetLength(double fractional_tolerance) const
{
	double l_len=0.0; 
	
	const RXCurve    * l_pCurve = RXCurve::Cast(m_pCurve); 
	const RXTriangle * l_pTri   = RXTriangle::Cast(m_pCurve); 
	
	assert(l_pCurve||l_pTri);
	assert(!(l_pCurve&&l_pTri));

	if (l_pCurve)
	{
		l_pCurve->GetONCurve()->GetLength(&l_len,fractional_tolerance);
	}
	
	if (l_pTri)
	{
		assert(0);  //fow now I do not what to code it!! 
	}

	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;
	
	return l_len;
}//double RXUPoint::GetLength

int RXUPoint::Set(const double &p_t, const RXTRObject * p_pCrv,const int sense)
{
	int l_ret = RXUPOINT_OK;
	const RXCurve * l_pCrv = RXCurve::Cast(p_pCrv);
	const RXTriangle * l_pTR = RXTriangle::Cast(p_pCrv);

	assert(l_pCrv||l_pTR);
	assert(!(l_pCrv&&l_pTR));
	
	if(p_pCrv)
		m_pCurve = p_pCrv;
	else
		if (l_pTR)
			m_pCurve = l_pTR;
		else
			g_World->OutputToClient("RXUPoint::Set, trying to set with an RXTRObject which neither a RXCurve or an RXTriangle",3);

	m_t = p_t;
	m_sense = sense;
	
	if (l_pCrv) l_pCrv = NULL;
	if (l_pTR) l_pTR = NULL;

	return l_ret;
}//int RXUPoint::Set(const double &p_t, const ON_Curve * p_pCrv);


int RXUPoint::Print(FILE *fp) const
{
		return fprintf(fp," curve = \tP%p \t t = %f \t sense = %d\n",m_pCurve,m_t,m_sense);

}//int RXUPoint::Print

int RXUPoint::IsValid() const
{
	int l_ret = 0;

	const ON_Curve   * l_pCurve = dynamic_cast<const ON_Curve*>(m_pCurve); 
	RXTriangle * l_pTri = RXTriangle::Cast((RXTriangle*)m_pCurve); 
	
	assert(l_pCurve||l_pCurve);

	if (l_pCurve)
	{
		l_ret = l_pCurve->IsValid();
	}
	
	if (l_pTri)
	{
		assert(0);  //fow now I do not what to code it!! 
	}

	//clean up 
	if (l_pCurve) l_pCurve = NULL;
	if (l_pTri)   l_pTri = NULL;
	
	return l_ret;
}//int RXUPoint::IsValid
