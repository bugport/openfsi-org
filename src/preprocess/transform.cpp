/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 1 may 2010. due to the re-working of DOFOs we had to change on-deflected to work from last-known-good not
 current deflected pos.
  JUl 97 There is an order-dependence problem.
 The order of the 3 conncted points is meant to reflect the order of their appearance in the script files
 They get pushed onto the sails dep list in that order and so get pulled off in reverse order.
But if a connect is unresolved (for instance bcause of an edit), it getss pulled off and then pushed back onto
the dep list and the order changes. 
To fix this, we give each connect an INDEX reflecting its position in the script
and we will use this index to fixe the order of the transform points
  * Oct 96 transform on displaced coords
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RX_FESite.h"
#include "RXSail.h"
#include "Gflags.h"
#include "global_declarations.h"

#include "f90_to_c.h"
#include "findtransform.h"

#define  TRDB (false)


int RelaxWorld::SetUpAllTransforms(int regardless)
{
  int i;
  double mt[4][4], imt[4][4];
  int retVal=0;

  cf_resolve_connects(&i);
  if(TRDB)
      cout<<"(SetUpAllTransforms)  cf_resolve_connects (should be done on hoist. )  "<<i<<endl;

  vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
  for(i=0;i<lsl.size();i++)  {
    SAIL *theSail = lsl[i];
    if(regardless ||  theSail->GetFlag(FL_NEEDS_TRANSFORM)  ) {
      ON_Xform mtx,imtx;
      retVal += theSail->SetUpTransform(g_Transform_On_Deflected,mtx);

      memcpy(mt,mtx.m_xform ,16*sizeof(double));
      imtx = mtx.Inverse();
      memcpy(imt,imtx.m_xform ,16*sizeof(double));

      /* Now apply the new matrix to the sails nodelist . Only set NEEDS_HEELING off*/
      cf_applynewtransform(theSail->SailListIndex()   , mt, imt);

      theSail->SetFlag(FL_NEEDS_POSTP); // strictly only if the transforms have changed
      theSail->ClearFlag(FL_NEEDS_TRANSFORM);
    }
  } // for i

  clGFlag(GLOBAL_DO_TRANSFORMS);
  return(retVal);
}

int RXSail::SetUpTransform(const int On_Deflected, 	ON_Xform &mtx) const

{
  /*
   *look for the first three connects in any hoisted models which
       reference a hoisted model and this sail (sdLocal->type) .
       use these 6 sites for the transform set up routines
       given above.
       before August 2013. was
   * look for the first three connects in the boat which
       reference the boat and this sail (sdLocal->type) .
       use these 6 sites for the transform set up routines
       given above.


     */
  int i;
  RX_FESite *s[3], *v[3];
  int nn1[10],nn2[10], count,L1,L2;

  ON_3dVector pV0,V1,V2;  // WRONG  a strange mixture of vectors and points
  ON_3dVector vS0,S1,S2,vxa,xb,xc,d,e,f;
  if(this->IsBoat()) {
    vxa.x = 0;d.x = 0;		vxa.y = 0;d.y = 0;		vxa.z = 0;d.z = 0;
    xb.x = 1;e.x = 1;		xb.y = 0;e.y = 0;		xb.z = 0;e.z = 0;
    xc.x = 0;f.x = 0;		xc.y = 1;f.y = 1;		xc.z = 0;f.z = 0;
    FindTransform(d,e,f,vxa,xb,xc,mtx,g_World->HeelAngle(),0,0);

    return(1);
  }

  // should check whether this sail is hoisted, but if it's not the connect count will be low;

  L1=4; L2 = this->GetType ().size () ;

  cf_get_first_three_connects("boat",&L1,this->GetType().c_str() ,&L2, nn1,nn2, &count);
  fflush(stdout);
  if(count ==3) {
    for(i=0;i<count;i++ ) {
      s[i]= this->GetSiteByNumber( nn2[i]);
      v[i] = g_boat->GetSiteByNumber( nn1[i]);
      if(TRDB)
      {
        cout << "transNode "<<i << " S is "<<qPrintable( QString::fromStdWString( s[i]-> RXSiteBase::TellMeAbout()  )  )<<endl;
        cout <<".  .  V is       " <<qPrintable( QString::fromStdWString( v[i]-> RXSiteBase::TellMeAbout() ))<<endl;
      }
    }
  }
  else // count isnt three
  {
    vxa.x = 0;d.x = 0;			vxa.y = 0;d.y = 0;			vxa.z = 0;d.z = 0;
    xb.x = 1;e.x = 1;			xb.y = 0;e.y = 0;			xb.z = 0;e.z = 0;
    xc.x = 0;f.x = 0;			xc.y = 1;f.y = 1; 			xc.z = 0;f.z = 0; // swapped f.y and f.z 2 July 2003
    if(TRDB)
    {
      cout << "transForm, count= "<<count <<endl;
    }

    FindTransform(d,e,f,vxa,xb,xc,mtx,0,0,0);

    return(1);
  } // end count isnt three

  if( On_Deflected) {
    pV0 = v[0]->GetLastKnownGood(RX_MODELSPACE);
    V1 = v[1]->GetLastKnownGood(RX_MODELSPACE);
    V2 = v[2]->GetLastKnownGood(RX_MODELSPACE);

    vS0  = s[0]->GetLastKnownGood(RX_MODELSPACE);
    S1  = s[1]->GetLastKnownGood(RX_MODELSPACE);
    S2  = s[2]->GetLastKnownGood(RX_MODELSPACE);
  }
  else {
    pV0 = v[0]->ToONPoint();
    V1 = v[1]->ToONPoint();
    V2 = v[2]->ToONPoint();

    vS0  = s[0]->ToONPoint();
    S1 = s[1]->ToONPoint();
    S2 = s[2]->ToONPoint();
  }

  ON_3dPoint pxa = g_boat->XToGlobal(pV0 );
  ON_3dPoint pxb = g_boat->XToGlobal(V1 );
  ON_3dPoint pxc = g_boat->XToGlobal(V2 );
  vxa=pxa;
  xb=pxb;
  xc=pxc;
  FindTransform(vxa,xb,xc,vS0,S1,S2,mtx,0,0,0);
  return(1);
}





