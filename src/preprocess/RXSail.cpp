
#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "rxviewframe.h"
#include "RelaxWorld.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RXCompoundCurve.h"
#include "RX_FEBeam.h"
#include "RX_FEPocket.h"
#include "RXQuantity.h"
#include "RXCurve.h"
#include "rxspline.h"
#include "rxnodecurve.h"
#include "RXScalarProperty.h"
#include "RXDrawingLayer.h"
#include "RXPside.h"
#include "rxpshell.h"
#include "rxrigidbody.h"
#include "rxsubmodel.h"
#include "RXLayer.h"
#include "RXShapeInterpolation.h"
#include "RXPressureInterpolation.h"
#include "RX_FETri3.h"
#include "RX_FEEdge.h"
#include "RX_PCFile.h"
#include "RXLogFile.h"
#include "RXAttributes.h"
#include "rxON_Extensions.h"
#include "rxsubwindowhelper.h"
#include "rxqtdialogs.h"
#include "RXRelationDefs.h"
#include "RX_SeamcurveHelper.h"
#include <QDir>
#include <QFile>
#include <QDebug>
#include "rxqtdialogs.h"
#include "printall.h"
#include "interpln.h"
#include "words.h"
#include <fstream>
#ifdef _OPENMP
#include <omp.h>
#endif
#ifndef linux
    #include "..\stringtools\stringtools.h" // defines FromUtf8 and ToUtf8
#else
    #include "../../stringtools/stringtools.h"
    #include "drop.h"
#endif
#include "RX_UI_types.h"
#include "summary.h"
#include "global_declarations.h"
#include "vectors.h"
#include "entities.h"
#include "f90_to_c.h"
#include "traceback.h"
#include "stringutils.h"

#include "filament.h"
#include "ReadName.h"
#include "read.h"
#include "RXGridDensity.h"
#include "RXdovefile.h"
#include "rxfixity.h"
#include "rxvectorfield.h"
#include "rximportentity.h"

#include "script.h"  // for Do Save state etc
#include "etypes.h"
#ifdef HOOPS
#include "hoopsToON.h"
#include "attachmod.h"
#endif
#ifdef COIN3D
#include <Inventor/nodes/SoIndexedFaceSet.h>
#endif
#include "RLX3dm.h"



#include "polyline.h"
#include "alias.h"
#include <vector>
#include <map>
using namespace std;
#include "griddefs.h" 
#include "question.h"
#ifdef _X
#include "trimmenu.h"
#include "menuCB.h"
#include "pansin.h"
#include "RLX3dm.h"
#endif

#include "resolve.h"

#include "RXSail.h"

//#define MAX_RCOUNT 5*this->MapSize()

RXSail::RXSail(void)
    :m_uv2xyz(0)
    ,m_pressInt(0)
    ,m_mexp(0)
    ,m_xyz2uv(0)
    ,m_pSailSum(0)
#ifdef HOOPS
    ,m_HoopsFlist(0), m_HoopsPcount(0), m_HoopsFcount(0)
    ,m_HoopsShell(0)
    ,ntri(0)
#endif

    ,m_tridata(0)
    ,m_postProc(0),
      m_QuadToUnitSquare(0)
{
    assert(" don't use default RXSail constructor"==0);
}
RXSail::RXSail(RelaxWorld *p_rw)
    :m_viewFrame(0)
    ,m_uv2xyz(0)
    ,m_pressInt(0)
    ,m_mexp(0)
    ,m_xyz2uv(0)
    ,m_pSailSum(0)
#ifdef USE_PANSAIL
    ,m_N(0)
    ,m_M(0)
#endif
    ,      m_Pressure_Factor(1.0)
    #ifdef HOOPS
    ,Hoops_Strings (0)
    ,Hoops_Residuals (0)
    ,Hoops_Nodes(0)

    ,m_HoopsCoords(0)
    ,m_HoopsFlist(0), m_HoopsPcount(0), m_HoopsFcount(0)
    ,m_HoopsShell(0)
    ,ntri(0)
    #endif

    ,m_tridata(0)
    ,m_postProc(0),
      m_QuadToUnitSquare(0)
{
    m_rw = p_rw;
    assert(m_rw);
    RXSail::Initialize();
    m_rw->AddSail(this);
    SetParent(p_rw);
    m_pSailSum=new class RXLogFile();
    this->m_postProc =RXNewGNode("post" ,0);
}

RXSail::~RXSail(void)
{
    int err=0;
    int sli = SailListIndex();
    int nsf = this->Needs_Saving();
    RXEntity * ep ;
    delete m_pSailSum;
    Graphic *gg = this->GetGraphic();
    // should Drop, then check if needs saving
#ifndef RXQT
#ifdef linux
    assert(!IsHoisted()); // in linux we must already have dropped it .
#endif
#endif
    if(IsHoisted()) Drop();

    Delete_Whole_Database();

    delete this->m_p3dmModel ; // else we should delete it
    if(gg) gg->m_qfilename.clear();
    if(gg) gg->GSail = 0;
    // send a message to the GUI telling it to delete
    if(this->m_viewFrame ){
        void*ptr = this->m_viewFrame ; this->m_viewFrame =0;
        RXDeleteWindow (ptr);
    }

    std::set<RXEntity*>::reverse_iterator it;
    for(;this->m_garbage .size(); ) {
        it=this->m_garbage.rbegin ();
        ep = *it;
        delete ep;
    }
    this->m_garbage.clear ();

    m_rw->RemoveSail(this);
    if(g_boat ==this) g_boat=0;

    GetWorld()->ClearExpList(this);
    this->RXENode::CClear();
    Check_All_Heap((1));
    // oct 2012 try moving the fortran cleanup down here

    if(sli>=0 && true ) {
        cf_removesail (sli , &err);   // this should come after because it unhooks (SSD)
        if(err) rxerror(" Sail not cleaned up completely",2);
    }
    if(this->m_uv2xyz) delete  this->m_uv2xyz;  this->m_uv2xyz=0;
    if(this->m_pressInt) delete this->m_pressInt;
    if(this->m_xyz2uv) delete  this->m_xyz2uv;
#ifdef HOOPS
    if(m_HoopsCoords) RXFREE(m_HoopsCoords);
    if(m_HoopsFlist) RXFREE(m_HoopsFlist);
#endif
    if(m_tridata)RXFREE(m_tridata);

    cout << "Done ~RXSail()%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%" <<endl;
    if(nsf) cout<<"... but it needs saving"<<endl;
}

int RXSail::Initialize(){

    int l_err  =0,   i;
    class RXSail *SP=0;

    for(i=0;i<NUM_OF_FLAGS;i++) m_Flags[i]=0;

#if !defined(linux)  && !defined(RXQT)
    cout << "we should pick a graphic from graph in HIOUtility_GaussScript::FileInputByKey " <<endl;
    struct GRAPHIC_STRUCT *l_g;
    l_g = (struct GRAPHIC_STRUCT* )CALLOC(1,sizeof(struct GRAPHIC_STRUCT));
    this->SetGraphic (l_g);
    l_g->m_ModelSeg  = 0;
    l_g->GSail =this;
    printf(" HORRIBLE windows lashup for initializing the graphic in RXSail::Initialize() Graphic size=%d\n ",(int)sizeof(struct GRAPHIC_STRUCT));
#endif

    m_attributes=ON_String("undefined");
    m_Zaxis.x = (float) 0.0;
    m_Zaxis.y = (float) 0.0;
    m_Zaxis.z = (float) 1.0;

    m_mould = NULL;
    GDPField = NULL;
    m_LoadingIndex=-1;
    m_Linear_Tol = g_SOLVE_TOLERANCE ;
    m_Cut_Dist= g_Gauss_Cut_Dist;
    m_Angle_Tol= g_ANGLE_TOL ;
    i = g_Use_ONCurve;
    SetFlag(FL_ONFLAG,i );

#ifdef FORTRANLINKED	
    int sli;

    m_SailListIndex=0;
    cf_getnextsail(&m_SailListIndex, &l_err);

    SP=this;
    if( !l_err) {

        sli = SailListIndex();
        cf_setactive	(sli,0,	&l_err);
        cf_sethoisted	(sli,0,	&l_err);
        cf_setsailptr	(sli,&SP,		&l_err);
    }
    if(l_err)
        rxerror("ERROR initialising saillist  initSail_unix",3);
#endif /// FORTRANLINKED

    Some_Deleted = 0;

    m_Fnodes.resize (1); // counting starts at 1 because this mirrors a fortran array
    m_Fedges.resize (1); // counting starts at 1 because this mirrors a fortran array
    m_FTri3s.resize (1); // counting starts at 1 because this mirrors a fortran array

    panel_count=0;
    m_StressResults=0;
    m_p3dmModel=0;

    for(i=0;i<RX_MAX_TYPES  ;i++)  	m_Selected_Entity[i]=0;

    this->m_mexp=0;

    return(0);
}//int Initialize


RXSTRING RXSail::GetName() const
{
    return m_SName;
}
void RXSail::SetName(const RXSTRING & p_Name)
{
    m_SName = p_Name;

}

void RXSail::SetGraphic(Graphic *g) {
    assert(g);
    m_Graphic=g;
    g->GSail=this;
} 


#ifdef NEVERlinux
int RXSail::win32_dbg_02(SAIL *theSail,const char*filename,HC_KEY p_key) { // static

    theSail->GetGraphic() ->m_ModelSeg  = p_key;
    assert(theSail->GetGraphic()->GSail ==theSail);
    theSail->ScriptFile()=ON_String(filename);
    //	theSail->SetFileDAT(filename);
    //	int k = theSail->m_fileDAT.ReverseFind('.');
    //	if(k>0) theSail->m_fileDAT=theSail->m_fileDAT.Left(k);
    //	theSail->m_fileDAT+= ON_String(".dat");

    return 0;
}
#endif
std::string RXSail::GetType() const // 
{
    return m_type;
}

void RXSail::SetFileIN( const QString &lp)
{
    this->m_fileIN= lp;
    // now rewrite all entity lines that contain a filename
}
QString RXSail::ScriptFile()
{
    return  this->m_fileIN;
}
QString RXSail::RelativeFileName(const QString & fileName,const QString &pbaseName)
{
    QDir baseDir;
    QString shortName, baseName;
    QFileInfo baseInfo;
    if(fileName.startsWith("$R"))
        return  fileName;
    if(pbaseName.isEmpty())
        baseName = this->ScriptFile();
    else
        baseName = pbaseName;

    baseInfo = QFileInfo(baseName );
    assert(baseInfo.isAbsolute () );

    if(baseInfo.isFile())
        baseDir = baseInfo.dir();
    else
        baseDir = QDir(baseName);


    if (QFileInfo(fileName).isAbsolute () )
        shortName= baseDir.relativeFilePath (fileName );
    else{
        shortName= baseDir.relativeFilePath (fileName );
        Q_ASSERT( shortName==fileName);
    }

    return shortName;
}
QString RXSail::AbsolutePathName(const QString & fileName,const QString &baseName)
{
    QString longName;
    if(fileName.startsWith("$R")) {
        longName=fileName;
        return fileName;
    }

    QFileInfo baseInfo;
    if(baseName.isEmpty())
        baseInfo = QFileInfo(this->ScriptFile() );
    else
        baseInfo = QFileInfo(baseName );
    assert(baseInfo.isAbsolute () );
    QDir base;
    if(baseInfo.isFile())
        base = baseInfo.dir();
    else
        base = QDir(baseName);

    // now go
    longName= base.absoluteFilePath ( fileName ) ;
    // followed by
    longName=  QDir::cleanPath ( longName  ) ;
    return longName;
}

int RXSail::ZeroSpecialLists(RXEntity* const p) // Jan 2007 only m_Selected_Entity, but the idea
{
    int i;
    if(!p) {
        for(i=0;i<RX_MAX_TYPES  ;i++)
            m_Selected_Entity[i]=0;
        m_mould=0;
        GDPField=0;
    }
    else {
        for(i=0;i<RX_MAX_TYPES  ;i++) {
            if( m_Selected_Entity[i]  ==p)
                m_Selected_Entity[i]=0;
        }
        if(m_mould==p)
            m_mould=0;
        if(GDPField==p)
            GDPField=0;
    }
    return 1;
}

int RXSail::ReDove( ) { // script command "redove : sailname"
    int rc=0;
    /*
ReDove means:
1) unr-solve and re-resolve all the dove entities.
2) call ReBuild_All_Materials(SAIL *theSail); 
3) wasteful but call int LoadNow(void)

 the logic here is horrible because we are getting around the unresolve system., which
correctly unresolves all the panels.

*/

    char type[256], name[256], line[512];

    HC_Open_Segment_By_Key(this->GetGraphic()->m_ModelSeg);
    HC_Open_Segment("data");
    rc = 0;

    vector<RXEntity* >thelist;
    vector<RXEntity* >::iterator it;
    thelist.clear();
    MapExtractList(PCE_DOVE, thelist, PCE_ISRESOLVED) ;

    for (it = thelist.begin (); it != thelist.end(); it++)  {
        RXEntity_p e  = dynamic_cast<RXEntity_p>(*it );

        e->SetInStack (0);

        strcpy(name,e->name()); strcpy(type,e->type()); strcpy(line,e->GetLine());
        e->CClear();
        e->Rename(name,type);
        e->SetLine(line);
#ifdef RX_USE_DOVE
        rc+=Resolve_Dove_File(e);
#else
        cout<<"NO DOVE TODAY"<<endl;
#endif

    }

    ReBuild_All_Materials(RX_MODELSPACE); //If it can, it sets dstf too
    HC_Close_Segment();
    HC_Close_Segment();

    return rc;
}

#ifdef _X

HC_KEY RXSail::PostProcExclusive() const{
    struct GRAPHIC_STRUCT *g = GetGraphic();
    HC_Open_Segment_By_Key(g->m_Phoops_widget->hoops.m_pHBaseModel->GetModelIncludeKey());
    HC_KEY k = HC_KOpen_Segment("postprocessing/exclusive");
    HC_Close_Segment();
    HC_Close_Segment();
    return k;
}

HC_KEY RXSail::PostProcNonExclusive( ) const{
    struct GRAPHIC_STRUCT *g = GetGraphic();
    HC_Open_Segment_By_Key(g->m_Phoops_widget->hoops.m_pHBaseModel->GetModelIncludeKey());
    HC_KEY k = HC_KOpen_Segment("postprocessing/nonexclusive");
    HC_Close_Segment();
    HC_Close_Segment();
    return k;
}

HC_KEY RXSail::PostProcTransformSeg()const {
    struct GRAPHIC_STRUCT *g = GetGraphic();
    HC_Open_Segment_By_Key(g->m_Phoops_widget->hoops.m_pHBaseModel->GetModelIncludeKey());
    HC_KEY k = HC_KOpen_Segment("postprocessing/transform");
    HC_Close_Segment();
    HC_Close_Segment();
    return k;
}
#elif defined (RXQT)
RXGRAPHICSEGMENT RXSail::PostProcExclusive()const {
    RXGRAPHICSEGMENT  s = this->m_postProc; //GetGraphic()->GNode();
    return s;}
RXGRAPHICSEGMENT RXSail::PostProcNonExclusive()const{
    return this->m_postProc; //this->GetGraphic()->GNode(); // for now
}
RXGRAPHICSEGMENT RXSail::PostProcTransformSeg()const{
    return this->m_postProc; //this->GetGraphic()->GNode(); // for now
}
#else

HC_KEY RXSail::PostProcExclusive()const {

    HC_Open_Segment_By_Key(this->GetGraphic()->m_ModelSeg);
    HC_KEY k = HC_KOpen_Segment("dummy_PostProcExclusive");
    HC_Close_Segment();
    HC_Close_Segment();
    return k;}
HC_KEY RXSail::PostProcNonExclusive()const{
    HC_Open_Segment_By_Key(this->GetGraphic()->m_ModelSeg);
    HC_KEY k = HC_KOpen_Segment("dummy_PostProcNonExclusive");
    HC_Close_Segment();
    HC_Close_Segment();
    return k;}
HC_KEY RXSail::PostProcTransformSeg()const{	
    HC_Open_Segment_By_Key(this->GetGraphic()->m_ModelSeg);
    HC_KEY k = HC_KOpen_Segment("dummy_PostProcTransformSeg");
    HC_Close_Segment();
    HC_Close_Segment();
    return k;}

#endif



int RXSail::PrintModelSummary(FILE *fpp,const int p_short){
    int k=0;

    if(p_short)
        return 1; // for testing  listmodel

    fprintf(fpp,"*******************************************************\n");
    fprintf(fpp,"*                                                     *\n");
    fprintf(fpp,"*                      SAIL '%10s'         	*\n",GetType().c_str());
    fprintf(fpp,"*                                                     *\n");
    fprintf(fpp,"*******************************************************\n");

#if defined(HOOPS)
    char uo[2048];
    struct Graphic_STRUCT *g = GetGraphic() ;

    HC_Open_Segment_By_Key(g->m_ModelSeg);
    HC_Open_Segment("data");
    HC_Define_System_Options("C string length=2040");
    HC_QShow_User_Options("/",uo);
    fprintf(fpp,"ROOT UOs\n%s\n\n",uo);

    HC_Show_Pathname_Expansion(".", uo);
    fprintf(fpp,"Sail Root seg is \n%s\n",uo);
    if(HC_QShow_Existence(".","user options")){
        HC_Show_User_Options(uo);
        fprintf(fpp,"SAIL UOs\n%s\n\n",uo);
    }
    HC_Define_System_Options("no C string length");
    HC_Close_Segment();

#endif
    fprintf(fpp," SLI is           %d\n",SailListIndex () );
    fprintf(fpp," ScriptFile is   <%s>\n",qPrintable(this->ScriptFile()));

    fprintf(fpp," Solve Tolerance= %f\n",m_Linear_Tol );
    fprintf(fpp," Angle Tolerance= %f\n", m_Angle_Tol);
    fprintf(fpp," Cut Distance   = %f\n", m_Cut_Dist );
    fprintf(fpp," Use ON Flag    = %d\n", GetFlag(FL_ONFLAG));
    fprintf(fpp," Hoisted?       = %d\n", IsHoisted());
    fprintf(fpp," boat?          = %d\n", IsBoat());

    fprintf(fpp," Map Size  =      %lu \n",this->MapSize());
    fprintf(fpp," m_Fnodes.size  = %lu \tcapacity %lu\n",this->m_Fnodes.size(),this->m_Fnodes.capacity()   );
    fprintf(fpp," m_Fedges.size  = %lu \tcapacity %lu\n",this->m_Fedges.size(),this->m_Fedges.capacity());
    fprintf(fpp," m_FTri3s.size  = %lu \tcapacity %lu\n",this->m_FTri3s.size(),this->m_FTri3s.capacity());

    fprintf(fpp," m_freeNNs.size = %lu\n",this->m_freeNNs.size());
    fprintf(fpp," m_freeENs.size = %lu\n",this->m_freeENs.size());
    fprintf(fpp," m_freeTNs.size = %lu\n",this->m_freeTNs.size());

    fprintf(fpp," garbage.size   = %lu\n",this->m_garbage.size());

    fprintf(fpp," The Models expression list\n");
    this->List(fpp," ",0,0);
    return k;
}
int RXSail::Hoist(void){

    int sli = SailListIndex();
    int err=1;
    int h=1;
    if(IsHoisted() ) {
        RXSTRING l_buf = TOSTRING(GetType().c_str()) + _T(" (") + TOSTRING(m_attributes.Array())+ _T(") already hoisted");
        rxerror(l_buf,2);
        return 0;
    }
    SAIL *l_sail = g_World->FindModel(GetType(),1);
    if(l_sail) {
        RXSTRING l_buf(_T("While hoisting '"));
        l_buf+= TOSTRING(GetType().c_str()) + TOSTRING(m_attributes);
        l_buf+=+_T("'\n Found another model ::'");
        l_buf+=  TOSTRING(l_sail->GetType().c_str()) + TOSTRING(l_sail->m_attributes);
        l_buf+= _T("' already hoisted") ;
        rxerror(l_buf,2);
        return 0;
    }

    SetLoadingIndex();

#ifdef FORTRANLINKED
    cf_sethoisted(sli,h,&err);
    cf_setactive(sli,h,&err);
    this->GetWorld ()->PackLoadingIndices ();
    this->AddToSummary();
    if(err)
        return false;
#endif
    SetFlag(FL_NEEDS_POSTP);
    SetFlag(FL_NEEDS_TRANSFORM);
    SetFlag(FL_HAS_PRESSURES,0);
    if(!this->m_fileBAG.isEmpty ())
        Post_Summary_By_Sail(this,"file$",qPrintable(this->m_fileBAG));
    this->m_Graphic->ConnectToPostProc() ;

    g_World->SetUpAllTransforms(1);// needs to follow setactive, but must use LastKnownGood rather than DeflectedPos
g_World->PostProcessAllSails(3);


    return true;
}
int RXSail::Drop()
{
    /* to drop the sail we must -
     remove all reference to it from the boat database
     this means getting rid of its entries in parent / dep list of boat
     removing all the connects generated by the mainsail etc. connections.
     decrementing the no of models - a note of which one is now free.
     
     */
    cout<<" TODO: verify postprocessing UnhooK in Drop" <<endl;

    relaxflagcommon.Stop_Running = 1;
    int retVal=0;
    int  err=1;

    int sli = SailListIndex();
    //    sprintf(buf,"file$%s",GetType().c_str());
    Post_Summary_By_Sail(this,"file$","dropped");

#ifdef _C_CONNECTS
    DeleteConnects(sd);
#endif

    DeleteSailReferences();
    this->RemoveFromSummary();// inverse of AddToSummary
    int h=0;
#ifdef FORTRANLINKED
    cf_sethoisted(sli,h,&err);  // does the unresolve_connects
    cf_setactive(sli,h,&err);
#endif
    this->GetWorld ()->PackLoadingIndices();
#ifdef _X
    UnTouchViewSegments(this);

    UpdateMenu();

    delete_trim_shell ( g_trim_shell );  g_trim_shell =NULL;
#endif
    return(retVal);
} // RXSail::Drop

// The loading index is  unique  for each hoisted sail
// to call this fn with the argument is dangerous because it risks
// giving duplicate values.
// it should only be called with arg from RelaxWorld::PackLoadingIndices()
int  RXSail::SetLoadingIndex(const int q)  // was 'pansail index' the loading index, starting with 1, applied to hoisted only
{

    if(q) {
        this->m_LoadingIndex =q;
        return q;
    }
    // find the smallest unused value by testing all the hoisted sails.
    set<int> myset;
    set<int>::iterator it;

    int  i,k,rc=-1;
    RelaxWorld *w = GetWorld();
    vector<RXSail*> lsl = w->ListModels(RXSAIL_ISHOISTED); // not the boat
    for(i=0;i<lsl.size();i++)  {
        SAIL *theSail = lsl[i];
        if(theSail==this) continue;
        k=theSail->GetLoadingIndex();
        myset.insert(k);
    }
    for(rc=1; ; rc++) {
        if(myset.find(rc)==myset.end()){
            m_LoadingIndex=rc;
            return rc;
        }
        else
            printf("SetLoadingIndex found value at index  %d\n",rc);
    }
    assert(0); // cant ever get here.
    m_LoadingIndex=rc;
    return rc;
}
#ifdef HOOPS
int RXSail::SubtractHoopsModel()
{
    /* detach the sail named Sname and with model no. m to the boat & insert it
 * into all the view windows
 */

    int retVal = 1;
    char Hname[512];
    char buf[512];
    int found = 0;

    sprintf(Hname,"%s",this->GetType().c_str());
    PC_Strip_Leading(Hname);
    PC_Strip_Trailing(Hname);

    if(strlen(Hname) < 1) {
        rxerror("SubtractModel called with bad Hname",2);
    }
    else {

        strrep(Hname,47,'+'); 		/* replace slashes with '+' */

        sprintf(buf,"/...camerabase/models/%s",Hname);
        HC_Begin_Segment_Search(buf);
        while(HC_Find_Segment(buf)) {
            HC_Delete_Segment(buf);
            found++;
        } /* end of find Hname segments */
        HC_End_Segment_Search(); /* end of camerabase search */

        if (HC_QShow_Existence("/...non_exclusive/wakes","self"))
            HC_Flush_Contents("/...non_exclusive/wakes","everything"); /* temporary fix to get rid of all foot wakes */
        /* this is necessary because the pansail foot wake info is stored PACKED into an array. */
        /* to do it properly you must know whether the OTHER sails have footwakes as well */


        if(Hoops_Nodes != 0){
            HC_Delete_By_Key(Hoops_Nodes);assert(!g_Hoops_Error_Flag);}
        Hoops_Nodes = 0;

        if(Hoops_Strings != 0){		// experiment April 2003
            HC_Delete_By_Key(Hoops_Strings);assert(!g_Hoops_Error_Flag);}
        Hoops_Strings = 0;
        HC_Open_Segment_By_Key(this->PostProcNonExclusive());
        HC_Flush_Contents(".","geometry,subsegments");
        HC_Close_Segment();

    }

    return(retVal);
}
#endif

int RXSail::DeleteSailReferences()
{
    /* set the active flag in the offsets list to 0.
     Delete the sail entity.
     eventually delete the hoops shells etc.
     */
    int retVal = 0;
#ifdef HOOPS
    this->SubtractHoopsModel();
#else
    HC_Flush_Segment( this->PostProcExclusive() );
#endif
    ClearFlag(FL_HAS_AERODATA);

    m_LoadingIndex=-1;
#ifdef _X
    Close_All_Options();
#endif
    return(retVal);
}

bool RXSail::IsHoisted()const{
    int err=1;
    bool rc= false;
#ifdef FORTRANLINKED
    rc= (0!=cf_ishoisted( SailListIndex() ,&err));
#endif
    if(!err) return rc;
    return false;
}

bool RXSail::IsBoat() const{
    bool rc = (this==g_boat); // YUCH
    if(rc) assert(GetFlag(FL_ISBOAT) );
    return rc;
}
int RXSail::SetFlag(const int which,const int value) 
{
    int old;
#pragma omp critical (rxsail_flag) 
    {
        assert(which>=0 && which < NUM_OF_FLAGS);
        old = m_Flags[which];
        if(old != value)
        m_Flags[which] = value;
    }
    return(old);
}
int RXSail::ClearFlag(const int which) 
{
    assert(which>=0 && which < NUM_OF_FLAGS);
    int old;
#pragma omp critical (rxsail_flag) 
    {
        old = m_Flags[which];
        m_Flags[which] = 0;
    }
    return(old);
}
int RXSail::GetFlag(const int which) const
{	int rv;
    assert(which>=0 && which < NUM_OF_FLAGS);
#pragma omp critical (rxsail_flag)
    {
        rv= m_Flags[which];
    }
    return(rv);
}

/**********************ONLY RETURNS RESOLVED ENTITIES *************************/

RXEntity_p RXSail::Get_Key_With_Reporting(const char *spin,const char *name,bool PleaseResolve) {

    /* A logic to allow a seam, batpatch, etc to be defined as (eg) (luff@50%)
     1)  see if the type includes 'relsite' and the name can be parsed as (luff@50%)
     2) If it can't be, skip to Z)
     3) try a get-key with Global_Report_Forming on the seamname
     4) If unsuccessful, return(NULL)
     5) If successful, generate a relsite at that position and return its key
     
     Z) try to Get_Key on (spin,name) as of old.
     
     */
    struct REPORT_FORM*r = this->GetReportForm();
    RXEntity * p;

    if(stristr(spin,"relsite")	) {
        char seamname[256],offset[256],sitename[256];
        if(1==Parse_Seam_Position(name,seamname,offset)) {
            char line[256];
            RXEntity* seam, *siteptr;

            seam= Get_Key_With_Reporting("seamcurve,compound curve",seamname);
            if(!seam)   //   (" seamcurve <%s> doesnt exist yet\n", seamname);
                return(NULL);

            sprintf(sitename,"%sat%sz",seam->name(),offset);	/* was %s%s++ */

            /* the above was  seamname not seam->name
  changed March 1997 in case the seam is the object of an alias */

            if(!(siteptr=this->GetKeyWithAlias("relsite",sitename))) {
                sprintf(line,"relSite: %s : %s : %s :! made by getkey",sitename,seamname, offset);
                /* NOTE . Keep the phrase 'made by getkey' . It is used in write_script */
                siteptr=Read_Relsite_Card(r->m_Sail ,line);
                siteptr->Needs_Finishing = !siteptr->Finish();
            }
            return(siteptr);
        }
    }

    p = this->GetKeyWithAlias(spin,name);
    if(p) {
        if(!p->Needs_Resolving)
            return p;
        if(PleaseResolve){
            p->Resolve();
            if(!p->Needs_Resolving)
                return p;
            // cout<<" unsuccessful a recursive resolve on "<<spin<<","<<name<<endl;
        } // pleaseResolve
    } // if p.  here it doesnt exist, resolved or no

    r->z=(REPORT_ITEM *)REALLOC(r->z,((r->N)+1)*sizeof(struct REPORT_ITEM));
    if(r->m_owner){
        r->z[r->N].otype=STRDUP(r->m_owner->type()); strtolower(r->z[r->N].otype);
        r->z[r->N].oname=STRDUP(r->m_owner->name()); strtolower(r->z[r->N].oname );
    }
    else {
        r->z[r->N].otype=NULL;
        r->z[r->N].oname=NULL;
    }
    r->z[r->N].ctype=STRDUP(spin);	 strtolower(r->z[r->N].ctype);
    r->z[r->N].cname=STRDUP(name);	 strtolower(r->z[r->N].cname);
    r->z[r->N].value=0;

    if(r->z[r->N].otype ) {
        if (strieq(  r->z[r->N].otype,"rename")) { /* PH 28/7/95 */
            RXFREE(r->z[r->N].cname);
            r->z[r->N].cname=STRDUP(r->z[r->N].oname);
        }
    }
    (r->N)++;
    return(NULL);
}	

RXEntity * RXSail::GetFirst(const  char* spin) const{

    for (ent_citer it = m_emap.begin (); it !=m_emap.end(); ++it){// RXSail::GetFirst
        RXEntity* p  = it->second;
        if(p && streq(p->type(),spin))
            return p;
    }
    return(0);
} // getfirst

RXEntity * RXSail::GetFirst(const RXSTRING spin) const{

    vector<RXSTRING> wds= rxparsestring(spin,_T( ","));

    for ( vector<RXSTRING>::const_iterator w=wds.begin(); w!=wds.end();++w  ) {
        string type = ToUtf8(*w);
        const char *ctype = type.c_str();
        for (ent_citer it = m_emap.begin (); it !=m_emap.end(); ++it){// RXSail::GetFirst
            RXEntity* p  = it->second;
            if(p && streq(p->type(),ctype))
                return p;
        }
    } // for w
    return(0);
} // getfirst



RXEntity* RXSail::Entity_Exists(const char *name,const char *type) const{ // fast doenst deal with aliasses
    assert(name&&type);
    RXEntity* pfound =0;
    string k = EntityKey(name,type);
    ent_citer it = m_emap.find(k);
    if(it!=m_emap.end()){
        pfound = it->second;
        return(pfound);
    }
    return 0;
} // Entity_Exists
RXEntity * RXSail::GetKeyNoAlias(const RXSTRING spin, const RXSTRING p_name) const{
    vector<RXSTRING> wds= rxparsestring(spin,_T( ","));
    string name = ToUtf8(p_name);
    for ( vector<RXSTRING>::const_iterator w=wds.begin(); w!=wds.end();++w  ) {
        RXEntity* pfound =0;
        string type = ToUtf8(*w);
        ent_citer it = m_emap.find(EntityKey(name.c_str(),type.c_str()   ));
        if(it!=m_emap.end()){
            pfound = it->second;
            return(pfound);
        }
    } // for w
    return(NULL);
}

RXEntity* RXSail::GetKeyWithAlias(const char*spin, const char*name) const{ //GetKeyWithAlias

    char StatSpin[512];
    unsigned j;
    RXEntity* p;

    if(spin) {
        strncpy(StatSpin,spin,510);
    }
    else cout<<"WHY  RXSail::Get Key with zero spin and name <"<<name<<">"<<endl;

    if(name) {
        p = Get_Aliassed_Entity(StatSpin,name);
        if(p)
            return(p);
    }
    else {
        printf("RXSail::Get Key type=%s, name NULL \n", spin);  fflush(stdout);
    }
    assert(name);
    std::string sline(StatSpin );
    std::vector<std::string> wds = rxparsestring(sline,",",false);
    int nw = wds.size();
    for (j=0; j<nw  ;j++) {
        RXEntity* pfound =0;
        ent_citer it = m_emap.find(EntityKey(name,wds[j].c_str()));
        if(it!=m_emap.end()){
            pfound = it->second;
            return(pfound);
        }
    } // for j
    return(NULL);
}
ent_citer RXSail::MapIncrement(ent_citer it){

    ent_citer rv;
#pragma omp critical (rxsail_map) 
    {
        if(m_maphaschanged )
        {
            m_maphaschanged=false;
            rv= m_emap.begin();
        }
        else{
            it++;
            rv=it;
        }
    }
    return rv;

}

int RXSail::MapInsertObject(RXEntity* p){ 
#pragma omp critical (rxsail_map) 
    {
        m_emap[EntityKey(p)]=p;
        m_maphaschanged=true;
    }
    return 1;
}

int RXSail::MapRemoveObject( RXEntity* p){ // It may not exist
    int rv=0;
#pragma omp critical (rxsail_map) 

    {
        entiter_t it = m_emap.find(EntityKey(p->name(),p->type()));
        if(it!=m_emap.end()){
            m_emap.erase(it);
            m_maphaschanged=true;
            rv=1;
        }
    }
    if(rv) return rv;
    printf("MapRemove didnt find  %s %s\n", p->type(),p->name());
    return 0;

}
RXEntity* RXSail::Get_Aliassed_Entity( const char *spin,const char *name) const {

    register RXEntity_p p;

    char buf[1024];
    assert(name&&spin);

    std::string sline(spin );
    std::vector<std::string> wds = rxparsestring(spin,",",false);
    int nw = wds.size();
    for (unsigned j=0; j<nw ;j++) {
        sprintf(buf,"%s(%s)",name,wds[j].c_str());	/* try a quick look up */
        strtolower(buf);
        string  kk= EntityKey(buf,"alias") ;
        //p=0;
        ent_citer it = m_emap.find(kk);
        if(it!=m_emap.end()){
            p = it->second;
            if(p) {
                struct PC_ALIAS *a;//	RXEntity_p pd = dynamic_cast<RXEntity_p>(p);
                a = (struct PC_ALIAS *) p->dataptr; //was pd
                return(a->p);
            }
        }
    } // for J
    return(0);
}
int RXSail::ExportAllExpressions(wostream &s){
    int rc=0;
    RXSTRING wh=TOSTRING(this->GetType().c_str());
    wh +=RXSTRING(L"$global");
    rc+= this->RXENode::Export(s,wh,0);

    for (ent_citer it = m_emap.begin (); it !=m_emap.end(); ++it){ // ExportAllExpressions
        RXEntity* p  = it->second;
        if(p) {
            if(p->GetMapSize ()){
                wh= TOSTRING(this->GetType().c_str());
                wh+=L"$"+TOSTRING(p->type());
                wh+=L"$"+TOSTRING(p->name());
                rc+= p->RXENode::Export(s,wh,0);
            }
        }
    }
    return rc;
}
int RXSail::ListAllExpressions(FILE *fp) {

    int rc=0;
    rc+= fprintf(fp, "\n**********\nExpressions for model %s (%S) \n",  this->GetType().c_str(),this->GetName ().c_str());
    rc+= this->RXENode::List(fp," ",0,1);
    for (ent_citer it = m_emap.begin (); it !=m_emap.end(); ++it){// ListAllExpressions
        RXEntity* p  = it->second;
        if(p) {
            if(p->GetMapSize ()){
                fprintf(fp,"Entity \t%16s\t%16s\n",p->name(),p->type());
                rc+= p->RXENode::List(fp," ",1,1);
            }
        }
    }

    return rc;
}
int RXSail::MapDump( FILE *fp){
    int c=0;
    for (ent_citer it = m_emap.begin (); it !=m_emap.end(); ++it,c++){ //MapDump
        RXEntity* p  = it->second;
        fprintf(fp," %d\t  %32s\t %p\t",c,it->first.c_str()  ,p);
        if(p) fprintf(fp,"%16s\t%16s\n",p->name(),p->type());

    }
    return c;
}
// a speed optimisation.  If we know that the following loop is going to create
// entities NOT of type <type> it is faster to prepare the list beforehand because
//otherwise we have to reset the iterator whenever an ent is created.
// If this routine gets slow, do it by sorting instead.
// to get all the unresolved objects, go
//MapExtractList(0, vector<RXEntity_p >&thelist,  PCE_ISNOTRESOLVED) ;
size_t RXSail::MapExtractList(const int type, vector<RXEntity* >&thelist, const int filter) const{

    for (ent_citer it = m_emap.begin (); it !=m_emap.end(); ++it){  // MapExtractList
        RXEntity* p  = it->second;

        if (filter&PCE_ISNOTRESOLVED && !p->Needs_Resolving)
            continue;
        if (filter&PCE_ISRESOLVED && p->Needs_Resolving)
            continue;
        if(type == 0 || p->TYPE==type)
            thelist.push_back(p);
    }

    return thelist.size();
} //RXSail::MapExtractList


string RXSail::EntityKey(const char *name,const char *type)const {
    string buf;
    /* the key should sort first by type, then by name.
and the type should give the sequence
set, site, basecurve, SC, relsite, and the rest. */
    assert(name&&type);
    //int i = INTEGER_TYPE(type);
    //char cbuf[256];
    //sprintf(cbuf,"%5.5i_",i);
    //
    //buf=string(name)+string("$")+string(type) ;
    buf=string(type)+string("$")+string(name) ;
    if(buf[0]=='/' )buf[0]='z';
    else  if(buf[0]=='\\' )buf[0]='s';

    return rxtolower(buf);
}

string RXSail::EntityKey(const RXEntity* p) {
    return  EntityKey(p->name(),p->type());
}
bool RXSail::IsOK(const char*p_s) const
{
    // see OpenBuildFile Line 227
    // A bag file doesnt need gendp.

    //	if(Global_Gauss_Cut_Dist > sc->arcs[1]/4.0) {
    //
    //	}
    if(this->GetMouldSurface()) {
        ON_3dPoint p;
        ON_3dVector du,dv;
        if(this->GetMouldSurface()->Ev1Der(0.5,0.5,p,du,dv)){

            // we want du to be largely aft and dv to be largely up (may be Y)
            du.Unitize (); dv.Unitize ();
            if(fabs(dv.x) > fabs(du.x) )
                rxerror("WARNING. the mould may need flipping ",2);
        }
    }

    return true;
}

ON_String  RXSail::Serialize(){ // like rewrite_line
    ON_String l_line;
    /*  0  NAME:
 1	main:
 2	description:
 3	x:
 4	y:
 5	z
 6	[:attributes]*/
    //	RXEntity_p e = sd->m_sdowner ;
    ON_String temp;
    l_line=ON_String("name : " )
            +ON_String(GetType().c_str())
            +ON_String(" : ")
            +m_attributes;

    temp.Format(" : %f : %f : %f ", m_Zaxis.x,m_Zaxis.y,m_Zaxis.z);
    l_line+=temp;
    int i = GetFlag(FL_ONFLAG );
    temp.Format(" :$Linear_Tol=%f,$Cut_Tol=%f,$Angle_Tol=%f,$onflag=%d,$pressure_factor=%f",
                this->m_Linear_Tol,
                this->m_Cut_Dist,
                this->m_Angle_Tol ,
                i,
                this->m_Pressure_Factor);

    l_line+=temp;
    l_line.MakeLower();

    if(this->FlagQuery(NO_BEAM_POST)) l_line+=  ",$nobeampost";
    if(this->FlagQuery(NO_UV_MAP)) l_line+= ",$nouvmap";
    return l_line;

} //  RXSail::Serialize()
int  RXSail::Deserialize(const char *p_s){

    /* NAME card is of type
     0    1           2 3 4
     NAME:name:anything else : x:y:z [:attributes]! comment  atts added Oct 2003 */
    // sn[6] might be "$pressure_Factor=<double>"

    char *sn[20],*lp;
    int i;
    SAIL *SP = this;
    int err,l3;
    int sli;


    int retVal = 0;  /* set to fail */
    //  char alias[256],buf[512];
    char line[2048];

    memset(sn,0,20*sizeof(char*)); // initialise

    strncpy(line,p_s,2040);
    strtolower(line);    // fortran character testing is case sensitive

    lp = strchr(line,'!');
    if(lp) *lp = '\0';	/* strip comments */

    lp=strtok(line,RXENTITYSEPS);
    i=0;
    while(((lp=strtok(NULL,RXENTITYSEPS))!=NULL)&& i < 20) {
        sn[i]=lp;
        PC_Strip_Leading(sn[i]);
        PC_Strip_Trailing(sn[i]);
        i++;
    }
    if(i < 2) {
        rxerror(" incomplete Name specification",4);
        goto End;
    }

    // this is the g_boat reader
    this->m_type = ToUtf8(TOSTRING(sn[0]));
    this->SetName(TOSTRING(sn[0]));

    if(this->GetName () ==L"boat") {
        if(g_boat&& g_boat !=this) {
            rxerror(" there is already a boat",3);
            g_boat->Drop();
        }
        g_boat=this;
        SetFlag(FL_ISBOAT,true);
    }
    m_attributes=ON_String(sn[1] );

    if(i >= 5) {		/* found a z-axis setting */
        this->m_Zaxis.x = atof(sn[2]);
        this->m_Zaxis.y = atof(sn[3]);
        this->m_Zaxis.z = atof(sn[4]);
    }

#ifdef FORTRANLINKED
    sli = this->SailListIndex();
    cf_setsailptr	(sli,&SP,		&err);

    //dbg
#ifdef _DEBUG
    void  *eSP ;
    IsHoisted();
    cf_isactive (sli,&err);
    eSP = cf_getsailpointer(sli,&err);
    assert(eSP==this);
#endif
    //end dbg


    l3= (int)this->m_type .size ();
    cf_setsailtype (sli,this->m_type.c_str(), l3, &err);
#endif

    // grandfather 'old' scripts
    this->m_Pressure_Factor=1.0;
    this->m_Linear_Tol = g_SOLVE_TOLERANCE ;
    this->m_Cut_Dist= g_Gauss_Cut_Dist;
    this->m_Angle_Tol= g_ANGLE_TOL ;
    this->SetFlag(FL_ONFLAG,g_Use_ONCurve );

    if(i>5)
        Parse_Name_Attributes(sn[5],this);

#ifdef HOOPS
    sprintf(alias,"?%s",this->GetType().c_str ()   );
    HC_Begin_Alias_Search();
    while(HC_Find_Alias(buf)){
        if(strieq(alias,buf)) {
            HC_UnDefine_Alias(alias);
            break;
        }
    }
    HC_End_Alias_Search();

    HC_Show_Pathname_Expansion(".", buf);
    HC_Define_Alias(alias,buf);  // EG ?boat
#endif
    retVal = 1;
End:
    return(retVal);


} // RXSail::Deserialize

// SSIN currently only implemented for editword. Thats bizarre
// but I'm not sure that it is a useful idea anyway.
bool RXSail::SaveStateIfNecessary(void)
{ 
    if( ! m_tempstatefile.IsEmpty())
        return false;
    if(!IsHoisted())
        return false;
    char  tmpfile[512];
    strcpy(tmpfile,"RlxtmpState_XXXXXX");

#ifdef linux
    if( mkstemp(tmpfile) ==-1)
        return false;
#elif ! defined(__GNUC__)
    if(_mktemp_s(tmpfile,510))
        return false;
#endif
    m_tempstatefile=ON_String(tmpfile)+ON_String(".ndd");
    QStringList wds;
    wds<< "Save State"<<GetType().c_str()<< m_tempstatefile.Array();


    return (0!=Do_Save_State(wds,GetGraphic()));

} //RXSail::SaveStateIfNecessary

bool RXSail::LoadStateIfNecessary(void) 	// do the state load and empty tempstatefile
{
    QStringList words;
    if( m_tempstatefile.IsEmpty())
        return false;
    if(!IsHoisted())
        return false;

    cout << "RXSail::LoadStateIfNecessary" <<endl;


    //    sprintf(line,"Load State : %s  : %s", GetType().c_str(), m_tempstatefile.Array());
    words<<"Load State "<<GetType().c_str()<<m_tempstatefile.Array();
    m_tempstatefile.Empty(); // this stops us recurring into here

    bool iret = (0!=Do_Load_State(words,GetGraphic()));

 // whatever was meant here, it looks like a disaster
#ifdef NEVER
#ifndef _DEBUG	
    remove(m_tempstatefile.Array());
#endif
#else
     m_tempstatefile.Empty();
#endif
    return iret;
} //RXSail::LoadStateIfNecessary

/*Writing a 3dm
1) if the sail has non-null m_p3dmModel, this model gets copied into the output file
2) For all  entities which have no ON_Object, append their Line into the notes, except for 
    case INTERPOLATION_SURFACE:
    case DXF :
    case PCE_TPN :
    case PCE_IGES :
    case PCE_3DM:
 which we skip
 and
    case SEAMCURVE
    case SITE:
    case RELSITE:
 whose geometry we write.
theres a confusion with layer indices. 

Finally, any other hoops objects which arent RXEntities get written into layer 0
but it's only nurbs surfaces and polylines.   which is a shame because hoops2on knows about

NurbsSurface ,Marker,Polyline,Polygon,Line, NurbsCurve
*/
int RXSail::Delete_Whole_Database()
{
    /* deletes all of the entities in the given sail
   * starts by checking if boat - if so then it unhoists the sails
   * and then continues.
   */
    RXEntity* p = NULL;
    int n=0;
    std::set<class RXEntity *>::iterator itt;
    char buf[1024];

    int i,retVal=0; //end
    HC_Open_Segment_By_Key(GetGraphic()->m_ModelSeg);
    HC_Open_Segment("data");

    if(this->IsBoat()) {     /* unhoist all the sails */

        vector<RXSail*> ll = GetWorld()->ListModels(RXSAIL_ISHOISTED);
        for(n=0;n<ll.size(); n++) {
            RXSail *l_s = ll[n];
#ifndef WIN32
            sprintf(buf,"drop : %s ",l_s->GetType().c_str());
            Do_Drop_Sail(buf,NULL);
#else
            this->Drop();
#endif	
        }
    }
    else { // not the boat
        int err=0;

#ifdef FORTRANLINKED
        if(this->IsHoisted()) {
#ifndef WIN32
            sprintf(buf,"drop : %s ", this->GetType().c_str()); // sEnt->name);
            Do_Drop_Sail(buf,NULL);
#else
            this->Drop();
#endif
        }
#else 
        assert(0);
#endif
    }
    ZeroSpecialLists(0);
    ent_citer it;
    for (it = MapStart(); it != MapEnd();it=MapIncrement(it)){
        p  = it->second;
        m_garbage.insert (p);
    } // for it

    retVal = 1;
    class RXEntity* eee;
    try{  // now a last pass - to do the freeing
        for (;m_garbage.size(); ){
            itt = m_garbage.begin();
            eee=*itt;
            delete eee; // removes it from garbage
        } // for garbage

    }
    catch (RXException e) {
        e.Print(stdout);
    }
    catch( char * str ) {
        printf( "Exception raised:  <%s>\n",str);
    }
    catch( unsigned int ex ) {
        printf( "Exception raised: no= %d\n",ex);
    }
    catch(...) {
        rxerror(" unidentified error in garbage collection",2);
    }

    for(i=0;i<RX_MAX_TYPES;i++)
        this->m_Selected_Entity[i]=0;

    //   this->Free_Sail_Arrays() ; // again(?) to catch edgelist

    HC_Close_Segment(); 	HC_Close_Segment();
    return(retVal);
}
int RXSail::DeleteSomeEnts(const int type ) { // (feb 2010) doesnt do anything if the psides already have m_sList.size()>0 
    int rc=0;
    RXEntity_p  p;
    vector<RXEntity* >thelist;
    MapExtractList(type,  thelist) ; rc=(int) thelist.size();
    vector<RXEntity* >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        p =dynamic_cast<RXEntity_p >(*it );
        rc++;
        m_garbage.insert(p);// p->Kill();
    }
    set<class RXEntity*>::iterator itg;
    for (;m_garbage.size(); ){
        itg = m_garbage.begin();
        p =dynamic_cast<RXEntity_p >(*itg );
        delete p;
    } // for garbage
    return rc;
} 
int RXSail::Tidy() {
    int rc=0;
    //#ifdef DEBUG
    //	cout << "model before tidy" <<endl;
    //	this->PrintModelSummary(stdout,0);
    //#endif
    // remove garbage
    RXEntity_p  p;
    set<class RXEntity*>::iterator itg;
    for (;m_garbage.size(); ){
        itg = m_garbage.begin();
        p =dynamic_cast<RXEntity_p >(*itg );
        p = (*itg );
        delete p;
    } // for garbage

    // shrink the Fnode list.
    // 1) pop the freelist onto a std::set which will put it into order
    // 2) while the last of m_fnodes is null, decrement fnodes.size and erase the N from the set
    // 3) while fnodes size < (capacity-block) set capacity downwards.(Needs the lists to be deques not vectors)
    // 4) finally, push the contents of the set back onto the freelist
    int n,nn=0; set<int> buf; set<int>::iterator it;
    for(;;){
        if(m_freeNNs.empty()) break;
        n=m_freeNNs.front();
        m_freeNNs.pop();		buf.insert (n);
    }
    //2
    for(;buf.size();){
        nn=m_Fnodes.size(); if(nn<2) break;
        class RX_FESite*s = this->m_Fnodes.back();
        if(s)		break;
        this->m_Fnodes.pop_back();		buf.erase (nn-1);
    }
    //3

    //4)
    for(;buf.size();){
        it=buf.begin();		n = *it;
        if(n<nn)
            m_freeNNs.push(n);
        buf.erase(it);
    }

    // shrink the Fedge list.
    buf.clear();
    for(;;){
        if(m_freeENs.empty()) break;
        n=m_freeENs.front();
        m_freeENs.pop();		buf.insert (n);
    }
    //2
    for(;buf.size();){
        nn=m_Fedges.size(); if(nn<2) break;
        class RX_FEedge*s = this->m_Fedges.back();
        if(s)		break;
        this->m_Fedges.pop_back();		buf.erase (nn-1);
    }
    //3

    //4)
    for(;buf.size();){
        it=buf.begin();		n = *it;
        if(n<nn)
            m_freeENs.push(n);
        buf.erase(it);
    }

    // shrink the FTri list.
    buf.clear();
    for(;;){
        if(m_freeTNs.empty()) break;
        n=m_freeTNs.front();
        m_freeTNs.pop();		buf.insert (n);
    }
    //2
    for(;buf.size();){
        nn=m_FTri3s.size(); if(nn<2) break;
        class RX_FETri3*s = this->m_FTri3s.back();
        if(s)		break;
        this->m_FTri3s.pop_back();		buf.erase (nn-1);
    }
    //3
    //4)
    for(;buf.size();){
        it=buf.begin();		n = *it;
        if(n<nn)
            m_freeTNs.push(n);
        buf.erase(it);
    }
    // remove garbage again
    for (;m_garbage.size(); ){
        itg = m_garbage.begin();
        p =dynamic_cast<RXEntity_p >(*itg );
        delete p;
    } // for garbage

    return rc;
}
#ifdef NEVER
int RXSail::Free_Sail_Arrays( void) {

    while (edgelist){
        FortranEdge *ss= EdgePop();
        delete ss;
    }

    while (trilist){
        FortranTriangle *t=TriPop();
        delete t;  // Peters guess Jan 2005 maybe just the pop
    }

    return 1;
}
#endif


int RXSail::NoteTri(RX_FETri3*s){
    int n=s->GetN();
#pragma omp critical (rxsail_ftris )
    {
        if(n>=m_FTri3s.capacity())
            m_FTri3s.reserve (n+TRIBLOCKSIZE);
        if(n>=m_FTri3s.size())
            m_FTri3s.resize (n+1);
        m_FTri3s[n]=s;
    }
    return n;
}
int RXSail::PushTriNo(const  int i)
{
    int rc=0;
#pragma omp critical (rxsail_ftris )
    {
        m_freeTNs.push(i);
        rc= m_freeTNs.size();
    }

    class RX_FETri3* p;
    for(;this->m_FTri3s.size()>1;){
        p = this->m_FTri3s.back();
        if(p)
            break;
        this->m_FTri3s.pop_back();
    }
    return rc;
}
int RXSail::PopTriNo()
{	int rc;	
#pragma omp critical (rxsail_ftris )
    {
        if(!m_freeTNs.empty()) {
            rc=m_freeTNs.front();
            m_freeTNs.pop();
        }
        else
            rc= m_FTri3s.size();
    }
    return rc;
}



int RXSail::PopEdgeNo() {
    if(!m_freeENs.empty()) {
        int rc=m_freeENs.front();
        m_freeENs.pop();
        return rc;
    }
    return  this->m_Fedges.size();
}
int RXSail::PushEdgeNo(const  int i) {
    int rc=0;
    m_freeENs.push(i);
    rc= m_freeENs.size();


    class RX_FEedge* p;
    for(;this->m_Fedges.size()>1;){
        p = this->m_Fedges.back();
        if(p)
            break;
        this->m_Fedges.pop_back();
    }
    return rc;
}
int RXSail::NoteEdge(RX_FEedge*e) {
    int n=e->GetN();
    if(n>=m_Fedges.capacity())
        m_Fedges.reserve (n+EDGEBLOCKSIZE);
    if(n>=m_Fedges.size())
        m_Fedges.resize (n+1);
    m_Fedges[n]=e;
    return n;
}

int RXSail::NoteNode(RX_FESite*s){
    int n=s->GetN();
#pragma omp critical (rxsail_fnodes )
    {
        if(n>=m_Fnodes.capacity())
            m_Fnodes.reserve (n+NODEBLOCKSIZE);
        if(n>=m_Fnodes.size())
            m_Fnodes.resize (n+1);
        m_Fnodes[n]=s;
    }
    return n;
}
int RXSail::PushNodeNo(const  int i)
{
    int rc=0;
#pragma omp critical (rxsail_nn )
    {
        m_freeNNs.push(i);
        rc= m_freeNNs.size();
    }

    class RX_FESite* p;
    for(;this->m_Fnodes.size()>1;){
        p = this->m_Fnodes.back();
        if(p)
            break;
        this->m_Fnodes.pop_back();
    }
    return rc;
}
int RXSail::PopNodeNo()
{	int rc;	
#pragma omp critical (rxsail_nn )
    {
        if(!m_freeNNs.empty()) {
            rc=m_freeNNs.front();
            m_freeNNs.pop();
        }
        else
            rc= m_Fnodes.size();
    }
    return rc;
}

double RXSail::Get_Init_Edge_Length(const int theEdge) const
{
    return( m_Fedges[theEdge]->GetLength() );
}


RX_FESite* RXSail::GetSiteByNumber(const int p_n) const{

    if (!(p_n>0 && p_n < this->m_Fnodes.size() )){
        ON_String buf; buf.Format("asked for node %d/%d",p_n, this->m_Fnodes.size());
        RXTHROW(buf.Array());
    }

    return  m_Fnodes[p_n];
}

int RXSail::WriteNastran(const char *p_fname) {
    char filename[512];
    filename_copy_local(filename,512,p_fname);
    return cf_write_nastran(SailListIndex()  ,filename,(int) strlen(filename));

}
int RXSail::Write_stl(const char* fnamein)
{
    int rc=0;
    char filename[512];
    filename_copy_local(filename,512,fnamein);


    ON_3dPoint p;
    ofstream dst;
    dst.open(filename,ios::out);
    if (dst.fail()) { cout<<" cant open file '"<<filename<<"'"<<endl; return rc; }
    dst<<"solid " << this->GetType().c_str()<<endl;

    for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
        RX_FETri3* t = *it;
        if(!t) continue;
        ON_3dPoint p0 = t->GetNode(0) -> DeflectedPos();
        ON_3dPoint p1 = t->GetNode(1) -> DeflectedPos();
        ON_3dPoint p2 = t->GetNode(2) -> DeflectedPos();
        ON_3dVector nor = ON_CrossProduct(p1-p0,p2-p0); nor.Unitize();

        dst <<"  facet normal "<<nor.x<<" "<<nor.y<<" "<<nor.z<<endl;
        dst <<"    outer loop"<<endl;
        dst <<"      vertex "<<p0.x<<" "<<p0.y<<" "<<p0.z<<endl;
        dst <<"      vertex "<<p1.x<<" "<<p1.y<<" "<<p1.z<<endl;
        dst <<"      vertex "<<p2.x<<" "<<p2.y<<" "<<p2.z<<endl;
        dst <<"    endloop"<<endl;
        dst <<"   endfacet"<<endl;
        rc++;

    } // tri loop

    dst<< "endsolid " <<this->GetType().c_str() <<endl;
    dst.close();
    cout<<" stl file <"<<filename<<"> has "<<rc<<" facets "<<endl;
    return rc;
}
int RXSail::Write_uvdp(const char* fnamein)
{
    int rc=0;
    char filename[512];
    filename_copy_local(filename,512,fnamein);
    ON_3dPoint p;
    ofstream dst;

    dst.open(filename,ios::out);
    if (dst.fail()) { cout<<" cant open file '"<<filename<<"'"<<endl; return rc; }
    rc=1;

    for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
        RX_FETri3* t = *it;
        RXSitePt q;
        if(!t) continue;
        t->Centroid(&q);
        dst <<q.m_u<<"  "<<q.m_v<<"  "<< t->GetPressure() <<endl;
    }

    dst<<" 0  -1  0"<<endl;
    dst<<" 1  -1  0"<<endl;
    dst<<" 2  0  0"<<endl;
    dst<<" 2  1  0"<<endl;
    dst<<" 1  2  0"<<endl;
    dst<<" 0  2  0"<<endl;
    dst<<" -1  1  0"<<endl;
    dst<<" -1  0  0"<<endl;
    dst.close();
    cout<<"written UVDP file :"<< filename<<endl;

    return rc;
}
/*
 VTK files (legacy )and vtu (unstructured grid) only allow one piece per file
 the sail surface is one piece.
  we can write all the line objects (bars, strings, beams, etc) as one piece
 If we want multiple pieces, we have to write a .pvd file which refers to datasets in vtu files.
 so we end up with multiple files
 the pvd format doesnt accept references to legacy (.vtk) files
*/


int RXSail::WriteVtkSurface(const QString fnamein)
{
    int rc=0;
    char filename[512];
    filename_copy_local(filename,512,qPrintable (fnamein));
    ON_3dPoint p;
    ofstream dst;

    dst.open(filename,ios::out);
    if (dst.fail()) { cout<<" cant open file '"<<filename<<"'"<<endl; return rc; }
    rc=1;
    dst<<"# vtk DataFile Version 1.0"<<endl;
    dst<<"OFSI model:an Unstructured Grid of surface elements"<<endl;
    dst<<"ASCII"<<endl<<endl;

    dst<<"DATASET UNSTRUCTURED_GRID"<<endl<<endl;
    dst<<"POINTS "<< this->m_Fnodes.size() <<" float"<<endl; // trobuel is - we dont want the six-nodes(rotational dofs)

    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            dst<< "0.  0.  0." <<endl;
        else  {
            p = s->DeflectedPos();
            dst <<p.x <<" " <<p.y <<" " <<p.z <<endl;
        }
    }
    // count active tris
    int c=0;
    for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
        RX_FETri3* t = *it;
        if(t) c++;
    }
    if(c>0)
    {
        dst <<endl <<"CELLS "<<c <<" "<< 4*c <<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            dst << 3 <<" "<< t->Node(0)<<" " << t->Node(1)<<" " << t->Node(2)  <<endl;
        }
        dst <<endl <<"CELL_TYPES "<<c <<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            dst << 5  <<endl;
        }
        dst <<endl <<"CELL_DATA "<<c <<endl;
        dst <<endl <<"SCALARS pressure float"<<endl<< "LOOKUP_TABLE default"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            dst << t->GetPressure() <<endl;
        }
        TriStressStruct tss;
        dst <<endl <<"SCALARS Upper_Principle_Stiffness float"<<endl<< "LOOKUP_TABLE default"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            dst << tss.m_Princ_Stiff  <<endl;
        }

        dst <<endl <<"SCALARS Upr_Principle_Stress float"<<endl<< "LOOKUP_TABLE default"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            dst << tss.m_princ[0]  <<endl;
        }
        dst <<endl <<"SCALARS Lwr_Principle_Stress float"<<endl<< "LOOKUP_TABLE default"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            dst << tss.m_princ[1]  <<endl;
        }
        dst <<endl <<"SCALARS crease float"<<endl<< "LOOKUP_TABLE default"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            dst << tss.m_cre  <<endl;
        }
        dst <<endl <<"SCALARS Upstrain float"<<endl<< "LOOKUP_TABLE default"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            double ex = tss.m_eps[0];
            double ey = tss.m_eps[1];
            double exy = tss.m_eps[2];
            double  vp =  0.5*((ex+ey) + sqrt((ex-ey)*(ex-ey) + exy*exy));
            dst << vp  <<endl;
        }

        dst <<endl <<"TENSORS Stress float"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->WriteStressTensor(dst);
        }
        dst <<endl <<"TENSORS Strain float"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->WriteStrainTensor(dst);
        }
        dst <<endl <<"TENSORS Elasticity float"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->WriteStiffnessTensor(dst);
        }
        dst <<endl <<"VECTORS LocalAxes float"<<endl;
        for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->WriteLocalAxis(dst);
        }
    }



    /*
thinkgs like displacement and sangle will be in 
POINT_DATA 27
SCALARS scalars float
LOOKUP_TABLE default
0.0
1.0
2.0
3.0
4.0
5.0		
*/
    dst <<endl <<"POINT_DATA "<<m_Fnodes.size() <<endl;
    dst <<endl <<"SCALARS S_Angle float"<<endl<< "LOOKUP_TABLE default"<<endl;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            dst<< "0. " <<endl;
        else  {
            float s_angle; int ierr;
            cf_get_svalue(SailListIndex (), s->GetN(),&(s_angle),&ierr);
            dst <<s_angle<<endl;
        }
    }
    dst <<endl <<"SCALARS G_Angle float"<<endl<< "LOOKUP_TABLE default"<<endl;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            dst<< "0. " <<endl;
        else  {
            float s_angle; int ierr;
            cf_get_gvalue(SailListIndex (), s->GetN(),&(s_angle),&ierr);
            dst <<s_angle<<endl;
        }
    }
    dst <<endl <<"VECTORS disp float"<<endl;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            dst<< "0. 0.  0." <<endl;
        else  {
            ON_3dVector dm = s->Deflection(); // in model space
            ON_3dVector  dw = this->VToGlobal(dm);
            dst <<dw.x<<" "<<dw.y<<" "<<dw.z<<endl;
        }
    }
    dst <<endl;
    // cout<<"written file :"<< filename<<endl;
    dst.close();

    return rc;
}

// now the strings in another file
/*
# vtk DataFile Version 2.0
polyline eample
ASCII
DATASET UNSTRUCTURED_GRID
POINTS 6 float
0 0 0
3 1 0
6 -1 0
9 1 0
12 0 2
15 1 -2

CELLS 2  10
6 0 1 2 3 4 5
2 5 1

CELL_TYPES 2
4
4

CELL_DATA 2

SCALARS tension float
LOOKUP_TABLE default
-101.0
101.0

OR
<VTKFile type="UnstructuredGrid" version="0.1" byte_order="LittleEndian">
  <UnstructuredGrid>
    <Piece NumberOfPoints="6" NumberOfCells="2">
      <PointData>
      </PointData>
      <CellData Scalars="animal">
        <DataArray type="Float32" Name="animal" format="ascii" RangeMin="-101" RangeMax="101">
          -101 101
        </DataArray>
      </CellData>
      <Points>
        <DataArray type="Float32" Name="Points" NumberOfComponents="3" format="ascii" RangeMin="0" RangeMax="15.165750888">
          0 0 0 3 1 0
          6 -1 0 9 1 0
          12 0 2 15 1 -2
        </DataArray>
      </Points>
      <Cells>
        <DataArray type="Int64" Name="connectivity" format="ascii" RangeMin="0" RangeMax="5">
          0 1 2 3 4 5
          5 1
        </DataArray>
        <DataArray type="Int64" Name="offsets" format="ascii" RangeMin="6" RangeMax="8">
          6 8
        </DataArray>
        <DataArray type="UInt8" Name="types" format="ascii" RangeMin="3" RangeMax="3">
          3 3
        </DataArray>
      </Cells>
    </Piece>
  </UnstructuredGrid>
</VTKFile>

*/
int RXSail::EvaluateAllReadOnly() {
    int rc=0;
    RXEntity_p p ;
    vector<RXEntity* > stringlist;
    vector<RXEntity* >::iterator it;


    MapExtractList(PCE_STRING,  stringlist,PCE_ISRESOLVED) ;
    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
        p  = *it;
        rc+=p->EvaluateAllReadOnly();

    } // for it
    return rc;
}

int RXSail::WriteVtkStrings (const QString fnamein)
{
    // we have strings, battens, flinks, fbeams
    int rc=0;
    char filename[512];
    filename_copy_local(filename,512,qPrintable (fnamein));
    ON_3dPoint p;
    class RX_FELinearObject*st;
    class RX_FEString* s;
    class RX_FEBeam*  b;
    vector<RXEntity* > stringlist, battenlist,  beamlist,pcfiles;//flinklist,
    vector<RXEntity* >::iterator it;
    int  ncells=0, nints=0,npts=0;
    MapExtractList(PCE_STRING,  stringlist,PCE_ISRESOLVED) ;
    MapExtractList(POCKET,  battenlist,PCE_ISRESOLVED) ;
    //    MapExtractList(PCE_STRING,  flinklist,PCE_ISRESOLVED) ; flinks dont have entities.
    MapExtractList(PCE_BEAMSTRING,  beamlist,PCE_ISRESOLVED) ;
    MapExtractList(PCE_PCFILE ,  pcfiles,PCE_ISRESOLVED) ;
    if(!(stringlist.size()+ beamlist.size() +pcfiles.size()))// +  battenlist.size()+ flinklist.size()  ) )
        return 0;
    // for flinks we can use the pcfiles' list and extern "C" double cf_getflinktension(const int sli, const int n);
    int sli = this->SailListIndex();
    ofstream dst;

    dst.open(filename,ios::out);
    if (dst.fail()) { cout<<" cant open file '"<<filename<<"'"<<endl; return rc; }
    rc=1;
    dst<<"# vtk DataFile Version 1.0"<<endl;
    dst<<"OFSI point and line data"<<endl;
    dst<<"ASCII"<<endl<<endl;
    dst<<"DATASET UNSTRUCTURED_GRID"<<endl<<endl;

    // count up to the first rotational node
    npts=0;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(s)
            if(cf_issixnode(sli,  s->GetN()) <00 )
                break;
        npts++;
    }
    dst<<"POINTS "<< npts <<" float"<<endl;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            dst<< "0.  0.  0." <<endl;
        else  {
            if(cf_issixnode(sli,  s->GetN()) <00 )
                break;
            p = s->DeflectedPos();
            dst <<p.x <<" " <<p.y <<" " <<p.z <<endl;
        }
    }
    // count active strings
    stringlist.insert(stringlist.begin(), beamlist.begin(),beamlist.end()); // beams first

    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
        RXEntity_p p  = *it;
        st = (RX_FELinearObject*)  dynamic_cast<class RX_FEString*>(p);
        if(!st)
            st = (RX_FELinearObject*)  dynamic_cast<class RX_FEBeam*>(p);
        std::vector<int>nnn = st->GetNodeList();
        if(!nnn.size())
            continue;
        ncells++;   nints+=(1+nnn.size());
    } // for it

    dst <<endl <<"CELLS "<< ncells<< " "<<nints <<endl;
    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
        RXEntity_p p  = *it;
        st = (RX_FELinearObject*)  dynamic_cast<class RX_FEString*>(p);
        if(!st)
            st = (RX_FELinearObject*)  dynamic_cast<class RX_FEBeam*>(p);
        std::vector<int>nnn = st->GetNodeList();
        if(!nnn.size())
            continue;
        dst<<  nnn.size() << " ";
        for (std::vector<int>::iterator ii= nnn.begin();ii!=nnn.end();++ii)
            dst<< *ii << " ";
        dst<<endl;
    } // for it
    dst <<endl <<"CELL_TYPES "<< ncells<<endl;
    for(int k=0;k<ncells;k++)
        dst<< 4 <<endl; // 4 is VTK_POLYLINE

    dst <<endl <<"CELL_DATA "<< ncells<<endl;

    dst <<endl <<"SCALARS RXeltype float " <<endl<<"LOOKUP_TABLE default" <<endl;
    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
        RXEntity_p p  = *it;
        st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
        if(!st)
            continue;
        std::vector<int>nnn = st->GetNodeList();
        if(!nnn.size())
            continue;
        b =  dynamic_cast<class RX_FEBeam*>(p);
        if(b) {
            dst<<2<< endl;
            continue;
        }
        s=dynamic_cast<class RX_FEString*>(p);
        if(s) {
            dst<<1<< endl;
            continue;
        }

        dst<<-1<< endl;
    } // for it
    /* beam results are (see tanpure.f90)
    m13	=>  force(1)
    m23	=> 	force(2)
    m12	=> 	force(3)
    m22	=> 	force(4)
    mt	=> 	force(5)
    p	=> 	force(6)
    sF12=> 	force(7)
    SF22=> 	force(8)
    sF13=> 	force(9)
    SF23=> 	force(10)  */
    //  tension
    dst <<endl <<"SCALARS tension float " <<endl<<"LOOKUP_TABLE default"<<endl;
    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
        RXEntity_p p  = *it;
        st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
        if(!st)
            continue;
        std::vector<int>nnn = st->GetNodeList();
        if(!nnn.size())
            continue;
        b =  dynamic_cast<class RX_FEBeam*>(p);
        if(b) {
            double f[10];
            std::vector<int>  ee = b-> Elements() ;

            std::vector<int>::const_iterator ii;
            for(ii=ee.begin(); ii !=ee.end(); ++ii) {
                if( cf_one_beamelementresult(sli, *ii, f)  )
                    dst<<f[5]<<endl;
                else
                    dst<<0.00<< endl;
                if(ee.size()>1) cout<< "In paraview you only see the first beam of a group"<<endl;
                break; // only the first one
            }
            continue;
        }
        s=dynamic_cast<class RX_FEString*>(p);
        if(s) {
            double t;int ns = s->GetN();
            if(cf_get_one_string_tension(&sli,&ns, &t))
                dst<<t<<endl;
            else
                dst<<0<< endl;
            continue;
        }
        dst<<0<< endl;
    } // for it
    ON_3dVector dm , dw ;

    dst <<endl <<"POINT_DATA "<<npts ;
    dst <<endl <<"VECTORS disp float"<<endl;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            dst<< "0.  0.0  0.00" <<endl;
        else  {
            if(cf_issixnode(sli,  s->GetN()) <00 )
                break;
            dm = s->Deflection(); // in model space
            dw = this->VToGlobal(dm);
            dst <<dw.x<<" "<<dw.y<<" "<<dw.z<<endl;
        }
    }


    //   cout<<"written VTK file :"<< filename<<endl;
    dst.close();


    return rc;
}

class rxON_Mesh * RXSail::MakeONMeshDeflected( )
{
    rxON_Mesh *ro = new rxON_Mesh();

    ON_3dPoint p;

    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
            continue;
        p = s->DeflectedPos(RX_GLOBAL);
        ro->SetVertex(s->GetN(),p);

    }

    // the active tris

    int tc=0;
    for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
        RX_FETri3* t = *it;
        if(!t)
            continue;
        ro->SetTriangle(tc++,t->Node(0),t->Node(1),t->Node(2));
    }
    if(ro->FaceCount()==0)
    {
        delete ro;
        ro=0;
    }
    return ro;
}

int RXSail::Write_3dm(const char * filenamein)
{
    //Write everything in the 3DM FILE
    char filename[512];
    filename_copy_local(filename,512,filenamein);
    QFileInfo fi( filename );
    QString qFileName=fi.dir().absoluteFilePath(fi.completeBaseName()) + ".3dm";
    strcpy(filename, qPrintable(qFileName));

#ifndef ON_V3
    int version=4;// version to write
#else
    int version=3;// version to write
#endif
    ON_String AttsBuffer;
    ON::Begin();
    FILE* fp = ON::OpenFile( filename, "wb" );
    rxONX_Model l_modelToWrite;	PC_3dm_Initialize(l_modelToWrite);  //Model where we want to write to

    // errors printed to stdout
    ON_TextLog error_log;

    // if the sail has a m_p3dmModel, copy it in the modelToWrite taking its settings and its layertable, and its objects

    PC_3DM_Model * l_3dmModel = NULL;
    rxONX_Model* 	   l_ONXModel = NULL;
    if (/*  (PC_3DM_Model*) */(m_p3dmModel))
        l_3dmModel = /*(PC_3DM_Model*) */(m_p3dmModel);
    if (l_3dmModel)
        l_ONXModel = l_3dmModel->m_pONXModel;

    if(l_ONXModel) {
        //We only need the model to set the layers the settings and all these things in the new 3DM file

        l_modelToWrite.m_settings    = l_ONXModel->m_settings;
        l_modelToWrite.m_layer_table = l_ONXModel->m_layer_table;

        //To print all the ON_Objects
        for (int i=0;i<l_ONXModel->m_object_table.Count();i++)  //l_Nitem;
        {
            ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
            mo = l_ONXModel->m_object_table[i]; //Peter try

            if (ON_Surface::Cast(l_ONXModel->m_object_table[i].m_object))
                mo.m_object = ((ON_Surface*)l_ONXModel->m_object_table[i].m_object)->NurbsSurface();
            else
                mo.m_object = l_ONXModel->m_object_table[i].m_object;

            int layer = l_ONXModel->m_object_table[i].m_attributes.m_layer_index;
            mo.m_bDeleteObject = false;
            mo.m_attributes = l_ONXModel->m_object_table[i].m_attributes;
            //			mo.m_attributes.m_layer_index = layer;  //GetONXObjLayer(p_Model,p_e);
            mo.m_attributes.m_name = ON_String("ONOut_")+ l_ONXModel->m_object_table[i].m_attributes.m_name; //; //ON_String(p_e->name) + ON_String("\t") + ON_String(p_e->line));
        } // for i
    } // if l_ONX_Model

    l_modelToWrite.m_settings.m_ModelUnitsAndTolerances.m_unit_system=RLX_DEFAULT_UNITS ;
    // do we have to check for the existence of layers?
    // layer table
    {
        // OPTIONAL - define some layers
        ON_Layer layer[3];

        layer[0].SetLayerName("MyDefault");
#ifdef ON_V3
        layer[0].SetMode(ON::normal_layer);
#endif
        //  layer[0].SetLayerIndex(0);
        layer[0].SetColor( ON_Color(0,0,0) );

        layer[1].SetLayerName("Mygauss_nodes");
#ifdef ON_V3
        layer[1].SetMode(ON::normal_layer);
#endif
        //  layer[1].SetLayerIndex(1);
        layer[1].SetColor( ON_Color(255,0,0) );

        layer[2].SetLayerName("SC_Plines");
#ifdef ON_V3
        layer[2].SetMode(ON::normal_layer);
#endif
        //  layer[2].SetLayerIndex(2);
        layer[2].SetColor( ON_Color(0,0,255) );

        l_modelToWrite.m_layer_table.Append(layer[0]);
        l_modelToWrite.m_layer_table.Append(layer[1]);
        l_modelToWrite.m_layer_table.Append(layer[2]);
    }

    // (2) To write all the entities which have no ON_Object into the notes
    class RX_FELinearObject *rxflo ;
    ON_String l_Notes;
    l_Notes.Empty();
    entiter_t it;
    for (it = m_emap.begin(); it != m_emap.end();++it){  // Write_3dm
        RXEntity* l_e  = it->second; //For each ENTITY in the sail WRITE ENTITY TO 3DM FILE
        int l_type = l_e->TYPE;

        switch(l_type) {
        case INTERPOLATION_SURFACE: // write the nurbs surface.

          //  rxerror(" cant write ISurf",3);

        case DXF : // skip all of these.
        case PCE_TPN :
        case PCE_IGES :
        case PCE_3DM:
        case PCE_PCFILE:
            break;

        case SEAMCURVE:{
            l_e->ReWriteLine();
            sc_ptr sc = ( sc_ptr ) l_e;
            if(l_e->Needs_Resolving)
                break;
            if(l_e->Needs_Finishing)
                break;
            rxON_PolylineCurve l_PLC;

            QString qatt = l_e->m_rxa.ToQString(1); // 1 means prettify
            AttsBuffer=ON_String(qPrintable(qatt));

            if (!sc->m_pC[1]->GetONPolyLineCurveFromRelaxPL(l_PLC)==RXCURVE_OK){
                QString buf(" (3dmOut)error getting Black ON_PolylineCurve  on SC " ); buf+= sc->name();
                sc->OutputToClient(buf,0);
            }
            else {
                ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
                mo.m_object = new ON_PolylineCurve (l_PLC);
                mo.m_bDeleteObject = false;  //??????????????
                mo.m_attributes.m_layer_index =  max(0,GetONXObjLayerByName(&l_modelToWrite,ON_String("SC_Plines")));

                mo.m_attributes.m_name = ON_String("SCOut_")+ON_String(l_e->name()) + ON_String( ",") +AttsBuffer;
                if ( ON_nil_uuid == mo.m_attributes.m_uuid )
                    ON_CreateUuid(mo.m_attributes.m_uuid);
            }
            if(sc->seam )
            {
                for(int kk=0; kk<3; kk+=2) {
                    if (!sc->m_pC[kk]->GetONPolyLineCurveFromRelaxPL(l_PLC)==RXCURVE_OK){
                        QString buf =QString(" (3dmOut)error getting Red (%1) ON_PolylineCurve  on SC " ).arg(kk);
                        buf+= sc->name();
                        sc->OutputToClient(buf,1);
                    }
                    else {
                        ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
                        mo.m_object = new ON_PolylineCurve (l_PLC);
                        mo.m_bDeleteObject = false;  //??????????????
                        mo.m_attributes.m_layer_index =  max(0,GetONXObjLayerByName(&l_modelToWrite,ON_String("SC_Red")));
                        mo.m_attributes.m_name = ON_String("Red_")+ON_String(l_e->name()) + ON_String( ",") +AttsBuffer;
                        if ( ON_nil_uuid == mo.m_attributes.m_uuid )
                            ON_CreateUuid(mo.m_attributes.m_uuid);
                    }

                }

            }

            // now the basecurve
            RXEntity_p bc = sc->BorGcurveptr;  // bc is null for tpn designs
            if(bc && bc->m_pONMO) {
                ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
                mo.m_object = bc->m_pONMO->m_object;
                mo.m_bDeleteObject = false;  //??????????????
                mo.m_attributes.m_layer_index = GetONXObjLayer(&l_modelToWrite,bc);
                mo.m_attributes.m_name = ON_String("BCOut_")+ON_String(bc->name()) + ON_String( ",") +AttsBuffer;
                if ( ON_nil_uuid == mo.m_attributes.m_uuid )
                    ON_CreateUuid(mo.m_attributes.m_uuid);
            }


            break;
        }
        case SITE:
        case RELSITE:  // write as a point then break;
        {
            l_e->ReWriteLine();
            Site * p = (Site *)l_e ;
            if(!p)
                break;
            ON_Point  point1 =  ON_Point(ON_3dPoint( p->x,p->y,p->z ));
            point1 =  ON_Point(p->DeflectedPos() );

            ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
            mo.m_object = new ON_Point (point1);
            mo.m_bDeleteObject = false;  //??????????????
            mo.m_attributes.m_layer_index = GetONXObjLayer(&l_modelToWrite,l_e);;
            mo.m_attributes.m_name = ON_String("SiteDefl_")+ON_String(l_e->name());// + ON_String( ",") +ON_String( l_e->attributes );

            QString qatt = l_e->m_rxa.ToQString(1); // 1 means prettify
            AttsBuffer=ON_String(qPrintable(qatt));
            mo.m_attributes.m_name += ON_String(",") + AttsBuffer;
            if ( ON_nil_uuid == mo.m_attributes.m_uuid )
                ON_CreateUuid(mo.m_attributes.m_uuid);
        }
            break;
        case POCKET:
        case PCE_STRING:
            l_e->ReWriteLine();
            rxflo = dynamic_cast<class RX_FELinearObject *>(l_e) ;
            if(rxflo)
            {
                class ON_3dPointArray x;
                vector<int>::iterator it;
                vector<int> theNodes = rxflo->GetNodeList();
                for(it=theNodes.begin();it!= theNodes.end();++it)
                {
                    class RX_FESite *ss = this->GetSiteByNumber( *it);
                    if(ss)
                        x.AppendNew()=ss->DeflectedPos();
                    else
                        cout<<" WHY isnt it a FESite?? "<< (*it)<<endl;
                }
                if(x.Count()>1){
                    ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
                    mo.m_object = new ON_PolylineCurve(x);
                    mo.m_bDeleteObject = true;  //??????????????
                    mo.m_attributes.m_layer_index = GetONXObjLayer(&l_modelToWrite,l_e);;
                    mo.m_attributes.m_name = ON_String("Defl_")+ON_String(l_e->name());// + ON_String( ",") +ON_String( l_e->attributes );

                    if ( ON_nil_uuid == mo.m_attributes.m_uuid )
                        ON_CreateUuid(mo.m_attributes.m_uuid);

                    QString qatt = l_e->m_rxa.ToQString(1); // 1 means prettify
                    AttsBuffer=ON_String(qPrintable(qatt));
                    mo.m_attributes.m_name += ON_String(",") + AttsBuffer;
                }
                else
                    cout<<" BIZARRE!!  short linearObject "<< l_e->name()<<endl;
            }
            else
                cout<<"BIZARRE!! why doesnt it cast to FeLO? "<<l_e->type()<<","<< l_e->name()<<endl;
            // dont break
        default:
            if (l_e->generated )
                continue;
            if(!rxIsEmpty(l_e->GetLine()))
                l_Notes += ON_String(l_e->GetLine()) + ON_String("\n");
        }// switch
    } // for i
    // write the deflected mesh
    class rxON_Mesh *ro = this->MakeONMeshDeflected( );
    if(ro)
    {
        ONX_Model_Object& mo = l_modelToWrite.m_object_table.AppendNew();
        mo.m_object =ro ;
        mo.m_bDeleteObject = true;  //??????????????
        mo.m_attributes.m_layer_index =  0;
        mo.m_attributes.m_name = ON_String("deflected_"); + ON_String(this->GetType().c_str());
    }

    //Write the note
    l_modelToWrite.m_properties.m_Notes.m_notes = l_Notes;

#ifdef HOOPS
    HC_Open_Segment_By_Key(this->GetGraphic()->m_ModelSeg);
    hoopsToON::SegmentTreeToON("(.,*,...)",l_modelToWrite );
    HC_Close_Segment();
#endif
    ON_BinaryFile archive( ON::write3dm, fp ); // fp = pointer from fopoen(...,"wb")
    // start section comment
    const char* sStartSectionComment = __FILE__ "3DMRelaxOut" __DATE__;

    // Set uuid's, indices, etc.
    l_modelToWrite.Polish();
    // writes model to archive
    ON_TextLog text_log;
    // FAILS in MSVC
    ON_TextLog * ptext_log=0; //&text_log
    if (l_modelToWrite.IsValid( ptext_log))
        l_modelToWrite.Write( archive, version, sStartSectionComment, &error_log );
    else
        rxerror("cant write the model: textlog isnt valid",2);
    ON::CloseFile( fp );

    ON::End();

    return 1;
}//Write_3dm

int RXSail::FreezeStrings(const int flags)
{
    RX_FEString *s;
    int rc=0;
    entiter_t it;
    for (it = m_emap.begin(); it != m_emap.end();++it){  // FreezeStrings
        s=   dynamic_cast <RX_FEString *> (it->second ) ;
        if(!s)
            continue;
        rc+=s->Freeze(flags);
    }
    return rc;
}

int RXSail::Write_Attributes(const QString &Filename) // really a debug check
{
    int rc=0;
    FILE *fp;
    if (( fp = RXFOPEN(qPrintable (Filename),"w")) == NULL)  {
        QString buf ="could not open <"+Filename+"> to write";
        rxerror(buf,2);
        return(0);
    }
    entiter_t it;
    for (it = m_emap.begin(); it != m_emap.end();++it){  // Write_Attributes
        RXEntity* e  = it->second;
        //     if(e->generated)  continue;
        e->ReWriteLine();
        rc+=fprintf(fp,"%s \t%s \t%s\n",e->type(), e->name(), qPrintable( e->AttributeString() ) );
    }
    FCLOSE(fp);
    return rc;
}

int RXSail::Write_Script(const char *File,const int which) {
    // Which=0 means gen file  else a normal script
    FILE *cycleptr;
    if (( cycleptr = RXFOPEN(File,"w")) == NULL)  {
        RXSTRING buf =L"could not open <"+TOSTRING(File)+L"> to write";
        rxerror(buf,2);
        return(1);
    }

    rxON_String temp(this->Serialize());
    temp.Replace_String(":","\t");
    fprintf(cycleptr,"%s\n",temp.Array());
    this->WriteToScript(cycleptr, L"?sail",this);

    if(which) {	// script only
        entiter_t it;
        for (it = m_emap.begin(); it != m_emap.end();++it){// this is Write_Script
            RXEntity* ee  = it->second;
            RXEntity_p e = dynamic_cast<RXEntity_p>(ee);
            if(stristr("deleted,sail,name,pocket,patch,fixity",e->type()))// fixity may 2013
                continue;
            if(strieq("input",e->type()) || strieq("infile",e->type()) ) {
                printf(" SKIP write of %s %s, an input\n", e->type(),e->name());
                continue;
            }

            // we skip any includes which don't have any children.
            //  this only happens because editing a child unsets its 'generated' flag.
            // if ALL dependents have level 0 skip the write

            if(strieq("include",e->type()) && !e->OMapN() ) {
                printf(" SKIP write of %s %s, an INCLUDE. No deps\n", e->type(),e->name());
                continue;
            }


            if(strieq("include",e->type()) && e->OMapN() ) {
                int flag = 0;
                set<RXObject*> ss = e->FindInMap(RXO_ANYTYPE,RXO_ANYINDEX,spawn);
                set<RXObject*>::iterator its;
                for(its=ss.begin ();its!=ss.end(); ++its){
                    RXEntity_p e2 = dynamic_cast<RXEntity_p> (*its);
                    if(e2) {
                        if(e2->edited) {// move to the script
                            e2->generated=0;
                        }
                        if(e2->generated)
                            flag ++;
                    }
                }
                if(!flag) {
                    printf(" SKIP write of %s %s, an INCLUDE. No zero-Depth deps\n", e->type(),e->name());
                    continue;
                }
            }
            if(!e->generated) {
                e->edited=0;
                write_one_line(e,cycleptr);
            }
            else {
                if(e->edited) {// move to the script
                    write_one_line(e,cycleptr);
                    e->edited=e->generated = 0;
                }
            }
        }
        for (it = m_emap.begin(); it != m_emap.end();++it){ // this is Write_Script
            RXEntity_p e =dynamic_cast<RXEntity_p>(it->second );
            if(!e->generated && stristr("pocket,patch,batpatch",e->type())) {
                write_one_line(e,cycleptr);
            }
            e->edited=0;
        }
    }
    else { /* .gen file the complete set of entities EXCEPT relsites made by getkey  */
        entiter_t it;
        for (it = m_emap.begin(); it != m_emap.end();++it){ // this is Write_Script
            RXEntity_p e =dynamic_cast<RXEntity_p>(it->second );  /// this will cras
            if(stristr("panel,pside,deleted,layer,zone",e->type()))
                continue;
            if(strieq("include",e->type())) {
                fprintf(cycleptr," ! %s\n", e->GetLine());
                continue;
            }
            if(stristr(e->GetLine(),"made by getkey")) {
                fprintf(cycleptr," ! %s\n", e->GetLine());
                continue;
            }
            if(stristr(e->GetLine(),"made by curve connect")) {
                fprintf(cycleptr," ! %s\n", e->GetLine());
                continue;
            }
            if(stristr(e->GetLine(),"made by batpatch")) {
                fprintf(cycleptr," ! %s\n", e->GetLine());
                continue;
            }
            if(stristr(e->GetLine(),"__tpn__")) {
                fprintf(cycleptr," ! %s\n", e->GetLine());
                continue;
            }
            if(strieq(e->type(),"fabric")) {
                // if(!e->Needs_ Computing)  WRONG. We need a better flag
                if(!e->OMapN()  && !e->NeedsComputing() )
                    continue;
            }
            write_one_line(e,cycleptr);
        }
    }
    fprintf(cycleptr,"EOF:\n");
    FCLOSE (cycleptr);

    ClearFlag(FL_NEEDS_SAVING); // that's write_script
    return(0);
}
#ifdef NEVER
bool RXSail::InsertFedge(RX_FEedge* e){
    /*
 m_Fedges [nn] and the edgelist may  either
  (1) * not know about any edge with this number.
  (2) * already know about this instance
  (3) * know about another instance with this number.

If  (1) we{
   We set the sails' m_Fedges [nn] to point to it, increasing capacity as necessary
   In Fortran,  the placeholder  should be vacant. Assert for this;
   we create an Fedge which points to our instance, and we fill it in.
   We set our instance's 'hasbeenloaded flag.
}
If  (2) we {
   do nothing.
}
If  (3) we {
   delete the existing edge with this number.
   then (1)
}
*/
    int n = e->GetN();
    assert(e);  assert(e->GetSail() ==this);
    if(e->IsInDatabase()) {// case (2)
        return true;
    }

    RX_FEedge* squatter=0;
    if( this->m_Fedges.size()> (n+1))
        squatter = m_Fedges[n];
    if(squatter)	{		// case (3)
        assert(squatter !=e);
        RemoveFedge(squatter);
    }
    // case (1)
#ifdef FORTRANLINKED
    assert( e->m_node[0] );
    assert( e->m_node[1] );
    int l_n = cf_insert_edge(SailListIndex(),n,this,(e->GetNode(0)->GetN() ),(e->GetNode(1)->GetN()),e->GetLength() );

#else
    assert(0);
#endif 
    while(m_Fedges.capacity()<=(e->GetN()+1))
    {
        m_Fedges.reserve (m_Fedges.capacity()+EDGEBLOCK);
        //ON_String buf; buf.Format(" fedges capacity = %d size=%d maxsize = %d\n", m_Fedges.size (),m_Fedges.capacity(),m_Fedges.max_size());
        //rxerror(buf,1);
    }
    if(m_Fedges.size()<=(e->GetN()+1))
        m_Fedges.resize (e->GetN()+1);
    this->m_Fedges[e->GetN()]=e;
    e->SetIsInDatabase(true);
    return true;

}
bool RXSail::RemoveFedge(RX_FEedge* e){
    /*
  1) remove it from the sails list
  2) remove it from the sail's fortran model edgelist
*/
    assert(e);  assert(e->GetSail() ==this);
#ifdef FORTRANLINKED
    int l_n = cf_remove_edge(SailListIndex(),e->GetN());

#else
    assert(0);
#endif
    if(e == m_Fedges.back())
        m_Fedges.pop_back();
    else
        m_Fedges[e->GetN()]=0;
    e->SetIsInDatabase(true);
    return false;
}

#endif


int RXSail::Mesh_All_PSides() {
    int rc=0;
    PSIDEPTR  p;
    vector<RXEntity* >thelist;
    MapExtractList(PCE_PSIDE,  thelist) ;
    vector<RXEntity* >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        p =dynamic_cast<PSIDEPTR >(*it );
        rc+=p->Mesh();
    }
    return rc;
} 
int RXSail::Mesh_All_Panels()
{
    int rc=0;
    /*
 BEFORE we do this, do a SetSnode on all psides, without the ALLOC
 For each panel {
  find out if redspace and blackspace are different (rbDiff)
  extract red-space boundary.
  construct a ShewchukTriangle input object
  constrained Delaunay.
  (dbg) Draw in redspace
  if(rbDiff)  map internal pts to blackspace. (dbg) draw in blackspace
  Create RXSiteRelSites from the internal points . Get their UV coordinates
  Create the FEEdges and FETriangles.  Set materials on the triangles (in the right space)
 }
 for timing purposes it would be good to separate out the xyz->uv stage and the build materials stage.
*/
    int count;     Panelptr  pp;
    vector<RXEntity* >thelist;
    MapExtractList(PANEL,  thelist) ;
    MapExtractList(PCE_PSHELL,  thelist) ;
    float PropHowFar;
    vector<RXEntity* >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        pp =dynamic_cast<Panelptr >(*it);
        if(!pp->Needs_ReGridding)
            continue;
        QString buf = QString("Meshing " ) + QString(pp->name()) + QString(" ")+pp->AttributeString();
        setStatusBar(buf,this->Client() );

        rc+= pp->Mesh();
    }
    SetMsgText("building materials..",this->Client());

    bool nthr_checked=false;// int  nthr=1;
    count = (int) thelist.size();
    if (!nthr_checked) {
#ifdef _OPENMP
        nthr = omp_get_num_threads();
        printf( "\nbuild materials with %d thread(s)\n", nthr);
#endif
        nthr_checked = true;
    }
    // this will take some work to parallelize because every time we read an entity (material, layer, SC) we have to protect with critical section

#ifdef NEVER
#pragma omp parallel shared(thelist,nthr_checked  , nthr,count ) private(i,p,pp) default(none)
#endif
    {
#ifdef NEVER
#pragma omp for
#endif
        for (int i = 0; i< count; i++)  {
            pp =  (Panelptr) thelist[i];
            if(!pp->NeedsMaterials )
                continue;
            div_t  howfar = div(i ,max(count/20,1)   );		// progress report
            if(howfar.rem==0) {
                PropHowFar=  100.*float(i)/float (count);
                QString qbuf;
                qbuf= QString("Set Properties %1% on %2" ).arg(int (PropHowFar)).arg(this->GetType().c_str()) ;
                setStatusBar(qbuf,this->Client());
            }
            pp->BuildTriMaterials();
            pp->NeedsMaterials=0;
            rc++;
        }
    }// end parallel
    return rc;
}
int RXSail::Mesh_All_Strings(){//pockets too
    int rc=0;
    Set_MALLOC_DATA("Mesh_Strings"); //debugging only

    vector<RXEntity* >thelist;
    this->MapExtractList(PCE_STRING,  thelist) ;
    vector<RXEntity* >::iterator it;
    int count=1000;
    if(g_Janet) {
        printf("mesh String, count   %d\n",(int)thelist.size());
#ifndef FORTRANLINKED
        assert(0);
#else
        int ok= cf_reserveforjanet(this->SailListIndex(),(int)thelist.size()+1) ;
        // if(ok!=2) printf("cf_reserveforjanet  failed  %d\n",ok);
#endif
    }
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        class RX_FEString *st =dynamic_cast<class RX_FEString * >(*it); assert(st);

        if(!st->m_Needs_Meshing) continue;
        st->Mesh ();
        st->PostMesh(); // does the AddFEA etc. The string is now ready for analysis
        rc++;
        if(count>1000) { cout<<"meshed string "<< st->name()<<endl; count=0;}
        count++;
    } /* for it */
 //   if(count!=1000) cout<<"Meshing SStrings done :"<<thelist.size() <<endl;
    return rc;
} //RXSail::Mesh_All_Strings
int RXSail::Mesh_All_BeamStrings(){
    int rc=0;

    vector<RXEntity* >thelist;
    this->MapExtractList(PCE_BEAMSTRING,  thelist) ;
    vector<RXEntity* >::iterator it;
    int count=1000;

    for (it = thelist.begin (); it != thelist.end(); it++)  {
        class RX_FEBeam *st =dynamic_cast<class RX_FEBeam * >(*it); assert(st);

        if(!st->m_Needs_Meshing) continue;
        st->Mesh (); // use the string method.  Creates FE Edges which is wasteful
        st->PostMesh(); // overridden. does the AddFEA etc. The string is now ready for analysis
        rc++;
        if(count>1000) { cout<<" meshed Bstring"<< st->name()<<endl; count=0;}
        count++;
    } /* for it */
    if(count!=1000) cout<<"Meshing BStrings done :"<<thelist.size() <<endl;
    return rc;
} //RXSail::Mesh_All_BeamStrings
int RXSail::Mesh_All_RigidBodies(){
    int k, rc=0;

    vector<RXEntity* >thelist;
    this->MapExtractList(PCE_RIGIDBODY,  thelist) ;
    vector<RXEntity* >::iterator it;
    int count=1000;

    for (it = thelist.begin (); it != thelist.end(); it++)  {
        class RXRigidBody *st =dynamic_cast<class RXRigidBody * >(*it); assert(st);

        if(!st->m_Needs_Meshing) continue;
        k=st->Mesh ();
        if(k) {
            rc++;
            if(count>1000) { cout<<"meshed Rigid Body "<< st->name()<<endl; count=0;}
            count++;
        }
    } /* for it */
    if(count!=1000) cout<<"Meshing Rigid Bodies done :"<<thelist.size() <<endl;
    return rc;
} //RXSail::Mesh_All_Beamtrings


int RXSail::PostMesh() {
    // reset the FEEdge flags and set their m_initialCorrection
    // this isnt in the spirit of fine-grain model management because it
    //involves a trawl through the entire model. The right way is to do it on creating the tris

    RX_FESite *s1,*s2;
    for(vector<RX_FEedge*>::iterator it= this->m_Fedges.begin (); it!=this->m_Fedges.end();++it)
    {
        RX_FEedge*e = *it;
        if(!e)
            continue;
        e->m_Flags=RXFEE_NOSHELL; // 0 as it happens
        cf_set_edgeflag(this->SailListIndex (),e->GetN(),0,1.0);
    }
// find all the seamcurves which are edges.
    vector<RXEntity* > pslist;
    vector<RXEntity* >::iterator it;
    this->MapExtractList(PCE_PSIDE, pslist,PCE_ISRESOLVED) ;
    for (it = pslist.begin (); it != pslist.end(); it++)  {
        class RXPside* ps = (class RXPside*) (*it);
         sc_ptr sc = ps->sc;
         if(!sc->edge) continue;
         class RX_FEedge*ee;
         std::vector<class RX_FEedge*>::iterator ite;
         for(ite = ps->m_eList.begin();ite!=ps->m_eList.end(); ++ite){
             ee = *ite;
             ee->m_Flags|=RXFEE_ISMODELEDGE;
         }
    }



    for(vector< RX_FETri3*>::iterator it= this-> m_FTri3s.begin (); it!=this-> m_FTri3s.end();++it)
    {
        RX_FETri3*t = *it;
        if(!t)
            continue;
        for(int i=0; i<3;i++)
        {
            int ie = t->m_iEd[i];
            RX_FEedge*e =this->m_Fedges[ie];
            e->m_Flags|=RXFEE_ISEDGE;
        }
    }

    class RXON_NurbsSurface* mould = this->GetMouldSurface();
    if(mould)
        for(vector<RX_FEedge*>::iterator it= this->m_Fedges.begin (); it!=this->m_Fedges.end();++it)
        {
            ON_3dVector  n1,n2,ch;
            RX_FEedge*e = *it;
            if(!e)
                continue;
            if(!e->IsOnShell()  )
                continue;
            s1 = e->m_node[0]; s2 = e->m_node[1];
            n1 = mould->NormalAt(s1->m_u,s1->m_v);
            n2 = mould->NormalAt(s2->m_u,s2->m_v);
            ch = *s2 - *s1;
            ch.Unitize ();	n1.Unitize (); n2.Unitize ();
            double cc =  cf_curvecorrection(&(ch.x),&(n1.x),&(n2.x)); // assumes x,y,z are contiguous.
            e->m_initialCorrection= cc; //chord/arc
            cf_set_edgeflag(this->SailListIndex (),e->GetN(),1,cc);

        }
    //  the initial arc length = m_linit /  m_initialCorrection
    if(0){
        HC_Open_Segment("edges");
        HC_Flush_Segment(".");
        HC_Set_Line_Weight(3.0);
        for(vector<RX_FEedge*>::iterator it= this->m_Fedges.begin (); it!=this->m_Fedges.end();++it)
        {
            RX_FEedge*e = *it;
            if(!e)
                continue;
            if(!e->IsInDatabase())
                continue;

            if(e->IsOnModelEdge()){
                HC_Open_Segment("ShellEdge");
                HC_Set_Color("blue");
                //   e->DrawDeflected ();
                HC_Close_Segment();
            }
            else if(e->IsOnShell())  {
                HC_Open_Segment("InShell");
                HC_Set_Color("red");
                //  e->DrawDeflected ();
                HC_Close_Segment();
            }
            else {
                HC_Open_Segment("PlainEdge");
                HC_Set_Color("black");
                //  e->DrawDeflected ();
                HC_Close_Segment();
            }
        }
        HC_Close_Segment();
    }
    return	1;
}
// ACCURACY !   PostMesh interrogates the mould at UV values from the mapping
// which aren't in general the same.
// we should rather get the initial site normals from the UV values before meshing
// but that is slow because it means doing a Find-Nearest on each site..
int RXSail::Mesh()
{
    int rcp=0,rcs=0;
    string buf("Meshing..."); buf+= this->GetType();
    SetMsgText(buf.c_str(),this->Client());
    this->Tidy();
    HC_KEY segkey = this->GetGraphic()->m_ModelSeg;

    HC_Open_Segment_By_Key(segkey);
    HC_Open_Segment("data");
    SetMsgText("Meshing...edges",this->Client());                       rcp+=this->Mesh_All_PSides();// RC OK
    SetMsgText("Meshing...edges...panels...",this->Client());		    rcp+=this->Mesh_All_Panels();
    SetMsgText("Meshing...edges...panels...strings",this->Client());	rcs+=this->Mesh_All_Strings(); // does the pockets too
    SetMsgText("Meshing...edges...panels...beams",this->Client());	    rcs+=this->Mesh_All_BeamStrings();
    SetMsgText("Meshing...edges...panels...pockets",this->Client());	rcs+=RX_FEPocket::MeshAll(this);
    SetMsgText("Meshing...edges...panels...Rigid Bodies",this->Client());rcs+=this->Mesh_All_RigidBodies();
    SetMsgText("Meshing...edges...submodels",this->Client());
    // mesh all submodels
    vector<RXEntity* > smlist;
    vector<RXEntity* >::iterator it;
    this->MapExtractList(PCE_SUBMODEL, smlist,PCE_ISRESOLVED) ;
    for (it = smlist.begin (); it != smlist.end(); it++)  {
        class RXsubModel *sm = (class RXsubModel * ) (*it);
        sm->Mesh();
    }
    this->PostMesh();
    if(rcp) {
        SetMsgText( "Meshing...UV mapping ",this->Client());
        if( !this->FlagQuery(NO_UV_MAP )  )  //"$nouvmap"
            this->MakeUVMapping_XYPlane(); // the original
        //  this->MakeUVMappingFromFEMesh(); // doesnt generate a nw triangulation so it is independent of orientation
    }
#ifdef FORTRANLINKED
    cf_hookalltris();
    double total;
    cf_calc_s_angles(this->SailListIndex(),1,&total);
#else
    assert(0);
#endif			
    SetMsgText( "Meshing...Done",this->Client());

    HC_Close_Segment();
    HC_Close_Segment();
    this->SetFlag(FL_HASMESH,1);
    this->ClearFlag(FL_HASNEWTOPOLOGY );
    if(rcp+rcs)
        this->SetFlag(FL_NEEDS_RUNNING,1);
    return(rcp+rcs);

}



//  strictly this RXSail::MeshSize is incomplete, because in R2 the grid density is controlled at SITE and Curve level as well
// as globally.
// we'd also like there to be an adaptive 
// for R3 we havent decided if we keep the same or we provide an easy way of varying density 
// it will probably be via the accumulation of several functions in (uv). some user-defined and some from analysis
// error estimates. 
double RXSail::MeshSize(const RXSitePt&p) const
{ double rv;
    double u,v,x,y,z;

    RXQuantity *ee = this->MeshExpression();

    if(ee) {
        try{
            rv = ee->evaluate();
        }  // maybe it isnt a function of u and v
        catch( MTParserException &e ) { // maybe we need u and v
            u = p.m_u;v=p.m_v;x=p.x;y=p.y;z=p.z;
            try	{ee->redefineVar(_T("u"),&u);}  catch( MTParserException &e5 ) {} // its OK for these to be undefined
            try	{ee->redefineVar(_T("v"),&v);}  catch( MTParserException &e5 ) {}
            try	{ee->redefineVar(_T("x"),&x);}  catch( MTParserException &e5 ) {}
            try	{ee->redefineVar(_T("y"),&y);}  catch( MTParserException &e5 ) {}
            try	{ee->redefineVar(_T("z"),&z);}  catch( MTParserException &e5 ) {}
            rv = ee->evaluate();
        }
        if(rv < 0.00001) {

            const char *lp=" Small Grid Density .  Please check your script file ";
            rxerror(lp ,4);
            RXTHROW(lp);
        }

        return rv;
    }

    if(!ee) {
        //     ee = dynamic_cast<RXQuantity *> (FindExpression(L"grid",rxexpLocalAndTree));

        RXScalarProperty *sp = ( RXScalarProperty * ) this->Entity_Exists("meshsize","scalar");
        if(sp)
        {
            rv= sp->ValueAt(p);
            //      qDebug()<<"TODO: step this:  found gridsize expression"<<rv;
            return rv;

        }
    }
    if(!ee) {
        rxerror(" there doesnt seem to be a grid density defined - defaulting to 1.0",1);
        return 1.0;
    }
}

int RXSail::Needs_Saving(void ) const{
    int iret=0;
    if(!GetFlag(FL_NEEDS_SAVING)) {
        ent_citer it;
        for (it = m_emap.begin(); it != m_emap.end();++it){ // RXSail::Needs_Saving
            RXEntity_p p =dynamic_cast<RXEntity_p>(it->second);
            if(p->edited) {
                cout<<"(RXSail::Needs_Saving) because '"<<p->type()<<"', '"<< p->name() <<  "' was edited\n";
                return 1;
            }
        }
    } else
        iret = 1;
    return iret;
}
//static
int RXSail::GetFortranNoDeno(const char *pmodel,const char* pnname){

    int rv=0,sn=-1;
    char model[256]; char nname[256];

    memset(model,' ',256); memset(nname,' ',256);
    model[255]=0; nname[255]=0;
    strncpy(model,pmodel,255); PC_Strip_Leading(model);  PC_Strip_Trailing(model);
    strncpy(nname,pnname,255);PC_Strip_Leading(nname);PC_Strip_Trailing(nname);
    class RXSail *thesail=0;
#ifdef FORTRANLINKED

    sn= cf_findhoistedsailbyname(model,(int)strlen(model),&thesail);
#else
    assert(0);
#endif
    if(sn<=0) thesail=0;
    if(!thesail)
    { //cout<<"no sail"<<endl;
        return -1;}
    //cout <<"search in sail "<<thesail->GetType() <<" for node " <<nname ;
    RXEntity * ne =  thesail->GetKeyWithAlias("site,relsite",nname);
    if(!ne)
    { //cout << "no node named ("<<nname<<") in model ("<<pmodel<<")"<<endl;
        return -1;}
    RX_FESite *ss = dynamic_cast< RX_FESite *> ( ne); assert(ss);
    rv = ss->GetN(); // wcout <<"GetFortran NoDeno found node "<< ne->name<<endl;

    return rv;
}

HC_KEY 	RXSail::DrawPlotps(RXGRAPHICSEGMENT k) const
{
    HC_KEY rk=0;
    HC_Open_Segment_By_Key(k);
    rk=HC_KOpen_Segment("plotps"); HC_Flush_Contents(".","everything");
    cf_plotps(SailListIndex(),k);
    HC_Close_Segment();
    HC_Close_Segment();
    return (rk);
}
HC_KEY 	RXSail::DrawStrainVectors(RXGRAPHICSEGMENT k) const
{
    HC_KEY rk=0;
    HC_Open_Segment_By_Key(k);
    rk=HC_KOpen_Segment("pstrain"); HC_Flush_Contents(".","everything");
    cf_plotpstrain(SailListIndex(),k);
    HC_Close_Segment();
    HC_Close_Segment();
    return (rk);
}

HC_KEY RXSail::DrawFilaments( const HC_KEY p_k) const
{
    HC_KEY rk=0;
    HC_Open_Segment_By_Key(p_k);
    rk=HC_KOpen_Segment("fils"); HC_Flush_Contents(".","everything");
    PCF_Draw_Colored_Filaments(this);
    HC_Close_Segment();
    HC_Close_Segment();
    return (rk);
}
HC_KEY RXSail::DrawMatRefVector( RXGRAPHICSEGMENT k) const	{
    HC_KEY rk=0;
    HC_Open_Segment_By_Key(k);
    rk=HC_KOpen_Segment("matref"); HC_Flush_Contents(".","everything");
    cf_plot_ref_direction(this->SailListIndex (),k);
    HC_Close_Segment();
    HC_Close_Segment();
    return (rk);
}

HC_KEY 	RXSail::DrawPrinStiff(RXGRAPHICSEGMENT k ) const
{
    HC_KEY rk=0;
    HC_Open_Segment_By_Key(k);
    rk=HC_KOpen_Segment("PrinStiff"); HC_Flush_Contents(".","everything");
    cf_plot_principal_stiff(SailListIndex(),k);
    HC_Close_Segment();
    HC_Close_Segment();
    return (rk);
}

HC_KEY RXSail::DrawDesignWindow( ) //normally PostProcNonExclusive
{

#if defined(RXQT)
    RXGRAPHICSEGMENT seg;
    RXGRAPHICSEGMENT k = this->GNode();
    //    HC_Flush_Geometry(k);

    //    map<int,pair<double,double> > mymap; map<int,pair<double,double> >::iterator   mi;
    vector<RXEntity_p >thelist;
    if(g_Janet)
        return 0;
    MapExtractList(SEAMCURVE, thelist);

    for (vector<RXEntity_p >::iterator it = thelist.begin (); it != thelist.end(); it++)  {
        RXEntity_p e  =*it;
        sc_ptr sc = (sc_ptr ) e;
        class RX_SeamcurveHelper scd;

        scd.SeamCurve_Prelims(sc);

        //        scd.m_type = e->SeamCurve_Type();

        scd.DrawSeamcurve(); // Dra_Sc is slow, so lets try not to draw too often.
    }


    //   RXGRAPHICSEGMENT seg = RXNewGNode("links",k)  ;
    //   cf_drawlinks(SailListIndex() ,seg);

    //   seg = RXNewGNode("curves",k)  ;


    /*    seg = RXNewGNode("paneledges",k)  ;
    this->DrawPanelEdges(seg);

    seg = RXNewGNode("strings",k) */ ;


    //        k = this->GNode(); // as a test
    //        this->DrawPanelEdges(k);
    //        this->DrawStrings (k); // these gave trouble in  motif

    return 0;
#endif
}


HC_KEY RXSail::DrawDeflected( RXGRAPHICSEGMENT pK) //normally PostProcNonExclusive
{
#if defined(RXQT)
    RXGRAPHICSEGMENT k = pK;
    HC_Flush_Geometry(k);
    RXGRAPHICSEGMENT seg = RXNewGNode("links",k)  ;
    cf_drawlinks(SailListIndex() ,seg);

    seg = RXNewGNode("beams",k)  ;
    cf_drawfbeams(SailListIndex() ,seg);

    seg = RXNewGNode("tris",k)  ;
    this->DrawTriangles(0.95,seg);

    seg = RXNewGNode("paneledges",k)  ;
    this->DrawPanelEdges(seg);

    seg = RXNewGNode("strings",k)  ;
    this->DrawStrings (seg);
    if(0){
        k = this->GNode(); // as a test
        //   cf_drawlinks(SailListIndex() ,k);
        //  this->DrawTriangles(0.95,k);
        this->DrawPanelEdges(k);
        this->DrawStrings (k); // these gave trouble in  motif
    }
    return 0;
#endif
#if defined( WIN32) &&defined (HOOPS)
    HC_KEY rk=0;
    vector<RXEntity* >thelist;
    RX_FEString *st;
    HC_Open_Segment_By_Key(p_k);
    rk=HC_KOpen_Segment("deflected");

    HC_Flush_Contents(".","everything");


    if(1) { // testframe  for postprocessing
        double total =0;
        HC_Set_Color_Map("blue, sky blue,green, yellow, orange, red,magenta " );

        HC_Show_Net_Color_Map_Count (  &g_Mapcount );
        HC_Set_Rendering_Options("color interpolation=on,color index interpolation");
        cf_calc_s_angles(SailListIndex(),1,&total);

        int state =0; int what=7; int updateGeom=1; // 12 is sangles, 7=up stress 8 pressure
        //HC_KEY reskey =
        this->LoadSailResults(state, what, updateGeom);
        g_default_maxmin[what]=0;
        g_ValMax[what]=(float)0.02;
        g_ValMin[what]=(float)-0.02; updateGeom =0;
        //reskey =
        this->LoadSailResults( state, what,updateGeom);
    }
    else
        DrawTriangles(0.9);

    this->DrawPanelEdges();

    this->DrawStrings (); // these give trouble in  motif

    HC_Close_Segment();
    HC_Close_Segment();
    HC_Update_Display();
    return (rk);
#endif
}
int RXSail::DrawStrings(RXGRAPHICSEGMENT p_k) {
    int rc=0;
    double sd=1,variance=0, avg=1.0, sumx =0; double np=0;
    double t1=0,t2=1;
    double mint=0,maxt=0;
    bool Skip_Zero_Tension =(0!=g_Janet);
    class RX_FEString*st;
    vector<RXEntity* > thelist;
    vector<RXEntity* >::iterator it;

    MapExtractList(PCE_STRING,  thelist,PCE_ISRESOLVED) ;
    if(!thelist.size ()) return 0;
    // get tension range



    // this assumes that if the first needs meshing, they all do
    // and worse, if the first one has been meshed, they all have.

    st = (RX_FEString*) *( thelist.begin ());
    if(st->d.Ne>0)
        mint = maxt = 	st->m_FPtq[0];
    else
        mint = maxt = 	0.;

    for (it = thelist.begin (); it != thelist.end(); it++)  {
        st = (RX_FEString*) (*it);
        if(st->d.Ne<1) continue;
        sumx+=st->m_FPtq[0]; np+=1.;
        mint=min(mint,st->m_FPtq[0] ); 		maxt=max(maxt,st->m_FPtq[0] );
    }
    if(np>0) {
        avg = sumx/np;
        for (it = thelist.begin (); it != thelist.end(); it++)  {
            st = (RX_FEString*) (*it);
            if(st->d.Ne<1) continue;
            variance+=pow(st->m_FPtq[0]-avg,2);
        }
        sd=sqrt(variance/np);
        if(fabs(avg) < 0.00000001 && fabs(variance) < 0.0000001) Skip_Zero_Tension=false;
        t1=avg- g_nSDs*sd; t2=avg+g_nSDs*sd;
        t1=max(t1,mint); t2=min(t2,maxt);

        t2=max(t1+0.000000001,t2);
    }// if np

    HC_Open_Segment_By_Key(this->GetGraphic()->m_ModelSeg );
    HC_Open_Segment("data");
    // now draw
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        RXEntity_p p  = *it;
        assert (p->TYPE==PCE_STRING );

        st = (RX_FEString*) p;
        if(st->d.Ne<1) continue;
#ifdef HOOPS
        HC_Open_Segment(st->type());
        HC_Open_Segment(st->name());
        HC_Flush_Contents(".","everything");
#endif
        st->DrawDeflected(p_k,t1,t2,Skip_Zero_Tension);
#ifdef HOOPS
        HC_Close_Segment();
        HC_Close_Segment();
#endif

    } /* for i */
    HC_Close_Segment();
    HC_Close_Segment();
    return rc;
}


int RXSail::CheckAndDisplayPanels( ) { // see post-proc for managing gNodes

    RXGRAPHICSEGMENT seg = RXNewGNode("panels", this->GNode()  )  ;

    RXGRAPHICSEGMENT p_k;
    ent_citer it;
    for (it = this->MapStart(); it != this->MapEnd(); it=this->MapIncrement(it))  {
        RXEntity_p p  = dynamic_cast<RXEntity_p>( it->second);
        if (p->TYPE==PANEL ){
            if(!((Panelptr)p)->Display_One_Panel(seg)) { // probably bad material
                p->Kill();
            }
        }
    }
    return(1);
}

int RXSail::DrawPanelEdges(RXGRAPHICSEGMENT p_k) {
    int rc=0;
    vector<RXEntity* >thelist;
    vector<RXEntity* >::iterator it;
#ifdef HOOPS
    HC_Open_Segment("psides");
    HC_Flush_Segment(".");
    HC_Set_Color("lines=black");
#endif
    thelist.clear();
    MapExtractList(PCE_PSIDE,  thelist,PCE_ISRESOLVED) ;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        PSIDEPTR ps  =dynamic_cast<PSIDEPTR> (*it);
        ps->DrawDeflected( p_k); rc++;
    } /* for i */
#ifdef HOOPS
    HC_Close_Segment();//psides
#endif
    return rc;
}
int RXSail::DrawTriangles(double scale,RXGRAPHICSEGMENT p_k) {
    if(!m_FTri3s.size()) return 0;
#ifdef HOOPS
    HC_Open_Segment("tris");
    HC_Flush_Geometry(".");
#endif
    if(0)  {
        vector<RX_FETri3*>::iterator ii = m_FTri3s.begin(); ii++;
        int nn,k;
        for(;ii != m_FTri3s.end();ii++) {
            FortranTriangle *te  = *ii;
            VECTOR p[3];
            if((te == NULL)|| ((te)->GetN() == 0)) continue;
            RX_FESite *q;
            for(k=0;k<3;k++) {
                nn = te->node[k];
                q = m_Fnodes[nn];
                ON_3fPoint r=q->DeflectedPos();
                p[k].x=r.x;	 p[k].y=r.y;	 p[k].z=r.z;
            }
            Shrink_Poly(3,p,scale);
            HC_KInsert_Polygon(3,&(p->x), p_k);
        }
    }
#ifdef COIN3D
    // now try to draw a SO shell
    //   collect the coordinates into a float array
    // collect the N13 into (n1,n2,n3,SO_END_FACE_INDEX,
    // do a RX_Insert_Shell ( xyx, nv, faceSet, nfaces)
    int cp=0,nv = this->m_Fnodes.size();
    int nf = this->m_FTri3s.size();
    float xyz[3*nv];
    int faceSet[ 4*nf];
    float *px =xyz; int*pi =faceSet;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        if(!s)
        { *(px++)=0; *(px++)=0; *(px++)=0; cp+=3;}
        else  {
            ON_3fPoint p = s->DeflectedPos();
            *(px++)=p.x ;*(px++)= p.y;*(px++)=p.z ; cp+=3;
        }
    }
    int cf=0;
    for( vector<RX_FETri3*>::iterator it=m_FTri3s.begin (); it !=this->m_FTri3s.end();++it)  {
        RX_FETri3* t = *it;
        if(!t) continue;
        *(pi++)=t->Node(0); *(pi++)= t->Node(1); *(pi++)=t->Node(2); *(pi++)=SO_END_FACE_INDEX; cf+=4;
        assert(cf<4*nf);
    }
    RX_InsertPolyhedron( xyz,cp,faceSet,cf,p_k);
#endif
#ifdef HOOPS
    HC_Close_Segment();
#endif
    return 1;
}
int RXSail::Refresh_PC_File( const char*filename) {
    RXEntity_p  e = m_Selected_Entity[PCE_PCFILE ]; if(!e) return 0;
    RX_PCFile *p = ( RX_PCFile *)e->dataptr; if(!p) return 0;
    int rc= p->Refresh(filename,RX_PCFile_LINK_LENGTHS+RX_PCFile_COORDS+RX_PCFile_LINK_PROPS);
    return rc;
}
class ON_3dPoint RXSail::XToGlobal(class ON_3dPoint const & p) {
    class ON_3dPoint rv;
#ifdef FORTRANLINKED
    XtoGlobal(SailListIndex(), p, rv) ;
#else
    rv=p;
#endif
    return rv;
}
class ON_3dPoint RXSail::XToLocal(class ON_3dPoint const & p) {
    class ON_3dPoint rv;
#ifdef FORTRANLINKED
    XtoLocal(SailListIndex(), p, rv) ;
#else
    rv=p;
#endif
    return rv;
}
class ON_3dPoint RXSail::VToGlobal(class ON_3dPoint const & p) {
    class ON_3dPoint rv;
#ifdef FORTRANLINKED
    VtoGlobal(SailListIndex(), p, rv) ;
#else
    rv=p;
#endif
    return rv;
}
class ON_3dPoint RXSail::VToLocal(class ON_3dPoint const & p) {
    class ON_3dPoint rv;
#ifdef FORTRANLINKED
    VtoLocal(SailListIndex(), p, rv) ;
#else
    rv=p;
#endif
    return rv;
}

int write_pcfile(RXSail **ps, const char *filename){ // designed to be called from fortran
    if(!ps) return 0;
    if(!(*ps)) return 0;
    RXEntity_p  e = (*ps)->m_Selected_Entity[PCE_PCFILE ]; if(!e) return 0;
    RX_PCFile *p = ( RX_PCFile *)e->dataptr; if(!p) return 0;
    FILE *fp = fopen(filename,"w"); if(!fp) return 0;
    int rc = p->Print (fp);
    fclose(fp);
    return rc;

}



int RXSail::MakeLastKnownGoodDxDyDz()
{
    /* updates the dx dy dz fileds of the sail sites within a sail, from FORTRAN
 * Also updates the values in the site ENTITY list versions of teh nodes
 * return :
      0 - failure
    > 1 - success
 */
    int cnt=0;
    RX_FESite *s;
    ON_3dPoint v;
#ifdef HOOPS
    char l_buf[256];
    if(!Hoops_Nodes) {
        HC_Open_Segment_By_Key(PostProcNonExclusive() );
        Hoops_Nodes = HC_KOpen_Segment("node_positions");
        HC_Close_Segment();
        HC_Close_Segment();
    }

    HC_Open_Segment_By_Key(Hoops_Nodes);
    HC_Flush_Contents(".","everything");
    HC_Set_Color("text=purple");
#endif
    vector<RX_FESite*>::iterator it; // m_Fnodes;
    for(it=m_Fnodes.begin();it!=m_Fnodes.end();++it) {
        s = *it;
        if((s != NULL) && (s->GetN() >0)) {
            v = s->DeflectedPos(RX_MODELSPACE);
#ifdef HOOPS
            int n = s->GetN();
            sprintf(l_buf,"%d",n);
            HC_Insert_Text(v.x,v.y,v.z,l_buf);
#endif
            s->SetDeflection (v - ON_3dPoint(s->x,s->y ,s->z));
            cnt++;
        }
    }

    Post_Displacements();
    Post_GlobalMeasures();
#ifdef HOOPS
    HC_Close_Segment();
#endif
    return(cnt);
}
int RXSail ::Post_GlobalMeasures()
{
    // things like:
    // Average STRESS
    //Average Strain
    //average string tension
    // total string length - done by shapegen

    int c=0;

    return(c);
}//int Post_GlobalMeasures

int RXSail ::Post_Displacements() 
{
    int c=0;

    char lab[256],v[256];
    RX_FESite *l_s;
    RXEntity_p p;
    vector<RXEntity* >thelist;
    MapExtractList(SITE,  thelist) ;
    MapExtractList(RELSITE,  thelist) ;
    vector<RXEntity* >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        p   =dynamic_cast<RXEntity_p>(*it);

        l_s=dynamic_cast<RX_FESite *>(*it);
        if(!l_s)
            continue;
        if((l_s->m_Site_Flags&PCF_EXPORT_DISP))
        {
            ON_3dPoint d = l_s->Deflection();
            sprintf(lab,"Disp$X$%s",p->name());
            sprintf(v,"%f",d.x);
            Post_Summary_By_Sail(p->Esail,lab,v);
            sprintf(lab,"Disp$Y$%s",p->name());
            sprintf(v,"%f",d.y);
            Post_Summary_By_Sail(p->Esail,lab,v);

            sprintf(lab,"Disp$Z$%s",p->name());
            sprintf(v,"%f",d.z);
            Post_Summary_By_Sail(p->Esail,lab,v);
            c++;
        }
        if((l_s->m_Site_Flags&PCF_EXPORT_COORDS))
        {
            ON_3dPoint d = l_s->DeflectedPos();
            sprintf(lab,"X$%s",p->name());
            sprintf(v,"%f",d.x);
            Post_Summary_By_Sail(p->Esail,lab,v);
            sprintf(lab,"Y$%s",p->name());
            sprintf(v,"%f",d.y);
            Post_Summary_By_Sail(p->Esail,lab,v);

            sprintf(lab,"Z$%s",p->name());
            sprintf(v,"%f",d.z);
            Post_Summary_By_Sail(p->Esail,lab,v);
            c++;
        }
    } // for ip
    return(c);
}//int Post_Displacements

class RXSail *RXSail::LoadIndexToModel(const int mno)  /* mno = 0,1,2 etc */
{
    int i;
    SAIL *s;
    vector<RXSail*> lsl = g_World->ListModels(RXSAIL_ALLHOISTED); // meant to include the boat
    for(i=0;i<lsl.size() ;i++) {
        s=lsl[i];
        if (s->GetLoadingIndex() == mno)
            return s;
    }
    return NULL;
}
int RXSail::ReBuild_All_Materials(const RX_COORDSPACE space) {

    RXPanel * p;
    vector<RXEntity* >thelist;
    MapExtractList(PANEL,  thelist) ;
    vector<RXEntity* >::iterator it;

    for (it = thelist.begin (); it != thelist.end(); it++)  {
        RXEntity_p pd =dynamic_cast<RXEntity_p>(*it);
        p  =  (Panelptr)pd;
        p->ReBuildTriangles(space) ;

    }
    return 1;
}
#ifdef HOOPS
int RXSail::Display_Panel_Colours(HC_KEY theShell) {

    int  trino;
    char buf[256];
    if(!theShell)
        return(0);
    FILE *fp=NULL;

    if(g_ArgDebug) {
        sprintf(buf,"%s/triangles.out",traceDir);
        fp = RXFOPEN(buf,"w");
    }

    /* find last OK OldTriangle_t in list */


    HC_Open_Geometry(theShell);
    trino=0;
    vector<RX_FETri3*>::iterator ii = this->m_FTri3s.begin(); ii++;
    for(;ii != this->m_FTri3s.end();ii++) {
        FortranTriangle *te  = *ii;
        if( te && (te->GetN() != 0) ) {
            //trino = te->GetN() - 1;
            sprintf(buf,"faces=%s",te->m_colour);
            if(g_ArgDebug)
                fprintf(fp,"triangle %d : %d layers colour = '%s'\n",trino+1,te->laycnt,te->m_colour);
            HC_Open_Face(trino);
            HC_Set_Color(buf);
            HC_Close_Face();
            trino++;
        }
    }
    HC_Close_Geometry();

    return(1);
}
#endif


RXEntity_p RXSail::Insert_Entity( // its ugly that 'name' isnt const.  Maybe the code relies on a side-effect.
                                  const char*p_type,
                                  const char*p_name,
                                  void*dp,
                                  const HC_KEY hk,
                                  const char *att,
                                  const char*line,
                                  const int GuaranteeUnique) {

    /* returns the ptr to the new entity
 July 2003 profiling showa it spend a third of its time in List and getNItems.  Lets try eliminating
 courtesy of sail.
 */
    RXEntity *p=NULL;
    char type[256],name[256];
    int dup=0;
    char buf[128];
    int l_Type;
    strncpy(type,p_type,256);
    strncpy(name,p_name,256);
    RXEntity_p old =0;
    if(!GuaranteeUnique) old= this->Entity_Exists  (name,type);
    PC_Strip_Trailing(type);
    PC_Strip_Trailing(name);
    Make_Valid_Segname(name);

    if(old) {
        char s[256];
        if(strieq(type,"include") && (!old->Needs_Resolving)) {
            sprintf(s," Attempt to include file '%s' more than once.\n IGNORED !",name);
            rxerror(s,2);
            return(NULL);
        }
        sprintf(buf,"%s&%d",name,dup);
        while((old = this->Entity_Exists(buf,type))) {
            dup++;
            sprintf(buf,"%s&%d",name,dup);
        } 		// now have unique name
        sprintf(s," %s, a %s, already exists, so renaming to %s",name,type,buf);
        rxerror(s,1);
        strcpy(name,buf);
    } // if old

    l_Type = INTEGER_TYPE(type);

    p = this->NewEntity(l_Type,name);

    p->Esail=this;
    p->SetNeedsComputing();
    p->Needs_Resolving=0;
    p->generated = 1;

    p->SetInStack(0);
    p->SetTypeName(type,name);

    p->SetLine(line);
    p->RXObject::SetOName(TOSTRING(name));

    RXEntityDefault* d;
    if(d=dynamic_cast<RXEntityDefault*>(p)) // horrible: the old C logic.
        d->SetDataPtr(dp);
    p->TYPE = l_Type;
    p->AttributeDestroy();
    if(att)
    {
        QString qatt(att); ;
        p->AttributeAdd(qatt.toLower());
    }

    p->hoopskey = (HC_KEY) hk;

    p->Esail->MapInsertObject(p);

    return(p);
} // end of PC_Insert_Entity



class RXEntity * RXSail::NewEntity(const int p_type, const char*name){
    class RXEntity *rc=0;
    /* expressions are found in:
 SEAMCURVE
 basecuRVe
 RXFE_STRING
 at least */
    switch(p_type) {
    case SITE:
    case RELSITE:
        rc= new RXSRelSite(this);
        return rc;
    case PANEL:
    case PCE_ZONE:
        rc= new RXPanel(this);
        return rc;
    case PCE_PSHELL:
        rc= new rxpshell(this);
        return rc;
    case PCE_LAYER:
        rc= new RXLayer(this);
        return rc;
    case PCE_STRING:
        rc= new RX_FEString(this);
        return rc;
    case PCE_BEAMSTRING:
        rc= new RX_FEBeam(this);
        return rc;
    case PCE_BEAMMATERIAL:
        rc= new RXBeamMaterial(this);
        return rc;
    case GENDP:
        return new RXGridDensity(this);
    case SEAMCURVE:
        rc= new RXSeamcurve(this);
#ifndef HOOPS
        if(! DISABLE_GRAPHICS )
            rc->setGNode(RXNewGNode(name,GNode()));
#endif
        return rc;
    case  PCE_NODECURVE:
        rc= new RXNodeCurve(this);
#ifndef HOOPS
        if(! DISABLE_GRAPHICS )
            rc->setGNode(RXNewGNode(name,GNode()));
#endif
        return rc;

    case COMPOUND_CURVE:
        return new RXCompoundCurve(this);
    case POCKET:
        return new RX_FEPocket(this);
    case PCE_PSIDE:
        return new RXPside(this);
    case PCE_SCALAR: {
        class RXScalarProperty*e = new RXScalarProperty(TOSTRING(name),TOSTRING("1"),this);
        return  e;
    }
    case PCE_DWGLAYER: {
        return new RXDrawingLayer(this);;
    }
    case  PCE_FIXITY: {
        return new  RXFixity(this);
    }
    case   PCE_VECTORFIELD: {
        return new  RXVectorField(this);
    }
    case  PCE_SPLINE: {
        return new RXExpSpline(this);
    }

    case TWODCURVE    :
        return new RXCurve2D(this);
    case PCE_UVCURVE    :
        return new RXCurveUV(this);
    case THREEDCURVE  :
        return new RXCurve3D(this);
    case GAUSSCURVE   :
        return new RXGaussCurve(this);
    case BASECURVE   :
        return new RXBCurve(this);
    case PCE_IMPORT:
        return new RXImportEntity(this);
    case PCE_RIGIDBODY:
        return new RXRigidBody(this);
    case PCE_SUBMODEL:
        return new RXsubModel(this);

    default:
        rc=new RXEntityDefault();
        rc->SetParent(this);
        return rc;
    };

}
int RXSail::Compute()
{
    int rc=0;
    assert(0);
    return rc;
}
int RXSail::Resolve()
{
    int rc=0;
    assert(0);

    return rc;
}
int RXSail::Finish()
{
    int rc=0;
    assert(0);
    return rc;
}
/***********************************************************/

int RXSail:: R_Solve() {	/* should return 1 if anything has moved */
    RXEntity_p p  ;
    int  depth = 0;
    int flag;
    int Something_Needs_Doing;
    flag=0;
    g_RCOUNT=0;

    do {
        Something_Needs_Doing=0;

        ent_citer it;
        for (it = this->MapStart(); it != this->MapEnd();it=this->MapIncrement(it)) {
            p =  it->second ;
            if(p->NeedsComputing()) {
                if(!p->Needs_Resolving) {
                    Something_Needs_Doing=1;
                    depth=0;
                   int iret = p->Recursive_Solve(depth,this);
                    if(g_RCOUNT >=5*this->MapSize()  )
                        break;
                    if(iret)
                        flag=1;
                }
            }
        }

    } while(Something_Needs_Doing && (g_RCOUNT <5*this->MapSize()));

    if(g_RCOUNT >  5*this->MapSize() -2 ) {
        RXSTRING sbuf = RXSTRING(L"model '")+  this->GetName() +RXSTRING(L"' may not be converged" );
        rxerror(sbuf,2);
    }
    return(flag);
}

#ifdef NEVER
int RXSail::UnResolveEntities(const char*type, const char *name ){
    // if name is blank, take the first one
    int rc=0;
    int t = INTEGER_TYPE(type);
    vector<RXEntity* >thelist;
    vector<RXEntity* >::iterator it;
    thelist.clear();
    MapExtractList(t, thelist, PCE_NOFILTER) ;

    for (it = thelist.begin (); it != thelist.end(); it++)  {
        RXEntity_p e  = dynamic_cast<RXEntity_p>(*it );
        if( !ON_WildCardMatchNoCase(e->name(), name) ) continue;
        rc+=e->CClear();
    }
    return rc;
}
#endif
// a variable format for exporting part-built models
int RXSail::Dump(const char* fname)
{
    char q=' ';
    int rc=0;
    ON_3dPoint p;
    ofstream os(fname );
    if(!os) return 0;
    double u,v,dp; int c=0;
    // first gather UV info for sites.
    map<int,pair<double,double> > mymap; map<int,pair<double,double> >::iterator   mi;
    vector<RXEntity_p >thelist;
    MapExtractList(SEAMCURVE, thelist);
    double u1,u2,v1,v2;
    for (vector<RXEntity_p >::iterator it = thelist.begin (); it != thelist.end(); it++)  {
        RXEntity_p e  =*it;
        RXSeamcurve *sc = (RXSeamcurve * ) e;
        RXSRelSite *e1= sc->End1site;
        RXSRelSite *e2= sc->End2site;
        if(!e1 ||!e2) continue;
        //  RXAttributes rxa1(e1->attributes ), rxa2(e2->attributes );
        if (e1->AttributeGetDble( QString ("$u"),&u1) &&e2->AttributeGetDble(QString("$u"),&u2)) {
            if (e1->AttributeGetDble(QString("$v"),&v1) && e2->AttributeGetDble(QString ("$v"),&v2)) {
                double tot=0.;
                vector<class RX_FEedge*>::iterator pt;
                for(int i=0;i<sc->npss;i++) {
                    PSIDEPTR ps = sc->pslist[i];
                    for (pt = ps->m_eList.begin (); pt != ps->m_eList.end(); pt++)  {
                        RX_FEedge *ee = *pt;
                        tot+= ee->GetLength();
                    }
                }
                double s=0.;
                for(int i=0;i<sc->npss;i++) {
                    PSIDEPTR ps = sc->pslist[i];
                    for (pt = ps->m_eList.begin (); pt != ps->m_eList.end(); pt++)  {
                        RX_FEedge*ee = *pt;
                        s+=ee->GetLength ()/tot;
                        RX_FESite*ss = ee->GetNode(1);
                        double t = ss->getDs()/sc->m_arcs [1];
                        mymap[ss->GetN()] = pair<double,double>(u2*s + u1*(1.-s), v2*s + v1*(1.-s)  );
                    }
                }
                mymap[e1->GetN()] = pair<double,double>(u1, v1  );
                mymap[e2->GetN()] = pair<double,double>(u2, v2  );
            }
        }
    }

    // now start work
    os<<"<?xml version=""1.0""?>"<<endl;
    os<<"<RELAXMODEL NAME="""<< ToUtf8(this->GetName()).c_str()<<"""  TYPE="""<< this->GetType().c_str() <<""" >"<<endl;
    os<<"<NODES>"<<endl;
    for( vector<RX_FESite*>::iterator it=m_Fnodes.begin (); it !=this->m_Fnodes.end();++it)  {
        RX_FESite* s = *it;
        RXSRelSite *rs = dynamic_cast<RXSRelSite *>( s);
        if(!s)
            continue;
        else  {
            p = s->DeflectedPos();
            os <<s->GetN()<<" "<< p.x <<" " <<p.y <<" " <<p.z ;
            mi = mymap.find(s->GetN() );
            if(mi !=mymap.end()) {
                os<<" "<<mi->second.first <<" "<<mi->second .second <<" ";
            }
            else
                os<<" "<<ON_UNSET_VALUE <<" "<<ON_UNSET_VALUE <<" ";
            os<<endl;
        }
    }
    os<<"</NODES>"<<endl;
    os<<"<EDGES>"<<endl;
    for( vector<RX_FEedge*>::iterator it=m_Fedges.begin (); it !=this->m_Fedges.end();++it)  {
        RX_FEedge* s = *it;
        if(!s)
            continue;
        else  {

            os << s->GetN()<<q<< s->GetNode(0)->GetN()<<q<< s->GetNode(1)->GetN();

            os<<q<<s->m_Linit<<q<<s->m_initialCorrection;

            os<<endl;
        }
    }
    os<<"</EDGES>"<<endl;

    os<<"</RELAXMODELDUMP>"<<endl;
    os.close ();
    cout <<" dumped to file: "<<fname<<endl;
    return rc;
}

//this-> MakeUVMapping_XYPlane(); // the original, uses class RXShapeInterpolation
///this->MakeUVMappingFromFEMesh(); // doesnt generate a nw triangulation so it is independent of orientation

int RXSail::MakeUVMapping_XYPlane( )
{
    //cout<<"FL_HASGEOMETRY     " <<GetFlag( FL_HASGEOMETRY  )<<endl;
    //cout<<"FL_HASPANELS       " <<GetFlag( FL_HASPANELS  )<<endl;
    //cout<<"FL_HASMESH 	      " <<GetFlag( FL_HASMESH  )<<endl;
    //cout<<"FL_NEWGEOMETRY 	  " <<GetFlag( FL_HASNEWGEOMETRY  )<<endl;
    //cout<<"FL_HASLACKOFFIT    " <<GetFlag(FL_HASLACKOFFIT   )<<endl;
    //cout<<"FL_HASNEWTOPOLOGY  " <<GetFlag( FL_HASNEWTOPOLOGY  )<<endl;
    //cout<<"FL_ONFLAG	      " <<GetFlag(  FL_ONFLAG )<<endl;
    //cout<<"FL_ISFROZEN	" <<GetFlag( FL_ISFROZEN  )<<endl;
    //cout<<"FL_ISBOAT	      " <<GetFlag( FL_ISBOAT  )<<endl;
    //cout<<"FL_ISTPN	      " <<GetFlag( FL_ISTPN  )<<endl;

    SetMsgText("triangulate..",this->Client());

    if(this->m_uv2xyz) delete  this->m_uv2xyz;
    this->m_uv2xyz=new RXShapeInterpolation(this, m_Fnodes, m_Fedges  );

    class RXImportEntity *rxie = dynamic_cast<class RXImportEntity *>(this->GetFirst("import"));// for sailpack nastran models
    if(rxie)
        rxie->SpecialTreatmentForLuffLeechFootHead();

    this->m_uv2xyz->m_xf= this->Trigraph();
    //  if the sail's atts include   $uvmap=(something)  we look for a vector field named myuvtouv
    // which returns the coordinates in flat space given uv in the unit square.


    int rc=this->m_uv2xyz->Prepare( this->GetWorld ()->m_LambdaForxyz ,0);// XYZ space - but WHY not just use existing triangulation?
    if(!rc)
    {
        if(this->m_uv2xyz) delete this->m_uv2xyz;
        this->m_uv2xyz=0;
    }
  //

    DumpInterpolations();
    return rc;


}
    int RXSail::DumpInterpolations()const    // following is debug
    {
        if(!this->m_uv2xyz )
            return 0;
        int rc=1;
         SetMsgText( "..... write debug files.",this->Client());
        QString dname("_UVmesh.txt"); dname.prepend(this->GetQType());dname.prepend("bad_");
        this->m_uv2xyz->Dump(qPrintable(dname));

        rxONX_Model  model;
        rxONX_Model*pmodel = &model;
        if(this->m_p3dmModel  )
            if(this->m_p3dmModel->m_pONXModel )
                pmodel = this->m_p3dmModel->m_pONXModel;

        rxON_Mesh*uvm = this->m_uv2xyz->MakeONMeshUV();
        rxON_Mesh*xyz = this->m_uv2xyz->MakeONMeshXYZ();
        if(uvm->IsValid()){
            ONX_Model_Object& mu = pmodel->m_object_table.AppendNew();
            mu.m_object = uvm;
            mu.m_bDeleteObject = true;
            mu.m_attributes.m_layer_index = 2;
            mu.m_attributes.m_name = ON_String("badUVMesh");
        }
        if(xyz->IsValid()){
            ONX_Model_Object& mx = pmodel->m_object_table.AppendNew();
            mx.m_object = xyz;
            mx.m_bDeleteObject = true;
            mx.m_attributes.m_layer_index = 2;
            mx.m_attributes.m_name = ON_String("badXYZMesh");
        }

        pmodel->Polish ();
        QString fname("badflattening_"); fname+= QString::fromStdString(this->GetType()) + QString(".3dm");

        pmodel->Write (qPrintable(fname),3);
        return rc;
    }

void RXSail::LocateInit  (){
    if(this->m_uv2xyz && this->m_uv2xyz->HasMesh())
        this->m_uv2xyz->LocateInitialize ();
}


ON_3dPoint RXSail::CoordsAt(const ON_2dPoint& p,const RX_COORDSPACE s)
{
    if(this->m_uv2xyz)
        return this->m_uv2xyz->CoordsAt (p,s);

    if(this->m_mould && (s == RX_MODELSPACE ||s== RX_BLACKSPACE) ) {
        ON_3dPoint rc(0,0,0);
        Evaluate_NURBS(this->m_mould, p.x,p.y,&rc);
        return rc;
    }
    //cout<<" cant get coordsAt for model "<<this->GetType().c_str()<<endl;
    return ON_3dPoint(ON_UNSET_VALUE, ON_UNSET_VALUE, ON_UNSET_VALUE);
}

ON_2dPoint RXSail::CoordModelXToUV(RXSitePt *q,int*err) const{
    // q must be in RX_MODELSPACE
    // the methods are:
    //1 if there is a xyz2UV grid (not implemented december 2009) use it.
    //2) If there is a mould, do a find-nearest

    ON_2dPoint uv(q->m_u,q->m_v);

    *err=1;
    if(this->m_uv2xyz) {
        *err=0;
        return uv;
    }

    RXON_NurbsSurface *l_rxs = this->GetMouldSurface ();

    if(l_rxs) {

        double m0,r0,m1,r1,u,v;
        m0 = l_rxs->Domain(0).Min(); r0 = l_rxs->Domain(0).Length();
        m1 = l_rxs->Domain(1).Min(); r1 = l_rxs->Domain(1).Length();

        if(ON_IsValid(uv.x))
            u = uv.x * r0 + m0;
        else
            u = ON_UNSET_VALUE ;
        if(ON_IsValid(uv.y))
            v = (uv.y) * r1 + m1;
        else
            v = ON_UNSET_VALUE;

        if(l_rxs->GetLocalClosestPoint(q->ToONPoint () ,u,v,&u,&v)) { // typically ON_NurbsSurface try l_rcxs not l_s
            q->m_u=uv.x  = (u-m0 )/ r0;
            q->m_v=uv.y = (v-m1 )/ r1;

        }
        else {
            ON_String l_buf; l_buf.Format(" site (%f %f %f) fails GetClosest",q->x,q->y,q->z);
            rxerror(l_buf,1);
            *err=1; return ON_2dPoint(ON_UNSET_VALUE,ON_UNSET_VALUE);
        }
        *err=0; return uv;
    } // l_rxs


    *err=1;
    return  ON_2dPoint(ON_UNSET_VALUE, ON_UNSET_VALUE);
}




ON_3dVector RXSail::DeflectionsAt(const ON_2dPoint& p,const RX_COORDSPACE s)
{
    ON_3dVector rv(0,0,0);
    int ii[3]; double ww[3];
    this->m_uv2xyz->GetMesh()->Locate(p,ii,ww);
    // here ii may be beyond the end of pts
    for(int k=0;k<3;k++) {
        if(ii[k]<=0) continue;
        rv+=this->m_uv2xyz->Pt(ii[k]).Deflection()  * ww[k];
    }
    return rv;
}

ON_3dVector RXSail::VelocityAt(const ON_2dPoint& p,const RX_COORDSPACE s)
{
    ON_3dVector rv;
    assert(0);
    return rv;
}

RXON_NurbsSurface* RXSail::GetMouldSurface() const{ // this is a paste from Evaluate_NURBS_By_XYZ

    RXON_NurbsSurface* rc=0;
    if(!m_mould) return 0;

    struct PC_INTERPOLATION*p = ( struct PC_INTERPOLATION*)m_mould->dataptr;
    assert(p);
    struct PC_NURB *l_n = p->NurbData;

    if ( l_n && l_n->m_pONobject){
        ON_Surface *l_s = ON_Surface::Cast (l_n->m_pONobject); // is this RXON_Surface??
        rc = (RXON_NurbsSurface *) l_s;
    }
    return rc;
}

int  RXSail::PostProcessOneModel(const int p_Wh){ 
    // for post-processing
    int rc=0;
#ifdef HOOPS
    this->Peters_Generate_Geometry();
    rc=Hoops_PostProcess(this,p_Wh);
#endif

    this->ClearFlag(FL_NEEDS_POSTP);
    return rc;
}
// remove all entries starting with $<getType()>$
int RXSail::RemoveFromSummary(  )
{
    int rc=0;
    RXMirrorDBI* db = this->GetWorld()->DBMirror();
    if(!db)
        return 0;


    rc=db->RemoveAllWithPrefix(this->GetType() );
    return rc;
}
// post all editable nodes , strings, battens to m_DBout
// call this when we hoist a sail
// also, when we open a DBout we call it on all hoisted models.
// also, whenever we execute a 'change'
int RXSail::AddToSummary()
{
    class RX_FEPocket *pock;
    RXEntity_p p; RXEntity_p  nodeEnt;
    RXExpressionI *a, *b, *c,*exp;
    string header;
    //nodes    //$boat$node$b_main_clew$b
    vector<RXEntity_p >thelist;
    int rc=0;

    this->MapExtractList(SITE,  thelist,PCE_ISRESOLVED) ;
    vector<RXEntity_p >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        nodeEnt = *it;
        class RXSRelSite* s;

        if(nodeEnt->AttributeGet("editable") || nodeEnt->AttributeGet("$trimmable"))
        {
            s = ( RXSRelSite*)  (nodeEnt );
            a =  s->FindExpression(L"a",rxexpLocal) ;
            b =  s->FindExpression(L"b",rxexpLocal) ;
            c =  s->FindExpression(L"c",rxexpLocal) ;
            rc++;
            header=string("node$")+s->name()+string("$a");

            Post_Summary_By_Sail(this,header.c_str(),ToUtf8(a->GetText()).c_str());

            header=string("node$")+s->name()+string("$b");
            Post_Summary_By_Sail(this,header.c_str(),ToUtf8(b->GetText()).c_str());

            header=string("node$")+s->name()+string("$c");
            Post_Summary_By_Sail(this,header.c_str(),ToUtf8(c->GetText()).c_str());
        }
    }
    // strings,  $genoa$string$drisse$Trim   //$boat$STRING$ecoutegenoa$Prestress
    //battens   $genoa$pocket$latte2$Trim
    thelist.clear();
    QString qbuf;
    MapExtractList(PCE_STRING,  thelist,PCE_ISRESOLVED) ;
    for (it = thelist.begin (); it != thelist.end(); it++)  {

        class RX_FEString* TheString = ( class RX_FEString * ) *it;
        assert(TheString);
        TheString->PostToMirror();
    }
    thelist.clear();
    if( !this->FlagQuery(NO_BEAM_POST)  ){
        MapExtractList(PCE_BEAMSTRING,  thelist,PCE_ISRESOLVED) ;
        for (it = thelist.begin (); it != thelist.end(); it++)  {
            class RX_FEBeam* TheBee = ( class RX_FEBeam *) *it;
            assert(TheBee);
            TheBee->PostToMirror();
        }
    }

    thelist.clear();
    this->MapExtractList(POCKET,  thelist,PCE_ISRESOLVED) ;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        pock =  (class RX_FEPocket * ) (*it);
        assert(pock->TYPE==POCKET );
        double*x= pock->GetTrimPtr();
        header=string("pocket$")+(*it)->name()+string("$trim");
        exp=pock->FindExpression(L"trim",rxexpLocal);
        if(exp)
            qbuf = QString (ToUtf8(exp->GetText()).c_str());
        else
            qbuf =  QString("%1").arg(*x);
        Post_Summary_By_Sail(this,header.c_str(),qPrintable(qbuf));
        rc++;
    }
    //$boat$mast_spline$side$0
    class RXSplineI* bc =( class RXSplineI* )this->GetKeyNoAlias(L"spline",L"mast_spline");
    if(bc) {
        for(int i=0;i< bc->size();i++) {
            header=string("mast_spline$side$") + to_string(i) ;
            Post_Summary_By_Sail(this,header.c_str(),bc->getText(i,1).c_str());
            header=string("mast_spline$aft$") + to_string(i) ;
            Post_Summary_By_Sail(this,header.c_str(),bc->getText(i,2).c_str());
            rc+=2;
        }

    }// if bc
    else rc= 0;
    return rc;
}

int RXSail::PostSomeResults()
{
    vector<RXEntity_p >::iterator it;
    vector<RXEntity_p >thelist;
    int rc=0;

    thelist.clear();
    MapExtractList(PCE_STRING,  thelist,PCE_ISRESOLVED) ;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        class RX_FEString* TheString = ( class RX_FEString * ) *it;
        assert(TheString);
        TheString->PostToMirror();
    }
    thelist.clear();
    if( !this->FlagQuery(NO_BEAM_POST)  ){
        MapExtractList(PCE_BEAMSTRING,  thelist,PCE_ISRESOLVED) ;
        for (it = thelist.begin (); it != thelist.end(); it++)  {
            class RX_FEBeam* TheBee = ( class RX_FEBeam *) *it;
            assert(TheBee);
            rc+=TheBee->PostToMirror();
        }
    }

    else rc= 0;
    return rc;
}
ON_Xform RXSail::Trigraph(void)const
{
    ON_3dVector l_Eltnormal;
    ON_Xform l_xf;
    ON_3dVector l_xg,l_x,l_y;

    l_Eltnormal = this->Normal();
    assert(l_Eltnormal.LengthSquared()-1. < 1e-8);

    if(l_Eltnormal.IsParallelTo(ON_3dVector(1,0,0)))
        l_xg = ON_3dVector(0,1,0);
    else
        l_xg = ON_3dVector(1,0,0);


    l_y = ON_CrossProduct(l_Eltnormal,l_xg);
    l_y.Unitize();

    l_x = ON_CrossProduct(l_y,l_Eltnormal);
    l_x.Unitize();

    l_xf = ON_Xform(ON_3dPoint(0,0,0),l_x,l_y,l_Eltnormal);
    l_xf.Transpose();
    return l_xf;

}

#define STEP_SIZE_U  ((double) (1.0/21.0))
#define STEP_SIZE_V  ((double) (1.0/21.0))
#define NROWS   (22)
#define NCOLS   (22)
double RXSail::ReferenceLength()
{
    double d=1;
    if(m_uv2xyz && m_uv2xyz->HasMesh()){
        this->m_uv2xyz->LocateInitialize ();
        ON_3dPoint p1 = this->CoordsAt(ON_2dPoint(0,0),RX_GLOBAL);
        ON_3dPoint p2 = this->CoordsAt(ON_2dPoint(1,1),RX_GLOBAL);
        return p1.DistanceTo (p2);
    }
    cout<<"RXSail::ReferenceLength is default value (1)"<<endl;
    return d;
}

int RXSail::write_ND_state(const char *filename)
{

    class RXLogFile l_logfile;
    class RXLogFile * dbptr = & l_logfile ; //this->m_pSailSum;

    int i,q;
    int rc= 0;
    char fname[256];
    FILE *fa=NULL;
    double scale;

    double u,v;
    char  format[128],buf[256];
    ON_3dPoint smod;
    RXEntity_p nodeEnt;


    if((filename == NULL) || (*filename == '\0')) return(0);
    strcpy(fname,filename);

    sprintf(buf,"$%s$",this->GetType().c_str());
    /*
December 2009.
*/
    /* Peter changed 2/2/97. was "%s$" but that gives trouble, for instance when there is a
model called <smallgenoa> which has different node naming to  <genoa>
also the 'flush summary call stops rubbish being passed to subsequent models.
THis had a virus effect. summary data could pass via the boat NDD files between separate
kinds of model. This would survive flushing of the main summary file
 */
    /* Feb 2003 a useful improvement would be to get Create_Filtered Summary to skip those records
which ought to have an entity but don't.  This stops things like the W60 spreacher block from getting
propagated through all files
*/
    dbptr->FlushMirrorTable ();

    if(g_World->DBMirror()  )
        if(!g_World->DBMirror()->Create_Filtered_Summary(dbptr,buf))
            rxerror(" writeState filtered summary returned an error",1);

    /* now post all the positions of the nodes */
    // scale by a characteristic dimension - the diagonal of the bounding box.

    scale = this->ReferenceLength ();

    vector<RXEntity_p >thelist;
    this->MapExtractList(SITE,  thelist,PCE_ISRESOLVED) ;
    this->MapExtractList(RELSITE,  thelist,PCE_ISRESOLVED) ;
    vector<RXEntity_p >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        nodeEnt = *it;
        if  (nodeEnt->AttributeGet("editable"))
        {
            RX_FESite *s = dynamic_cast<RX_FESite *> ( *it);
            *buf=0;
            ON_3dVector dd = s->Deflection();
            dbptr->post_summary_value(
                        "$"+this->GetType()+ "$node$"+nodeEnt->name()+"$dx",
                        to_string(dd.x/scale));
            dbptr->post_summary_value(
                        "$"+this->GetType()+ "$node$"+nodeEnt->name()+"$dy",
                        to_string(dd.y/scale));
            dbptr->post_summary_value(
                        "$"+this->GetType()+"$node$"+nodeEnt->name()+"$dz",
                        to_string(dd.z/scale));
        }
    }

    // a fixup for empty sailsums
    if(!(dbptr->GetNCols()) )
        dbptr->post_summary_value("StateFile",fname);


    dbptr->Write_MirrorDB_As_CSV(fname);

    if(m_uv2xyz && m_uv2xyz->HasMesh()){
        this->m_uv2xyz->LocateInitialize ();
    }
    else{
        QString buf("No UV coords defined -cant write ND file: " );
        buf+=QString::fromStdString( this->GetType());
        rxerror(buf,1);
        return 1;
    }

    fa=RXFOPEN(fname,"a");
    if(fa == NULL) {
        return(0); }
    printf("Writing ND file  %s\n",fname);

    /* print outside 4 points */

    strcpy(format,"%15.8f\t %15.8f\t %15.8f\t %15.8f\t %15.8f\t %15.8f\t %15.8f\n" );
    fprintf(fa,format,-1.0,-1.0,0.0,0.0,0.0,0.0,0.0);
    fprintf(fa,format,-1.0, 2.0,0.0,0.0,0.0,0.0,0.0);
    fprintf(fa,format, 2.0, 2.0,0.0,0.0,0.0,0.0,0.0);
    fprintf(fa,format, 2.0,-1.0,0.0,0.0,0.0,0.0,0.0);
    /*
coordsAt uses site->DeflectedPosition.
called with GLOBAL it returns the current position in world space (not lastknowngood)
called with MODELSPACE it returns the current position in world space transformed them to modelspace.
*/

    for(i=0;i<NROWS;i++) {
        for(q=0;q<NCOLS;q++) {
            u = ((double) i) * STEP_SIZE_U;
            v = ((double) q) * STEP_SIZE_V;
            smod= this->CoordsAt(ON_2dPoint(u,v),RX_MODELSPACE );//
            ON_3dVector dd =  this->DeflectionsAt(ON_2dPoint(u,v),RX_MODELSPACE );
            dd=dd/scale;

            fprintf(fa,"%15.8f\t %15.8f\t %15.8g\t %15.8g\t %15.8g\t %15.8f\t %15.8f\n",
                    u,v,dd.x,dd.y,dd.z,smod.x,smod.y);
            rc++;
        }
    }

    FCLOSE(fa);
    return(rc);
}

// pseudo code for read

//        sh= new class RXShapeInterpolationP;
//        sh->Read(wstring(fname));

//        if(!sh->HasMesh () ) { cout << "state(ndd)file doesnt create a mesh"<<endl; return 0 ;}

//        sh->L ocate Initialize();

//        for each SITE which has (UV)
//                ON_2dPoint p2 (u,v);
//                ON_3dVector d= sh->ValueAt (p2) *scale;
//                Set Deflection(d)

// finally the entities whose deflections are recorded in the file.


int RXSail::read_ND_state(const char *filename)
{
    RX_FESite *s;
    ON_3dVector dl ;

    double scale =   this->ReferenceLength ();
    class RXLogFile l_logfile;
    class RXLogFile * dbptr = & l_logfile ; //this->m_pSailSum;
    int rvv;
    // first apply the distributed properties, then the explicit nodal ones.

    int nlines;
    class RXShapeInterpolationP sh(this) ;
    double res[5];
    assert(sh.dataSize()<=5);
    nlines=sh.Read(TOSTRING(filename),2,5); // -ve if file cant be opened
    if(3<=nlines) { // skiplines, dataperline.
        int npts = sh.NPoints() ;
        if(sh.NPoints()  >=3){
            sh.Triangulate();
            if( sh.HasMesh () ) {
                sh.LocateInitialize();
                for(vector< RX_FESite *>::iterator it=this->m_Fnodes.begin();it!=this->m_Fnodes.end();++it) {
                    s =dynamic_cast<RX_FESite*> ( *it);
                    if(!s) continue;
                    if(!(s->m_Site_Flags&PCF_ISFIELDNODE))
                        continue;
                    rvv=sh.ValueAt (ON_2dPoint(s->m_u,s->m_v),res);
                    if(rvv !=BAD){
                        dl = ON_3dVector(res) *scale;
                        s->SetDeflection (dl); //expects the vector in model space.  this updates the FEA DB too
                    }
                } // for it
            }
            else //nomesh
                cout << "state(ndd)file doesnt create a mesh"<<endl;
        }
        else
            cout << "state(ndd)file has too few points"<<endl;
    }
    else
        if(nlines<0) cout << "cant read state(ndd)file (Permissions?, corrupt?) '"<<filename<<"'"<<endl;
    // now the explicitly written nodal deflections
    // the first two lines are a CSV file

    dbptr->SetFileName(filename);
    dbptr->read_and_post_one_row( (int) 0,QString(""),dbptr);//cout<<"rpor"<<endl;

    int i;
    class SUMMARY_ITEM *si;
    for( i=0,si=dbptr->GetListHead();i<dbptr->GetNCols();i++,si++) {
        if(stristr(si->header,"node$") &&(stristr(si->header,"$dx")||stristr(si->header,"$dy")||stristr(si->header,"$dz"))) {
            char buf[128];
            sprintf(buf,"%f",si->value * scale);
            dbptr->post_summary_value(si->header,buf);
        }
    }
    dbptr->Apply_Summary_Values();
    dbptr->FlushMirrorTable();
    this->MakeLastKnownGoodDxDyDz();
    return(1);
}
/// Resolve_All_Entities would be much better as a TBB parallel_do
///
int RXSail::Resolve_All_Entities(){

    //   SAIL *this =this;
    int nc=1;	  // number of changes this loop
    int Some_Left=0;	  // count of entities NOT resolved

#ifndef RXQT
    int regardless=0;  // a guess was it 1 or 0
    Finish_All_Questions(this,regardless); // ask the questions and acts on the answers. NOT regardless
#endif

    while (nc) {
        Report_Init(this,this->GetReportForm());
        nc = 0;
        Some_Left=0;

        // first do the sets and layers so tolerances get set
        // this is a horrible work-around for order dependence
        // the right way is for RXSail::EntityKey to provide a value
        // which orders the map for efficient resolving

        ent_citer ite;
        for (ite = this->MapStart(); ite != this->MapEnd(); ite=this->MapIncrement (ite)) {
            RXEntity_p e  =  (RXEntity_p)  ite->second;
            if (e && e->Needs_Resolving && e->TYPE == PCE_SETDATA ) {
                nc+= Resolve_Set_Card(e);
            }
            else
                if (e && e->Needs_Resolving && e->TYPE == PCE_DWGLAYER)
                    nc+=e->Resolve ();
        }

        if(nc)Report_Clear(this->GetReportForm());
    } // WHILE nc

    //now do the rest
    nc=1;
    while (nc) {
        Report_Init(this,this->GetReportForm());
        nc = 0;
        Some_Left=0;
        vector<RXEntity* >thelist;
        vector<RXEntity* >::iterator it;
        thelist.clear();
        this->MapExtractList(0, thelist, PCE_ISNOTRESOLVED) ;
        if(thelist.size ()) qDebug()<<"no of entities to resolve now = "<<thelist.size ()<<"in"<<this->GetQType();
        for (it = thelist.begin (); it != thelist.end(); it++)  {
            RXEntity_p e  = dynamic_cast<RXEntity_p>(*it );
            if( !e->Needs_Resolving  ) // it was caught by a recursive resolve
                continue;
            assert(   e->TYPE != PCE_NAME );
            if(e->TYPE==PCE_DELETED)
                e->TYPE=INTEGER_TYPE(e->type());
            this->GetReportForm()->m_owner=e;
            Some_Left++;
#ifdef _DEBUG
            char buf[512];
            sprintf(buf,"RES_%s_%s        ", e->type(),e->name());
            Set_MALLOC_DATA(buf) ;
#endif

#ifndef _DEBUG
            try {
#endif
                nc+=e->Resolve();

#ifndef _DEBUG
            } //end try
            catch (RXException xe) {
                xe.Print(stdout);
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                return -1;
            }
            catch( char * str ) {
                printf( "Exception raised:  <%s>\n",str);
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                return -1;
            }
            catch( unsigned int ex ) {
                printf( "Exception raised(unsigned): no= %u\n",ex);
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                return -1;
            }
            catch(  int ex ) {
                printf( "Exception raised  (signed): no= %d\n",ex);
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                return -1;
            }
#ifdef MICROSOFT
            catch( CMemoryException* ex )
            {
                wchar_t buf[1024];
                ex->GetErrorMessage(buf,1024);
                wcout<<L" memory excpetion"<< buf << endl;

                cout<< "CMemoryexception while resolving " << e->type() <<e->name() <<endl;
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                ex->Delete();
                return -1;
            }
            catch( CResourceException* ex )
            {
                wchar_t buf[1024];
                ex->GetErrorMessage(buf,1024);
                cout<<"CResourceException"<< buf << endl;

                cout<< "Resource Exception while resolving " << e->type() <<e->name() <<endl;
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                ex->Delete();
                return -1;
            }
            catch( CFileException* ex )
            {
                wchar_t buf[1024];
                ex->GetErrorMessage(buf,1024);
                cout<<"CFileException"<< buf << endl;

                cout<< "CFileexception while resolving " << e->type() <<e->name() <<endl;
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                ex->Delete();
                return -1;
            }

            catch(CException *ex) {
                wchar_t buf[1024];
                ex->GetErrorMessage(buf,1024);
                cout<<"CException"<< buf << endl;

                cout<< "Cexception while resolving " << e->type() <<e->name() <<endl;
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                ex->Delete();
                return -1;
            }
#endif
            catch(...) {
                cout<< "otherexception while resolving " << e->type() <<e->name() <<endl;
                printf(" processed  %d/%d\n", Some_Left,(int)thelist.size ());
                return -1;
            }
#endif
        } // loop over entities
        HC_Update_Display();
        if(nc)Report_Clear(this->GetReportForm());
    } // WHILE nc
    if(Some_Left) this->SortReportForm();

    return(Some_Left);
} // Resolve_All_Entities

// for Werner's sail++
int RXSail::WernerPoints(const int N,  std::vector<RXSitePt> &vv , std::vector<RXSitePt> &cps ,int flags) // cosine distribution

{ //up the leading edge first
    ON_3dPoint x ;
    int nn=0;
    double eps = 0.000;
    double vscale = 0.975; // skip the top 2.5%
    double r,c, rdash,cdash;
    double pmax = 1.0 + 1.0/((double)N)/2.;
    double dc =  (1.00 -2.*eps)  /( (double)N);
    double dr =  (1.00 -2.*eps) /( (double)N);


    if(this->m_uv2xyz)
        this->m_uv2xyz->LocateInitialize(); // oct 2012 not essential but good
    //  qDebug()<<"vertex points " <<QString::fromStdString(this->GetType());
    for (c = eps;c<pmax;c+=dc){
        cdash = c;if (flags&SPP_V_COSINE ) cdash = pow(sin( c * ON_PI/2.0),2);
        for (r = eps;r< pmax;r+=dr)	{
            rdash =r; if (flags&SPP_U_COSINE ) rdash = pow(sin( r * ON_PI/2.0),2);
            x=this->CoordsAt(ON_2dPoint(rdash,vscale*cdash),RX_GLOBAL);// lose 2% of the head as a test
            vv.push_back(    RXSitePt(x.x,-x.y, x.z,r,c, nn++));
        }
    }
    // qDebug()<<" control points";
    for (c =dc/2.+eps/2.;c<1.0;c+=dc){
        cdash = c;if (flags&SPP_V_COSINE ) cdash =  pow(sin( c * ON_PI/2.0),2);
        for (r = dr/2. + eps/2.;r< 1;r+=dr){
            rdash = r; if (flags&SPP_U_COSINE ) rdash = pow(sin( r * ON_PI/2.0),2);
            x=this->CoordsAt(ON_2dPoint(rdash, vscale*cdash),RX_GLOBAL);
            cps.push_back(  RXSitePt(x.x,-x.y, x.z,r,c, nn++));
            // qDebug()<<rdash<<0.95*cdash<<x.x<<x.y<<x.z;
        }
    }
    assert(nn== (N*N + (N+1)*(N+1) )  )  ;
    return nn;
}
