/* interpln.h , prototypes for interpln.c */
#ifndef _INTERPLN_H_
#define  _INTERPLN_H_

class RXSitePt;
#include "griddefs.h"
#include "pctable.h"
#include "nurbs.h"

EXTERN_C int Compute_Interpolation_Card(RXEntity_p e);  

EXTERN_C int Resolve_Interpolation_Card(RXEntity_p e);
EXTERN_C VECTOR SSDPC_Interpolate_By_UV(RXEntity_p e, double u,double v,int *error);
extern   double PC_Interpolate(RXEntity_p gauss, const RXSitePt &p);
extern   double PC_Interpolate(RXEntity_p e, double xx,double yy,double zz);
EXTERN_C int PC_Surface_Normal(RXEntity_p g,double xx,double yy,double zz, ON_3dVector &n);

EXTERN_C int Draw_All_Int_Surfaces(SAIL *sail);
EXTERN_C int Draw_Interpolation_Surface(RXEntity_p e);
EXTERN_C int Sample_Enough_Surface_Values(const RXSitePt &e1b,const RXSitePt &e2b,double e1w,double e2w,RXEntity_p g,double **Ks,double**Sb,int *nks);

#define PCI_UNDEF ((float)-12345678.9)


struct PC_INTERPOLATION {
 // struct Edge * data; /* edgelist start */
  void*data;
  int nr,nc;
  double**x, **y,**z;
  char type[2]; /* either XY or UV or FN or NU*/
  RXEntity_p refSC;
  double Scale_Power;

  //Site **slist;
  int nsites;

 int NurbFlag;
 struct PC_NURB *NurbData;  /* Why not use the 'data' pointer? */
};

#endif  //#ifndef _INTERPLN_H_


