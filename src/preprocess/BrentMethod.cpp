// BrentMethod.cpp: implementation of the BrentMethod class.
//
//////////////////////////////////////////////////////////////////////
 #include "StdAfx.h"

 

#include "opennurbs_defines.h"
#include "BrentMethod.h"

#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

BrentMethod::BrentMethod()
{
	m_ITMAX=100;
	m_EPS=1.0e-10;
	m_tol=0.0001;
	m_iter=0;
	m_xmax=0;
	m_xmin=0;
	m_ErrorFlag=0;
	m_result=0;
	 m_FuncCount=0;

}

BrentMethod::~BrentMethod()
{

}

int	BrentMethod::Bracket(double p_seed,double &xmin, double & xmax)
{ 
		xmin=p_seed-10; xmax=p_seed +10; 
		m_xmin=xmin; m_xmax=xmax;
		return 1;
}
int BrentMethod::zpbrent( const double x1, const double x2 )
{


//	int iter;
	double a=x1,b=x2,c=x2,d=ON_UNSET_VALUE,e =ON_UNSET_VALUE,min1,min2;
	double fa=Func(a);
	double fb=Func(b);
	double fc,p,q,r,s,tol1,xm;
	m_ErrorFlag=0;
	if ((fa > 0.0 && fb > 0.0) || (fa < 0.0 && fb < 0.0)) {
		m_ErrorFlag=m_ErrorFlag|BRENT_ROOT_NOT_BRACKETED;
		return 0;
	}
	fc=fb;
	for (m_iter=0;m_iter<m_ITMAX;m_iter++) {
		if ((fb > 0.0 && fc > 0.0) || (fb < 0.0 && fc < 0.0)) {
			c=a;
			fc=fa;
			e=d=b-a;
		}
		if (fabs(fc) < fabs(fb)) {
			a=b;
			b=c;
			c=a;
			fa=fb;
			fb=fc;
			fc=fa;
		}
		tol1=2.0*m_EPS*fabs(b)+0.5*m_tol;
		xm=0.5*(c-b);
		if (fabs(xm) <= tol1 || fb == 0.0) 
		{m_result=b; 
		return 1;
		}
		if (fabs(e) >= tol1 && fabs(fa) > fabs(fb)) {
			s=fb/fa;
			if (a == c) {
				p=2.0*xm*s;
				q=1.0-s;
			} else {
				q=fa/fc;
				r=fb/fc;
				p=s*(2.0*xm*q*(q-r)-(b-a)*(r-1.0));
				q=(q-1.0)*(r-1.0)*(s-1.0);
			}
			if (p > 0.0) q = -q;
			p=fabs(p);
			min1=3.0*xm*q-fabs(tol1*q);
			min2=fabs(e*q);
			if (2.0*p < (min1 < min2 ? min1 : min2)) {
				e=d;
				d=p/q;
			} else {
				d=xm;
				e=d;
			}
		} else {
			d=xm;
			e=d;
		}
		a=b;
		fa=fb;
		if (fabs(d) > tol1)
			b += d;
		else
			b += SIGN(tol1,xm);
			fb=Func(b);
	}
	m_ErrorFlag =m_ErrorFlag|BRENT_NOT_CONVERGED;
	return 0;

}
double BrentMethod::GetResult() const
{
		return m_result;
}
int BrentMethod::ReportError()const
{
		return m_ErrorFlag;
}
int BrentMethod::ReportFunctionCount()const
{
		return m_FuncCount;
}
int BrentMethod::ReportIterationCount()const
{
		return m_iter;
}

double BrentMethod::Func(const double x)  //  override this method
{
	 m_FuncCount++;
    return max(2.*x-3./7.,1.0e-6);
}
int BrentMethod::DBG_Listing(FILE *fp)
{
	int k;
	double t, n = 10;
	k=fprintf(fp," Brent evaluation \n  x\t  y\n");
	for(t=m_xmin;t<=m_xmax; t+= (m_xmax-m_xmin)/n)
	{
		k+=fprintf(fp,"%lf\t%lf\n",t,Func(t));
	}
	return k;
}
#ifndef NEVER
int BrentTest() {



/*	2)	Bracket the root
3)	Find the root by Brent
4)	Extract the results.
5)	Report errors
*/
	double xmin,xmax;
	BrentMethod mybrent = BrentMethod();
	mybrent.Bracket(0.0,xmin,xmax);
	mybrent.zpbrent(xmin,xmax);
	printf(" Brent result = %f\n",mybrent.GetResult());
	printf(" Brent flag = %d\n",mybrent.ReportError());
	printf(" Brent Function count = %d\n",mybrent.ReportFunctionCount());
	printf(" Brent Last Iteration count = %d\n",mybrent.ReportIterationCount());

return 1;
}



#endif
