#ifndef _PANOUT_H_
#define _PANOUT_H_ 

#ifndef USE_PANSAIL
#error("dont compile panout.h")
#endif
EXTERN_C HC_KEY  Insert_Pansail_Wake(int pMod,int has_foot_wake) ;
EXTERN_C int Panel_Colour_Mesh(HC_KEY key,int _n,int _M, float *vc,int quant);
EXTERN_C int Colour_Mesh(HC_KEY key,int Nin,int Min, float *vc,int quant);
EXTERN_C HC_KEY Insert_Pansail_Model(int _N,int _M, float xv[] ,float yv[],float zv[]);

#endif  //#ifndef _PANOUT_H_




