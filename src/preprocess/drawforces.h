#ifndef _DRAWFORCES_H_
#define  _DRAWFORCES_H_


EXTERN_C int PDF_Draw_One_Force(float fx,float fy,float fz, float mx,float my,float mz,float R,const char*t) ;

EXTERN_C int PDF_Draw_Arrow(VECTOR x0,VECTOR x1,const char*t);
EXTERN_C int PDF_Draw_All_Forces() ;

#endif  //#ifndef _DRAWFORCES_H_


