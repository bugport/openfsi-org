// RXCrossing2.cpp: implementation of the RXCrossing2 class.
//
//////////////////////////////////////////////////////////////////////


#include "StdAfx.h"


#include "RXCrossing2.h"

#ifdef _DEBUG_NEVER
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif




RXCrossing2::RXCrossing2()
{
	Init();
}//RXCrossing2::RXCrossing2

//Copy constructor
RXCrossing2::RXCrossing2(const RXCrossing2 & p_Obj)
{
	Init();
	this->operator =(p_Obj);
}//RXCrossing2::RXCrossing2(double p_x,double p_y,double p_z)

RXCrossing2::~RXCrossing2()
{
	Clear();
}//RXCrossing2::~RXCrossing2

void RXCrossing2::Init()
//meme alloc + init val to 0 0 0
{
	m_Point1.Init();  
	m_Point2.Init();
	
	m_j1=m_j2=-1;
}//void RXCrossing2::Init

int RXCrossing2::Clear()
{
	m_Point1.Clear();
	m_Point2.Clear();

	m_j1=m_j2=-1;
	return(1);
}//int RXCrossing2::Clear

RXCrossing2& RXCrossing2::operator = (const RXCrossing2 & p_Obj)
{
	//Make a copy
	Clear();

	m_Point1 = p_Obj.m_Point1;
	m_Point2 = p_Obj.m_Point2;
	m_j1 = p_Obj.m_j1;
	m_j2 = p_Obj.m_j2;	
	return *this;
}//void RXCrossing2::operator =

int RXCrossing2::SetPoint1(const RXUPoint & p_pt)
{
	m_Point1 = p_pt;
	return 1;
}//int RXCrossing2::SetPoint1

int RXCrossing2::SetPoint2(const RXUPoint & p_pt)
{
	m_Point2 = p_pt;
	return 1;
}//int RXCrossing2::SetPoint2

int RXCrossing2::SetJ1(const int j)
{
	assert("Do not use SetJ1 please, thomas 23 NOV 04"==0);
	m_j1 = j;
	return 1;
}//int RXCrossing2::SetJ1

int RXCrossing2::SetJ2(const int j)
{
	assert("Do not use SetJ2 please, thomas 23 NOV 04"==0);
	m_j2 = j;
	return 1;
}//int RXCrossing2::SetJ2

/*
int RXCrossing2::GetJ1(void) const
{
	double l_t = m_Point1.Gett();
	int l_j = 0; 
	
	assert((l_t>=0.0)&&(l_t<=3.0));
	if (l_t<=1)
		l_j = 0; 
	else
		if (l_t<=2)
			l_j = 1; 
		else
			l_j = 2; 
	return l_j;
	//before thomas 23 NOV 04 it was :  return m_j1; 
}//int RXCrossing2::GetJ1
*/
/*
int RXCrossing2::GetJ2(void) const
{
	double l_t = m_Point2.Gett();
	int l_j = 0; 
	
	assert((l_t>=0.0)&&(l_t<=3.0));
	if (l_t<=1)
		l_j = 0; 
	else
		if (l_t<=2)
			l_j = 1; 
		else
			l_j = 2; 
	return l_j;
	//before thomas 23 NOV 04 it was :  return	m_j2 ;
}//int RXCrossing2::GetJ2
*/
ON_3dPoint RXCrossing2::EvalPt(const int &p_ID) const
{
	assert((p_ID==0)||(p_ID==1));
	ON_3dPoint l_pt(0.0,0.0,0.0);
	
	if(p_ID==0)
		l_pt = m_Point1.GetPos();
	if(p_ID==1)
		l_pt = m_Point2.GetPos();
	return l_pt;
}//ON_3dPoint RXCrossing2::EvalPt

double RXCrossing2::Gett(const int &p_ID) const
{
	assert((p_ID==0)||(p_ID==1));
	double l_t=0.0;
	if(p_ID==0)
		l_t = m_Point1.Gett();
	if(p_ID==1)
		l_t = m_Point2.Gett();
	return l_t;
}//double RXCrossing2::Gett

RXUPoint * RXCrossing2::GetPoint1()
{
	return &m_Point1;
}//RXUPoint * RXCrossing2::GetPoint1

RXUPoint * RXCrossing2::GetPoint2()
{
	return &m_Point2;
}//RXUPoint * RXCrossing2::GetPoint2

RXUPoint * RXCrossing2::GetPoint(const int & p_ID)
{
	assert((p_ID>-1)&&(p_ID<2));
	if (p_ID==0)
		return &m_Point1;
	else
		return &m_Point2;
}//RXUPoint * RXCrossing2::GetPoint

double RXCrossing2::Distance() const
{
//	returns the distance between the 2 points
	ON_3dPoint l_pt1 = EvalPt(0);
	ON_3dPoint l_pt2 = EvalPt(1);
	double l_dist = l_pt1.DistanceTo(l_pt2);
	return l_dist;
}//double RXCrossing2::Distance

double RXCrossing2::GetCrvlength(const int &p_ID,double fractional_tolerance) const
{
	assert((p_ID==0)||(p_ID==1));
	double l_len = 0.0;
	if(p_ID==0)
		return m_Point1.GetLength(fractional_tolerance);
	if(p_ID==1)
		return m_Point2.GetLength(fractional_tolerance);
	return 0.0;
}//double RXCrossing2::GetCrvlength

int RXCrossing2::Print(FILE *fp)  const
{
	assert(fp);
	assert((FILE*)fp);
	fprintf(fp," point 1 \t");	m_Point1.Print(fp);
	fprintf(fp," point 2 \t");	m_Point2.Print(fp);
	return 1;
}

