
/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 Sept 2002.  Merged MSW and LNX versions.

 Jan 98   Make Fanned now does one extra after finishing
  GetGaussSeam returns the algebraic endangles.
 Compute_SC sets NeedsComputing on all its IA.s regardless.
Oct 97 uninitialise problem in COmpute deflections of relsites
 Aug 1997 a detail. Change comment from O P T I M I S E
July 1997 Recursive Solve may be made Nonrecursive by setting
g_Gauss_Recursion to 0

July 1997 Get Position now works for a Relsite on side 0 or 2
 It does it by recurring into Get Position with the curve and offset

June 1997  Evaluate Quantity used to return (uninitialised)if it couldnt read the string. 
  Nov 96   Hoops segments organised a bit into ?sail/<type>/<name> to make PC_Value_Of friendly
 Many small changes to get batpatches to work on moulds
 Now that SITES can be polar ffset from other sites, we had to move COmpute_Site out of
 Resolve_Site into Finish_Site.
  End Oct 96. SCs now work on a mould. GEODESICS too
  This is a bit slow for non-moulded sails because we use Displace_Curve to place the red-space lines
  But it does work for moulded sails with re-cuts!!
  A small inaccuracy was discovered: the endangles werent updated always
 * 2.10.96 a bug in Compute_Relsite. It relies on InitAuto and that wrong
   else if(ns > 1) fixes it
  *  2.9.96 in Compute_Relsite  we now calculate any goalseek or edit moves
     before the length-control moves. This made it necessary to re-compute
   the LC SC's	before measuring their length

   The effect should be that goal-seek and LC can co-exist on a SITE

 * 7.7.96 FUNCTIONAL CHANGE.
 *  the DEPTH of a gausscurve SC is factored by (1+depth).
 * previously, the depth field didnt matter here.
 *  this is flagged by YasX=2
Now it does. To get the same results as before, it should be set to 0
 *   as a result of the below lets try loosening the site solve tol t 1.0mm
  *   BUG FIX.  End angle comparison with  ANGLE TOL  scaled wrongly.
  This tends to stop shape feeding from panel seams to joinging
  seams, especially in larger sails.
  ALSO, endangle was always the penultimate result.
  which is wrong and can lead to a fan not kicking off

 * 10/5/96  R_Solve is NOT RECURSIVE. Paneller sets N e ds comput OFF on new panels
     27/3/96 Psides created with Needs _Computing zero
 *    	21/3/96 R ecursive solver depth limited to 2000
 *	6/3/96 first shot at goalseek.
 *	   Compute_Compound curve is now inside R SOlve as well as after.
 *   Definition of PC_Dist changed to (V*,V*) for uniformity
 *    27/2/96  Update_Curve called from COmpute_SC. So GetPosition, etc could call the
 *	 CURVE routine, rather than the polyline one. This saves calculating ds all the time
 *
 *    15/2/96 Evaluate_Quantity now accepts <units>[<seamname>[<model>]]
 *			with separators	   ",()[]:;/{}"
 * 	 BUT BEWARE. the seam is not be computed at Resolve time, and so
  *		this routine should be called during Geosolve for correct action
  *
 *   13/2/96 Zero_Panel_Count now not called. RELIES ON initialising
 *  5-12 december 1995. Gcurve integration added, mainly to COmpute SC
 * 20/10 Valid Hoops Color routine added. Uses hoops error mechanism to check
 if a string is OK as a color definition
 
 *   28/7/95 a note on Sort_One_Node
 *    This is called by Break Curve. It is triggered on all the nodes on a SC (including
 *		ends) IF the pside list changes
  *		If an ending-on SC has not been  Breaked, it wont have psides
 *		 SO the nodes pslists will be finished when IT is breaked

 
 
 *    25/7/95  SCs may have NULL basecurve. If they come from a mould method
 *  10/7/95 entities now by INTGER TYPE faster. Also panel count now external. Defined in entities.c
 *   and zeroed in preproc.c
 * 28/5/95  Compute SC now doesnt interrogate attributes each call.
 *			faster BUT any change to SC must be followed by Resolve SC
 
 18/5/95   Evaluate_Quantity changed to NOT call error..2 errors
 13/5/95   gauss call changed. Also seam allowance is drawn
 14/3/95 Gauss call added to Compute Seamcurve
 *   16/1/95  Make=Fanned was wr ong for SEAMS
 *   15/1/95		Compute_SC return value was incorrect for curves
 *    12/1/95        Xabs and Length-controlled allowed. So a mast may use library basecurves
 *   9/12/94	    (in Geo solve)
 panels adjacent to a site which is moved
 have their Geometry Has_Changed flag set
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */




/*   date:    6/6/94  sc->arcs enabled. THese are the 3 polyline arc lengths of the seamcurve.
     They are set in Compute_ Seamcurve and in TPN.
     They are used primarily by Get Position, to save it calling Polyline_Length every time
     
     Friday  18/2/1994  	 PETER   */
/* ADAM  10/1/94
   DONE
   A) Get Position to work on a curve. Then use it in RelSITE
   B) Determination of left/right
   C) Calculate a seam.
   D) Get Position to work on a seam. needs left and right
   C) keep offsets as strings (eg 50% or 3.2m) not as float + att
   E) Put imposed angles in the right places.
   F) Find dep s
   G1) allowing for IMPOSED_ANGLES in the Curve calc
   G2) Allowing for IMPOSED_ANGLES in the seam calc.
   I) the solver; calls Compute SeamCurve and Get Pos in the right order
   (19/10/93 works, but needs a convergence criterion_
   *  seamcurves may be Sketch (= the p neller ignores them) else "draw"
   I   The panel-ler
   *  SEAMS. middle points are linearly placed between end side 0's
   L) No fixed arrays (string_To_Polyline, basecurves )
   *   Sort for IMPOSED_ANGLES to get coincident abutting seams
   in the correct order
   O) Dead-ends. Convert zero-e{1,2}{l,r} psides to wrap.
   DONE in Finish_Psides
   *
   N) Zero-length Psides. Break_Curve ignores them.
   *   SITE and RELSITE types coalesced.
   *  PSITES to have node numbers.
   * now doesnt panel the perimeter
   * read_TPN
   18/2/94
   * Panel_Solve re-named Geo_Solve
   * debug re-named g_Peter_Debug
   * Recursive Solve.	 responds to Computes returning 1 if they moved anything
   This routine is to be called after any editing or reading of seams or sites
   * Compute Seamcurve return value 1  UNLESS
   Endangles have changed by less than g_ANGLE_TOL. AND there are no dependant (SITES,RELSITES)
   Else 0
   SO if there ARE any dependant relsites, we let them test for whether the curves have moved
   * Seamcurve structure has RXEntity_p owner and  RXEntity_p **pslist. int Npss
   and in Make Pside Request
   * Pside structure had end1off,end2off
   and flags 	e2of_malloc, e1of_malloc  which are 1 of the offsets may be RXFREEd
   *	SeamCurve_Init. set Make Pside Request = 1.
   pslist = NULL; npss = 0;
   * Delete_Pside Deletes Both SidePanels, and sets SidePanel ptrs of the other psides to NULL
   Also removes the reference to itself in the owning seamcurve pslist
   * PSIDES have end1off,end2off NOT polyline pointers.
   * TPN.C, entities.c broken out, to keep source files manageable.
   * Compute Seamcurve. IF the number or order of relSITES has altered,
   It Deletes all the Psides on it and sets 'Make Pside Request' in the seamcurve structure
   * Make_Psides responds to Make Pside Request flag in the seamcurve.
   If this is set, it deletes any existing Psides and re-generates a set
   Returns Number changed
   
   
   NEXT;
   * investigate why Print All_E fails if a seamcurve has not been SOLVED.
   
   * Delete_Relsite Only if no children.
   *				This is meant to be called when a batten or panel boundary is removed
   The reason for (if no children) is that maybe since the patch was inserted
   another seamcurve was defined as ending on it.
   BEWARE. Pside end<n>offs point into the relsite, so a relsite can only be deleted if
   all the psides on the curve owning the relsite have already been deleted.
   
   * crosses
   * coincident lines. Decision.
   * A batpatch is a special curve.
   * On reading, intersections with all other curves (black-space) are found
   * Relsites are inserted there. Their position is governed by their offset on the
   * underlying curve.
   * seamcurves are inserted between these. A list is maintained in the batpatch, for deletion.
   (also a list of nodes)
   * patch structure includes a list of panels (WHY?)
   *	 Materials are attached to curves. each section (between offsets) has a left and right
   * material list.
   * the offsets move with the relsites they concide with.
   * Pside. Remove the poly, once mesh wotks on offsets instead. (get_Pside_Polygon)
   * Read_Patches. 2 types.
   1) 2 pts and a basecurve (like current seamcurves)
   2) 2 pts defining an axis system
   * Analysis data to be owned by entities, to enable selective changes.
   * Make Put_SNODES and Mesh sensitive to flags
   
   Once that is done, it should be possible to add/remove battens and patches.
   
   * Interpolation routines.
   
   Q) Gaussian
   H) The flags dealing with units and scaling. Offsets are now strings
   but depths are (float + attributes)
   M  Relsites based on arc lengths of (eg) edges
   N) Curves and seams crossing. Needed for patches and for Mould designs
   M) Zaxis. Determined from fixed nodes.
   *  SeamCUrveInit MALLOCs a string of 50. This is re-alloced by
   Compute Seamcurve but would cause a blow-up if a Basecurve
   had 50 points
   CAUTIONS
   Psides must have different end endities but these may be very close
   (the gridder will screen out very close entities)
   
   
   
   PROCESS
   The Process is as follows:
   1) All geometrical solving is done on SEAMCURVES
   2) Then generate a set of PSIDES.
   
   3) Then generate a set of Panels and their triangles.
   
   4) Now add the battens and patches by calling them Seamcurves and looking for
   intersections of psides and seamcurves.
   Material in in any new  panels made is inherited from owner panels.
   
   
   POSSIBLE EDITS
   1) New material on a panel. Beware the panel may have patches
   over-laid on it.
   
   2) Edited geometry(eg some new seam depths) but panel topology unchanged
   = regenerate triangle edgelengths and material props but not topology
   (This may arise from a new RLX or TPN file, or from a PCDRAW edit)
   
   2B) as (2) but the panel topology changed. Here we have new Psides, and
   we try to re-generate their deflections by linear interpolation
   from the end sites.
   
   3) Change mesh density on a Pside
   = throw away the triangles of the panel either side and re-generate
   
   4) Add a patch or batten after building is complete.
   We only know a patch is a patch because its added last, with keyword PATCH.
   
   When we add a patch, we read a seamcurve and compute it.
   Then we check each  panel to see if any of its Psides cross the new curve.
   If one does, we make a new PsideList for the panel and mark it for
   re-gridding.
   If Two do, we generate two panels from the old one. Both of these
   panels are given the material from the old panel.
   One of them has the new patch material added. WHICH ONE??
   
   adding a batten is just like 2B above.
   
   5)  remove a patch or batten)
   Delete the panels to left and right of each Pside in the
   removed seamcurve.
   Make_Panels, flagging the new panels as new.
   Re-grid all new panels
   
   5) Add a new seam
   
   
   For recovery of displacements, each SNODE contains a displacement
   as well as coordinates. So we can always recover displacements to
   a greater or lesser extent.
   
   MATERIALS
   To define material direction we need user-defined heuristics(" top right")
   
   
   */
/*  #define _PDEBUG_   */

#include "StdAfx.h"
#include "RXSail.h"
#include "RXSeamcurve.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSRelSite.h"
#include "RelaxWorld.h"
#include "f90_to_c.h"
#include "RXOffset.h"
#include "RXPside.h"
#include "RXRelationDefs.h"
#include "RX_FEString.h"
#include "RXQuantity.h"
#include "RXCompoundCurve.h"
#include "global_declarations.h"
#include "griddefs.h"
#include "RXCurve.h"
#include "etypes.h"
#include "polyline.h"

#ifdef linux
#include "../../stringtools/stringtools.h"
#else
#include "..\stringtools\stringtools.h"
#endif

#ifndef _NOT_MSW
#define M_PI  3.141592654
#define _max max
#define _min min
#endif
#include "material.h"
#include "entities.h"
#include "printall.h"

#include "peternew.h"

#include "rxbcurve.h"
#include "dxf.h"	 
#include "gauss.h"
#include "gcurve.h"

#include "interpln.h"

#include "RXExpression.h"
#include "pansin.h"
#include "geodesic.h"
#include "drawing.h"
#include "targets.h"
#include "iangles.h"
#include "RXContactSurface.h"
#include "RXPanel.h"

#include "panel.h"


ON_3dVector RXEntity::Normal()const {

    switch(this->TYPE) {
    case SITE:
    case RELSITE:{
        assert(0); // should inherit the site's method
        Site *q=( Site *) this;
        return q->Normal();
    }
    case PANEL:  {
        assert(0);
    }
    default:
        break;
    }
    return this->Esail->Normal(); // Entity Normal default
}

/**********************************************/
/*Get_Compound_Curve_Element finds the segment of a CC in which offin occurs
 *and creates offout, the corresponding offset in the segment sc
 *It is called in Resolve Relsite (to transfer the site form a CC to the segment)
 *
 *
 **/
sc_ptr  Get_Compound_Curve_Element(const class RXEntity* ptr, RXOffset*offin,RXOffset **offout) {

    /* given offin one entity e, return offout and the seamcurve on which the point occurs */
    double offset, a;
    class RXCompoundCurve * np;
    sc_ptr  sc = NULL;
    int k,rev=0;

    RXSTRING  l_oname,l_exp;
    if(ptr->TYPE != COMPOUND_CURVE) return(NULL);

    np = (class RXCompoundCurve * )ptr;


    if (!np->m_ccNcurves) 		return(NULL);

    offset = (offin->Evaluate( np,1));
    a = 0.0;
    for(k=0;k<np->m_ccNcurves;k++) {
        sc = np->m_ccCurves[k];  if(!sc) return(NULL); // looks WRONG - in any case assymmetric
        a = a + sc->GetArc(1);
        rev=np->m_ccRevflags[k];
        if(a > offset)     break;
    }
    if(!rev) offset = offset - (a- sc->GetArc(1));
    else 	 offset = a - offset;

    offset = offset/sc->GetArc(1)*100.0;
    l_exp = TOSTRING(offset) + L"%";

    l_oname = L"CCEXP_" + TOSTRING(offset);
  //  *offout =dynamic_cast<RXOffset*> ( ptr->AddExpression (new RXOffset(l_oname,l_exp ,sc,0))); // maybe WRONG, why on ptr??
    *offout = new RXOffset(l_oname,l_exp ,sc,0) ;

    return(sc);
}
/***********************************************/
ON_3dPoint RXSeamcurve::Get_End_Position(const int flag){
    /* flag will be either SC_START or SC_END
 if the endNsite exists, copy into v.
 else take the first or last point of the C[1] */
    RXEntity_p e=this;
    ON_3dPoint rv;
    VECTOR v;
    Site *s;

    sc_ptr sc = (sc_ptr  )e;
    if (flag==SC_START) {
        if(sc->End1site)  {
            s = (Site *  )sc->End1site;
            rv = s->ToONPoint();
            rv = sc->End1site->ToONPoint();

        }
        else {
            assert("are you sure the start is p[1]"==0);	memcpy(&v, sc->p[1] ,3*sizeof(float));
            rv = ON_3dPoint( v.x,v.y,v.z);

        }
        return rv;
    }
    else if (flag==SC_END) {
        if(sc->End2site) {
            s =  (Site *  )sc->End2site;
            rv = s->ToONPoint();
        }
        else {
            memcpy(&v, &( sc->p[1][sc->c[1]-1])  ,3*sizeof(float));
            rv = ON_3dPoint( v.x,v.y,v.z);
        }
        return rv;
    }
    assert(0);


    return rv;
}
int Get_Position(const RXEntity_p ptr, RXOffset *offin,const int side,ON_3dPoint *p){
    VECTOR v;
    int rc =  Get_Position(ptr,offin,side,&v);
    *p=ON_3dPoint(v.x,v.y,v.z);
    return rc;
}

/***********************************************/
int Get_Position(const RXEntity_p ptr, RXOffset *offin,const int side,VECTOR *v){

    /* the entity may be a SITE or RELSITE
 or it may be a SC, in which case <offset>  and <side> are needed*/
    /* It may be a seam, in which case <side>= 0,1,or 2 for (left, black, right)
   If its a PSIDE, offset refers to its SC.
   If the SC is a Seam thats fine.
   If the SC is a  Curve, there is a special meaning for sides 0 and 2;
   They are curve 1, but displaced by a weighted mean of the endsites of the pside.
*/

    double OffsetValue;
    assert(ptr);

    if(ptr->Needs_Resolving)
        return(0);

    if(ptr->Needs_Finishing)
        return(0);
    switch (ptr->TYPE) {
    case SEAMCURVE:
        OffsetValue=(offin->Evaluate( (sc_ptr  )ptr,side));
        break;
    case COMPOUND_CURVE:
        OffsetValue=(offin->Evaluate( (sc_ptr  )ptr,side));
        break;
    case PCE_PSIDE:
    {
        PSIDEPTR ps = (PSIDEPTR)ptr;
        OffsetValue=(offin->Evaluate( (sc_ptr  )ps->sc,side)); /* GUESS 1 not side */
        break;
    }
    default:
        OffsetValue=0.0;
    }
    return (Get_Position_By_Double (ptr,OffsetValue, side,v));
}
int Get_Position(const RXEntity_p ptr, RXOffset *offin,const int side,class RXSitePt *pt) {
    /* the entity may be a SITE or RELSITE
 or it may be a SC, in which case <offset>  and <side> are needed*/
    /* It may be a seam, in which case <side>= 0,1,or 2 for (left, black, right)
   If its a PSIDE, offset refers to its SC.
   If the SC is a Seam thats fine.
   If the SC is a  Curve, there is a special meaning for sides 0 and 2;
   They are curve 1, but displaced by a weighted mean of the endsites of the pside.
*/

    double OffsetValue;
    assert(ptr);

    if(ptr->Needs_Resolving)
        return(0);

    if(ptr->Needs_Finishing)
        return(0);
    switch (ptr->TYPE) {
    case SEAMCURVE:
        OffsetValue=(offin->Evaluate( (sc_ptr  )ptr,side));
        break;
    case COMPOUND_CURVE:
        OffsetValue=(offin->Evaluate( (sc_ptr  )ptr,side));
        break;
    case PCE_PSIDE:
    {
        PSIDEPTR ps = (PSIDEPTR )ptr;//->dataptr;
        OffsetValue=(offin->Evaluate( (sc_ptr  )ps->sc,side)); /* GUESS 1 not side */
        break;
    }
    default:
        OffsetValue=0.0;
    }
    return (Get_Position_By_Double (ptr,OffsetValue, side,pt));
}


/***********************************************/

/***********************************************/

int Get_Position_On_Pside(const class RXPside * ps,double offin,int side,ON_3dPoint &pt){
    /* return 1 if OK, else 0 */
    /*
This routine adds a correction if either end of the pside is a CROSS site and the crossing SC
 is a seam.
 */
    RXEntity_p sco, se, sco2=NULL;
    sc_ptr sc;

    Site *s;

    VECTOR red,black;
    int  end, TheSide,ret,iret;
    VECTOR b2r[2];
    double eoff[2],f;
    RXEntity_p EndSCO[2] ;

    sco = ps->sc; sc = (sc_ptr  )sco;

    iret =Get_Position_By_Double(sco,offin,side,pt);
    if(!iret) return 0;

    ret = ps->IsSpecial(EndSCO);
    if(!ret) return(iret);

    /* here  at least one end needs correction  the CURVE[1] of the endsite is a seam*/

    memset(b2r,0,2*sizeof(VECTOR));

    eoff[0] = (ps->End1off->Evaluate(sc,1));
    eoff[1] = (ps->End2off->Evaluate(sc,1));

    for (end=0;end<2;end++) {
        if(EndSCO[end] ){
            sco2=EndSCO[end];

            se = ps->Ep(end);
            s = (Site *)se;

            TheSide =2 * ps->Find_Special_Side(  sco2,se);

            Get_Position(sco2,s->Offsets[1],1,&black);
            Get_Position(sco2,s->Offsets[1],TheSide,&red);
            //Draw_PS_Delta(black,red);

            b2r[end].x = red.x - black.x;
            b2r[end].y = red.y - black.y;
            b2r[end].z = red.z - black.z;

        } /* if endSC */
    }/* for end */

    if(fabs(eoff[1] - eoff[0])<1e-10) f = 0.5;
    else f = (offin-eoff[0])/(eoff[1]-eoff[0]);

    pt.x = pt.x  + b2r[0].x*(float)(1.-f) + b2r[1].x *(float)f;
    pt.y = pt.y + b2r[0].y*(float)(1.-f) + b2r[1].y *(float)f;
    pt.z = pt.z  + b2r[0].z*(float)(1.-f) + b2r[1].z *(float)f;
    return 1;
}

/**************************************/

int Get_Position_By_Double(const class RXEntity* ptr,const double offin,const int side,ON_3dPoint &p){
    VECTOR v;
    int rc = Get_Position_By_Double(ptr,offin, side,&v);
    p.x=v.x; p.y=v.y;p.z=v.z;
    return rc;
}
int Get_Position_By_Double(const class RXEntity* ptr,const double offin,const int p_side,VECTOR *p_v){

    /* the entity may be a SITE or a curve, in which case <offset> is needed*/
    /* It may be a seam, in which case <side>= 0,1,or 2 for (left, black, right) */

    int side = p_side;
    if(ptr->Needs_Resolving)  {
        if(ptr->TYPE==PCE_PSIDE) ptr->OutputToClient(" What about a Pside needing resolving?",2);
        printf(" GPBD on %s %s needs resolving\n", ptr->type(), ptr->name());
        return(0);
    }

    if(ptr->Needs_Finishing) {
        if(ptr->TYPE==PCE_PSIDE)  ptr->OutputToClient(" What about a Pside needing Finishing?",2);
        return(0);
    }
    switch (ptr->TYPE) {
    case SITE:
    {
        Site *np = (Site *)ptr;
        (*p_v).x=(*np).x;
        (*p_v).y=(*np).y;
        (*p_v).z=(*np).z;
        break;
    }
    case RELSITE: {
        Site *np = (Site *)ptr;
        if(side==1) {
            (*p_v).x=(*np).x;
            (*p_v).y=(*np).y;
            (*p_v).z=(*np).z;
        }
        else {
            if(np->CUrve[1] && np->Offsets[1])
                Get_Position(np->CUrve[1],np->Offsets[1],side,p_v);
            else
                Get_Position(np->CUrve[0],np->Offsets[0],side,p_v);
            /* ASSYMMETRICAL */

        }
        break;
    }
    case COMPOUND_CURVE: {
        RXOffset*str;
        RXEntity_p e;
        RXSTRING s;
        sc_ptr  sc= (sc_ptr  )ptr;
        s = TOSTRING(offin); //sprintf(string,"%f",offin);

        RXOffset o(L"GPBDCC" ,s,  sc, 0);
        e = Get_Compound_Curve_Element(ptr, &o,&str);

        Get_Position(e,str,side,p_v);
        assert(str); delete str;
        return(1);
    }
    case SEAMCURVE:
    case PCE_NODECURVE:
    {
        /* Get the seam and go offset along it */
        sc_ptr np = (sc_ptr  )ptr;
        double offset;

        //			int c = np->c[side];
        if(side>2 ||side<0) { printf(" Side is %d\n", side);
            ptr->OutputToClient("(Get_Position) Side out of range",2);
            side=1;
        }

        assert(np->m_pC[side]) ;
        assert(!np->m_pC[side]->GetType());
        offset = offin;
        ON_3dPoint l_v;
        if(0>=np->m_pC[side]->Place_On(offset,&l_v)) {
            char str[512];

            sprintf(str,"Cannot PlaceOn %s %s at %f",ptr->type(),ptr->name(),offset);
            HC_Update_Display(); ptr->OutputToClient(str,3);

        }
        p_v->x=l_v.x; p_v->y=l_v.y; p_v->z=l_v.z;
        break;
    }
    case PCE_PSIDE: {
        ON_3dPoint l_v;
        int iret = Get_Position_On_Pside((const class RXPside*) ptr,offin,side,l_v);
        p_v->x=l_v.x; p_v->y=l_v.y; p_v->z=l_v.z;
        return iret;
    }
    default:  {
        char s[256];
        sprintf(s," Get_Position By Double does not support'%s' '%s'\n",ptr->type(),ptr->name());
        ptr->OutputToClient(s,3);
        (*p_v).x=(float)0.0; (*p_v).y=(float)0.0;(*p_v).z=(float)0.0; return(0);
    }
    }
    return(1);
}

int Get_Position_By_Double(const class RXEntity* ptr,const double offin,const int p_side,class RXSitePt *p_v){

    //   the entity may be a SITE or a curve, in which case <offset> is needed
    //   It may be a seam, in which case <side>= 0,1,or 2 for (left, black, right)
    //In this variant the return value is a RXSitePt.  Where possible the (u,v) values are returned along with the (xyz)

    int side = p_side;
    if(ptr->Needs_Resolving)  {
        if(ptr->TYPE==PCE_PSIDE)  ptr->OutputToClient(" What about a Pside needing resolving?",2);
        printf(" GPBD on %s %s needs resolving\n", ptr->type(), ptr->name());
        return(0);
    }

    if(ptr->Needs_Finishing) {
        if(ptr->TYPE==PCE_PSIDE)  ptr->OutputToClient(" What about a Pside needing Finishing?",2);
        return(0);
    }

    switch (ptr->TYPE) {
    case SITE:
    {
        Site *np = (Site *)ptr;
        (*p_v) =(*np) ;
        break;
    }
    case RELSITE: {
        Site *np = (Site *)ptr;
        if(side==1) {
            (*p_v) =(*np) ;
        }
        else {
            if(np->CUrve[1] && np->Offsets[1])
                Get_Position(np->CUrve[1],np->Offsets[1],side,p_v);
            else
                Get_Position(np->CUrve[0],np->Offsets[0],side,p_v);
            /* ASSYMMETRICAL */

        }
        break;
    }
    case COMPOUND_CURVE: {
        RXOffset*str;
        RXEntity_p e;
        RXSTRING s;
        sc_ptr  sc= (sc_ptr  )ptr;
        s = TOSTRING(offin); //sprintf(string,"%f",offin);

        RXOffset o(L"GPBDCC" ,s,  sc, 0);
        e = Get_Compound_Curve_Element(ptr, &o,&str);

        Get_Position(e,str,side,p_v);
        assert(str); delete str;
        return(1);
    }
    case SEAMCURVE:
    case PCE_NODECURVE:
    {
        /* Get the seam and go offset along it */
        sc_ptr np = (sc_ptr  )ptr;
        double offset;

        //			int c = np->c[side];
        if(side>2 ||side<0) { printf(" Side is %d\n", side);
            ptr->OutputToClient("(Get_Position) Side out of range",2);
            side=1;
        }
        assert(np->m_pC[side]) ;
        assert(!np->m_pC[side]->GetType());
        offset = offin;
        ON_3dPoint l_v;
        if(0>=np->m_pC[side]->Place_On(offset,&l_v)) {
            char str[512];

            sprintf(str,"Cannot PlaceOn %s %s at %f",ptr->type(),ptr->name(),offset);
            HC_Update_Display(); ptr->OutputToClient(str,3);

        }
        *p_v=l_v;
        break;
    }
    case PCE_PSIDE: {
        ON_3dPoint l_v;
        int iret = Get_Position_On_Pside((const class RXPside*)ptr,offin,side,l_v);
        *p_v =l_v ;
        return iret;
    }
    default:  {
        char s[256];
        sprintf(s," Get_Position By Double does not support'%s' '%s'\n",ptr->type(),ptr->name());
        ptr->OutputToClient(s,3);
        (*p_v).x=(float)0.0; (*p_v).y=(float)0.0;(*p_v).z=(float)0.0; return(0);
    }
    }
    return(1);
}
int Compute_Sign(const ON_3dPoint &a, const ON_3dPoint &b, const ON_3dPoint &c, const ON_3dPoint &d,const ON_3dVector &zax)
{
    /* A line goes from a to b. another goes from c( on ab) to d
     This routine returns -1 if a c d is a right turn, and 1 if it
     is a left turn */

    ON_3dVector v1,v2,v3;
    double  z;

    v1 = b-a;
    v2 = d-c;

    v3 = ON_CrossProduct(v1,v2);
    z = ON_DotProduct(v3,zax);
    if(fabs(z) < 0.00001)
        return 0;
    if (z<0) return(-1);
    return(1);
}


int Compute_Entity_Endsign(RXEntity_p p,const ON_3dPoint &e1,const ON_3dPoint &e2){
    // would be FASTER if we used GetStartPt and GetEndpt

    // *p is an end<n>ptr
    ON_3dPoint pt1,pt2;

    /* for each end, we see if its on a relSITE.  If it is, we see what entity
     that relSITE sits on. If it sits on a curve or seam we measure the sign
     4/8/94 we check to see the no of points on the curve. If its zero we revert
     to curve(1) = blackspace
     */

    if (p->TYPE==RELSITE || p->TYPE==SITE) {
        Site *rptr = (Site *)p;
        RXEntity_p cptr = rptr->CUrve[0];
        sc_ptr  sc;
        if(cptr) {
            if (cptr->TYPE==SEAMCURVE) {

                /*  get positions of end1 and end2 of the entity (*cptr) */
                int sign; double arc;
                sc = (sc_ptr  )cptr;
                arc = sc->GetArc(1);
                if(!Get_Position_By_Double(cptr,0.0,1,pt1)|| !Get_Position_By_Double(cptr,arc,1,pt2)){
                    p->OutputToClient(" in Compute_E_E Get_P failed",1);
                    return(0);
                }
                ON_3dVector l_v =p->Normal();
                sign= Compute_Sign( pt1,pt2,e1,e2, l_v);

                //	if(!sc->c[sign+1]) {
                if(! ( sc->m_pC[sign+1]->HasGeometry() ) ) {
                    printf(" reverting to side 1 on %s\n",p->name()); sign=0;}
                return(sign);	/* -1 or 1 */
            }
        }
    }
    else {
        char s[256];
        sprintf(s," seam end is on a %s (not a SITE or relSITE)",p->type() );
        p->OutputToClient(s,1);
        return(0);
    }
    return 0; //arbitrary
}

/*****************************************************************/

int Measure_End_Angles(sc_ptr  p,int seamflag, int Use_Gauss_EA , double e1a,double e2a ) {
    /* the correct logic is to keep the old endangles.
    the error is the difference between the current value and the
 value the last time it was updated.  So we record old angles when we return iret=1
 and if the last move was small we check whether  a change is needed from the last

 Jan 98 the introduction of the relaxation factor.
 We measure the current angles, but in the endNangle fields we store a relaxed value
 This will damp the gauss solver.
 However, with a relfactor we creep exponentially towards the solution.  We should keep
 going so long as the angle passed back differs from the angle measured last time we passed one
 back. This will keep it going until the angles passed back are close to the measured angles.
 Otherwise, a small relaxation factor would stop it early. The movements are down by relfac.


 */
    int bc = p->c[0], l_iret = 0,l_err=0;
    double newangle1, newangle2;
    double l_Atol = p->Esail ->m_Angle_Tol;
    if (seamflag) {
        if(Use_Gauss_EA) {
            newangle1 =e1a;
            newangle2= e2a ;
        }
        else {
            VECTOR a,b;
            PC_Vector_Difference(&((p->p)[0][1]),&(((*p).p)[0][0]),&a);
            PC_Vector_Difference(&((p->p)[2][1]),&(((*p).p)[2][0]),&b);
            newangle1 = PC_Signed_True_Angle(b, a,p->Normal(),&l_err);
            if(l_err) {
                ON_String buf;
                buf.Format("Measure_End_Angles(1) on '%s' failed",p->name());
                p->OutputToClient(buf,1); cout<< " P0 is "<<endl;
                RXCurve::DumpPLine(p->p[0],bc,stdout);
                cout<< " P2 is "<<endl;
                RXCurve::DumpPLine(p->p[2],bc,stdout);
            }
            assert(bc-2>=0);
            PC_Vector_Difference(&(((*p).p)[0][bc-2]),&(((*p).p)[0][bc-1]),&a);
            PC_Vector_Difference(&(((*p).p)[2][bc-2]),&(((*p).p)[2][bc-1]),&b);
            newangle2= PC_Signed_True_Angle(a, b,p->Normal(),&l_err);
            if(l_err) {
                ON_String buf;
                buf.Format("Measure_End_Angles(2) on '%s' failed",p->name());
                p->OutputToClient(buf,1); cout<< " P0 is "<<endl;
                RXCurve::DumpPLine(p->p[0],bc,stdout);
                cout<< " P2 is "<<endl;
                RXCurve::DumpPLine(p->p[2],bc,stdout);
            }
        }

        if (fabs(p->end1angle - newangle1) >l_Atol ) 	l_iret = 1;
        p->end1angle = newangle1 * g_GAUSS_FACTOR + p->end1angle*(1.0-g_GAUSS_FACTOR );

        if (fabs(p->end2angle - newangle2) >l_Atol)  	l_iret = 1;
        p->end2angle = newangle2  * g_GAUSS_FACTOR +   p->end2angle *(1.0-g_GAUSS_FACTOR );
    }
    else {
        p->old_end1angle = p->end1angle=0.0;
        p->old_end2angle = p->end2angle=0.0;
        newangle1 = newangle2 = 0.0;
    }
    if (fabs(	p->old_end1angle - p->end1angle) > l_Atol * g_GAUSS_FACTOR) l_iret=1;
    else if (fabs( p->old_end2angle - p->end2angle) > l_Atol * g_GAUSS_FACTOR)  l_iret=1;

    if(l_iret) { /* record the angle at the last change */
        p->old_end1angle = newangle1; /*  p->end1angle ;*/
        p->old_end2angle = newangle2 ; /* p->end2angle ; */
    }
    return(l_iret);
}


/************************************************************/

int Coalesce_Cranks(float x[],double a[],int *crank,int N){
    /*There are N crank angles A at arclengths X if the flag crank{} is set

 If two cranks are at the same x, we add their A's and set crank to 0 on the second.
This routine serves two purposes
 1) It is a speed optimisation
 2)

*/

    int k, kb;

    for (kb=0;kb<N;kb++) {
        if(crank[kb]){
            for(k=kb+1; k<N;k++) {
                if(crank[k] && (x[k]-x[kb] < FLOAT_TOLERANCE)) {
                    a[kb] = a[kb]+ a[k];
                    crank[k]=0; a[k]=0.;

                } /*  if crank  k and close*/
            } /* for k */


        }/*  if crank  kb*/
    } /* for kb */

    return 1;
}
int Crank_Polyline(VECTOR **p,int *c,float x[],double a[],int *crank,int Nin){

    /* the polyline ((*p),c) is in Metres, so is x. a is radians.
     This routine generates a new polyline which is p bent by
     angles a at distances x along it. X is assumed sorted
     ignore small angles
     IA may contain relsites, not seams. This is flagged by
     */
    int k, ka, N=Nin;
    int kb = 0;

    VECTOR v;
    double cosa,sina, m[3][3] ,drx,dry,drz,px,py,pz;
    double *s  =(double *)MALLOC( (2+Nin+(*c)) * sizeof(double));
    if (s==NULL) { g_World->OutputToClient("out of memory",3); }


    for (kb=0;kb<N;kb++) {
        if(crank[kb]){
            s[0]= 0.0000;
            for (k=1;k<*c;k++) {
                s[k] = s[k-1] + PC_Dist(&((*p)[k-1]),&((*p)[k]));  /** arc lengths of the baseSITEs*/
            }

            for (ka=0;ka<(*c)-1;ka++) {
                if (x[kb]>= FLOAT_TOLERANCE && x[kb]>=s[ka]-FLOAT_TOLERANCE && x[kb]<(s[ka+1]-FLOAT_TOLERANCE)) {  /* x is between k and k+1 */

                    if (FLOAT_TOLERANCE < fabs(x[kb]-s[ka])){
                        Place_On_Poly(x[kb],*p,*c,&v);
                        if(g_TRACE){
                            printf(" inserting at %f, v is %f %f %f\n",x[kb],v.x,v.y,v.z);
                            PolyPrint("before insert \n", *p,*c);
                        }
                        Insert_Into_Poly(p,c,v,ka); ka++;	 /* does the re-alloc */
                        if(g_TRACE){ PolyPrint("after insert \n", *p,*c);}
                    }
                    else{
#ifdef _PDEBUG_
                        /*	    printf(" duplicate at x=%f, a=%f so no insert\n",x[kb],a[kb]); */
#endif
                    }

                    /* make transformation */

                    cosa =  cos(a[kb]);
                    sina =  sin(a[kb]);
                    m[0][0] = cosa; 	 m[0][1] = -sina; 		m[0][2]=0.0;
                    m[1][0] = sina;		 m[1][1] =  cosa; 		m[1][2]=0.0;
                    m[2][0] =  0.0;      m[2][1] =   0.0;    	m[2][2]=1.0;

                    /* transform the remainder of p. Pivot is p[ka] */
                    px= (double) (*p)[ka].x;
                    py= (double) (*p)[ka].y;
                    pz= (double) (*p)[ka].z;

                    for (k=ka+1;k<*c;k++) {
                        drx= (double)(*p)[k].x - px;
                        dry= (double)(*p)[k].y - py;
                        drz= (double)(*p)[k].z - pz;
                        (*p)[k].x =(float) (px + m[0][0]* drx + m[0][1]*dry);/* + m[0][2]*drz);*/
                        (*p)[k].y =(float) (py + m[1][0]* drx + m[1][1]*dry);/* + m[1][2]*drz);*/
                        (*p)[k].z =(float) (pz + drz);
                        /*  (*p)[k].z =(float) ((double)(*p)[ka].z + m[2][0]* drx + m[2][1]*dry + m[2][2]* drz);*/
                    }
                }
            }
        }
    }
    if(g_TRACE){
        PolyPrint("leaving Crank\n", *p,*c);}
    RXFREE(s);
    return(1);
}

int Make_Fanned_Experimental  (sc_ptr  sc,int flag,float *p_arc, double *a0,float chord, int *c, VECTOR **p) {
    // As the origianl Make_Fanned except that we take the start and end point Y as 0 rather than the basecurve's yvalue.

    // May 2009 it's not quite there. We should find the'y=0' end points taking into account the rotation at the curve's start and end
    // and anyway, at some later stage the redcurve end points get shifted to the blackcurve end points.
    //
    /* INPUT  struct IMPOSED_ANGLE*ia,int Nin
   *arc is an estimate of the arc length
   chord is the straight-line distance between the end points in m
   we are trying to find a polyline of the same chordlength
   *a0 is an estimate of the initial slope of the curve
   *c points in <p> are the fanned offsets ( X range 0 to 1, Y range in m)
   there are N imposed angles in ia
   flag may be "flat", "seam" or "curve".
   If it is "flat" we insert the points but we do not do any cranking
   If it is "seam" we insert the points and crank half the angle.
   If it is "flat" we insert the points and crank the whole angle
   
   
   RETURNS
   *arc, *a0 get updated.
   in (*c,p) we put the new curve. It is the correct length but needs
   to be placed between endpoints.
   ia.i = the indices of the crank points get updated.
   */


    /* ) evaluate the angles and sort. This is necessary because the angle offsets
     may be given in m or %, and so some may overtake others as the arc-length
     changes.*/

    int YasX,k,ko, loops=0;//,Yabs
    double New_Chord;
    double nominal_Length,yf, temp;
    ON_3dPoint start,end;
    int KeepGoing=0;
    double errA, dy, errX, dx;
    double tx,ty, norx,nory ,norz;//tz
    int *crank;
    float *x   ;
    double *a;
    VECTOR *pout;

    crank   = (int*)MALLOC(sizeof(int)*(sc->n_angles+1) );
    x     = (float *)MALLOC(sizeof(float)*(sc->n_angles+1) );
    a     = (double *)MALLOC(sizeof(double)*(sc->n_angles+1) );
    pout = (VECTOR *)MALLOC((2+sc->n_angles+(*c)) *sizeof(VECTOR));
    if(pout == NULL )  g_World->OutputToClient("out of mem in panel",3);

    nominal_Length= *p_arc;

    YasX=sc->YasX;


    do   // loop until arc-length stops changing -no do one more than that)
    {

        tx = cos(*a0); ty = sin(*a0);// tz=0.0;
        norx = -ty;       nory = tx;        norz=0.0;
        if(YasX) yf = nominal_Length;
        else     yf = 1.0;
        // generate pout which is p_p rotated about the origin and stretched
        for (k=0;k<*c;k++) {
            double pkx=	(*p)[k].x;
            double pky=	(*p)[k].y;
            pout[k].x = (float) (tx* pkx* nominal_Length + norx * pky * yf);
            pout[k].y = (float) (ty* pkx* nominal_Length + nory * pky * yf);
            pout[k].z = (float) 0.0; /*tz* (*p)[k].x* nominal_Length + norz * (*p)[k].y * yf;*/
        }
        start =ON_3dPoint(pout[0].x   , 0.0  ,pout[0].z  );
        end =ON_3dPoint(pout[(*c)-1].x,  0.0,  pout[(*c)-1].z    );
        temp = PC_Polyline_Length(*c,pout);
        *p_arc = (float) temp;
        if(temp< sc ->Esail ->m_Linear_Tol ){  *p_arc = chord;cout<< "another nasty fix-up for geodesics"<<endl;}
        /* because this points to p->arcs[0] its OK to use EValuate Offset */

        Sort_IA(sc->ia,sc->n_angles);

        for (k=0;k<sc->n_angles;k++) {
            crank[k] = !(sc->ia[k].s->TYPE==RELSITE);
            if(sc->ia[k].s->TYPE==SEAMCURVE) {
                sc_ptr  scp =(sc_ptr  )sc->ia[k].s;
                if(!(scp->FlagQuery(CUT_FLAG)) )	{
                    if(!scp->seam) crank[k] = 0;
                }
            }
            if(sc->seam)
                x[k] = sc->ia[k].m_Offset->Evaluate( sc,0);/* was 1 16/1/95 PH */ /*(*arc)));*/
            else
                x[k] =  sc->ia[k].m_Offset->Evaluate( sc,1);
            if (flag ==CURVEFLAG && sc->ia[k].Angle && (sc->ia[k].sign ) )
                a[k] =  *(sc->ia[k].Angle) *  *(sc->ia[k].sign);
            else if (flag==SEAMFLAG && sc->ia[k].Angle )
                a[k] = -0.5 * *sc->ia[k].Angle;
            else a[k] = 0.0;

            if (flag == FLATFLAG) a[k]=0.0;
        }

        ko = *c;
        Coalesce_Cranks(x,a,crank,sc->n_angles);
        Crank_Polyline(&pout,&ko,x,a,crank,sc->n_angles);  /* pout is now MALLOCed to the right size */
        /* May 2000 pout now has the extra vertices at non-relsite IAs and has been cranked
  It is no longer the right chord length or orientation */
        start =ON_3dPoint(pout[0].x   , 0.0  ,pout[0].z  );
        end =ON_3dPoint(pout[(*c)-1].x, pout[(*c)-1].y,  pout[(*c)-1].z    );

        New_Chord = start.DistanceTo (end); //  (float) PC_Dist(&(pout[0]),&(pout[ko-1]));

        if(New_Chord > FLOAT_TOLERANCE) {
            errA = (float)-atan2(end.y-start.y,end.x-start.x);
        }
        else errA=(float)0.0;
        errX = chord-New_Chord;

        dx = errX;
        dy = errA;

        *a0=*a0 + dy;
        nominal_Length = nominal_Length + dx;
        if (fabs(errX)>sc->Esail ->m_Linear_Tol||
                fabs(New_Chord*errA)>sc ->Esail ->m_Linear_Tol) KeepGoing=2;
        else KeepGoing = KeepGoing -1;

    } while (KeepGoing>0 && (100>loops++));

    if(loops>100) {
        printf("%s CRANK didnt solve in %d loops dx %f dy %f  (TOL=%f)\n",sc->name(),loops,dx,dy,sc->Esail->m_Linear_Tol);
    }

    *p_arc =(float) PC_Polyline_Length(ko,pout);

    *c = ko;
    *p = (VECTOR *)REALLOC (*p,sizeof(VECTOR)*(ko+1));
    for (k=0;k<ko;k++) {
        (*p)[k].x = pout[k].x;
        (*p)[k].y = pout[k].y;
        (*p)[k].z=  pout[k].z;
    }
    /*      At end, measure chord and chordangle
   Increment initial angle
   Increment ref length LOOP */

    /*      pass back the arc length and initial angle ready for use next time
   and in p the finished polyline
   maybe we store the nominal_Length value to speed up the routine next time*/

    RXFREE(x); RXFREE(a); RXFREE(crank); RXFREE(pout);
    return(1);
}
int Make_Fanned_(sc_ptr  sc,int flag,float *arc, double *a0,float chord, int *c, VECTOR **p) {

    /* INPUT  struct IMPOSED_ANGLE*ia,int Nin
   *arc is an estimate of the arc length
   chord is the straight-line distance between the end points in m
   we are trying to find a polyline of the same chordlength
   *a0 is an estimate of the initial slope of the curve
   *c points in <p> are the fanned offsets ( X range 0 to 1, Y range in m)
   there are N imposed angles in ia
   flag may be "flat", "seam" or "curve".
   If it is "flat" we insert the points but we do not do any cranking
   If it is "seam" we insert the points and crank half the angle.
   If it is "flat" we insert the points and crank the whole angle
   
   
   RETURNS
   *arc, *a0 get updated.
   in (*c,p) we put the new curve. It is the correct length but needs
   to be placed between endpoints.
   ia.i = the indices of the crank points get updated.
   */


    /* ) evaluate the angles and sort. This is necessary because the angle offsets
     may be given in m or %, and so some may overtake others as the arc-length
     changes.*/

    int YasX,k,ko, loops=0;//Yabs,
    double New_Chord;
    double nominal_Length,yf, temp;

    int KeepGoing=0;
    double errA, dy;
    double errX, dx;

    int *crank;
    float *x   ;
    double *a;
    VECTOR *pout;

    //if(sc->Geo_Or_UV) return 0; //removed jul 28 2001 for fanned curves on moulds

    crank   = (int*)MALLOC(sizeof(int)*(sc->n_angles+1) );
    x     = (float *)MALLOC(sizeof(float)*(sc->n_angles+1) );
    a     = (double *)MALLOC(sizeof(double)*(sc->n_angles+1) );
    pout = (VECTOR *)MALLOC((2+sc->n_angles+(*c)) *sizeof(VECTOR));
    if(pout == NULL )  g_World->OutputToClient("out of mem in panel",3);

    nominal_Length= *arc;

    YasX=sc->YasX;


    do              /* loop until arc-length stops changing -no do one more than */
    {
        double tx,ty,norx,nory;// ;,norz;,tz
        tx = cos(*a0); ty = sin(*a0);/// tz=0.0;
        norx = -ty;       nory = tx;   //     norz=0.0;
        if(YasX) yf = nominal_Length;
        else     yf = 1.0;

        for (k=0;k<*c;k++) {
            double pkx=	(*p)[k].x;
            double pky=	(*p)[k].y;
            pout[k].x = (float) (tx* pkx* nominal_Length + norx * pky * yf);
            pout[k].y = (float) (ty* pkx* nominal_Length + nory * pky * yf);
            pout[k].z = (float) 0.0; /*tz* (*p)[k].x* nominal_Length + norz * (*p)[k].y * yf;*/
        }
        temp = PC_Polyline_Length(*c,pout);
        *arc = (float) temp;
        if(temp< sc ->Esail ->m_Linear_Tol ) *arc = chord; /* a nasty fix-up for geodesics */
        /* because this points to p->arcs[0] its OK to use EValuate Offset */

        Sort_IA(sc->ia,sc->n_angles);

        for (k=0;k<sc->n_angles;k++) {
            crank[k] = !(sc->ia[k].s->TYPE==RELSITE);
            if(sc->ia[k].s->TYPE==SEAMCURVE) {
                sc_ptr  scp =(sc_ptr  )sc->ia[k].s;
                if(!(scp->FlagQuery(CUT_FLAG)) )	{
                    if(!scp->seam) crank[k] = 0;
                }
            }
            if(sc->seam)
                x[k] = sc->ia[k].m_Offset->Evaluate( sc,0);/* was 1 16/1/95 PH */ /*(*arc)));*/
            else
                x[k] = sc->ia[k].m_Offset->Evaluate( sc,1);
            if (flag ==CURVEFLAG && sc->ia[k].Angle && (sc->ia[k].sign ) )
                a[k] =  *(sc->ia[k].Angle) *  *(sc->ia[k].sign);
            else if (flag==SEAMFLAG && sc->ia[k].Angle )
                a[k] = -0.5 * *sc->ia[k].Angle;
            else a[k] = 0.0;

            if (flag == FLATFLAG) a[k]=0.0;
        }

        ko = *c;
        Coalesce_Cranks(x,a,crank,sc->n_angles);

        Crank_Polyline(&pout,&ko,x,a,crank,sc->n_angles);  /* pout is now MALLOCed to the right size */
        /* May 2000 pout now has the extra vertices at non-relsite IAs and has been cranked
  It is no longer the right chord length or orientation */
        //  if(streq(sc->name(),"pocket4")) {
        //	  PolyPrint(" base \n", pout,ko);
        //   //g_TRACE=0;
        //}
        New_Chord = (float) PC_Dist(&(pout[0]),&(pout[ko-1]));

        if(New_Chord > FLOAT_TOLERANCE) {
            errA = (float)-atan2(pout[ko-1].y-pout[0].y,pout[ko-1].x-pout[0].x);
        }
        else errA=(float)0.0;
        errX = chord-New_Chord;

        dx = errX;
        dy = errA;

        *a0=*a0 + dy;
        nominal_Length = nominal_Length + dx;
        if (fabs(errX)>sc ->Esail ->m_Linear_Tol||
                fabs(New_Chord*errA)>sc->Esail ->m_Linear_Tol) KeepGoing=2;
        else KeepGoing = KeepGoing -1;

    } while (KeepGoing>0 && (100>loops++));

    if(loops>100) {
        printf("%s make_Fanned didnt solve in %d loops dx %f dy %f  (TOL=%f)\n",sc->name(),loops,dx,dy,sc->Esail->m_Linear_Tol);
    }

    *arc =(float) PC_Polyline_Length(ko,pout);

    *c = ko;
    *p = (VECTOR *)REALLOC (*p,sizeof(VECTOR)*(ko+1));
    for (k=0;k<ko;k++) {
        (*p)[k].x = pout[k].x;
        (*p)[k].y = pout[k].y;
        (*p)[k].z=  pout[k].z;
    }
    /*      At end, measure chord and chordangle
   Increment initial angle
   Increment ref length LOOP */

    /*      pass back the arc length and initial angle ready for use next time
   and in p the finished polyline
   maybe we store the nominal_Length value to speed up the routine next time*/

    RXFREE(x); RXFREE(a); RXFREE(crank); RXFREE(pout);
    return(1);
}
/************************************************************ 
int Scale_Base_CurveForGeo(VECTOR *Bin,VECTOR* base,int bc,float depth,int Xabs,int Yabs,int YasX,double scl) {
 //   given a raw base-curve Bin, scales it to depth and unit chord
 //    Xabs is true means copy Bin into base without scaling
 //    YasX means ignore depth and preserve aspect ratio THEN MULTIPLY BY DEPTH
 //    Yabs means y values are in metres. We multiply them by depth
 //    (this is to give an easy way of getting curves 'the right way up '
 //7.7.96 special to adjust gausscurves. If YasX is 2, we factor the curve by (1+depth)


  int k;
  float xmin,xmax,ymin,ymax;
  float yfactor,xfactor;
  
  xmin=Bin[0].x;  xmax=xmin;
  ymin=Bin[0].y;  ymax=ymin;
  
  for (k=1;k<bc;k++){
    xmax = max(xmax,Bin[k].x);
    ymax = max(ymax,Bin[k].y);
    xmin = min(xmin,Bin[k].x);
    ymin = min(ymin,Bin[k].y);
  }
  
  if (fabs(ymax-ymin)>0.0)
   yfactor= depth/(ymax-ymin);
  else
   yfactor =(float)0.0;
  if (fabs(xmax-xmin)>0.0)
   xfactor= (float)1.0/(xmax-xmin);
  else
   xfactor =(float)0.0;

  if (Xabs) {
    if(scl < FLOAT_TOLERANCE)			 //added 12/1/95
      xfactor = (float)1.0;
    else
      if((xmax-xmin) > FLOAT_TOLERANCE)
 xfactor = (float) scl/(xmax-xmin);
      else
 rxerror("basecurve and Length specifier inconsistent",2);
  }
  if (YasX==1) { yfactor = xfactor;
  
 }
  if (YasX==2)  yfactor = xfactor* ((float) 1.00 + depth);
  if (Yabs)  yfactor = depth;
  
  for (k=0;k<bc;k++){
    base[k].x = Bin[k].x * xfactor;
    base[k].y = Bin[k].y * yfactor;
    base[k].z = Bin[k].z * yfactor;
  }
  return(1);
} */

int Scale_Base_Curve(VECTOR *Bin,VECTOR* base,int bc,float depth,int Xabs,int Yabs,int YasX,double scl) {
    /* given a raw base-curve Bin, scales it to depth and unit chord
     Xabs is true means copy Bin into base without scaling
     YasX means ignore depth and preserve aspect ratio THEN MULTIPLY BY DEPTH
     Yabs means y values are in metres. We multiply them by depth
     (this is to give an easy way of getting curves 'the right way up '
 7.7.96 special to adjust gausscurves. If YasX is 2, we factor the curve by (1+depth)
     */

    int k;
    float xmin,xmax,ymin,ymax;
    float yfactor,xfactor;

    xmin=Bin[0].x;  xmax=xmin;
    ymin=Bin[0].y;  ymax=ymin;

    for (k=1;k<bc;k++){
        xmax = max(xmax,Bin[k].x);
        ymax = max(ymax,Bin[k].y);
        xmin = min(xmin,Bin[k].x);
        ymin = min(ymin,Bin[k].y);
    }

    if (fabs(ymax-ymin)>0.0)
        yfactor= depth/(ymax-ymin);
    else
        yfactor =(float)0.0;
    if (fabs(xmax-xmin)>0.0)
        xfactor= (float)1.0/(xmax-xmin);
    else
        xfactor =(float)0.0;

    if (Xabs) {
        if(scl < FLOAT_TOLERANCE)			 /* added 12/1/95 */
            xfactor = (float)1.0;
        else
            if((xmax-xmin) > FLOAT_TOLERANCE)
                xfactor = (float) scl/(xmax-xmin);
            else
                g_World->OutputToClient("basecurve and Length specifier inconsistent",2);
    }
    if (YasX==1) { yfactor = xfactor;
        /*	if(!Xabs) yfactor = yfactor *depth;  Nov 96 doesnt quite work. See batpatches */
    }
    if (YasX==2)  yfactor = xfactor* ((float) 1.00 + depth);
    if (Yabs)  yfactor = depth;		  /* added 5/5/94 */

    for (k=0;k<bc;k++){
        base[k].x = Bin[k].x * xfactor;
        base[k].y = Bin[k].y * yfactor;
        base[k].z = Bin[k].z * yfactor;
    }
    return(1);
}
int Make_Absolute_BaseCurve(VECTOR*p,int c,float Chord,double p_tol)  { // not XYZabs 
    /* the idea.
     To rotate  (p,c) until a point at{chord} is on X axis  */
    double arc,oldarc, ch ,costheta; //ch2,
    ON_3dVector r,r2,tangent;
    ON_3dPoint v,v2;
    double ds,ry,rz;
    float cosry,sinry,cosrz,sinrz;
    int i, count=0;
    arc = (double) Chord;
    ds = 0.05*Chord;
    do {
        count++;
        oldarc = arc;
        Place_On_Poly((float)arc,p, c, v);
        ch = v.DistanceTo (ON_3dPoint(0,0,0));
        if(ch > p_tol) {
            /* get the tangent */
            Place_On_Poly(	(float)(arc+ds),p, c, v2);
            tangent = v2-v;
            costheta = ON_DotProduct(v,tangent)/ch/tangent.Length();
            if(costheta < FLOAT_TOLERANCE) costheta = 0.1;
        }
        else costheta = 1.0;
        arc = arc + (Chord - ch)/costheta;
    }
    while (fabs(arc - oldarc) >p_tol && count<100);

    /* we now have vector v pointing from start to the point at dist {chord}
     so   rotate about Y by -atan2(v.z,v.x)
     Then rotate about X by -atan2(v.y,sqrt(v.x^2 + v.z^2))*/

    ry = atan2((double) v.z,(double) v.x);
    rz = atan2((double) v.y,sqrt((double)v.x*(double)v.x + (double)v.z*(double)v.z));
    cosry = (float) cos(ry); 	  sinry =(float) sin(ry);
    cosrz = (float) cos(rz);    sinrz =(float) sin(rz);
    for(i=1;i<c;i++) {

        r = ON_3dVector( p[i].x-p->x,  p[i].y-p->y  ,  p[i].z-p->z   );
        r2.x =   r.x*cosry 		+ r.z*sinry;
        r2.y = 			   r.y;
        r2.z = - r.x*sinry		+ r.z*cosry;

        r.x =   r2.x*cosrz + r2.y*sinrz;
        r.y = - r2.x*sinrz + r2.y*cosrz ;
        r.z = 									r2.z;
        p[i].x =r.x+p->x;p[i].y =r.y+p->y;p[i].z =r.z+p->z;
    }



    return(1);
}

int RXSeamcurve:: Compute_Seamcurve_Trigraph // (sc_ptr  p,int side1,int side2,VECTOR* e1b,VECTOR*e2b,VECTOR*xlb,VECTOR*ylb,VECTOR*zlb,float *Chord,const ON_3dVector p_Zax)  
(int side1,int side2,
 RXSitePt * e1b,RXSitePt *e2b,
 ON_3dVector*xlb,ON_3dVector*ylb,ON_3dVector*zlb,
 float *Chord,const ON_3dVector p_Zax)

{sc_ptr  p=this;

    ON_3dVector Zax =p_Zax;
    int err = 0;
    if(!p->End1site || !p->End2site) return 1;
    if(!Get_Position((*p).End1site,(*p).End1dist,side1,e1b)) err=1;
    if(!Get_Position((*p).End2site,(*p).End2dist,side2,e2b)) err=1;
    if(err)  return(1);
    *xlb=*e2b-*e1b ;
    *Chord=  xlb->Length();

    if (*Chord<(float)0.000001) {
        char s[256];
        printf(" ends of %s are at  (%f %f %f) and (%f %f %f) \n",p->name(), e1b->x,e1b->y,e1b->z, e2b->x,e2b->y,e2b->z);
        sprintf(s," zero length chord %s" , p->name());
        rxerror(s,2);
        return(1);
    }

    xlb->Unitize ();
    *ylb = ON_CrossProduct(Zax,*xlb);
    *zlb = ON_CrossProduct(*xlb,*ylb);
    zlb->Unitize ();
    return(0);
}

///***************************************************************/
//int RXGaussCurve:: Compute_GaussCurve(float Chord,int Seam,double p_Linear_Tol) {
//RXEntity_p bceptr  =this;
//    /* The input X values are assumed normalised on the SC length.
//     The imposed-angles are grouped into a reasonable number. Say 16
//     and then generates a cranked curve of the right arc-length*/



//    /* INPUT
//  the pin of the gausscurve
//   chord is the straight-line distance between the end points in m
//   we are trying to find a polyline of the same chordlength

//   RETURNS

//   in (*c,p) of the gausscurve curve. It is the correct length but needs
//   to be placed between endpoints.

//   */
//    int N,k,ko, loops=0;
//    double arc,New_Chord;
//    double nominal_Length;
//    double a0=0.0;
//    double errA, dy;
//    double errX, dx;
//    int *crank;
//    float *x;
//    double *a;
//    VECTOR *pout;

//    if(g_Gcurve_Count < 3) {
//        bceptr->OutputToClient  ("g_Gcurve_Count=128",2);
//        g_Gcurve_Count=128;
//    }
//    N=0;
//    if ( this->Group(g_Gcurve_Count,p_Linear_Tol ))
//   // if(Group_Polyline( gc->p ,&gc->c ,g_Gcurve_Count,p_Linear_Tol))
//          printf( " Grouped %s\n",bceptr->name());
//    N = this->m_pin.size() ;

//    crank = (int*)MALLOC(sizeof(int)*(N+1) );
//    x     = (float *)MALLOC(sizeof(float)*(N+1) );
//    a     = (double *)MALLOC(sizeof(double)*(N+1) );
//    pout = (VECTOR *)CALLOC(3+N, sizeof(VECTOR));
//    if(!pout) 	bceptr->OutputToClient ("out of mem in panel",3);

//    for (k=0;k<N;k++) {
//        crank[k] = 1;
//         x[k] =  m_pin[k].X()*Chord;
//         a[k] =  -m_pin[k].Y();
//        if (Seam)
//            a[k] = 0.5 * a[k];
//    }

//    nominal_Length= Chord;
//    do              /* loop until arc-length stops changing */
//    {
//        double tx,ty;//,ltz,lnorx,lnory,lnorz;
//        tx = cos(a0); ty = sin(a0); //ltz=0.0;
//        // lnorx = -ty;       lnory = tx;       lnorz=0.0;
//        pout[1].x = (float) (tx* nominal_Length);
//        pout[1].y = (float) (ty* nominal_Length);
//        ko = 2;
// //       assert("RXPOLYline"==0);
//           for (k=0;k<N;k++) {
//        x[k] =m_pin[k].X()* (float)nominal_Length;
//           }

//        Crank_Polyline(&pout,&ko,x,a,crank,N);  /* pout is now MALLOCed to the right size */

//        New_Chord = (float) PC_Dist(&(pout[0]),&(pout[ko-1]));
//        arc =  PC_Polyline_Length(ko,pout);

//        if(New_Chord > FLOAT_TOLERANCE) {
//            errA = (float)-atan2(pout[ko-1].y-pout[0].y,pout[ko-1].x-pout[0].x);
//        }
//        else
//            errA=(float)0.0;
//        errX = Chord-New_Chord;

//        dx = errX * arc/New_Chord;
//        dy = errA;

//        a0=a0 + dy;
//        nominal_Length = nominal_Length + dx;

//    } while ((fabs(errX)>p_Linear_Tol
//              || fabs(New_Chord*errA)>p_Linear_Tol )
//             && (100>loops++));

//    this->c = ko;
//    this->p = (VECTOR *)REALLOC (this->p,sizeof(VECTOR)*(ko+1));
//    for (k=0;k<ko;k++) {
//        this->p[k].x = pout[k].x;// also ser pin
//        this->p[k].y = pout[k].y;
//        this->p[k].z=  pout[k].z;
//    }
//    /*      At end, measure chord and chordangle
//   Increment initial angle
//   Increment ref length LOOP */
//    RXFREE(x); RXFREE(a); RXFREE(crank); RXFREE(pout);

//    return(1);
//}
/***************************************************************/

int PC_Pull_Pline_To_Mould( RXEntity_p mould,VECTOR**p, int *c,int adaptive,double tol){  
    int k;
    VECTOR *q,*qp,qm;
    double zz;
    if(!mould) return 0;
    if(!adaptive) {
        for(k=0;k<*c; k++) {
            (*p)[k].z = (float) PC_Interpolate(mould,(double)(*p)[k].x,(double)(*p)[k].y,(double)(*p)[k].z);
        }
    }
    else {
        if(!(*c)) return 0;

        (*p)->z = (float) PC_Interpolate(mould,(double)(*p)->x,(double)(*p)->y,(double)(*p)->z);
        for(k=1;k<*c; k++) {
            q=&((*p)[k]);
            qp=&((*p)[k-1]);
            q->z =(float) PC_Interpolate(mould,(double)q->x,(double)q->y,(double)q->z);
            if(adaptive && dists2D( q,qp) > 10.0*tol)
            {
                // test the previous interval
                qm.x=(qp->x+q->x)/(float)2.; qm.y=(qp->y+q->y)/(float)2.;qm.z=(qp->z+q->z)/(float)2.;
                zz= PC_Interpolate(mould,(double)qm.x,(double)qm.y,(double)qm.z);
                // if need to add,
                if(fabs(zz-qm.z) > tol) {
                    qm.z=(float)zz;
                    Insert_Into_Poly(p,c,qm,k-1);// //REALLOCS. inserts v into p just after ka
                    k--;
                }
            }
            else{
                cout<< " (PC_Pull_Pline_To_Mould) should skip the adaptive\n"<<endl;
                //adaptive=0;
            }
        }
    }
    return (*c);
}

int PC_Pull_SC_To_Mould(RXEntity_p sce){  // only called ONCE when m-lded flag is 1
    int  l;
    RXEntity_p mould;
    sc_ptr sc = (sc_ptr )sce;

    if(sc->m_scMoulded !=1) return 0;

    mould = sce->Esail->GetMould();

    for(l=0;l<3;l++) {
        if( PC_Pull_Pline_To_Mould(mould,&(sc->p[l]), &(sc->c[l]),1, sce->Esail->m_Linear_Tol/5.0))
            sc->m_arcs[l] = (float) sc->GetArc(l);
    }
    sc->m_scMoulded++;// A speed optimisation - dangerous
    return 1;
}


int Update_Xabs_Offset(sc_ptr p){
    /* Xabs curves will not, in general, have the end2 at 100%
   This routine provides a fix-up,  changing the IA offset to match */
    int k,rv;
    ON_3dVector t1; ON_3dPoint p1;
    double s=0.0,d = 0.0, tol;
    tol =  p->GetArc(1) /1000.0 ; if(tol < 0.001) tol = 0.001;
    if(!p->End2site) return 0;
    Site *qqq = (Site*) p->End2site;
    rv = p->m_pC[1]->Drop_To_Curve(qqq->ToONPoint () ,&p1,&t1, tol,&s,&d);
    if(rv <=0) { 	p->OutputToClient ("DrpToCurve failed",2); }
    for(k=0;k<p->n_angles;k++) {
        if(p->ia[k].s==p->End2site){
            p->ia[k].m_Offset->Change(TOSTRING(s));/* printf(" changing offset of %s in %s to %f\n", p->End2site->name(), p->owner->name(), s); */
            Sort_IA(p->ia,p->n_angles);
            return 1;
        }
    }
    return 0;
}


ON_3dPoint* Compute_SiteWhichSnap(Site *p,ON_3dPoint *v1,ON_3dPoint *v2) {
    /* see Technical Note : Finding Intersections
   v1 is the intersection point on sc1
   v2 ditto on sc2
   We copy one of these into v depending on the atribs of the scs*/
    sc_ptr sc1 = (sc_ptr  )p->CUrve[0];
    sc_ptr sc2 = (sc_ptr  )p->CUrve[1];
    ON_3dPoint *v=NULL;
    double s1,s2,d1,d2;
    if((sc1->m_scMoulded|| sc1->Geo_Or_UV) && !(sc2->m_scMoulded || sc2->Geo_Or_UV))
        return v1;
    if(!(sc1->m_scMoulded || sc1->Geo_Or_UV) && (sc2->m_scMoulded || sc2->Geo_Or_UV))
        return v2;

    if( sc1->XYZabs && !sc2->XYZabs)
        return v2;
    if(!sc1->XYZabs &&  sc2->XYZabs)
        return v1;
    if( sc1->XYZabs &&  sc2->XYZabs){ /* eg an all-DXF model */
        s1 = (p->Offsets[0]->Evaluate(sc1,1));
        s2 = (p->Offsets[1]->Evaluate(sc2,1));
        d1 = min(fabs(s1), fabs(sc1->GetArc(1)-s1));
        d2 = min(fabs(s2), fabs(sc2->GetArc(1)-s2));
        if(d1<d2)		{v=v2; }
        else			{v=v1; }
        return v;
    }

    /* here, none are XYZAbs*/
    PC_Insert_Arrow(p,45.0," Ambiguous cross-site");
    p->OutputToClient(" Ambiguous cross-site",1);
    return v1;
}

int RXSeamcurve::Append_One_Pside( Site **This_Node,Site**Last_Node,RXOffset*e1o,RXOffset*e2o){

    /* appends the new pside to the end of the current list. NOTE that
     if some psides have recently been deleted, the pside names will get duplicated
     SO Break Curve calls a re-name_PS which generates new names for this list
     of psides.
     */
    char l_name[256];
    RXEntity_p et;

    SAIL * sail = this->Esail ;
    sprintf(l_name,"%s(%d)new",this->name(),this->npss);

    if(this->npss && (et =sail ->Entity_Exists(l_name,"pside"))) {
        char s[256];
        sprintf(s,"deleting entity in append_one_pside\n%s"	,et->name());
        rxerror(s,2);
        et->Kill();
    }
    RXEntity_p pse = this->Esail->Insert_Entity("pside",l_name,0,(long)0," ","! a pside",(g_Janet>0));
    PSIDEPTR ps = dynamic_cast<PSIDEPTR > (pse);
    this->SetRelationOf(pse,spawn|niece,RXO_PSIDE_OF_SC,this->npss);
    (this->npss)++;
    this->pslist = (PSIDEPTR  *)REALLOC(this->pslist,(this->npss)*sizeof(PSIDEPTR ));
    this->pslist[(this->npss)-1]= ps;

    ps->End1off = e1o;
    ps->End2off = e2o;

    ps->SetEp(0,*Last_Node);
    ps->SetEp(1,(*This_Node));

    ps->sc=this;

    ps->Finish();
    ps->Add_To_EndNode_Lists();
    ps->SetNeedsComputing(0);
    return(1);
}

/**********************************************************/
int  RXPanel::Clear_Panel_Flags( ) {
    PSIDEPTR np;
    int k,c;
    c= m_psidecount;

    for (k=0;k<c;k++) {
        np = this->m_pslist[k];
        if ( reversed[k] ) {
            (*np).leftpanel[1]=NULL;
        }
        else {
            (*np).leftpanel[0]=NULL;
        }
    }
    return(1);
}

int Grow_Panel(PSIDEPTR  enext,PSIDEPTR  p, Panelptr  panel,RXEntity_p estart) {

    /* grows a panel from the starting pside enext by tracing around
     to the left until the end site equals the start site estart
     REQUIRES the search to be seeded on an internal pside
     */
    int OK=1,found=0,k,count ,loop=256;
    PSIDEPTR np;
    RXEntity_p Elast, next_site, Start_PS = p; // a guess

    count=1;
    next_site=NULL;
    do
    {
        if (enext==NULL){ OK=0; break;}

        np = enext;
        Elast=p;
        p = enext;

        for (k=0;k<2;k++ ) {
            found=0;
            if ( (np->er[k])&& (np->er[k])==Elast) {
                found=1;
                if (np->leftpanel[k])
                    return(0);
                if(count>=panel->allocsize) {
                    panel->allocsize= panel->allocsize*2;
                    panel->reversed=(int*)REALLOC(panel->reversed ,panel->allocsize*sizeof(int));
                    panel->m_pslist   =(PSIDEPTR *)REALLOC(panel->m_pslist,panel->allocsize*sizeof(PSIDEPTR ));
                }
                panel->reversed[count]=k;
                panel->m_pslist[count]=np; panel->SetRelationOf(np,aunt,RXO_PSIDE_OF_PANEL ,count);
                count++;
                panel->m_psidecount = count;

                if (np->el[1-k])
                    enext = (np->el[1-k]);
                else
                    enext=NULL;
                next_site=np->Ep(1-k);
                break; // out of for k
            }
        }
        if (!found){
            OK=0;
            break;
        }
        OK=1;
    } while (!((Start_PS == enext) && (next_site==estart)) && loop--);

    if(loop <=0) OK=0;
    if(!OK) return 0;
    if(panel->m_psidecount ==2 ) {	// Oct 2000 dont allow unless the psides are from different SCs
        if(panel->m_pslist[0]->sc == panel->m_pslist[1]->sc)
            OK=0;
        else
            OK=1;
    }
    return(OK );
}

int Paneller(SAIL *p_sail) {

    /* method:
     Find a Pside whose leftpanel[0] is NULL.
     start a new panel.. mark the pside's leftpanel[0].
     append e2l.  and loop
     Skip any panels which consist entirely of psides whose owning seamcurves
     have the edge flag set
    OR have the sketch flag set  Feb 2003
    TODO July 2014 - avoid the infinite loop in the case where ....

     */
#define PANEL_BLOCK 5
    int OK=1;
    int k ;
    int c=0;
    RXEntity_p  estart;
    PSIDEPTR  enext ;
    Panelptr  panel;
    char Panel_Name[32];
    // 2009 moved the sort-nodes call outside the do while batpatch loop in preproc
    //	Sort_Nodes(p_sail);// added 13 jan 2006

    ent_citer it;
    for (it = p_sail->MapStart(); it != p_sail->MapEnd(); it=p_sail->MapIncrement(it)) {
        PSIDEPTR np   = dynamic_cast<PSIDEPTR >( it->second);
        if(!np) continue;
        assert (np->TYPE==PCE_PSIDE) ;
        /* try to start a panel firstly  by going forwards on
   the left edge, and then by going backwards on right edge */

        for (k=0;k<2;k++) {
            sc_ptr owner;
            if ( np->sc)  {
                owner = (sc_ptr  )np->sc;
            }
            else {				/* A Pside from TPN */
                owner=NULL;
                QString qs(" TPN Pside !!!!!!!!!in Pside `" ); qs+= np->GetQName() ;qs+="`";
                p_sail->OutputToClient (qs,2);
                continue; // March 2015
            }
            if( (np->leftpanel[k] ==NULL) && (!owner->edge && !owner->sketch )) {	/* DONT seed on an edge */
                int side,l;
                Get_Next_Entity_Name("panel",Panel_Name,p_sail);

                char buf[512]; sprintf(buf,"$comment=seededon-%s",np->name()); // was atts
                RXEntity_p epp= p_sail->Insert_Entity( "panel",Panel_Name,NULL,(long)0,0,"!generated");  // no spawn needed
                panel = dynamic_cast< Panelptr> (epp);
                panel->Fillin (PANEL_BLOCK);


                panel->m_psidecount=1;

                panel->reversed[0]=k;
                panel->m_pslist[0]=np;  panel->SetRelationOf(np,aunt,RXO_PSIDE_OF_PANEL ,0);
                panel->allocsize=PANEL_BLOCK;

                estart=np->Ep(k);
                if (np->el[1-k]!=NULL )enext = np->el[1-k];
                else enext=NULL;

                OK= Grow_Panel(enext,np,panel,estart);

                /* returns 0 if it cant close a panel. 6/1/95 insensitive to internals */
                if (!OK) {
                    panel->Clear_Panel_Flags();
                    delete panel;
                }
                else {  /* insert the entity and set leftpanel on its psides  PETER NEW*/
                    c++;
                        class RXQuantity*me = p_sail->MeshExpression();
                        if(me)
                            panel->SetRelationOf(me,aunt,RXO_MESH_OF_PAN );

                    panel->Needs_ReGridding = 1;
                    panel->SetNeedsComputing(0);
                    panel->Geometry_Has_Changed = 1;
                    for (l=0;l<panel->m_psidecount;l++) {
                        PSIDEPTR pss;
                        side = panel->reversed[l];
                        pss = panel->m_pslist[l];
                        pss->leftpanel[side] = panel;
                    }
                    panel->Tidy_Panel_Materials();
                }
            }
        }	 /* end of for k */

    } /** end FOR*/
    return(c);
}


/**********************************************************/


int rxdecrement(int j,int count) {
    int i2;
    i2=j-1;        if (i2<0)   i2=i2+count;
    return(i2);
}
int rxincrement(int j,int count) {
    int i1;
    i1=j+1; if (i1>=count) i1=i1-count;

    return(i1);
}

int Sort_Nodes(SAIL *sail) {

    ent_citer it;
    for (it =  sail->MapStart(); it !=  sail->MapEnd(); ++it) {
        Site* ep  = dynamic_cast<Site*>( it->second); //SLOW
        if(!ep)
            continue;
        if(!ep->Needs_Resolving && ( ( ep->TYPE==SITE) || (ep->TYPE==RELSITE) ) ) {
            ep->Sort_One_Node();

        }
    }


    return(1);
}
/***********************************************************/
int Make_All_Psides(SAIL *sail) {
    int  count=0;
    ent_citer it;
    for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it)) {
        int t =  it->second->TYPE;
        if(t==SEAMCURVE ||t==PCE_NODECURVE ){
             sc_ptr p  = (sc_ptr)( it->second);
             count+=p->Break_Curve();
        }
    }

    /* if(count) Sort_Nodes(); not necessary.break Curve sorts the nodes it touches */
    /*28/7/95 needed (i think) on RLX designs
    In fact, needed if we add a second SC to a relsite AFTER its owning sc
    has been broken. BreakC only sorts if the PSlist changes.
    SO the solution is to set Make Psides Regardless on any ending SC,
    in Finish_SC.
    And that already happens, so no problem
    */
    return(count);
}



