#ifndef _RXPANEL_HDR__
#define _RXPANEL_HDR__
#include "opennurbs.h"

#include "vectors.h"
#include "RXEntity.h"

#ifdef linux
#define VOID  void

#endif
#include "triangle.h"
#include <vector>


class rxON_BoundingBox;

struct PANELDATA {
    int c;
    RXEntity_p *list;
    RXEntity_p refSC, mat; // changed 23 aug 2003 to find references to 'ref'
    RXEntity_p owner;
}; //struct PANELDATA 
typedef struct PANELDATA PanelData;


class TRIANGLE_POINT:public ON_3dPoint {
public:
    TRIANGLE_POINT();
    ~TRIANGLE_POINT();
    int m_marker;
    //ON_2dPoint m_uv;
    double m_u,m_v;
    class RX_FESite *m_Sptr;
    map<int,int> m_e; // first is the other node, second the edge no.

    bool PullToMould(RXEntity_p mould,const ON_Xform *axes);

};//class TRIANGLE_POINT 

#define TLD_ALLOCED		1
#define TLD_PRESTRESS	2
#define TLD_PSALLOCED	4
#define TLD_SPARE		8

typedef class TRIANGLE_POINT TriPt;


class RXPside;

class RXPanel:
        public RXEntity
{
public:
    RXPanel() ;
    virtual ~RXPanel() ;
    RXPanel(SAIL *sail);
    int CClear();
    int Init(void);
    int Compute(void);
    int Resolve(void);
    int Finish(void);
    int ReWriteLine(void);
    int Dump( FILE *fp) const;

public:
    int Fillin(const int sidecount); // functionality of the old constructor
    int Mesh();

    void PanelTriangleListsPrint(FILE *ff);
    int Edges_Are_All_Curves();
    int List(FILE *fp) ;
    HC_KEY DrawHoops(	);
    int ReBuildTriangles(const RX_COORDSPACE space);
    virtual int BuildTriMaterials();
    virtual double BuildOneTriMaterial(const int i, const RX_COORDSPACE space);
    int Print_Panel_Triangles( FILE *fp) const;
    int Print_Panel_DMatrices( FILE *fp)const;
    static  MatValType *rotate_material(double *Q,double theta);
    static int rotate_stress(double Q[3],double theta);
    int Gauss_One_Panel(const int what);

protected:

    int Triangulate(vector<RXSitePt> inpts, vector<int> enos, struct triangulateio &out, const int flag=0);

    static MatValType *MatMult33(double *a,double *b,double *q);
    static MatValType *VMult33(MatValType *a,MatValType *b,MatValType *q);
    vector<int> GetEdgeNumbers(void) const;
    vector<RXSitePt> GetBoundaryPoints(const RX_COORDSPACE space);
    void report(FILE *fp,struct triangulateio *io,
                int markers,int reporttriangles,int reportneighbors,int reportsegments,
                int reportedges, int reportnorms);
    void DrawTriangulation( struct triangulateio *io );
    int MakePtsEdgesTris( );
    int ReMakePtsEdgesTris( );
    /// RegenerateIntPts sets the internal points to black.
    int RegenerateIntPts(const RX_COORDSPACE c);
    RXSitePt BlackPos(const int i)const;
    RXSitePt RedPosition(const int i) const;
    class RX_SimpleTri BlackTri(const int i, const RX_COORDSPACE space);
    class RX_SimpleTri RedTri(const int i);
    int WriteAsPoly(struct triangulateio *io);// the Shewchuk poly file format
    int GetFilamentList();
    int GetTightBoundingBox(rxON_BoundingBox& bbox, bool bGrowBox=0,const ON_Plane* onb=NULL) const;
    int Check_For_Edge_Material( RXEntity_p *matkey);
    RXEntity_p Get_Panel_Ref_Vector( ON_3dVector &x) ;
    struct LINKLIST* Get_Panel_Ref_SC() ;
    RXEntity_p Get_PanelData_Ref_SC() ;
    RXEntity_p Get_PanelData_Material();
    int Record_SC_Edit(const char *kw,RXEntity_p matkey);
    int Entirely_On_Panel(sc_ptr sc,int side);
    int CollectFieldLayers() ;
    bool FieldIsLocal(const RXEntity_p fieldEnt );
    double MeshSize(const RXSitePt centroid) const;
public:
    int Supply_Material(const char*matname);
    int Clear_Zone_Flags();
    int Clear_Panel_Flags();
    int Tidy_Panel_Materials();
    RXSTRING TellMeAbout() const;
    int Display_One_Panel(HC_KEY seg);
    int Draw_Panel_Materials(const int c,VECTOR *poly);
    ON_3dVector Normal()const;
    RXEntity_p User_Selects_Ref_SC();

    void AddTri3(RX_FETri3* t);
protected:

    struct triangulateio *m_t;
    vector<RXSitePt> redpts, blackpts;
    vector<RX_FETri3*> m_FEtris;
    vector<RX_FEedge*> m_FEedges;
    struct mesh *m_m;
    struct behavior *m_b;
    ON_Xform m_tr, m_tinv;

public:
    int m_psidecount;
    int *reversed;
    PSIDEPTR *m_pslist;
    int allocsize; 	  /* for the two above */


    struct PANELDATA *m_PanelData;
    int Geometry_Has_Changed;
    int Needs_ReGridding,NeedsMaterials;
    int m_IsInBlackSpace;
    int m_HasRedspace;


    set<class RXEntity*> m_fils;

};
EXTERN_C int Ptr_Sort_Fn( const void *ia,const void *ib);
#endif
