/*	
   this is shapegen.h. C language headers for shapegen.f 

    SUBROUTINE Section_Analyse(XG,YG,NG,basetwist,chord,cr,cp,
     & th,fps,aps,ea,xa , aft_depth,usd)
      IMPLICIT NONE
* parameters

      REAL*8  XG(*),YG(*)  ! 1st point MUST be (0,0)
      INTEGER NG
      REAL*8 BaseTwist
* RETURNS

      REAL*4 Chord,CR,CP,TH,FPS,APS,EA,XA
       INTEGER usd	 */

#ifndef SHAPEGEN_16NOV04
#define SHAPEGEN_16NOV04

extern "C" void section_analyse_(double*XG,double*YG,int*NG,
				double*basetwist,float*chord,
				float*cr,float*cp,float*th,float*fps,
				float*aps,float*ea,float*xa,float *aft_depth,int*usd);

#endif //#ifndef SHAPEGEN_16NOV04

