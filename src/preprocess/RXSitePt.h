#ifndef _RXSITEPT_HDR__
#define _RXSITEPT_HDR__

#include "RXSiteBase.h"

class RXSitePt :
        public RXSiteBase
{
public:
    RXSitePt(void);
    RXSitePt(const double x,const double y,const double z,const double u,const double v,const int nn);
    ~RXSitePt(void);
    int Init(void);

    RXSitePt& operator =(const ON_3dPoint& p);
    RXSitePt& operator-=(const RXSitePt& p);

    RXSitePt operator*(const  double d ) const;
    RXSitePt operator/(const double d ) const;

    RXSitePt operator+( const RXSitePt& p ) const;


    double m_u,m_v;     // should initialize to ON_UNSET_VALUE
    float m_gdp;  /* Global value of distribution function    */

public:
    int m_Site_Flags;	// see griddefs.h for values

    /////////////////////////////////
    // Coordinate access methods.

    // to set the model-space coords.
    virtual bool Set(const ON_3dPoint &v);

    // to retrieve the model-space coordinates
    const ON_3dPoint ToONPoint(void) const;

    double getDs() const { assert ( ON_IsValid(m_dsEphemeral ) ); return m_dsEphemeral;}
    void setDs(const double x) { m_dsEphemeral =x;}
private:
        double m_dsEphemeral;   /* a value along curve  */
public:

    RXSTRING TellMeAbout() const;
}; //class RXSitePt

class RXSitePtWithData :
        public  RXSitePt
{
public:
    RXSitePtWithData(void);
    RXSitePtWithData(const double x,const double y,const double z,const double u,const double v,const int nn,const int datasize);
    RXSitePtWithData(const ON_2dPoint&uv ,const int ndata,const double* dbuf,const int c);
    RXSitePtWithData(const ON_3dPoint&xyz,const ON_2dPoint&uv ,const int ndata,const double* dbuf,const int c);
    ~RXSitePtWithData(void);

    int GetNDpts(void)const{ return m_nd;}
    double* GetDataHead(void)const{ return d;}
private:
    int m_nd; // no of data pts
    double* d; // the data


};
#endif

