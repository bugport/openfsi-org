
#include "StdAfx.h"
 #include "RXSail.h"
#include "RXSRelSite.h"
#include "RXEntityDefault.h" 
#include "stringutils.h"
#include "resolve.h"
#include "entities.h"
#include "f90_to_c.h"
#include "gle.h"
/*
Linear Substructure: airframe :boat
StartNodes
node$x$1$sub$2	4.000000 	0.000043 	0.000021 	-0.100000 	-0.100000 	-0.100000
node$x$1$sub$3	6.000000 	0.000075 	0.000037 	-0.000000 	-0.000000 	-0.000000
EndNodes
 StartStiffness
  	  -0.84300E+06  	   -11.594      	   -5.6147      	   0.56200E+06  	    8.4523      	    4.1362      
  	   -11.594      	   -25313.      	  -0.84004E-04  	    8.7820      	    13125.      	   0.68134E-04  
  	   -5.6147      	  -0.84004E-04  	   -50625.      	    4.2860      	   0.67879E-04  	    26250.      
  	   0.56200E+06  	    8.7820      	    4.2860      	  -0.56200E+06  	   -8.8720      	   -4.3760      
  	    8.4523      	    13125.      	   0.67879E-04  	   -8.8720      	   -7500.0      	  -0.70800E-04  
  	    4.1362      	   0.68134E-04  	    26250.      	   -4.3760      	  -0.70800E-04  	   -15000.      
 EndStiffness
*/

int Resolve_Gle_Card(RXEntity_p e){
	struct LINKLIST *plist=NULL;
	RXEntity_p p, se;
	Site *s;
	int OK = 0;
	char *w = NULL ,*lp = NULL;
	char m[256], name[256],nodename[256],names[64][64];
	int lm,k,nn=0,i, cf=0,cx=0;
	double *Kmat = NULL;
	int *nodes = NULL;
	double*F0 = NULL, *X0 = NULL;
	
	char*line = STRDUP(e->GetLine());
	PC_Strip_Leading(line);
	 *m = 0;   // linux port was null
	 *name= 0;
	cout<< "\nResolve_Gle_Card\n"<<endl;
	if(!line) return 0;
	lp = strtok(line,":\t\n"); printf("lp=%s\n", lp);
	if(lp)
		OK=1; 
	if(OK)
		if(w=strtok(NULL,":\t\n "))  /* spaces allowed but not on first line */
			strcpy(name,w);
		else
			OK=0;
	 printf(" name=%s\n", w);
	if(OK)
		if(w=strtok(NULL,":\t\n ")) 
			strcpy(m,w);
		else
			OK=0;
	 printf(" model =%s\n", w);
	if(OK) {
		do{
			w=strtok(NULL,":\t\n ");
			printf(" <%s>  ",w); 
		} while(w&& !strieq(w,"StartNodes"));
		if(!w) OK=0;
		if(OK) {					/* get the node names */
			while(w=strtok(NULL,":\t\n ")){
				if(stristr(w,"EndNodes")) { cout<< "break on endnodes"<<endl; break;}
				//printf(" node %d is <%s>\n",nn, w);
				strcpy(nodename,w);
				X0 =(double*)REALLOC(X0, (6+cx)*sizeof(double));
				for(k=0;k<3;k++) {
					w=strtok(NULL,":\t\n "); if(!w) break;
					X0[cx++]=atof(w);
				}
				if(!w){OK=0;   break;} // cout<< "break(1)"<<endl;
				F0 =(double*)REALLOC(F0, (6+cf)*sizeof(double));
				for(k=0;k<3;k++) {
					w=strtok(NULL,":\t\n "); if(!w) break;
					F0[cf++]=atof(w);	
				}
				//cout<< " got the 6 nos"<<endl;
				if(!w) { //cout<< "break 2"<<endl; 
					OK=0; 
					break; }
 				se= e->Esail->Get_Key_With_Reporting("site",nodename); 

				if(!se) { printf("couldnt resolve site <%s>\n",nodename); OK=0;break;}
				nodes = (int*)REALLOC(nodes, (1+nn)*sizeof(int));
				nodes[nn] =100-nn;
				memset(names[nn] ,0,64);
				strcpy(names[nn] , nodename);
				s = (Site*) se;
				nodes[nn] = s->GetN();
				//printf("Resolved site %s  n = %d\n", nodename, nodes[nn]); 
		 		Push_Entity(se,&plist);	  
				nn++; 
				//printf("at end of loop w %p <%s>\n", w,w);
			} /* end node search */
			printf("after break w= %p <%s>\n", w, w);
			if(w){
				w=strtok(NULL,":\t\n "); //printf(" w is <%s> SB  StartStiffness\n", w);
				printf("nn=%d\n",nn);
				Kmat = (double*)MALLOC(nn*nn*9*sizeof(double));
				for(i=0;i<nn*nn*9;i++){
					w=strtok(NULL,":\t\n ");
					if(!w) break;
				//	printf(" Kmat (%d) is %s\n", i,w);
					Kmat[i] = atof(w);
				}
				if(!w) 
					OK=0;
				else {
					w=strtok(NULL,":\t\n "); printf(" w is <%s> SB  EndStiffness\n", w);
					}
			} else OK=0;
	}
	}/* preamble OK */
	if(OK){ cout<< " Enter create_gle\n"<<endl;
	//	cout<< " X0 is\n"<<endl; for(k=0;k<3*nn;k++) printf("  %d  %f\n", k,X0[k]);
	//	cout<< " F0 is\n"<<endl; for(k=0;k<3*nn;k++) printf("  %d  %f\n", k,F0[k]);
		k = (int) strlen(m); 
		int has_tt=0;
		cout<< "WRONG.. GLE  passing an integer as dataptr  "<<endl;
 		i= cf_create_gle(m,k ,e ,nn,Kmat,nodes,F0,X0,has_tt,names) ;  
		e->PC_Finish_Entity("linear substructure",name,(void*) i,0,plist,NULL,e->GetLine());	
		e->Needs_Resolving= 0;
		e->Needs_Finishing = 0;
		e->SetNeedsComputing(0);
	}


	if(line) RXFREE(line);
	if(Kmat)RXFREE(Kmat);
	if(nodes)RXFREE(nodes);
	if(F0) RXFREE(F0);
	if(X0) RXFREE(X0);
	printf(" return %d\n", OK);
return OK;
}
