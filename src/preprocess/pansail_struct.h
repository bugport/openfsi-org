

#ifndef PANSAIL_STRUCT_16NOV04
#define PANSAIL_STRUCT_16NOV04

 #error("pansailstruct.h SSD ")


#ifndef PANSAIL_INPUT_VALUES_16NOV04
#define PANSAIL_INPUT_VALUES_16NOV04
struct PANSAIL_INPUT_VALUES{
 	float Reference_Length;	 
 	double Pitch_Velocity;  /* bow going down */
  	double Roll_Velocity;  /* roll to stbd */
	int PANSAIL_N_DEFAULT;
	int PANSAIL_M_DEFAULT;
	float Wind_u;
	float Wind_v;
	float Wind_w;
	int Flow_Separation;
};
#endif //#ifndef PANSAIL_INPUT_VALUES_16NOV04

#ifndef PANSAIL_MAST_GEOMETRY_16NOV04
#define PANSAIL_MAST_GEOMETRY_16NOV04
struct PANSAIL_MAST_GEOMETRY{ // should belong to each sail
	int Mast_Dia_Root;
	int Mast_Dia_Tip;
	int Mast_Length_For_Mast_Data;
	// and a Curve for mast dia
};
#endif //#ifndef PANSAIL_MAST_GEOMETRY_16NOV04


#endif //#ifndef PANSAIL_STRUCT_16NOV04
