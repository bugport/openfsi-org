#ifndef _CFROMF_H_
#define  _CFROMF_H_


class  RX_FEPocket;
class  RX_FESite;
class RX_FEString;
class RX_FETri3;
class CTensylLink;
// Must be consistent with cfromf_declarations.f90

extern "C" int cf_gravity_field ( double*x,  double* acc);
extern "C" void cf_DofoAllJacobians(const int sli, int*err); 
extern "C" int cf_SetParentRelation   (RX_FESite* p1,class RXEntityDefault* p2);
extern "C" int cf_UnSetParentRelation (RX_FESite* p1,class RXEntityDefault* p2);
extern "C" int cf_SetParentRelationStr (RX_FEString* p1,class RXEntityDefault* p2);
extern "C" int cf_UnSetParentRelationStr (RX_FEString* p1,class RXEntityDefault* p2);
extern "C" int cf_SetToLastKnownGood(const int sn);  
EXTERN_C void cf_flagset(const char*flags);
extern "C" int cf_postprocess();
extern "C" int  cf_IsEdgeNode(const void *p_s,const int n);
extern "C" int cf_getTensyllinkPropNo (const class CTensylLink *p );
 
extern "C" int check_messages(int *k);
extern "C" int fc_OnReload(void);
extern "C" int fc_getlastknowngood(RX_FESite*cptr, double *x);
extern "C" int fc_getundeflected(RX_FESite*cptr, double *x); //global

EXTERN_C int ClearBatten(RX_FEPocket *ptr);
EXTERN_C int Anchor_Loads(double *tmax);
EXTERN_C int Draw_Resids(double*tmax);
#ifdef NEVER
EXTERN_C void akm_flush_contents_(const char *seg,const char*s);
EXTERN_C void akm_set_visibility_(const char *s);
#endif

EXTERN_C double get_vector_plot_scale(void);
EXTERN_C int pcc_send_as_client_(char *buf, int lbuf);
EXTERN_C void inquiresnode_(int *p, float*x,float*y,float*z);
EXTERN_C void Get_Next_Material_Stress(class RX_FETri3 **t,double *eg,int *Force_Linear,double *slr,int *flag);

EXTERN_C int  cf_GetWrinkledStress(double e[3], double d[3][3], double  sl[3]);

EXTERN_C int fc_isnan_d_(double *a);
EXTERN_C int getfortrannodeno(char *model, char *nname);

EXTERN_C int contact_find_nearest(RXEntity_p *cptr, double oldpt[3], double proposedpt[3], double newpt[3]) ;
/* returns 0 if no intersection was found, else C_GOING_UP or C_GOING_DOWN
  *k is the parameter on the line from oldpt to newpt
*/
EXTERN_C int slidecurve_find_jacobian(RXEntity_p *cptr, double uv[2],double j[9]); // return OK
EXTERN_C int contact_find_jacobian(RXEntity_p *cptr, double uv[2],double j[9]); // return OK

//EXTERN_C Site* get_ptr_to_node(SAIL *sail,int *n) ;
EXTERN_C RXEntity_p get_slidecurve(Site *const snode) ;
EXTERN_C RXEntity_p get_contact_surface(Site *const snode) ;
EXTERN_C int fc_dump_snode(Site *const snode) ;
EXTERN_C int fc_SetSiteAttribute(Site *s,const char* what,const char*val );

EXTERN_C int fc_getsitename ( Site *s,char*output) ;
EXTERN_C int fc_getentityname (RXEntity_p s,char*output, const int olength) ;
EXTERN_C int slidecurve_update_param(RXEntity_p *cptr,double *p_t,double *dt); // return OK
EXTERN_C void slidecurve_evaluate(const RXEntity_p *cptr,const double *p_t,double p[3]); 
EXTERN_C void slidecurve_lengthto(const RXEntity_p *cptr,const double *p_t,double *length); 
EXTERN_C void contact_evaluate(const RXEntity_p *cptr,const double uv[2],double p[3]); // return OK
 
 EXTERN_C int contact_test_passedge(RXEntity_p *cptr,  // return non-zero if uv2 is off edge
						   const double uv1[2], 
						   const double uv2[2], 
						   double *k);

EXTERN_C int contact_test_isbelow (RXEntity_p *cptr, // the contact surface entity
				  double p_proposedpt[3],// the points are in global coordinates
				  double p_newptuvw[3]) ;
				 
EXTERN_C int contact_find_intersection(RXEntity_p *cptr,
					double p_oldpt[3], double p_proposedpt[3],
					double p_newptxyz[3],double p_newptuv[3],double*k)  ;

EXTERN_C int slidecurve_find_intersection(RXEntity_p *cptr,
					   double p_oldpt[3],
					   double p_newptxyz[3],double *p_newptu) ;

EXTERN_C int cf_get_nodal_fixing (Site *snode,char *output);
EXTERN_C int fc_delete_object(class RXObject *ptr);
EXTERN_C int cf_set_dofo_eptr (const int sli,const int dofoN,const class RXObject*ptr);

EXTERN_C int cf_JaySize();
EXTERN_C void cf_DerivativeList(const double t, const double*y, double*dxdy, const double damp, const int N);
EXTERN_C void cf_PackCoordinates(double*y);
EXTERN_C void cf_PackResiduals(double*y);
EXTERN_C void cf_zeroVelocity();
EXTERN_C double cf_MeasureKE(const double*disp, const int nv);
EXTERN_C int fc_runtopeak(const int method,int*kit); //returns iexit
EXTERN_C void cf_ApplyDisplacements(const double*disp, const int isIncrement);
EXTERN_C void cf_ApplyVelocities(const double*disp);

EXTERN_C void cf_FormJayMatrix( class RXSparseMatrix1 **jay, class RXSparseMatrix1 **rhs, int*nrhs, double rcapvalue );

EXTERN_C void cf_FormInverseMassMatrix( class RXSparseMatrix1 **Bee);
EXTERN_C void cf_FormStiffnessMatrix( class RXSparseMatrix1 **m,const int bytest);
EXTERN_C void cf_FormStiffnessMatrixByTest( class RXSparseMatrix1 **m);
#endif

