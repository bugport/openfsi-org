#pragma once
#include "RXEntity.h"

class RXDrawingLayer :
	public RXEntity
{
public:
	RXDrawingLayer(void);
	RXDrawingLayer(class RXSail *s);
	~RXDrawingLayer(void);


		virtual int Dump(FILE *fp) const;
		virtual int Compute(void);
		virtual int Resolve(void);
		virtual int Finish(void);
		virtual int ReWriteLine(void);
};
