#include "StdAfx.h"
#include <fstream>
#include "rxvectorfield.h"
#include "RXSail.h"
//#include "RXAttributes.h"
#include "RXInterpolationI.h"

RXInterpolationI::RXInterpolationI( )
{
    assert(0);
}

RXInterpolationI::RXInterpolationI(class RXSail *s):
    m_sail (s)
{
    msh.Init();
}

RXInterpolationI::~RXInterpolationI(void)
{
}

double RXInterpolationI::ValueAt(const ON_2dPoint& pin)
{
    int rc;
    ON_2dPoint p(pin);
    RXVectorField *f = 0;//dynamic_cast< RXVectorField *>(this->m_unitSquareToQuad);
    assert(strlen("double RXInterpolationI::ValueAt(const ON_2dPoint& pin) hasnt been stepped")==0);
    if(f){
        RXSitePt pp(0.,0.,0.,p.x,p.y,-1);
        double rv[3];

        rc= f->ValueAt(pp,rv);
        p.x=rv[0]; p.y=rv[1];
    }
    return  _ValueAt(p );

}

int RXInterpolationI::ValueAt(const ON_2dPoint& pin, double*rv) //this one is called by read_ND_state
{
    ON_2dPoint p(pin); int rc;
    RXVectorField *f = 0;//dynamic_cast< RXVectorField *>(this->m_unitSquareToQuad);
    if(f){
        RXSitePt pp(0.,0.,0.,p.x,p.y,-1);
        double rv[3];

        rc= f->ValueAt(pp,rv);
        p.x=rv[0]; p.y=rv[1];
    }
    return  _ValueAt(p,rv);

}
void RXInterpolationI::LocateInitialize()
{ this->msh.LocateInitialize();
}

