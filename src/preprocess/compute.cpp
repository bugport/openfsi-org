/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXCompoundCurve.h"
#include "RXSail.h"
#include "compute.h"

int Compute_All_Compound_Curves(SAIL*sail) {

	ent_citer it;
	    for (it = sail->MapStart(); it != sail->MapEnd(); it=sail->MapIncrement(it)) {
		class RXCompoundCurve *  e  =dynamic_cast<class RXCompoundCurve * >( it->second);
		if(!e) continue;
	   if(strieq(e->type(),"compound curve")) { // SLOW
		  	  	e->Compute_Compound_Curve(); 
	   	}
	 }	
return(1);
/* This updates the arclength and the pslist of
the COm_Cur. Safer always to do it as its not slow. 
NOTE FIn_C_C calls the compute*/
}
 
 

 
