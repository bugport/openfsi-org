/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* batstiff.c  4/5/94  */

#include "StdAfx.h"
#include "RXEntityDefault.h"

#include "hoopcall.h"
#include "polyline.h"
#include "etypes.h"
#include "batdefs.h"
#include "batpatch.h"

#include "batstiff.h"

float KBpolynomial(float *a,int n,float x){
    int k;
    float y;
    y=(float)0.0;
    for (k=0;k<n;k++) {
        if(k)  y = a[k]*pow((double) x,(double)k) + y;
        else 	 y = a[k] + y;
    }
    return(y);
}
float KBatten_Stiffness(const RXEntity_p e, float x1,float x2,float x3){
    double ei,y1,y2,y3,a1,a2;
    int n;
    struct PC_BATTEN *b;
    /* returns knot stiffness (nm per rad) of a batten (base batten)
  from the spline function for EI
   In A.DAT file this is EI(5)
   But beware the numbering in the A.DAT.
   EI(5,1) is the spring stiffness at node 2 (end of edge 1) etc
    */

    if(e->TYPE !=BATTEN)
        return((float)0.0);

    double l_length,l_force;
    RXExpressionI *q ;
    q = (e->FindExpression (L"length",rxexpLocal));
    l_length=q->evaluate () ;
    q = (e->FindExpression (L"force",rxexpLocal));
    l_force=q->evaluate () ;



    b = (PC_BATTEN*)e->dataptr;
    n = b->c;
    ei = 0.0;
    if  (l_length <FLOAT_TOLERANCE){
        e->OutputToClient("zero length in batten library",1);
        return((float) 0.0);
    }

    if(n>2) {
        /*		   y1=KBpolynomial(b->a,n,x1);
     y2=KBpolynomial(b->a,n,x2);
     y3=KBpolynomial(b->a,n,x3);	*/

        y1 = Batten_Spline((double)x1,b->k);
        y2 = Batten_Spline((double)x2,b->k);
        y3 = Batten_Spline((double)x3,b->k);

        a1 = (y2-y1)/(x2-x1)/l_length;
        a2 = (y3-y2)/(x3-x2)/l_length;
        if(a1 >a2+1.0E-6)
            ei = l_force * y2 / (a1-a2);
        else
            ei=0.0;
    }
    else  {
        /* cout<< " n zero "<<endl;
        TO match mid-point deflection on interval (x1 to x3) , K = 3 ei/(x3-x1)
        To match angular deflection over half the interval,    K = 2 ei/(x3-x1)
        here we have K = 1 ei/(x3-x1)  which is lower.

*/
        ei = l_force*(l_length)*(l_length)/9.869604/(fabs(x3-x1)/2.0)/2.0;
    }


    /* 	printf("KEI being exported is %f\n",ei);   */
    return((float) ei);
}

float Batten_Stiffness(const RXEntity_p e, float x){
    double ei,y,ydd;
    int n;
    char string[256];
    struct PC_BATTEN *b;
    /* returns ei of a batten (base batten) from the spline function for EI	*/

    if( e->TYPE !=BATTEN)
        return((float)0.0);
    b = (PC_BATTEN*)e->dataptr;

    double l_length,l_force;
    RXExpressionI *q ;
    q = (e->FindExpression (L"length",rxexpLocal));
    l_length=q->evaluate () ;
    q = (e->FindExpression (L"force",rxexpLocal));
    l_force=q->evaluate () ;


    n = b->c;
    ei = (float)0.0;
    if  (l_length <FLOAT_TOLERANCE){
        e->OutputToClient("zero length in batten library",1);
        return((float) 0.0);
    }
    if(n>2) {

        y = Batten_Spline((double)x,b->k);
        ydd = Batten_Curvature(	(double)x,b->k);

        if (fabs(ydd) < FLOAT_TOLERANCE){
            sprintf(string,"Zero radius in batten curve of '%s'",e->name());
            //	 rxerror(string,1);
            ei = 0.0;
        }
        else {
            ei = - y /ydd * (l_length) * (l_length) * l_force;
            if ( ei<0.0)  {
                sprintf(string,"Inflection in batten curve of\n%s",e->name());
                e->OutputToClient(string,1);
                ei=0.0;
            }
        }
    }
    else  {
        ei = l_force*(l_length)*(l_length)/(float) 9.869604 ;
    }
    return((float) ei);
}

