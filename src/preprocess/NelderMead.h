// NelderMead.h: interface for the NelderMead class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NELDERMEAD_H__CF6B091F_4223_4548_8484_C4D9C32EFCF4__INCLUDED_)
#define AFX_NELDERMEAD_H__CF6B091F_4223_4548_8484_C4D9C32EFCF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "opennurbs.h"
#include "RXOptimisation.h"
 
#define NP_DEFINED 3
#define MP_DEFINED 4



class NelderMead  : public RXOptimisation
{
public:
	ON_SimpleArray<double> GetResult(void ); // returns a vector N+1 with the func value last.
	int SetStart( ON_SimpleArray<double> & x ,ON_SimpleArray<double> &scales);
	int Print(FILE *fp);
	 
	NelderMead();
	virtual ~NelderMead();
	virtual double func(ON_SimpleArray<double>  &x);
	bool FindMinimum( );

protected:
	inline void get_psum(ON_Matrix &p, ON_SimpleArray<double>  &psum);

	double m_ftol;
//
	int m_nmax;

	int m_MP,m_NP;
	ON_SimpleArray<double>  m_x,m_y;
	ON_Matrix m_p;

private:

	double amotry(ON_Matrix &p, ON_SimpleArray<double>  &y, ON_SimpleArray<double>  &psum, 	const int ihi, const double fac);



};
EXTERN_C	int NelderMeadtest(void);

#endif // !defined(AFX_NELDERMEAD_H__CF6B091F_4223_4548_8484_C4D9C32EFCF4__INCLUDED_)
