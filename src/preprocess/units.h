#ifndef _UNITS_H_
#define  _UNITS_H_


struct _OUTPUT_UNITS{
	char kw[32];
	char units[8];
	double f;
	int flag;
};
#define N_UNITS  8

//EXTERN_C struct _OUTPUT_UNITS Global_Units[N_UNITS ];

#define OTHER_OP  0
#define STRESS_OP  1
#define STRAIN_OP  2
#define SANGLE_OP  3
#define DEGREE_OP 4
#define PRESS_OP  5
#define CP_OP  6
#define EA_OP  7

#endif  //#ifndef _UNITS_H_



