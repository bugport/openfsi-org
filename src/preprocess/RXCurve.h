
#pragma once

#ifdef __cplusplus	
	#include "RXGraphicCalls.h"	
	#include "RXTRObject.h"
	#include "rxON_Extensions.h"
	class RXOffset; 
	class rxON_BoundingBox;
	class rxON_PolylineCurve;
	#include "vectors.h"

	#define RXCURVE_ERROR -1

	#define RXCURVE_OK 1
	#define RXCURVE_FALSE 0

	#define RXCURVE_GEOM_POLYLINE 1
	#define RXCURVE_GEOM_ONCURVE  2

	#define RXCURVE_IN_DOMAIN 1
	#define RXCURVE_OFF_DOMAIN_AT_START 2
	#define RXCURVE_OFF_DOMAIN_AT_END 3

	#ifndef RXCURVECLASSID //7115A93B 4741 465A BD9D 40AF0B783894
		#define RXCURVECLASSID "7115A93B-4741-465A-BD9D-40AF0B783894"
	#else
		RXCURVECLASSID defined somewhereelse
	#endif

	#define  PCC_OK     		(1)
	#define  PCC_IFOUND_OK		(1)
	#define  PCC_NOT_CONVERGED	(-1)
	#define  PCC_OUT_OF_RANGE   	(-2)
	#define  PCC_PARALLEL		(-4)
	#define  PCC_SMALL_ANG		(-8)

class rxON_BoundingBox;
	//!  Class to define a cuvre in relax Replace PC_Curve
 
	class RXCurve :  public RXTRObject
	{
	public:
#ifndef ON_V3
		int GetTightBoundingBox(rxON_BoundingBox &bbox, bool bGrowBox, const ON_Xform *onb) ;
#else
		int GetTightBoundingBox(rxON_BoundingBox &bbox, bool bGrowBox, const ON_Plane *onb) const;
#endif

		int IsPtInsideBB(ON_3dPoint p_t,double tol=0.000001);
		ON_3dPointArray Divide(RXOffset  &o1, RXOffset &o2, sc_ptr sc,int side, int p_n);
		int GetONPolyLineCurveFromRelaxPL(rxON_PolylineCurve & p_PlC) const;
		bool IsValid() const;		
		//!Constructor
		RXCurve();
		//!Constructor by copy
		RXCurve(const RXCurve & p_Obj);

		RXCurve(const ON_Curve * p_Obj);		

		//!constructor from a polyline 
		RXCurve(VECTOR*pxin,int cxin);

		//!Destructor
		virtual ~RXCurve();

		void Init();

		int Clear();
        void Set_Ent(RXEntity_p p_e){ m_e = p_e;}

        RXEntity_p Get_Ent() const { assert(m_e); return m_e;}
//		RXEntity_p GetEntity() const{ assert(m_e); return m_e;}

		RXCurve& operator = (const RXCurve & p_Obj);
		
		virtual char * GetClassID() const;
		virtual int IsKindOf(const char * p_ClassID) const;
		static RXCurve * Cast( RXTRObject * p_pObj);
		static const RXCurve * Cast( const RXTRObject * p_pObj);

		ON_3dPoint PointAt(const double & p_t) const; 
		ON_3dVector TangentAt(const double & p_t) const;

		//!fill the curve structure with a polyline
		//!
		//	pxin : array of point 
		//	cxin : nb of points 
		//
		int Fill_Curve(VECTOR*pxin,int cxin);
static	int DumpPLine(const VECTOR*pxin,const int cxin,FILE *fp);

		//!Evaluates the curve position and derivative at p_offset 
		//!
		//	p_delta	: finite difference step (ONly used if the curve is not defined with an ONCurve)
		//
		int Find_Derivative(const double p_offset, ON_3dPoint *pos, ON_3dVector *tan, const double & p_delta,double fractional_tolerance) const;
		int Find_Tangent(const double p_offset, ON_3dPoint *pos, ON_3dVector *tan, const double & p_delta = .001) const;

		int Update(VECTOR*p,const int & c);

		//!Evaluate the curve at p_offset (meters) and set p_v
		int Place_On(const double & p_offset, ON_3dPoint* p_v,double fractional_tolerance) const;
		int Place_On(const double & p_offset, ON_3dPoint* p_v) const;
		
		
		ON_3dPoint GetStartPoint() const; 
		ON_3dPoint GetEndPoint() const;

                //set p_t the parameter on the curve at p_lenght for the start of the curve
		//
		//	return RXCURVE_IN_DOMAIN if 0<=p_length<=this->Length
		//	return RXCURVE_OFF_DOMAIN_AT_START if p_length<0
		//	return RXCURVE_OFF_DOMAIN_AT_END if this->Length<p_length
		//	return RXCURVE_ERROR if error, do not change p_t
		//
		int GetParamAt(const double & p_length, double & p_t,double fractional_tolerance=1.0e-8) const;

		int Somewhere_NearV(VECTOR *a,double tol);
		int Somewhere_Near(const ON_3dPoint & t,double tol =1e-6) ; 
		struct PC_QCD *Find_Bounding_Box(int i1,int i2,int monotonic) const;

		//! Drop_To_Curve:  return values.  -1 error  1 converged, 0 unconverged
		//!    where a is the point in question
		//	  C is  the curve
		//
		//	  tol is used twice - 
		//		  its used as the step length along the polyline for each iteration
		//	  its also used to stop iteration (s change is less than tol)
		//
		//	  ss is initially the length along the curve to start
		//	  ss is set to the length along the curve where the perpendicular hits
		//	  dist is the perpendicular distance.
		//	
		//	try s = s as initial length along polyline
		//find tangent at s
		//find location (r) of perpendicular between a and poly
		//measure distance along this tangent then add this distance to s
		//increment s by ((a-r) dot t)
		//loop if inc > tol
		//dist is (a-r) cross t
		//TOL has a double purpose. Delta for tangent calc, and convergence limit
		 ///

//		int Drop_To_Curve(const ON_3dPoint *p_a,ON_3dPoint*p_r, ON_3dVector*p_t,double p_tol,double*p_ss,double*p_dist) const;
		int Drop_To_Curve(const ON_3dPoint p_a, ON_3dPoint*p_r, ON_3dVector*p_t,double p_tol,double*p_ss,double*p_dist,double fractional_tolerance=1.0e-8) const;
//		int Drop_To_Curve(const ON_3dPoint p_a, ON_3dPoint*p_r, ON_3dVector*p_t,double p_tol,double*p_ss,double*p_dist) const;

		//Curves_Coincident test is p_c2 is conincident to this
		///! test at up to 9 points on c1.  For each point, drop a perp to c2.
		//	If the distance between where we are and the dropped point is > g_SOLVE_TOLERANCE,
		//	the curves arent coincident. 
		//	Else they might still be 
		int Curves_Coincident(RXCurve *p_c2) const;

	//NOT USED ANYWHERE	int Find_Int(RXCurve*,double,double,double*,VECTOR*,VECTOR*,RXCurve*,double,double,double*,VECTOR*,VECTOR*);

		void SetONCurve(ON_Curve * p_obj,const char*p_text=NULL);
		ON_Curve * GetONCurve() const;

		void SetType(const int & p_type);
		int GetType() const;
		
		void Set_p(VECTOR	* p_p);
		VECTOR	*Get_p() const;

		void Set_c(const int & p_c);
		int Get_c() const;
		
		void Set_C0(const int & p_C0);
		int Get_C0() const;
		
		void Set_ds(double	* p_ds);
		double *Get_ds() const;

		double Get_arc(double fractional_tolerance=1.0e-8) const;

		double Get_chord() const;

		void Set_zax(VECTOR	*p_zax);
		VECTOR	* Get_zax() const;

		void Set_dy(const double & p_dy);
		double	Get_dy() const;

		void Set_dz(const double & p_dz);
		double Get_dz() const;

		void Set_qcd(struct PC_QCD *p_qcd);
		struct PC_QCD * Get_qcd() const;
		
		//!Curve_To_Line_Intersection : Find an intersection between C1 and the line segment from a to b
		//! 
		// Return PCC_IFOUND_OK if any intersection is found.
		// Extensions are searched too, depending on flags
		//	otherwise  0\n
		//
		// An intersection is defined as a line normal to both curves,\n
		//  between	a point v1 at s1 (arclength) on C1 \n
		//  and		a point v2 at t2 (parameter) on  (a to b) \n
		//
		// HORRIBLE sideefects.  It modifies the 4 vectors and I don't know why
		//
		//options are  (EXT_1_L+EXT_1_R+EXT_2_L+EXT_2_R)
		///
		int Curve_To_Line_Intersection(double*p_s1,
										 ON_3dPoint *p_v1, ON_3dPoint*p_v2,
										 ON_3dPoint *p_a, ON_3dPoint *p_b, 
										 double*p_t2,int*p_indexout,int p_flags) const;

		//!returns 1 is the 2 curve have the same g�ometry
		//!
		//	perform a fast check
		//
		int Curve_Compare(const RXCurve & p_curve, const double & p_tol) const;

                int Draw(RXGRAPHICSEGMENT n=0, const char*color=0) const;
		int HasGeometry() const;
		int IsNear(const ON_3dPoint &p_pt, ON_Curve * p_crv, const double p_tol) const;

//						   const  & p_Crv1,
		int  IntersectRXC(					const RXCurve & p_Crv2,
				double**p_ss1, double**p_ss2, int*p_nc, //Define the intersections (OUTPUT)
				const double & p_CutTol, const double & p_pTol )const;

	protected:
		
		//!check the ON_curve object before setting it
		//!
		//	if valid return a point on p_crv else NULL
		//
		const ON_Curve * CheckONCurve(const ON_Curve * p_crv,char *string) const;
		
		//!To define the curve as a polyline (need to be removed)
		VECTOR	*m_pline;  
		//! if the curve is define as a polyline nb of points (need to be removed)
		int	m_c;
		//! ??????
		int		m_C0;

		double	*m_ds;
		//!Arcs and chord of the curve (need to be removed)
		//double	m_arc;
	//		double m_chord;

		VECTOR	*m_zax;
		double	m_dy;
		double	m_dz;
		
		struct PC_QCD *m_qcd;	// Quick Cut Data see boxsearch.c/.h
		
		//!denition of the curve in a ON_Curve object
		ON_Curve* m_pONCurve; 
		// Peter would it be better if this were 		ON_Curve& m_rONCurve;  ??????????????? f

		//! TRUE if the ON_Curve belongs to this RXCurve object. 
		//! the onlycase for now is when we set it from a Relax Polyline (to remplace VECTOR * m_p)
		int m_ONCurveBelongtoThis; 
		//!Type of the curve
		int m_type;  // may be PCE_NURB or 0. If its a NURB, qcd is a PC_NURBS structure 
		RXEntity_p m_e; //We get global tolerances by dereferencing m_e

		rxON_BoundingBox m_bb;
	};

#endif //#ifdef __cplusplus	
 

