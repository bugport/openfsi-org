#pragma once
class RXSail;
#ifdef _X
#include "Grap hicStr uct.h"
#include "find_include.h"
EXTERN_C XtCallbackProc freeze_value_changed_CB	(Widget w,XtPointer client_data,XmToggleButtonCallbackStruct *call_data);
EXTERN_C XtCallbackProc freeze_arm_CB		(Widget w,XtPointer client_data,XmToggleButtonCallbackStruct *call_data);
EXTERN_C XtCallbackProc freeze_disarm_CB 	(Widget w,XtPointer client_data,XmToggleButtonCallbackStruct *call_data);
#endif
EXTERN_C int get_state_freeze_button(char*, char*);

EXTERN_C int PCP_Freeze(class RXSail *s);
EXTERN_C int PCP_UnFreeze(class RXSail *s);
EXTERN_C int Set_All_FreezeButtons(const class RXSail *p_sail, const int p_stat);
 
