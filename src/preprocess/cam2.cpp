/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 feb/97  when we resolve a camera we re-style any camerasegs that use it
	this enables swooping
1/10/96 
 SetHardCamera called with NULL sets previous camera PCAM
 5.6.96 two changes.
 first a dollar in the light color filed is changed to a comma
 so "white$ambient=green" is parsed correctly
Second. This color is set on the PARENT of distant lights.
It will remain in force until the viewport is killed
 *       4/95 printfs
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXEntityDefault.h"
#include "RXSail.h"
#include "RXQuantity.h"
#include "entities.h"
#include "stringutils.h"

#include "camera.h"

#define CAMDBG  (false)

#define SURROUND .05

#define UV_STEP 4

static Camera *PCAM=NULL;		/* previous camera */


 int Print_Camera(const char *s,const char*segin) {
#ifdef HOOPS
	char buf[256];
	VECTOR pos,target,Up;
	float width,height;
if(!CAMDBG) return 0;
	HC_Show_Pathname_Expansion(segin,buf);
	printf("\n%s\n Camera  in seg <%s>\n",s, buf);
	HC_QShow_Net_Camera(buf, &pos,&target,&Up,&width,&height,buf);


	printf(" pos      %f %f %f\n", pos.x, pos.y, pos.z);
	printf(" target   %f %f %f\n", target.x, target.y, target.z);	
	printf(" UP        %f %f %f\n", Up.x, Up.y, Up.z);
	printf(" W=%f H=%f proj=%s\n", width,height,buf);

#endif
return 1;
}


int Resolve_Camera_Card(RXEntity_p e)
{ 

/* camera card is of type
         0    1              2                3                    4                   5       6            7                 [8                 9]   [10]
camera:name: position x,y,z: target z,y,z: topvector x,y,z: width:height: projection[:optional light colour:x y z]  [:atts] ! comment 

*/
char seg[256], buf[256];
char *name,attrib[200];
char *words[200],*lp,*lp2,**wd;

Camera *cam=NULL;
int i,err,k;
char *posStr;
char line[2048]; 
int retVal = 0;  /* failure */
	RXSTRING tempstring;
	vector<RXSTRING> rxwords;
strcpy(line,e->GetLine());
wd=words;
if(CAMDBG) printf(" Resolve_Camera with \n<%s>\n", line);
strrep(line,'!','\0');

lp=strtok(line,RXENTITYSEPS);
i=0;
while((lp=strtok(NULL,RXENTITYSEPS))!=NULL) {
	words[i]=lp;
	PC_Strip_Leading(words[i]);
	PC_Strip_Trailing(words[i]);
	i++;
}
if(i < 7) 
    goto End;
if(i>10){
		strcpy(attrib,words[10]);
	}
	else
		strcpy(attrib,"none"); 
name = *wd;
wd++;
	cam = (Camera *) CALLOC(1,sizeof(Camera));

        if(!cam)
	    goto End;
	cam->name = STRDUP(name);
	posStr = *wd;	
	tempstring= TOSTRING(*wd);wd++;
	rxwords =  rxparsestring(tempstring, L", ");
	if(rxwords.size()==3)
		for(k=0; k<3 ;k++)
			cam->pos[k]=RXQuantity::OneTimeEvaluate(rxwords[k] ,L"m",(class RXENode*) e,&err);
	else cout<<"default camera pos at (0,0,0)" <<endl;

	 	
	tempstring= TOSTRING(*wd);wd++;
	rxwords =  rxparsestring(tempstring, L", ");
	if(rxwords.size()==3)
		for(k=0; k<3 ;k++)
			cam->target[k]=RXQuantity::OneTimeEvaluate(rxwords[k] ,L"m",(class RXENode*) e,&err);

 	tempstring= TOSTRING(*wd);wd++;
	rxwords =  rxparsestring(tempstring, L", ");
	if(rxwords.size()==3)
		for(k=0; k<3 ;k++)
			cam->Up[k]=RXQuantity::OneTimeEvaluate(rxwords[k] ,L"m",(class RXENode*) e,&err);

	tempstring= TOSTRING(*wd);wd++;
	cam->width=RXQuantity::OneTimeEvaluate(tempstring  ,L"m",(class RXENode*) e,&err);

	tempstring= TOSTRING(*wd);wd++;
	cam->height=RXQuantity::OneTimeEvaluate(tempstring  ,L"m",(class RXENode*) e,&err);

	cam->projection = STRDUP(*wd);

if(cam->width < 0.00001) cam->width=(float)0.00001;
if(cam->height< 0.00001) cam->height= (float)0.00001;

if( i >8) {
  	wd++;
  	cam->light_colour = STRDUP(*wd);

	lp2 =  cam->light_colour;
	while(*lp2) {
		if(*lp2=='$') *lp2=','; 
		lp2++;
	}

	 wd++;
	lp = strtok((*wd),", ");
	cam->light_pos.x = atof(lp);
	lp = strtok(NULL,", ");
	cam->light_pos.y = atof(lp);
	lp = strtok(NULL,", ");
	cam->light_pos.z = atof(lp);


}
else 
 	 cam->light_colour = NULL;

if(CAMDBG){ 
printf("\n camera\n%s\n", e->GetLine());
 printf(" pos %f %f %f\n", cam->pos.x, cam->pos.y, cam->pos.z); 
 printf(" tar %f %f %f\n", cam->target.x, cam->target.y, cam->target.z); 
 printf(" up  %f %f %f\n", cam->Up.x, cam->Up.y, cam->Up.z); 
 printf(" w,h %f %f %s\n", cam->width, cam->height, cam->projection); 
 printf(" light %f %f %f %s\n", cam->light_pos.x, cam->light_pos.y, cam->light_pos.z, cam->light_colour);
} 

	e->PC_Finish_Entity("camera",name,(void *)cam,(long)0,NULL,attrib,e->GetLine());


retVal = 1;	/* got here so OK ! */

End:
	if(retVal)  {
		e->Needs_Resolving= 0;
		e->Needs_Finishing = 0;
		e->SetNeedsComputing(0);

#ifdef HOOPS
		HC_Open_Segment("/cameras");
			e->hoopskey= HC_KOpen_Segment(e->name());

			HC_Set_Camera(&(cam->pos),&(cam->target),&(cam->Up), cam->width,cam->height,cam->projection);
			//Print_Camera(" Just after Set",".");

			HC_Control_Update(".","set created");
		HC_Close_Segment();
		HC_Close_Segment();

/* the following tricks HOOPS into redrawing the style segs */
	HC_Begin_Segment_Search("/.../camerabase");
	while(HC_Find_Segment(seg)) { 
		HC_QShow_One_Net_User_Option(seg,"petercam", buf);
		if(strieq(buf,e->name())) {
			HC_Open_Segment(seg);
				Print_Camera(" Resolve_Camera... window seg before", ".");
				HC_Flush_Contents(seg,"style,camera");
				HC_Style_Segment_By_Key(e->hoopskey);

if(CAMDBG)			printf(" styling %s as %s\n", seg, e->name());
				HC_Show_Segment(e->hoopskey, buf);		
				Print_Camera(" cam seg ", buf);
				Print_Camera(" window seg after ", ".");
			
				HC_Update_Display();
			HC_Close_Segment();
			/* should do via 	SetHardCamera(e,Graphic *g); */
		}
	else {
		if(CAMDBG) printf(" <%s> isnt <%s>\n", buf, e->name());
	}
	}

	HC_End_Segment_Search();
#endif
	}

	return(retVal);
}
int Record_Default_Camera(void){
char buf[512];
HC_KEY key;
#ifdef HOOPS
	if(!PCAM) {	
		PCAM = (Camera*)CALLOC(1, sizeof(Camera));
	}	
	HC_Show_Net_Camera( &(PCAM->pos),&(PCAM->target),&(PCAM->Up),
				&(PCAM->width),&(PCAM->height),buf);

	PCAM->projection=(char*)REALLOC( PCAM->projection,strlen(buf)+2);
	strcpy(PCAM->projection,buf);
	if(PCAM->light_colour) { 
		 RXFREE(PCAM->light_colour); 
		 PCAM->light_colour=NULL;
		}
	if(HC_QShow_Existence("distant_lights","self")) {
		HC_Open_Segment("distant_lights");
		HC_Begin_Contents_Search(".","distant lights");
			if(HC_Find_Contents(buf,&key)) { 
			HC_Show_Distant_Light(key,&(PCAM->light_pos.x),&(PCAM->light_pos.y),&(PCAM->light_pos.z));
			HC_QShow_One_Net_Color("^","lights",buf);
			PCAM->light_colour=STRDUP(buf);
			}
		HC_End_Contents_Search();

		HC_Close_Segment();
	}


/*	sprintf(buf,"petercam= pos(%f %f %f )  tar (%f %f %f )  up( %f %f %f)  (w %f h %f)  proj<%s>    light(%f %f %f) color<%s> ",  
	PCAM->pos.x, PCAM->pos.y, PCAM->pos.z,  PCAM->target.x, PCAM->target.y, PCAM->target.z,  
	PCAM->Up.x,PCAM->Up.y,PCAM->Up.z,PCAM->width, PCAM->height, PCAM->projection, 
	PCAM->light_pos.x,PCAM->light_pos.y,PCAM->light_pos.z, PCAM->light_colour);

 	HC_Set_User_Options(buf); */


/*	printf(" recording camera\npos      (%f %f %f)\n", PCAM->pos.x,PCAM->pos.y,PCAM->pos.z);
	printf(" target (%f %f %f)\n", PCAM->target.x,PCAM->target.y,PCAM->target.z);
	printf(" Up      (%f %f %f)\n", PCAM->Up.x,PCAM->Up.y,PCAM->Up.z);
	printf(" w,h     (%f %f)\n", PCAM->width,PCAM->height);
	printf(" Proj    (%s)\n", PCAM->projection);  */
#endif
return 1;
}

Camera *Transform_Camera(RXEntity_p cEnt) {
	Camera *cam = (Camera *)cEnt->dataptr;
	static Camera c;
	memcpy( &c, cam, sizeof(struct CAMERA));
	if(cEnt->Esail) {
		c.pos	= cEnt->Esail->XToGlobal(ON_3dPoint(c.pos));
 		c.target= cEnt->Esail->XToGlobal(ON_3dPoint(c.target));
 		c.Up	= cEnt->Esail->VToGlobal(ON_3dVector(c.Up));
	}

 return &c;
}
#ifdef _X
int SetHardCamera(RXEntity_p cEnt,Graphic *g)


/* cEnt NULL means apply the last camera (from  PCAM, a static where it lives) */
	HC_KEY segkey;
	Camera *cam;

	char buf[256];
	int alloc=0;

	if(!PCAM) {
		if(!cEnt){ cout<< "no default cam\n"<<endl; return 0;}
	}

	if(!cEnt) {
		cam = (Camera *)CALLOC(1,sizeof(Camera));
		memcpy(cam, PCAM, sizeof(Camera));
		cam->projection=STRDUP(PCAM->projection);
		if(PCAM->light_colour)
			cam->light_colour=STRDUP(PCAM->light_colour);
		alloc=1;	
		}
	else
		cam = (Camera *)cEnt->dataptr;


	segkey = g->m_ViewSeg;
	HC_Open_Segment_By_Key(segkey);
		if(cEnt){
			//char buf[256];
			sprintf(buf,"petercam=%s",cEnt->name());
			HC_Set_User_Options(buf); /* for use by Snapshot */
			HC_Open_Segment_By_Key(g->m_ModelSeg);
				HC_Set_User_Options(buf); /* for use by Snapshot */			
			HC_Close_Segment();	
            if(cEnt->AttributeFind("$transform")){
				printf("transforming camera '%s'\n",cEnt->name());
				cam = Transform_Camera(cEnt); // returns ptr to static
			}
		}
		 Record_Default_Camera(); 	

  		HC_Set_Camera(	&(cam->pos),&(cam->target),&(cam->Up),
					cam->width,cam->height,cam->projection);  

	if(cEnt) { 
		//char l_dbgstr[256];
		Arg al[64];
		int l_ac=0;
		HC_Flush_Contents(".", "style,camera");
		HC_Show_Segment(cEnt->hoopskey,buf);
	
 		HC_Style_Segment(buf);
		HC_Control_Update(".","set created");
		HC_Update_Display();

 		sprintf(buf,"%s:OfsiView",cEnt->name()); 
  		l_ac=0;
 		XtSetArg(al[l_ac],XmNtitle,buf); l_ac++;
 		XtSetArg(al[l_ac],XmNiconName,buf);l_ac++;
 		XtSetValues(g->topLevel,al,l_ac);


		}
	HC_Set_Camera(	&(cam->pos),&(cam->target),&(cam->Up),
					cam->width,cam->height,cam->projection);  

	HC_Open_Segment("distant_lights"); 
		HC_Flush_Contents(".","everything");
	 	 if(!(is_empty(cam->light_colour))) {
	   		 sprintf(buf,"lights=%s",cam->light_colour);
	   		 HC_QSet_Color("^", buf);
	   		 HC_Insert_Distant_Light(cam->light_pos.x,cam->light_pos.y,cam->light_pos.z);
			HC_QSet_Visibility("(^,.)","lights=on"); // oCT 2006  to try and override the hoops MVA lighting
	 	 }
	HC_Close_Segment();
/*	HC_Close_Segment();
	HC_Open_Segment_By_Key(g->axisSeg);

 			HC_Set_Camera(	&(cam->pos),&(cam->target),&(cam->Up),
					cam->width,cam->height,cam->projection);

		PC_Box_Size("axis*","text","viewpoint",
			    &xmax,&xmin,&ymax,&ymin,&zmax,&zmin);
		x = SURROUND*(xmax-xmin);
		y = SURROUND*(ymax-ymin);
		z = SURROUND*(zmax-zmin);
		xmax += x;ymax += y; zmax += z;
		xmin -= x;ymin -= y; zmin -= z;
		PCwin_Set_To_Box(xmin,xmax,ymin,ymax,zmin,zmax);
	HC_Close_Segment();*/

	if(alloc) {
		RXFREE(cam->projection); RXFREE(cam);
	}
	HC_Update_Display();
return(1);

}
#endif

