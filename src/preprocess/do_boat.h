/* defines for do_boat.c  */

#ifndef DO_BOAT_16NOV04
#define DO_BOAT_16NOV04


#define I_OFF    (21)
#define NAME_OFF (1)
#define BAS_OFF  (3)
#define E_OFF    (4)
#define P_OFF    (30)
#define J_OFF    (25)
#define RAKE_OFF (29)
#define FREEB_OFF (50)
#define LOA_OFF   (26)
#define PROF_LENGTH_OFF  (49)
#define PROF_WIDTH_OFF (49)
#define BG_OFF    (12)

#define BH_OFF    (13)
#define GRAKE_OFF  (53)
#define FOOT_OFF   (54)

#endif //#ifndef DO_BOAT_16NOV04

