//RX_integrate.cpp

#include "StdAfx.h"  
 
#include "RX_integrate.h"
 /* 
 To integrate we have to first define the function to integrate. Typically something like
Declaration
	EXTERN_C double  Integrand1 ( void* p, const double t);

Definition
	double  Integrand1 ( void *pp, const double t) 
	{
		RXPattern *p = (RXPattern*) pp;
		return p->phiDashed(t);
	}

Then to integrate this fn between limits x0 and x1 we'd go

	rv= RX_Integrate( Integrand1,(void*)this, x0,x1);
*/
double RX_Integrate(IFuncPtr func ,void* p, const double x0,const double x1, const double tol, double*perr){
double y0,y1,ymid,err,rv;
int depth=0;
	y0=func(p,x0);
	y1 = func(p,x1);
	ymid = (y0+y1)/2.0;
	rv =  RX_RIntegrate(func ,	p,	x0,	x1,	y0,	y1, ymid,tol,depth,	&err);
	if(perr) *perr=err;
	return  rv;
}

double  RX_RIntegrate(
	IFuncPtr fp ,
	void * p,
	const double x0,
	const double x1, 
	const double y0,
	const double y1, 
	const double ymid,  // quadratic estimate of the mid=point evaluation
	const double tol, 
	 int depth,
	double*err )
{
	double rv=0, x3,y3,dx,y03, y31,err1=0,err2=0;
	depth++;
	x3 = (x0 + x1)/2.;
	y3 = fp(p,x3);
	dx = (x1-x0);  // ATT double of the classical Simpsons notation
	rv = (y0 + y1 + 4. * y3) * dx/6.0 ; //WANT TO DIVIDE BY sdashed but where do we sample it"
	*err = (y3-ymid) * dx * 2./3.;
//	printf(" %f\t%f\t %d\t %f\n", x0,x1,depth,*err);
	if(depth >12) return rv;
	if(fabs(*err) < tol) return rv;
	y03 = (3.*y0 - y1 + 6.*y3)/8.;
	y31 = (-y0 + 3.*y1 + 6.*y3)/8.;
	rv   =  RX_RIntegrate(fp ,p,x0, x3, y0,y3, y03,tol, depth, &err1);
	rv  +=  RX_RIntegrate(fp ,p,x3, x1, y3,y1, y31,tol, depth, &err2);
	*err=err1 + err2;
return rv;
}

