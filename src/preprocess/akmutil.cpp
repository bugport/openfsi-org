/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :

 Sept 2000 fixed a bug.  AKM Insert Multiline text strtok'ed the arg, which is often a constant string
 Oct 98 replace_word returns null if no action. Also it is case-insensitive
  Dec 97 make_into_words buffer limit removed
 * verifyfilename moved from aeromik.c
 *   NAUGHTY!!!  stristr didnt free its memory
 * 26/2/96 is_directory doesnt terminate in MSW
  *  19/12	Ifdefs for MSVC compatibility
 *   22/8/95 printfs
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */
#include "StdAfx.h"
#include <QDir>
#include <QFileInfo>

#include "RelaxWorld.h"
#include "RXGraphicCalls.h"
#include "unixver.h" 

#include <ctype.h>

#include "words.h"
#include "text.h"
#include "files.h" // this is one of ours. thanks adam.
#include "akmutil.h"

#ifdef linux 
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#else

#include <direct.h>
#endif





#ifndef _HPUX_SOURCE /*   31Jan97 Adam  */
int is_directory(const char *path)  /* check if is a directory and is accesible */
{
#ifdef _NOT_MSW
    struct stat status;
    return(access(path,W_OK) ==0 &&    /* exists and is read/writable */
           stat(path,&status) ==0 &&  /* get the status */
           (status.st_mode & S_IFDIR)); /* is a directory */
#else

    char buf[256];
    if(!_getcwd(buf,255)) return(0);
    if(0==_chdir(path)) {
        _chdir(buf);
        return(1);
    }
    sprintf(buf,"Cant CD to \n%s",path);
    g_World->OutputToClient(buf,1);

    return(0);
#endif
}
#else

#if 0
int is_directory(char *path)  /* check if is a directory and is accesible */
{
    char buf[256];
    if(!_getcwd(buf,255)) return(0);
    if(0==_chdir(path)) {
        _chdir(buf);
        return(1);
    }
    sprintf(buf,"Cant CD to \n%s",path);
    rxerror(buf,1);

    return(0);

}
#else
int is_directory(char *path)  /* check if is a directory and is accesible */
{
    struct stat status;
    char *ret[]={"FALSE","TRUE"};

    int retVal = access(path,W_OK) ==0 &&    /* exists and is read/writable */
            stat(path,&status) ==0 &&  /* get the status */
            (status.st_mode & S_IFDIR);
    /* printf("HPUX is_directory  %s -> %s\n",path,ret[retVal]); */

    return(retVal);
}
#endif

#endif
int Increment_String (QString &value)
{
    int rc=0;
     // if it ends with an integer, increment the integer
    // else append '_00'
    QRegExp  rx("\\b(.+)(\\d+)\\b");
    int pos = rx.indexIn(value);
    QStringList list = rx.capturedTexts(); int nl = list.length();
    if(pos>=0 && nl>2){
        int k = list.back().toInt();
        value = list.at(1) + QString("%1").arg(k+1); rc++;
    }
    else {
        value+="+00";
    }
//    qDebug()<<" (increment_String)  on "<<value;
//    qDebug()<<" capture count = "<< rx.captureCount()<< "listLength = "<< rx.capturedTexts().count();
//    qDebug()<<" capture words = "<< rx.capturedTexts();
    return rc;
}

int VerifyFileName( QString filename,const QString prompt,const QString pattern)  {
    /* returns 1 if OK, 0 if user didnt select a file
 SIDE-EFFET. In MSW, it checks to see if the second char is a $
 if it is, it converts it to a backslash.
 This is because we file MS filenames with dollars not colons
 because the colon upsets our parsing.
 See write_one_line for the inverse
 */
    FILE *fp;
    char flocal[512];
    filename_copy_local(flocal,512,qPrintable(filename));
#ifdef WIN32
    char *lp = flocal;   lp++;
    if(*lp == '$')
        *lp = ':';
#endif
    fp = RXFOPEN(flocal,"r");
    if(fp) {
        FCLOSE(fp);
        return(1);
    }
    QString file2 = getfilename(NULL,flocal,prompt,pattern,PC_FILE_READ);
    if(file2.isEmpty())
        return(0);
    filename= file2 ;
    return(2);
}
int validfile(const QString &fname, const char* dir )
{
    return validfile(qPrintable(fname),dir);
}
int validfile(const std::string &fname, const char* dir )
{
    return validfile(fname.c_str(),dir);
}
int validfile(const char *pfname, const char* dir)
{
    FILE *fp;
    assert (OK_TO_READ   ==  1);    assert (OK_TO_WRITE  ==  0);    assert (BAD_FILENAME == -1);

    if(!pfname)         return(BAD_FILENAME);
    if(!pfname
            || strchr(pfname,'?')
            || strchr(pfname,'*') // Mar 2003
            || strchr(pfname,'@'))
        return(BAD_FILENAME);

    QString qfname(pfname);
    if(dir && !qfname.startsWith("$R") ) {
        QDir base(dir);
        qfname= base.absoluteFilePath ( qfname ) ;
        qfname=  QDir::cleanPath ( qfname  ) ;
    }
    if((fp = RXFOPEN(qPrintable(qfname),"r"))) {
        FCLOSE(fp);
        return(OK_TO_READ);
    }
    if((fp = RXFOPEN(qPrintable(qfname),"w"))) { // strangely, linux permits a '*' in a 'w' filename

        FCLOSE(fp);
        remove(qPrintable(qfname));
        return(OK_TO_WRITE);
    }
    return(BAD_FILENAME);
}



//static unsigned long akm_total=0;


void akm_insert_ink_(float *x,float *y, float *z)
{
    HC_Insert_Ink(*x,*y,*z);
}
int akm_restart_ink_()
{
    HC_Restart_Ink();
    return 0;
}

void akm_open_segment_(const char *namein)
{
    char name[256]; memset(name,0,256); strncpy(name,namein,255); name[255]='\0';  HC_Open_Segment(name);
}

void  akm_close_segment_(void)
{
    HC_Close_Segment();
}

int akm_delete_segment_(const char *seg)
{
    HC_Delete_Segment(seg);
    return 0;
}

int akm_insert_line_(float *x1, float *Y1, float *z1,float *x2, float *y2, float *z2,RXGRAPHICSEGMENT *k, double*h,double*s,double*v)
{

    RX_Insert_Line(*x1,*Y1,*z1,*x2,*y2,*z2,*k);
    return 0;
}

int akm_insert_marker_(float *x1, float *Y1, float *z1)
{
    // HC_Insert_Marker(*x1,*Y1,*z1);
    return 0;
}


int akm_set_color_(const char *col)
{
    HC_Set_Color(col);
    return 0;
}

int Akm_Set_Color_By_Value(const char*what,const char*type,float * a,float*b,float *c)
{
    HC_Set_Color_By_Value(what,type,*a,*b,*c);
    return 1;
}


int AKM_copy_file(char *s,char *d) {	

#ifdef linux
    int retVal=0;
    char *sdata;

    sdata = readdata(s);
    if(sdata) {
        if(writedata(d,sdata))
            retVal=1;
        RXFREE(sdata);
    }
    return(retVal);
#else
    assert(" AKM_copy_file"==0);
    return(0);
#endif
}

#ifdef HOOPS

void AKM_Insert_Multiline_Text(float x,float y, float z, const char *s_in)
{
    float offx,offy; /* assumes in x, z space !! */

    char *sz; char buf[512];
    char *s = STRDUP(s_in);

    sz = strtok(s,"\n");

    HC_Show_Pathname_Expansion(".",buf);

    while(sz && *sz) {

        HC_Compute_Text_Extent(".",sz,&offx,&offy);

        HC_Insert_Text(x,y,z,sz);
        offy = offy* (float) 1.75;
        y = y - offy;
        z = z - offy;
        sz = strtok(NULL,"\n");
    }
    RXFREE(s);
}
#endif
