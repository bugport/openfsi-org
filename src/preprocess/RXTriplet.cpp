/*
RXTriplet is incomplete because we keep a set of doubles
rather than a set of expressions
*/
#include "StdAfx.h"
#include "RXSail.h"
#include "RXQuantity.h"
#include "RXTriplet.h"
 
RXTriplet::RXTriplet(void)
{
	Init();
}
RXTriplet::RXTriplet( const double px,const double py,const double pz){
	x=px; y=py; z=pz; 
}
RXTriplet::RXTriplet(SAIL *sail,const RXSTRING &sx,const RXSTRING &sy,const RXSTRING &sz)
{
	//x =  Evaluate_ Quantity(sail,sx,L"length"); WRONG  need to make an expression and evaluate it.
	//y =  Evaluate_ Quantity(sail,sy,L"length");
	//z =  Evaluate_ Quantity(sail,sz,L"length");
	int err=0;
	class RXENode *nn = dynamic_cast<class RXENode *>(sail);
	x= RXQuantity::OneTimeEvaluate(sx  ,L"m",nn,&err); 
	y= RXQuantity::OneTimeEvaluate(sy  ,L"m",nn,&err);
	z= RXQuantity::OneTimeEvaluate(sz  ,L"m",nn,&err);

}




RXTriplet::~RXTriplet(void)
{
}

int RXTriplet::Init(void)
{
	x=y=z=ON_UNSET_VALUE; 


	 return 1;
}



//	RXTriplet& operator-=(const RXTriplet&);

RXTriplet& RXTriplet::operator-=(const RXTriplet& p)
{
  x -= p.x;
  y -= p.y;
  z -= p.z;
  return *this;
}


RXTriplet RXTriplet::operator*(const  double d ) const
{
		return RXTriplet(x*d,y*d,z*d);

}

RXTriplet RXTriplet::operator/(const double d ) const
{
  const double one_over_d = 1.0/d;
return RXTriplet(x*one_over_d,y*one_over_d,z*one_over_d			 );

}

RXTriplet RXTriplet::operator+( const RXTriplet& p ) const
{
return RXTriplet(x+p.x,y+p.y,z+p.z);

}

bool RXTriplet::Set(const ON_3dPoint &v) // model space
{
	x=v.x; y=v.y;z=v.z;
	return true;
}
int RXTriplet::SetX(const RXSTRING &p, class RXSail *sail)
{
    return  Set(sail,0,p);
}
int RXTriplet::SetY(const RXSTRING &p, class RXSail *sail)
{
    return  Set(sail,1,p);
}
int RXTriplet::SetZ(const RXSTRING &p, class RXSail *sail)
{
   return  Set(sail,2,p);
}

int RXTriplet::Set(class RXSail *sail,const int i, const RXSTRING &v){

	int err=0;
	class RXENode *nn = dynamic_cast<class RXENode *>(sail);
 
	double xxx = RXQuantity::OneTimeEvaluate(v  ,L"m",nn,&err); 


	switch (i) {
		case 0:
                        x=xxx; 	//SetX(xxx);
			return true;
		case 1:
                        y=xxx; //SetY(xxx);
			return true;
		case 2:
                        z=xxx;// SetZ(xxx);
			return true;
		default:
			return false;

	};
	return false;
}

const ON_3dPoint RXTriplet::ToONPoint(void) const
{
	ON_3dPoint r;
	r.x=x; r.y=y; r.z=z; 
	return r;
}
int RXTriplet::Print(FILE *fp){
	return fprintf(fp,"%S",this->TellMeAbout ().c_str());
}

RXSTRING RXTriplet::TellMeAbout(void)const{
  RXSTRING rc(_T("RXTriplet (  "));
	  rc+= TOSTRING(x) +_T(" ,");
	  rc+= TOSTRING(y) +_T(" ,");
	  rc+= TOSTRING(z) +_T(")\n");

  return rc;
}
