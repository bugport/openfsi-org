// 10 July 2003  strip_Trailing on w in Extract_Word_From_Att
// 22 aug 2003.  Now permit quotes " 
// the parse is [keyw][anything][=][optional whitespace or '"'] [retstring][]""){},]
// except that if the first non-white char after '=' is '"' the terminator must be a '"'
#include "StdAfx.h"
#include "RXAttributes.h"
#include <unixver.h>
#include <string>
using namespace std;
#include "stringutils.h"
#include "RelaxWorld.h"

#include "words.h"

#ifdef _CONSOLE

#define STRDUP strdup // ifdef CONSOLE
#define RXFREE free
#define MALLOC malloc
#define CALLOC calloc
#define REALLOC realloc
#endif


//int Extract_Float (const RXSTRING &att,const RXSTRING &keyw,float*v) {
//	double d;
//	int rval;
//	rval = Extract_Double (att, keyw,&d);
//	*v = (float) d;
//	return rval;
//}
//
//int Extract_Float(const char*att, const char*keyw,float*v) {
//	double d;
//	int rval;
//	rval = Extract_Double (att, keyw,&d);
//	*v = (float) d;
//	return rval;
//}

int Extract_Double  (const RXSTRING &att,const RXSTRING &keyw,double*v)  {
    /* somewhere in <att> there may be  a sequence <keyw [whitespace]=[whitespace]<double>	*/

    const char*sp ;
    char *s;
    wchar_t *stop;


    size_t i = att.find(keyw);
    if(i == wstring::npos) {
        *v=ON_UNSET_VALUE;
        return 0;
    }
    i+= keyw.length();
    i = att.find(L"=",i);
    if(i == wstring::npos) {
        *v=ON_UNSET_VALUE;
        return 0;
    }

    i++;

    RXSTRING w = att.substr(i); // [whitespace] something

    const wchar_t *lp = w.c_str();
    *v = wcstod(lp,&stop);
    return 1;
    /*
   if((sp =stristr(att,keyw))) {
   s = STRDUP(sp);
   sp = s + strlen(keyw);
// sp should now be [optional whitespace]=
   while(*sp && isspace(*sp)) sp++;

// that's got rid of the whitespace
   if(!(*sp)) { RXFREE(s); return 0;}


   if(*sp  != '=') {
   //	printf(" extract_double no equals %s  %s\n %s\n",keyw,att,sp);
    RXFREE(s);
    return 0;
   }
   *sp++;
// sp should now be [optional whitespace][a number] [optionally something else]
   while(*sp && isspace(*sp)) sp++;

// sp should now be [a number] [optionally something else]

   *v = strtod(sp,&stop);

   RXFREE(s);
   return(1);

   }
   else {
   *v=0.0;
   return(0);
  }
   */
}



int Extract_Double (const char*att, const char*keyw,double*v) {
    /* somewhere in <att> there may be  a sequence <keyw [whitespace]=[whitespace]<double>	*/

    const char*sp ;
    char *s;
    char *stop;
    //if(g_janet)
    //{assert(str_islower(att)); assert(str_islower(keyw)); }
    if((sp =stristr(att,keyw))) {
        s = STRDUP(sp);
        sp = s + strlen(keyw);
        // sp should now be [optional whitespace]=
        while(*sp && isspace(*sp)) sp++;

        // that's got rid of the whitespace
        if(!(*sp)) { RXFREE(s); return 0;}


        if(*sp  != '=') {
            //	printf(" extract_double no equals %s  %s\n %s\n",keyw,att,sp);
            RXFREE(s);
            return 0;
        }
        *sp++;
        // sp should now be [optional whitespace][a number] [optionally something else]
        while(*sp && isspace(*sp)) sp++;

        // sp should now be [a number] [optionally something else]

        *v = strtod(sp,&stop);

        RXFREE(s);
        return(1);

    }
    else {
        *v=0.0;
        return(0);
    }
}

int Extract_Integer (const char*att, const char*keyw,int*v) {
    /* somewhere in <att> there may be  a sequence <keyw [whitespace]=[whitespace]<double>	*/
    const char* sp0;
    char*s,*t;
    char *sp;

    if((sp0 =stristr(att,keyw))) {
        s = STRDUP(sp0);
        sp = s + strlen(keyw);
        t = strtok( sp," \t=([{}])");
        *v = atoi(t);
        RXFREE(s);
        return(1);
    }
    else {
        *v=0;
        return(0);
    }

}

int make_into_words(char *line,char ***wd,const char *sep_toks)
{
    /* NOTE: you must free the char ** returned BUT must not free the contents !!
   it just points to the original string !!!
 */
    int i=0;
    char **words=NULL;
    long int asize=0, bsize=16;
    char *lp;

    lp=strtok(line,sep_toks);
    while(lp) {
        if(i >= asize) {
            words= (char **) REALLOC(words, bsize*sizeof(char*));
            if(!words) {
                //	error(" too many words in dataset",2);
                return 0;
            }
            asize=bsize;
            bsize=bsize*2;
        }
        words[i]=lp;
        PC_Strip_Leading(words[i]);
        PC_Strip_Trailing(words[i]);

        i++;
        lp = strtok(NULL,sep_toks);
    }

    if(i>0 && *(words[i-1]) =='\000') {
        printf(" blank last word (%d) skipped. First was <%s>\n", i-1, words[0]);
        if(i>1)
            printf(" previous=<%s>\n",words[i-2]);
        i--;
    }

    (*wd) = (char **) CALLOC(i+1,sizeof(char *));// one more so we are null-terminated
    if(! (*wd))  g_World->OutputToClient("Out Of Memory - Crash",3);
    memcpy((*wd),&(words[0]),i*sizeof(char *));
    if(words) RXFREE(words);
    return(i);
}


int Replace_Words(char **line,int Index, char **win, int ls, const char*seps){
    //	*line is the string to modify
    //	index is the index of the word in line to start replacing
    //  win is a NULL-terminated list of words
    //	ls is the total length of all the words in win
    //	seps are the separators to be used in parsing '*line'

    size_t i,c, bl;
    char**words=NULL;
    char*buf ;
    char**w = win;
    bl = strlen(*line) ;
    c = make_into_words(*line,&words,seps);
    bl = bl + ls + 2*(max(Index,(int)c)+1 );
#ifdef WIN32
    printf("Replace Word %d buflen is %Iu ....  ",Index,bl);
#else
    printf("Replace Word %d buflen is %zd ....  ",Index,bl);
#endif


    buf = (char*)MALLOC(bl);
    *buf=0;

#ifdef WIN32
    printf("before. %Iu words... \n",c);
#else
    printf("before. %zd words... \n",c);
#endif

    for(i=0;i<c;i++) {
#ifdef WIN32
        printf(" word %Iu '%s' ", i, words[i]);
#else
        printf(" word %zd '%s' ", i, words[i]);
#endif

        if(i==Index) {

            strcat(buf, (*w));
            printf(" -> '%s'\n", *w);
            w++;
            if(*w)
                Index++; // use index as a flag to repace the next one too
        }
        else {
            strcat(buf, words[i]);
            cout<< "\n"<<endl;
        }
        strcat(buf," :");
        if(strlen(buf) > (size_t) bl)
            cout<< "overflow"<<endl;  //error("overflow",3);
    } //for i
    if( Index>=c) {
        for(i=c;i<Index;i++) {			// pad with blanks
#ifdef WIN32
            printf(" word %Iu ' ' \n", i );
#else
            printf(" word %zd ' ' \n", i );
#endif
            strcat(buf," :");
            if(strlen(buf) > (size_t)bl)
                cout<< "overflow"<<endl;  //error("overflow",3);
        }
        while(*w) {
#ifdef WIN32
            printf(" word %Iu ' ' -> '%s'\n",i++, *w);
#else
            printf(" word %zd ' ' -> '%s'\n",i++, *w);
#endif


            strcat(buf, *w);
            strcat(buf, " :");
            if(strlen(buf) > (size_t)bl)
                cout<< "overflow"<<endl;  //error("overflow",3);
            w++;
        }
    }
    i=strlen(buf)-1;
    printf(" space alloced = %d, used=%d\n",(int)bl, (int)i+1);
    buf[i]=0;
    RXFREE(*line);
    *line=STRDUP(buf);
    RXFREE(buf);
    RXFREE(words);
    return 1;
}

int Replace_Word(char **line,int Index, const char *w, const char*seps){
    int i,c;
    size_t bl;
    char**words=NULL;
    char*buf ;
    bl = strlen(*line) ;
    c = make_into_words(*line,&words,seps);
    bl = bl + strlen(w) + (max(Index,c) +1)*(4)  + 10;
    //	printf("Replace Word %d buflen is %d ....  ",Index,bl);

    buf = (char*)MALLOC(bl);
    *buf=0;


    //	printf("before. %d words... ",c);
    for(i=0;i<c;i++) {
        if(i==Index) {
            //printf(" replace word '%s' ", words[i]);
            strcat(buf, w);
            //printf(" with '%s'\n", w);
        }
        else strcat(buf, words[i]);
        strcat(buf," :");
        if(strlen(buf) > (size_t) bl) g_World->OutputToClient("overflow",3);
    } //for i
    if( Index>=c) {
        for(i=c;i<Index;i++) {
            strcat(buf," :");
        }
        strcat(buf, w);
        strcat(buf, "  ");
    }
    buf[strlen(buf)]=0;
    RXFREE(*line);
    *line=STRDUP(buf);
    RXFREE(buf);
    RXFREE(words);

    return 1;
}

char *Replace_String_Multiple(char **s,char *w1,char *w2) {
    const char *p;
    char *s2,*lp,*llp,*p2 = NULL;
    size_t l1,l0,l2,c=9;

    lp = *s;
    l0 = strlen(*s);
    l1=strlen(w1) ;
    l2=strlen(w2) ;
    llp = s2 = (char*)MALLOC(sizeof(char)*(l0+l1+10*(l2-l1) +32)); /* 3 is for \0 at end of */
    /* each of the strings*/

    while((p= stristr(lp,w1)) ) {
        if (!(c--)) break;
        while(lp !=p) {
            *llp = *lp;
            lp++;llp++;
        }
        strcpy(llp,w2);
        lp += l1;
        strcat(llp,lp); llp += l2;
    }

    RXFREE(*s);
    p2 = *s=STRDUP(s2);
    RXFREE(s2);
    return(p2);
}

extern  string Replace_String_Multiple(string &s,const string w1,const string w2){
    assert("please step this"==0);
    cout<<"Replace_String on ' "<<s<<"' with '"<<w1<<"' to '"<<w2<<"'\n"<<endl;
    size_t p;
    while(p = s.find(w1) != string::npos)
        s.replace(p,w1.length(),w2);
    cout << " gives  <"<<s<<">"<<endl;
    return s;
}
RXSTRING Replace_String(RXSTRING &s,const RXSTRING w1,const RXSTRING w2){

    size_t p = s.find(w1);
    if(p==string::npos)
        return s;
    s.replace(p,w1.length(),w2);
    return s;
}
string Replace_String(string &s,const string w1,const string w2){

    size_t p = s.find(w1);
    if(p==string::npos)
        return s;
    s.replace(p,w1.length(),w2);
    return s;
}

char *Replace_String(char **s,const char *w1,const char *w2) 
{
    char *p,*s2,*lp,*llp,*p2 = NULL;
    size_t l1,l2,l3;
    //printf("Replace_String on '%s' with '%s' to '%s'\n", *s,w1,w2);

    lp = *s;
    l2 = strlen(*s);
    l1=strlen(w1) ;
    llp = s2 = (char*)MALLOC(sizeof(char)*(l2+l1+strlen(w2)+3)); /* 3 is for \0 at end of */
    /* each of the strings*/
    p = stristr(*s,w1);
    if(p) {
        while(lp !=p) {
            *llp = *lp;
            lp++;llp++;
        }
        strcpy(llp,w2);
        lp += l1;
        strcat(llp,lp);
        //printf("p = '%s'\n",p);
        l3=p-(*s);
        RXFREE(*s);

        p2 = *s=STRDUP(s2);
        p2+=l3;
        //printf("P2 is '%s'\n", p2);
    }
    RXFREE(s2);
    return(p2);
}



#ifdef _CONSOLE_WORD  // changed july 2002 so we can debug optimise in MSVC
int main(int argc, char**Argv) {

    int  k=0,  nw=0,  i=0, ls;
    char **words=NULL,*s=NULL, *data=NULL;

    s = strdup(Argv[2]); // the edit string
    data = strdup(Argv[1]); // the data to edit

    printf("Do EditWord. line =<%s>\n",data);
    printf(" editstring = <%s>\n",s);
    nw = make_into_words(s,&words,"$:\t");

    printf("back from make_into_words with %d\n",nw);
    if(nw>4) {

        i = atoi(words[4]); // index of the first word to replace
        printf("first 4  %s  %s  %s  %s (%d) \n", words[1] ,words[2],words[3],words[4],i);


        printf("old line is <%s>\n. edit line has %d words", data,nw);
        ls = strlen(Argv[2]) - (words[5] - s);
        printf("space needed = %d\n",ls );
        Replace_Words(&data,i, &(words[5]),ls, ":");


    }
    printf(" data is now \n%s>", data);
    cout<< " DONE\n"<<endl;
    return 1;
}
#endif
