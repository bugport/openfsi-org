// CheckAll.h declarations and prototypes for the CheckAll.cpp

#ifndef _CHECKALL_H_
#define _CHECKALL_H_
#ifdef NEVER // to see why
EXTERN_C int UpdateAllSolved(const char *seedtype);
EXTERN_C int CheckAllSliding(void);

EXTERN_C int CheckAll_SSD(void);

#endif
EXTERN_C int AKM_Each_Cycle(void);


#endif //#ifndef _CHECKALL_H_

