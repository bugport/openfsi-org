#-------------------------------------------------
#
# Project created by QtCreator 2012-01-11T23:09:32
#
#-------------------------------------------------
QT       -=  gui
TARGET = stringtools
TEMPLATE = lib
CONFIG += staticlib

include (../../openfsicompilerconfig.pri)

DEFINES +=  RXLIB
#######DEFINES +=  RLX_64
######DEFINES +=  RX_ICC

win32-g++ {
    DEFINES +=_GNU_SOURCE
}

INCLUDEPATH += ../preprocess
INCLUDEPATH += ../../thirdparty/opennurbs
INCLUDEPATH += ../../thirdparty/MTParserLib


# Input
HEADERS += ConvertUTF.h  stringtools.h UtfConverter.h \
    ../../thirdparty/opennurbs/opennurbs.h

SOURCES += ConvertUTF.c stringparse.cpp UtfConverter.cpp

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}


