#include "StdAfx.h"
#include <string>
#include "UtfConverter.h"
#include <iostream>
using namespace std;
#include "ConvertUTF.h"

namespace UtfConverter
{

std::wstring FromUtf8ByString(const std::string& utf8string) // by string
	{
		size_t widesize = utf8string.length();
		if (sizeof(wchar_t) == 2)
		{
			std::wstring resultstring;
			resultstring.resize(widesize+1, L'\0');
			const UTF8* sourcestart = reinterpret_cast<const UTF8*>(utf8string.c_str());
			const UTF8* sourceend = sourcestart + widesize;
			UTF16* targetstart = reinterpret_cast<UTF16*>(&resultstring[0]);
			UTF16* targetend = targetstart + widesize;
			ConversionResult res = ConvertUTF8toUTF16
				(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			if (res != conversionOK)
			{
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif

			}
			*targetstart = 0; 
			return resultstring;
		}
		else if (sizeof(wchar_t) == 4)
		{
			std::wstring resultstring;
			resultstring.resize(widesize+1,L'\0');
			const UTF8* sourcestart = reinterpret_cast<const UTF8*>(utf8string.c_str());
			const UTF8* sourceend = sourcestart + widesize;
			UTF32* targetstart = reinterpret_cast<UTF32*>(&resultstring[0]);
			UTF32* targetend = targetstart + widesize;
			ConversionResult res = ConvertUTF8toUTF32
				(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			if (res != conversionOK)
			{
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
			}
		 	*targetstart = 0;
			return resultstring;
		}
		else
		{
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
		}
		return L"";
}
 std::wstring FromUtf8(const std::string& instring) // this one uses narrow/append
{
    size_t k, insize = instring.length();
//cout << "Fromutf8 " << instring;
	wchar_t wt; char c;
    std::wstring resultstring;
    for(k=0;k<insize;k++) {
			c = instring[k];
			wt = wcin.widen(c);
			resultstring.append(1,wt);
	}
return resultstring;
} //  std::string ToUtf8  this one uses narrow
 std::string ToUtf8(const std::wstring& widestring) // this one uses narrow/append
{
	size_t k, widesize = widestring.length();
	wchar_t wt; char c;
	std::string resultstring;
	for(k=0;k<widesize;k++) {
			wt = widestring[k];
			c = wcin.narrow(wt,'X');
			resultstring.append(1,c);
	}
//	resultstring.append(1,0); aug 20 try without
return resultstring;
} //  std::string ToUtf8  this one uses narrow


std::string ToUtf8_ND(const std::wstring& widestring) // this one uses new and delete
    {
        size_t widesize = widestring.length();
		wcout << L"ToUtf8_ND input = <" << widestring<<L">.. length is " << widesize <<endl;
        if (sizeof(wchar_t) == 2)
        {
            size_t utf8size = 3 * widesize + 1;
            char* utf8stringnative = new char[utf8size];
            const UTF16* sourcestart = reinterpret_cast<const UTF16*>(widestring.c_str());
            const UTF16* sourceend = sourcestart + widesize;
            UTF8* targetstart = reinterpret_cast<UTF8*>(utf8stringnative);
            UTF8* targetend = targetstart + utf8size;
            ConversionResult res = ConvertUTF16toUTF8(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
            if (res != conversionOK)
            {
                delete [] utf8stringnative;
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
            }
            *targetstart = 0;
            std::string resultstring(utf8stringnative);
            delete [] utf8stringnative;
            return resultstring;
        }
        else if (sizeof(wchar_t) == 4)
        {
            size_t utf8size = 4 * widesize + 1;
            char* utf8stringnative = new char[utf8size];
            const UTF32* sourcestart = reinterpret_cast<const UTF32*>(widestring.c_str());
            const UTF32* sourceend = sourcestart + widesize;
            UTF8* targetstart = reinterpret_cast<UTF8*>(utf8stringnative);
            UTF8* targetend = targetstart + utf8size;
            ConversionResult res = ConvertUTF32toUTF8(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
		cout << "(wchar_t is 4) . result <" << utf8stringnative<<">"<<endl;
            if (res != conversionOK)
            {
		cout << " But there's an exception" << endl;
                delete [] utf8stringnative;
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
            }
            *targetstart = 0;
            std::string resultstring(utf8stringnative);
            delete [] utf8stringnative;
		cout << "returning  <" << resultstring<<">"<<endl;
            return resultstring;
        }
        else
        {
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
        }
        return "";
    } //  std::string ToUtf8  this one uses new and delete



std::string ToUtf8byString(const std::wstring& widestring) // the original- by string
	{
		size_t widesize = widestring.length();

		if (sizeof(wchar_t) == 2)
		{
			size_t utf8size =  3 * widesize + 1; 
			std::string resultstring;
			resultstring.resize(utf8size, '\0');
			const UTF16* sourcestart =
				reinterpret_cast<const UTF16*>(widestring.c_str());
			const UTF16* sourceend = sourcestart + widesize;
			UTF8* targetstart = reinterpret_cast<UTF8*>(&resultstring[0]);
			UTF8* targetend = targetstart + utf8size; // peter did widesize, was utf8size;
			ConversionResult res = ConvertUTF16toUTF8
				(&sourcestart, sourceend, &targetstart, targetend, strictConversion);
			cout << "(wchar_t is 2) . result <" << resultstring<<">"<<endl;
			if (res != conversionOK)
			{
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
			}
			*targetstart = 0;
			return resultstring;
		}
		else if (sizeof(wchar_t) == 4)
		{
			size_t utf8sizE = 4 * widesize + 1;
			std::string resultstring;
			resultstring.resize(utf8sizE, '\0');
			const UTF32* sourcestart = 
				reinterpret_cast<const UTF32*>(widestring.c_str());
			const UTF32* sourceend = sourcestart + widesize;
			UTF8* targetstart = reinterpret_cast<UTF8*>(&resultstring[0]);
			UTF8* targetend = targetstart +widesize; // peter did widesize, was  utf8size;
			ConversionResult res = ConvertUTF32toUTF8
				(&sourcestart, sourceend, &targetstart, targetend, strictConversion);

			if (res != conversionOK)
			{
			cout << " But there's an exception" << endl;
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
			}
			*targetstart = 0;
			cout << "(wchar_t is 4) . result <" << resultstring<<">"<<endl;
			return resultstring;
		}
		else
		{
#ifndef MICROSOFT
				throw std::exception();
#else
				throw std::exception("La falla!");
#endif
		}
		return "";
	}
}
