// AMGTapingZone.h: interface for the AMGTapingZone class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGTapingZone_DEC2005
#define AMGTapingZone_DEC2005


#include "AMGHoopsObject.h"
#include "AMGZoneCycle.h"
#include "AMGTape.h"

#include "opennurbs.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class dovestructure;
class AMGZoneNode;
class AMGZoneEdge;

class AMGTapingZone : public  AMGHoopsObject
{
public:

/*virtual	int SetWeights(const double &lengthWeight, 
		const double &AreaWeight, 
		const double &MOIWeight,
		const double &FreeEdgeWeight ,
		const double &AngleWeight, 
		const int p_sh_pow);*/

virtual	int SetWeights(const double &lengthWeight, 
		const double &AreaWeight, 
		const double &MOIWeight,
		const double &FreeEdgeWeight ,
		const double &AngleWeight, 
		const double &ThkWeight, 
		const int p_sh_pow);

	virtual void SetStackType(const int type);
	int IsNearACorner(const double u, const double v);
	double GetMaxLength();
	double GetMinLength();
	double GetTapeWidth();
	int DeleteFromBookmark();
	int SetBookmark();
	AMGZoneNode * CreateZoneNode(AMGZoneEdge *prev,
								 AMGZoneEdge *next, 
								 AMGZERef *p_whereami,
								 const int pFlag);
	AMGZoneEdge * CreateZoneEdge(AMGZoneNode*from, AMGZoneNode*to, ON_Curve*c, int pFlag);
	int DeleteZoneEdge(AMGZoneEdge*n);
	AMGZERef* CreateZER( AMGZoneEdge *theEdge, double t2 , int Flag ,AMGTape*theTape ); 
	int DeleteZER(AMGZERef *p_zr);
	int Print(FILE *fp);
	int SetParameters(const double aerr,const double mlo,
		const double minL,const double MaxL,const double w,
		const double buttOverlap,const double maxGap,const double DownHillLimit);
	int IsInside(const ON_3dPoint q);
	virtual int MakeTapes(const AMGLevel level);

	AMGTapingZone();

	AMGTapingZone(const ON_String & p_path,
			const ON_String & p_name,
			ON_Curve * p_boundary,
			dovestructure * p_dove);

	virtual ~AMGTapingZone();
//	ON_SimpleArray<AMGTape *>  *GetTapeList( ){ if(!m_z) return 0; return &(m_z->m_tapelist);}	
	ON_SimpleArray<AMGTape *>  *GetTapeList( );

	AMGZoneCycle *m_z;  // the active zonecycle.
	ON_SimpleArray<AMGTape *> m_tapelist; // unstructured list of ALL tapes in this layer.
	ON_SimpleArray<AMGZoneEdge *>	m_edges;
	dovestructure * GetDove() { if(m_z) return (m_z->m_dove); return 0;} 

	int Append(AMGZoneNode *p_p);
	int Append(AMGZoneEdge *p_p);
	int Append(AMGZERef *p_p) ;


protected:
	ON_SimpleArray<AMGZoneNode *>	m_nodes;
	ON_SimpleArray<AMGZERef *>  	m_zerefs;
	int DeleteZoneNode(AMGZoneNode*n);

	dovestructure * m_dove;

#define NPARMS 8
	double m_parms[NPARMS];
#define  ALPHA		(m_parms[0])
#define  MAXLATOV	(m_parms[1])
#define  MINLEN		(m_parms[2])
#define  MAXLEN		(m_parms[3])
#define  TWIDTH		(m_parms[4])
#define  BUTTOVL	(m_parms[5])
#define  MAXGAP 	(m_parms[6])
#define  DOWNHILL 	(m_parms[7])
};

#endif // AMGTapingZone_DEC2005
