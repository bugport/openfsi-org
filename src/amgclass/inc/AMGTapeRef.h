// AMGTapeRef.h: interface for the AMGTapeRef class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGTapeRef_JUNE2006
#define AMGTapeRef_JUNE2006

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGTapeRef : public  AMGHoopsObject
{
public:

	AMGTapeRef();
	AMGTapeRef(const ON_String & p_path,
		   const ON_String & p_name, 
		   const double & p_val = 0);
	
	virtual ~AMGTapeRef();

	
	int SetDoc(CDocument * p_pdoc);

	int SetYarnType(const ON_String & p_str);
	int SetGlue(const ON_String & p_str);
	int SetExtra(const ON_String & p_str);
	int SetMaterialCode(const ON_String & p_str);
	int SetWidth(const double & p_val);
	int SetLinearMass(const double & p_val);
	int SetNbYarns(const int & p_val);

	ON_String GetYarnType() const;
	ON_String GetGlue() const;
	ON_String GetExtra() const;
	ON_String GetMaterialCode() const;
	double GetWidth() const;
	double GetLinearMass() const;
	int GetNbYarns() const;

	//returns the modulus of the tape in N/m
	int GetModulus(double & p_val) const;

protected: 
	CDocument * m_pdoc;
};

#endif // AMGTapeRef_DEC2005
