

// CDoveHoopsView.h : interface of the CDoveHoopsView class, derived from CSolidHoopsView
//PHA March 07


#if !defined(AFX_CDOVEHOOPSVIEW_H_MARCH_07)
#define AFX_CDOVEHOOPSVIEW_H_MARCH_07
#ifdef PETERSMSW
#error "dont compile this CDoveHoopsView in peters test frame"
#endif

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// CDoveHoopsView.h : header file
//
#pragma message("this CDoveHoopsView.h has GetHNetClientMgr() and RXFileOpenMessageHandler.h ")

#include "CSolidHoopsView.h"
#include "RXFileOpenMessageHandler.h"


////////////////////////////////////////////////////////////////////////////////
// CDoveHoopsView view

class CDoveHoopsView : public CSolidHoopsView
{
protected:
	CDoveHoopsView();           // protected constructor used by dynamic creation
//	virtual void OnPaint();
	DECLARE_DYNCREATE(CDoveHoopsView)

	//HStreamFileReadingThread is a friend that communicates back and forth with this
	//class via the following two variables, m_cease and m_stream_lock

// Attributes
public:

	void  OnFileSaveAs() ;
	void  OnFileSave() ;
	
	bool CopySafe() ;
 // Operations
public:
#ifdef PETERSMSW
	HNetClientMgr * GetHNetClientMgr ();
#endif
 

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDoveHoopsView)
	public:
	virtual void OnInitialUpdate();
//	virtual void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
//	virtual void OnSaveRedline1();
//	virtual void OnSaveRedline2();
//	virtual void OnSaveRedline3();
 
	protected:
	

	virtual void OnFileLoad() ;
	virtual void OnViewAnimate() ;
//	virtual void OnPrint(CDC* pDC, CPrintInfo* pInfo);
//	virtual void OnActivateView(BOOL bActivate, CView* pActivateView, CView* pDeactiveView);
//	virtual void OnNcDestroy();
//	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

private:

// Implementation
protected:
	virtual ~CDoveHoopsView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

  
	// Generated message map functions
protected:
	//{{AFX_MSG(CDoveHoopsView)
		afx_msg void OnRButtonDown(UINT nFlags, CPoint point); // dec3 2006
		afx_msg void OnDoveToolsInfo();
		afx_msg void OnDoveToolsFilterAll();
		afx_msg void OnDoveToolsFilterThk();
		afx_msg void OnDoveToolsFilterKy();
		afx_msg void OnDoveToolsFilterKg();
		afx_msg void OnDoveToolsFilterAng();
		afx_msg void OnDoveToolsFilterNu();
		afx_msg void OnDoveToolsAddScrim();
		afx_msg void OnDoveToolsNoAddScrim();
		afx_msg void OnDoveToolsRebuildNu();
		afx_msg void OnDoveToolsRebuildKg();
		afx_msg void OnDoveToolsDrawTraceFast();
		afx_msg void OnDoveToolsDrawTraceSlow();

		afx_msg void OnDoveToolsZeroOrientation();
		afx_msg void OnDoveToolsViewOptions();
		afx_msg void OnDoveToolsRebuild();
		afx_msg void OnDoveToolsStyleEdit();
		afx_msg void OnDoveTools();


		afx_msg  void OnDoveThicknessEdit();
		afx_msg  void OnDoveKeEdit();
		afx_msg  void OnDoveKgEdit();
		afx_msg  void OnDoveNuEdit();
		afx_msg  void OnDoveAngleEdit();
		afx_msg  void OnDoveAngleEdit_NW();
		afx_msg  void OnDoveAngleEdit_NE();
		afx_msg  void OnDoveAngleEdit_SW();
		afx_msg  void OnDoveAngleEdit_SE();

		afx_msg void OnUpdateDoveAngleEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveThicknessEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveKeEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveKgEdit(CCmdUI* pCmdUI);
		afx_msg void OnUpdateDoveNuEdit(CCmdUI* pCmdUI);


	//}}AFX_MSG


#ifdef HOOPS_ONLY // Peter
	afx_msg  void OnDoveEdgeEdit();
	afx_msg  void  OnDoveXSEdit();
	afx_msg  void OnDoveEmit();

#else


 
#endif // HOOPS_ONLY

	bool SaveFile( LPCTSTR csFilePathName );
	void LoadFile( LPCTSTR csFilePathName, HStreamFileToolkit* tk, bool isHNetServerStreaming) ;

	DECLARE_MESSAGE_MAP()

	
   
};



 
/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CDOVEHOOPSVIEW_H_MARCH_07)
