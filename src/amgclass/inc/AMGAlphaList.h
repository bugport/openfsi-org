// AMGAlphaList.h: interface for the AMGAlphaList class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGAlphaList_DEC2005
#define AMGAlphaList_DEC2005

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGAlphaList : public  AMGHoopsObject
{
public:
	ON_2dPoint GetAlpha(const int & p_id) const;
	int GetThickness() const;

	AMGAlphaList();
	AMGAlphaList(const ON_String & p_path,
		   const ON_String & p_name, 
		   const double & p_val = 0);
	
	virtual ~AMGAlphaList();

	AMGAlphaList& operator=(const AMGAlphaList&);

	int SetUv(const double & p_u,const double & p_v);
	int SetSegmentTapes(const ON_String & p_seg);
	int SetBlank(const ON_Surface * p_surf);

	int ComputeAlphaList(const int & p_viewKey,
						 const ON_String & p_scene, 
						 const double & p_MaxThickness); //Stop counting when p_MaxThickness is reached

	ON_2dPointArray GetAlphaList();
protected: 
	ON_2dPoint m_uv;
	

	// for each point x == the value of alpha, y == the thickness of the tape (normal Tape == 1 X plies == 0.33)
	ON_2dPointArray m_Alphas; 

	ON_String m_segTapes; 
	const ON_Surface * m_pblank;

};

int DoubleCompare(const double * a , const double * b);
int Point2dCompare(const ON_2dPoint * a , const ON_2dPoint * b);


#endif // AMGAlphaList_DEC2005
