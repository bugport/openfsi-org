// AMGVlink.h: interface for the AMGVlink class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGVLINK_H__C5FCD42B_B05B_4D41_9B68_93C37803D8C3__INCLUDED_)
#define AFX_AMGVLINK_H__C5FCD42B_B05B_4D41_9B68_93C37803D8C3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "opennurbs_array.h"
#include "AMGVertex.h"

class AMGPolygon;
class AMGVlink;
class AMGVertex;

class AMGVlinkIntersection  : public layerobject
{
//	friend AMGVertex;
//	friend AMGVlink;
public:
	int Print(FILE *fp);
	AMGVertex * m_v;
	AMGVlinkIntersection();
	AMGVlinkIntersection(const AMGVlink*e1,const AMGVlink*e2, const double t1, const double t2 );
	virtual ~AMGVlinkIntersection();

	void Set(const AMGVlink*e1,const AMGVlink*e2, const double t1, const double t2 ,ON_3dPoint &p_p);
static	int PrintList(ON_ClassArray<AMGVlinkIntersection> &p_Ints,FILE *fp) ;
static	int PrintList(ON_ClassArray<AMGVlinkIntersection *> &p_pInts,FILE *fp) ;

	AMGVertex *SSDCreateVertex(ON_ClassArray<AMGVlinkIntersection> &p_Ints) ;

	const AMGVlink *m_e1, *m_e2;
	double m_t1,m_t2;
	ON_3dPoint  m_p;

};

class AMGVlink  : public layerobject

{
	friend class AMGVertex;
public:
	int IntersectionCount(ON_ClassArray<AMGVlinkIntersection> p_Ints);
	int GetIsInOther()const;
	double GetLength()const;
	int Print(FILE *fp) const;
	void SetIsInOther(int i);
	AMGPolygon *GetOwningPolygon()const;
	AMGVertex  *OtherEnd(const AMGVertex *v1)const;
	AMGVlink(AMGPolygon*p, AMGVertex *v1, AMGVertex *v2);
	AMGVlink();
	virtual ~AMGVlink();

protected:// ephemeral members.  Set (by AMGPolygon::PrepareBoolean) and used for Boolean operations
	AMGVertex * CommonVertex(AMGVlink *);
	int m_IsinOther;

private: 	// permanent members.  There are set at polygon creation time - by AMGPolygon::Connect
	AMGPolygon* m_p;
	AMGVertex * m_v2;
	AMGVertex * m_v1;

};

#endif // !defined(AFX_AMGVLINK_H__C5FCD42B_B05B_4D41_9B68_93C37803D8C3__INCLUDED_)
