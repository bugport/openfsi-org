// AMG2DEdge.h: interface for the AMG2DEdge class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMG2DEdge_DEC2005
#define AMG2DEdge_DEC2005

#include "AMGHoopsObject.h"
//#include "Z:\build\cpp\on_relax\opennurbs_point.h"	// Added by ClassView


#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#include "opennurbs_extensions.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMG2DEdge : public  AMGHoopsObject
{
public:
	double GetCord() const;
	int Regenerate();

	int ExportToRelax(ONX_Model & p_3dmModel);
	ON_PolylineCurve GetEdgeAsPolyline(const int & p_count) const;
	ON_2dPoint PointAt(const double & p_pos) const;
	AMG2DEdge();
	AMG2DEdge(const ON_String & p_model,
				   const ON_String & p_name, 
				   const ON_Curve * p_curve = NULL);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMG2DEdge();

	int SetOffsetCurve(const ON_Curve * p_pCrv);
	ON_Curve * GetOffsetCurve() const;
	ON_Curve * GetImportedEdges() const ;
	int WriteMarks(const ON_String p_fullpath);

	
	int SetStart(const ON_String & p_path);
	int SetEnd(const ON_String & p_path);
	int SetYinside(const ON_String & p_path);

	int GetStart(ON_2dPoint  & p_pt) const;
	int GetEnd(ON_2dPoint  & p_pt) const;
	int GetYinside(ON_2dPoint  & p_pt) const;

	ON_2dPointArray GetCurvatureMap(const int & p_nbPts);
	
	ON_Curve * GetEdge() const;
};

#endif // AMG2DEdge_DEC2005
