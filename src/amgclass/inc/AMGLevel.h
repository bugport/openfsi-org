// AMGLevel.h: interface for the AMGLevel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGLEVEL_H__34919288_D0F3_42A6_A07E_FE36DD7D132A__INCLUDED_)
#define AFX_AMGLEVEL_H__34919288_D0F3_42A6_A07E_FE36DD7D132A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "layerobject.h"

#define AMGLEVEL_ALL 0
#define AMGLEVEL_ALPHA 1 //he left diag
#define AMGLEVEL_BETA 2 //	right diag
#define AMGLEVEL_CHARLIE 4
#define AMGLEVEL_DELTA 8
#define AMGLEVEL_ECHO 16
#define AMGLEVEL_FOXTROT 32 // on-axis (?blue)
#define AMGLEVEL_GOLF  64  // dark blue
#define AMGLEVEL_ZEBRA 128 // transverse scrim

// IT is essential that these values remain in sequence. 

#define AMGLEVEL_ZEROLEVEL	((int) 256) // a speed optimisation.  Call with this level and it just returns the ref dir without 
// calculating the stack

class AMGLevel : public layerobject  
{
public:
	AMGLevel(const int n, const int type);
	AMGLevel(const int n);
	int GetLevelNo() const { return m_layernumber;}
	AMGLevel();
	virtual ~AMGLevel();

private:
	int m_layernumber;
};

#endif // !defined(AFX_AMGLEVEL_H__34919288_D0F3_42A6_A07E_FE36DD7D132A__INCLUDED_)
