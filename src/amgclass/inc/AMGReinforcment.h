// AMGReinforcment.h: interface for the AMGReinforcment class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGREINFORCMENT_FEB2007
#define AMGREINFORCMENT_FEB2007

#include "AMGHoopsObject.h"
//#include "Z:\build\cpp\on_relax\opennurbs_point.h"	// Added by ClassView


#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGReinforcment : public  AMGHoopsObject
{
public:
	int Update();
	AMGReinforcment();
	AMGReinforcment(const ON_String & p_model,
				   const ON_String & p_name, 
				   int               p_pdoc = NULL);
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGReinforcment();
	
	int SetParentGeom(const ON_String & p_path);
	int GetParentGeom(ON_String & p_seg);

	int SetPosition(const double & p_pos);
	int GetPosition(double & p_angle) const;

	ON_Curve * AMGReinforcment::GetParentCurve() const;
	int GetParentPoint(ON_3dPoint & p_pt) const;

	int SetOrientation(const double & p_angle);
	int GetOrientation(double & p_angle) const;

	int SetReinforcment(const ON_String & p_seg);
	int GetReinforcment(ON_String & p_seg);

	int SetPlyName(const ON_String & p_name);
	int GetPlyName(ON_String & p_name);

	
	int GetPanelName(ON_String & p_name);

protected: 
	int UpdateTransformationMatrix();
	int SetPanelName(const ON_String & p_name);
};

#endif // AMGReinforcment_DEC2005
