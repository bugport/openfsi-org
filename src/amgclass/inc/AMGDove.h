// AMGDove.h: interface for the AMGDove class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGDove_DEC2005
#define AMGDove_DEC2005

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGDove : public  AMGHoopsObject
{
public:
	AMGDove();
	AMGDove(const ON_String & p_path,
				 const ON_String & p_name, 
				 ON_Surface      * p_pSrfs1 = NULL, 
				 ON_Surface      * p_pSrfs2 = NULL,
				 ON_Surface      * p_pSrfs3 = NULL,
				 ON_Surface      * p_pSrfs4 = NULL,
				 ON_Surface      * p_pSrfs5 = NULL);
	
	ON_String GetSurfaceName(const int & p_ID);
	int SetOneSurfaceVisible(const int & p_ID);
	int ShowAllSurfaces();
	
	virtual ~AMGDove();
	
};

#endif // AMGDove_DEC2005
