// AMGSolidDoveDoc.h: interface for the AMGSolidDoveDoc class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGSOLIDDOVEDOC_H__B05C96AD_3BFC_4971_BE0B_5E65CF92A9F3__INCLUDED_)
#define AFX_AMGSOLIDDOVEDOC_H__B05C96AD_3BFC_4971_BE0B_5E65CF92A9F3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CSolidHoopsDoc.h"

class AMGSolidDoveDoc : public CSolidHoopsDoc  
{
protected:
	AMGSolidDoveDoc();
	DECLARE_DYNCREATE(AMGSolidDoveDoc)

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(AMGSolidDoveDoc)
	public:
//	virtual void Serialize(CArchive& ar);   // overridden for document i/o
//	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
//	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	protected:
//	virtual BOOL OnNewDocument();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~AMGSolidDoveDoc();
	// Generated message map functions
protected:
	//{{AFX_MSG(AMGSolidDoveDoc)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}


#endif // !defined(AFX_AMGSOLIDDOVEDOC_H__B05C96AD_3BFC_4971_BE0B_5E65CF92A9F3__INCLUDED_)
