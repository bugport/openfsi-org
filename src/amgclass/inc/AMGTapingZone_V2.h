// AMGTapingZone_V2.h: interface for the AMGTapingZone_V2 class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGTAPINGZONE_V2_H__E4E89A32_1CF0_4507_8896_4F7F59EFF6B3__INCLUDED_)
#define AFX_AMGTAPINGZONE_V2_H__E4E89A32_1CF0_4507_8896_4F7F59EFF6B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#include "AMGLevel.h"
#include "AMGTapingZone.h"

class AMGTapingZone_V2 : public AMGTapingZone  
{
public:
	void SetStackType(const int type);

	int Append(AMGZoneNode *p_p);
	int Append(AMGZoneEdge *p_p);
	int Append(AMGZERef *p_p);

	AMGTapingZone_V2();
	AMGTapingZone_V2(const ON_String & p_path,
			const ON_String & p_name,
			ON_Curve * p_boundary,
			dovestructure * p_dove);

	virtual ~AMGTapingZone_V2();
	int Print(FILE *fp);
	int MakeTapes(const AMGLevel level);


protected:
	int WalkEdgeByGeometry(AMGZoneEdge*e, const AMGZoneEdge*eFirst, AMGZoneCycle *p_zc);
	int CreateNewCycles_Geometric(const AMGLevel level);
	int WalkEdge(AMGZoneEdge*e, const AMGZoneEdge*eFirst, AMGZoneCycle *p_zc);
	int CreateNewCycles_Recursive(const AMGLevel level);
//	dovestructure * m_dove;
	int SplitEdges();
	int DrawEdges();
	
	ON_SimpleArray<AMGZoneCycle *> m_cycles; // a list of cycles waiting to be processed.
private:

	int CoalesceEdges();
	AMGZoneEdge * FindFirstEdge(AMGZoneEdge * p_e);
//	AMGZoneEdge * FindNextEdge(AMGZoneEdge * p_e);
};

#endif // !defined(AFX_AMGTAPINGZONE_V2_H__E4E89A32_1CF0_4507_8896_4F7F59EFF6B3__INCLUDED_)
