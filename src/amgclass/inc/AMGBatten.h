// AMGBatten.h: interface for the AMGBatten class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGBatten_DEC2005
#define AMGBatten_DEC2005

#include "AMGHoopsObject.h"
#include <HEventManager.h> 
#include <HUtilitySubwindow.h> 
//#include "HNetClientMgr.h"

#include "opennurbs_extensions.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGReinfRosette.h"

class AMGBatten : public  AMGHoopsObject
{
public:
	ON_Curve * GetEdgeByName(const ON_String & p_name) const;
	int SetEdges(ON_SimpleArray<AMGHoopsObject *> p_pEdges);
	int GetBothEnds(ON_3dPoint  & p_pt1,ON_3dPoint  & p_pt2) const;
	int GetForward(ON_3dPoint &p_pt) const;
	int GetAft(ON_3dPoint &p_pt) const;
	AMGBatten();
	AMGBatten(const ON_String & p_model,
					 const ON_String & p_name, 
					 const double * p_posonleech = NULL, 
					 const double * p_orientation= NULL, 
					 const int * p_IsFullLength= NULL,
					 const double * p_Length= NULL,
					 const int * p_IsOnPress= NULL,
					 const ON_String * p_pressname = NULL,
					 const double * p_pocketwidth = NULL,
					 const int * p_featidleech= NULL,
					 const int * p_featidptonleech= NULL,
					 const int * p_featidplaneorient= NULL,
					 const int * p_featidptlength= NULL,
					 const int * p_featidbatten= NULL);
	
	virtual ~AMGBatten();
	
	ON_String Serialize();
	// returns pointer t the char where reading should continue. 
	char* Deserialize(char* p_lp );

	double GetPosOnLeech() const;
	double GetOrientation() const;
	int GetIsFullLength() const;	
	double GetLength() const;
	double GetPocketWidth() const;
	ON_String GetPanelName();
	
	int SetPosOnLeech(const float & p_val);
	int SetOrientation(const float & p_val);
	int SetIsFullLength(const int & p_val);	
	int SetLength(const float & p_val);
	int SetPocketWidth(const float & p_val);
	int SetPanelName(const ON_String & p_val);

	int IsOnPress() const;
	int SetIsOnPress(const int & p_val);
	ON_String GetPressName() const;
	int SetPressName(const ON_String & p_str);

	int GetRelaxBattenRef(ON_String & p_str) const;
	int SetRelaxBattenRef(const ON_String & p_str);

	#ifdef _AMG2005
	ProError EditBatten();
	#endif

	int GetFeatIDPtOnLeech() const;
	int GetFeatIDPlaneOrient() const;
	int GetFeatIDPtLength() const;
	int GetFeatIDBatten() const;

	int WriteMarks(const ON_String p_fullpath, 
						 ON_SimpleArray<AMGHoopsObject *> p_Edges, 
						 ON_SimpleArray<AMGHoopsObject *> p_Presses); //p_Edges[0] is the luff 
															       //p_Edges[1] is the leech 
	int UpdateRosettes();
	int AddRosette(AMGHoopsObject *p_rosette, const double & p_position,const BOOL   & p_ISOnStarBoard);
	int RemoveRosette(const ON_String & p_name,BOOL p_DoUpdate = TRUE);

	int GetRosetteList(ON_ClassArray<ON_String> & p_listStbd,
							      ON_ClassArray<ON_String> & p_listPort);

	int ResetRosettes(AMGReinfRosette p_rosette);

	int ExportToRelax(ONX_Model & p_3dmModel);
protected: 
	ON_SimpleArray<AMGHoopsObject *> m_pEdges;
};

//int SortBattensByPosOnLeechIncreasing(const AMGBatten * p_batten1, const AMGBatten * p_batten2);


#endif // AMGBatten_DEC2005
