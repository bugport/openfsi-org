//
// Copyright (c) 2000 by Tech Soft America, LLC.
// The information contained herein is confidential and proprietary to
// Tech Soft America, LLC., and considered a trade secret as defined under
// civil and criminal statutes.  Tech Soft America shall pursue its civil
// and criminal remedies in the event of unauthorized use or misappropriation
// of its trade secrets.  Use of this information by anyone other than
// authorized employees of Tech Soft America, LLC. is granted only under a
// written non-disclosure agreement, expressly prescribing the scope and
// manner of such use.
//
// $Header: /files/homes/master/cvs/hoops_master/hoops_mvo/source/AMGOpTapePolygon.h,v 1.19 2004/06/04 22:32:45 heppe Exp $
//

//	AMGOpTapePolygon.h : interface of the AMGOpTapePolygon class
//
//	Draws a polyline in the view plane based on pointer input
//	Polyline points are stored in protected member variables and ready after 
//	OnLButtonDblClk method has been called. 
//	Derived Classes should call this::OnLButtonDblClk in their overloaded implementation.
//	Polyline points are in window space.


#ifndef _AMGOPTAPEPOLYGON_H
#define _AMGOPTAPEPOLYGON_H

#include "HTools.h"
#include "HBaseOperator.h"
#include "HOpConstructPolyline.h"

#include "dovestructure.h"
#include "AMGLevel.h"

#include "opennurbs.h"

//! The AMGOpTapePolygon class computes a selection list for objects inside a user-defined polygonal area.
/*!
  AMGOpTapePolygon employs all of the drawing functionality of HOpConstructPolyline to define a temporary, overlayed polygonal selection 
  area, and then maps the polygon information to the HOOPS routine HC_Compute_Selection_By_Polygon. The operation consists 
  of the following steps:
  <ol>
  <li>Left Button Down:				operation initiated, first point of the polygon recorded
  <li>No Button Down and Drag:		rubberband line segment to desired position of next point
  <li>Left Button Down:				next point in polygon recorded, etc.
  <li>Left Button Double Click:		polygon completed, and flushed from scene, selection list computed, objects highlighted, operation ended
  </ol>
  More Detailed Description: see event methods 
  Functional Note:  This operator does not provide exact results for HOOPS' Shell and Mesh primitives.
*/
class  AMGOpTapePolygon : public HOpConstructPolyline
{
public:
	/*! constructor */
  	AMGOpTapePolygon(HBaseView* view, int DoRepeat=0, int DoCapture=1);
	~AMGOpTapePolygon();

	/*!
		\return A pointer to a character string denoting the name of the operator  'AMGOpTapePolygon'
	*/
	virtual const char * GetName();  

	HBaseOperator * Clone(); /*! returns a pointer to a copy of the operator */


	/*!
		OnLButtonDblClk passes the polyline array from HOpConstructPolyline::OnLButtonDblClk into the HOOPS routine
		HC_Insert_Polygon and inserts the polygon into the scene.  The polyline information is also passed to
		Compute_Selection_By_Polygon.  Currently selected items are highlighted, and items previously highlighted are
		de-selected.
		\param event An HEventInfo object containing information about the current event.
		\return A value indicating the result of the event handling.
	*/
 	virtual int OnLButtonDblClk(HEventInfo &event);

	/*!
		OnLButtonDown initiates the polygon selection mechanism.
		\param event An HEventInfo object containing information about the current event.
		\return A value indicating the result of the event handling.
	*/
	virtual int OnLButtonDown(HEventInfo &event); 

	virtual int OnMouseMove(HEventInfo &event);
	
	virtual int OnMButtonDown(HEventInfo &event);
	
	int SetParameters(const ON_String & p_segpanels, 
									const ON_String & p_segtapes,
									const ON_String & p_panelname,
									 const double & p_MaxDev, // max deviation in radian 
									const double & p_LatOverlap_mm, //lateral overlap in mm
									const double & p_MinLength,
									const double & p_MaxLength,
									const double & p_TapeWith,
									const double & p_ButtOverlap,
									const double & p_maxGap,
									const AMGLevel &    p_level, 
									dovestructure * p_pdove, 
									const int & p_stacktype);
	int TapePolygon();
private:
	
	bool	m_bFirstTimeThrough;	// bool used to determine if we flush out the selection polygon

	 ON_String m_segPanels;
	 ON_String m_segTapes;

	ON_String  m_panelname;
	ON_PolylineCurve m_PLc;
	double m_MaxDev; 
	double m_LatOverlap;
	double m_MinLength;
	double m_MaxLength;
	double m_TapeWith;
	double m_ButtOverlap;
	double m_maxGap;
	AMGLevel    m_level;

	dovestructure * m_pdove;

	int m_stacktype;
	};
#endif
