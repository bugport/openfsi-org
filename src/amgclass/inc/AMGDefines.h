#ifndef AMGDEFINES_H_MAY_2005
#define AMGDEFINES_H_MAY_2005

#ifndef RHINO_DEBUG_PLUGIN
	#include "opennurbs.h"
	#include "opennurbs_extensions.h"
#endif


#ifdef __cplusplus
	#define EXTERN_C extern "C"
#else
	#define EXTERN_C extern
#endif

#ifndef AMG_FEATIDMOULD
#define AMG_FEATIDMOULD 1119   //Hard coded ref to template_main.prt
#else
	AMG_FEATIDMOULD allready defined
#endif

#ifndef AMG2_SEPARATOR
#define AMG2_SEPARATOR "\t"
#else
	AMG2_SEPARATOR allready defined
#endif

	
/*#ifndef AMG_SLASH
#define AMG_SLASH "/"
#else
	AMG_SLASH allready defined
#endif*/

#ifndef AMG_FTPUPLOAD
#define AMG_FTPUPLOAD "AMG_FTPUPLOAD"
#else
	AMG_FTPUPLOAD allready defined
#endif

#ifndef AMG_PUBLISH_GEOM
#define AMG_PUBLISH_GEOM "PUBLISH_GEOM"
#else
	AMG_PUBLICH_GEOM allready defined
#endif

#ifndef AMG_PUBLISH_2DGEOM
#define AMG_PUBLISH_2DGEOM "PUBLISH_2DGEOM"
#else
	AMG_PUBLICH_GEOM allready defined
#endif

#ifndef AMG_PUBLIC_OBJ
#define AMG_PUBLIC_OBJ "PublicObj"
#else
	AMG_PUBLIC_OBJ allready defined
#endif

#ifndef AMG_COLON	
#define AMG_COLON ":"
#else
	AMG_COLON allready defined
#endif

	
#ifndef AMG_COMENT	
#define AMG_COMENT "!"
#else
	AMG_COMENT allready defined
#endif

#ifndef AMG_EDITABLE_OBJ	
#define AMG_EDITABLE_OBJ "EditableObj"
#else
	AMG_EDITABLE_OBJ allready defined
#endif

#ifndef AMG_POLYLINE	
#define AMG_POLYLINE 100
#else
	AMG_POLYLINE allready defined
#endif 
	
#ifndef AMG_INTERPOLATECRV
#define AMG_INTERPOLATECRV 101
#else
	AMG_INTERPOLATECRV allready defined
#endif 
	
#ifndef AMG2_EVENT_TYPE
#define AMG2_EVENT_TYPE "AMGEVENTTYPE"
#else
	AMG2_EVENT_TYPE allready defined
#endif 

#ifndef AMG2_EDIT_CURVEFROMCURVATURE
#define AMG2_EDIT_CURVEFROMCURVATURE "EDITCURVEFROMCURVATURE"
#else
	AMG2_EDIT_CURVEFROMCURVATURE allready defined
#endif 
	
#ifndef AMG2_FEAT_ID
#define AMG2_FEAT_ID "FEATID"
#else
	AMG2_FEAT_ID allready defined
#endif 

#ifndef AMG2_DATA_FILE
#define AMG2_DATA_FILE "DATAFILE"
#else
	AMG2_DATA_FILE_PATH allready defined
#endif 



#endif //#define AMGDEFINES_H_MAY_2005