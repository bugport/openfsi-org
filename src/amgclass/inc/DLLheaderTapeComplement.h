namespace tapecomplementing
{
    class tapecomplementFuncs
    {
    public:
        
        static __declspec(dllexport) int tapescomplement(ON_ClassArray<ON_Polyline> &l_tapes, ON_ClassArray<ON_Polyline> &l_tapes_complement, ON_Polyline &l_blank, double l_area_min, double l_circumference_min);
    };
}