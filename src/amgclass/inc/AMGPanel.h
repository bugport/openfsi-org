// AMGPanel.h: interface for the AMGPanel class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGPanel_DEC2005
#define AMGPanel_DEC2005

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "AMGTape2.h"

class AMGPanel : public  AMGHoopsObject
{
public:
	int PrintTapesToPdf(int p_pdf,const ON_Xform & p_TransToPdf, double & p_tapeLength) const;

	AMGPanel();
	AMGPanel(const ON_String & p_path,
		   const ON_String & p_name, 
		   const double & p_val = 0);
	
	virtual ~AMGPanel();
	
	AMGPanel& AMGPanel::operator = (const AMGPanel & p_Obj);

	
	int PrintToMpf(FILE * fp, 
				   const ON_String & p_layername, 
				   const int & p_Speed,
				   BOOL p_TapeAllreadyFound, 
				   int & p_PlyId, 
				   int & p_tapeID, 
				   float * p_TransformationMatrix = NULL, 
				   const ON_String & p_Nextlayername = AMG_NO_NAME, 
				   const double    & p_tapeMinLength = 0.0, 
				   const double    & p_scalefactor   = 1.0, 
				   const int         p_pPDF    = NULL,
				   ON_Xform  *       p_pTransToPdf = NULL, 
				   double * p_tapelength = NULL); 

	int ComputeTapesBoundingBox(ON_BoundingBox & p_BB);
	int GetPlyList(ON_ClassArray<ON_String> & p_plies);

	int InitListTapes(const ON_String * p_plyname = NULL);

	int SetDoc(CDocument * p_pdoc);
	int SetListMarkers(ON_ClassArray<AMGTape2> p_listMarkers);
protected: 
	BOOL m_IsBoundingBoxSet;
	ON_BoundingBox m_BB;


	CDocument * m_pdoc;


	ON_ClassArray<AMGTape2> m_listtapes; // liste of the tapes in the the panel for each tape .x key on the polygon .y key on the segment which has the include and the modeling matrix  
	ON_ClassArray<AMGTape2> m_keysAlltapeForMarks;
	ON_ClassArray<AMGTape2> m_listmarkers;

};

#endif // AMGPanel_DEC2005
