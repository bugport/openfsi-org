// AMGSail.h: interface for the AMGSail class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGSail_DEC2005
#define AMGSail_DEC2005

#include "AMGHoopsObject.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGSail : public  AMGHoopsObject
{
public:
	int SetWindow(const double & p_left,  
						   const double & p_right,  
						   const double & p_bottom,  
						   const double & p_top, 
						   const double & p_xmargin, 
						   const double & p_ymargin);
	AMGSail();
	AMGSail(const ON_String & p_path,
			   const ON_String & p_name);
	
	ON_String Serialize(); 
	char* Deserialize(char* p_lp );

	virtual ~AMGSail();
	
};

#endif // AMGSail_DEC2005
