// AMGSolidDoveModel.h: interface for the AMGSolidDoveModel class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGSOLIDDOVEMODEL_H__7547EDDE_3016_440B_BD8B_C945CE99310D__INCLUDED_)
#define AFX_AMGSOLIDDOVEMODEL_H__7547EDDE_3016_440B_BD8B_C945CE99310D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "HSolidModel.h"
class doveforstack;

class AMGSolidDoveModel : public HSolidModel  
{
public:
	struct PC_3DM_Model * m_3dmm;
 	doveforstack * m_TheDove; // Nov 2006 was doveforstack. may be one of several derived classes

	AMGSolidDoveModel();
	virtual ~AMGSolidDoveModel();
	HFileInputResult Read(const char * FileName);
    HFileInputResult ReadDoveFile(const char * FileName);
	HFileInputResult ReadPLTFile(const char * FileName);
	HFileInputResult ReadCSVFile(const char * FileName);

};

#endif // !defined(AFX_AMGSOLIDDOVEMODEL_H__7547EDDE_3016_440B_BD8B_C945CE99310D__INCLUDED_)
