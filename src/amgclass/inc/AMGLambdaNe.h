// TestMatlab2.cpp : Defines the entry point for the console application.
//


#ifndef _AMGLAMBDANE_H_MAY07
#define _AMGLAMBDANE_H_MAY07

//p_ep pointer ion the matlab engine
int getAlphas(int p_ep, const double &pNeIn, const double &pLambdaIn, const double &pK1, const double &pK2, double &alpha1in, double &alpha2in); 


#endif //_AMGMATLAB_H

