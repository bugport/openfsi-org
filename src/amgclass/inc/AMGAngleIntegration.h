// AMGAngleIntegration.h: interface for the AMGAngleIntegration class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_AMGANGLEINTEGRATION_H__C6F0B3B1_0CE9_4169_8D42_8002BC8028DB__INCLUDED_)
#define AFX_AMGANGLEINTEGRATION_H__C6F0B3B1_0CE9_4169_8D42_8002BC8028DB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class 	AMGTapingZone_V3;
class	AMGTape;
#include "RXIntegration.h"

class AMGAngleIntegration : public RXIntegration  
{
public:
	AMGAngleIntegration(AMGTapingZone_V3 *p_tz,AMGTape *p_t);
	AMGAngleIntegration();
	virtual ~AMGAngleIntegration();

protected:
	double m_v;
	double m_u;
	AMGTapingZone_V3 * m_tz;
	AMGTape *m_tape;
	double func(double p_x);
};

#endif // !defined(AFX_AMGANGLEINTEGRATION_H__C6F0B3B1_0CE9_4169_8D42_8002BC8028DB__INCLUDED_)
