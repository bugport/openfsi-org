#ifndef _AMGTAPINGCONSTANTS_H_			
#define  _AMGTAPINGCONSTANTS_H_			
			
// definitions for GetCorner.
		
#define AMG_TAPE_START	0		
#define AMG_TAPE_LEFT	0		
#define AMG_TAPE_END	1	// for GetCorner.  set if you want the end. Else you get the start.	
#define AMG_TAPE_RIGHT	2	// for GetCorner.  set if you want the RH side. Else you get the LH side.

	
#define AMGPOLY_TAPELEFT	1		
#define AMGPOLY_TAPERIGHT	2
#define AMGPOLY_TAPESTART	4		
#define AMGPOLY_TAPEEND 	8	
			
//An edge  may be Boundary, Butt, Tape-Side Left or Tape-side Right.		
#define AMG_EDGE_ISLEFT        	1	
#define AMG_EDGE_ISRIGHT       	2	//means this is on the RH side of a tape block looking in the forwards tape sense. Its RH side is free
#define AMG_EDGE_ISBOUNDARY    	4	
#define AMG_EDGE_IS_BUTT    	8	 
#define AMG_EDGE_HAS__BEEN_TAPED	16	
#define AMG_EDGE_ISNT__CYCLEEDGE	32 // AMG_EDGE_HAS_BEEN_TAPED//32	
#define AMG_EDGE_ISBEINGTAPED	64	
#define AMGZER_CONTIG_NEXT   	128	
#define AMGZER_CONTIG_PREV  	256	
#define AMGZER_ISENDOFTAPE  	512	
#define AMGZER_ISMIDOFTAPE  	1024	
#define AMGZER_ISSTARTOFTAPE	2048	
#define ZONEEDGE_IN_STACK   	4096	
#define ZONENODE_DONE_SPLIT  	8192	// marks a ZN as being on a ZE which it has split.
#define ZONENODE_INSPACE    	16384	// marks a ZN as being in space, presumable ending at least 2 ZE's.
#define ZONENODE_TAPEMIDFWD 	32768	// marks a ZN as being in the middle of a tape which is in the same sense as the (WHICH? ZE
#define ZONENODE_TAPEMIDREV 	65536	// marks a ZN as being in the middle of a tape which is reversed wrt the (WHICH? ZE
#define ZONENODE_WILL_SPLITBEFORE	131072	// marks a ZN as being on a ZE which it must later split. The taped section is after
#define ZONENODE_WILL_SPLITAFTER	2097152	// marks a ZN as being on a ZE which it must later split. The taped section is before
#define AMGZER_IS_FAR_BUTT   	262144	// a bit mis-named. This is set on a zeref which is at the other end of the tape from the butt which  creates it.
#define AMGZER_IS_GLUE      	524288	
#define AMGDBG_NODEONBDY      	1048576	
//	2097152	
//	4194304	
//	8388608	
//	16777216	
//	33554432	

		
#define AMGZER_GOPASTCORNERS 	-29		
#define AMGZER_STOPATCORNERS 	-37		
#define BS_EDGE             	-16		
#define BS_FREE             	-32		
#define BS_ANGLEINCREASING    	-73		
#define BS_ANGLEDECREASING   	-152	
#define BS_ANGLENOCHANGE		-103

#define BS_CREATENEWBUTT		-4
#define BS_NO_NEWBUTT   		-8	

#define AMGPRINT_OBJ			1
#define AMGPRINT_RHINO			2
// IMPORTANT  all combinations of the BS_xxx constants must be distinct.
// most compilers would pick this up but don't rely on it! 		

#ifndef TODO
#ifndef AMGUI_2005
	#define TODO assert(0);
#else
	#define TODO assert(1);  //TR 30 aug 06
#endif 
#endif
#endif			
