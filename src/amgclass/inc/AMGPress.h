// AMGPress.h: interface for the AMGPress class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AMGPress_DEC2005
#define AMGPress_DEC2005

#include "AMGHoopsObject.h"

#include "opennurbs_extensions.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class AMGPress : public  AMGHoopsObject
{
public:
	ON_Curve * Get2dFrmBatten(AMGHoopsObject * l_p2dluff,
							  AMGHoopsObject * l_p2dleech,
							  AMGHoopsObject * l_p2dfoot) const;
	ON_Curve * GetScarfsLines(const double & p_pos, // (in) position of the scarf lien beetween the shoulder (0) and Dwarm (1.0)
					BOOL p_BtmSracf, // (in)  if true btm scarf else top scarf 
						AMGHoopsObject * l_p2dluff,
							 AMGHoopsObject * l_p2dleech,
							 AMGHoopsObject * l_p2dfoot);

	int GetBothEnds(ON_SimpleArray<AMGHoopsObject *> p_Edges, 
						 ON_3dPoint & p_PtOnLuff, 
						 ON_3dPoint & p_PtOnLeech); //p_Edges[0] is the luff 
													//p_Edges[1] is the leech 
													//p_Edges[2] is the FOOR
													//p_Edges[3] is the head

	int ExportToRelax(ON_SimpleArray<AMGHoopsObject *> p_Edges, 
					  ONX_Model & p_3dmModel);

	ON_2dPointArray GetCurvatureMap(const int & p_nbPts);

	ON_LineCurve GetCordLine(ON_SimpleArray<AMGHoopsObject *> p_Edges);

	ON_Curve * GetEdgeByName(ON_SimpleArray<AMGHoopsObject *> p_Edges, const ON_String & p_name) const; 

	//return the despth of the batten (>0) for a given position on the broad seam
	// p_t = 0 --> luff
	// p_t = 1 --> leech
	// p_t < 0  or p_t>1 we are outside of the sail we return a value doing a linear extrapolation
	// if the result is <0 we return 0
	double GetBattenDepthAt(const double p_t) const;
	ON_2dVector GetBattenTangentAt(const double p_t, 
									const double p_eps) const;

	int WriteMarks(const ON_String p_fullpath, 
									ON_SimpleArray<AMGHoopsObject *> p_Edges); //p_Edges[0] is the luff 
															       //p_Edges[1] is the leech WriteMarks

	int WriteMarksConnections(const ON_String p_fullpath, 
				   ON_SimpleArray<AMGHoopsObject *> p_Edges); 

	int WriteMarksGrid(const ON_String p_fullpath, 
									ON_SimpleArray<AMGHoopsObject *> p_Edges); //p_Edges[0] is the luff 
															       //p_Edges[1] is the leech 
	int WriteGP13Spline(ON_String       * p_fullpath = NULL, 	
					    ON_2dPointArray * p_offsets = NULL,
					    ON_3dPointArray * p_DeltaLs = NULL, 
						 const double &    p_scalefactor = 1.0) const;

	double GetCord() const;
	int SetCord(const double & p_val);

	int GetBroadseam(ON_Polyline &p_pl) const;
	int SetBroadseam(const ON_Polyline & p_pl) ; 

	
	//set an offset to the forming batten
	int SetDeltaFrmBat(const int & p_stationID, 
							 const double & p_delta);
	int GetDeltaFrmBat(ON_Polyline &p_pl) const ; 
	
	int GetBSOffsets(ON_2dPointArray &p_pl) const;
	double GetBSOffsetAt(const double & p_pos) const;

	double GetDeltaL(const double & p_pos) const;

	AMGPress();

	virtual ~AMGPress();

	AMGPress(const ON_String & p_model,
					 const ON_String & p_name, 
					 const double * p_relpos = NULL, 
					 const double * p_relposleech = NULL,
					 const double * p_orientation= NULL, 
					 const double * p_DbetweenShoulders= NULL,
					 const double * p_DWarm= NULL,
					 const double * p_Pied= NULL,
					 const ON_Polyline * p_broadseam= NULL, 
					  const int * p_featidleech= NULL,
					 const int * p_featidptonluff= NULL,
					 const int * p_featidplaneorient= NULL,
					 const int * p_featidbatten= NULL, 
					 const ON_Polyline * p_formingbattens= NULL, 
					 const ON_3dPointArray * p_marks= NULL);

	ON_String Serialize();
	// returns pointer t the char where reading should continue. 
	char* Deserialize(char* p_lp );
	

	int Remove();
	
	int SetPosition(const double & p_val);
	int SetPositionLeech(const double & p_val);
	int SetOrientation(const double & p_val);
	int SetDShoulders(const double & p_val);
	int SetDWarm(const double & p_val);
	int SetPied(const double & p_val) ;
	int SetDRefOnPanel(const double & p_val) ;

	int SetDistStation(const double & p_val);
	int SetDistQc(const double & p_val);
	int SetPressLength(const double & p_val); 


	double GetPosition() const;
	double GetPositionLeech() const;

	double GetAbsolutePosition(ON_SimpleArray<AMGHoopsObject *> p_Edges) const;
	double GetAbsolutePositionLeech(ON_SimpleArray<AMGHoopsObject *> p_Edges) const; 

	double GetOrientation() const;
	double GetDShoulders() const;
	double GetDWarm() const;
	double GetPied() const;
	double GetDRefOnPanel() const;
	double GetDistStation() const;
	int GetNbStation() const;
	double GetDistQc() const;
	double GetPressLength() const; 

	int GetFeatIDPtOnLuff() const;
	int GetFeatIDPlaneOrient() const;


	
#ifdef _AMG2005
	ProError EditPress();
#endif

protected: 
	int WriteMarkLine(const ON_2dPoint  & p_start,
							const ON_2dVector & p_dir,
							const double	  & p_step,
							const double     & p_radius,
							ON_SimpleArray<AMGHoopsObject *> p_pEdges,
							ON_3dPointArray & p_array);

};

int WriteMark(ON_3dPointArray & p_array, 
	  const ON_2dPoint & p_pos, 
	  const int	   &	p_nbpts = 0,
	  const double & p_radius = 0);

#endif // AMGPress_DEC2005
