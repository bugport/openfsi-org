/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Part of meshlib.a
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* implementation of do_err_dialog for the Motif version */

#include "StdAfx.h" 

#include <X11/Intrinsic.h>
#include <X11/Shell.h>

#include <Xm/Xm.h>
#include <Xm/MessageB.h>
#include <Xm/TextF.h>

#include "traceback.h"
#include "griddefs.h"
#include "entities.h"
#include "elements.h"
#include "GraphicStruct.h"
#include "global_declarations.h"
#include "resdiag.h"
#include "resolve.h"
#include "sailutil.h"
#include "global_declarations.h"

#include "motif_err_dialog.h"

static int NOT_DONE;
static int RETVAL;
static int g_ERR_OFFSET;

void err_ok_CB(Widget w, caddr_t client_data, XmAnyCallbackStruct * call_data )
{
RETVAL = 0;
NOT_DONE = 0;
}

void err_cancel_CB(Widget w, caddr_t client_data, XmAnyCallbackStruct * call_data )
{
RETVAL = 1;
NOT_DONE = 0;
}

void err_degrade_CB(Widget w, caddr_t client_data, XmAnyCallbackStruct *call_data )
{
g_ERR_OFFSET ++;
RETVAL = 1;
NOT_DONE = 0;
}

int do_err_dialog(const char *s, int severity)
{
Widget msgBox=NULL;
XmString xmOK=NULL,xmCancel=NULL,xmstr=NULL,xmTitle=NULL;
XtInputMask xmask;

Widget parent = NULL;
Widget okButton = NULL;
Widget cancelButton = NULL;
	int l_ac=0;
	Arg al[128];

  parent = g_graph[0].mainWindow;

if(!parent){
	printf("do_err_dialog sev=%d <%s> \n",severity, s);
	return(1);
}
if(severity < 2) {
printf(" do_err_dialog %s sev=%d ofst=%d\n", s,severity, g_ERR_OFFSET);
return 0;
}

 if(severity >3 )
 	g_ERR_OFFSET =0;
  severity = severity - g_ERR_OFFSET;

XtSetArg (al[l_ac], XmNdefaultButtonType, XmDIALOG_OK_BUTTON);l_ac++;
XtSetArg (al[l_ac], XmNmessageAlignment, XmALIGNMENT_CENTER);l_ac++;
xmstr = XmStringCreateLtoR((char*)s, (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
XtSetArg(al[l_ac],  XmNmessageString, xmstr); l_ac++;

switch (severity) {
    case(4): /* WARNING which requires an OK or  Cancel */
//#ifdef DEBUG
	print_trace();
//#endif	
	printf("(RX Error) %s\n", s);
	XtSetArg (al[l_ac], XmNdialogType, XmDIALOG_WARNING);l_ac++;
	xmOK = XmStringCreateLtoR((char*) "Continue", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNokLabelString, xmOK); l_ac++;
	xmCancel = XmStringCreateLtoR((char*)"Cancel", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNcancelLabelString, xmCancel); l_ac++;
	xmTitle = XmStringCreateLtoR((char*)"Warning", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNdialogTitle, xmTitle); l_ac++;
    break;
    case(3):
	printf("(RX Error) %s\n", s);
//#ifdef DEBUG
	print_trace();
//#endif
	XtSetArg (al[l_ac], XmNdialogType, XmDIALOG_ERROR);l_ac++;
	xmOK = XmStringCreateLtoR("Continue", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNokLabelString, xmOK); l_ac++;
	xmCancel = XmStringCreateLtoR("Shut Down", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNcancelLabelString, xmCancel); l_ac++;
	xmTitle = XmStringCreateLtoR("Relax Error", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNdialogTitle, xmTitle); l_ac++;
    break;
    case(2):
	printf("(RX Warning) %s\n", s);
	XtSetArg (al[l_ac], XmNdialogType, XmDIALOG_WARNING);l_ac++;
	xmOK = XmStringCreateLtoR((char*)"OK", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNokLabelString, xmOK); l_ac++;
	xmCancel = XmStringCreateLtoR((char*)"Cancel", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNcancelLabelString, xmCancel); l_ac++;
	xmTitle = XmStringCreateLtoR((char*)"Warning", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNdialogTitle, xmTitle); l_ac++;
   	 break;
     case 1:
		printf("(RX Advisory)  %s \n", s);
	break;
    default:
	printf("(RX Error) %s\n", s);
	XtSetArg (al[l_ac], XmNdialogType, XmDIALOG_INFORMATION);l_ac++;
	xmOK = XmStringCreateLtoR("OK", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNokLabelString, xmOK); l_ac++;
	xmTitle = XmStringCreateLtoR("Message", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNdialogTitle, xmTitle); l_ac++;
    break;

}


msgBox = XmCreateMessageDialog(parent,"Error",al,l_ac);

cancelButton = XmMessageBoxGetChild(msgBox, XmDIALOG_CANCEL_BUTTON);
if(severity < 3) {	 /* ie. only want the ok button NOT the cancel */	
   // XtUnmanageChild(cancelButton);
	l_ac=0;
	xmCancel = XmStringCreateLtoR("Skip All", (XmStringCharSet)XmSTRING_DEFAULT_CHARSET);
	XtSetArg(al[l_ac],  XmNcancelLabelString, xmCancel); l_ac++;
  	XtAddCallback(cancelButton, XmNactivateCallback,(XtCallbackProc)err_degrade_CB, (XtPointer) 0);

}
else {
   XtAddCallback(cancelButton, XmNactivateCallback,(XtCallbackProc)err_cancel_CB, (XtPointer) 0);
}
    	
okButton = XmMessageBoxGetChild(msgBox, XmDIALOG_OK_BUTTON);
XtAddCallback(okButton, XmNactivateCallback,(XtCallbackProc)err_ok_CB, (XtPointer) 0);

XtManageChild(msgBox);


XtAddGrab(msgBox,True,False);

RETVAL = 0;
NOT_DONE = 1;

while(NOT_DONE) {	
    xmask = XtAppPending(g_app_context);
    if(xmask) 
	XtAppProcessEvent(g_app_context,xmask);
}

/* printf("Motif err return code = %d\n",RETVAL); */
XtDestroyWidget(msgBox); 	

if(xmstr)
    XmStringFree ( xmstr );
if(xmOK)
    XmStringFree ( xmOK );
if(xmCancel)
    XmStringFree ( xmCancel );
if(xmTitle)
    XmStringFree ( xmTitle );

return(RETVAL);
}






