#include "StdAfx.h"

#ifdef linux
#include "traceback.h"
#include <mcheck.h>
#include <pthread.h> // for nanosleep
#endif
#include "rxsubwindowhelper.h"
#ifdef HOOPS
#include "HBaseModel.h"
#include "HBaseView.h"
#else
#include "RXGraphicCalls.h"
#endif
#include <signal.h>

#include "griddefs.h"

#include "CheckAll.h"

#include "Global.h" // for Xm
#include "getglobal.h"

#include "sailutil.h"
#include "Gflags.h"
#ifdef _X
#include "Menu.h"
#include "trimmenu.h"
#include "OpenBuildFile.h"
#endif
#include "text.h"

#include "MakeNewDrawingArea.h"
#include "words.h"
#include "files.h"

#ifdef HOOPS
#include "fonterrorfix.h"
#endif
#include "global_declarations.h"
#include "f90_to_c.h"
#include "script.h"
#include "ReadBoat.h"
#include "drawwind.h"
//#include "utility.h" // for parse_args Dec 2005

#include "stringutils.h"
#include "RXSail.h"

#include "RelaxWorld.h"
#include "mainstuf.h"
#define V " v3.00-00-20Q "


#ifdef USE_CURSORS
Cursor boat_cursor;
Cursor question_cursor;
Cursor clock_cursor;
Cursor crosshair_cursor;
Cursor bogosity_cursor;
Cursor sail_cursor;
Cursor camera_cursor;
Cursor normal_cursor;
#endif

int minor_version = 8;
int major_version = 200;


void  rlx_siig_hand(int oursigal) {
    /*    Close_Err_File();
       printf("in my signal handler with %d\n",oursigal); */
    exit(1);
}
#ifndef linux
int XIO_error_handler(void)
{
    FILE *xf;

    xf = RXFOPEN("/tmp/AKM_xerrors","a+");
    if(!xf) {
        printf("cannot open Xerror file\n");
        xf = stdout;
    }
    fprintf(xf,"XIOerror:FATAL but dont know what\n");
    FCLOSE(xf);
    exit(1);

}
#endif
#ifdef _X
int X_error_handler(Display *d, XErrorEvent *ev)
{
    FILE *xf;

    xf = RXFOPEN("/tmp/AKM_xerrors.txt","a+");
    if(!xf) {
        printf("cannot open Xerror file\n");
        xf = stdout;
    }//						was ul
    fprintf(xf,"Xerror:\n  serial = %ld \n  error_code = %ul \n  request_code = %ul \n  minor_code = %ul\n",ev->serial,ev->error_code,ev->request_code,ev->minor_code);
    FCLOSE(xf);
    return(0);
}


Widget create_main_shell(Graphic *g,Display *arg_display,char *app_name,int app_argc,char **app_argv)
{
    char buf[256];

    Arg al[96];
    int l_ac=0;
    XtSetArg(al[l_ac], XmNallowShellResize, True); l_ac++;
    XtSetArg(al[l_ac], XmNargc, app_argc); l_ac++;
    XtSetArg(al[l_ac], XmNargv, app_argv); l_ac++;
    sprintf(buf,"%s %s Boat: ( Untitled )",RELAX_VERSION,__DATE__);
    XtSetArg(al[l_ac],XmNtitle,buf); l_ac++;
    XtSetArg(al[l_ac],XmNiconName,"Relax 3"); l_ac++;
    XtSetArg(al[l_ac],XmNwidth,750); l_ac++;
    XtSetArg(al[l_ac],XmNheight,150); l_ac++;

    g->topLevel = XtAppCreateShell ( app_name, "XRelax", applicationShellWidgetClass, arg_display, al, l_ac );
    l_ac = 0;
    /* create main window */

    g->mainWindow = XmCreateMainWindow(g->topLevel,(char*) "mainWindow",al,l_ac);
    XtManageChild(g->mainWindow);
    CreateMsgArea(g);
    g->menuBar = CreateMenuBar(g->mainWindow,g);

    g->workArea = MakeNewDrawingArea(g->mainWindow,"boatbase",g, (HBaseModel*) 0 ); // pass NULL model. THe widget makes one

    XmMainWindowSetAreas(g->mainWindow,g->menuBar,NULL,NULL,NULL,g->workArea);

    /* add the mesage area */
    l_ac=0;
    XtSetArg(al[l_ac],XmNmessageWindow,(XtArgVal) g->msgform); l_ac++;
    XtSetValues(g->mainWindow,al,l_ac);

    // I htink this is alreqady done
    XtRealizeWidget(g_graph[0].topLevel);
    HC_Open_Segment_By_Key(g->m_ViewSeg);//cant do this till after the Realize
    HC_Close_Segment();

    HC_Open_Segment_By_Key( g->m_Phoops_widget->hoops.m_pHView->GetViewKey() );
    HC_Show_Pathname_Expansion(".",buf); //cant do this till after the Realize
    g->hoops_driver = STRDUP(buf);
    HC_Close_Segment();
    HC_Open_Segment_By_Key(g->m_ViewSeg);

    HC_Close_Segment();
    return(g->topLevel);
}
#endif

void relaxinit_1(){
    int i;
    char *lp;
    assert(sizeof(void *) == sizeof(long int)) ; // probably only for HC_Set_User_Value(ssd)
    assert(!cout.bad());
    if(sizeof(void *) != 8)
        cout<<" Are you sure that its OK to have pointer size ="<<sizeof(void *) <<" on this platform????"<<endl; // or else change basictypes.f
    assert(!cout.bad());
    g_World= new RelaxWorld();
    assert(!cout.bad());

    Set_MALLOC_DATA("startup_qwerty") ;

    if(getenv("R3ROOT") ){
        strcpy( g_RelaxRoot, getenv("R3ROOT"));
        system(" cd $R3ROOT");
    }
    else
    { *g_RelaxRoot =0; cout<<"WARNING:: $R3ROOT not defined. "<<endl;}

    if ( getenv("R3CODE"))
        strcpy( g_CodeDir, getenv("R3CODE"));
    else     if ( getenv("R2CODE"))
        strcpy( g_CodeDir, getenv("R2CODE"));
    else
        *g_CodeDir  =0;

    i=0;
    lp=NULL;
    assert(!cout.bad());
 //   RXSubWindowHelper::rootgraphic = 0;// new RXSubWindowHelperTopDesign(); //nextGraphic(WT_DESIGNWINDOW);

    assert(!cout.bad());

    assert(!cout.bad());
    g_World->GetGlobalValues(INI_FILE);
    g_World->GetGlobalValues(DEFAULTS_FILE);
}

#ifndef RXQT
void relaxinit_2( int argc,char **argv, class QObject* TheWorld)
{

    g_World->SetClient(TheWorld);
#ifdef _X
    char buf[512];
    if (parse_args ("-debug", argc, argv) != 0) {
        g_ArgDebug = 1;

        XSynchronize (g_display, True);

        printf("r2a started with debug on - hit a key to continue\n");
        getchar();
    }

    char * applicationName = STRDUP("RelaxII");

    create_main_shell( RXSubWindowHelper::rootgraphic,g_display, applicationName, argc, argv );

    XFlush(XtDisplay(g_graph[0].topLevel));

    g_trim_shell = create_trim_shell (g_graph[0].topLevel);

    g_boat=0;

    //     Draw_Reflection();
    initTextWindows();
    //  Draw_Wind((float)0.0,  g_PansailIn.Reference_Length);

    sprintf(buf,"LogFiles:IN:(none) | OUT:(none)");
    RXSubWindowHelper::rootgraphic->setStatusBar(QString("LogFiles:IN:(none) | OUT:(none)" ) ,1);

    int l_ac;
    if ( (l_ac = parse_args ("-boat", argc, argv))) {
        assert(0); ReadBoat(QString(argv[l_ac+1]),RXSubWindowHelper::rootgraphic); HC_Update_Display();
    }
    else if ( (l_ac = parse_args ("-script", argc, argv))) {
        Read_Script(argv[l_ac+1]);
    }
#endif
#ifdef HOOPS
    else  {
        HC_Open_Segment_By_Key(g_graph[0].m_ModelSeg);
        HC_Open_Segment("title");
        HC_Set_Text_Alignment("v");
        HC_Set_Text_Font("size=40 pt");
        HC_Insert_Text(0,00,10,"Relax 3/OpenFSI.org");
        HC_Close_Segment();
        HC_Open_Segment("copyright");
        HC_Set_Text_Alignment("v");
        HC_Set_Text_Font("size=12 pt");
        HC_Insert_Text(0,0,2,"Copyright 1994-. Peter Heppel Associates");
        HC_Close_Segment();
        HC_Close_Segment();
    }
    HC_Update_Display();
#endif
}
#endif
