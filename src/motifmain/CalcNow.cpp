/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 march 2002 could be an ifdef error on s g i or IBM

 Oct 97 set Offsets shouldnt change nds
 Whenever we change a starting offset, we set MESH_HAS_CHANGED
 We hope this will force a load early enough. Ie before any C tries
 to access the fortran common blocks
 Set Offsets now sets nds to 1. Why didnt it before ???

  * 26.6.96   crease  sets /LS not /CS
 * 26/3/96 PC PANSAIL renamed PCN_PANSAIL to force re-compilation
 *   9.1.96   Changed so that will deal with MDIM models in relax
 *            BUT only MAXCOM will see PANSAIL
 *            Adam Molyneaux.
 *   24/7/95  test_deltak_ added.   A routine in angcheck.f NOT FOR RELEASE
 *   and while I'm here, turn off the debug prints via CNPRNT
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h" 
#include <QDebug>
#include "RXGraphicCalls.h"
#include "RelaxWorld.h"
#include "RXLogFile.h"
#include "rxqtdialogs.h"
#include "RXSail.h" 
#include "RXMesh.h" 
#include "RXShapeInterpolation.h"
#include "RXEntityDefault.h" 
#include "rxsubwindowhelper.h"

#define CNPRNT (0)

#include <time.h>
#include "global_declarations.h"

#include "stringutils.h"
#include "../stringtools/UtfConverter.h"

#ifdef USE_PANSAIL
#include "pansail.h"
#include "readedg.h"
#endif
#include "Gflags.h"
#include "ch_rowt.h"

#ifdef _X
#include "Menu.h"
#include "ch_row.h"
#endif

#include "vppint.h"
#include "CheckAll.h"
#include "script.h"

#include "akmutil.h"
#include "f90_to_c.h"

using namespace UtfConverter;

#define      OFFSETS_CHANGED  (1966)


int  RelaxWorld::Set_Output_Flags(void) { // If a sail is hoisted, st its data has changed ON

    int count=0; size_t i;
    vector<RXSail*> lsl = this->ListModels(RXSAIL_ALLHOISTED); // boat too
    for (i=0;i<lsl.size();i++) {
        if( !lsl[i]->GetFlag(FL_NEEDS_POSTP))
            count++;
        lsl[i]->SetFlag(FL_NEEDS_POSTP);
    }
    return 1;
}

int RelaxWorld::__CalcNow() {

    std::map <int,struct PCN_PANSAIL *> panList;
    int count=0; size_t i;

    char file[512], buf[1024] ;
    int retVal=0;
    int converged=0;
    int rcount=0;
    QString reportString;

    memset(file,' ',sizeof(char)*512);

    RXSubWindowHelper::Post_All_Hoisted();

    vector<RXSail*> lsl = this->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(i=0;i<lsl.size();i++)  {
        SAIL *theSail = lsl[i];
        if(theSail->GetFlag(FL_NEEDS_RUNNING)) {
            theSail->SetFlag(FL_NEEDS_RUNNING , 0);
            theSail->SetFlag(FL_NEEDS_POSTP );
        } // if(s->NEEDS_RUNNING)
    } /* end for each... */


    /*   analysis loop is : */
    
    /*   for each phase
  while ncycle && not.converged
  if(!INITITAL_RUN && TRIM_HAS_CHANGED)
  run_stress
  clFlag(g_Analysis_Flags,TRIM_HAS_CHANGED);
  else
  run_wind
  run_stress
  endif
  continue ncycle
  next phase
  */
    
    if(g_CH_Run_Data.run_to_line > 0)
        rcount = 0;

    do { // this loop is for batch running
        MakeRunNameUnique();

        mkFlag(g_Analysis_Flags,MASS_SET_FLAG);
        // negative means disable
        for(i=0;i<3;i++) {
            if(!g_ControlData[i].ncycle)
                continue; // so we don't grey on ending on phase 2
            cout<<endl<<"Running Phase :"<<i+1<<" of 3"<<endl;
            count=0;
            converged = 0;
            while((count++ < g_ControlData[i].ncycle) && !converged) {
                if (this->DBMirror() )
                    this->DBMirror()->post_summary_value( "Status","-1 Running");
                     setStatusBar(QString("Running %1..." ).arg(g_World->GetRunName() ));

                if( relaxflagcommon.VPP_Connected < 0)
                    cout<<"TODO:: Passive_VPP_Call();"<<endl;
                if(rxFlag(g_Analysis_Flags,MASS_SET_FLAG))
                    relaxflagcommon.Mass_Set_Flag = -1; // means do a full prep
                else
                    relaxflagcommon.Mass_Set_Flag = 0; // skip the prep = a bad idea

                if(rxFlag(g_Analysis_Flags,TRIM_CHANGE) && !rxFlag(g_Analysis_Flags,INITIAL_RUN) && !rxFlag(g_Analysis_Flags,WIND_CHANGE)  && (g_ControlData[i].ncycle>1) )
                    retVal = run_stress(&(g_ControlData[i]));
                else {
                    run_wind(&(g_ControlData[i]),panList);
                    retVal = run_stress(&(g_ControlData[i]));
                    clFlag(g_Analysis_Flags,WIND_CHANGE);
                }
                clFlag(g_Analysis_Flags,INITIAL_RUN);
                clFlag(g_Analysis_Flags,TRIM_CHANGE);

                //  checks members of the current summary state against previous values.
                converged = this->Check_Convergence();
                /*	    printf(" Summary says converged =%d\n (-1=dont know. 1 if converged)\n", converged);*/

                //  if(converged < 0)   converged = check_convergence();
                if(g_qAfter_Each_Cycle.size()  ) {
                    char *buff = STRDUP(qPrintable( g_qAfter_Each_Cycle));
                    if(this->DBMirror() )
                        while(this->DBMirror()->Replace_Words_From_Summary(&buff,0)) printf("AEC replace gave %s\n",buff); /* 0 means dont append */
                    Execute_Script_Line(buff,0);//RXSubWindowHelper::rootgraphic);
                    RXFREE(buff);
                }
                if(!g_World)  // probably we are closing down
                    return 0;
                g_World->PostProcessAllSails(3);
                AKM_Each_Cycle();
                if(retVal) break;
            }			 /* end while FSI loop*/
            reportString += QString(" Phase %1 nfsi= %2 :").arg(i).arg(count);
            if(retVal)  break;
        } // end for i (= analysis phase loop
        if (this->DBMirror() ) {
            this->DBMirror()->post_summary_value( "analysis Report", qPrintable(reportString));
            this->DBMirror()->post_summary_value( "Status","2 FSI Finished");
        }
        reportString.clear();
        // This will update velocities.
        Set_Output_Flags();  // for now just sets DATA_HAS_CHANGED /

        AKM_Each_Cycle();

        if(converged!=1) {
            if(retVal) {
                HC_QSet_Color ("/","windows=pink");
                if (this->DBMirror() )
                    this->DBMirror()->post_summary_value( "Status","0 UserStopped");
            }
            else {
                HC_QSet_Color ("/","windows=light light grey");
                cout<< " need more cycles"<<endl;
                if (this->DBMirror() )
                    this->DBMirror()->post_summary_value( "Status","0 UNConverged");
            }
        }
        else  {
            HC_QSet_Color("/","windows=white");
            if (this->DBMirror() )
                this->DBMirror()->post_summary_value( "Status","2 FSI Finished");
            this->MakeLastKnownGood();
        }
        /* end screen color */
        if(retVal)
            break;

        struct tm *newtime;  // just a little aside to set the time properly
        time_t aclock;
        time( &aclock );                 /* Get time in seconds */
        newtime = localtime( &aclock );  /* Convert time to struct tm form  */
        QString  ttt(asctime(newtime)); // can we strip the trailing '\n'
        Post_Summary_By_Sail(NULL,"time",qPrintable( ttt.trimmed() ));
        if (this->DBMirror() ) {
            QString label=QString("$textresults");
            this->DBMirror()->PostFile(label,"shapeout.txt");

            // write this run to any otput log
            QStringList  dum;
            Do_LogResult(dum);
        }
        for(i=0;i<lsl.size();i++)  {
            SAIL *theSail = lsl[i];
            theSail->EvaluateAllReadOnly();
        }
#ifdef HOOPS
        Change_Hoops_Strings("/.../text_overlay","user options,text",g_World->m_LastRunName,g_World->GetRunName() ,g_World->Summout());
#endif

        if(!g_qAfterEachRun.isEmpty()    ) {
            char *buff = STRDUP(qPrintable(g_qAfterEachRun));
            if(this->DBMirror() )
                while(this->DBMirror()->Replace_Words_From_Summary(&buff,0) ) ;
            Execute_Script_Line(buff,0);//RXSubWindowHelper::rootgraphic);
            RXFREE(buff);
        }

        if( relaxflagcommon.VPP_Connected == 1)
            cout<<"TODO :: Active_VPP_Call();"<<endl;

        if(g_CH_Run_Data.run_to_line > 0) {
            g_CH_Run_Data.chosen++;
            if(g_CH_Run_Data.chosen < g_CH_Run_Data.run_to_line) {
                rcount++;
                cout<<"Running line "<<g_CH_Run_Data.chosen <<" in datafile '"<< g_CH_Run_Data.m_label.c_str()<<"'"<<endl;
                cout<<"This is called '"<<g_CH_Run_Data.list[g_CH_Run_Data.chosen]<<"'"<<endl;

                sprintf(buf,"Running line %d (%s)..", g_CH_Run_Data.chosen, g_CH_Run_Data.list[g_CH_Run_Data.chosen]);
                if(g_CH_Run_Data.sum ) {
                    g_CH_Run_Data.sum->read_and_post_one_row(g_CH_Run_Data.chosen, QString(g_CH_Run_Data.list[g_CH_Run_Data.chosen]),g_World->DBMirror());
                    if( g_World->DBMirror())
                        g_World->DBMirror()->Apply_Summary_Values();
                }
#ifdef HOOPS
                Change_Hoops_Strings("/.../text_overlay","user options,text", g_World->m_LastRunName,g_World->GetRunName(),g_World->Summout());
#endif
                mkFlag(g_Analysis_Flags,TRIM_CHANGE);/* WHY??  WRONG */
                SetMsgText(buf);
            }
        } // if runtoline>0
        else {
#ifdef HOOPS
            Change_Hoops_Strings("/.../text_overlay","user options,text",g_World->m_LastRunName,g_World->GetRunName(),g_World->Summout());
#endif
            IncrementRunName();
        }

        if(retVal)	  break;

    }while(g_CH_Run_Data.run_to_line > 0 && g_CH_Run_Data.chosen < g_CH_Run_Data.run_to_line && !(relaxflagcommon.Stop_Running));
    g_Analysis_Flags = 0;  /* clears all the flags */
    if(g_CH_Run_Data.run_to_line >  0) {
        sprintf(buf,"Ran %d lines",rcount);
        rxerror(buf,101);
    }
    else
        g_World->OutputToClient("analysis DONE",101);
    g_CH_Run_Data.run_to_line = 0;  /* stop it for next time */

    return(retVal);
}


int RelaxWorld::run_stress(control_data_t *data)
{
    int retVal=0;
    size_t i;
    char cl[256];
    char buf[256]; *buf=0;

    memset(cl,' ',sizeof(char)*256);  cl[0] = '\0';

    if(data->press != RELAX_PRESSURE)
        strcpy(cl,"/NW");
    else
        strcpy(cl,"  ");

    if(rxFlag(data->relax_flags,NON_LINEAR_FLAG))
        strcat(cl,"/NL");
    if(rxFlag(data->relax_flags,CREASING_FLAG))
        strcat(cl,"/D2");
    strcat(cl,data->extra_flags_str);

    if(CNPRNT) printf("calling relax with flags = '%s'\n",cl);


    vector<RXSail*> rc;
    rc = g_World-> ListModels(RXSAIL_ALL);
    for(i=0;i<rc.size();i++)  {
        sprintf(buf,"rxshape.0%lu",(unsigned long)i+1);
        if(validfile(buf) == 1) {
            sprintf(buf,"mv rxshape.0%lu  shapeold.0%lu",(unsigned long)i+1,(unsigned long)i+1);
            system(buf);
        }
    }

    relaxflagcommon.Stop_Running = 0;
    if(strstr(data->extra_flags_str,"norun"))
        relaxflagcommon.Mass_Set_Flag = 2;

    cf_rlx_go(&retVal,&(data->m_ConvLimit),&g_Relax_V_Damp,cl);//was g_Relax_Tolerance
    relaxflagcommon.Mass_Set_Flag = 1;

    mkGFlag(GLOBAL_DO_RELAX_TEXT);
#ifdef linux
    system("rm -f  shape.out shapeout.txt");
    system("cat totalforces.txt anchorloads.txt rxshape* > shapeout.txt");
    system("rm -f totalforces.txt anchorloads.txt");
#else
    system("del  shape.out shapeout.txt");
    //	system("type totalforces.txt anchorloads.txt shape.* > shapeout.txt");
    system("del  toTalforces.txt anchorloAds.txt");
#endif
    //    updateOneTextWindow(GLOBAL_DO_RELAX_TEXT);
    if(!g_World)  // can occur when we quit during analysis
        return retVal;
    if(retVal)  {
        g_World->OutputToClient("results may be unconverged !! ",1);
        HC_QSet_Color("/","windows=pink");
        if(g_World->DBMirror() )
            g_World->DBMirror()->post_summary_value( "Status","0 Cancelled");
    }
    g_World->OutputToClient("cycle done",101);
    g_World->PostSomeResults();
    return(retVal);
}

int RelaxWorld::run_wind(control_data_t *data,	std::map <int,struct PCN_PANSAIL *> panList)
{
    int retVal=0;

    if(CNPRNT) cout<< "called run_wind"<<endl;
    /// first, lets void LocateInitialize();  all the models
    vector<RXSail*> lsl =  g_World->ListModels(RXSAIL_ALLHOISTED); // boat too
    for(size_t i=0;i<lsl.size();i++)  {
        SAIL *s = lsl[i];
        s->LocateInit();
    }


    g_World->m_wind.m_Appwind_Untransformed = 0; // turned on by RelaxPressure

    switch(data->press) {

    case WERNER_PRESSURE:
        g_World->run_Werner();
        break;
    case FILE_PRESSURE:
    case CUSTOM_PRESSURE:
    case CONSTANT_PRESSURE:
    case SCRIPT_PRESSURE:
    case FIELD_PRESSURE:
    case INTERPOLATE_PRESSURE:
        break;

    case RELAX_PRESSURE:
        assert(0);
        //        g_World->m_wind.g_Appwind_Untransformed = 1;
        //        refwind =  g_World->m_wind.Get(RXWindGeometry::Reference_Length );

        //        g_AWA = 0.0;  /* or else appwind takes it into account */
        //        appwind_(&refwind,&u,&v,&w);
        //        aws = sqrt(u*u + v *v + w*w);
        //        g_AWA = -atan2( v,u);
        //#ifdef _X
        //        update_control_form_awa (g_trim_shell, (double) g_AWA, (double) aws);
        //#endif
        //        sprintf(buf,"%f" ,-g_AWA*57.29577951);
        //        if (g_World->DBMirror() )
        //            g_World->DBMirror()->post_summary_value("g_AWA",buf);
        //        break;

    } //switch

    g_World->SetPressuresOnActive(data);   /* takes care of itself to choose what to set as pressures */

    g_World->PostProcessAllSails(1);

    return(retVal);
}
