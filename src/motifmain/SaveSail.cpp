
/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
 *	shape.%2.2d to deal OK with 10 or more models
   May 1997. The boat also gets a .gen file for debugging
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* SaveSail() - writes the bagged sail info + the mesh */

#include "StdAfx.h"  
#include <QDir>
#include "rxsubwindowhelper.h"
#include "RXSail.h"  
#include "RelaxWorld.h"
#include "RXEntity.h"
#include <time.h>
#include "rxON_Extensions.h"
#include <QFileInfo>
#include "f90_to_c.h"
#include "global_declarations.h" 

#include "entities.h"
#include "akmutil.h"
#include "OpenBuildFile.h" 
#include "rxqtdialogs.h"

#include "SaveSail.h" 

// see OpenBuildFile.cpp.  May 2012 modified so that it no llonger writes a bag/script pair. It puts all the data in the bagfile
int SaveSail(Graphic *g,const int type,const int SaveGeneratedToo) { //  uses g->m_qfilenameint SaveSail(Graphic *g,const int type,const int SaveGeneratedToo)
#ifdef _X
    static char windowName[256];
#endif
   // FILE *fbag;
    QString fileBase;
    char  buf[256];
    RXEntity_p  p=0;
    setStatusBar("Saving..." );
    
    if(!g->GSail) {
        g_World->OutputToClient ("no sail",2);
        return(0);
    }
    QFileInfo fi( g->m_qfilename );
    assert(fi.isAbsolute());
    fileBase=fi.dir().absoluteFilePath(fi.completeBaseName());//        g->m_qfilename = fileBase+".bag";
#ifndef USE_INPUT_CARD
    while((p = g->GSail->GetFirst("infile")))
        p->Kill();

     if(type == WT_DESIGN) { // save all into the bagfile

        sprintf(buf,"%s_flat.ndd",qPrintable(fileBase));
        g->GSail->write_ND_state(buf);  /* zero state info */

        sprintf(buf,"%s.bag",qPrintable(fileBase));
        g->GSail->m_fileBAG = buf;

        g->m_qfilename = fileBase+".bag";
        g->GSail->SetFileIN(g->m_qfilename) ;
        g->GSail->Write_Script(qPrintable(g->GSail->ScriptFile() ),(SaveGeneratedToo==0)); /* write basic info */

#ifdef _X
        Arg al[64]; int l_ac=0;
        sprintf(windowName,"%s boat: %s",RELAX_VERSION,g->filename);
        l_ac=0;
        XtSetArg(al[l_ac],XmNtitle,windowName);l_ac++;
        XtSetValues(g->topLevel,al,l_ac);
        XmUpdateDisplay(g->topLevel);
#endif
    }
#else // old-style. write the bag/script pair
    if(type == WT_DESIGN) {
        g->m_qfilename=fileBase+".bag";
        fi.setFile(g->m_qfilename ) ;

        sprintf(buf,"%s_flat.ndd",qPrintable(fileBase));
        g->GSail->write_ND_state(buf);  /* zero state info */
 // 1)  Write the BAGfile

        fbag = RXFOPEN(qPrintable(g->m_qfilename),"w");
        if(!fbag) g_World->OutputToClient (qPrintable(g->m_qfilename),4);
        fprintf(fbag,"! RELAX II - SAILBAG\n");

        rxON_String ttt= rxON_String(g->GSail->Serialize().Array());
        ttt.Replace_String(RXENTITYSEPS,"\t");
        fprintf(fbag,"%s\n",ttt.Array());

        fprintf(fbag,"!Original data template  was '%s\n",qPrintable(g->GSail->ScriptFile()));

        sprintf(buf,"%s.script",qPrintable(fileBase));
        g->GSail->SetFileIN(fileBase+".script" )  ;

        g->GSail->Write_Script(qPrintable(g->GSail->ScriptFile() ),(SaveGeneratedToo==0));

        g->GSail->ClearFlag(FL_NEEDS_SAVING);
        QFileInfo  fiBase(g->GSail->ScriptFile());
        QString localScriptName = fiBase.fileName(); // just the filename. Same Dir as BAG
        fprintf(fbag,"input\t %s \t %s ! %s\n",g->GSail->GetType().c_str(),qPrintable(localScriptName) , qPrintable(g->GSail->ScriptFile()));	/* xxxx.IN filename */

        FCLOSE(fbag);

        g->m_qfilename = g->GSail->m_fileBAG = fileBase+ QString(".bag");

#ifdef _X
        Arg al[64]; int l_ac=0;
        sprintf(windowName,"Sail: %s",g->filename); // Feb 2003
        l_ac=0;
        XtSetArg(al[l_ac],XmNtitle,windowName);l_ac++;
        XtSetValues(g->topLevel,al,l_ac);
        XmUpdateDisplay(g->topLevel);
#endif
    }
#endif
    else if(type == WT_BOATFILE) {

        sprintf(buf,"%s_flat.ndd",qPrintable(fileBase));
        g->GSail->write_ND_state(buf);  /* zero state info */

        sprintf(buf,"%s.INbase",qPrintable(fileBase));// a debug measure.  Used to be .bag

        g->GSail->m_fileBAG = buf;
        sprintf(buf,"%s.in",qPrintable(fileBase));
        g->m_qfilename = fileBase+".in";
        g->GSail->SetFileIN(g->m_qfilename) ;
 #ifdef USE_INPUT_CARD
        QFileInfo  fiInFile(g->m_qfilename);
        QString localScriptName = fiInFile.fileName();
        sprintf(buf,"input\t%s\t %s ! %s\n",g->GSail->GetType().c_str(),qPrintable(localScriptName) , qPrintable(g->GSail->ScriptFile()));	/* xxxx.IN filename */

        //    sprintf(buf,"input : %s : %s",g->GSail->GetType().c_str(),qPrintable(g->GSail->ScriptFile() ));
        if((p = g_boat->GetKeyWithAlias("input",g->GSail->GetType().c_str()))) {
            if(p->line)
                RXFREE(p->line);
            p->line = STRDUP(buf);
        }
        else
            Just_Read_Card(g_boat,buf, "input",g->GSail->GetType().c_str(),&depth);
#endif
        assert(g);
        g->GSail->Write_Script(qPrintable(g->GSail->ScriptFile() ) ,(SaveGeneratedToo==0)); /* write basic info */

#ifdef _X
        Arg al[64]; int l_ac=0;
        sprintf(windowName,"%s boat: %s",RELAX_VERSION,g->filename);
        l_ac=0;
        XtSetArg(al[l_ac],XmNtitle,windowName);l_ac++;
        XtSetValues(g->topLevel,al,l_ac);
        XmUpdateDisplay(g->topLevel);
#endif
    }
    else
        assert("In SaveSail.c with unknown WT_TYPE"==0);
    setStatusBar(QString("Sail Saved: ")+QString(g->GSail->GetType().c_str()) );

    return(0);
}

int RXSail::save_state(const char *filename)
{
    char buf[256];
    char *lp;
    char fileBase[256];
    time_t t1;
    FILE *fp;

    strncpy(fileBase,filename,250);
    lp = strstr(fileBase,".");
    if(lp)
        *lp = '\0';

    strcat(fileBase,".sst");

    lp = strstr(fileBase,".");
    if(lp)
        *lp = '\0';
    strcat(fileBase,".ssh");

    fp = RXFOPEN(fileBase,"w");
    if(!fp) {
        this->OutputToClient("cant open .ssh file !",2);
        return(0);
    }
    time(&t1);
    fprintf(fp,"Saved State on :  %s\n",asctime(localtime(&t1)));
    fprintf(fp,"Sail Bag       :  %s\n",qPrintable(this->m_fileBAG));
    fprintf(fp,"Wind data      : Heading    = %12.5f\n",g_World->m_wind.Get(RXWindGeometry::Heading));
    fprintf(fp,"               : WindSpeed  = %12.5f\n",g_World->m_wind.Get(RXWindGeometry::Vwind));
    fprintf(fp,"               : Boat Speed = %12.5f\n",g_World->m_wind.Get(RXWindGeometry::Vboat));
    fprintf(fp,"               : Heel Angle = %12.5f\n",g_World->HeelAngle());
    FCLOSE(fp);

    sprintf(buf,"cat rxshape%2.2d.txt >> %s",this->SailListIndex(),fileBase);
    system(buf);


    lp = strstr(fileBase,".");
    if(lp)
        *lp = '\0';
    strcat(fileBase,".ndd");
    this->write_ND_state(fileBase);

    return(0);
}

int Do_Save_Script(SAIL *s,Graphic *g)
{
    int retVal = 0;
    int valid;
    char buf[128];

    if(!s || s->ScriptFile().isEmpty()) {
        s->OutputToClient("No Filename found in this model",2);
        retVal = 1;
    }
    valid = validfile(qPrintable(s->ScriptFile()));

    if(valid < 0) {
        sprintf(buf,"File '%s' could not be written",qPrintable(s->ScriptFile()));
        s->OutputToClient(buf,2);
        retVal = 2;
    }
    else if(valid == 1) {
        /* file already exists so ask if want to overwrite */
        sprintf(buf,"Overwrite file '%s' ?",qPrintable(s->ScriptFile()));
        if(s->OutputToClient(buf,2)) {
            retVal = 3;
        }
    }

    if(retVal == 0) {
        assert(g);
        g->GSail->Write_Script(qPrintable(s->ScriptFile()),1); /* write basic info */
    }

    return(retVal);
}

