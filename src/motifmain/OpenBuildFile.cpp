/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 *
 *
 * Modified :
  * 14.6.96 attempt to Delete Whole Database on close sail DO NOT SHIP BEFORE TESTING WELL
 * 16/5/96  commas taken from default atts, Excel doesnt like them
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/* OpenBuildFile() opens a new sail */

#include "StdAfx.h"
#include <QDebug>
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "RelaxWorld.h"
#define DBG 0
#ifdef _X
#include "Xm/Protocols.h"
#include "MakeNewDrawingArea.h"
#endif

#include "rxsubwindowhelper.h"

#include "drop.h"

#include "script.h"
#include "ReadBagged.h"

#ifdef _X
#include "savesailas.h"
#include "sailmenu.h"
#endif

#include "global_declarations.h"
#ifdef RXQT
#include "rxqtdialogs.h"
#endif
#include "OpenBuildFile.h"


int ReadScript(Graphic *g,const QString filename,int hint_depth)
{
    QString buf;
    int depth;
    int retVal=1;

    buf="Reading '"+ filename +"'";
    setStatusBar (buf);
#ifdef HOOPS
    HC_Open_Segment_By_Key(g->m_ModelSeg);
    HC_Open_Segment("data");
    HC_Set_User_Options("adjectives=\" flat$xscaled$yscaled$draw  \" ");
#endif

    depth=hint_depth;
    g->GSail->SetFileIN(filename); // peter aug 2012
    g->GSail->startmacro(qPrintable(filename),&depth);
    if(g->GSail->Pre_Process(NULL) <= 0)  {
        retVal=0;
    }
#ifdef HOOPS
    HC_Close_Segment();
    HC_Close_Segment();	    /* close the base window segment */
#endif
    assert(g->GSail->GetGraphic()==g);
    g->GSail->SetFlag(FL_HASGEOMETRY,1);
    g->GSail->SetFlag(FL_HASPANELS,1);

    setStatusBar ("Ready(ReadScript)");
    return(retVal);
}

Graphic *OpenBag(const QString filename,int *err)
{
    //    Historically, a model was saved as a .bag file, which (only) contains a reference (an 'input' line)
    //   to a .script file which holds the data
    // That was silly. It is better to have a single file
    //  This function still reads the old bag/script pairs, triggered by the bag containing an 'input' line refering to the .script
    //  but it can equally read a model as a single file.
    //  Write_Script no longer writes the 'input' card, and SaveAs no longer generates a bag/script pair

    char buf[256];
    int retVal=1;
    Graphic *g=NULL;

#ifdef _X
    const char *retStr[2] = {"FAILED","SUCCESS"};
    Arg al[64];
    int l_ac=0;
#endif 
    (*err) = -1;  /* failed */

    /* checks if open already - returns it if it is  */

    if((g = RXSubWindowHelper::is_bag_open(filename)) != NULL) {
        (*err) = 0;
        printf("bag '%s' already open\n",qPrintable(filename));
        if(g && g->GSail &&  g->GSail->GetFlag(FL_NEEDS_GAUSSING))
            Do_Solve(NULL,g); // added March 2003  but BringAll takes care of it
        return(g);
    }

    /* otherwise open a design window, read the bag */

    g = new RXSubWindowHelperDesign();
    g->GSail->SetFileIN(filename );
    setStatusBar (QString("Process BAG :") + filename );

    // this script may contain an 'input' card to reference another script, or it may be autonomous.
    retVal = ReadScript(g,filename,0);// the bag   May 2012 depth now zero so we don't need an input file

    if(!g->GSail->IsOK() || !retVal ) {
        setStatusBar ("Process script '" + filename +"' failed !");
        (*err) = -2;
        return(g);
    }
    //grandfather the old bag/script pair.
    if(!g->GSail->ScriptFile().isEmpty()) {
        if( g->GSail->ScriptFile()!=filename){  // this happens for the old bagfile/scriptfile pair because the 'inpit' line modifies Sail->ScriptFile()
            retVal = ReadScript(g,g->GSail->ScriptFile(),0); // the .script file
            if(!retVal) {
                setStatusBar ("Process script '" + filename +"' failed !");
                (*err) = -3;
                return(g);
            }
        }
//        else
//            qDebug()<<" dont read "<<filename<<" a second time";
    }
    assert(!cout.bad());
    Do_Mesh(NULL,g);

    g->GSail->m_fileBAG = QString(filename);
    g->m_qfilename = g->GSail->m_fileBAG ;
    sprintf(buf,"Sail: %s",qPrintable(g->m_qfilename)); // feb 2003
#ifdef _X
    l_ac=0;
    XtSetArg(al[l_ac],XmNtitle,buf); l_ac++;
    XtSetArg(al[l_ac],XmNiconName,g->GSail->GetType().c_str());l_ac++;
    XtSetValues(g->topLevel,al,l_ac);
#endif

    setStatusBar ("Ready" );
    (*err) = 0; /* OK */
    return(g);
}

#ifdef _X
void SailDeleteCB(Widget w, XtPointer client_data, XtPointer call_data) // keep as a reference this is the  old callback on deleting a design windos.
{
    Graphic *g;
    char buf[256];

    g = (Graphic *) client_data;

    if(!g  )
        return;  /* already been here */

    if(g->GSail->Needs_Saving()) {
        if(g->GSail->IsBoat()){
            sprintf(buf,"SaveAs:  : %s/*.in",g_currentDir);
            printf(" a BOAT %s\n", buf);
        }
        else {
            sprintf(buf,"SaveAs:  : %s/*.bag",g_currentDir);
        }
        Execute_Script_Line(buf,g);
    }
    if(g->GSail->IsHoisted()){
        sprintf(buf,"Forcing DROP on %s",g->GSail->GetType().c_str());
        g_World->OutputToClient (buf,1);
        Do_Drop_Sail(NULL, g);
    }

    g->GSail->~RXSail();
    cout<< "(SailDeleteCB):: Are we sure that ~RXSail() does all the clearing up on its own?"<<endl;

} // SailDeleteCB

Graphic *OpenDesWindow() {// NEVER

    RXSubWindowHelperDesign *gfree = new RXSubWindowHelperDesign();
    //RXSubWindowHelper::nextGraphic(WT_DESIGNWINDOW);

    create_top_design(gfree,g_graph[0].topLevel);
    XtPopup(gfree->topLevel,XtGrabNone);
    XFlush(XtDisplay(gfree->topLevel));

    gfree->GSail = new RXSail(g_World);
    gfree->GSail->SetGraphic(gfree);
#ifdef RXQT
    RXGRAPHICSEGMENT w = RXNewWindow("Design Window", gfree->GSail);
    gfree->GSail->setGNode(w);
#endif
    return(gfree);
}

#endif

#ifdef  _X
void create_top_design (Graphic *g,Widget parent)
{
    Atom	wm_delete_window;
    char buf[200];
    Arg al[64];
    int l_ac=0;
    XtSetArg(al[l_ac], XmNallowShellResize, TRUE); l_ac++;
    sprintf(buf,"%s Sail: ( Untitled )",RELAX_VERSION);
    XtSetArg(al[l_ac],XmNtitle,buf);l_ac++;
    XtSetArg(al[l_ac],XmNiconName,"GAUSS Design");l_ac++;
    //XtSetArg(al[l_ac],XmNwidth,600);l_ac++;
    //XtSetArg(al[l_ac],XmNheight,650);l_ac++;
    XtSetArg(al[l_ac],XmNallowShellResize,True);l_ac++;
    //XtSetArg(al[l_ac],XmNwinGravity,NorthWestGravity);l_ac++;
    /* XtSetArg(al[l_ac],XmNdeleteResponse,XmDO_NOTHING); l_ac++;  */
    if(parent == NULL) {
        g_World->OutputToClient ("parent = NULL",2);
    }
    g->topLevel = XtCreatePopupShell ( "design", topLevelShellWidgetClass, parent, al, l_ac );
    l_ac = 0;
    g->mainWindow = XmCreateMainWindow(g->topLevel,(char*)"mainWindow",al,l_ac);
    XtManageChild(g->mainWindow);
    g->menuBar = CreateSailMenuBar(g->mainWindow,g);
    XtManageChild(g->menuBar);

    CreateMsgArea(g);
    l_ac = 0;
    g->workArea = MakeNewDrawingArea(g->mainWindow,"sailbase",g,(HBaseModel*) 0); // let the widget make the HBM
    XmMainWindowSetAreas(g->mainWindow,g->menuBar,NULL,NULL,NULL,g->workArea);
    l_ac=0;
    XtSetArg(al[l_ac],XmNmessageWindow,(XtArgVal) g->msgform);
    l_ac++;
    XtSetValues(g->mainWindow,al,l_ac);

    wm_delete_window = XmInternAtom(XtDisplay(g->topLevel),
                                    (char*)"WM_DELETE_WINDOW",
                                    false);

    XmAddWMProtocolCallback(g->topLevel, wm_delete_window, SailDeleteCB, (XtPointer) g);

}
#endif

