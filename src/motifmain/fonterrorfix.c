
/* an error handler that removes the font error */

/* 
category    An integer. Identifies the general category the error belongs to, out of      
            those defined in the hpserror.h include file. For example, memory errors      
            fall in the HEC_MEMORY error category.                                        
specific    An integer. Identifies the specific error out of those defined in the hps     
            error.h include file. For example, running out of memory.                     
            HES_OUT_OF_MEMORY, is a specific memory error in the                          
            HEC_MEMORY category.                                                          
severity    An integer. Indicates the seriousness of the offense. In particular, a "0"    
            means the message is just informational, a "1" means the message is a         
            warning (but the system is going to go ahead with the operation), a "2"       
            is a generic "error", and a "3" is a fatal error (indicating that something   
            is drastically amiss, and there's no reasonable way to continue.)             
msgc        An integer. Indicates how long msgv is.                                       
msgv        An array of pointers to character strings, with one string for each line of   
            the message.                                                                  
stackc      An integer. Indicates how long stackv is.                                     
stackv      An array of pointers to character strings, where each string is the name      
            of a currently active HOOPS routine. The originally called routine            
            would be the first one in the list.                                           
  */
//#include "StdAfx.h"  better to put ifdef c++ in it
#include "hc.h"
#include <stdio.h>
#include "fonterrorfix.h"

extern int g_Hoops_Error_Flag;

/* category, specific, severity, msgc, msgv, stackc, stackv)
*/

void PC_Report_Error_Without_Font (int cat,int spec,int sev,int msgc,const char *msgv[],int stackc,const char*stackv[]){

	switch (cat){
			case 2: // unexpected option type
			case 48:
			case 50:
			{	if(spec!=309){
				PC_Print_Error(cat,spec,sev,msgc,msgv,stackc,stackv);
				g_Hoops_Error_Flag = 1;
				}
				break;
			}
			case 17: {
				PC_Print_Error(cat,spec,sev,msgc,msgv,stackc,stackv);
				break;
			}
			case 58:
			case 94: // non square pixels for fonts
				{
				g_Hoops_Error_Flag = 0;
				break;
				}
				
		//	case 48:  // 48/254 is include non-existent segment(s)
		//	{
		//		printf(" case 48 ( %d %d %d)\n",cat, spec, sev );
		//		break;
		//		}
			case 5:
			case 6: // 6/344 is bad color
			case 8:
			case 25:
			case 46:
			//case 48:
			case 51:
			case 60:
			case 61: // 318 is Re-defining existing alias
			case 68:
				{
					//PC_Print_Error(cat,spec,sev,msgc,msgv,stackc,stackv);
					//g_Hoops_Error_Flag = sev+1;
					 g_Hoops_Error_Flag=0;
					break;
					 }
			case 3:

				PC_Print_Error(cat,spec,sev,msgc,msgv,stackc,stackv); // and hoops error
				 g_Hoops_Error_Flag=6;
				 return;
			default: {
					HC_Report_Error(cat,spec,sev,msgc,msgv,stackc,stackv);
					g_Hoops_Error_Flag = sev+1;
					 }
	}
  g_Hoops_Error_Flag=0;// we don't wan t to assert this
	//printf(" cat %d hoops error flag now %d\n", cat, g_Hoops_Error_Flag);
}

int PC_Print_Error(int cat,int spec,int sev,int msgc,const char *msgv[],int stackc,const char*stackv[]){
	FILE *fp;
	int k;
	fp = stdout; //fopen("hoopserrs.txt","a");
	if(fp) {		
			fprintf(fp, "\n****************\n PC_HOOPS ERROR cat = %d spec=%d sev=%d\n", cat,spec,sev);
			for(k=0;k<msgc;k++)
				fprintf(fp," message %d is %s\n", k, msgv[k]);
			for(k=0;k<stackc;k++)
				fprintf(fp," stack   %d is %s\n", k, stackv[k]);
			
			fprintf(fp, "****************\n");	
			fflush(fp);
		//	fclose(fp);
	}
	return 0;
}

int PC_Set_Error_Reporting() {
	FILE *fp;
	fp = fopen("hoopserrs.txt","w");
	if(fp) fclose(fp);
	//puts("PC_Set_Error_Reporting suppressed");  return 0;
#ifndef __cplusplus
	HC_Define_Error_Handler(&PC_Report_Error_Without_Font);
	HC_UnDefine_Error_Handler(&HC_Report_Error);
#else
//	assert(0); //"thomas moved to cpp but was not able to deal with pointer on functions");
#endif

	return 1;
}
