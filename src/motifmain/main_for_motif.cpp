/* main program for RelaxII
 * Copyright Peter Heppel Associates 1994/2011
 */ 

#include "StdAfx.h"
#include <pthread.h> // for nanosleep
#include "global_declarations.h"
#include "CheckAll.h" // for AKM_Each_Cycle
#include "stringutils.h"
#include "commslib.h"
#include "fonterrorfix.h"
#include "mainstuf.h"


#define V " v3.00-00-10Q "
Arg al[64]; // the global one.  Changed from 20 just in case Oct 2002

#ifdef linux
	#ifndef NO_PANSAIL
		#ifdef JANET
		const char *RELAX_VERSION=	"Relax/JANET"
						V
						"64bit linux++";
		#else
		const char *RELAX_VERSION=	"Relax/PANSAIL"
						V
						"64bit linux++";
		#endif
	#else
		char *RELAX_VERSION=		"Relax"
						V
						"64bitNP linux++";
	#endif
#else
	#ifndef NO_PANSAIL
		char *RELAX_VERSION=		"Relax/PANSAIL "
						V
						" 64bit";
	#else
		char *RELAX_VERSION=		"Relax "
						V
						"64bitNP";
	#endif
#endif
// *******************************BEGINNING OF THE 'MAIN' FUNCTION****************************


 int main( int argc,char **argv) 
{
         relaxinit_1();

         XtToolkitInitialize ();
         g_app_context = XtCreateApplicationContext ();

        char *display_name=NULL;
	if(getenv("DISPLAY") )
		display_name = STRDUP(getenv("DISPLAY"));
	else
		display_name=0;
 	 if (is_empty(display_name)) {
    		fprintf(stderr, "Unable to find a DISPLAY environment variable\n");
   		return (0);
 	 }

  strcpy(g_Application_Name, argv[0]);
  g_display = XtOpenDisplay (g_app_context, display_name,argv[0], "RelaxII", NULL, 0,(int *)&argc, argv);

  if (!g_display)
    {
      printf("%s: can't open display, exiting...\n", argv[0]);
      exit (-1);
    }
   relaxinit_2( argc,argv , NULL );/* initialize some things for dialogs */

	PC_Set_Error_Reporting() ;
  XtInputMask xmask;
  while(true) {
    	xmask = XtAppPending(g_app_context);
    	if(xmask) 
      		XtAppProcessEvent(g_app_context,xmask);
   	  else if(g_Flag) {
      		 AKM_Each_Cycle();
	 }
    	 PCC_ProcessSocketMessages(); 
  }
// lets  do AKM_Each_Cycle and  PCC_ProcessSocketMessages();  in the idle fn (use QTimer)
  // then once we have a qt socket server we retire PCC_ProcessSocketMessages();
  return(0);
}



