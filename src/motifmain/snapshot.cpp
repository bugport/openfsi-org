/* RelaxII source code
 to take a snapshot of the current session
 * Copyright Peter Heppel Associates 1996
 *
 * written by  	P Heppel
 *
 Oct 98.  BUG FIX  fn snapshot  could overwrite filename. Now insulated

 A snapshot file consists of a script to restore the current program stare
1) CD to  CWD
2) Open the boat
3) Hoist the sails
4) Open the summary files. Choose the right line
5) Load a state on all the sails
6) Open the cameras and set them up 
7) Calculate

 * Modified :
 
 */

#include "StdAfx.h" 
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "rxsubwindowhelper.h"


#include "stringutils.h"

#include "parse.h"

#include "script.h"

#include "global_declarations.h"
#include "RelaxWorld.h"
#include "RXLogFile.h"
#include "snapshot.h"



int snapshot(const char*filein) {
    FILE *fp;
    char *lp;

    char*filename = (char*) MALLOC(strlen(filein)+8);
    strcpy(filename, filein);
    if((lp=strrchr(filename,'.'))) *lp=0;
    strcat(filename, ".mac");


    if(!(fp =FOPEN(filename,"w") )) { RXFREE(filename); return 0;}

    fprintf(fp," RELAX SAVE SESSION script file <%s> \n",filename);
    fprintf(fp,"cd: %s\n", g_currentDir);

    if ((true)) {
        if(g_boat) {
            if(g_boat->GetFlag(FL_NEEDS_SAVING))
                g_boat->OutputToClient("Please save the boat and then Save Session again",2);
            if(!g_boat->m_fileIN.Array()) {
                char buf[350];
                g_boat->OutputToClient("Cant Save Session. Please save the boat (name null) and try again",2);
                FCLOSE(fp);
                sprintf(buf, "rm %s", filename);
                system (buf);
                RXFREE(filename);
                return 0;
            }
            fprintf(fp,"open boat : %s \n", g_boat->m_fileIN.Array());
        }
        else {
            fprintf(fp,"!NO BOAT FILENAME\n");
            fprintf(fp,"!open boat : \n");
        }
    }
    else fprintf(fp,"!  NO BOAT MODEL\n");

    fprintf(fp,"\n! the Hoists \n");
    RXSubWindowHelper::Write_All_Hoists(fp);
    fprintf(fp,"\n! the SUMMARIES \n");

    if(g_World->Summout())
        fprintf(fp,"%s\n",g_World->Summout()->ScriptCommand ().c_str());
    //		if(g_World->Summout()->GetFileName())
    //			fprintf(fp,"open_summary: %s\n",g_World->Summout()->GetFileName());

    if(g_World->m_Last_Summary_File) {
        fprintf(fp,"open_input: %s\n",g_World->m_Last_Summary_File);
        fprintf(fp,"choose_line: %d  ! was %s\n", g_World->m_Last_Summary_Line,g_World->GetRunName()); // may be  SUMMARY_TAIL
    }
    if(g_World->Summin())
        fprintf(fp,"%s\n",g_World->Summin()->ScriptCommand ().c_str());
    //if(g_World->Summin()->GetFileName()
    //	&& (!is_empty(g_World->m_Last_Summary_File)
    //	&&(!strieq(g_World->Summin()->GetFileName(), g_World->m_Last_Summary_File) ) )) {
    //	fprintf(fp,"open_input: %s\n",g_World->Summin()->GetFileName());
    //}

    fprintf(fp,"\n! the VIEWS \n");
    RXSubWindowHelper::Write_All_Cameras(fp);
    fprintf(fp,"\n! the STATES \n");
    RXSubWindowHelper::Write_All_States(filename,fp);

    fprintf(fp,"! Calculate:\n");
    //Write_All_Graphics(fp);
    FCLOSE(fp);
    RXFREE(filename); filename=NULL;

    return(1);
} 
