/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


#include "StdAfx.h" 

#include "global_declarations.h"
#include "commslib.h"
#include "CheckAll.h"
#include "GraphicStruct.h"

#include  "akmupdis.h"

#define MAX_EVENTS 1000
#ifndef _X
int akm_update_display_(int *what)
{
    return 1;
}

#else

int akm_update_display_(int *what)
{
  static int 		i;
  static XEvent 		xevent;
  static XKeyEvent	*inputkey;
 // static XButtonEvent	*button;
  static KeySym		keysym;
  static char		buff[8];
  static int		count=0;
  
  static XtInputMask xmask;
/* printf("Entered AKM_Update with %d\n",*what); */
  count++;
  if(count > 2) {
    count = 0;
  }
  else {
    return(1);
  }
   	if(g_graph[0].EMPTY) 
		puts(" EMPTY g_graph[0] in akm Update_Display");
	else {
  		if(g_graph[0].topLevel) 
			XmUpdateDisplay(g_graph[0].topLevel); 
		else
			puts(" NULL g_graph[0] in akm Update_Display");
	}
  switch(*what) {
  case 1: /* reload deflected shape and update everything */
    i=MAX_EVENTS;
    while(i)  {
      xmask = XtAppPending(g_app_context);
      if(xmask) 
	i--;
      else    
	return(1);
      XtAppProcessEvent(g_app_context,xmask);
    }

  HC_Update_Display();
  return(1);
  break;

  case 2:
    i=MAX_EVENTS;
    while(i)  {
      	xmask = XtAppPending(g_app_context);
      	if(xmask) 
		i--;
     	 else    
		return(1);
      	XtAppProcessEvent(g_app_context,xmask);

	i = i -   PCC_ProcessSocketMessages(); 
    }

  HC_Update_Display();
  return(1);
  break;
  
  case 4:
	printf("akmUpdate with 4");
    i=MAX_EVENTS;
    while(i)  {
      	xmask = XtAppPending(g_app_context);
      	if(xmask) 
		i--;
     	 else    
		return(1);
      	XtAppProcessEvent(g_app_context,xmask);

	i = i -   PCC_ProcessSocketMessages(); 
    }
	AKM_Each_Cycle();
 	HC_Update_Display();
  return(1);
 
 case 28:   /* test routine */
 
  i=10; // 00; // Peter changed Dec 2003 was 1 SHOULD do a flush
  while(i)  {
    
    xmask = XtAppPending(g_app_context);
  //  printf("XtPending returnd %ld\n",xmask);
    if(xmask) 
      i--;
    
    switch(xmask) {
    case XtIMXEvent :
      printf("   This is an XtIMXEvent ");
      XtAppPeekEvent(g_app_context,&xevent);
      printf("event->type = %d\n",xevent.type);
      if(xevent.type == 2) {
	inputkey = (XKeyEvent *)(&xevent);
	XLookupString(inputkey, buff, 1, &keysym, NULL);
	printf("KeyPress:keycode = %d, state %d, buff=%c,\n",inputkey->keycode,inputkey->state, buff[0]);
	if(inputkey->keycode == 27 && inputkey->state == 4)  {  /* ctrl 'c' key ! */
	  g_World->OutputToClient ("Job cancelled by User ! ",1);
	  *what = 0;
	  return(0);
	  }
	  if(inputkey->keycode == 9 )  {  /* Esc key ! */
	  g_World->OutputToClient ("Job cancelled by User ! ",1);
	  *what = 0;
	  return(0);
	}
      }
      XtAppProcessEvent(g_app_context,xmask);
      
      break;
    case  XtIMTimer :
      printf("   This is an XtIMTimer\n");
      XtAppProcessEvent(g_app_context,xmask);
      break;
    case  XtIMAlternateInput :
      printf("   This is an XtIMAlternateInput\n");
      XtAppProcessEvent(g_app_context,xmask);
      break;
    }
  }
  return(1);
  break;
  
}
printf("AKM_Update_Display called with %d\n",*what);
return(1);

}
#endif

