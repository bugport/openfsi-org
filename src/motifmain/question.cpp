/* questions.c  26.6.96 PH */

/* 2/9/96  MALLOC prototyping */


#include "StdAfx.h" 
#include "RXSail.h"
#include "RXGraphicCalls.h"
#include "RXEntity.h"
#include "qmenudef.h"
#include "rxsubwindowhelper.h"
#include "RelaxWorld.h"

#include "stringutils.h"
#ifdef HOOPS
#include "hoopcall.h"
#endif
#include"getglobal.h"
#include "akmutil.h"
#include "printall.h"

#include "entities.h"
#include "panel.h"

#include "read.h"


#ifdef _X

#include <Xm/Text.h>  /* added 15/3/97 */
#endif

#include "xquestion.h"
#include "words.h"
#include "etypes.h"
#include "global_declarations.h"
#include "resolve.h"


#include "question.h"



#ifdef WIN32
#define _max max
#define _min min
//BC24/04/97#endif WIN32
#endif


int Fill_In_QData( char**p,char**d,char**t,char**a,int c){
    int k;
    struct QDATA_ROW  *Row;
    g_question_data=  (struct QDATA *) CALLOC(1, sizeof(struct QDATA));
    if(!g_question_data || c<=0) return 0;

    g_question_data->TheRows= (struct QDATA_ROW *)CALLOC((c+1),sizeof(struct QDATA_ROW));// sept 2003 was MALLOC

    g_question_data->m_Current_Text_Widget=NULL;
    g_question_data->current_row=NULL;
    g_question_data->Status_Flag=0;
    g_question_data->has_been_edited=0;
    g_question_data->Nrows = c;
    g_question_data->NOT_DONE = 1;
    Row= g_question_data->TheRows;
    for(k=0;k<c;k++,Row++ ) {
        Row->qrprompt = (p[k]);
        Row->qrdefault= (d[k]);
        Row->qrtype= (t[k]);
        Row->qrtext= (a[k]);
        Row->prompt_W=NULL;
        Row->default_W=NULL;
        Row->text_W=NULL;
        Row->QTYPE=0;
        Row->row_has_been_edited=0;
        Row->flag=0;
        Row->qdata_p= g_question_data;
    }
    return 1;
}

int Bring_Back_QData( char**p,char**d,char**t,char**a,int c){

    if(!g_question_data) return 0;

    if(! g_question_data->has_been_edited) return 0;
#ifndef _X
    cout<< "Bring_Back_QData needs X"<<endl;
#else
    int k; char*s;
    struct QDATA_ROW  *Row;
    Row= g_question_data->TheRows;
    for(k=0;  k<c && k<g_question_data->Nrows ;k++,Row++ ) {
        if(Row->row_has_been_edited  && Row->text_W) {
            s = XmTextGetString( Row->text_W);
            RXFREE(a[k]);
            a[k] = STRDUP(s);
            Row->row_has_been_edited=0;
        }
    }
#endif
    return 1;
}



int XQuestion(char**p,char**d,char**t,char**a,int c,const Graphic *g){
#ifndef _X
    cout<< "XQuestion  needs X"<<endl;
#else
    XtInputMask xmask;

    if(!c)  return 0;
    if(!Fill_In_QData( p,d,t,a, c)) {g_World->OutputToClient (" CANT Question",2); return 0;}

    /* this works 	question_shell  = create_question_shell(g_graph[0].topLevel);	 >topLevel */
    if(!g_question_data) { g_World->OutputToClient ("cant create shell with no lines",2); return 0;}

    g_question_shell  = create_question_shell(g->topLevel); /* this is untried */
    g_question_data->m_q_shell = g_question_shell;
    XtManageChild(g_question_shell->m_question_shell);
    XtRealizeWidget(g_question_shell->m_question_shell);
    XtMapWidget(g_question_shell->m_question_shell);
    XtAddGrab(g_question_shell->m_question_shell ,True,False); /* this is untried */


    while(g_question_data->NOT_DONE) {
        xmask = XtAppPending(g_app_context);
        if(xmask)
            XtAppProcessEvent(g_app_context,xmask);
    }

    if(g_question_data->Status_Flag==QOK) {
        Bring_Back_QData( p,d,t,a,c);
        assert(g_question_shell);
        XtUnmanageChild(g_question_shell->m_question_shell);
        XtDestroyWidget(g_question_shell->m_question_shell);
    }
    //else printf(" g_question_data->Status_Flag= %d\n", g_question_data->Status_Flag);

    XmUpdateDisplay(g_graph[0].topLevel);

    XmUpdateDisplay(g->topLevel);

#endif


    return 1;

}
int QReplace_Word(char **line,int Index, char *w, char*seps){
    int i,c; size_t bl;
    char**words=NULL;
    char*buf ;
    bl = strlen(*line) ;
    c = make_into_words(*line,&words,seps);
    bl = bl + strlen(w) + (max(Index,c) +1)*(4)  + 10;
    //printf("Replace Word %d buflen is %d ....  ",Index,bl);

    buf = (char*) MALLOC(bl);
    *buf=0;


    //printf("before. %d words... ",c);
    for(i=0;i<c;i++) {
        if(i==Index) {
            //printf(" replace word '%s' ", words[i]);
            strcat(buf, w);
            //printf(" with '%s'\n", w);
        }
        else strcat(buf, words[i]);
        strcat(buf," :");
        if(strlen(buf) > (size_t) bl) g_World->OutputToClient("overflow",3);
    } //for i
    if( Index>=c) {
        for(i=c;i<Index;i++) {
            strcat(buf," :");
        }
        strcat(buf, w);
        strcat(buf, "  ");
    }
    buf[strlen(buf)]=0;
    RXFREE(*line);
    *line=STRDUP(buf);
    RXFREE(buf);
    RXFREE(words);
    return 1;
}

int Finish_Question (RXEntity_p e ) {
    /*		0	1	2	3	4	5	6
 format is	 question:prompt:	default :type [:	etype:	ename:	wordindex]: answer */
    RXEntity_p   t;

    char**words=NULL;
    int  i, c,Index;
    char answer[256]; *answer=0;
    char *eline=NULL;
    char *line = STRDUP(e->GetLine());
    c = make_into_words(line,&words,":\t\n");
#ifdef HOOPS
    HC_Parse_String(words[c-1],";",-1, answer);
#else
    cout<<"Finish Question, answwer ignored"<<endl;
#endif
    //printf(" range of answers <%s> took %s\n", words[c-1],answer);

    if(c==5 ) {
        char buf[512];
        sprintf(buf,"%s=%s",words[1],answer);
        printf("Setting Root User options %s\n",buf);
        HC_QSet_User_Options("/",buf);
    }
    else if (c==8) {
        t = e->Esail->GetKeyWithAlias(words[4],words[5]);
        if(!t) {
            char buf[256];
            sprintf(buf,"A Question failed. Couldnt find '%s' '%s'\n from\n %s", words[4],words[5],e->GetLine());
            e->OutputToClient(buf,2); return 1;
        }
        eline = STRDUP(t->GetLine());
        Index = atoi(words[6]);
        //printf(" New word in %s %s at %d (%s)\n", t->type,t->name,Index,words[c-1]);
        Replace_Word(&(t->line),Index, answer,":\t");
        printf(" New Line  %s\n", t->GetLine());
        if(!strieq(t->line,eline)) {
            t->CClear();  /* This is severe. If the edit involved numeric data only
    a Set Needs Computing is all thats necessary*/
            t->Needs_Resolving=1;
            t->Needs_Finishing=1; /* added as a guess May 1997 */
            t->SetNeedsComputing();
            //printf("\n UNRESOLVED %s   %s \n", t->type,t->name);



        }

        t->SetNeedsComputing();  /* hopefully sparks off a Compute on its nieces */
        if(eline) RXFREE(eline);
    }
    else  {
        cout<< " QUESTION odd words"<<endl;
        for(i=0;i<c;i++) cout<<i<<"  "<<words[i]<<endl;

    }

    RXFREE(line);
    if(words) RXFREE(words);

    return 1;
}

int Ask_All_Questions(Graphic *gin,int regardless) { /* appends the anwers to the lines
 Returns 1 IF a run is needed but more to the point, it sets Needs Finishing on the Question
It is only sensitive to questions that needs resolving (ie brand new ones) unless the regardless
flag is set, which it is meant to be when calling from a menu
*/

    char *line,*lp;const char*Firstword ;
    int i;
    char**a = NULL, **p=NULL, **d=NULL, **t=NULL, **b= NULL;
    RXEntity_p  *elist = NULL;
    char**words;
    int wc, c=0;
    int iret=0;
    size_t L1;
    Graphic *g=0;

    SAIL *sail =NULL;

    if(gin)
        g = gin;
    else
        assert(" Ask_All_Questions: g = (GetCurrentSail()->Graphic() "==0);

    // assert( g->GSail  == GetCurrentSail());
    if(!g) return 0;
    sail = g->GSail ;

    RXEntity_p e;
    vector<RXEntity_p >thelist;
    sail->MapExtractList(PCE_VELOCITY,  thelist) ;
    vector<RXEntity_p >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        e  = *it;

        Firstword = e->type();
        if (e->TYPE == PC_QUESTION) {
            if(regardless || e->Needs_Resolving) {
                e->Needs_Resolving=0;
                line = STRDUP(e->line);
                wc = make_into_words(line,&words,":\t");

                p = (char**) REALLOC(p,(c+1)*sizeof(void*)); /* prompt  */
                d = (char**) REALLOC(d,(c+1)*sizeof(void*)); /* default */
                t = (char**) REALLOC(t,(c+1)*sizeof(void*)); /* type eg NUMERIC */
                a = (char**) REALLOC(a,(c+1)*sizeof(void*)); /* answers, semicolon separated */
                b = (char**) REALLOC(b,(c+1)*sizeof(void*)); /* a record of the old data a */
                elist = (RXEntity_p   *) REALLOC(elist,(c+1)*sizeof(void*));
                elist[c] = e;
                p[c]=STRDUP(words[1]);
                d[c]=STRDUP(words[2]);
                t[c] = STRDUP(words[3]);

                /*  we concatenate the answers to the old ones to allow some kind of audit trail
 yet we only want to offer the last word hence this */
                lp = strrchr(words[wc-1],';');
                if(lp)
                    a[c]=STRDUP(++lp);
                else
                    a[c]=STRDUP(words[wc-1]);
                b[c] = STRDUP(a[c]); c++;
                RXFREE(line);
            }
        }
    }

    if(c) {

        iret = XQuestion(p,d,t,a,c,g);
    }

    for(i=0;i<c;i++) {
        if(!strieq(a[i],b[i])) {
            elist[i]->Needs_Finishing = 1;
            sail->SetFlag(FL_NEEDS_SAVING );
            iret++;
            //printf("a[i]'%s' isnt b[i]'%s' so iret=%d \n",a[i],b[i],iret);
            L1 = strlen(elist[i]->line);
            if( L1 < (256-13 -strlen(a[i]))) {
                elist[i]->line = (char*) REALLOC(elist[i]->line, strlen(elist[i]->line)+strlen(a[i])+13);
                strcat(elist[i]->line," ;"); strcat(elist[i]->line,a[i]);
                printf(" Line length was %u \n", (unsigned) L1);
            }
            else
                printf(" DONT append: atts too long <%s>\n", elist[i]->GetLine());
        }

        RXFREE(p[i]);
        RXFREE(d[i]);
        RXFREE(t[i]);
        RXFREE(a[i]);
        RXFREE(b[i]);
    }
    if(c) {
        RXFREE(p);
        RXFREE(d);
        RXFREE(t);
        RXFREE(a);
        RXFREE(b);
        RXFREE(elist);
    }

    return iret ;
}

int Finish_All_Questions(SAIL *sail, const int regardless) {/* to Finish a Q means to use the answer to substitute into a string 

this is controlled  by the Needs Finshing flag We ALWAYS */

    int iret;
    Graphic *g = sail->GetGraphic();

    if(g_NON_INTERACTIVE) { return 1;}
    g_UNRESOLVE_ALL=0;
    iret = Ask_All_Questions(g,regardless);  /* dont care return 0; */

    RXEntity_p e;
    vector<RXEntity_p >thelist;
    sail->MapExtractList(PC_QUESTION,  thelist) ;
    vector<RXEntity_p >::iterator it;
    for (it = thelist.begin (); it != thelist.end(); it++)  {
        e  = *it;

        if(e->Needs_Finishing)
            Finish_Question(e);
        e->Needs_Finishing=0;
    }

    if( g_UNRESOLVE_ALL) {
        g_UNRESOLVE_ALL=0;
    }
    return iret;
}
#ifndef NEVER_WINDOWS

#endif



int Env_Edit(Graphic *gin,const char *filter,const char *newin) { 
    char *lp, *buf;
    int i,c=0;
    char**a = NULL, **p=NULL, **d=NULL, **t=NULL, **b= NULL;
    FILE *fp;
    int iret=0;
    Graphic *g;
    char line[512];
#ifndef _X
    cout<<"Env EDIT needs X "<<newin<<endl; return 0;
#endif

    if(gin)
        g = gin;
    else
        g=0;
    if(!g)
        return 0;
    char *l_New = STRDUP(newin);

    //printf(" filter=<%s>, new =<%s>\n",filter,l_new);
    PC_Strip_Leading(l_New);
    system ("set > env.txt");
    fp = RXFOPEN("env.txt","r");

    p = (char**)REALLOC(p,(1)*sizeof(void*)); /* prompt  */
    d = (char**)REALLOC(d,(1)*sizeof(void*)); /* default */
    t = (char**)REALLOC(t,(1)*sizeof(void*)); /* type eg NUMERIC */
    a = (char**)REALLOC(a,(1)*sizeof(void*)); /* answers, semicolon separated */
    b = (char**)REALLOC(b,(1)*sizeof(void*)); /* a record of the old data a */

    p[c]=STRDUP(l_New);

    d[c]=STRDUP(" ");
    t[c] = STRDUP("TEXT");

    //  we should concatenate the answers to the old ones to allow some kind of audit trail

    a[c]=STRDUP(d[c]);
    b[c] = STRDUP(a[c]);


    for(c=1;;) {
        if(!fgets(line,511,fp)) break;
        if(stristr(line,filter)) {
            PC_Strip_Trailing(line);
            //printf("file line at input is <%s>\n",line);
            p = (char**)REALLOC(p,(c+1)*sizeof(void*)); /* prompt  */
            d = (char**)REALLOC(d,(c+1)*sizeof(void*)); /* default */
            t = (char**)REALLOC(t,(c+1)*sizeof(void*)); /* type eg NUMERIC */
            a = (char**)REALLOC(a,(c+1)*sizeof(void*)); /* answers, semicolon separated */
            b = (char**)REALLOC(b,(c+1)*sizeof(void*)); /* a record of the old data a */
            lp = strchr(line,'=');
            if(lp) *lp=0;
            p[c]=STRDUP(line);
            lp++;

            PC_Strip_Leading_Chars(lp, "\'\"");
            PC_Strip_Trailing_Chars(lp, "\'\"");
            PC_Strip_Leading(lp);
            PC_Strip_Trailing(lp);
            //		printf(" second token stripped <%s>\n",lp);
            d[c]=STRDUP(" ");
            t[c] = STRDUP("TEXT");

            //  we should concatenate the answers to the old ones to allow some kind of audit trail

            a[c]=STRDUP(lp);
            b[c] = STRDUP(a[c]);
            c++;
        }
    }
    FCLOSE(fp);
#ifdef linux
    system("rm -f env.txt");
#else
    system("del env.txt");
#endif
    if(c) {
        iret = XQuestion(p,d,t,a,c,g);
    }

    for(i=0;i<c;i++) {
        if(!strieq(a[i],b[i])) {
            printf("a[i]'%s' isnt b[i]'%s' so iret=%d \n",a[i],b[i],iret);
            buf = (char*) MALLOC(strlen(a[i]) + strlen(p[i]) + 32);
            sprintf(buf,"environment : %s :%s",p[i],a[i]);
            Append_Defaults_File(buf);
            sprintf(buf,"%s=%s",p[i],a[i]);
#ifdef linux
                putenv( strdup(buf));
#else
            _putenv(_strdup(buf));
#endif

            RXFREE(buf);
        }

        RXFREE(p[i]);
        RXFREE(d[i]);
        RXFREE(t[i]);
        RXFREE(a[i]);
        RXFREE(b[i]);
    }


    if(c) {
        RXFREE(p);
        RXFREE(d);
        RXFREE(t);
        RXFREE(a);
        RXFREE(b);
    }
    RXFREE(l_New);
    return iret ;
}

