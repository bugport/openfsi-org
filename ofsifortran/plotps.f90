! sept 2008 new element data structures.
! dec 2005  trim all character arguments to C functions. Hoops 13 
! June 2004 set Plot ref length
! October 2002  vectors_f module
!*     date  17/11/94   Set Current Model(Model) added
      
!*
module plotps_f
	use basicTypes_F
	use cfromf_f
	implicit none

contains
function set_Plot_ref_length( ) result (a)
	real(kind=double) :: a
!DEC$ IF (1 < 2) 

	a = 1.0

!DEC$ ELSE 
	call appwind_get_ref_length(a)
!DEC$ ENDIF 

end function set_Plot_ref_length
end module plotps_f

SUBROUTINE PLOT_ref_direction(sli,seg )  bind(C,name= "cf_plot_ref_direction" ) ! by 1% strain
        use math_f
        use coordinates_f
        use cfromf_f
        use saillist_f
        use hoopsinterface
        IMPLICIT NONE
        integer(c_int), intent(in),value ::sli
        integer(kind=cptrsize) ,  value ::seg

        REAL*8 A(3), DS
        REAL*8 global(3),Local(3),Thet
        REAL*8 TT(3,3)

        INTEGER  J, N

        REAL*4 x1(3),x2(3)
        real(kind=double) :: g_plot_scale
        character*12 :: color = 'red'
        type(sail),pointer  ::s
        TYPE ( trilist) ,POINTER :: tris
        type(tri), pointer :: t


 	s=>saillist(sli)      
 	tris=>s%tris      
    if(tris%count<1) RETURN
    
 	g_plot_scale= get_vector_plot_scale()
    DS = 0.30
        

      ! CALL Set_Current_Model(Model)  
    CALL hf_SET_COLOR(trim(color)//char(0))  

 do_N:  DO N=1,s%tris%count
        t=>s%tris%list(N);  if(.not. t%use_tri) cycle
        tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe )
        thet = -t%THETAE_rad
        a = 0.0D00
        DO  j=1,3
          A=get_Cartesian_X_by_ptr(t%nr(j)%m_fnpe) /3.0 + A ! centroid
        enddo

        local(1) = cos(thet); Local(2) = sin(thet); local(3) = 0.0d00

        global = matmul(tt,local)

        X1=A+DS*global
        X2=A-DS*global
        CALL akm_INSERT_LINE(x1(1),x1(2),x1(3),x2(1),x2(2),x2(3),seg)
       ENDDO do_N
END SUBROUTINE PLOT_ref_direction

SUBROUTINE PLOT_Principal_Stiff(sli,seg)  bind(C,name= "cf_plot_principal_stiff" ) ! by 1% strain
        USE tris_f
        use nonlin_f
        use math_f
        use coordinates_f
        use plotps_F
        use cfromf_f
        use saillist_f
        use hoopsinterface
        IMPLICIT NONE
        integer(c_int), intent(in),value ::sli
        integer(kind=cptrsize) ,  value ::seg

        REAL*8 A(3) ,SC,AVERAGE,DS,SCMAX
        REAL*8 global(3),Local(3),Thet
        REAL*8 TT(3,3)

      INTEGER I, J, N ,state
      REAL*8 :: stress(3),eps(3) ,princ(3), crease
      data eps /1.0,1.0,0.0/
	REAL*4 x1,y1,z1	,x2,y2,z2


      character*12 color(2)
      character*12 name(2)
      data color/'black','gray'/
      data name /'black','gray'/
	real(kind=double) :: g_plot_scale
    type(sail),pointer  ::s 
    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer :: t 

 	s=>saillist(sli)      
 	tris=>s%tris      
    if(tris%count<1) RETURN	
	
  call FlagSetOld()
 	g_plot_scale= get_vector_plot_scale()
      AVERAGE=0.0

 DO N=1,s%tris%count
             t=>s%tris%list(N);  if(.not. t%use_tri) cycle
            call Get_NL_Stress_From_Tri(t,eps,stress,crease,state)
            CALL Stress_To_Princ(stress,princ)
            AVERAGE=AVERAGE+PRINC(1)
      ENDDO
      
      AVERAGE=AVERAGE/FLOAT(s%tris%count )
      SC=0.005/AVERAGE*(g_plot_scale)
      SCMAX=0.2*(g_plot_scale) 
  !         if(use_prestress .eq. 1) then
    !		write(outputunit,*) ' not accounting for prestress in PLOT_Principal_Stiff'
 !           endif

     
 DO N=1,s%tris%count
            t=>s%tris%list(N);  if(.not. t%use_tri) cycle
            tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe )           
            
            call  Get_NL_Stress_From_Tri(t,eps,stress,crease, state)
            CALL Stress_To_Princ(stress,princ)
            
            thet = -t%THETAE_rad + PRINC(3) 
            
     	    a = 0.0D00
               DO  j=1,3
                  A =get_Cartesian_X_by_ptr(t%nr(j)%m_fnpe) /3.0 + A ! centroid
               enddo
            
            DO I=1,2
               CALL hf_OPEN_SEGMENT(TRIM(name(i))//char(0))
               CALL hf_SET_COLOR(trim(color(i))//char(0))
               
               DS=MIN (SC*PRINC(I), scmax)
               DS=MAX(-SCMAX,DS)
               THET=THET+FLOAT(I-1)*PI/2.0
               
               local(1) = cos(thet)
               Local(2) = sin(thet)
               local(3) = 0.0d00
    
               global = matmul(tt,local)
               X1=A(1)+DS*global(1)
               Y1=A(2)+DS*global(2)
               Z1=A(3)+DS*global(3)
               
               X2=A(1)-DS*global(1)
               Y2=A(2)-DS*global(2)
               Z2=A(3)-DS*global(3)
               
               CALL akm_INSERT_LINE(x1,y1,z1,x2,y2,z2,seg)
               CALL hf_CLOSE_SEGMENT()
            ENDDO
      ENDDO
END SUBROUTINE PLOT_Principal_Stiff

SUBROUTINE PLOT_One_Tri_Line(t,angle,seg)
        use tris_f
        use math_f
        use coordinates_f
        use cfromf_f
        use saillist_f
        use hoopsinterface
        IMPLICIT NONE
        type(tri),pointer, intent(in) ::t
        real(kind=double), intent(in) :: angle
        integer(kind=cptrsize)   ::seg
        REAL*8 A(3)
        REAL*8 global(3),Local(3),Thet
        REAL*8 TT(3,3), ds
        INTEGER  J
        REAL*4 x1,y1,z1,x2,y2,z2
        real(kind=double) :: g_plot_scale

        g_plot_scale= get_vector_plot_scale()
    tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe )
       thet = -t%THETAE_rad + angle

		a = 0.0D00
               DO  j=1,3
                  A =get_Cartesian_X_by_ptr(t%nr(j)%m_fnpe) /3.0 + A ! centroid
               enddo
 
 		DS=0.25*sqrt(T%m_Area)
         	
          	local(1) = cos(thet)
      		Local(2) = sin(thet)
      		local(3) = 0.0d00
      		!CALL MULTV(global,TT,local)
      	      global = matmul(tt,local)
      		X1=A(1)+DS*global(1)
      	     	Y1=A(2)+DS*global(2)
		Z1=A(3)+DS*global(3)

	        X2=A(1)-DS*global(1)
      	     	Y2=A(2)-DS*global(2)
		Z2=A(3)-DS*global(3)

                CALL akm_INSERT_LINE(x1,y1,z1,x2,y2,z2,seg)
END SUBROUTINE PLOT_One_Tri_Line
SUBROUTINE PLOTPS(sli,seg) bind(C,name=  "cf_plotps" )
    USE, INTRINSIC :: ISO_C_BINDING
    use tris_f
    use math_f
    use coordinates_f
 !   use plotps_F
    use cfromf_f
    use saillist_f
    use basictypes_f
    use hoopsinterface
    IMPLICIT NONE
    integer(c_int), intent(in),value ::sli
    integer(kind=cptrsize) ,  value ::seg
    REAL*8 A(3) ,SC,AVERAGE,DS,SCMAX
    REAL*8 global(3),Local(3),Thet
    REAL*8 TT(3,3)

    INTEGER I, J,  N , color_Index
    REAL*8 crease,PST
    REAL(kind=double), dimension(3) :: stress,eps,princ
    REAL(c_float) :: x1,y1,z1,x2,y2,z2
    real(c_double) :: hue
    character*12 name(2)
    character*2 colors(12)


    data name /'upr','lwr'/
    data colors /'aa','bb','cc','dd','ee','ff','gg','hh','ii','jj','kk','ll'/
    real(kind=double) :: g_plot_scale
    type(sail),pointer  ::s
    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer :: t

    s=>saillist(sli)
    tris=>s%tris
    call FlagSetOld()
    
    g_plot_scale= get_vector_plot_scale()
!H=225.0 I=0.575 C=1.000
!H=195.0 I=0.575 C=1.000
!H=165.0 I=0.575 C=1.000
!H=145.0 I=0.575 C=1.000
!H= 95.0 I=0.575 C=1.000
!H= 75.0 I=0.575 C=1.000
!H= 55.0 I=0.575 C=1.000
!H= 35.0 I=0.575 C=1.000
!H= 15.0 I=0.575 C=1.000
!H=355.0 I=0.575 C=1.000
!H=335.0 I=0.575 C=1.000
!H=305.0 I=0.575 C=1.000

     if(tris%count<1) RETURN
      AVERAGE=0.0
      
DO N=1,s%tris%count
             t=>s%tris%list(N);  if(.not. t%use_tri) cycle
            j= One_Tri_Stress_OP(N,sli,stress,eps,princ,crease,PST)
            AVERAGE=AVERAGE+PRINC(1)
      ENDDO
      
      AVERAGE=AVERAGE/FLOAT(s%tris%count )
	if(average .le. 0.000001) average = 1.0
      SC=0.01/AVERAGE*(g_plot_scale)
      SCMAX=0.12*(g_plot_scale)


don:	 DO N=1,s%tris%count
             t=>s%tris%list(N);  if(.not. t%use_tri) cycle
      	    j= One_Tri_Stress_OP(N,sli,stress,eps,princ,crease,PST)
   tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe )
      	thet = -t%THETAE_rad + PRINC(3)

		a = 0.0D00
		DO  j=1,3
                  a=get_Cartesian_X_by_ptr(t%nr(j)%m_fnpe)/3.0 + A ! centroid
		enddo

doi:		DO I=1,2
        	color_Index = int( princ(I)/(average) *6.0) +1
		color_Index = min(12, color_Index)
		color_Index = max(1, color_Index)
		hue = 225 - 280./12*color_Index
		if(hue .lt.0) hue = hue +360.0
	
		DS=MIN (SC*PRINC(I), scmax)
      	DS=MAX(-SCMAX,DS)
      	THET=THET+FLOAT(I-1)*PI/2.0

      	local(1) = cos(thet)
      	Local(2) = sin(thet)
      	local(3) = 0.0d00
      	global = matmul(tt,local)
		X1=A(1)+DS*global(1)
		Y1=A(2)+DS*global(2)
		Z1=A(3)+DS*global(3)

		X2=A(1)-DS*global(1)
		Y2=A(2)-DS*global(2)
		Z2=A(3)-DS*global(3)

		if( color_Index .lt. 1 .or. color_Index .gt. 12) Then
			write(outputunit,*)' color index out of range'
			color_index=5
		endif
		CALL hf_Open_Segment(trim(colors(color_Index))//char(0))
			CALL hf_OPEN_SEGMENT(trim(name(i))//char(0))
                                CALL akm_INSERT_LINE(x1,y1,z1,x2,y2,z2,seg)
		  		CALL HF_Set_Color_By_Value('lines'//char(0),'HIC'//char(0),hue,0.5,1.000)
			CALL hf_Close_Segment() ! name(i) 
		CALL hf_CLOSE_SEGMENT() ! colorIndex
	ENDDO doi
      ENDDO don
END SUBROUTINE PLOTPS
SUBROUTINE drawVelocity (sli)  
 	use math_f
 	use coordinates_f
 	use vectors_F
	use cfromf_f
 	use saillist_f
 	use hoopsinterface
       IMPLICIT NONE 
        integer(c_int), intent(in) ::sli
       INTEGER    N  

     REAL(kind=double), dimension(3) :: a,vv
     REAL(c_float) ,dimension(3) :: x1,X2
     real(c_double) :: lv
     real(kind=double) :: g_plot_scale 
     logical err 

 	type(sail),pointer  ::s 
     TYPE ( nodelist) ,POINTER :: nodes
    type(fnode), pointer :: nn 
    character(len=64) ::buf
    integer ::c
    integer(kind=cptrsize) ::seg
    seg=0
 	s=>saillist(sli)      
 	nodes=>s%nodes      
     call FlagSetOld()
    
 	g_plot_scale= get_vector_plot_scale()
    write(buf,'(a,i3.3)') "VVecs",sli
    c=0
		CALL hf_Open_Segment(buf//char(0))
		call  hf_flush_contents("."//char(0),"everything"//char(0)) 
        call  hf_set_line_weight( 3.0d00)
        call hf_set_color("magenta"//char(0))
don:	 DO N=1,s%nodes%ncount
                nn=>s%nodes%xlist(N);  if(.not. nn%m_Nused) cycle
                a=get_Cartesian_X_by_ptr(nn) 
                vv =   get_Cartesian_V_By_Ptr(nn);
                call normalise(vv,lv,err); 
                if(err) cycle
                if(lv< 0.00000001) cycle;
                x1 = a
                
                x2 = a + vv * sqrt(lv) * g_plot_scale  ! sqrt makes the plot more readable
                CALL akm_INSERT_LINE(x1(1),x1(2),x1(3),x2(1),x2(2),x2(3),seg)
                c=c+1
      ENDDO don
		CALL hf_CLOSE_SEGMENT()  
		if(c > 0) call HF_UPDATE_DISPLAY ()
END SUBROUTINE drawVelocity
       
SUBROUTINE PLOTPSTRAIN(sli,seg) bind(C,name="cf_plotpstrain" )
        use tris_f
        use math_f
        use coordinates_f
        use plotps_F
        use cfromf_f
        use saillist_f
        use hoopsinterface
        IMPLICIT NONE
        integer(c_int), intent(in),value ::sli
        integer(kind=cptrsize) ,  value ::seg
        REAL*8 A(3) ,SC,AVERAGE,DS,SCMAX
        REAL*8 global(3),Local(3),Thet
        REAL*8 TT(3,3)

        INTEGER I, J,  N
        REAL*8 stress(3),eps(3),princ(3),crease,PST
        REAL*4 x1,y1,z1,x2,y2,z2
        character*14 color(2)
        character*12 name(2)
        data color/'orchid','blue'/
        data name/'plus','minus'/
        real(kind=double) :: g_plot_scale
        type(sail),pointer  ::s
        TYPE ( trilist) ,POINTER :: tris
        type(tri), pointer :: t

 	s=>saillist(sli)      
 	tris=>s%tris      
    if(tris%count<1) RETURN
 	g_plot_scale= get_vector_plot_scale()
     AVERAGE=0.0
     call FlagSetOld()
DO N=1,s%tris%count
             t=>s%tris%list(N);  if(.not. t%use_tri) cycle
            j=One_Tri_Stress_OP(N,sli,stress,eps,princ,crease,PST)
             eps(3) = eps(3) / 2.0D00
             CALL Stress_To_Princ(eps,princ)
            AVERAGE=AVERAGE+PRINC(1)
      ENDDO
      
      AVERAGE=AVERAGE/FLOAT(s%tris%count )
	if(average .lt. 0.00001) average = 1.0
      SC=0.01/AVERAGE*(g_plot_scale)
      SCMAX=0.12*(g_plot_scale)
       	DO I=1,2
		CALL hf_OPEN_SEGMENT(trim(name(i))//char(0))
		CALL hf_SET_COLOR(trim(color(i))//char(0))
		CALL hf_CLOSE_SEGMENT()
	enddo

 DO N=1,s%tris%count
             t=>s%tris%list(N);  if(.not. t%use_tri) cycle
      	    j= One_Tri_Stress_OP(N,sli,stress,eps,princ,crease,PST)
              eps(3) = eps(3) / 2.0D00
             CALL Stress_To_Princ(eps,princ)

         tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe )
        thet = -t%THETAE_rad + PRINC(3)

      	a = 0.0D00
         DO  j=1,3
                  A =get_Cartesian_X_by_ptr(t%nr(j)%m_fnpe) /3.0 + A ! centroid
         enddo
      	     
       	DO I=1,2
!  say orchid is +,  blue is -ve. 	
	
        if(princ(I) > 0.0) then
                CALL hf_OPEN_SEGMENT(trim(name(1))//char(0))
        else
                CALL hf_OPEN_SEGMENT(trim(name(2))//char(0))
        endif
        DS= SC*PRINC(I)		
        THET=THET+FLOAT(I-1)*PI/2.0
        local(1) = cos(thet)
        Local(2) = sin(thet)
        local(3) = 0.0d00

        global = matmul(tt,local)
        X1=A(1)+DS*global(1)
        Y1=A(2)+DS*global(2)
        Z1=A(3)+DS*global(3)

        X2=A(1)-DS*global(1)
        Y2=A(2)-DS*global(2)
        Z2=A(3)-DS*global(3)

                CALL akm_INSERT_LINE(x1,y1,z1,x2,y2,z2,seg)
		CALL hf_CLOSE_SEGMENT()
	   ENDDO
      ENDDO
END  SUBROUTINE PLOTPSTRAIN


