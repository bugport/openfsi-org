MODULE fbeamList_f
USE ftypes_f
USE  linklist_f
USE  saillist_f
IMPLICIT NONE

CONTAINS  

 integer function HookAllBeams()
 use nodelist_f
IMPLICIT NONE

INTEGER I,sli,n,istat
Type(sail) ,pointer::s
TYPE ( fbeamlist) ,pointer   	:: fbeams
type(beamelement) , pointer :: e
        type(fnode),pointer ::thenode
HookAllBeams=0

do sli=1,SailCount
if( .not. is_used(sli)) cycle
    s=>saillist(sli)

        fbeams=>saillist(sli)%fbeams
  do i = 1, fbeams%count
      e=>fbeams%list(i)
      if (.not. e%m_used)  cycle
      n = e%n12(1);
      if(n>0) then
             if(n>ubound(s%nodes%xlist ,1)) write(*,*) ' (6) thats why it crashes on exit'
      e%xn1=>s%nodes%xlist(n)
      endif
      n = e%n12(2);
              if(n>ubound(s%nodes%xlist ,1)) write(*,*) ' (7) thats why it crashes on exit'
      if(n>0)then
      e%xn2=>s%nodes%xlist(n)
      endif

      thenode=> e%xn1	; thenode%m_ElementCount =thenode%m_ElementCount +1
      istat=IsSixNode(thenode); if(istat==0) write(outputunit,*)' NOT a six-node!!!!!!'

      thenode=>  e%xn2;	thenode%m_ElementCount =thenode%m_ElementCount +1
      istat=IsSixNode(thenode);  if(istat==0) write(outputunit,*)' NOT a six-node!!!!!!'

          fbeams%list(i)%tn1=> saillist(sli)%nodes%xlist( fbeams%list(i)%xn1%sixnoderef  )
          fbeams%list(i)%tn2 =>saillist(sli)%nodes%xlist( fbeams%list(i)%xn2%sixnoderef  )


! type(fnode), pointer  :: xN1=>NULL() ,xN2=>NULL(),tn1=>NULL(),tn2 =>NULL()

  enddo
ENDDO
HookAllBeams=1

end function HookAllBeams



function CreateFortranfbeam (sli, NN ,p_n1,p_n2,p_zi,p_ti, p_mass,emp, axLoadFlag,beta,name,err) result(rc) ! should place it at N
USE realloc_f
use nodelist_f
USE, INTRINSIC :: ISO_C_BINDING
IMPLICIT NONE
	INTEGER , intent(in) :: sli,nn,p_n1,p_n2
	real(c_double), intent(in) ::beta,p_zi,p_ti,p_mass
	real(c_double), intent(in), dimension(5) ::emp 
        integer(C_INT), intent(in),value :: axLoadFlag
	character(len=*) ::name	
	INTEGER , intent(out) :: err
	integer(C_int) :: rc

	TYPE ( fbeamlist) ,POINTER :: fbeams
	INTEGER :: istat
        type(fnode),pointer ::thenode
	rc=-1
	err = 16
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	fbeams=>saillist(sli)%fbeams
	err=8
	if(NN < 1) then
	write(outputunit,*) 'cant create a beam with zero index'
	return
	endif
	err = 0
  
!	write(outputunit,*) ' CreateFortranfbeam ',sn, NN,err
!
! Normal conditions
!	just starting : 
!	NN <= count. >> set X,Y,Z used
!	NN > ubound >> realloc then set X,Y,Z used  if alloc OK. set count
!	NN > count but < Ubound  set count.set  X,Y,Z used

! error conditions
!   unable to alloc
!   NN < 0
!
       ! fbeams%fblblock=1024
	if(.not. ASSOCIATED(fbeams%list)) THEN

                ALLOCATE(fbeams%list(fbeams%fblblock), stat=istat)
		if(istat /=0) THEN
			err = 1
			return
		endif
		fbeams%count=0
	ENDIF
	DO
		if(NN >= UBOUND(fbeams%list,1)) THEN
                        CALL reallocate(fbeams%list, ubound(fbeams%list,1) + fbeams%fblblock, err)
			if(err > 0) return
		ELSE
			exit
		ENDIF
	ENDDO
		if(NN > fbeams%count) fbeams%count= NN
		!logical :: 
		fbeams%list(NN)%IsUpright=.false.;
		fbeams%list(NN)%IsBar=.false.
		fbeams%list(NN)%m_Currently_Active=.true.
		fbeams%list(NN)%m_Used= .true. 
		!integer :: 
		fbeams%list(NN)%L=nn 
		fbeams%list(NN)%n3=0
		fbeams%list(NN)%npo=0
		fbeams%list(NN)%N= nn
		!integer, dimension(2) :: 
		fbeams%list(NN)%n12(1) = p_n1
		fbeams%list(NN)%n12(2) = p_n2
		!REAL  (kind=double) ::
		fbeams%list(NN)%zt=p_zi
		 fbeams%list(NN)%zi=p_zi
		 fbeams%list(NN)% tq=0
		 fbeams%list(NN)%ti=p_ti
		 fbeams%list(NN)% beta1 =beta
		!REAL  (kind=double) ,dimension(3,3) :: 
		fbeams%list(NN)%ttu=0
		!REAL  (kind=double) ,dimension(5) :: 
                fbeams%list(NN)%emp= emp
                fbeams%list(NN)%axload = axloadflag
		!real(kind=double), pointer, dimension(:) :: 


		!character*64 
		fbeams%list(NN)%m_part_name	=Name
                fbeams%list(NN)%m_BEMass= p_mass
	
            fbeams%list(NN)%xn1=>saillist(sli)%nodes%xlist(p_n1)
	    fbeams%list(NN)%xn2=>saillist(sli)%nodes%xlist(p_n2)
	    	
	    thenode=> saillist(sli)%nodes%xlist(p_n1)	; thenode%m_ElementCount =thenode%m_ElementCount +1
	    istat=IsSixNode(thenode); if(istat==0) write(outputunit,*)' NOT a six-node!!!!!!'
	    thenode=> saillist(sli)%nodes%xlist(p_n2);	thenode%m_ElementCount =thenode%m_ElementCount +1
	    istat=IsSixNode(thenode);  if(istat==0) write(outputunit,*)' NOT a six-node!!!!!!'
	    
		fbeams%list(NN)%tn1=> saillist(sli)%nodes%xlist( fbeams%list(NN)%xn1%sixnoderef  )
		fbeams%list(NN)%tn2 =>saillist(sli)%nodes%xlist( fbeams%list(NN)%xn2%sixnoderef  )  		
        rc=nn
END function CreateFortranfbeam 


function ChangeFortranfbeam (sli, NN ,p_zi,p_ti, p_mass,emp,axloadFlag,beta,name,err) result(rc) !  place it at N
! set all new properties but dont change topology or TTU
USE realloc_f
use nodelist_f
USE, INTRINSIC :: ISO_C_BINDING
IMPLICIT NONE
        INTEGER , intent(in) :: sli,nn
        real(c_double), intent(in) ::beta,p_zi,p_ti,p_mass
        real(c_double), intent(in), dimension(5) ::emp
         integer(C_INT), intent(in),value :: axLoadFlag
        character(len=*) ::name
        INTEGER , intent(out) :: err
        integer(C_int) :: rc
        TYPE ( fbeamlist) ,POINTER :: fbeams

        rc=-1
        err = 16
        if(.not. associated(saillist)) return
        if(sli > ubound(saillist,1)) return
        if(sli < lbound(saillist,1)) return
        fbeams=>saillist(sli)%fbeams
        err=8
        if(NN < 1) then
        write(outputunit,*) 'cant create a beam with zero index'
        return
        endif
        err = 4

        if(NN > fbeams%count) return
        !logical ::
        fbeams%list(NN)%m_Currently_Active=.true.
        fbeams%list(NN)%m_Used= .true.
        !integer ::
        fbeams%list(NN)%L=nn  ! whats this??
        fbeams%list(NN)%n3=0
        fbeams%list(NN)%npo=0
        fbeams%list(NN)%N= nn

        !REAL  (kind=double) ::
        fbeams%list(NN)%zt=p_zi
        fbeams%list(NN)%zi=p_zi
        fbeams%list(NN)% tq=0
        fbeams%list(NN)%ti=p_ti
        fbeams%list(NN)% beta1 =beta

        fbeams%list(NN)%emp= emp
        fbeams%list(NN)%axload= axloadflag
        fbeams%list(NN)%m_part_name	=Name
        fbeams%list(NN)%m_BEMass= p_mass

        err=0

        rc=nn
END function ChangeFortranfbeam

FUNCTION   NextFreeBeam(sn,err) result(n)
	USE realloc_f
	USE saillist_f
	use ftypes_f

	IMPLICIT NONE
	integer, intent(in)   :: sn
	INTEGER,INTENT (out)  ::  err
	integer :: i,k, istat,n
	TYPE ( fbeamlist) ,POINTER :: sList
	type (beamelement ), pointer:: this =>NULL()
  
	err = 0
	slist=>saillist(sn)%fbeams
	if(.not. ASSOCIATED(sList%list)) THEN
                !sList%Block= 16 ! BEAMELEMENTLISTBLOCKSIZE
                call reALLOCATE(sList%list, sList%fblblock, istat)

		if(istat /=0) THEN
			err = 1
			return
		endif

		sList%list%m_used = .FALSE.

		this =>sList%list(1)
		sList%count =1
		this%N=1
		this%m_used = .TRUE.
		n=1
		return
	ENDIF
	do i=1,ubound(sList%list,1)
		if(.not. sList%list(i)%m_used) THEN
				this=> sList%list(i); this%N=i; n=i
				sList%list(i)%m_used = .true.
				if(i > sList%count) then
					sList%count= i
				endif
				return
		endif		
	enddo
	! write(outputunit,*) ' we get here if no spare spaces in battenlist'
	i =  ubound(sList%list,1) 
!	sList%block = max(i,2);

        CALL reallocate(sList%list, ubound(sList%list,1) + max(i,2) ,err) ! what on earth?????????


	if(err /=0) then
		write(outputunit,*) ' reallocation error in BattenList'
	endif	

	do k= i+1,ubound(sList%list,1) 
		sList%list(k )%m_used = .FALSE.
	enddo
	this=> sList%list(i+1)
	this%m_used=.true. ; this%N=i+1

	sList%count=i+1
	n=this%n

END  FUNCTION NextFreeBeam
SUBROUTINE RemoveFortranbeam (sn, NN,err) 
USE realloc_f
use cfromf_f  ! for zeroptr
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( fbeamlist) ,POINTER :: fbeams
	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	fbeams=>saillist(sn)%fbeams
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, reallocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   fbeamlist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!
	err=8
	if(.not. ASSOCIATED(fbeams%list)) return
	if(NN > UBOUND(fbeams%list,1)) return
	if(NN  <LBOUND(fbeams%list,1)) return
	err=0
	
		fbeams%list(NN)%N=NN
		fbeams%list(NN)%m_used= .FALSE.
	
	if(NN == fbeams%count) THEN
		 fbeams%count =fbeams%count-1
                if( fbeams%count+fbeams%fblblock < ubound(fbeams%list,1) ) THEN
                        CALL reallocate(fbeams%list, ubound(fbeams%list,1) - fbeams%fblblock, err)
		ENDIF
	ENDIF

END SUBROUTINE RemoveFortranbeam



SUBROUTINE SetfbeamUsed   (sn, NN,flag,err)
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn,flag
	INTEGER , intent(out) :: err
	TYPE ( fbeamlist) ,POINTER :: fbeams
!
! sets the Used flag depending on the value of 'flag'
!  the usual error returns. 
!  the only twist is that if this is the last beamelement, we decrement count if flag=0
!  similarly, we increment count if NN is big. But only up to 
!
!	write(outputunit,*) 'SetfbeamUsed   ', sn, NN,flag
	err=16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	fbeams=>saillist(sn)%fbeams
	if(.not. ASSOCIATED(fbeams%list)) return
	if(NN > UBOUND(fbeams%list,1)) return
	if(NN  <LBOUND(fbeams%list,1)) return
	err=0
	if(flag/=0) THEN
		if(fbeams%list(NN)%m_used) err=2
		fbeams%list(NN)%m_used=.TRUE.
		if(NN > fbeams%count) fbeams%count=NN 
	else
		if(.NOT. fbeams%list(NN)%m_used) err=1
		fbeams%list(NN)%m_used=.FALSE.
		if(NN ==  fbeams%count) fbeams%count=fbeams%count-1 
	endif

END SUBROUTINE SetfbeamUsed 


function AssembleBeamElts(sli,m)result (rc)
    use saillist_f
    use stiffness_f
    use tanglobals_f
    use tanpure_f
    IMPLICIT NONE

! parameters
    integer, intent(in) :: sli
    integer(kind=cptrsize)  :: m
    integer rc
  
  ! returns
 ! nothing, but increments the global stiffness matrix (m)
 ! if p_comp is false, we add the direct stiffness but skip the geometric.

!* locals
    INTEGER N ,j
    real(kind=double),dimension(12,12) :: gl
    type(sail), pointer  ::sl
    type (beamelement) , pointer	:: be
    integer,  dimension(4):: nn
    logical :: l_some_negs
    if(.not. is_active(sli)) return
    sl=>saillist(sli)
    rc=0

!$OMP PARALLEL   PRIVATE(N,be,gl,l_some_negs ,nn,j ) shared(m, sl,g_dxdr,geomatrix,g_Bowing,g_BowShear) reduction (+:rc) default(none)
!$OMP   DO SCHEDULE(auto)
   do_n : DO N=1,sl%fbeams%count
      	be =>sl%fbeams%list(n) 
        if(.not. be%m_used) cycle
        gl = one_beam_global_stiffness(be,g_dxdr,geomatrix,g_Bowing,g_BowShear) !  direct stiffness plus geometric if required
        l_some_negs = .false.
        do j=1,12
                if(gl(j,j) < 0) l_some_negs=.true.
        enddo
        if(l_some_negs) then
                write(outputunit,*) trim(be%m_part_name)//" needs subdividing"
                rc=rc+1
        endif
        nn(1) = be%xn1%nn;  nn(2) = be%tn1%nn;  nn(3) = be%xn2%nn;  nn(4) = be%tn2%nn;
!$OMP CRITICAL(addstf)
        call addStiffness(sl,nn,gl,m)
!$OMP END CRITICAL(addstf)
      ENDDO do_n  
!$OMP END DO  NOWAIT
!$OMP END PARALLEL
end function AssembleBeamElts

function  MakeAllBeamMasses(sli) result (rc)
    use saillist_f
    use tanglobals_f
    use tanpure_f
    use rx_control_flags_f
    IMPLICIT NONE
! parameters
    integer, intent(in) :: sli
     integer rc
  
  ! returns
 ! nothing, but increments the nodal masses
 ! if p_comp is false, we add the direct stiffness but skip the geometric.

!* locals
    INTEGER N ,j
	real(kind=double),dimension(12,12) :: gl 
    type(sail), pointer  ::sl 
	type (beamelement) , pointer		:: be
    logical :: l_some_negs 
    if(.not. is_active(sli)) return
	sl=>saillist(sli)
    rc=0

    do_n : DO N=1,sl%fbeams%count
      	be =>sl%fbeams%list(n) 
        if(.not. be%m_used) cycle
 		gl = one_beam_global_stiffness(be,g_dxdr,geomatrix,g_Bowing,g_BowShear) !  direct stiffness plus geometric if required
		l_some_negs = .false. 
		do j=1,12
			if(gl(j,j) < 0) l_some_negs=.true.
		enddo
		if(l_some_negs) then
				write(outputunit,*) trim(be%m_part_name)//" needs subdividing"
				rc=rc+1
		endif

                 be%xn1%XM(1:3,1:3) = be%xn1%XM(1:3,1:3) + gl(1:3,1:3)
                 be%xn2%XM(1:3,1:3) = be%xn2%XM(1:3,1:3) + gl(7:9,7:9)
                 be%tn1%XM(1:3,1:3) = be%tn1%XM(1:3,1:3) + gl(4:6,4:6)
                 be%tn2%XM(1:3,1:3) = be%tn2%XM(1:3,1:3) + gl(10:12,10:12)
      ENDDO do_n   
 END function MakeAllBeamMasses
 ! start from links_pure 

SUBROUTINE Beam_Residuals(sli)
      use coordinates_f
      use tanglobals_f
      use  tanpure_f
      IMPLICIT NONE
	  integer, intent(in) ::sli
 
! returns
! nothing, but increments nodal residuals

! locals
    INTEGER N 
    type(sail), pointer  ::sl
    real(kind=double) ::fg(12)  !global_force
	type(beamelement) , pointer :: be
    if(.not. is_active(sli)) return
	sl=>saillist(sli)

    if( g_PleaseHookElements  ) write(outputunit,*) 'hook crash 17'	
!$OMP PARALLEL DO PRIVATE(N,fg,be  ) shared(saillist,sl,sli,G_Bowing,G_Bowshear) default(none)
    do_n : DO N=1,sl%fbeams%count
        be=>sl%fbeams%list(n)
        if(.not. sl%fbeams%list(n)%m_used) cycle
        fg =- one_beam_element_force(be,G_Bowing,G_Bowshear)

        call increment_r_by_ptr(be%xn1,fg(1:3))
        call increment_r_by_ptr(be%tn1,fg(4:6))
        call increment_r_by_ptr(be%xn2,fg(7:9))
        call increment_r_by_ptr(be%tn2,fg(10:12))
       ENDDO do_n
!$OMP END PARALLEL DO
 END  SUBROUTINE Beam_Residuals
    

 function  Drawfbeams (sli,seg) result(rc)
 !   use hoopsinterface
    use coordinates_f
    use cfromf_f
    implicit none
    integer, intent(in) ::sli
    integer(kind=cptrsize) , value :: seg
    TYPE ( fbeamlist) ,POINTER :: fbeams
    INTEGER :: n,rc
    TYPE ( beamelement) ,POINTER :: f
    real(c_FLOAT), dimension(3) ::a,b
    type(fnode), pointer ::e1, e2
    if(.not. associated(saillist)) return
    if(sli > ubound(saillist,1)) return
    if(sli < lbound(saillist,1)) return
    fbeams=>saillist(sli)%fbeams
    rc=0;
! now draw
    do n=1,fbeams%count
	f=>fbeams%list(n)
        if(.not. f%m_used)  cycle
        e1 => saillist(sli)%nodes%xlist(f%n12(1) ); e2 => saillist(sli)%nodes%xlist(f%n12(2) );
        A=get_Cartesian_X_by_ptr(e1)
        B=get_Cartesian_X_by_ptr(e2)
        CALL akm_INSERT_LINE(a(1),a(2),a(3),b(1),b(2),b(3),seg)
        rc=rc+1
    enddo
 end  function  Drawfbeams
 
subroutine PrintBeams(sli,u) 
use tanpure_f
use tanglobals_f

   implicit none
    integer ,intent(in) ::sli, u   
	TYPE ( fbeamlist) ,POINTER :: fbeams  
    type(beamelement),pointer :: be
    real(kind=double), dimension(10) :: force
    integer::n,L,j,k,istat
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	fbeams=>saillist(sli)%fbeams
	
	write(u,'(1h /a)' )'linear element properties'
	write(u,'(2x,a4,6(1h	a10,2x))') 'type ','        ea   ','      eiyy  ','      eizz  ','      gj   ','    beta(rad)','(kns&m)  upr'
	do  n=1,fbeams%count
			be=>fbeams%list(N)
			if(.not. be%m_used) cycle
                        write(u,'(1x,i6,2x,5(1h	,g12.5,1x),1h	L4,1h	g13.4 )',iostat=istat) n,(be%emp(k),k=1,4),be%beta1,be%IsUpright, be%m_BEMass
	enddo

       write(u,'(1h /a)')'beam element forces'
! now output forces and moments
        write(u,'(1h /4(1x,a5,1h	),7(2x,a8,1x,1h	))') 'no','end1','end2','type','zi',&
        &'m13','m23','m12','m22','torque','tension'
	do  L=1,fbeams%count
			be=>fbeams%list(L)
			if(.not. be%m_used) cycle
                !	if (mod(l,20) .eq. 1) then
                !                write(u,'(1h /4(1x,a5,1h	),7(2x,a8,1x,1h	))') 'no','end1','end2','type','zi',&
                !		&'m13','m23','m12','m22','torque','tension'
                !	endif
		
		force= one_local_element_force(be,be%zi,G_Bowing,G_BowShear)

                write(u,'(4(1x,i5,1h	),7(1x,g13.6,1h	),a)',iostat=istat) l,be%n12,be%npo,be%zi,&
		& (force(j),j=1,6),trim(be%m_part_name)
       
	end do
end subroutine PrintBeams

SUBROUTINE  ShowfbeamCount(sn, u,l,c, err) 
USE ftypes_f
 
IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err,u,l,c

	TYPE ( fbeamlist) ,POINTER :: fbeams

	err = 16
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	err=8
	fbeams=>saillist(sn)%fbeams
	if(.not. ASSOCIATED(fbeams%list)) return
	u =UBOUND(fbeams%list,1)
	L  =LBOUND(fbeams%list,1)
	c = fbeams%count
	err=0
END SUBROUTINE  ShowfbeamCount

END MODULE fbeamList_f 

subroutine BeamsTransformAll(sli,mt,err) ! see 'dofos and transforms.doc'
    use basictypes_f
    use BeamElementMethods_F
    USE  saillist_f
     IMPLICIT NONE   
	INTEGER , intent(in) :: sli
	REAL (kind=double), dimension(4,4):: mt 
	INTEGER , intent(inout) :: err	
	integer :: ok
	INTEGER  :: k 
	TYPE ( fbeamlist),Pointer :: beams
	TYPE ( beamelement) ,POINTER :: b
	err=16
	ok=0
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return
	beams=>saillist(sli)%fbeams
	
	err=8
	if(.not. ASSOCIATED(beams%list)) return
	err=0
	do k =  UBOUND(beams%list,1), LBOUND(beams%list,1),-1
	    if(.not.beams%list(k)%m_Used) cycle	
	    b=>beams%list(k) 
          err=err+ TransformBeam (b ,mt,sli)
	enddo
	if(err ==0) OK=1
end  subroutine BeamsTransformAll  
    
SUBROUTINE  removeAllfbeams(sn,err)
use ftypes_f
use saillist_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer SomeErrors
	TYPE ( fbeamlist) ,POINTER :: links
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

    links=>saillist(sn)%fbeams
	if(associated(links%list)) deallocate(links%list,stat=err)
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranlinks (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllfbeams

subroutine beamtest (vv,nv,rv)
USE, INTRINSIC :: ISO_C_BINDING
use basictypes_f
use ftypes_f
use saillist_f
   USE nodeList_f
!use feaprint_f
use ifport
use parse_f
use nodalrotations_f
use  cfromf_f
implicit none
INTEGER (c_int), intent(in) :: nv
INTEGER (c_int), intent(in), dimension(nv) :: vv

INTEGER(c_int), intent(inout)  :: rv
integer  j,k, sli,n

character*512 dum,name
        real(kind=double), dimension(3,3)::q
        real(kind=double),  dimension(3) ::r
type(sail), pointer :: s
!type(tri), pointer::t
type(fnode), pointer :: thenode
real(kind=double), allocatable, dimension(:,:) ::x
sli=1
rv=-16
if(sli>sailcount) then
return
endif
s=>saillist(sli)

write(*,*)  ' beamtest ',vv

rv=16
!  we assume a small model has been loaded ( sli=1)
!  we loop over a range of rotations
!  for each rotation we transform all the xxx  - assume no dofos
! and then feaprint
allocate(x(3,s%nodes%ncount  ))
do k = 0,5
    r(1) = k; r(2) = 1-k*k; r(3) = k/2; r = r/25.
    q = qmatrix(r)
    write(outputunit,'("rvec=",3f13.8)') r
    write(outputunit,'("q= ",3f13.8)') q(1,:)
    write(outputunit,'("   ",3f13.8)') q(2,:)
    write(outputunit,'("   ",3f13.8)') q(3,:)

    DO  N=1,s%nodes%ncount
      thenode=> s%nodes%xlist(n)
      if(.not. thenode%m_Nused) then ;  cycle; endif
      x(:,N) =  thenode%xxx
      if(issixnode(thenode)<0) then
            thenode%xxx=r
        else
            thenode%xxx=matmul(q,thenode%xxx);
      endif
    ENDDO

    DO  N=1,s%nodes%ncount
        thenode=> s%nodes%xlist(n)
        if(.not. thenode%m_Nused)   cycle
        thenode%xxx  =    x(:,N)
    ENDDO

    dum = ' '
    name = ' '
    call getcurrentdir(dum,255);
    call denull(dum)
    write(name,'(a,a,i2.2,a)') TRIM(dum),'/relaxout_',sli,'.txt'
    write (dum,'("mv ",a,"  ",a,".",i3.3)') trim(name),trim(name),k
    write(outputunit,*) trim(dum)
    j = system(trim(dum))

enddo
    deallocate(x)

end subroutine beamtest
