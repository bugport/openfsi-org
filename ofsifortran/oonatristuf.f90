module OONATRISTUF_F
	use tris_f
	use  mathconstants_f
	implicit none	
CONTAINS
subroutine big_axis_matrix(t,t9by6)
	implicit none
!parameters
	real*8 t(3,3)	! an element dca
! returns
	real*8 t9by6(9,6)

! locals
	integer i,l ,m, r,c

    t9by6 = 0.0

	do i=0,2
		do l=1,3
			do m=1,2
				r=  i*3 + l
				c=  i*2 + m
				t9by6(r,c) = t(l,m)
			enddo
		enddo
	enddo
end subroutine big_axis_matrix

subroutine one_tri_local_stiffness(t,tt,kl)
	use wrinkle_m_f
	use rx_control_flags_f
	implicit none

! parameters
	type(tri) ::  t			! element number
	real(kind=double), dimension(3,3) :: tt	! dca
!returns
	real*8 kl(6,6)	
! locals
    real*8 b(3,6)
    real*8 xl(3,3)
    real*8 ddmod(3,3)		! material stiffness matrix
    real*8 factor

    REAL*8  stress(3),  d(3)
    INTEGER state,k
    forall(k=1:3) d(k) = t%m_er(k)%m_fepe%m_delta0
    t%m_eps = matmul(t%g,d)     

	state = get_wrinkled_stiffness(t%N,  t%m_eps, t%dstfold, &
	debug2.and. t%creaseable, ddmod, stress, t%wr_angle)

        if(gPrestress) then ! added apr 2014
        ddmod =matmul( matmul( ElasticModifier ,ddmod),Transpose(ElasticModifier))
        endif
	if(any(isnan(ddmod))) then 
		kl=0.    ! a good place for a breakpoint
		return
	endif

	call rotate_material(ddmod,t%THETAE_rad )

	call one_tri_local_coords(t,tt,xl)

! matrix b relates local disps to strains ( eps=b*u)

	b(1,1) = xl(2,2) - xl(3,2)
	b(1,2) = 0.0d00
	b(1,3) = xl(3,2) - xl(1,2)
	b(1,4) = 0.0d00
	b(1,5) = xl(1,2) - xl(2,2)
	b(1,6) = 0.0d000
	b(2,1) = 0.0d00
	b(2,2) = xl(3,1) - xl(2,1)
	b(2,3) = 0.0d00
	b(2,4) = xl(1,1) - xl(3,1)
	b(2,5) = 0.0d00
	b(2,6) = xl(2,1) - xl(1,1)
	b(3,1) = xl(3,1) - xl(2,1)
	b(3,2) = xl(2,2) - xl(3,2)
	b(3,3) = xl(1,1) - xl(3,1)
	b(3,4) = xl(3,2) - xl(1,2)
	b(3,5) = xl(2,1) - xl(1,1)
	b(3,6) = xl(1,2) - xl(2,2)

	kl=0.0

	factor = 0.5/(b(2,6)*b(1,3) - (-b(2,4))*(-b(3,6)) )


	kl = matmul(matmul(transpose(b),ddmod),b)  
	kl = kl * factor

end subroutine one_tri_local_stiffness
end module OONATRISTUF_F


subroutine one_tri_global_stiffness_oona(t,kglobal)
	use OONATRISTUF_F
	use ftypes_f
	use tris_f
	use math_f
	use rx_control_flags_f
       use tanglobals_f
	implicit none

! parameters
        type(tri),intent(in) :: t		! the element

!returns
        real*8,intent(out) :: kglobal(9,9)	! stiffness matrix in global coords
!
!locals
	real*8 tt(3,3)	! axis transformation matrix
	real*8 kl(6,6)	! local stiffness matrix
	real*8 kg(9,9)	! global geometric stiffness matrix
	real*8 t9by6(9,6)

	tt= scaxismatrixByPtr(t%nr(1)%m_fnpe,t%nr(2)%m_fnpe,t%nr(3)%m_fnpe ) !    zaxis=>a(1:3,3)

	call big_axis_matrix(tt,t9by6)
        call one_tri_local_stiffness(t,tt,kl)
        kglobal=matmul(t9by6,matmul(kl,transpose(t9by6)))

        if(.not. GeoMatrix) return

        call One_Tri_Global_Geo_Stiffness(t,kg)
        kglobal= kglobal + kg

end subroutine one_tri_global_stiffness_oona
