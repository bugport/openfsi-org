module battenf90_f
use ftypes_f
use kbatten_F
use batten_f
implicit none
integer  BATTENLISTBLOCKSIZE
parameter (BATTENLISTBLOCKSIZE = 2 )


!To install on linux, first look at the two objectpath files to make sure that the 
!source files in linux are the same directories as here in msw.  

!It would be a good idea to make sure that all module files are in f90. 
!We've moved math.f and surface.f to /f90/... and copied to msw/f90

!If not, then shift the MSW f files around to match




	  logical,parameter :: kbflag = .true.

! kbflag sets the choice of Kbattens or beam battens at compile time
! beam battens 'seem' OK 6 june 2004 but to verify we need to check the rotational residuals
! and plot the moments graphically.
! we have checked that the Feb 2004 ttundeformed error in stroona isnt relevant here.

! Beam battens would be much better if 
!		we used the up-to-date strOOna formulation.  
!		The strOOna formulation accepted larger deformations within an element
!		The stroona formulation contains the torques from moment-curvature.


!For the batten data type see file batten storage.XLS. Need to add 'force' for plotting

contains

integer (C_INT) function Initialise(b)
use cfromf_f  ! for zeroptr
 type(batten)  ::b
 
 Initialise=1
 		b%count =0		
	!	b%nodes	=>NULL()		 
		b%ndm		=0		 
	!	b%se=>NULL()	 
	!	b%rev =>NULL() 
		b%slflag	=0	 
	!	b%ei=>NULL() 
		!b%rots=>NULL()	 
		!b%m_xib=>NULL()	 
		!b%ttu	=>NULL() 
		!b%b0	=>NULL()	 
		!b%zib	=>NULL()	 
		!b%ztb	=>NULL()	 
		!b%rb=>NULL()	! 
		!b%m_xp=>NULL()	! 
		!b%dfdx	=>NULL()!	 
		!b%drdm=>NULL()	! 
		!b%drdx=>NULL()	!	 
		b%zisLessTrim	=0.	
		b%btrim	=0.	 
		b%eas    =0. 
                b%btq=0
!		TYPE ( beamelement),POINTER, DIMENSION(:)	:: beamels 
		b%geom=1

		b%N=-99
		!b%used=.false.
		b%text="initialize"
		b%cptr=0
	 	b%sli = -1
 
end  function Initialise

function setbattentrim(sli,n,x) result (ok)  bind(C,name="cf_setbattentrim"   )
 USE, INTRINSIC :: ISO_C_BINDING
use saillist_f
        implicit none
        integer(C_INT), intent(in),value ::sli,n
        real (c_double), intent(in),value :: x
	type(batten) , pointer:: this
    integer(C_INT) :: ok
    OK=0
  ! we should do a bounds-check here before setting OK=1  

	this=>saillist(sli)%battens%list(n)
	
	if( this%zisLessTrim + x .gt. 0) then
	    this%btrim=x
	    write(outputunit,'(a,i3,i6,a,a12,a,en14.3 )',err=100) '(F) set batten trim on (',sli,n,' ) <',trim(this%text),'> to ',x	 
100	    ok=1
    	else
            OK=0
    	endif
end function setbattentrim

integer  function fillbatten(sli,n, se,rev,zi,trim,EA,Islide,ei)  !  sets bo and xib to zero
use saillist_f
use edgelist_f
        implicit none
        integer, intent(in) ::sli,n,Islide
        integer, dimension(*), intent(in) :: se,rev
        real (kind=double), intent(in) :: zi,trim,ea
        real(kind=double), intent(in), dimension (*) ::ei

	type(batten) , pointer:: this
	integer, dimension(2) ::s
    integer ok
	this=>saillist(sli)%battens%list(n)

    this%se =  se(1:this%count);
    this%rev= rev(1:this%count);
	this%b0=0.; this%m_xib=0.0
	this%zisLessTrim=zi
	this%btrim=trim;
	this%eas=ea
	this%slflag=islide
	s(1)=6; s(2)=this%count	

	this%ei=reshape(ei(1:6*this%count),s)

   ok= Make_Node_List(this%sli,this%count,this%se,this%rev,this%nodes)
   ok= Set_Batten_XIB (this%count,this%se,this%nodes,this%m_xib,this%zisLessTrim,this%sli)! nodes are (count+1)
   CALL KPCB_One_Initial_TT(this%count,this%m_Xib,this%B0,this%TTU,this%Zib)

fillbatten=1;
end function fillbatten




subroutine initialise_batten(sli,n,c,cptr,t,err) !see batten storage.XLS
use saillist_f
	integer, intent(in)			::sli,n,c
	integer(kind=cptrsize), intent(in), value ::cptr
	character(len=32) , intent(in) ::t
	integer, intent(out)::err
	
	type(batten) , pointer:: this
	integer :: ndm
	err=0;
	this=>saillist(sli)%battens%list(n)
	this%sli=sli
	this%text=t;
	this%cptr = cptr;
	this%count	 =   c
	allocate(this%nodes(c+1))		;this%nodes=0
	this%ndm	 =  3*(c+1)		; ndm = this%ndm
	allocate(this%se(c))			;this%se=0	
	allocate(this%rev(c))			;this%rev=0
	this%slflag	 =   0
	allocate(this%ei(6,c))			;this%ei=0 
	allocate(this%rots(3,(c+1)))		;this%rots=0
	allocate(this%m_xib(3,(c+1)))		;this%m_xib=0
	allocate(this%ttu(3,3,(c+1)))	;this%ttu=0
	allocate(this%b0(c))			;this%b0=0
	allocate(this%zib(c))			;this%zib=0
	allocate(this%ztb(c))			;this%ztb=0
	allocate(this%rb(3,(c+1)))		;this%rb=0
	allocate(this%m_xp(3*(c+1)))		;this%m_xp=0
	allocate(this%dfdx(ndm,ndm))	;this%dfdx=0
	allocate(this%drdm(ndm,ndm))	;this%drdm=0
	allocate(this%drdx(ndm,ndm))	;this%drdx=0
	this%zisLessTrim	=   0
	this%btrim=0
        this%EAS  =0
        this%btq  =0
	this%geom	=  1  ! .true.
end subroutine initialise_batten

FUNCTION   NextFreeBatten(sn,err) result(n)
	USE realloc_f
	USE saillist_f
	IMPLICIT NONE
	integer, intent(in)   :: sn
	INTEGER,INTENT (out)  ::  err
	integer :: i,k, istat,n
	TYPE ( battenlist) ,POINTER :: sList
	type (batten), pointer:: this =>NULL()
  
	err = 0
	slist=>saillist(sn)%battens
	if(.not. ASSOCIATED(sList%list)) THEN
		sList%m_BBlock= BATTENLISTBLOCKSIZE
		call reALLOCATE(sList%list, sList%m_Bblock, istat)

		if(istat /=0) THEN
			err = 1
			return
		endif

		sList%list%used = .FALSE.

		this =>sList%list(1)
		sList%count =1
		this%N=1
		this%used = .TRUE.
		n=1
		return
	ENDIF
	do i=1,ubound(sList%list,1)
		if(.not. sList%list(i)%used) THEN
				this=> sList%list(i); this%N=i; n=i
				sList%list(i)%used = .true.
				if(i > sList%count) then
					sList%count= i
				endif
				return
		endif		
	enddo
	! write(outputunit,*) ' we get here if no spare spaces in battenlist'
	i =  ubound(sList%list,1) 
	sList%m_Bblock = max(i,2); 
!	write(outputunit,*)  '	sList%m_Bblock ', 	sList%m_Bblock

     	CALL reallocate(sList%list, ubound(sList%list,1) + sList%m_Bblock,err)


	if(err /=0) then
		write(outputunit,*) ' reallocation error in BattenList'
	endif	

	do k= i+1,ubound(sList%list,1) 
		sList%list(k )%used = .FALSE.
	enddo
	this=> sList%list(i+1)
	this%used=.true. ; this%N=i+1

	sList%count=i+1
		n=this%n

END  FUNCTION   NextFreeBatten

function CreateBatten (sli, count ,err) result (this) ! does the allocation
	USE saillist_f
	use cfromf_f  ! for zeroptr
	integer, intent(in)		:: sli	! sailListIndex
	integer, intent(in)		:: count ! the number of edges this batten.
	integer, intent(out)	:: err
	TYPE ( batten) ,POINTER :: this
 	integer	:: n 
	TYPE ( battenlist) ,POINTER :: battens
    character(len=32) :: t
    t="initial name"
	this=>NULL()

	err = 16 


	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) then
		write(outputunit,*) ' sli HI ',sli
		return
	endif	
	if(sli < lbound(saillist,1)) then
		write(outputunit,*) ' sli LO ',sli
		return
	endif
	battens=>saillist(sli)%battens
	err=8
	err = 0

	n= nextFreeBatten(sli,err)
!
! Normal conditions
!	just starting : 
!	NN <= count. >> set X,Y,Z used
!	NN > ubound >> realloc then set X,Y,Z used  if alloc OK. set count
!	NN > count but < Ubound  set count.set  X,Y,Z used

! error conditions
!   unable to alloc
!   NN < 0
!
! now initialise to zero
	call initialise_batten(sli,n,count,0,t, err) ! count is no of edges

end function CreateBatten

SUBROUTINE RemoveFortranBatten (sn, NN,err) 
    USE, INTRINSIC :: ISO_C_BINDING
USE realloc_f
use saillist_f
 
use cfromf_f
IMPLICIT NONE
	INTEGER , intent(in) :: sn,nn
	INTEGER , intent(out) :: err
	TYPE ( battenlist) ,POINTER :: battens
	INTEGER :: ISTAT
	err = 16

	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return
	battens=>saillist(sn)%battens
!
!   What it does.
!	unsets the USED flag. realloc if  it can
!
! USUAL Conditions

!   NN =  count  >> decrement count
!    if then count < ubound-block, reallocate
!

! Unusual conditions
!   sailno out of range  >> err 16
!   stringlist not associated  >>err 8
! NN < lbound or > Ubound  >>err 8
!
  
	err=8
	if(.not. ASSOCIATED(battens%list)) return
	!write(outputunit,*) NN," (RFB)bounds ", LBOUND(battens%list,1)  ,UBOUND(battens%list,1)
	if(NN > UBOUND(battens%list,1)) return
	if(NN < LBOUND(battens%list,1)) return
	err=0
	if(.not. battens%list(NN)%used ) return	
	if(battens%list(NN)%cptr.ne.0 ) then
          err=ClearBatten(battens%list(NN)%cptr) ! just sets isindatabse to OFF
	    err=0
	endif

    istat=0
	if(associated(battens%list(NN)%nodes )) DEALLOCATE(battens%list(NN)%nodes,stat=istat)	! 	nodes	i	c+1
	if(associated(battens%list(NN)%se	)) DEALLOCATE(battens%list(NN)%	se	,stat=istat)	! 	se	i	c
	if(associated(battens%list(NN)%rev	)) DEALLOCATE(battens%list(NN)%	rev	,stat=istat)	! 	rev	L	c
	if(associated(battens%list(NN)%b0	)) DEALLOCATE(battens%list(NN)%	b0	,stat=istat)	! 	b0	r	c
	if(associated(battens%list(NN)%dfdx	)) DEALLOCATE(battens%list(NN)%	dfdx,stat=istat)	! 	dfdx	r	ndm x nbm
	if(associated(battens%list(NN)%drdm	)) DEALLOCATE(battens%list(NN)%	drdm,stat=istat)	! 	drdm	r	ndm x nbm
	if(associated(battens%list(NN)%drdx	)) DEALLOCATE(battens%list(NN)%	drdx,stat=istat)	! 	drdx	r	ndm x nbm
	if(associated(battens%list(NN)%ei	)) DEALLOCATE(battens%list(NN)%	ei	,stat=istat)	! 	ei	r	6 c
	if(associated(battens%list(NN)%rb	)) DEALLOCATE(battens%list(NN)%	rb	,stat=istat)	! 	rb	r	3(c+1)
	if(associated(battens%list(NN)%m_xp	)) DEALLOCATE(battens%list(NN)%	m_xp,stat=istat)	! 	rb	r	3(c+1)
	if(associated(battens%list(NN)%rots	)) DEALLOCATE(battens%list(NN)%	rots,stat=istat)	! 	rots	r	3(c+1)
	if(associated(battens%list(NN)%ttu	)) DEALLOCATE(battens%list(NN)%	ttu	,stat=istat)	! 	ttu	r	3(c+1)
	if(associated(battens%list(NN)%m_xib )) DEALLOCATE(battens%list(NN)%m_xib,stat=istat)	! 	xib	r	3(c+1)
	if(associated(battens%list(NN)%zib	)) DEALLOCATE(battens%list(NN)%	zib	,stat=istat)	! 	zib	r	c
	if(associated(battens%list(NN)%ztb	)) DEALLOCATE(battens%list(NN)%	ztb	,stat=istat)	! 	ztb	r	c


		battens%list(NN)%N=NN
		battens%list(NN)%count=0
		battens%list(NN)%text= "removed"
		battens%list(NN)%	nodes	=>NULL()
		battens%list(NN)%	se	=>NULL()
		battens%list(NN)%	rev	=>NULL()
		battens%list(NN)%	b0	=>NULL()
		battens%list(NN)%	dfdx	=>NULL()
		battens%list(NN)%	drdm	=>NULL()
		battens%list(NN)%	drdx	=>NULL()
		battens%list(NN)%	ei	=>NULL()
		battens%list(NN)%	rb	=>NULL()
		battens%list(NN)%	m_xp	=>NULL()
		battens%list(NN)%	rots	=>NULL()
		battens%list(NN)%	ttu	=>NULL()
		battens%list(NN)%	m_xib	=>NULL()
		battens%list(NN)%	zib	=>NULL()
		battens%list(NN)%	ztb	=>NULL()


		battens%list(NN)%cptr=0
		battens%list(NN)%used = .FALSE.

	if(NN == battens%count) THEN
		 battens%count =battens%count-1
! was < until 19 may 2003
		if( battens%count+battens%m_Bblock <= ubound(battens%list,1) ) THEN
			CALL reallocate(battens%list, ubound(battens%list,1) - battens%m_Bblock, err)
			battens%m_Bblock = max(battens%m_Bblock/2,2)
		ENDIF
	ENDIF

END SUBROUTINE RemoveFortranBatten
function Print_All_FBatten_Forces(p_unit,p_sli,p_howmuch) result(ok)
	use saillist_f
        use  cfromf_f
        implicit none
	INTEGER, intent(in):: p_Unit,p_sli
	integer,intent(in)::  p_howmuch

        integer L,Nbatts,OK,k
	TYPE ( battenlist) ,POINTER :: sList
	type (batten), pointer:: t
        character(len=256) atext,v
	
	slist=>saillist(p_sli)%battens
	OK = 1
	Nbatts = slist%count

        if(p_howmuch .le.BP_JUSTFORCES ) then
            if(nBatts>0) then
                write(p_unit,*) ' '
                write(p_unit,'(13x,a)') 'Batten        zi          trim      axialForce'
            endif
        endif
       DO L = 1,Nbatts
		t=>slist%list(L)
		if(p_howmuch .gt.BP_JUSTFORCES ) then
			write(p_unit,*)
			write(p_unit,*) ' ===========================================' 			
                        write(p_unit,*) ' FBATTEN FORCES FOR BATTEN ',L,'  ',trim(t%text)  ! typically 'corne'
		else
			write(p_unit,'(1x,a18,1x,\)') trim(t%text)
		endif

		if(kbflag) then
                        CALL KPrint_One_Batten_Forces (p_unit,p_howmuch,t%nodes,t%count,t%EI,t%se,t%rev,t%zislesstrim,t%btrim,t%EAS,t%btq,t%SLFlag,p_sli)
 ! post something like $boat$batten$corne$tension    t%btq
                        atext = 'kBatten$'//trim(t%text)//'$tension'
                        write(v ,'(G16.8 )') t%btq
                        k= Post_Summary_By_SLI(p_sli, trim(atext)//char(0),trim(v )//char(0))

		else
                        CALL Print_One_Batten_Forces (p_unit,t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_xib,t%ztb,t%zib,p_sli)
		endif
		if( p_howmuch >= BP_LONGPRINT ) then
 			call  Print_One_Batten_Matrices (p_unit,p_sli,t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%b0,t%Ztb,t%ZIB,t%GeoM,t%NDM,t%Dfdx,t%DrDm,t%DrDx&
     &					,t%rb,t%se,t%rev,t%zislesstrim,t%EAS,t%SLFlag)
 		endif
 	 ENDDO
end function Print_All_FBatten_Forces

function FBatten_Resids(p_sli) result(ok)
use saillist_f
      IMPLICIT NONE

! OBJECT To return R (residual Forces) to calling function given
!existing rotations and X
	integer, intent(in) :: p_sli
	integer OK
	integer L, Nbatts  
	TYPE ( battenlist) ,POINTER :: sList
	type (batten), pointer:: t
	logical ::flag
	flag = kbflag

	slist=>saillist(p_sli)%battens
	OK = 1
	Nbatts = slist%count
!!!!$ O M P PARAL LEL DO PRIVATE (L,t) shared (saillist,flag,nbatts,slist,p_sli) default(none)
	DO L = 1,Nbatts
		t=>slist%list(L)
		if(kbflag) then
			call KMake_One_Batten_Forces(t%nodes,t%count,t%EI,t%se,t%rev,t%zislesstrim,t%btrim,t%EAS,t%SLFlag,p_sli)
		else
                        call Make_One_Batten_Forces(t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%Ztb,t%ZIB,t%Rb,t%se,t%zislesstrim,t%btrim,t%EAS,t%SLFlag,.true.,t%sli)
		endif
	ENDDO
!!!!$ O M P  EN D PARALLEL DO 
END function FBatten_RESIDs

function AssembleBattenElts(sli,m) result(OK) ! see FMake_All_Batten_Matrices
 	use saillist_f
 	use  stiffness_f
	IMPLICIT NONE
! parameters
    integer, intent(in) :: sli
    integer(kind=cptrsize)  :: m
     type(sail), pointer  ::sl=>NULL()
	integer				:: OK      
	integer L,Nbatts
	TYPE ( battenlist) ,POINTER :: sList
	type (batten), pointer:: t
    if(.not. is_active(sli)) return
	sl=>saillist(sli)
	slist=>saillist(sli)%battens
	OK = 1
	Nbatts = slist%count

dol:DO L = 1,Nbatts
		t=>slist%list(L)
		if(kbflag) then
			call KMake_One_Batten_Axes(t%nodes,t%count,t%TTU,t%m_Xib,t%b0,t%ZIB,t%se,t%sli)
                        call  KMake_One_Batten_Matrices(t%nodes,t%count,t%EI,t%TTU,t%Ztb,t%ZIB,t%Ndm,t%Dfdx,t%DrDm,t%DrDx &
		 & ,t%se,t%rev,t%zisLessTrim+t%btrim,t%EAS,t%SlFlag,sli)
		else
			call Make_One_Batten_Axes(t%nodes,t%count,t%TTU,t%m_Xib,t%b0,t%ZIB,t%se,t%sli)
			call Make_One_Batten_Matrices&
		    &(t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%b0,t%Ztb,t%ZIB,t%GeoM,t%Ndm,t%Dfdx,t%DrDm,t%DrDx&
		    & ,t%se,t%rev,t%zisLessTrim+t%btrim,t%EAS,t%SlFlag,t%sli) ! set xp
		endif
		call set_batten_xp(t)

      if(any(isnan(t%dfdx))) then
           write(outputunit,*) ' BAD batten'
           t%dfdx=0
           cycle
      endif

      if(t%count < 1) cycle	
      call addStiffness(sl,t%nodes(1:t%count+1),t%dfdx,m)
 	
	ENDDO dol

end function AssembleBattenElts

function FMake_All_Batten_Matrices(p_sli) result(ok)
	use saillist_f
	IMPLICIT NONE
 
	integer, intent(in)	:: p_sli
	integer				:: OK      

	integer L,Nbatts
	TYPE ( battenlist) ,POINTER :: sList
	type (batten), pointer:: t

	slist=>saillist(p_sli)%battens
	OK = 1
	Nbatts = slist%count

dol:DO L = 1,Nbatts
		t=>slist%list(L)
		if(t%count<1)then
		 cycle
		 endif
		if(kbflag) then
			call KMake_One_Batten_Axes(t%nodes,t%count,t%TTU,t%m_Xib,t%b0,t%ZIB,t%se,t%sli)
                        call  KMake_One_Batten_Matrices(t%nodes,t%count,t%EI,t%TTU,t%Ztb,t%ZIB,t%Ndm,t%Dfdx,t%DrDm,t%DrDx &
		 & ,t%se,t%rev,t%zisLessTrim+t%btrim,t%EAS,t%SlFlag,p_sli)
		else
			call Make_One_Batten_Axes(t%nodes,t%count,t%TTU,t%m_Xib,t%b0,t%ZIB,t%se,t%sli)
			call Make_One_Batten_Matrices&
		    &(t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%b0,t%Ztb,t%ZIB,t%GeoM,t%Ndm,t%Dfdx,t%DrDm,t%DrDx&
		    & ,t%se,t%rev,t%zisLessTrim+t%btrim,t%EAS,t%SlFlag,t%sli) ! set xp
		endif
		call set_batten_xp(t)
      	CALL Add_One_BMasses(p_sli,t%DfDx,t%NDM,t%nodes,t%count)
	ENDDO dol
		if(.not. kbflag) then
			l = zero_One_Batten_Residuals(p_sli,10.0D00)
		endif

END function FMake_All_Batten_Matrices


function FWrite_Battens(u,p_sli) result(error)
use saillist_f
      IMPLICIT NONE
      INTEGER u	 ,p_sli
 
!* returns

      integer error

! locals
		INTEGER  nbatts,L
		logical geom,l_error
		TYPE ( battenlist) ,POINTER :: sList
		type (batten), pointer:: t

		slist=>saillist(p_sli)%battens
	    error=0
		Nbatts = slist%count
		geom = .true.
		if(Nbatts >0) geom = (slist%list(1)%geom .eq. 1)
        write(u,*) " "
		write(u, '(a,i5,L10)',err=98) "Battens  ", Nbatts ,geom
    write(u,*) "count, lbound,ubound ",nbatts, lbound(slist%list ),ubound(slist%list )
		DO L = 1,Nbatts
			t=>slist%list(L)
			if(.not. t%used) cycle

			write(u,'(i5,1x,a)',err=98)t%count,t%text
			if(t%count==0) cycle			
			call Print_One_Batten(u,t%count,t%EI,t%b0,t%se,t%zisLessTrim,t%btrim,t%EAS,t%SlFlag,l_error)
			if(l_error) error = 1
		ENDDO
      RETURN
98    error = 1 
      END function FWrite_Battens

function zero_One_Batten_Residuals(p_sli, Tmax) result(OK);
	use saillist_f
	implicit none
	integer, intent(in) :: p_sli
	real(kind=double), intent(in)	::tmax
	integer OK
	integer I,J,L,Nbatts,istat
	TYPE ( battenlist) ,POINTER :: sList
	type (batten), pointer:: t
	real(kind=double), dimension(:), allocatable :: rots,r
	integer, dimension(1) :: sh
	integer, dimension(2)	::sh2
	real(kind=double)		:: rmax, l_factor = 0.6
	slist=>saillist(p_sli)%battens
	OK = 1
	Nbatts = slist%count

	if(kbflag) return
dol:	DO L = 1,Nbatts
		t=>slist%list(L)
!	iteratively update rots until rotresids are small
		allocate(rots(t%ndm)) ;		allocate(r(t%ndm)) ;
		sh(1) = t%ndm ; 	sh2(1) = 3 ; sh2(2) = t%count+1 
do_J:		do j = 1,10
doI	:		do i=1,100

	!				update matrices
					call Make_One_Batten_Axes(t%nodes,t%count,t%TTU,t%m_Xib,t%b0,t%ZIB,t%se,t%sli)

					call Make_One_Batten_Matrices&
					& (t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%b0,t%Ztb,t%ZIB,t%GeoM,t%Ndm,t%Dfdx,t%DrDm,t%DrDx,t%se,t%rev,t%zisLessTrim+t%btrim,t%EAS,t%SlFlag, t%sli )
	!				update forces
                                         call Make_One_Batten_Forces(t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%Ztb,t%ZIB,t%Rb,t%se,t%zisLessTrim,t%btrim,t%EAS,t%SLFlag,.false.,t%sli)

					rots = RESHAPE (t%rots, sh )
					r = RESHAPE (t%rb, sh )

					rots = rots + matmul(t%drdm,r)*l_factor;
					t%rots = RESHAPE (rots, sh2 )
					rmax = maxval(abs(r))

					if(rmax .lt. Tmax) exit do_J
				enddo doI
				l_Factor = l_Factor / 2.0d00
			enddo do_J
			deallocate(r,rots,stat=istat)
		enddo dol
end function zero_One_Batten_Residuals

Function Update_One_Batten_Rotations (t,sli) result(ok)
use coordinates_f
implicit none
	TYPE ( batten) ,POINTER :: t
	integer, intent(in) ::sli
!	rots = rots + drdx* dx
! where dx = this X - last X
! then set last_X to this_X
	integer :: ok 
	integer i,j,istat
	real(kind=double), dimension(:), allocatable :: rots,dx, l_x
	integer, dimension(1) :: sh
	integer, dimension(2)	::sh2
	ok=1
	
	if(kbflag) return
	
	! debug
        call Make_One_Batten_Forces(t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%Ztb,t%ZIB,t%Rb,t%se,t%zisLessTrim,t%btrim,t%EAS,t%SLFlag,.false.,sli)

	allocate(rots(t%ndm)) ;		allocate(dx(t%ndm)) ; 	allocate(l_x(t%ndm)) 
	sh(1) = t%ndm ; 	sh2(1) = 3 ; sh2(2) = t%count+1 
	j=1
	do i=1,t%count+1
		l_x(j:j+2) = get_Cartesian_X(t%nodes(i),sli)
		j = j + 3
	enddo
	dx = l_x -  RESHAPE (t%m_xp, sh )
	rots =  matmul(t%drdx,dx)
	t%rots = t%rots + RESHAPE (rots, sh2 ) ! the sense is a guess  Both + and - are unstable! 
	t%m_xp = l_x
	deallocate(dx,rots,l_x,stat=istat)
! debug
!	call Make_One_Batten_Forces(t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%Ztb,t%ZIB,t%Rb,t%se,t%zisLessTrim,t%trim,t%EAS,t%SLFlag,.false.)

end function Update_One_Batten_Rotations

subroutine set_batten_xp(t) 

use coordinates_f
	TYPE ( batten) ,POINTER :: t
	integer :: i,j
	j=1
	if(t%count<1) return
	do i=1,t%count+1
		t%m_xp(j:j+2) = get_Cartesian_X(t%nodes(i),t%sli)
		j = j + 3
	enddo
end subroutine set_batten_xp

function Update_All_Batten_Rotations() result(ok) ! all models
use saillist_f
	integer :: sli,ok
	integer i
	TYPE ( battenlist) ,POINTER :: battens
	TYPE ( batten) ,POINTER :: t
	logical	:: SomeErrors
	SomeErrors=.false.
	ok=1
	if(kbflag) return

	if(.not. associated(saillist)) return

	do sli = lbound(saillist,1),ubound(saillist,1)
		if(.not. is_active(sli)) cycle
		battens=>saillist(sli)%battens
!$O M P PARALLEL DO PRIVATE (i,t,ok) shared (saillist,SomeErrors) default(none)
		do i = 1,battens%count
			t=>battens%list(i)
			ok = Update_One_Batten_Rotations (t,sli) 
			if(ok ==0) SomeErrors = .true.
		enddo
!$O M P END PARALLEL DO
	enddo
		ok=1
	if(someErrors) ok = 0

end function Update_All_Batten_Rotations

function Zero_All_Batten_Residuals(tmax) result(k)
use saillist_f
	real(kind=double), intent(in)	::tmax
	integer :: k

! forall sli function zero_One_Batten_Residuals

	integer :: sli 
	k = 0
	if(kbflag) return
	if(.not. associated(saillist)) return

	do sli = lbound(saillist,1),ubound(saillist,1)
		if( .not. is_active(sli)) cycle
		k = zero_One_Batten_Residuals(sli,tmax)
	enddo
end function Zero_All_Batten_Residuals



! (count, list)  is a sequence of nodes in sli
! we cal

function Set_Batten_XIB (count,se,list,xib,zis,sli) result(ok) !based on Default_Batten
	use vectors_f
	use saillist_f
	use coordinates_f
      IMPLICIT NONE
	integer, intent(in) ::sli
	integer ::count
	  INTEGER, dimension(count), intent(in) ::  se ! edge list
	  INTEGER, dimension(count+1), intent(in) ::list ! list is (count+1) nodes
      REAL(c_double), intent(out), dimension(3,count+1) :: Xib
      REAL(c_double), intent(in) :: Zis  ! total length (approx)

      INTEGER I 
      REAL*8 C(3), length ,Z
      LOGICAL error 
      integer::ok; ok =1
!* method.

!* if any XIB is non-zero return
!* ELSE assume that we have to fit the batten to the sail. 
!* the origin of XIB is arbitrary so we set it out from the origin,
! in the direction of the chord, stepping by the edge lengths.
!* 

      z = Zis
      if (z .GE. 1.0E-06) THEN
   !    DO I=1,count
     !     DO K=1,3
     !      if(Dabs(xib(k,i)) .gt. 1.0D-6) RETURN  ! xib already exist. 
      !    ENDDO
     !  ENDDO
        if(any(dabs(xib(1:3,1:count+1))>1e-6)) return
      ENDIF
      z = 0.0D00
	if(count < 1) then
		write(outputunit,*) ' (Set batten XIB ) ignoring short. Count ',count
		ok=0; return
	endif
     c  = get_Cartesian_X(list(count+1),sli) -get_Cartesian_X(list(1),sli)
          Xib(1:3,1) = 0.0d00
 
      CALL normalise(c,length,error)
   
      if (error) THEN
	        write(outputunit,*)' Short batten (or closed)'
 	        count=0
	        return
       ENDIF

      DO I=2,count+1
          Xib(1:3,i) = Xib(1:3,i-1) + C * saillist(sli)%edges%list(se(i-1))%m_Zlink0
       ENDDO
 END function Set_Batten_XIB 

end module battenf90_f


SUBROUTINE removeAllFBattens(sn,err)  ! not in any module
use ftypes_f
use saillist_f
use battenf90_f

IMPLICIT NONE
	INTEGER , intent(in) :: sn
	INTEGER , intent(out) :: err

	integer i,SomeErrors
	TYPE ( battenlist) ,POINTER :: battens
	err = 16
	SomeErrors=0
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

	battens=>saillist(sn)%battens
	do i = battens%count,1,-1
		call RemoveFortranBatten (sn, i,err) 
		if(err /=0) SomeErrors = SomeErrors+1
	enddo
	if(SomeErrors /=0) then
		write(outputunit,*) 'model',sn,' removeFortranBatten (from remove ALL) Nerr=',SomeErrors
	endif
END SUBROUTINE removeAllFBattens

subroutine Print_All_Batten_Matrices(sli, unit)
use saillist_f
use batten_f
implicit none

integer, intent(in) :: sli,unit
	integer i 
	TYPE ( battenlist) ,POINTER :: battens
	
		TYPE ( batten) ,POINTER :: t
	battens=>saillist(sli)%battens
	do i = battens%count,1,-1
	    t=>battens%list(i)
	    
    call Print_One_Batten_Matrices (unit,sli,&
     &  t%nodes,t%count,t%EI,t%rots,t%TTU,t%m_Xib,t%b0,t%Ztb, &
     t%ZIB,t%GeoM,t%NDM,t%Dfdx,t%DrDm,t%DrDx&
     &,t%rb,t%se,t%rev,t%zisLessTrim,t%eas,t%SLFlag)
	enddo
end subroutine Print_All_Batten_Matrices
