	MODULE BAR_ELEMENTS_F
	use basictypes_f
	implicit none
	CONTAINS
	function Bar_Matrix(d,zi,zt,tq,ea,s) result(iret)  ! only used for masses
	implicit none
	real(kind=double),intent(in),dimension(3)	:: D ! Unnormalised vector
	real(kind=double), intent(in)				:: zi,zt,tq,ea
	real(kind=double),intent(out),dimension(3,3)	:: s 
	integer :: iret
	real(kind=double),dimension(3,3)	:: sd 

	iret =			Bar_Matrix_Geo(d,zt,tq,s)
	iret = iret +	Bar_Matrix_Dir(d,zi,zt,ea,sd)

	s = s + sd
end function  Bar_Matrix

function Bar_Matrix_Dir(d,zi,zt,ea,s) result(iret)
	implicit none
	real(kind=double),intent(in),dimension(3)	:: D ! Unnormalised vector
	real(kind=double), intent(in)				:: zi,zt,ea
	real(kind=double),intent(out),dimension(3,3)	:: s 
	integer :: iret
	integer :: i
	iret=1
	do i=1,3
		s(i,i:3) = d(i) * d(i:3)	
		s(i,1:i-1) = s(1:i-1,i)
	enddo
	
	s = s * ea/zt**2/zi 
end function  Bar_Matrix_Dir	
	
function Bar_Matrix_Geo(d,zt,tq,s) result(iret)
	implicit none
	real(kind=double),intent(in),dimension(3)	:: D ! Unnormalised vector
	real(kind=double), intent(in)				:: zt,tq
	real(kind=double),intent(out),dimension(3,3)	:: s 
	integer :: iret
	integer :: i	
#ifdef _DEBUG
    if(abs(sqrt(d(1)**2 + d(2)**2 + d(3)**2 )-zt  ) > 1e-10) then
        write(outputunit,*) 'Bar_Matrix_Geo incompatible args', d,zt
    endif
#endif

	iret=1
	s=0
	do i=1,3
		s(i, i:3) = -d(i) * d(i:3)	
		s(i, 1:i-1) = s(1:i-1,i)
	s(i,i) = s(i,i) + zt**2
	enddo
	
	s = s * tq/zt**3
#ifdef _DEBUG_NEVER
    write(151,'(a,13(1x,G15.8))') 'Bar_Matrix_Geo', d,tq,s
#endif

end function  Bar_Matrix_Geo
END MODULE BAR_ELEMENTS_F