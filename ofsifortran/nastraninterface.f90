module nastran_interface_f
contains
function write_Nastran(sli,p_fname) result (ok)
	  use saillist_f
	  integer, intent(in) :: sli
	logical error
	  integer :: i,ok 
	  character(len=*),intent(in) :: p_fname
	  character(len=256) :: fname
	  ok=1
	  fname = trim(p_fname)
	  write(*,*) ' fn write_Nastran with args ', sli,'  <',trim(fname),'>'
	  if(fname == "") then
		 fname = saillist(sli)%sailname
		 write(*,*) ' using mname ', trim(fname)
	 endif
	i = index(fname,'.')
	if(i > 1 .and. i> len_trim(fname)-5) then  ! > 1 so we never get a file beginning with a dot
		  fname(i:) = '.NAS'
	else
		fname = trim(fname)// '.NAS'
	endif
	  write(*,*) ' write nastran made filename <',trim(fname),'>'
	  open(unit=200,file=trim(fname),err=99)
	  write(200,*) '$'
	  write(200,*) '$ OpenFSI.org bulk data output - grid, tris and their properties only'
	  write(200,*) '$ comma-separated free format'
	  write(200,*) 'BEGIN BULK' 
	  write(200,*) '$'
	  write(200,*) '$---1--|---2---|---3---|---4---|---5---|---6---|---7---|---8---|---9---|--10---|'
		call write_Nastran_Nodes(200,sli,error)
		call write_Nastran_Triangles(200,sli,error)
		write(200,*) '$end of OpenFSI.org output'
	  write(200,*) 'ENDDATA' 
	  write(200,*) '$'
	close(200)
	if(error) ok=0
	return 
	
99 	write(*,*) 'cant open <',trim(fname),'>'
	ok=0
	return 
 end function write_Nastran

SUBROUTINE write_Nastran_Nodes(Uout,sli,error)
        use coordinates_f
        use saillist_f
        use ftypes_f

        IMPLICIT NONE
        integer, intent(in) ::sli
        INTEGER,intent(in) :: Uout
        LOGICAL ,intent(out) :: Error
        TYPE ( nodelist) ,POINTER :: nodes
        INTEGER :: i  
        REAL*8 xo(3)
      
		write(uout,*) '$ '
		write(uout,*) '$ OpenFSI.org grid nodes - doesnt parse on all readers'
		write(uout,*) '$ '

!	 zndal looks like,,,	
!$---1--|---2---|---3---|---4---|---5---|---6---|---7---|---8---|---9---|--10---|
!GRID 1 0.0 0.0 0.0
!GRID 2 0.0 1.0 0.0
!GRID 3 0.0 2.0 0.0
!GRID 16 6.0 3.0 0.0
!CQUAD4 11 21 1 5 6 2
!CQUAD4 19 21 11 15 16 12
! $ STATIC LOAD (APPLIED FORCES)
! $---1--|---2---|---3---|---4---|---5---|---6---|---7---|---8---|---9---|--10---|
!LOAD 300 1. 1. 100 1. 200
!FORCE 100 16 400. -1.0
!FORCE 200 13 400. 1.0
! $
!MAT1 31 1.07+07 .33 2.59-04 +MAT1
!+MAT1 60000. 60000. 40000.
!PLOAD2 100 1. 11 THRU 14
!PSHELL 21 31 .25 31
!SPC1 102 123456 1 2 3 4
!ENDDATA
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return

	nodes=>saillist(sli)%nodes

	if(ASSOCIATED(nodes%Xlist)) THEN
		do i =1,  nodes%Ncount
		   if(nodes%xlist(i)%m_Nused)  THEN
                       CALL XtoLocal(sli , nodes%xlist(i)%XXX,  xo)
 			write(Uout,'(A,2(1h,i6),3(1h,,G15.6))',err=10) 'GRID',i,0,xo(1),xo(2),xo(3)
	   	   ENDIF	
		ENDDO
	ENDIF
	!	
      return 
10    error = .true.
END SUBROUTINE write_Nastran_Nodes

SUBROUTINE write_Nastran_Triangles(Uout,sli,error)
        use coordinates_f
        use saillist_f
        use ftypes_f
        IMPLICIT NONE
        integer, intent(in) ::sli,Uout
        LOGICAL,intent(out) :: Error
        INTEGER n	   ,nout
        REAL*8 a,thk  
        INTEGER n1,n2,n3, j,k,nnt
        type(tri), pointer :: t
        TYPE (tri),  DIMENSION(:), POINTER :: tlist	
    
		thk = 0.001
		write(uout,*) '$ '
		write(uout,*) '$ OpenFSI.org 3-node orthotropic triangles '
		write(uout,*) '$ '
        tlist=>saillist(sli)%tris%list
        nnt = saillist(sli)%tris%count

      	DO N=1,NNT
           if(.not. tlist(N)%used ) cycle
           t=>tlist(N)
		  nout = n 
		  n1 =  t%n13(1) !  n13(n,1) -nds(Model)+1
		  n2 =   t%n13(2) ! n13(n,2) -nds(Model)+1
		  n3 =   t%n13(3) !  n13(n,3) -nds(Model)+1

		  a =   t%m_area
		  if(.not. t%use_tri) then
			 nout = -nout
		  endif
! original		  write(Uout,'(A,i6,6(3h , ,g14.7))',err=10)'MAT2 , ',nout,((dstfold(j,k,N)/thk,J=k,3),K=1,3)
		  write(Uout,'(A,1h,,i6,6(3h , ,g14.7))',err=10)'MAT2',nout,((t%dstfold(j,k)/thk,J=k,3),K=1,3)


	! PSHELL  pid mid1  thk mid2  0.0
		  write(Uout,'(A,2(3h , ,i6),3h , ,F10.4,3h ,  ,I6,3h , ,F10.4 )') 'PSHELL',nout, nout,thk,nout,0.0

	  !    write(Uout,'(A,2i6,1x,9(1x,G15.5) )') 'CORD2R  ',nout,0,Origin, PtOnZ, PtOnX
      ENDDO

	  	write(uout,*) '$ '
		write(uout,*) '$ END OpenFSI.org triangle properties '
		write(uout,*) '$ '

      	DO N=1,NNT
           if(.not. tlist(N)%used ) cycle
           t=>tlist(N)
		  nout = n 
		  n1 =  t%n13(1) !  n13(n,1) -nds(Model)+1
		  n2 =   t%n13(2) ! n13(n,2) -nds(Model)+1
		  n3 =   t%n13(3) !  n13(n,3) -nds(Model)+1

		  a =   t%m_area
		  if(.not. t%use_tri) then
			 nout = -nout
		  endif
!		  write(Uout,'(A, 5(3h , ,i6),3h , ,F13.5,3h , ,i3)',err=10) 'CTRIA3', nout,nout,n1,n2,n3, thetae(N),0
! femap format
!12345678901234567890123456789012345678901234567890123456789
!CTRIA3       382     382      91     216      90-162.357 
!		  write(Uout,'(A8, 5i8,F8.3)',err=10) 'CTRIA3', nout,nout,n1,n2,n3, thetae(N)

! free format
		  write(Uout,'(A, 5(2h, ,i8),1(2h, ,F12.5))',err=10) 'CTRIA3', nout,nout,n1,n2,n3, t%THETAE_rad
		  ! to 0 MCID means use PSHELL field 4
	  ! field 4 of the Pshell is the thickness (0.001m)
      ENDDO

	  	write(uout,*) '$ '
		write(uout,*) '$ END OpenFSI.org 3-node orthotropic triangles '
		write(uout,*) '$ '
      return
10    error = .true.
      END  SUBROUTINE write_Nastran_Triangles
end module nastran_interface_f
