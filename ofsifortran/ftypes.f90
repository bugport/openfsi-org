!
! Aug 2006	integer(kind=int _ptr_ kind()) ::
! June 2002. Added the XM_Ptr type to permit element-by-element approach.

MODULE ftypes_f
USE  basictypes_f
USE, INTRINSIC :: ISO_C_BINDING
! USE mkl95_PRECISION, ONLY: WP => DP
 USE f95_PRECISION, ONLY: WP => DP
USE  linklist_f
use dofo_subtypes_f
use omp_lib
 
    IMPLICIT NONE
    INTEGER, PARAMETER::  I_size = 28 !  00
    INTEGER, PARAMETER :: R_size = 128 ! 000
    INTEGER, PARAMETER :: OUTPUT = -1024
    INTEGER, PARAMETER :: IGNORE  = -2048
    INTEGER, PARAMETER :: BTRANS = 3   ! 2**3 = 8
    INTEGER, PARAMETER ::  BDELTA  = 2  ! 2**2 = 4
    INTEGER, PARAMETER :: TRANSFORM = 2**BTRANS
    INTEGER, PARAMETER :: DELTA  = 2**BDELTA

    integer, parameter :: BP_JUSTFORCES = 1
    integer, parameter :: BP_NORMAL = 2
    integer, parameter :: BP_LONGPRINT = 3



	TYPE :: TriRef
	    type (Tri),pointer :: t=>NULL()
	end TYPE TriRef					

	TYPE :: batten
!variable name	type	size	KI	KR
		integer :: count =0		
		integer,pointer, dimension(:) :: nodes	=>NULL()			!	c+1	2	
		integer :: ndm		=0								!	1	3	
		integer,pointer, dimension(:):: se =>NULL()				!	c	4	
!K	I	19		
		integer,pointer, dimension(:):: rev =>NULL()			!	c	5	
		integer :: slflag	=0								!	1	6	
		REAL (kind=double) ,pointer, dimension(:,:) :: ei =>NULL()	!		6 c		1
		REAL (kind=double) ,pointer, dimension(:,:) :: rots =>NULL()	!		3(c+1)		2
		REAL (kind=double) ,pointer, dimension(:,:) :: m_xib =>NULL()	!	3,(c+1)		3
		REAL (kind=double) ,pointer, dimension(:,:,:) ::ttu	 =>NULL()!	3,3,(c+1)		4
		REAL (kind=double) ,pointer, dimension(:) :: b0	 =>NULL()	!	c		5
		REAL (kind=double) ,pointer, dimension(:) ::zib	 =>NULL()	!	c		6
		REAL (kind=double) ,pointer, dimension(:) ::ztb	=>NULL()	!	c		7
		REAL (kind=double) ,pointer, dimension(:,:) ::rb =>NULL()	!	3, c+1		8
		REAL (kind=double) ,pointer, dimension(:)   ::m_xp =>NULL()	!	3* c+1		the last X values, packed
		REAL (kind=double) ,pointer, dimension(:,:) ::dfdx 	=>NULL()!	ndm x nbm		9
		REAL (kind=double) ,pointer, dimension(:,:) ::drdm =>NULL()	!	ndm x nbm		10
		REAL (kind=double) ,pointer, dimension(:,:) ::drdx =>NULL()	!	ndm x nbm		11
		REAL (kind=double) ::zisLessTrim	=0.	
		REAL (kind=double) ::bTrim	=0.							!	1		12
                REAL (kind=double) ::eas    =0.
                REAL (kind=double) ::btq    =0.
		integer :: geom=1

		INTEGER :: N=-99
		LOGICAL :: used=.false.
		character (len=32)	::text="initialize"
		integer(kind=cptrsize) :: cptr=0
		integer :: sli = -1
	END TYPE batten

        TYPE :: battenlist
            TYPE (batten), DIMENSION(:), POINTER :: list =>NULL()
            INTEGER :: count = 0, m_Bblock ! =100
            TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE battenlist

        type, abstract:: fsplineI
                integer:: m_np =0 ! the number of CVs
               ! note for an interpolating spline this is NOT the length of localCVS
                ! nor the length of the string's pointlist
                integer:: degree,m_type
                 LOGICAL :: Sliding , Interpolatory
                type (string),pointer ::m_ownerString =>NULL()
                real(kind=double), allocatable, dimension(:) :: kt  ! the knots
                real(kind=double), allocatable, dimension(:,:) :: localCVs
                real(kind=double):: tmin = 0.0d00
                real(kind=double):: tmax = 1.0d00
        contains
           procedure (FSplineJMatrix) ,deferred,pass(this) :: FSplineJMatrix
           procedure (FSplineJMatrixMultiple) ,deferred,pass(this) :: FSplineJMatrixMultiple
           procedure (ListFspline) ,deferred,pass(this) :: ListFspline
           procedure (FsplineEvaluate ) ,deferred,pass(this)  :: FsplineEvaluate
           procedure (derivativebyds ) ,deferred,pass(this)  :: derivativebyds
           procedure (FindNearest ) ,deferred,pass(this)  :: FindNearest
           procedure (ClearFspline ) ,deferred,pass(this)  :: ClearFspline
        end type fsplineI

        type dofo_spline
                class (fsplineI), pointer:: m_spl =>NULL()
                real(kind=double) :: OriginalSlaveTee =-99
                real(kind=double), allocatable, dimension(:) :: OriginalSlaveTees
                real(kind=double), allocatable, dimension(:) :: SlaveTeeOrigin
        end type dofo_spline

       type :: dofo
            INTEGER ::   m_dofosli=-999  ,m_dofoN=-1;
            INTEGER(kind=cptrsize) :: m_rxfptr=0
            INTEGER ::  ndof, m_dofotype=0 , dofoRowIndex=-1
            logical ::IsLinear = .true. ! if type <=5 dofos are born linear.
            type (fnoderef_p) , pointer, dimension(:) :: m_Fnodes=>NULL()  ! Dependent nodes. get their coords exclusively from here.
            real(kind=double), pointer,dimension(:)   :: origin=>NULL()
            real(kind=double), pointer, dimension(:,:) :: jacobi=>NULL()
            real(kind=double), pointer, dimension(:,:) :: InvJacobi=>NULL() ! CAREFUL. transpose is OK for the orthogonal types but the slidesurface wants the inverse
            real(kind=double), pointer, dimension(:) :: t=>NULL(),vt=>NULL(),rt=>NULL() ! parameter, paramspeed and paramR
            real(kind=double), pointer, dimension(:,:) :: xmt =>NULL(), bt=>NULL()
            real(kind=double), pointer, dimension(:,:) :: Stifft =>NULL()

            TYPE ( dofo_SlideLine), pointer         :: d1=>NULL();
            TYPE ( dofo_SlidePlane), pointer        :: d2=>NULL();
            TYPE ( dofo_SlideSurface), pointer      :: d3=>NULL();
            TYPE ( dofo_SlideCurve), pointer        :: d4=>NULL();
            type ( dofo_Spline), pointer            :: d5=>NULL();
            type ( dofo_rigidBody), pointer         :: d6=>NULL();
! any reference to class in a derived type seems to crash Intel(R) Visual Fortran 11.1.026 through 175 IA-32 and intel64

#ifdef NO_DOFOCLASS
           type (dofo),pointer :: parent1=>NULL(), parent2=>NULL(), child=>NULL()  ! or class
#else
           class (dofo),pointer ::parent1=>NULL(), parent2=>NULL(), child=>NULL() ! or type
#endif

            real(wp),  allocatable, dimension(:,:)::  dofosv
            real(wp),  allocatable, dimension(:)::  m_svOrg     !m_svOrg + dofoSV dot (childTees)  is the recovered  Tees when we delete the child

            real(kind=double) :: m_remaining_timestep =1.0 , ke=-1
            character(len=256) :: m_DofoLine = ""
        END type dofo

	TYPE :: dofoholder
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::o =>NULL() ! or class
#else
        class (dofo),pointer ::o =>NULL()
#endif
            type(dofoholder), pointer ::m_next =>NULL()
	    integer :: nn =0
	    logical ::used = .false.
	end TYPE  dofoholder

	TYPE :: dofolist
		TYPE (dofoholder), DIMENSION(:), POINTER :: list =>NULL()
		INTEGER :: count =0, dblock =100
	END TYPE dofolist 

	TYPE :: Fnode
            REAL (kind=double)  ,dimension(3) :: XXX,V
            REAL (kind=double)  ,dimension(3) :: R
            INTEGER :: nn = -7, m_sli=-13	, fnodeRowIndex=-1, sixnoderef=0
            type (doforef) , pointer, dimension(:) :: m_pdofo => NULL() !dont forget to unhook when we delete the fnode

            logical :: m_isunderdofocontrol = .false.
            integer :: masterdofoNo=-1

            integer(kind=cptrsize) :: m_RXFEsiteptr=0 , m_rxeptr=0, m_connectSurf=0 ,m_slider=0

            REAL (kind=double) ,dimension(3,3) ::   B,XM
            REAL (kind=double) ,dimension(3) ::   m_Nodal_Load =0!  ! March 31 2008 for nets.
            REAL (kind=double) ,dimension(3) ::P = 0.0,Rold =0.0, m_Nnor =0;
            REAL (kind=single) :: S_angle=0, Errindex=0 , G_angle=0

            logical :: m_Nused=.false.
            logical :: m_trace =.false.
            integer :: m_ElementCount=0;
            ! type ElRef,pointer, dimension(:) :: m_els =>NULL()
            ! goes with         call AddToNodesElRefs(flinks%list(NN)%m_e2,RXFLINK,nn,2)
#ifndef NOOMP
            INTEGER (KIND=OMP_LOCK_KIND)::m_lock  ! to protect member r
#endif
	END TYPE Fnode

	TYPE :: nodelist
            TYPE (Fnode), DIMENSION(:), POINTER :: xlist
            INTEGER :: Ncount, block =1000
            TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE nodelist
	TYPE :: Fnodeptr
	    type (Fnode),pointer :: m_fnpe=>NULL()
	end TYPE Fnodeptr

! dofo_spline was here
	TYPE :: Fedge
		!  unstressed arclength is 	e%m_ZLINK0/ e%ChOverArcInit if we are running /CV
		! else just e%m_ZLINK0
		REAL (kind=double) ,dimension(3)::m_DLINK
		REAL (kind=double)  ::m_DELTA0, m_ZLINK0 , m_TLINK
		real(kind=double) :: ChOverArcInit =1.0d00
		INTEGER ,dimension(2) :: m_L12
		TYPE(Fnode) ,POINTER ::  m_ee1=>null() ,m_ee2=>null() 
		type(TriRef), dimension(2) :: m_tris
		INTEGER :: N 
		integer(kind=cptrsize) ::Cedgeptr =0
		LOGICAL :: IsOnSurf =.false.
		LOGICAL :: m_Eused
	END TYPE Fedge
	TYPE :: edgelist
		TYPE (Fedge), DIMENSION(:), POINTER :: list =>NULL()
		INTEGER :: count=0
                INTEGER :: block   =3000
		TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE edgelist

	TYPE :: FedgeRef
	    type (Fedge),pointer :: m_fepe
	end TYPE FedgeRef
	
	TYPE :: Flink 
		REAL (c_double)  ::m_EA =0, m_TLINK =-1, m_Zlink0=-1,m_ti=0,  m_mul=0 ! mass/unit length, analysis units
		REAL (c_double),dimension(3)  ::	m_d =0
		TYPE(Fnode) ,POINTER ::  m_e1 =>NULL(),m_e2 =>NULL()
		INTEGER(c_int) :: N  =-1
		Integer(c_int) :: IsTensionControlled = 0  ! 0 means tq = ti + EA*eps.   only used in SUBROUTINE JanetSpecial 
		                                           ! 1 means it thinks it is constant tension. but need to check if its getting short.
		                                           ! 2  means it thinks it is 'constant tension'. but is short so it has finite modulus	                                           
	 	integer(kind=cptrsize) ::m_flptr ;  ! ptr to the 
		LOGICAL(c_bool) :: m_used =.false.
	END TYPE Flink

	TYPE :: flinklist
		TYPE (Flink), DIMENSION(:), POINTER :: list 
		INTEGER :: count, block  =10000
		TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE flinklist

	TYPE :: beamelement
		logical :: IsUpright, IsBar	,m_Currently_Active,m_Used= .false. 
                integer :: L, n3, npo,N=0, axLoad=0  !axload >0 = tension only. <0 = compression only
		integer, dimension(2) :: n12=-1

                REAL  (kind=double) ::zt, zi, tq,ti, beta1,m_BEMass=0
		REAL  (kind=double) ,dimension(3,3) :: ttu=0
		REAL  (kind=double) ,dimension(5) :: emp=0   ! ea, ei2,ei3,gj,ti
                REAL  (kind=double) :: rotstiff=100.0

		type(fnode), pointer  :: xN1=>NULL() ,xN2=>NULL(),tn1=>NULL(),tn2 =>NULL()

		character*64 m_part_name
	END TYPE beamelement	
	
	
	TYPE :: fbeamlist
		TYPE (beamelement), DIMENSION(:), POINTER :: list =>NULL()
                INTEGER :: count=0, fblblock  =5000
		TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE fbeamlist	
	
	TYPE :: tri
		REAL (kind=double) ,dimension(3):: m_eps, tedge, m_stress
		REAL (kind=double) ,dimension(3,3) :: DSTFold	! unwrinkled material matrix
		REAL (kind=double) ,dimension(3,3) :: G    		! strain from edge lengths

		REAL (kind=double) ,dimension(3)   :: m_ps =0, m_E0 =0, m_tnorm = 0
		REAL (kind=double)  ::THETAE_rad =0, m_AREA=0, PREST=0
		REAL (kind=double)  ::CurrentArea, Ustiff, wr_angle=0
		INTEGER,dimension(4) ::  N13
		INTEGER,dimension(3) ::  edgeNo			
		type(fedgeRef), dimension(3) :: m_er
		type(Fnodeptr), dimension(4) :: nr 
		LOGICAL ::  creaseable=.true. ,used	
		LOGICAL ::  use_tri =.true.  !  integer*4  flag to use / not use triangle	
		INTEGER :: N 
		INTEGER ::tribuckled =0, wr_state=0
		INTEGER :: Ccount  =0 ! count since last state change
		integer(kind=cptrsize) :: CtriPtr	 =0
	END TYPE   tri
	TYPE :: trilist
		TYPE (tri),  DIMENSION(:), POINTER :: list 
                INTEGER :: count, block      =2500
		TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE trilist	

	type :: stringref
	sequence
		integer :: sli	! the sail list index of the model holding this string
		integer :: n	! the index of the string in the model
	end type stringref
 
	TYPE :: Nconntype
		INTEGER  :: e	! the edge index in the master string
		INTEGER :: 	m_ncn,m_ncsli	! the slave node no
		
		REAL(kind=double)::  t	! parameter from edge end1 to edge end 2 (0-1)
! NOTE t is in the sense of the edge LIST. If one edge is reversed, t will be 1 at end0 and 0 at end1
		integer :: kkv		! set to the vertex we want to KerKlunk to 
		type (stringref) :: sr ! the model and index		
	END TYPE Nconntype


TYPE :: Connect
                integer ::  ConnectRank = 99999  ! user sets this; it determines how the node-pairs are used for the model transformations.
		LOGICAL ::  used
  		LOGICAL ::  resolved
                integer(kind=cptrsize) :: Cptr =0 ! RXEntityDefault*
		
		INTEGER :: type1, type2, m_node1,m_node2
		INTEGER :: m_sli1 =0,m_sli2 =0  !we can set these in resolve_connect
		LOGICAL :: m_Sliding=.false., m_IsSpline=.false.
		LOGICAL :: m_Interpolatory = .false.
                integer :: m_SplineDegree = 3
		TYPE (Nconntype), DIMENSION(:), pointer  :: slavelist =>NULL()

		TYPE (string ), POINTER :: s1 ,  s2  ! 1 is master, 2 is slave 
		CHARACTER (len=256) :: Mname, string	
		CHARACTER (len=32) model1,model2, ent1,ent2 	
	
#ifdef NO_DOFOCLASS
             type (dofo),pointer ::m_cdofo =>NULL() ! or class
#else
	    class (dofo),pointer ::m_cdofo =>NULL()
#endif

END TYPE Connect  

TYPE :: ConnectHolder
		TYPE (Connect), POINTER :: o 
END TYPE ConnectHolder

TYPE :: ConnectList
		TYPE (ConnectHolder), DIMENSION(:)  ,POINTER :: list 
		INTEGER :: count, block =24
END TYPE ConnectList

! the string datatype is split up so that it can be accessed both from fortran and from C
! the scalar variables are grouped into mystringdata which is mirrored by the C struct StringContents 
! the idea is that memory for each of the first 5 members of type::string is provided separately by C.
!  we use the function Hook_String_Memory to construct a string  derived type. 
!  We can then read and write itsmembers from both languages.

! for values of  m_CanBuckle3 See RX_FEString.h
    TYPE, bind(c) :: mystringdata   ! must match struct StringContents 
        REAL (c_double)  :: m_zisLessTrim ! initial L from edges or card. Without Trim
        REAL (c_double)  :: EAS
        REAL (c_double)  :: TIS ,m_trim, m_Mass	,m_zt_s
        integer (C_Int) ::  m_TCnst   ! means that EA is zero so tension is always TI
        integer (C_Int) ::  m_Slider  ! means that the tension is uniform along the string: intermediate nodes slide
        INTEGER (c_int) ::  m_CanBuckle3  ! strongly negative = never buckle, zero = depends on flag, positive=always
        INTEGER (kind=cptrsize) :: m_Scptr ! RX_FEString or RX_FEBeam
        INTEGER (kind=cptrsize) :: m_ExpCptr  ! not used
        INTEGER (c_int) ::   Ne
        REAL (c_double)  :: m_tqOut
    END TYPE mystringdata
    
    type :: string 
        type (mystringdata),pointer ::d     ! shared with C
        INTEGER (C_Int),dimension(:), pointer :: se =>null()
        logical(C_Int), dimension(:), pointer :: rev =>null()
        real(c_double),dimension(:), pointer  :: tq_s  =>null()
        CHARACTER(len=256)   :: m_text = " "

        type (connectholder) , pointer,dimension(:) :: m_Connects =>Null()
        class (fsplineI) , pointer :: m_fspline =>Null()
        type (fnoderef_p) , pointer, dimension(:) :: m_pts=>NULL()

        INTEGER(C_Int) :: N_connect =0
        INTEGER (c_int) :: Nn, m_sli
        logical :: used = .false.
     end type string

	TYPE :: stringholder
		TYPE (string) ,POINTER :: o 
	END TYPE  stringholder

	TYPE :: stringlist
		TYPE (stringholder), DIMENSION(:)  ,POINTER :: list 
		INTEGER :: count, SLblock ! =100
		TYPE (linklist) ,POINTER :: freelist =>NULL()
	END TYPE  stringlist



	TYPE :: doforef
#ifdef NO_DOFOCLASS
            type (dofo),pointer ::d =>NULL() ! or class
#else
	   class (dofo),pointer ::d =>NULL()
#endif
             integer ::  m_rowno = 1  ! the number of the first row referring to this node in the dofo's jacobi
             integer :: m_dflg =0
	end TYPE doforef

    abstract interface
     function FSplineJMatrix(this,d,origin,ss) result(j)
         USE  basictypes_f
         import :: fsplineI
         import :: dofo
         class (fsplineI),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout) ::d  ! or class
#else
           class (dofo),intent(inout) ::d ! or type
#endif
         real(kind=double) ,intent(in) ::ss
         real(kind=double) ,intent(inout),dimension(:) ::origin
         real(kind=double), pointer, dimension(:,:):: J
       end   function  FSplineJMatrix
       function FSplineJMatrixMultiple(this,d,origin,ss) result(j)
            USE  basictypes_f
            import :: fsplineI
            import :: dofo
            class (fsplineI),intent(in) :: this
#ifdef NO_DOFOCLASS
           type (dofo),intent(inout) ::d  ! or class
#else
           class (dofo),intent(inout) ::d ! or type
#endif
            real(kind=double),dimension(:) ,intent(in) ::ss
            real(kind=double) ,intent(inout),dimension(:) ::origin
            real(kind=double), pointer, dimension(:,:):: J
        end   function  FSplineJMatrixMultiple
          function ListFspline(this,u,fullprint) result(ok)
                import :: fsplineI
                class (fsplineI) :: this
                integer,intent(in) ::u
                logical, intent(in),optional :: fullprint
                integer:: ok
          end  function ListFspline
          RX_PURE recursive function FsplineEvaluate(this,s) result(x) ! linear interpolation outside the domain
                USE  basictypes_f
                import :: fsplineI
                implicit none
                class (fsplineI), intent(in) ::this
                real(kind=double), intent(in) :: s
                real(kind=double),dimension(3)  :: x
        end function FsplineEvaluate
        RX_PURE  function derivativebyds(this,sin) result(x)
                USE  basictypes_f
                import :: fsplineI
                implicit none
                class (fsplineI), intent(in) ::this
                real(kind=double), intent(in) :: sin
                real(kind=double),dimension(3)  :: x
         end  function derivativebyds
         function FindNearest(this,q) result(t)
                 USE  basictypes_f
                 import :: fsplineI
                 implicit none
                 class (fsplineI), intent(in) ::this
                 real(kind=double),dimension(:) ,intent(in) ::q
                 real(kind=double) ::t
          end function FindNearest
          subroutine ClearFspline(this)
              USE  basictypes_f
              import :: fsplineI
              implicit none
              class (fsplineI)  ::this
          end subroutine ClearFspline


    end interface

END MODULE  ftypes_f





