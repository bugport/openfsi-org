!
! prototypes for the dofo functions which cannot be in module dofo_f
! NOR IN module dofolist_f
! because that would make a recursive 'use'

module dofoprototypes_f
   use ftypes_f
   use basictypes_f
INTERFACE

function Complete_rigidBodyDOFO(d, theNodes,atts )  result (ok)
  use ftypes_f
#ifdef NO_DOFOCLASS
       type (dofo),pointer ::d ! or class
#else
      class (dofo),pointer ::d
#endif
   integer, dimension(:) ,intent(in) ::  theNodes
       character(len=*), intent(in) :: atts
    integer ::  ok
end function Complete_rigidBodyDOFO

!recursive function CreateChildDofo(theNode)  result(ok)
!use ftypes_f
 !   type(fnode),intent(in) ::theNode
!    integer::ok
!end function CreateChildDofo


function Complete_NodeSplineDOFO(d, p_theNode,spl)  result (ok)
    use ftypes_f
    implicit none
#ifdef NO_DOFOCLASS
     type(dofo),pointer ::d  ! or class
#else
    class(dofo),pointer ::d
#endif
    type(fnode), pointer,intent(in) :: p_theNode 
    class(fsplineI), pointer ::spl
    integer :: ok 
end function Complete_NodeSplineDOFO

function Complete_StringSplineDOFO(d, theConn,spl)  result (ok)
    use ftypes_f
    implicit none
#ifdef NO_DOFOCLASS
     type (dofo),pointer ::d  ! or class
#else
    class (dofo),pointer ::d
#endif
    TYPE (Connect),POINTER,intent(inout) :: theConn

    class(fsplineI), pointer,intent(inout) ::spl
    integer :: ok 
end function Complete_StringSplineDOFO

#ifdef NeVEr
function DOFOCalcLinearT(d) result (ok) !  verified. estimates T via least-squares from X
   use ftypes_f
    implicit none
#ifdef NO_DOFOCLASS
             type (dofo),pointer,intent(inout) ::d  ! or class
#else
            class (dofo),pointer,intent(inout) ::d
#endif
    integer :: ok 
end function DOFOCalcLinearT
#endif
recursive function SetParentTeesBySV(d,IsRecursive) result(ok)
    use ftypes_f
#ifdef NO_DOFOCLASS
             type (dofo),pointer,intent(inout) ::d ! or class
#else
            class (dofo),pointer,intent(inout) ::d
#endif
    logical, intent(in) :: IsRecursive
    integer::ok
end function SetParentTeesBySV

function CreateDOFO(sli, p_dofotype ,err) result (d) ! from dofolist
	USE ftypes_f
	integer, intent(in)		:: sli	! sailListIndex
	integer, intent(in)		:: p_dofotype 
	integer, intent(out)	:: err
#ifdef NO_DOFOCLASS
             type (dofo),pointer  ::d ! or class
#else
            class (dofo),pointer  ::d
#endif
end function CreateDOFO

elemental function IsUnderDOFOControl(nn) result (yes)
use ftypes_f
    implicit none
    type(Fnode), intent(in) :: nn   
    logical yes
end function IsUnderDOFOControl

function  DOFO_TestForCapture(kit) result (ok) !MUST be consistent with dofo.f
        integer, intent(in) :: kit
        integer :: ok
END FUNCTION  DOFO_TestForCapture     
      
function  DOFO_UpdateJay( ) result (ok) !MUST be consistent with dofo.f
        integer :: ok
END FUNCTION DOFO_UpdateJay
function  DOFO_TestForRelease( ) result (ok) !MUST be consistent with dofo.f
        integer :: ok
END FUNCTION DOFO_TestForRelease  
RX_PURE function  Get_DOFO_Cartesian_X(p_N,r) result (rv) !MUST be consistent with dofo.f
         use ftypes_f  
         type(Fnode),intent(in) :: p_N;
         integer, intent(in)    ::r  ! normally  p_N%masterdofo
        real(kind=double), dimension(3)	:: rv
END function  Get_DOFO_Cartesian_X
pure function  Get_DOFO_Cartesian_V(p_N) result (rv) !MUST be consistent with dofo.f
         use ftypes_f  
         type(Fnode),intent(in) :: p_N;
        real(kind=double), dimension(3)	:: rv
END function  Get_DOFO_Cartesian_V
pure function  Get_DOFO_Cartesian_R(p_N) result (rv) !MUST be consistent with dofo.f
         use ftypes_f  
         type(Fnode),intent(in) :: p_N;
        real(kind=double), dimension(3)	:: rv
END function  Get_DOFO_Cartesian_R



function DeleteAllDOFOs(sli,err) result(ok) !see prototype in dofoprototypes.f
	    INTEGER , intent(in) :: sli
	    INTEGER , intent(inout) :: err
	    integer :: ok
    END FUNCTION
    
function define_All_DOFO_masses() result(ok)
        INTEGER :: ok 
end function define_All_DOFO_masses

function Integrate_DOFO_Motion(starting,ekt,dbg) result(ok) ! all models
        use basictypes_f
        real(kind=double), intent(inout) :: ekt
        logical, intent(in) ::starting
        logical, intent(in) , optional ::dbg
        integer::ok
end function Integrate_DOFO_Motion

function Backtrack_DOFO_Motion(dbg) result(ok)
         logical, intent(in) , optional ::dbg
        integer::ok 
end function Backtrack_DOFO_Motion

subroutine Max_DOFO_Residuals(smax,nmax, rmax)
use basictypes_f
! places the highest R in rmax
! records the node with the highest R as (nmax,smax)

    integer, intent(out) :: smax,nmax  ! SLI and node number of the node with highest R. Need not be in this model
    real(kind=double),intent(out),dimension(3) :: rmax
end subroutine Max_DOFO_Residuals

    
function DOFO_WriteAsFixings(sli,u)  result(ok)
        implicit none
        integer, intent(in) ::sli,u 
        integer :: ok
end function DOFO_WriteAsFixings 
   
function Complete_SlideFixedSurfaceDOFO(d, nn,newptuvw, remTimStep)  result(OK)
        use ftypes_f
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::d  ! or class
#else
        class (dofo),pointer ::d
#endif
        type(fnode) :: nn ! was ptr may 2008
        real(kind=double), intent(in), dimension(:) :: newptuvw
        real(kind=double), intent(in) :: remTimStep
        integer :: ok
end  function Complete_SlideFixedSurfaceDOFO  
    
function Complete_SlideFixedCurveDOFO(d, nn,u)  result(OK)
        use ftypes_f
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::d  ! or class
#else
        class (dofo),pointer ::d
#endif
        type(fnode) :: nn 
        real(kind=double), intent(in) :: u
        integer :: ok
end  function Complete_SlideFixedCurveDOFO 
function Complete_NNCDOFO(d, n1,n2)  result(OK)
        use ftypes_f
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::d  ! or class
#else
        class (dofo),pointer ::d
#endif
        type(fnode)  :: n1,n2
        integer :: ok
end function Complete_NNCDOFO
 
function dofo_from_fixingstring(sli,theNode, line) result(error)
        use ftypes_f
         integer,intent(in) ::sli  
        type(fnode) ::theNode 
        character(len=*) , intent(in) ::line
        integer Error 
end function dofo_from_fixingstring  

function dofoCompositeJacobian(dnew)  result(ok)
        use ftypes_f

#ifdef NO_DOFOCLASS
         type (dofo),pointer ::dnew ,d1,d2  ! or class
#else
        class (dofo),pointer ::dnew ,d1,d2
#endif 
        integer :: ok
end function dofoCompositeJacobian

recursive function DestroyDofo (sn, NN)  result(err) bind(C,name="cf_delete_dofo")
    USE, INTRINSIC :: ISO_C_BINDING
         INTEGER (c_int), intent(in),value :: sn,nn
         INTEGER (C_INT) :: err
end function DestroyDofo
	
function TransformDofo(d,mt) 	result(err)
    use ftypes_f
    use basictypes_f
    IMPLICIT NONE  
#ifdef NO_DOFOCLASS
         type (dofo),pointer ::d  ! or class
#else
        class (dofo),pointer ::d
#endif
	REAL (kind=double), dimension(4,4):: mt,imt 
  	INTEGER   :: err
end  function TransformDofo

recursive function ComputeDOFOJacobian(d) result(ok)
    use ftypes_f
    implicit none
#ifdef NO_DOFOCLASS
         type (dofo),pointer, intent(inout) ::d  ! or class
#else
        class (dofo),pointer, intent(inout) ::d
#endif
    type(fnode), pointer :: thenode
    integer ::ok 
end  function ComputeDOFOJacobian

subroutine CollectDofosForPull(ll, err)
    use ftypes_f
    IMPLICIT NONE
       TYPE (dofolist),intent(inout) :: ll
       INTEGER ,intent(out) :: err
end      subroutine CollectDofosForPull

function DOFO_commonNodes(d1,d2, nn1,nn2) result (k) ! nn1,nn2 are indices in the dofos %m_fnodes
use ftypes_f
implicit none
#ifdef NO_DOFOCLASS
         type (dofo), intent(in) :: d1,d2  ! or class
#else
        class (dofo), intent(in) :: d1,d2
#endif
    integer, pointer, dimension(:), intent(out):: nn1,nn2
    integer k
end function DOFO_commonNodes

RX_PURE  function  Dofo_Evaluate(d, row ) result (rv) ! row is (eg)  p_N%m_pdofo(k)%m_rowno
use ftypes_f
use basictypes_f
#ifdef NO_DOFOCLASS
             type (dofo),intent(in) ::d  ! or class
#else
            class (dofo),intent(in) ::d
#endif
    integer, intent(in) ::row
    real(kind=double), dimension(3)	:: rv
    end  function  Dofo_Evaluate
end INTERFACE
end module dofoprototypes_f
