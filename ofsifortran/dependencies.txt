

# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)bar_elements.o: bar_elements.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)basictypes.o: basictypes.f90 


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)batn_els.o: batn_els.f90   basictypes.o  batstuf.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)batstuf.o: batstuf.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)batten.o: batten.f90   basictypes.o  batn_els.o  batstuf.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)battenf90.o: battenf90.f90   basictypes.o  batn_els.o  batstuf.o  batten.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  stiffness.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)beamelementlist.o: beamelementlist.f90   basictypes.o  beamElementMethods.o  beamelementtype.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  hoopsinterface.o  invert.o  linklist.o  nodalrotations.o  nodelist.o  parse.o  realloc.o  rlxflags.o  saillist.o  sf_vectors.o  stiffness.o  tanpure.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)beamElementMethods.o: beamElementMethods.f90   basictypes.o  beamelementtype.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  invert.o  linklist.o  nodalrotations.o  parse.o  realloc.o  rlxflags.o  saillist.o  sf_vectors.o  tanpure.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)beamelementtype.o: beamelementtype.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)bspline.o: bspline.f90 basictypes.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)cfromf_declarations.o: cfromf_declarations.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)connects.o: connects.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  fglobals.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gletypes.o  linklist.o  links_pure.o  math.o  mathconstants.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  saillist.o  stiffness.o  stringlist.o  stringsort.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)connectstuf.o: connectstuf.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)coordinates.o: coordinates.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)dofoarithmetic.o: dofoarithmetic.f90   basictypes.o  dofo_subtypes.o  dofoprototypes.o  ftypes.o  linklist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)dofo.o: dofo.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofolist.o  dofoprototypes.o  edgelist.o  fglobals.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gletypes.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  saillist.o  stiffness.o  stringlist.o  stringsort.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)dofolist.o: dofolist.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  fglobals.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gletypes.o  linklist.o  links_pure.o  math.o  mathconstants.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  saillist.o  stiffness.o  stringlist.o  stringsort.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)dofoprototypes.o: dofoprototypes.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)dofo_subtypes.o: dofo_subtypes.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)edgelist.o: edgelist.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)f90_to_c.o: f90_to_c.f90   bar_elements.o  basictypes.o  batn_els.o  batstuf.o  batten.o  battenf90.o  beamElementMethods.o  beamelementlist.o  beamelementtype.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  feaprint.o  fglobals.o  flinklist.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gle.o  gletypes.o  hoopsinterface.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  nastraninterface.o  nodalrotations.o  nodelist.o  nonlin.o  parse.o  plotps.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  saillist.o  sf_vectors.o  stiffness.o  stringlist.o  stringsort.o  tanpure.o  trilist.o  tris.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)feaprint.o: feaprint.f90   bar_elements.o  basictypes.o  batn_els.o  batstuf.o  batten.o  battenf90.o  beamElementMethods.o  beamelementlist.o  beamelementtype.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  fglobals.o  flinklist.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gle.o  gletypes.o  hoopsinterface.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  nodalrotations.o  nodelist.o  nonlin.o  parse.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  saillist.o  sf_vectors.o  stiffness.o  stringlist.o  stringsort.o  tanpure.o  tris.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)fglobals.o: fglobals.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)flagset.o: flagset.f90   basictypes.o  dofo_subtypes.o  fglobals.o  ftypes.o  linklist.o  rlxflags.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)flinklist.o: flinklist.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  hoopsinterface.o  linklist.o  links_pure.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  stiffness.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)flopen.o: flopen.f90 basictypes.o  parse.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)fnoderef.o: fnoderef.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)Fslidecurve.o: Fslidecurve.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)fspline.o: fspline.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  fglobals.o  fnoderef.o  ftypes.o  gletypes.o  linklist.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)fstrings.o: fstrings.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  links_pure.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  stringsort.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)ftypes.o: ftypes.f90   basictypes.o  dofo_subtypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)gle.o: gle.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)gletypes.o: gletypes.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)gravity.o: gravity.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  flinklist.o  ftypes.o  gletypes.o  hoopsinterface.o  linklist.o  links_pure.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  stiffness.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)hoopsinterface.o: hoopsinterface.f90 


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)invert.o: invert.f90 basictypes.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)isnan.o: isnan.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)kbat_els.o: kbat_els.f90   basictypes.o  batstuf.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)kbatten.o: kbatten.f90   basictypes.o  batstuf.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  invert.o  kbat_els.o  linklist.o  links_pure.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)linklist.o: linklist.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)links_pure.o: links_pure.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  math.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)mathconstants.o: mathconstants.f90 


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)math.o: math.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  mathconstants.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)msvc_fortran_utils.o: msvc_fortran_utils.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)mtkaElement.o: mtkaElement.f90 


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)nastraninterface.o: nastraninterface.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)nlstuf.o: nlstuf.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  fglobals.o  ftypes.o  invert.o  linklist.o  mathconstants.o  nonlin.o  rlxflags.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)nodalrotations.o: nodalrotations.f90   basictypes.o  dofo_subtypes.o  fglobals.o  ftypes.o  invert.o  linklist.o  sf_vectors.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)nodelist.o: nodelist.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)nograph.o: nograph.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)nonlin.o: nonlin.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  fglobals.o  ftypes.o  invert.o  linklist.o  mathconstants.o  rlxflags.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)oonatristuf.o: oonatristuf.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  nonlin.o  parse.o  realloc.o  rlxflags.o  saillist.o  tris.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)parse.o: parse.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)plotps.o: plotps.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  hoopsinterface.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  nonlin.o  parse.o  realloc.o  rlxflags.o  saillist.o  tris.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)realloc.o: realloc.f90   basictypes.o  dofo_subtypes.o  ftypes.o  gletypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)relax32.o: relax32.f90   bar_elements.o  basictypes.o  batn_els.o  batstuf.o  batten.o  battenf90.o  beamElementMethods.o  beamelementlist.o  beamelementtype.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  feaprint.o  fglobals.o  flinklist.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gle.o  gletypes.o  gravity.o  hoopsinterface.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  mtkaElement.o  nodalrotations.o  nodelist.o  nograph.o  nonlin.o  parse.o  realloc.o  relaxsq.o  removeelement.o  rlxflags.o  rxfoperators.o  rximplicitsolution.o  saillist.o  sf_tris_pure.o  sf_vectors.o  shapegen.o  stiffness.o  stringlist.o  stringsort.o  tanpure.o  tris.o  ttmass9.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)relaxsq.o: relaxsq.f90   bar_elements.o  basictypes.o  batn_els.o  batstuf.o  batten.o  battenf90.o  beamElementMethods.o  beamelementlist.o  beamelementtype.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  feaprint.o  fglobals.o  flinklist.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gle.o  gletypes.o  gravity.o  hoopsinterface.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  mtkaElement.o  nodalrotations.o  nodelist.o  nograph.o  nonlin.o  parse.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  rximplicitsolution.o  saillist.o  sf_tris_pure.o  sf_vectors.o  stiffness.o  stringlist.o  stringsort.o  tanpure.o  tris.o  ttmass9.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)removeelement.o: removeelement.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)rlxflags.o: rlxflags.f90   basictypes.o  dofo_subtypes.o  fglobals.o  ftypes.o  linklist.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)rxfoperators.o: rxfoperators.f90 


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)rximplicitsolution.o: rximplicitsolution.f90   bar_elements.o  basictypes.o  batn_els.o  batstuf.o  batten.o  battenf90.o  beamElementMethods.o  beamelementlist.o  beamelementtype.o  cfromf_declarations.o  connects.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  feaprint.o  fglobals.o  flinklist.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gle.o  gletypes.o  gravity.o  hoopsinterface.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  mtkaElement.o  nodalrotations.o  nodelist.o  nograph.o  nonlin.o  parse.o  realloc.o  removeelement.o  rlxflags.o  rxfoperators.o  saillist.o  sf_tris_pure.o  sf_vectors.o  stiffness.o  stringlist.o  stringsort.o  tanpure.o  tris.o  ttmass9.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)saillist.o: saillist.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)sf_tris_pure.o: sf_tris_pure.f90 basictypes.o  mathconstants.o  sf_vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)sf_vectors.o: sf_vectors.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)shape.o: shape.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)shapegen.o: shapegen.f90   bar_elements.o  basictypes.o  batn_els.o  batstuf.o  batten.o  battenf90.o  cfromf_declarations.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  fglobals.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gletypes.o  invert.o  kbat_els.o  kbatten.o  linklist.o  links_pure.o  math.o  mathconstants.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  saillist.o  stiffness.o  stringlist.o  stringsort.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)solvecubic.o: solvecubic.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)solve.o: solve.f90 


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)spline.o: spline.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)sspline.o: sspline.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)stiffness.o: stiffness.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)stringlist.o: stringlist.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  connectstuf.o  coordinates.o  dofo_subtypes.o  dofoarithmetic.o  dofoprototypes.o  edgelist.o  fglobals.o  fnoderef.o  fspline.o  fstrings.o  ftypes.o  gletypes.o  linklist.o  links_pure.o  math.o  mathconstants.o  nodelist.o  parse.o  realloc.o  removeelement.o  rlxflags.o  saillist.o  stiffness.o  stringsort.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)stringsort.o: stringsort.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)tanpure.o: tanpure.f90   basictypes.o  beamelementtype.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  invert.o  linklist.o  nodalrotations.o  parse.o  realloc.o  rlxflags.o  saillist.o  sf_vectors.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)tohoops.o: tohoops.f90   basictypes.o  cfromf_declarations.o  dofo_subtypes.o  dofoprototypes.o  fglobals.o  ftypes.o  gletypes.o  linklist.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)trilist.o: trilist.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  nonlin.o  parse.o  realloc.o  rlxflags.o  saillist.o  tris.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)tris.o: tris.f90   basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  ftypes.o  gletypes.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  nonlin.o  parse.o  realloc.o  rlxflags.o  saillist.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)tris_pure.o: tris_pure.f90 basictypes.o  mathconstants.o  vectors.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)ttmass9.o: ttmass9.f90   bar_elements.o  basictypes.o  cfromf_declarations.o  coordinates.o  dofo_subtypes.o  dofoprototypes.o  edgelist.o  fglobals.o  flinklist.o  ftypes.o  gletypes.o  gravity.o  hoopsinterface.o  invert.o  linklist.o  links_pure.o  math.o  mathconstants.o  mtkaElement.o  nonlin.o  parse.o  realloc.o  rlxflags.o  saillist.o  sf_tris_pure.o  sf_vectors.o  stiffness.o  tris.o  vectors.o  wrinkleFromMathematica.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)vectors.o: vectors.f90 basictypes.o  


# +++++++++++++++++++ START MODULE Dependencies +++++++++++++++++++++++
$(OBJECTS_DIR)wrinkleFromMathematica.o: wrinkleFromMathematica.f90   basictypes.o  dofo_subtypes.o  ftypes.o  linklist.o  mathconstants.o  
