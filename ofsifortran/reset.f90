SUBROUTINE reset_edges(sli) bind(C,name="cf_reset_edges" )
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f  ! for outputunit
    USE ftypes_f
    USE saillist_f
    use solveimplicit_f, only :Make_Current_Edge_Lengths
    IMPLICIT NONE
    INTEGER  (c_int) , intent(in), value        :: sli
!locals

    integer(c_int) :: OK
    real*8 tot

    TYPE ( trilist) ,POINTER :: tris
    type(tri), pointer ::t
    integer:: k,n,count
    OK=0
    tris=>saillist(sli)%tris
   tot= 0; count=0
      CALL Make_Current_Edge_Lengths(sli) !updates DLINK and delta0
      DO N=1,tris%count
       t=> tris%list(N)
         if(t%use_tri ) then
            do k=1,3
                t%m_er(k)%m_fepe%m_Zlink0 =    t%m_er(k)%m_fepe%m_Zlink0 +   t%m_er(k)%m_fepe%m_delta0
                tot = tot + abs(  t%m_er(k)%m_fepe%m_delta0)
                t%m_er(k)%m_fepe%m_delta0= 0.0

            enddo
            count=count+1
         endif
      ENDDO
      write(outputunit,*)  "ResetEdges processed " , count," 'elements, total change =  ",tot
END SUBROUTINE reset_edges

SUBROUTINE Shrink_edges(sli)  bind(C,name="cf_shrink_edges" )
USE, INTRINSIC :: ISO_C_BINDING
use basictypes_f  ! for outputunit
USE ftypes_f
USE saillist_f
USE tris_f
use nonlin_f
use invert_f
IMPLICIT NONE
    integer(C_INT), intent(in), value :: sli
    TYPE ( trilist) ,POINTER :: tris
    type(edgelist), pointer :: edges
    type(tri), pointer ::t
    type (fEdge), pointer :: e
    integer:: k,n, nedges,count
    REAL(kind=double), allocatable, dimension(:) ::New_Length
    REAL*8 A(3,3),de,tot
    LOGICAL error

    tris=>saillist(sli)%tris
    edges => saillist(sli)%edges
    count= 0; tot=0;


!locals
! Purpose of routine To reset the edge lengths  of a run including prestress
!  so that next time, a run with  prestress zero would give the same result.
! NOTE

! Method
! 1) using the current stress
! 2) Eps = dstfold(inv) x stress
! 3) delta edges =   G (transposed) * eps
! 4) put the new edge lengths in a local array.
! 5) At the end, copy these edge lengths into ZLINKo 
      
    nedges = edges%count
    allocate(New_Length(nedges))
    DO N=1,edges%count
       New_Length(N) = edges%list(n)%m_ZLINK0
    ENDDO
  DO N=1,tris%count
     t=> tris%list(N)
     if(t%use_tri ) then
        CALL INVERT3(t%dstfold,A,error)
        if(error) THEN
                write(outputunit,*)' tri', N, ' no DSTF'
        ELSE
           t%m_eps = matmul(a,t%m_stress)
           CALL INVERT3(t%G ,A,error)
           if(error) THEN
                  write(outputunit,*)' tri', N, ' no G'
            ELSE
               DO k=1,3
               ! de = DotProduct(A(K,1:3), t%m_eps;
                   DE=A(K,1)*t%m_eps(1)+A(K,2)*t%m_eps(2)+A(k,3)*t%m_eps(3)
                  e=>t%m_er(k)%m_fepe
                  New_Length(e%N) = e%m_Zlink0 - de
                  tot = tot + abs(de); count=count+1
               ENDDO
          ENDIF
       ENDIF
      endif
    ENDDO
  !  forall(k=1:3) d(k) = t%m_er(k)%m_fepe%m_delta0
  !  t%m_eps = matmul(t%g,d)
  !  call Get_NL_Stress_From_Tri(t,t%m_eps,t%m_stress,crease,state)
  !  tedge = matmul(transpose(t%G),t%m_stress) * t%m_Area



  DO N=1,edges%count
      edges%list(n)%m_ZLINK0 = New_Length(N)
  ENDDO
  deallocate(New_Length)
  write(*,*) "Shrink  nchanges = ",count, " totalChange=",tot
!  call areacalc(sli)
!10     	FORMAT(a,i5,3g13.5)
END SUBROUTINE Shrink_edges
