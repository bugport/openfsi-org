module gravity_f
integer, parameter :: GNOPRINT=2
integer, parameter :: GPRINT=4
integer, parameter :: GTEST =8
integer, parameter :: GWITHV =16
 

! 20 april 2008  verified the calc and added the function for sphere drag.
!  We should verify the Cd/Re plots from printout. 
! we have verified the calc by inspection.  Now need to verify it by printout (/HC)


contains


function Cylinder_Cd(re) result(cd) ! only valid sub-critical
use basictypes_f
implicit none
	real(kind=double), intent(in) :: re
	real(kind=double)	::	cd,x,y

	if(re > 300000) then
		cd = 1.0
		return
	else if( re < 1.0) then
		cd = 0.01
		return
	endif
	x = log10(re)
	y =-0.005758076301566283*(-14.631090730269687 + x)*(-4.553729871788093 + x)*(-2.591336590752562 + x);
! was	 -0.0148 * x**4 + 0.1302 * x**3 -0.233 * x**2 - 0.5026 * x + 1.2863
	cd = 10.0 ** y
end function Cylinder_Cd

function Sphere_Cd(re) result(cd) ! accurate( 2%) sub-critical.  Overestimate supercritical
use basictypes_f
implicit none
	real(kind=double), intent(in) :: re
	real(kind=double)	::	cd,x,y

	if( re < 0.10) then
		cd = 0.01
		return
	endif

	x = log10(re)
	if(x>4.5) x=4.5
	y  =  0.002147069623537581*(-5.702221647408095 + x)*(-2.1163126165864563 + x)*(55.62036065641352 + x)
	cd = 10.0 ** y
end function Sphere_Cd

function windageOnCylinder( e1,e2,dia,meancd,no,what, unit) result   (forces)
	use basictypes_f
	use vectors_f
	use cfromf_f
 	
    implicit none
    real(kind=double), intent(in), dimension(:) ::  e1,e2  ! coords at end
    real(kind=double), intent(inout) :: meancd,no  
    real(kind=double), intent(in)   ::  dia 
    integer,intent(in) :: unit,what 
    
    
    real(kind=double), dimension(6), target :: forces
    real(kind=double), dimension(:), pointer :: load,knot
    
    ! locals
    
    real(kind=double) ::  chordlen, re, cd, magvp,magvn, dpmbyVn,area,  wspeed
	real(kind=double), dimension(3) ::  c, vn,vpar, frictionForce !, fTot 
	real(kind=single), dimension(3) :: v = 0, cntrd  

    logical err;
    forces=0
    load=>forces(1:3)
    knot=>forces(4:6)
    		c = e2 - e1
				cntrd =(e2 + e1)* 0.5
				call appwind(cntrd(3), v(1),v(2),v(3))  
				if(iand(what,GPRINT)>0) write(unit,20) v
				wspeed = sqrt(dot_product(v,v))	

				call normalise(c, chordlen,err)
				if(iand(what,GPRINT)>0) write(unit,20) c*chordlen,chordlen
! parallel component (dot(v,c) * c
! normal = the rest
				vpar = c * DOT_PRODUCT(v,c)  	! parallel		
				vn   = v - vpar			! normal
				magVp =  sqrt(dot_product(vpar,vpar))
				magVn =  sqrt(dot_product(vn,vn))

				if(iand(what,GPRINT)>0) write(unit,20) magVn,magVp,dia

				re = 1.225/1.78E-5 * magVn * dia ! april 2008

				cd = cylinder_cd(re)
				if(iand(what,GPRINT)>0) write(unit,20)  Re,Cd				
				MeanCd = MeanCd + Cd; no = no + 1
				dpmbyVn = magvn * 0.6125 * cd * dia  ! divided by magvn because
! in the next stement we multiply by vn not vn(normalized)
 
				load = chordlen * dpmbyVn * vn * 0.5  ! half for each end NORMAL LOAD
				if(iand(what,4)>0) write(unit,20) load
! now the parallel skin-friction take Cf = 0.003
! velocity is magVP.  Direction is vp/MagVp

				area = 3.142 * dia * chordlen
				FrictionForce = 0.6125 * magVp * area * 0.003 * (vpar) ! OK because vp isnt normalised
				if(iand(what,GPRINT)>0) write(unit,20)  FrictionForce			
				!fTot = fTot + FrictionForce 
				load = load + FrictionForce/2.0	! each node

 
! now the drag of the knots.  Say they are 3 x dia
				area = (3. * dia) **2 * 0.78
				re = 1.225/1.78E-5 * sqrt(dot_product(v,v))  * dia *3.0
				cd = Sphere_cd(3. * re)
				! te following line IS correct because V has units of velocity
				knot = 0.6125 * wspeed * area * cd * (v)  ! drag of one knot
				if(iand(what,GPRINT)>0) write(unit,20 )3.*dia, re, cd,knot
				knot=knot/4. ! heuristic. THere are usually 4 bars coming to a node.
				if(iand(what,GPRINT)>0) write(unit,*) ''				
10	format(20(a14,TR1,$,: ))
20	format(20(g14.6,TR1,$,: ),$)
end  function windageOnCylinder

function Windage_Loads(whatin) result(iret)
    use basictypes_f
    use fglobals_f
    use saillist_f
    use vectors_f
    use coordinates_f
    use edgelist_f
    use cfromf_f
    use flinklist_f
    IMPLICIT NONE
    integer, intent(in),optional :: whatin
    integer what

    TYPE (stringlist), pointer 	:: strings
    type (string) , pointer		:: this

    real(kind=double), dimension(6), target :: forces
    real(kind=double), dimension(:), pointer :: load,knot
    real(kind=double) :: zt_st, lc, diam,re, cd, magvp,magvn, dpmbyVn,area, meanCd, no,wspeed
    real(kind=double), dimension(3) ::tot, c, vn,vpar, frictionForce, fTot,KnotTot, e1,e2
    real(kind=single), dimension(3) :: v = 0, cntrd ,v_el
    real(kind=double)  :: logre
    integer :: i, sli, iret,nn(2),s
    logical err, Use_WIndage
    TYPE (edgelist),pointer:: eds
        type (flink) , pointer		:: f
    integer unit

    iret=0
    what=0
    call check_cl('WD' ,Use_Windage )
    if(.not. Use_Windage) then
       ! write(*,*) ' skip windage (no /WD)'
        return
    endif
    write(*,*) ' windage (/WD)'
    forces=0
    load=>forces(1:3)
    knot=>forces(4:6)
    if(present(whatin)) what=whatin

    unit=90+what;

    tot=0; fTot = 0; meanCd=0.0; no=0 ; KnotTot = 0

	if(iand(what,GTEST)>0) then
            write(unit,*) ' Re Log10Re   CD_Cylinder CD_Sphere'
            logre=.0;
            do i = 1,150
                re = 10**LogRe
               write(unit,'(4G16.8)') Re,  Log10(Re), Cylinder_Cd(re),  Sphere_Cd(re)     
           	Logre = logre+ 6./150.
            enddo
	endif
	
10	format(20(a14,TR1,$,: ))
20	format(20(g14.6,TR1,$,: ),$)


	if(iand(what,GPRINT)>0) write(unit,10) "vx     ", "vy     ", "vz     ","cx     ","cy     ","cz    ","chord ","vNorm     ", "V_ax     ","dia     " &
	, "Re     ", "Cd     ","Fnx_half  ","Fny_Half   ","fnZ_Half   ","Fax    ", "Fay    ","faZ    ","knotdia","knotRe ","KnotCd ","knotFx  ","knotFy ","knotFz "
	if(iand(what,GPRINT)>0) write(unit,*)" " 
mdo:	do sli = 1,sailcount 
		if(.not. is_active(sli) ) cycle
		strings=>saillist(sli)%strings
		if(.not. associated(strings) )cycle
		if(strings%count <=0) cycle
		eds=>saillist(sli)%edges 
		do s = 1, strings%count  ! as DO this worked. As FORALL it didnt, even without 'this'
			this=>strings%list(s)%o
			if(.not. this%used ) cycle
			diam = sqrt (abs(this%d%m_Mass)/ 900.0 / 0.78)  
			if(diam .lt. 0.00001) cycle
			if(.not. associated(this%se)) cycle

do_i:			do  i=1, this%d%ne   ! as a forall this WAS OK

 			    zt_st =  eds%list(this%se(i))%m_Zlink0
				nn = eds%list(this%se(i))%m_L12  
                e1 = get_Cartesian_X(nn(1),sli); e2 = get_Cartesian_X(nn(2),sli);
                forces = windageOnCylinder( e1,e2,diam,meancd,no,what, unit)  ! fills in 'load' and 'knot'
doit:           if(.false.) then
				c = e2 - e1
				cntrd =(e2 + e1)* 0.5
				call appwind(cntrd(3), v(1),v(2),v(3))  ! where does d get its value??
				if(iand(what,GPRINT)>0) write(unit,20) v
				wspeed = sqrt(dot_product(v,v))	
				
                if(iand(what,GWITHV)>0) then
                    v_el = (eds%list(this%se(i))%m_ee1%v +   eds%list(this%se(i))%m_ee2%v)/2.0
                    v = v - v_el; wspeed = sqrt(dot_product(v,v))
                endif				
				
				call normalise(c, lc,err)
				if(iand(what,GPRINT)>0) write(unit,20) c*lc,lc
! parallel component (dot(v,c) * c
! normal = the rest
				vpar = c * DOT_PRODUCT(v,c)  	! parallel		
				vn   = v - vpar			! normal
				magVp =  sqrt(dot_product(vpar,vpar))
				magVn =  sqrt(dot_product(vn,vn))

				if(iand(what,GPRINT)>0) write(unit,20) magVn,magVp,diam

				re = 1.225/1.78E-5 * magVn * diam ! april 2008

				cd = cylinder_cd(re)
				if(iand(what,GPRINT)>0) write(unit,20)  Re,Cd				
				MeanCd = MeanCd + Cd; no = no + 1
				dpmbyVn = magvn * 0.6125 * cd * diam  ! divided by magvn because
! in the next stement we multiply by vn not vn(normalized)
 
				load = lc * dpmbyVn * vn * 0.5  ! half for each end NORMAL LOAD
				if(iand(what,4)>0) write(unit,20) load
! now the parallel skin-friction take Cf = 0.003
! velocity is magVP.  Direction is vp/MagVp

				area = 3.142 * diam * zt_st
				FrictionForce = 0.6125 * magVp * area * 0.003 * (vpar) ! OK because vp isnt normalised
				if(iand(what,GPRINT)>0) write(unit,20)  FrictionForce			
				fTot = fTot + FrictionForce 
				load = load + FrictionForce/2.0	! each node

 
! now the drag of the knots.  Say they are 3 x dia
				area = (3. * diam) **2 * 0.78
				re = 1.225/1.78E-5 * sqrt(dot_product(v,v))  * diam *3.0
				cd = Sphere_cd(3. * re)
				! te following line IS correct because V has units of velocity
				knot = 0.6125 * wspeed * area * cd * (v)  ! drag of one knot
				if(iand(what,GPRINT)>0) write(unit,20 )3.*diam, re, cd,knot
endif doit
				knotTot = knotTot + knot*2.0
				saillist(sli)%nodes%xlist(nn(1))%p = saillist(sli)%nodes%xlist(nn(1))%p + load + knot				
				saillist(sli)%nodes%xlist(nn(2))%p = saillist(sli)%nodes%xlist(nn(2))%p + load + knot
				tot = tot + load * 2.0	
				if(iand(what,GPRINT)>0) write(unit,*)' '
			end do do_i
		end do
	enddo mdo

 mdo2:	do sli = 1,sailcount 
 		if(.not. is_used(sli) ) cycle
		if(.not. is_hoisted(sli)) cycle
		if(.not. is_active(sli) ) cycle

 do_fl :    DO i=1,saillist(sli)%flinks%count
      	        f=>saillist(sli)%flinks%list(i) 
                if(.not. f%m_used) cycle
		        zt_st =  f%m_Zlink0  ! initial length 
			    load = 0;  knot=0;
	            e2= get_Cartesian_X_By_Ptr(f%m_e2);
	            e1= get_Cartesian_X_by_Ptr(f%m_e1)
 
	            diam = sqrt (abs(f%m_mul)/ 900.0 / 0.78)  
		    	if(diam .lt. 0.00001) cycle		
                forces = windageOnCylinder( e1,e2,diam,meancd,no,what, unit)  ! fills in 'load' and 'knot'	        
                f%m_e1%p = f%m_e1%p  + load  +knot
                f%m_e2%p = f%m_e2%p  + load  +knot
                tot = tot + load * 2.0	;
                knotTot = knotTot + knot*2.0
          ENDDO do_fl		
 
	enddo mdo2	
	
	if(iand(what,GWITHV)==0) then
 	    if( any(tot /= 0.0).and. no>0)  then
 		    write(outputunit,'(3(a,1x,3f10.4))') 'Windage=',tot,' of which fric=',fTot,' and knot',KnotTot
 		    write(outputunit,'(a,f10.3 )') 'mean Cd = ',meanCd/no
 	    endif	
 	    if(iand(what,GPRINT)>0 .and. no>0 .and. any(tot /= 0.0))  then
 		    write(unit,'(3(a,2x,3g11.3))') 'Windage= ',tot,' of which fric=',fTot,' and knot ',KnotTot
 		    write(unit,*) ' mean Cd = ',meanCd/no
 	    endif
 	else
    	write(outputunit,'(a,1x,3g10.3,\)') 'Wind=',tot
    endif
	windage = tot
	close(unit,status='keep')
end function Windage_Loads

function Gravity_Loads(ax,flagsin) result(iret)
	use basictypes_f
	use fglobals_f
	use saillist_f
	use flinklist_F
	use cfromf_f
	IMPLICIT NONE
        real(kind=double), intent(in),dimension(3) ::ax  ! the accelerations. Ignored on strings battens, fbeams & flinks if a gravity field is present.
	integer, optional, intent(in) :: flagsin
    logical:: HaveField
	TYPE (stringlist), pointer 	:: strings
	type (string) , pointer		:: this
        type(sail), pointer             ::sl=>NULL()
	type (flink) , pointer		:: f
     !   TYPE (batten), pointer		:: b
        TYPE (beamelement),pointer	:: bm

	real(kind=double) :: zt_st, totedgelength,fac
	real(kind=double), dimension(3) ::load,tot,x,acc
        integer :: i,  sli, iret,nn(2),s, c, flags
	TYPE (edgelist),pointer:: eds
        type(Fedge),pointer ::ed
   
    if(present(flagsin))then; flags=flagsin; else; flags=0; endif
	iret=0
	tot=0

!  we'd like to use forall here but we need to maintain a flag for whether strings is associated.
x=0;
HaveField = cf_gravity_field(x,acc)
if(HaveField ) write(outputUnit,*) 'taking all line element loading from a gravity field'
mdo:	do sli = 1,sailcount 
		if(.not. is_used(sli) ) cycle
		if(.not. is_hoisted(sli)) cycle
		if(.not. is_active(sli) ) cycle

		sl=>saillist(sli)
		c = 0		
		strings=>saillist(sli)%strings
 		if(.not. associated(strings) )cycle
		eds=>saillist(sli)%edges 
	 	if(strings%count <=0) cycle
		if(.not. associated(strings%list)) then
                    write(outputunit,*) ' stringlist not associated '
                    cycle
		endif

do_s:		do s = 1, strings%count  ! as DO this worked. As FORALL it didnt, even without 'this'
                    this=>strings%list(s)%o
                    if(.not. associated(strings%list(s)%o) ) then
                            write(outputunit,*) ' list(',s,') not associated '
                            cycle
                    endif
                    if(.not. strings%list(s)%o%used ) cycle

                    c=c+1
                    totedgelength=0.0
                    do i=1,this%d%ne
                        totedgelength=totedgelength + eds%list(this%se(i))%m_Zlink0
                    end do
                    fac = this%d%m_zislesstrim/totedgelength
                    do i=1, strings%list(s)%o%d%ne    ! as a forall this WAS OK
                        ed=>eds%list( strings%list(s)%o%se(i) )
                        zt_st = ed%m_Zlink0
                        zt_st=zt_st*fac
                        nn = ed%m_L12
                        x = ( saillist(sli)%nodes%xlist(nn(1))%XXX + saillist(sli)%nodes%xlist(nn(2))%XXX )/2.0

                        if(.not.(HaveField .and. cf_gravity_field(x,acc))) acc=ax
                        load = zt_st * strings%list(s)%o%d%m_mass* acc * 0.5

                        saillist(sli)%nodes%xlist(nn(1))%p = saillist(sli)%nodes%xlist(nn(1))%p + load
                        saillist(sli)%nodes%xlist(nn(2))%p = saillist(sli)%nodes%xlist(nn(2))%p + load
                        tot = tot + load * 2.0
                    end do
                end do do_s
        enddo mdo
		
!  the FLINKS can accept a gravity field  
		
 mdo2:	do sli = 1,sailcount 
            if(.not. is_used(sli) ) cycle
            if(.not. is_hoisted(sli)) cycle
            if(.not. is_active(sli) ) cycle

            sl=>saillist(sli)
 do_fl :    DO i=1,sl%flinks%count
                f=>sl%flinks%list(i)
                if(.not. f%m_used) cycle
                            zt_st =  f%m_Zlink0  ! initial length
                            x = ( f%m_e1%XXX + f%m_e2%XXX )/2.0
                          if(.not.(HaveField .and. cf_gravity_field(x,acc)))   acc=ax
                                load = zt_st * f%m_mul* acc * 0.5
                    f%m_e1%p = f%m_e1%p  + load
                    f%m_e2%p = f%m_e2%p  + load
                    tot = tot + load * 2.0
              ENDDO do_fl

#ifdef NEVER
 do_fb :    DO i=1,sl%battens%count
                b=>sl%battens%list(i)
                if(.not. b%used) cycle
                do j = 1,b%count
                     ed=>eds%list(b%se(j))
                     zt_st = ed%m_Zlink0
                     zt_st=zt_st*fac
                     nn = ed%m_L12
                     x = ( saillist(sli)%nodes%xlist(nn(1))%XXX + saillist(sli)%nodes%xlist(nn(2))%XXX )/2.0
                      if(.not.(HaveField .and. cf_gravity_field(x,acc))) acc=ax
                     load = 0 ! zt_st * strings%list(s)%o%d%m_mass* acc * 0.5

                     saillist(sli)%nodes%xlist(nn(1))%p = saillist(sli)%nodes%xlist(nn(1))%p + load
                     saillist(sli)%nodes%xlist(nn(2))%p = saillist(sli)%nodes%xlist(nn(2))%p + load
                     tot = tot + load * 2.0

                enddo
              ENDDO do_fb
#endif
do_fbm :    DO i=1,sl%fbeams%count
             bm=>sl%fbeams%list(i)
             if(.not. bm%m_used) cycle
             zt_st =  bm%zi ! initial length
             nn = bm%n12
             x = ( saillist(sli)%nodes%xlist(nn(1))%XXX + saillist(sli)%nodes%xlist(nn(2))%XXX )/2.0
              if(.not.(HaveField .and. cf_gravity_field(x,acc)))acc=ax
             load = zt_st * bm%m_BEMass* acc * 0.5
             saillist(sli)%nodes%xlist(nn(1))%p = saillist(sli)%nodes%xlist(nn(1))%p + load
             saillist(sli)%nodes%xlist(nn(2))%p = saillist(sli)%nodes%xlist(nn(2))%p + load
             tot = tot + load * 2.0
           ENDDO do_fbm


	enddo mdo2

	if(iand(flags,GNOPRINT)==0) then
	if(any(tot /= 0.0)) write(outputunit,'(a,2x,3f15.3)') ' Inertial Load = ',tot
	endif
	grav = tot

end function Gravity_Loads
end module gravity_f
