!C
!C June 1997  the SLideflag is now a simple 0 or 1
!C
!C this is batstuf.f
!C routines common to Kbatten and old batten
!C
!C Oct 96 the 'terminate on string doesnt line up is replaced with 
!C	count = 0
!C	return
!C	hopefully this will just remove the batten from the analysis
!C
module batstuf_f
contains
      
       SUBROUTINE PCB_Equil_matrix(Zcur,Meq)
      IMPLICIT NONE
!*    generates the equilibrium matrix Meq (12x6)
      REAL*8 Zcur
      REAL*8 Meq(12,6)

!* locals
      REAL*8 L_inv
!      integer I2
      
 !     I2=72
  
	meq=0.0d00
      L_inv = 1.0d00/Zcur

      Meq(1,6) = -1.0d00
      Meq(2,1) = L_inv
      MEQ(2,2) = L_inv
      Meq(3,3) = -L_inv
      Meq(3,4) = -L_inv
      Meq(4,5) = -1.0d00
      Meq(5,3) = 1.0d00
      Meq(6,1) = 1.0d00
      Meq(7,6) = 1.0d00
      Meq(8,1) = -L_inv
      Meq(8,2) = -L_inv
      Meq(9,3) = L_inv
      Meq(9,4) = L_inv
      Meq(10,5) = 1.0d00
      Meq(11,4) = 1.0d00
      Meq(12,2) = 1.0d00
      END SUBROUTINE PCB_Equil_matrix 


  
      SUBROUTINE Combine( bigS, Meq,Ke,Kg)
      IMPLICIT NONE 
      
!*    Finally the tangent stiffness matrix is T *( Meq * Ke * Meq  + Kg)* T

      REAL*8 BigS(12,12), Ke(6,6),Meq(12,6) ,Kg(12,12)

 !     REAL*8 spare(12,12)

	BigS = Kg + MATMUL(Meq,MATMUL(Ke,TRANSPOSE(Meq)))  ! June 2003 for speed

      END subroutine COmbine


      SUBROUTINE Print_One_Batten(u,count,EI,b0,se,zisLT,trim,eas,Slide_Flag,error)
      use basictypes_f
  
      IMPLICIT NONE
      	integer, intent(in) :: count,u
      REAL*8 EI(6,*),b0(*)
      INTEGER se(*)		! tri-edges
      REAL(double), intent(in) :: trim, zisLT,eas		! overall length and EA
      INTEGER Slide_Flag

!* returns
      LOGICAL error
!*locals
      INTEGER J,K 
   
      REAL*8 temp

      temp = zisLT
      if(Slide_Flag .eq. 1)THEN
  	    temp = 0.0D00
      ENDIF
      write(U,'(3(2x,G14.4),2x,i6)',err=98) zisLT,trim,eas, Slide_Flag
      
      DO J=1,count
         write(u,10,err=98) se(j),(ei(K,j),K=1,5),b0(j),(0.0D00,K=1,3) ! ,(xib(K,j),K=1,3)
   !        write(u,11,err=98) se(j),b0(j)  ! ,(xib(K,j),K=1,3)         
       ENDDO
11     FORMAT(i5,1x,3(1x,G12.5),2G12.4,F6.2,3F8.4 )
10     FORMAT(i5,1x,3(1x,G12.5),2G12.4,F6.2,3F8.4 )
      write(u,'(3x,3F10.4)',err=98) 0.0, 0.0, 0.0 !(xib(K,c+1),K=1,3)
        
      error = .false.  
      return
98    error = .TRUE.
END  SUBROUTINE Print_One_Batten 


SUBROUTINE Add_One_BMasses(sli, Df_Dx,NDM,list,count)
      use saillist_f
      IMPLICIT NONE
!* parameters
    integer, intent(in) :: sli
      INTEGER,intent(in) :: NDM
      INTEGER,intent(in) :: count, List(count+1)
      REAL*8 df_Dx(NDM,NDM)
!* returns nothing, but increments gnode%XM

!* locals
      INTEGER I,ii,J,K,N
      REAL*8 seed
      type(fnode), pointer ::t

!* NOTE. To add the absed row sum into a node is
!C  a) conservative
!C  B) changes the principal stiffness direction to the first octant
!C  So instead we use the SIGN intrinsic, which preserves the quadrant

      if(any(isnan(df_dx))) then
       write(outputunit,*) ' BAD batten'
       df_dx=0
       endif

      	if(count < 1) return	
      DO ii=1,count+1
 	    N = list(ii)
            if(n>ubound(saillist(sli)%nodes%xlist ,1)) write(*,*) ' (5) thats why it crashes on exit'
 	    t=>saillist(sli)%nodes%xlist(n)
          DO J=1,3
            DO K=1,3
            seed = df_dx((ii-1)*3+j,(ii-1)*3+k )
            DO i = 1,count+1      ! ROW SUM
      	      t%XM(j,k) = t%xm(j,k)+ sign(df_dx((ii-1)*3+j,(i-1)*3+k),seed)
      	     ENDDO
          ENDDO
        ENDDO
        if(any(isnan(t%XM))) write(outputunit,*) ' BAD batten at node ',t%nn
      ENDDO
      END  SUBROUTINE Add_One_BMasses
 
function PCB_New_XIB(count,list,se,xib,sli) result (OK)
	use vectors_f
	use coordinates_f
	use saillist_f
    IMPLICIT NONE
    integer, intent(in out)  :: count  ! can get zeroed
    INTEGER list(count+1) ! list is (count+1) nodes
    INTEGER se(count)		! the edges
    REAL*8 Xib(3,count+1)
    integer, intent(in) :: sli

    INTEGER I
    REAL*8 C(3), length
    LOGICAL error
    integer OK
    TYPE (edgelist),pointer:: eds
 !   type(Fedge),pointer ::ed ,e2
    eds=>saillist(sli)%edges 

!* method. 
!* Get a vector from the chord in nodecom space
!*  set XIB as along it
!*
		if(count < 1) then
			write(outputunit,*) 'XIB short bat'
			OK =0  ! added PH jan 2005 
			return
		endif
		Xib(1:3,1) = 0.0d00
		c  = get_Cartesian_X(list(count+1),sli) - get_Cartesian_X(list(1),sli)
		CALL normalise(c,length,error)
   
		if (error) THEN
			count=0
			write(outputunit,*) ' short batten(1) removed'
			OK = 0
		endif

		DO I=2,count+1
			Xib(1:3,i) = Xib(1:3,i-1) + C * eds%list( se(I-1))%m_Zlink0
!			Xib(1:3,i) = Xib(1:3,i-1) + C * Zlink0(se(I-1)) ! WIERD
			! why is the initial coord on the deflected direction? 
		ENDDO
		OK = 1
	END function PCB_New_XIB
 
 end module batstuf_f
