
! this is doforigidbody.f90! it contains the worker functions for the DOFO rigid body,
! which are called by
!Complete_rigidBodyDOFO(d, theNodes )
! ComputeDOFOJacobian(d)
 !ComputeDOFOMass(d)
 ! Get_DOFO_RigidBodyX(p_N)
! etc

!  March 2014 I suspect that it doesnt handle parameter changes correctly.
!

MODULE doforigidbody_f
USE ftypes_f
use dofo_subtypes_f
USE  linklist_f
use vectors_f
use dofoprototypes_f
use basictypes_f

IMPLICIT NONE
contains

function dofoRigidBodyJacobian(d)  result(ok)
 use fnoderef_f
 use nodalrotations_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),pointer  ::d  ! or class
#else
           class (dofo),pointer ::d  ! or type
#endif
    integer::ok,n1,n2, j,r
    real(kind=double), dimension(3,3)  :: dqdr1,dqdr2,dqdr3
    real(kind=double), dimension(3)  :: Xc ! current 
    type(fnode),pointer::theNode
    ok=0
    n1 = d%d6%nTransNodes
    n2 =ubound(d%m_Fnodes,1)

! the jacobi is divided into 4 
! J11 (translation-translation) = 1 to firtrotrow-1 and 1 to 3 = identity matrices
! J12 is translations resulting from dofo rotation
! J21 is nodal rotations resulting from dofo translation (empty)
! J22 is rotations due to rotations - identity matrices

     select case (d%ndof )  
     case (3)  ! no rotations - just the first quarter
        forall (j=1:n1) d%jacobi(3*j-2:3*j,:) = IdentityMatrix(3)   
     case(6)
 ! J11
        forall (j=1:n1) d%jacobi(3*j-2:3*j,1:3) = IdentityMatrix(3) 
 ! J21
        forall (j=n1+1:n2) d%jacobi(3*j-2:3*j,1:3) =   0
 ! J12
        dqdr1 =  DQDr(d%t(4:6),1) 
        dqdr2 =  DQDr(d%t(4:6),2) 
        dqdr3 =  DQDr(d%t(4:6),3) 
        forall (j=1:n1)
            d%jacobi(3*j-2:3*j,4) = matmul(dqdr1,d%d6%xl(:,j))
            d%jacobi(3*j-2:3*j,5) = matmul(dqdr2,d%d6%xl(:,j))
            d%jacobi(3*j-2:3*j,6) = matmul(dqdr3,d%d6%xl(:,j))
        end forall
 ! J22
       forall (j=n1+1:n2) d%jacobi(3*j-2:3*j,4:6) =   IdentityMatrix(3)           
     case default
        pause
     end select  
    d%invJacobi=transpose(d%jacobi)
 
! reset the origin  so that  
! current-X =   Get_DOFO_RigidBodyXWorker(d,r ); origin = X - (newJ).(current-t)
! for the translational nodes this gives a new origin
! for the rotationals it should just give (0,0,0) so beware of drift.  
  do j = 1,n2
         r = d%m_Fnodes(j)%m_fnrRow
        thenode=>decodenoderef(d%m_Fnodes(j))
        Xc = Get_DOFO_RigidBodyXWorker(d,r )
        d%origin(r:r+2) = Xc  - matmul(d%jacobi(r:r+2,:),d%t)
  enddo
  
end function dofoRigidBodyJacobian


pure function Get_DOFO_RigidBodyXWorker(d,row ) result (rv)
    use nodalrotations_f
    implicit none
#ifdef NO_DOFOCLASS
           type (dofo),intent(in)  ::d  ! or class
#else
           class (dofo),intent(in) ::d  ! or type
#endif
    integer, intent(in) :: row
    real(kind=double), dimension(3)	:: rv
    integer ::localrow
    localrow = (row+2)/3
    if(d%ndof .eq. 3) then    !  This says a 3-dof rigid link doesnt rotate.
        rv = d%t(1:3) + d%d6%centroidUndefl  + d%d6%xl(1:3,localrow)
    else
        if(row .lt. d%d6%firstRotRow) then
            rv = d%t(1:3) + d%d6%centroidUndefl  + matmul(qmatrix(d%t(4:6)) , d%d6%xl(1:3,localrow))
        else
            rv = d%t(4:6)  ! its a rotation node
        endif
    endif
end function  Get_DOFO_RigidBodyXWorker
end MODULE doforigidbody_f

