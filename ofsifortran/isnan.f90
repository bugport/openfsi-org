module isnan_f
use basictypes_f
contains
function myisnan(i) result (j)
implicit none
real(kind=double), intent(in) :: i
logical   :: j
!j = .false. 
j = ISNAN(i)
end function myisnan


end module isnan_f



