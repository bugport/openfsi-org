!  STRINGSORT.f90 
!
!
!****************************************************************************
!
!  MODULE: STRINGSORT
!
!  PURPOSE:  re-orders an array of edges so they represent a continuous string
!
!****************************************************************************
module stringsort_f
USE basictypes_f 
IMPLICIT NONE
	TYPE :: stringSortListItem
		INTEGER :: m_i		! index of the original entry
		INTEGER :: n1		! low node
		INTEGER :: n2		!  high node
		logical :: m_rev
		TYPE (stringSortListItem), POINTER :: next,prev
	END TYPE stringSortListItem
type :: stringsortInterfaceItem 
    integer ne
    integer, dimension(2) ::m_L12
    logical m_r 
end type stringsortInterfaceItem
 contains
 
 integer function string_sort2(ss,ne) result(iret)
use saillist_f
implicit none

	INTEGER					:: ne	 

 	type (stringsortInterfaceItem) ,dimension (ne)	:: ss
	TYPE (stringSortListItem), POINTER 	:: head,tail
	integer :: i, n1,n2,ne_in
	logical :: changing 
	iret = 0 ; changing= .True. ; ne_in=ne
	ss%m_r  = .false.
	nullify(head)
	head=>Insert_After(head,ss(1)%ne,ss(1)%m_L12(1) ,ss(1)%m_L12(2),.false.); ss(1)%ne=0
	tail=>head
	do while(changing)
	changing=.false.
	do i=2,ne
		if(ss(i)%ne == 0) cycle
		!n1 = l12(se(i),1); n2 = l12(se(i),2)
        n1 = ss(i)%m_L12(1) ; n2= ss(i)%m_L12(2) 
		if(n1 == tail%n2) then
			tail=>insert_after(tail,ss(i)%ne,n1,n2,.false.)
			ss(i)%ne=0 ; changing= .True.
		else if(n2 == tail%n2) then
			tail=>insert_after(tail,ss(i)%ne,n1,n2,.true.)
			ss(i)%ne=0 ; changing= .True.
		else if(n2 == head%n1) then
			head=>insert_before(head,ss(i)%ne,n1,n2,.false.)
			ss(i)%ne=0 ; changing= .True.
		else if(n1 == head%n1) then
			head=>insert_before(head,ss(i)%ne,n1,n2,.true.)
			ss(i)%ne=0 ; changing= .True.
		endif
	enddo
	enddo
	ne =  extract_stringlist2(head,ss)
	iret = ne
	if(ne_in /=ne) iret=0
end function string_sort2

function extract_stringlist2(head,ss) result(c)
    implicit none
	TYPE (stringSortListItem), pointer:: head	
	type (stringsortInterfaceItem) ,dimension (*)	:: ss

	TYPE (stringSortListItem) ,pointer:: sn,next 
	integer:: c 
	c =0
	if(.not. associated(head)) return
	sn=>head
	do while (.true.)
		c=c+1
		ss(c)%ne=sn%m_i ; ss(c)%m_r = sn%m_rev
		if(.not. associated(sn%next)) exit
		next=>sn%next
		deallocate(sn)
		sn=>next
	end do 

end function extract_stringlist2
 
 
 
integer function string_sort(se,ne,rev,edim,L12) result(iret)
use saillist_f
implicit none

	INTEGER					:: ne	 
 	INTEGER	,intent(in)			:: edim	
 	INTEGER , intent(in),dimension (edim,2)	:: L12
	INTEGER ,dimension (ne)			:: se
	logical , intent(out),dimension(ne)	:: rev  
	TYPE (stringSortListItem), POINTER 	:: head,tail
 
	integer :: i, n1,n2,ne_in
	logical :: changing 
	iret = 0 ; changing= .True. ; ne_in=ne
	rev = .false.
	nullify(head)
	head=>Insert_After(head,se(1),l12(se(1),1),l12(se(1),2),.false.); se(1)=0
	tail=>head
	do while(changing)
	changing=.false.
	do i=2,ne
		if(se(i) == 0) cycle
		n1 = l12(se(i),1); n2 = l12(se(i),2)

		if(n1 == tail%n2) then
			tail=>insert_after(tail,se(i),n1,n2,.false.)
			se(i)=0 ; changing= .True.
		else if(n2 == tail%n2) then
			tail=>insert_after(tail,se(i),n1,n2,.true.)
			se(i)=0 ; changing= .True.
		else if(n2 == head%n1) then
			head=>insert_before(head,se(i),n1,n2,.false.)
			se(i)=0 ; changing= .True.
		else if(n1 == head%n1) then
			head=>insert_before(head,se(i),n1,n2,.true.)
			se(i)=0 ; changing= .True.
		endif
	enddo
	enddo
	ne =  extract_stringlist(head,se,rev)
	iret = ne
	if(ne_in /=ne) iret=0
end function string_sort


 function Insert_After(before,i,n1,n2,rev) result (sn)
implicit none
	INTEGER , intent(in) :: i,n1,n2
	logical,intent(in) :: rev
	TYPE (stringSortListItem), pointer:: before	
	TYPE (stringSortListItem), POINTER :: sn

	allocate(sn)
	sn%m_i=i; sn%n1=n1;sn%n2=n2;sn%m_rev=rev
	if(rev) then
		sn%n1=n2;sn%n2=n1
	endif
	nullify(sn%prev)
	nullify(sn%next)
	if(.not. associated(before)) then
		before=>sn
	else
		sn%next=>before%next
		before%next=>sn
		sn%prev=>before
	endif

end function Insert_after
function Insert_before(after,i,n1,n2,rev) result (sn)
implicit none
	INTEGER , intent(in) :: i,n1,n2
	logical,intent(in) :: rev
	TYPE (stringSortListItem), pointer:: after	
	TYPE (stringSortListItem), POINTER :: sn

	allocate(sn)
	sn%m_i=i; sn%n1=n1;sn%n2=n2;sn%m_rev=rev
	if(rev) then
		sn%n1=n2;sn%n2=n1
	endif
	nullify(sn%prev)
	nullify(sn%next)
	if(.not. associated(after)) then
		after=>sn
		nullify(sn%next)
	else
		sn%prev=>after%prev
		after%prev=>sn
		sn%next=>after
	endif

end function Insert_before

function extract_stringlist(head,se,rev) result(c)
implicit none
	TYPE (stringSortListItem), pointer:: head	
	INTEGER ,			 dimension (*)		:: se
	logical , intent(out),dimension(*)		:: rev 

	TYPE (stringSortListItem) ,pointer:: sn,next 
	integer:: c 
	c =0
	if(.not. associated(head)) return
	sn=>head
	do while (.true.)
		c=c+1
		se(c)=sn%m_i ; rev(c) = sn%m_rev
		if(.not. associated(sn%next)) exit
		next=>sn%next
		deallocate(sn)
		sn=>next
	end do 

end function extract_stringlist

function print_stringlist(head) result(c)
implicit none
	TYPE (stringSortListItem), pointer:: head	
	TYPE (stringSortListItem),pointer :: sn
	integer:: c 
	c =0
	if(.not. associated(head)) return
	sn=>head
	write(outputunit,*) '  print_stringlist'
	do while (.true.)
		c=c+1
		write(outputunit,*) c, sn%m_i,sn%n1,sn%n2,sn%m_rev
		if(.not. associated(sn%next)) exit
		sn=>sn%next
	end do 
	write(outputunit,*) ' end linked list'

end function print_stringlist
end module stringsort_f

