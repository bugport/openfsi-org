!C file NONLIN.F 15/11/94
!C
! dec 2005  trim all C char args
!C  29/12/94  PH an error in function txy !!!!!!!!!!!!!!!
!C
!C
!C these are the nonlin routines - FORTRAN side
!C Get NL Stress sums the layer stresses into s
!C Get NL Stress_From_NLCOM gets ONE stress from nlcom - for Prepare NL Mat
MODULE nonlin_f
use ftypes_f
implicit none

contains
! There are two material models.  
! 1) linear classical laminate with infinitesimal wrinkling ( enabled by the /FI flag
! and 2) 
! nonlinear, calculated layer by layer.  DISABLED by the /FI flag
! the non-linear model . for traditional materials stress-strain is piecewise linear.  THe Wrinkling option is not implemented.
! For filament fields, negative tension in a filament is relieved unless the filament's SC has the $nobuckle flag.

subroutine Get_NL_Stress_From_Tri(t,epsin,stress,p_crease,state)
    use basictypes_f
    use wrinkle_m_f
    use cfromf_f
    use rx_control_flags_f
    IMPLICIT NONE	
    type(tri),intent(inout) :: t		! the element
    REAL (kind=double), intent(in) :: epsin(3)	! strain in the tris Material Ref Direction
    REAL (kind=double), intent(out) :: stress(3)	! stress in the tris Material Ref Direction
    REAL (kind=double),intent(out) :: p_crease
    LOGICAL :: pPrestress

    integer, intent(out) :: state
! locals
    INTEGER FLAG
    INTEGER IForce_Linear 
    REAL*8 slr(3)	! stress this layer - tri mat ref axes
     REAL (kind=double) :: eps(3)	! strain in the tris Material Ref Direction
    REAL*8 ang

    pPrestress=  gPrestress
    if( pPrestress )then
        eps = matmul( Transpose(ElasticModifier) ,epsin)
    else
        eps = epsin
    endif


    p_crease = 0.0
SlashFI: if( Use_DSTF_Old) then  ! get stress by laminate theory, with infinitesimal wrinkling if set
                ang = t%wr_angle
                call get_wrinkled_stress_only(t, eps, t%dstfold,debug2.and.t%creaseable,stress,t%wr_angle,p_crease,state)
                t%wr_state = state
                if (pPrestress) then
                    stress = matmul( ElasticModifier ,stress)+ t%m_ps
                 endif
                return
         endif  SlashFI

	state = 0
    if(Force_Linear) THEN
      		IForce_Linear = 1
    ELSE
      		IForce_Linear  = 0
	ENDIF

   stress = 0.0D00
   flag = 2  
   DO WHILE(flag .NE. 0)
	      	CALL Get_Next_Material_Stress(t%CtriPtr,eps,IForce_Linear,slr,flag)
		    stress=stress+slr
   ENDDO
   if( pPrestress )then
       stress = matmul( ElasticModifier ,stress)+ t%m_ps
    endif
END subroutine Get_NL_Stress_From_Tri
      
SUBROUTINE FortPolyline_Interp(x,y,N,s,x0,yret,grad,error) bind(C,name="fortpolyline_interp_")
      USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f
      IMPLICIT NONE
      real(c_double)::  x(*),y(*),yret,grad
      REAL(c_double), intent(in)	:: x0
      INTEGER(c_int) , intent(in)	:: N
      
      INTEGER(c_int) , intent(inout)	:: s          ! the seed
      INTEGER(c_int) , intent(out)	:: error
      INTEGER*4 seg, s1
      REAL*8 dydx 
      REAL*8 grads(2*N), xg(2*N)   ! gradient by linear interp from the mid-points xg
      REAL*8 f        
      error=0
      do seg = 1,N-1
         s1 = seg+1
         dydx = (y(s1) - y(seg))/(x(s1) - x(seg))
         grads(seg) = dydx
         xg(seg) = (x(s1) + x(seg)) /2.0D00

         if(x0 .LE. x(s1)) then ! PH LT not LT
            yret = dydx *(x0 - x(seg)) + y(seg)

              if(x0 .LE. xg(seg)) THEN     ! interp grad from previous seg
                  IF(seg .eq. 1) THEN
                    dydx = dydx
                 ELSE
                    f = (x0 - xg(seg-1)) / (xg(seg) - xg(seg-1))
                    dydx = grads(seg-1) * (1.0D00-f) + grads(seg)*f
                 ENDIF
              else       ! interp grad from next seg

                 IF(seg .eq. N-1) THEN
                    dydx = dydx
                 ELSE
                    grads(s1) = (y(s1+1) - y(s1))/(x(s1+1) - x(s1))
                    xg(s1) = (x(s1+1) + x(s1)) /2.0D00
                    f = (x0 - xg(seg)) / (xg(s1) - xg(seg))
                    dydx = grads(seg) * (1.0D00-f) + grads(s1)*f
                 ENDIF
              endif
              grad = dydx
              s = seg
              return
         endif
      enddo
! C must be off the far end of the curve
      grad = dydx
      yret = dydx*(x0 - x(seg)) + y(seg)
      s = seg
      Error = 0
      END SUBROUTINE FortPolyline_Interp


REAL*8 FUNCTION Sx(x)
  !    use nonlin_f
      IMPLICIT NONE	
      include 'nlcom.f'
      REAL*8 x ,Yret,dummy
      INTEGER error
      CALL fortPolyline_Interp(en,fn,Npn,seedx,x,yret,dummy,error)
      Sx = Yret
END FUNCTION Sx

REAL*8 FUNCTION Sy(ey)
    !  use nonlin_f
      IMPLICIT NONE	
      include 'nlcom.f'
      REAL*8 ey ,Yret,dummy
      INTEGER error

      CALL fortPolyline_Interp(en,fn,Npn,seedx,ey,yret,dummy,error)
      Sy = Yret * Zk
END FUNCTION Sy

SUBROUTINE Get_NL_Stress_From_NLCOM(e,s)
      IMPLICIT NONE	
      include 'nlcom.f'
   
      REAL*8 e(3)	! strain
      REAL*8 s(3)	! stress

      REAL*8 nuxy,nuyx

!C inputs	strain in element axes	      thru arg
 

      nuxy = vxy(e(1))
      nuyx = vyx(e(2))
      
      s(1) =  Sx(e(1))         +  Zk * Sx(e(2)) * nuxy
      s(2) =  Sx(e(1)) * nuyx  +  Zk * Sx(e(2))
      s(3) =  Txy(e(3))
END SUBROUTINE Get_NL_Stress_From_NLCOM


 


!C**********************************************************************
      
      SUBROUTINE Prepare_NL_Mat(ex,fx,fy,f45,N,ref,nua,zka,SW,enout,fnout,esout,fsout,errout) bind(C,name="fc_prepare_nl_mat"  )
      IMPLICIT NONE

!C**********************************************************************

!C arguments
      REAL*8 ex(*),fx(*),fy(*),f45(*)  ! test data
      INTEGER N			       ! point count
      REAL*8 ref		       ! strain at whic nu defined
      REAL*8 nua			       ! poissons, INPUT & OUTPUT
	  REAL*8 SW					! strip width
!C returns
      REAL*8 zka		    	       ! 90 deg factor
      REAL*8 enout(*),fnout(*),esout(*),fsout(*)   ! the interpolaing functions
       INTEGER errout


!C uses NLCOM as a workspace for this material only

      include 'nlcom.f'

!C*****************************************************

      REAL*8 Exdum, count,yret !  epsx,Sx ,
      INTEGER ERROR
      INTEGER K
      npn=N
       errout=0
      nu = nua
      zk=0.0D00
       count=0.0D00
!C	READ(9,*,err=11,end=11) en(npn),fn(npn),fy,fs(npn)
!C				ex	fx	fy  f45
  	DO K=1,npn
		es(k) = ex(k)
		en(k) = ex(k)
		fn(K) = fx(K) /SW
		fs(K) = f45(K)/SW
		
		if(fx(k) .ne. 0.0D00) THEN
			zk = zk + (fy(K)/fx(k))
			count=count+ 1.0D00
		ENDIF

        ENDDO

12	FORMAT(i5,4G14.3)

      if (count .gt. 0.0) THEN
      	Zk = Zk / count
	ELSE
	zk = 1.0D00
	ENDIF
              CALL fortPolyline_Interp(en,fn,Npn,k,ref,yret,Exdum,error)
	 if(error .ne. 0) errout=1
	NU = NU * (1.0D00-NU*NU) /Exdum
               
! poissons ratio is proportional to tangent modulus. 
! This correction means that after (fn,en) has been adjusted
! to refer to a Ey = 0 test, not a sigY=0 test, the poissons ratio
! comes out about OK

!C      DO K =1,10
!C      epsx = dble(K-2) * 0.005
!C     write(outputunit,20)' epsx,sx',  epsx,Sx(epsx),seedx
!C      ENDDO
      
20	FORMAT(a,2G15.5,I5)

     
      CALL Test_Translate(error)
        if(error .ne. 0) errout=1

      DO K=1,N
      enout(k) = en(K)
      esout(k) = es(K)
      fnout(k) = fn(K)
      fsout(k) = fs(K)

      ENDDO
      nua = nu
      zka=zk


      END SUBROUTINE Prepare_NL_Mat

!!C*************************************************************

!!C*************************************************************
      SUBROUTINE Test_Translate(error)

!C a zero degree test is given in (fn,en,npn) with zk and NU
!C already given (ie NLCOMINIT already called
!C
!C this routine replaces Fn with the values offset to allow
!C for (nonlinear) 1 - nu squared
!C
!C THis version solves for sigma-y = 0 by NR.


      IMPLICIT NONE
       INTEGER error
      include 'nlcom.f'
      INTEGER K 
      REAL*8 angle,tau,gamma

      REAL*8 :: delta = 0.000001D00
      LOGICAL DONE
      REAL*8 sigy,sigy2,ey,ey2, ex, gradinv
      INTEGER C, C_outer
      REAL*8 Test_Sx  , Sx_Now
      REAL*8 :: sx_Next,DSXbydFN ,df = -0.001D00
      INTEGER Npn_safe
      LOGICAL :: LDB  = .false.
       
      ey = 0.0D00
        error = 0
      NPN_Safe = npn
      DO K=1,npn_Safe
        npn = k
	if(npn .eq. 1) npn = 2

!C WRONG	 If fn(1),en(1) does NOT pass thru the origin
!C this is incorrect

      	Test_Sx = fn(k)
      	ex = en(k)
 	 if(LDB) write(outputunit,*)K,'th 0 degree test ',Test_Sx,' at ', ex

!C these two nexted loops are to correct fn(K)  for poissons
!C  (en,fn) were input as strip tensile tests
!C   but they must be converted to (ey = 0) tests
!C
!C
	C_Outer = 0
	DO WHILE (C_outer .le. 1000)
               if(LDB)write(outputunit,*) ' C_Outer = ',C_Outer
	C=0
      	DONE = .false.
      	DO WHILE (.not. Done)
	      	sigy = Sy(ey) + Sx(ex) * nu * Sy_Dash(ey)

		Done=(abs(sigy) .le. 1.D-6*abs(Test_Sx))
		DONE = (DONE .OR.(C.gt. 1000) )
		C=C+1
		
      		ey2 = ey + delta
      		sigy2= Sy(ey2) + Sx(ex) * nu * Sy_Dash(ey2)

      		if(abs(sigy2-sigy) .gt. 1.0D-12) THEN
	      		gradinv = delta /(sigy2-sigy)
			else
				gradinv =1.0D00/SY_Dash(0.005D00)
			ENDIF

      		ey = ey -sigy*gradinv
		sigy = Sy(ey) + Sx(ex) * nu * Sy_Dash(ey)
              ENDDO	
	  if(LDB) write(outputunit,'(a,2G14.5)') ' ey,sigy NOW ',ey ,sigy
      
	sx_Now = Sx(ex) + Sy(ey) * nu * Sx_Dash(ex)

	 if(LDB)write(outputunit,*) 'sx_Now  ',SX_Now

!C alter fn(K) until sx_Now equals SX_Test

	fn(K) = fn(K) + DF

	sx_Next = Sx(ex) + Sy(ey) * nu * Sx_Dash(ex)
	
	DSXbydFN = (SX_Next-sx_Now)/DF
      
	fn(k) = fn(k) - DF + (Test_Sx - sx_Now) /DSxbyDFN

 	sx_Now = Sx(ex) + Sy(ey) * nu * Sx_Dash(ex)

	if(LDB) write(outputunit,*) ' fn(',k,') Now ',fn(K), '  Sigma-X =', sx_Now

	if(abs(Test_Sx-sx_Now).LE.1.0D-06*abs(Test_Sx))  THEN
                  C_outer = C_Outer + 99
               ENDIF
              c_outer = c_outer+1
	ENDDO

      ENDDO

      
      
      
! the polyline (et,Y,Npn) is the function Sx for this material
! Now we can convert the shear test results into a (tau,gamma)
! polyline

      DO k=1,nPn
	       	angle =  atan(1.0D00)
		CALL angletest(angle,en(k),fs(k),gamma,tau)
	      	fs(k) = tau
		es(k) = gamma
                            if(gamma .lt. 0.0D00) THEN
		        error = 1
             write(outputunit,*) 'BAD SHEAR TEST near strain=',en(K)
                            ENDIF

	ENDDO
 40     FORMAT(a,6g12.3)
      END SUBROUTINE Test_Translate


      SUBROUTINE Old_Test_Translate()
 !     use nonlin_f
!C a zero degree test is given in (fn,en,npn) with zk and NU
!C already given (ie NLCOMINIT already called
!C
!C this routine replaces Fn with the values offset to allow
!C for (nonlinear) 1 - nu squared
!C
!C the waek part is the iterative cubic solution, which fails
!C if FACNU is not gt about 0.85
!C
!C IF IN DOUBT KEEP POISSONS DOWN 
!C
!C
      IMPLICIT NONE
      include 'nlcom.f'
      REAL*8 m0,m1,a1(nldd) ,y(nldd),dummy
      INTEGER K ,seed
      REAL*8 ezero ,yoff ,angle,tau,gamma
      INTEGER error
     
      y(1) = 0.0D00
      do k=1,npn-1
      	m1 = (fn(k+1) - fn(k))/(en(k+1)-en(k))
      	m0 = fn(k) - en(k)*m1
       	a1(k) = Cubic_Solve(m1)
        y(k+1) = y(K) + a1(K) * (en(k+1)-en(k))
      ENDDO

! so far we have the slopes of the Sx curve. We need the
! zero offset. 
! This is done by finding the strain where measured stres is zero, 
! and adjusting Sx(x) so its zero there too 
      
      seed = 0	     
      CALL fortPolyline_Interp(fn,en,npn,seed,0.0d00,ezero,dummy,error)
      CALL fortPolyline_Interp(en,y,npn,seed,ezero,yoff,dummy,error)
      write(outputunit,*) '   a1   y  fn  en '

      DO K=1,npn
      		y(k) = y(K) - yoff
      		write(outputunit,40) ' adj', a1(k),y(k),fn(k) ,en(K)
             	fn(K) = y(k)
      ENDDO

! the polyline (et,Y,Npn) is the function Sx for this material
! Now we can convert the shear test results into a (tau,gamma)
! polyline

      DO k=1,nPn
	       	angle =  atan(1.0D00)
		CALL angletest(angle,en(k),fs(k),gamma,tau)
	      	fs(k) = tau
		es(k) = gamma

	ENDDO
 40     FORMAT(a,6g12.3)
      END SUBROUTINE Old_Test_Translate

!C***************************************************


        
      REAL*8 FUNCTION Sx_Dash(x)
   !   use nonlin_f
      IMPLICIT NONE	
     
      REAL*8 x
      REAL*8 Yret
      INTEGER error
      INTEGER s
      include 'nlcom.f'


      CALL fortPolyline_Interp(en,fn,Npn,seedx,x,yret,Sx_Dash,error)
      s = seedx
!C      Sx_Dash  = (fn(s+1) - fn(s))/(en(s+1)-en(s) )
!C       CALL Material_Interp(matno,shearorX,seed,x,yret,gradret,zk,nu,error)
!C       Sx_Dash = gradret

      	END FUNCTION Sx_Dash
      REAL*8 FUNCTION Sy_Dash(ey)
!      use nonlin_f
      IMPLICIT NONE	
     
      REAL*8 ey
      REAL*8 Yret
      INTEGER error
      INTEGER s
      include 'nlcom.f'


      CALL fortPolyline_Interp(en,fn,Npn,seedx,ey,yret,Sy_Dash,error)
      s = seedx
      Sy_Dash = Sy_Dash*ZK

      END FUNCTION Sy_Dash



      REAL*8 FUNCTION Txy_Dash(xin)
  !    use nonlin_f
      IMPLICIT NONE	
      include 'nlcom.f'
      REAL*8 xin
      REAL*8 Yret ,x,grad
      INTEGER error
      INTEGER s
      x = abs(xin)
      CALL fortPolyline_Interp(es,fs,Npn,seeds,x,yret,grad,error)
      s = seeds
      Txy_Dash = sign(grad,xin)
      END  FUNCTION Txy_Dash



!C**************************************************************

      REAL*8 FUNCTION Cubic_Solve(m1 )
      IMPLICIT NONE
      REAL*8 m1, a1
      include 'nlcom.f'
      REAL*8 K, err ,facnu 

      INTEGER I
10     	K = zk * NU* NU
      	a1 = M1

       	DO  I = 1,1000
      	facnu =  1.0D00 - a1*a1 * K
      	if(facnu .lt. 0.1) THEN
      		write(outputunit,*)' Poissons too high'
      		NU = NU /2.0
      		GOTO 10
      	ENDIF
      	err = a1 * facnu - M1
           	a1 = a1 - err  
            	     
            	     
 1	format(a,5g11.3)
 	if( abs(err) .le. 1.0D-6* (1.0D-4 + abs(a1)) ) THEN
          	Cubic_Solve = a1
      	RETURN
      ENDIF
      ENDDO
      write(outputunit,*) ' pulled out '
      write(outputunit,1)' a1,err,fnu m1',a1,err,facnu, m1

       	Cubic_Solve = a1
      END FUNCTION Cubic_Solve

!C*************************************************************


      SUBROUTINE angletest(a,e,f,gamma,tau)
      IMPLICIT NONE
      REAL*8 a	! angle
      REAL*8 e,f	! extension and stress
! returns
      REAL*8 gamma,tau ! shear strain and stress after (o,90) 
      		 ! extensions have been allowed for

! this is to analyse a test result which gave stress f (uniaxial on
! direction (a) and extension in direction theta of (e)
!
! THE PROBLEM	etheta given, Ecross unknown  gammatheta ignored 
!		ftheta given, Fcross zero  tautheta zero
!
!  	From which we find gamma0, tau0 and so D(3,3)
!	
! the process is
!	1) get sigma0, sigma90, tau0 by plane stress trans
!	2) find what e0,e90 will be (from Sx)
!	3) find the component of (e0,e90) in direction a
!	4) the strain due to shear must be the difference
!	5) so print the shear stress and shear strain


      REAL*8 ct,st ,s0(3),e0(3) ,etheta
      ct = cos(-a)
      st = sin(-a)

      s0(1) = f * ct**2
      s0(2) = f * st**2
      s0(3) = 0.5D00 * f * sin(2.0D00*a)

      CALL Get_XY_STRAIN(s0,e0)

       
! Now get component of (e0,e90) at angle theta	
      etheta = e0(1) * ct**2 + e0(2)* (-st)**2


! The remainder of strain at theta must be due to shear strain gamma

      etheta = e - etheta
      gamma = ( sin(2.0D00* a) * etheta ) * 2.0D00 
      tau = s0(3)
      
10    FORMAT(a,4G12.3)

      END SUBROUTINE angletest

!C*************************************************************

       SUBROUTINE Get_XY_STRAIN(s0,e)
   !          use nonlin_f
       use invert_f
      IMPLICIT NONE
      REAL*8 s0(*), e(*) 

      include 'nlcom.f'

! given the stresses, get the strains 
! 1) make a guess. ex and ey ignoring poissons
! 2) Newton

      REAL*8 sy,de(3),dl(3,3),inv(3,3),s(3),dummy
      INTEGER Count ,K,seed
      INTEGER OK
      LOGICAL error
                 
     	CALL fortPolyline_Interp(fn,en,NpN,seed,s0(1),e(1),dummy,OK)
      sy = s0(2) / zk
      CALL fortPolyline_Interp(fn,en,Npn,seed,sy,e(2),dummy,OK)
      e(3) = 0.0D00
      OK = 0
      	count=0
     	DO WHILE (OK .eq. 0)
	   count=count+1
		
           CALL Get_Tangent_Stiffness(e,dl)
           CALL Get_NL_Stress_From_NLCOM(e,s)
           
	   if(count .eq. 1000) THEN
 	   	write(outputunit,*)' cant get strain from stress '
		write(outputunit,10 )'  aiming at ',(s0(k),K=1,3)
		write(outputunit,10) ' best guess ',s
     		write(outputunit,10) ' strain is  ',(e(k),K=1,3)

		return
	   ENDIF
           if(  abs(s(1)-s0(1))  .le. 1.0D-5*abs(s(1)+s0(1)) &
     &	   .AND.abs(s(2)-s0(2))  .le. 1.0D-5*abs(s(2)+s0(2))) &
     &     return
           
           CALL Invert2(2,dl,inv,3,error)

           de(1) = inv(1,1)*(s(1)- s0(1))+ inv(1,2) * (s(2) - s0(2))
           de(2) = inv(2,1)*(s(1)- s0(1))+ inv(2,2) * (s(2) - s0(2))

           e(1) = e(1) - de(1)
           e(2) = e(2) - de(2)
           			       
            ENDDO
10    	format(a,3g14.4)


      END SUBROUTINE Get_XY_STRAIN

!C*****************************************************

      SUBROUTINE Get_Tangent_Stiffness(e,dret)
      IMPLICIT NONE	
      include 'nlcom.f'
  
      REAL*8 e(3)	! strain
      REAL*8 Dret(3,3)	! tangent stiffness matrix 

      Dret(1,1) = Sx_Dash(e(1))
      Dret(2,2) = Zk * Sx_Dash(e(2))
      Dret(2,1) = NU * Dret(1,1) * Dret(2,2)

        Dret(1,2) =  Dret(2,1)
      Dret(1,3) = 0.0D00
        Dret(2,3) = 0.0D00
      Dret(3,1) = 0.0D00
      Dret(3,2) = 0.0D00
      Dret(3,3) = Txy_Dash(e(3))
              
      END  SUBROUTINE  Get_Tangent_Stiffness




!C*********************************************

      REAL*8 FUNCTION vxy(x)
      IMPLICIT NONE	
      include 'nlcom.f'
      REAL*8 x 
      vxy = NU * Sx_Dash(x) 
      END FUNCTION vxy
 
      REAL*8 FUNCTION vyx(y)
      IMPLICIT NONE	
      include 'nlcom.f'
      REAL*8 y
      vyx = zK * NU * Sx_Dash(y)
      END FUNCTION vyx
      
      REAL*8 FUNCTION Txy(xin)
   !         use nonlin_f
      IMPLICIT NONE	
      include 'nlcom.f'
      REAL*8 xin,grad
      
      REAL*8 x ,Yret
      INTEGER error
      x = abs(xin)
      CALL fortPolyline_Interp(es,fs,Npn,seeds,x,yret,grad,error)
      Txy = sign(yret,xin)
      END FUNCTION Txy
   end MODULE nonlin_f
