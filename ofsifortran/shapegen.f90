! March 2004 included shapemes to ensure prototyping
!*   Date: Saturday  8/1/1994
!*   translated from LAHEY FFTOSTD output
!*
! Oct 2006  rnblank to len_trim 
!*       FILE: shapegen.f
!* 10/9/97 nblank to RNBLANK for portability
!*   24.6.96   post string length to more precision. Also prestress posted
!* but why bother. Its not read anywhere...
!* date:  Thursday 7 October 1993
!* sheet angle and "shape report" now read from PROJECT
!* this i	if(Icard(L) .ne. 0) cycle		! don't write fixed-length string to summary.s a request from A.L. see fax 4/8/93
!* If (Shape Report=Boat axes) in projfile, read <sheet angle>
!* and give angles relative to boat centreline
!*
!* date:  Sunday 11 October 1992  Gennakers now have luff curve printed
!*

!*       DATE:  Tuesday 5 December 1989
!*
!*       TrimItems now taken from SAILCOM common - Peter
!*       OCTOBER 1989 modified to output the luff sag also

!* December 1989   uses the new Get_Even_Stripes routine - Peter
!*  But getting contours off L12 is slower than getting them off 
!*  by 3 seconds
module shapegen_f

use saillist_f
contains

    SUBROUTINE Measure_Tolerances(Model,changed) 
      IMPLICIT NONE
! returns True if any outside tol
	integer, intent(in) ::model
      INTEGER CHANGED

      INTEGER Rowdim,Codim
      PARAMETER (Rowdim = 11)
      PARAMETER (CoDim = 8 )
      CHARACTER*10 Items(1,Codim)
      INTEGER Rmax,Cmax, Rmax1, Rmax2, Ibiggest,Jbiggest
      LOGICAL  error
      REAL*4 old(rowdim,Codim), new(rowdim,Codim),tol(Codim),biggest,D
      CHARACTER*32 Fname
      CHARACTER*64 S(5)
      INTEGER I, J, Iunit,RunNum, Nover
      CHARACTER*12 Clabels(8)
      data Clabels /'Camber','Position','Twist','Fwd %','Aft%',  'Ent Ang','Ex Ang','Chord'/

      RunNum =-1


      CALL Get_Shape_File_Name(model,Fname)

      CALL Read_Tols(Items,1,CoDim)

      DO I=1,CoDIm
     	 CALL StringToReal(Items(1,I),tol(I),error)
      ENDDO
      Cmax = 7
      Iunit = 108
      CALL read_Shape_File(Fname ,Iunit, old,rowdim,Codim,Rmax1,Cmax)


      CALL shapegen (Model)

      Cmax = 7
      CALL read_Shape_File(Fname,Iunit, new ,rowdim,Codim,Rmax2,Cmax)

      Rmax = min(Rmax1,Rmax2)
      Ibiggest=0
      Jbiggest=0
      Biggest=0.0
      Nover=0
      CHANGED=0
      DO  I=1,rmax
      DO  J=1,cmax
      IF( abs(old(i,j) - new(i,j) ) .gt. tol(j) ) THEN
      D= abs((old(i,j) - new(i,j) ) / tol(j)  )
      Nover=Nover+1
      if (D .gt. Biggest) THEN
      Biggest=D
      Ibiggest=I
      Jbiggest=J
      ENDIF
      Changed = 1
      ENDIF
      ENDDO
      ENDDO
      if (changed .ne. 0) THEN
      I=Ibiggest
      J=Jbiggest
      WRITE(s(1),10)'         SUPERCONVERGENCE'
      WRITE(s(2),5)Nover, ' records are outside tolerance'
      write(s(3),12)'Most change:',Clabels(j),' at stripe ',i
      write(S(4),10)'     Old value   New value'
      write(S(5),15) old(i,j),new(i,j)
!      CALL dialogBox( S,5)
5      FORMAT(i2,A)
12     FORMAT(A,A,A,i2)
10     FORMAT(A)
15     FORMAT(3(5x,f7.3))

      ENDIF
      END subroutine Measure_Tolerances

SUBROUTINE Get_Shape_File_Name(Model,filename)
      IMPLICIT NONE
! arguments
      INTEGER, intent(in) ::  Model 
      
       CHARACTER*(*) Filename

       write(filename,'(a,i2.2,a)')'rxshape',Model,'.txt'
END SUBROUTINE Get_Shape_File_Name
       
SUBROUTINE Read_Shape_File(Fname, Iunit, val ,rowdim,Codim,Rmax,Cmax)
      IMPLICIT NONE
! arguments
      CHARACTER*(*) Fname
      INTEGER Iunit
      INTEGER RowDim,CoDIm
      INTEGER Cmax

! returns
      REAL*4 val(RowDim,Codim)
      INTEGER Rmax

! locals
      INTEGER i, J, K, ios

      OPEN(Iunit,file=Fname,status='old',err=99)
      Rmax = 0
      ios = 0
      DO WHILE ((ios .eq. 0) .and. (Rmax .lt. rowdim))
      rMAX = rMAX + 1
      READ(iunit,*,iostat=ios)(val(Rmax,K),K=1,Cmax)

!  READING ROW 12!!
      if( ios .ne.0) rmax=rmax - 1
      ENDDO
      CLOSE(Iunit)
      RETURN

99     continue  ! error zone
      CLOSE(Iunit)
      Rmax = RowDim ! File opening error zone
      Cmax = Codim
      DO  100 i=1,Rmax
      DO 100 J=1,Cmax
100    val( i,j) = 0.00
END SUBROUTINE Read_Shape_File

 
SUBROUTINE Read_Tols(Items,RowDim,CoDim)
      IMPLICIT NONE
! arguments
      INTEGER RowDim,CoDim
! returns
      CHARACTER*10 Items(Rowdim,Codim) 

      CHARACTER*16 string
      INTEGER I,Num

      string  = '7@8@@AAA'

      Num = 8 ! Min(8,RNBLANK(string) )
      DO 10 I=1,num
10     Items(1,I) = decrypt(string(i:i) )

END SUBROUTINE Read_Tols  
CHARACTER*10 FUNCTION decrypt(s)
      IMPLICIT NONE
      CHARACTER*(*) s
      INTEGER I
      REAL*4 q, power,mantissa
      CHARACTER*6 string

      I = Ichar(s) - 33

      Power =int (float( i)/10 )
      Mantissa = Mod(i,10)

      Q = 10**Power * mantissa
      Q = Q / 1.0e3
      CALL RealToString(Q,string)
      Decrypt = String
END FUNCTION decrypt
subroutine StringToReal(s,x,error)
    implicit none
    character(len=*), intent(in) :: s
    real(kind=4), intent(out) :: x
    logical, intent(out) :: error
    if(len_trim(s)<1) then ;error=.true.; return; endif
    read(s,*,err=10) x
    error=.false.
    return
10 error = .true.
end subroutine StringToReal
subroutine realtostring(x,s)
use basictypes_f
    implicit none
	real ,intent(in) :: x
	character(len=*), intent(out) :: s
	character(len=32) :: a
	integer k
	write(a,'(G15.3)') x
	a = adjustl(a)
	k = len_trim(a)
	if(k>6)  write(outputunit,*) '<',trim(a),'> has length ',k
    s=trim(a)
end subroutine realtostring   
SUBROUTINE shapegen(sli) 
	use saillist_f
      IMPLICIT NONE
	integer, intent(in) ::sli

! a routine to create a SHAPE REPORT file

   !!!   REAL*4 XSTRIP(12,60,3)
      CHARACTER*32 FIleType

      INTEGER Iunit 
     


!*       A note on axes... These routines use INDEX1 ,2, 3 as X,Y,Z
!*       The contours are lines of constant INDEX3
!*       The points in each stripe are ordered along INDEX1
!*       These axes are contained in a different COMMON Block to the
!*       INDICES used in EVALIB


      CALL Get_Shape_File_Name(sli,Filetype)

      Iunit=109
      open(UNit=Iunit,File=FileType,status='unknown')  
      write(Iunit,*)'******** Shape Report **********'
       write(Iunit,'(a,1x,tr1,1x,i5,1x,tr1,a )' )' MODEL no ',sli,saillist(sli)%sailname 
#ifdef NEVER
       write(iunit,*)'NOTE.'
       write(iunit,*)'These shapes are only correct for zero Heel Angle'

      if(nts(Model) .le. nte(Model)) THEN
          NumOfStripes=11
          NumOnStripe = 34 
          CALL Get_Even_Stripes (Model,XSTRIP ,NumOfStripes,NumOnStripe)
          DO  I=1,NumOfStripes
            Nstrip(I) = NumOnStripe
          ENDDO
          CALL WriteStrip (Iunit,XSTRIP,Nstrip,NumOfStripes,Model)
      ENDIF
#endif
 !      write(Iunit,'(a)') torep(sli)
       CALL trimdata (Iunit,sli)

       
      close(Iunit)

      END subroutine shapegen

SUBROUTINE trimdata (Iunit,p_sli)
	USE kbatten_f
	use fglobals_f
	use saillist_f
	use stringlist_f
	use battenf90_f
	use saillist_f
	use cfromf_f
    IMPLICIT NONE
    INTEGER ,intent(in) :: Iunit, p_sli

    character*64 atext,v_ph
    character*256  b
    integer*4 indx
    real*8 tq_local, totLen
    integer N ,k
    TYPE (stringlist), pointer 	:: strings
    type (string) , pointer		:: theString

    if( .not. is_active(p_sli)) return
    strings=>saillist(p_sli)%strings

      write(Iunit,30) '  tension(KN)',&
      tab,' name ',&
      tab,'  Length',&
      tab,'Trim',&
      tab,'Prestress',&
      tab,'EA  '
    totLen=0;
do_n : DO N=1,strings%count
      	theString =>strings%list(n)%o

if_ne:	if(theString%d%ne .gt. 0) then
            atext = "(M) "
            if(0 .ne. theString%d%m_slider) atext = "    "

            write(Iunit,20 )atext(1:3),theString%tq_s(1)/1000.0d0,  &
            tab,trim(theString%m_text),&
            tab,theString%d%m_zisLessTrim+theString%d%m_Trim,&
            tab,theString%d%m_Trim,&
            tab, theString%d%tis/1000.0d0,&
            tab,theString%d%eas
            totLen=totLen+theString%d%m_zisLessTrim+theString%d%m_Trim
!	if(iand(theString%d%Icard ,PCS_NOTRIM) .ne. 0 ) cycle	! don't write $notrim string to summary. DO write FLength
	        b=theString%m_text
!  m_text is 'boat,halyard'/ Let's strip the 'boat' 
	     	indx = index(b,',')
     		     if(indx .gt.0) then 
			       b(:indx)=' '
				b=adjustl(b)
		        endif	

	        if(0 .ne. theString%d%m_slider ) then
                    atext = 'string$'//trim(b)//'$Tension'//char(0)
     		    indx = index(atext,',')
			    if(indx .gt.0) then 
			        atext(indx:indx) = '$'
		        endif
                write(v_ph,'(G16.8,A1)') theString%tq_s(1),char(0)
            else
                atext = 'string$'//trim(b)//'$Tension_Mean'//char(0)
                indx = index(atext,',')
                if(indx .gt.0) then   ! cant see why this would ever happen
                    atext(indx:indx) = '$'
		        endif
		        tq_local = sum(theString%tq_s(1:theString%d%ne))/ dble(theString%d%ne)
   	 	        write(v_ph,'(G16.8,A1)') tq_local,char(0)
	        endif
  
            k= Post_Summary_By_SLI(p_sli, atext//char(0),v_ph//char(0))

            atext = 'string$'//trim(b)//'$Prestress'//char(0)
            indx = index(atext,',')

            if(indx .gt.0) then 
                atext(indx:indx) = '$'
             endif
            write(v_ph,'(G16.8,A1)') theString%d%tis,char(0)

        endif if_ne
        ENDDO do_N
        write(v_ph,'(G16.8,A1)')totLen,char(0)
        k= Post_Summary_By_SLI(p_sli, "TotalLength"//char(0),v_ph//char(0))

        write(Iunit,*)

20      FORMAT(a4,EN12.3,1x,a, A20,a,3(EN13.4,a), EN14.5)
30     	FORMAT(/a14, A16,a,3(a9,1x,a),1x,a,a11)
!       CALL Print_All_BFs(Bi(1,p_Model),Br(1,p_Model),Iunit,.true.)
	  k = Print_All_FBatten_Forces(Iunit,p_sli,BP_JUSTFORCES)
END SUBROUTINE trimdata  
end module shapegen_f       
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!      
#ifdef NEVER
      SUBROUTINE WriteStrip (Iunit,XSTRIP, Nstrip,NumOfStripes,Model)
      IMPLICIT NONE
	integer, intent(in) ::model
!* ktchanged 25/08/92 include 'filename'
      include 'offsets.f'
      include 'sailcom.f'
      INTEGER Iunit
      INTEGER NSTRIP(*),NumOfStripes,i,j ,ng
      REAL*4 XSTRIP(12,60,3)
      REAL*8 XLF,YLF

      REAL*4 Chord,CR,CP,TH,FPS,APS,EA,XA,aft_depth
      REAL*8 XG(60),YG(60)
      REAL*8 BaseTwist

      INTEGER RowDim,CoDim    ! max indexes.
      parameter(Rowdim=11)
      parameter(codim=8)
      CHARACTER*32 string
      CHARACTER*5 items(1,CoDim)      ! The items. one line at a time
       character*5 :: name(ROWDIM) = ' '
!  data name /' ',' ','1/4 ',' ', ' ','1/2 ', ' ',' ','3/4', ' ',' '/
      LOGICAL Boat_Axes
      INTEGER upsidedown
      INTEGER INDEX1,INDEX2,INDEX3 

!*       A note on axes... These routines use INDEX1 ,2, 3 as X,Y,Z
!*       The contours are lines of constant INDEX3
!*       The points in each stripe are ordered along INDEX1
!*       These axes are contained in a different COMMON Block to the
!*       INDICES used in EVALIB

      COMMON/shapeway/index1,index2,index3
	name(3) = '1/4 '
	name(6) = '1/2 '
	name(9) = '3/4'

      INDEX1=1
      INDEX2=2
      INDEX3=3

      BaseTwist=0.0d00

      Boat_Axes=.TRUE.   ! 2/1/95 PH

  
        write(Iunit,902) ' ','Depth','Pos ','Twist ','Fwd%', 'Aft%',' EA ',' XA ','chord'

      DO I=1,NumOfStripes


      NG=NSTRIP(I)

      IF (NG .gt.55) then
      write(outputunit,*)' NG reduced to 55 from ',NG
      NG=55
      ENDIF
!*               IF (NG .EQ. 0) GOTO 200

      XLF=XSTRIP(I,1,INDEX1)
      YLF=XSTRIP(I,1,INDEX2)
      DO J=1,NG
      XG(J)=XSTRIP(I,J,INDEX1)-XLF
      YG(J)=(XSTRIP(I,J,INDEX2)-YLF)
      ENDDO

      CALL Section_Analyse(XG,YG,NG,basetwist,chord,cr,cp,th,fps,aps,ea,xa,aft_depth,upsidedown )

      if (Boat_Axes) THEN
      EA = EA + abs(TH)
      XA =abs (TH) - XA
      ENDIF

      Call realToString(chord,items(1,8))
      CALL realtostring(cr,Items(1,1))
      CALL realtostring(cp,Items(1,2))
      CALL realtostring(TH,Items(1,3))
      CALL realtostring(FPS,Items(1,4))
      CALL realtostring(APS,Items(1,5))
      CALL realtostring(EA,Items(1,6))
      CALL realtostring(XA,Items(1,7))
      write(Iunit,902) name(i), (items(1,j),j=1,codim)

      ENDDO
!      CALL ReadFromProject  ('GENERIC_TYPE',string)
      string = ' '
      IF ((string .eq.'GENOA') .OR. (string .eq. 'GENNAKER')) THEN

      CALL GetLuffOffsets(Model,XG,YG,NG)

      DO j=1,NG
      YG(j) = YG(j) * xg(ng)          ! multiply by chord
      ENDDO
      CALL Section_Analyse(XG,YG,NG,0.0d00,chord,cr,cp,th,fps,aps,ea,xa,aft_depth,upsidedown )

      if (Boat_Axes)THEN
      WRITE(Iunit,'(a,f6.1,a)')'  Sheet Angle=',-BaseTwist*57.293,' Twist, EA , XA are relative to boat centreline'
      ELSE
      WRITE(Iunit,*)' Twist is in sail axes. EA and XA are relative to chord'
      ENDIF
      write(Iunit,1000)'Max=',cr/100.0,'m  pos=',cp,'lo%=',fps,'hi%=',aps
    
      ENDIF
1000    FORMAT(' Luff Curve: ',(1X,a4,F6.3),(a7,F6.3),2(1X,a5,F6.3))

1001            FORMAT(a,a,a,a,a,a)

902     FORMAT (10(2x,a5,1x,1h	))

      END subroutine WriteStrip

      SUBROUTINE GetLuffOffsets(Model,XG,YG,NG)
	use vectors_f
	use coordinates_f
      IMPLICIT NONE
	integer, intent(in) ::model
! PARAMETERS : none; gets data from SAILCOM

! RETURNS
      REAL*8 XG(*),YG(*)
      INTEGER NG

!      include 'nodecom.f'
      include 'linkcom.f'
      include 'offsets.f'

! criterion. Finds a string of points conencted by links of type 2
! which are also connected to triangles

! LOCALS
      INTEGER list(60), N,J
      LOGICAL starting, error
      REAL*8 O(3),chord(3),P(3), length, normal(3)
      Ng = 0
      starting = .TRUE.
     
      if(active(Model)) THEN
      DO N=nls(Model),nle(Model)
        if(Linkname(N) .eq.'luff') THEN
         IF (starting) THEN
          Ng =NG + 1
          list(ng) = N12(N,1)
          starting = .false.
         ENDIF
         IF(ng .lt. 60)          Ng =NG + 1
         list(ng) = N12(N,2)
         IF (.not. (onatri(Model,list(NG) ) ) ) Ng = Ng - 1
        ENDIF
      ENDDO
      ENDIF
     
! we now have a list of points

    	  O = get_Cartesian_X(list(1))
    	 chord = get_Cartesian_X(list(NG))

      CALL normalise(chord,length,error)

      DO j= 1,ng
 
  	P = get_Cartesian_X(list(j)) - O
	    
      CALL Vprod(P,chord,normal)
      CALL Normalise(normal,yg(J),error)
      XG(j) = DOT_PRODUCT(P,chord)
enddo

      END subroutine GetLuffOffsets

      CHARACTER*1 FUNCTION encrypt(s)
      IMPLICIT NONE
! converts the string S to a single char
      CHARACTER*(*) s
      REAL*4 R, power,mantissa
      INTEGER I
      LOGICAL Error
      CALL StringToReal(S,R,error)
      R = R * 1.e3
      R = max(R,1.0)
      R = Min(R,1.e8)

      IF ((error) .or. (R .le. 0.0) ) THEN
      Encrypt = '@' ! the code for 1.00
      ELSE
      Power = int(log10(r))
      Mantissa = 10**( MOD(log10(r),1.0))

      I = 10* Power + mantissa
      encrypt = char(I+33)
      ENDIF

END FUNCTION encrypt


end module shapegen_f
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      subroutine akm_decrypt(s,q)
      IMPLICIT NONE
      CHARACTER*(*) s
      INTEGER I
      REAL*4 q, power,mantissa

      I = Ichar(s) - 33

      Power =int (float( i)/10 )
      Mantissa = Mod(i,10)

      Q = 10**Power * mantissa
      Q = Q / 1.0e3


      END subroutine akm_decrypt

      SUBROUTINE akm_read_Tols(rtol)
      IMPLICIT NONE
! returns
      real*4 rtol(8)
! Locals
      CHARACTER*16 string
      INTEGER I,Num

      string  = '7@8@@AAA'

      Num = 8 ! Min(8,RNBLANK(string) )
      DO 10 I=1,num
10    call akm_decrypt(string(i:i),rtol(i) )

      END SUBROUTINE akm_read_Tols   

SUBROUTINE Section_Analyse(XG,YG,NG,basetwist,chord,cr,cp,th,fps,aps,ea,xa,aft_depth,upsidedown ) bind(C, name="section_analyse_")
      IMPLICIT NONE
! parameters

      REAL*8  XG(*),YG(*)  ! 1st point MUST be (0,0)
      INTEGER NG
      REAL*8 BaseTwist
! RETURNS

      REAL*4 Chord,CR,CP,TH,FPS,APS,EA,XA,aft_depth
       INTEGER upsidedown

! LOCALS
      REAL*8 XP(101),YP(101)
      INTEGER J,Nmax
      REAL*8 X1,Y1,X2,Y2,X3,Y3,X1MX2Q,X1MX3Q,Z,A,B, CC

      REAL*8 COST,SINT,YYY,Ymax,ysum
	aft_depth=0
	nmax=-999 ! uninitialized found Nov 2006
	XP(1)=0.0
	DO  J=1,100
		XP(J+1)=XP(J)+.01
	enddo

      if(ng .gt. 1) THEN

      TH=DATAN(YG(NG)/XG(NG))

      COST=COS(TH)
      SINT=SIN(TH)
      DO J=1,NG
		YYY=YG(J)*COST-XG(J)*SINT
		XG(J)=XG(J)*COST+YG(J)*SINT
		YG(J)=YYY
      ENDDO
      CHORD=XG(NG)
       ysum=0.0D00
      DO J=1,NG
		XG(J)=XG(J)/CHORD
		Ysum = Ysum + YG(J)
		YG(J)=abs(YG(J)/CHORD )
      ENDDO
!  xg,yg normalised to chord. May be upside-down hence the ABS
        upsidedown=0
        if(Ysum .lt. 0.0) upsidedown=1

      CALL SPLINE(XG,YG,XP,YP,NG,101)
      YMAX=-5.
      DO J=1,101
      IF(YP(J) .GT. YMAX) THEN
      YMAX=YP(J)
      NMAX=J
      ENDIF
      ENDDO
      ELSE ! only one given point
      th=0.00
      chord=0.00
      Nmax=0
      ENDIF
!    QUADRATIC INTERPOLATION FOR MAX POINT
      if( (nMAX .GT.100) .OR. (nMAX .LT.2) ) THEN
!               flat section so set results directly
      CR = 0.0
      CP = 50.0
      FPS = 0.0
      APS = 0.0
      EA = 0.0
      XA = 0.0
      ELSE    ! good

      X1=XP(NMAX-1)
      Y1=YP(NMAX-1)
      X2=XP(NMAX)
      Y2=YP(NMAX)
      X3=XP(NMAX+1)
      Y3=YP(NMAX+1)

      X1MX2Q=X1*X1-X2*X2
      X1MX3Q=X1*X1-X3*X3
      Z=X1MX2Q/X1MX3Q

      B=((Y1-Y2)-Z*(Y1-Y3))/((X1-X2)-Z*(X1-X3))
      A=((Y1-Y2)-B*(X1-X2))/X1MX2Q

      CP=-B/2.0/A

      CC=Y2-A*X2*X2-B*X2
      CR=CC+B*CP+A*CP*CP

      EA=57.29577951*DATAN(YP(6)/XP(6))
      XA=57.29577951*DATAN(YP(91)/(XP(101)-XP(91)))
	aft_depth = yp(76) - (yp(51) + yp(101))/2.0
	aft_depth = aft_depth/(xp(101)-xp(51))
      XP(1)=0.5*CP
      XP(2)=XP(1)+0.5

      CALL SPLINE(XG,YG,XP,YP,NG,2)


      TH=(TH- BaseTwist)  * 57.29577951
      CR=100.0*CR
      CP=100.0*CP
      IF (cr .gt. 0.01) THEN
     	 FPS=MAX (YP(1)/CR*10000.0d0, 0.0d0)
     	 APS=MAX (YP(2)/CR*10000.0d0       , 0.0d0)
      ELSE
      FPS=0.0
      APS=0.0
      ENDIF
      ENDIF
      END subroutine Section_analyse

#endif
  

