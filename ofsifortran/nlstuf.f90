!  nlstuf.f   parts of nlprep.f required by display_Mat
        SUBROUTINE Uni_Stress_Test(theta,etheta,N,stheta,retVal) bind(C,name= "uni_stress_test_"  )
        USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_f  ! for outputunit
        IMPLICIT NONE
        REAL(c_double),intent(in) :: theta, etheta
        INTEGER(c_int),intent(in) :: N

!  returns
        REAL(C_DOUBLE),intent(out) :: Stheta
         INTEGER(C_INT),intent(out) :: retval  ! o for failure
! objective
!
! we have a state of strain 
!		(etheta given, gammatheta zero, ecross unknown)
! This generates a stress state 
!		(stheta unknown, Scross zero, tautheta unknown)

!this routine is meant to solve this state for ecross and stheta

! locals
        REAL*8  ecross,scross
        REAL*8 grad ,dum ,ec1,ec2,sc1,sc2, rfact
        REAL*8:: delta = 1.0D-05 , de,oldde
        INTEGER I,k, nits
        LOGICAL tracing
        tracing = .FALSE.
       rfact = 1.0
       nits=100
        retval = 1
       
!       write(outputunit,99)' Scross  (theta,etheta',theta,etheta
        DO K=1,4 
         ecross = 0.0D00
          oldde = 0.0
         DO I = 1,nits
               ec1 = ecross - delta/2.0D00
              CALL Get_Scross(theta,etheta,ec1,dum,sc1)
               ec2 = ecross + delta/2.0D00
               CALL Get_Scross(theta,etheta,ec2,dum,sc2)
            
               CALL Get_Scross(theta,etheta,ecross,stheta,scross)

                if(dabs(sc2-sc1) .ne. 0.0) THEN
                 grad = (ec2-ec1)/(sc2-sc1)
                 de = - scross *grad * rfact
                 ecross = ecross + de
               ELSE
                 ecross = ecross - scross * grad /2.0D00 *rfact
                 write(outputunit,*)' use old grad '
               ENDIF
                if(tracing) THEN
!                     write(outputunit,99)' scross,ecross,grad',scross,ecross,grad
                ENDIF
               if (dabs(scross) .le. 1.0D-03*stheta)  RETURN
              
                 if(abs(oldde)  .gt. abs(de))  THEN
!	          write(outputunit,*) ' getting worse so backtrack'
!                 write(outputunit,99)'BACK scross,ecross,grad',scross,ecross,grad
                          de = de /2.0d00
                         ecross = ecross  - de
                 ENDIF
                  oldde = de
          ENDDO
!          write(outputunit,99)'  st , scrs,ecrs,r ',stheta, scross , ecross,rfact
         rfact = rfact * 0.5 
          nits = nits *2
          tracing = .TRUE.
        enddo 


!        write(outputunit,*)' Scross not converged  at all'
20       CONTINUE
         
        write(outputunit,99)'NOT CONVERGED scross ,stheta,theta, ecross',scross,stheta,theta*57.293, ecross
	retVal=0
99     format(a,4(x,g12.4)) 
        END

        SUBROUTINE Get_Scross( theta,etheta,ecross,stheta,scross)
        use nonlin_f
        IMPLICIT NONE
        REAL*8 theta,etheta,ecross
! returns
        REAL*8 Scross ,Stheta

! locals
        REAL*8 ct,st,s2t,e(3),s(3)
         ct = cos(theta)
         st = sin(theta)
         s2t = sin(2.0D00*theta)
         e(1) = etheta*(ct**2 ) + ecross * st**2
         e(2) = etheta*(st**2 ) + ecross * ct**2
         e(3) = (etheta-ecross)*(st*ct) * 2.0D00 
         CALL Get_NL_Stress_From_NLCOM(e,s)
         scross = s(1)*st**2 + s(2)*ct**2 - s(3)* s2t
         stheta = s(1)*ct**2 + s(2)*st**2 + s(3)* s2t
         END
