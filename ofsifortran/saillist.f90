MODULE saillist_f
      USE, INTRINSIC :: ISO_C_BINDING
      use basictypes_F
       USE  linklist_f
        USE  fglobals_f    

!
IMPLICIT NONE
!
!FLAG NOTES.  SL_Active is the same as  hoisted unless we use it to turn off models during the run
!		If we are doing this, then before each run it should be set to Hoisted.
!		currently (May 2003) it mirrors Hoisted.
!
    type ::restrictedSailData  ! was  , private till gfortran 4.2 complained
	    integer(c_int) :: SailIsUsed	=0	! non-zero means this space not blank
	    logical(c_int) :: hoisted	=.false.! means display and run if changed . Boat Too
	    logical(c_int) :: SL_active=.false.	! means run in analysis May 2003 NOT  USED 
        integer :: sli=-1
    end type restrictedSailData

	TYPE,public :: sail
            type (restrictedSailData) ::rd

            INTEGER :: RowIndex	! the first row (indexed from zero) of its block of the global stiffness matrix
            logical :: active_Originally

            integer(kind=cptrsize) :: m_SailPtr	! class RXSail*
            real(kind=double) :: tcon_this =0
            integer :: nmax= 0   ! the node which has tconmax
            real(C_DOUBLE), dimension(4,4) :: Mt, IMt
            CHARACTER (len=64) :: sailname

            TYPE ( nodelist) 	:: nodes
            TYPE ( edgelist) 	:: edges
            TYPE ( triList)    	:: tris
            TYPE ( stringlist) 	:: Strings
            TYPE ( dofolist) 	:: dofos
            TYPE ( battenlist) 	:: battens
            TYPE ( Flinklist) 	:: flinks
            TYPE ( fbeamlist)   :: fbeams
	END TYPE sail

!!!!!!!!!!!!!!!!!!!!!!!! 'GLOBAL'  declarations !!!!!!!!!!!!!!!!!!!!!!!!!!

TYPE (sail) , dimension(:) ,POINTER ,public:: saillist =>NULL()
INTEGER  ,public:: SailCount=0, SailBlock=32 
logical :: g_PleaseHookElements = .true. 
TYPE (linklist),public ,POINTER :: g_sailfreelist=>NULL()

type (dofolist) :: AllDofos

CONTAINS
function Transform_VectorBySLI(sli,xi ) result(xo)
	implicit none
	integer, intent(in) ::sli
	real(kind=double), intent(in), dimension(3) :: xi
	real(kind=double), dimension(3) :: xo
    real(kind=double), dimension(:,:) ,pointer:: Mt
    integer ::err
    err=0
        mt=>saillist(sli)%mt
        xo(1) = xi(1)* mt(1,1) + xi(2)*mt(2,1)  + xi(3)*mt(3,1)
        xo(2) = xi(1)*mt(1,2) + xi(2)*mt(2,2)  + xi(3)*mt(3,2)
        xo(3) = xi(1)*mt(1,3) + xi(2)*mt(2,3)  + xi(3)*mt(3,3)
end function Transform_VectorBySLI

function Transform_CoordsBySLI(sli,xi) result(xo)  ! moel to world
	implicit none
	integer, intent(in) ::sli
	real(kind=double), intent(in), dimension(3) :: xi
	real(kind=double),  dimension(3) :: xo

    real(kind=double), dimension(:,:) ,pointer:: Mt
    integer ::err
    err=0
	mt=>saillist(sli)%mt
	xo(1) = xi(1)*mt(1,1) + xi(2)*mt(2,1) + xi(3)*mt(3,1) + mt(4,1)
	xo(2) = xi(1)*mt(1,2) + xi(2)*mt(2,2)  + xi(3)*mt(3,2) + mt(4,2)
	xo(3) = xi(1)*mt(1,3) + xi(2)*mt(2,3)  + xi(3)*mt(3,3) + mt(4,3)
END function Transform_CoordsBySLI

function UnTransform_CoordsBySLI(sli,xi) result(xo) ! world to model
	implicit none
	integer, intent(in) ::sli
	real(kind=double), intent(in), dimension(3) :: xi
	real(kind=double),  dimension(3) :: xo

    real(kind=double), dimension(:,:) ,pointer:: iMt
    integer ::err
    err=0
    imt=>saillist(sli)%imt
    xo(1) = xi(1)*imt(1,1) + xi(2)*imt(2,1)  + xi(3)*imt(3,1) + imt(4,1)
    xo(2) = xi(1)*imt(1,2) + xi(2)*imt(2,2)  + xi(3)*imt(3,2) + imt(4,2)
    xo(3) = xi(1)*imt(1,3) + xi(2)*imt(2,3)  + xi(3)*imt(3,3) + imt(4,3)
END function UnTransform_CoordsBySLI

function UnTransform_VectorBySLI(sli,xi ) result(xo)
	implicit none
	integer, intent(in) ::sli
	real(kind=double), intent(in), dimension(3) :: xi
	real(kind=double), dimension(3) :: xo
    real(kind=double), dimension(:,:) ,pointer:: iMt

    imt=>saillist(sli)%mt
    xo(1) = xi(1)*imt(1,1) + xi(2)*imt(2,1)  + xi(3)*imt(3,1)
    xo(2) = xi(1)*imt(1,2) + xi(2)*imt(2,2)  + xi(3)*imt(3,2)
    xo(3) = xi(1)*imt(1,3) + xi(2)*imt(2,3)  + xi(3)*imt(3,3)
END function UnTransform_VectorBySLI

!**********************************************
!the following set should be the same as the BySLI transformations


SUBROUTINE  XtoLocal(sli  , xin,  xout) bind(C,name='XtoLocal')
	IMPLICIT NONE
	REAL (C_DOUBLE), INTENT(in), dimension(3) :: xin
	integer (C_INT ), INTENT(in),VALUE  :: sli
	REAL (C_DOUBLE), INTENT(out), dimension(3) :: xout

	 xout = MATMUL(TRANSPOSE(saillist(sli)%IMT(1:3,1:3)), xin) + saillist(sli)%IMT(4,1:3)

END  SUBROUTINE  XtoLocal

SUBROUTINE  XtoGlobal(sli  ,  xin,  xout) bind(C,name='XtoGlobal')
	IMPLICIT NONE
	REAL (C_DOUBLE), INTENT(in), dimension(3) :: xin
	integer (C_INT ), INTENT(in),value :: sli
	REAL (C_DOUBLE), INTENT(out), dimension(3) :: xout

	 xout = MATMUL(TRANSPOSE(saillist(sli)%mt(1:3,1:3)), xin)+ saillist(sli)%mt(4,1:3)

END  SUBROUTINE  XtoGlobal

SUBROUTINE  VtoLocal(sli  ,  xin,  xout) bind(C,name='VtoLocal')
	IMPLICIT NONE
	REAL (C_DOUBLE), INTENT(in), dimension(3) :: xin
	integer (C_INT ), INTENT(in),value :: sli
	REAL (C_DOUBLE), INTENT(out), dimension(3) :: xout

	 xout = MATMUL(TRANSPOSE(saillist(sli)%IMT(1:3,1:3)), xin) 

END  SUBROUTINE  VtoLocal

SUBROUTINE  VtoGlobal(sli  , xin,  xout) bind(C,name='VtoGlobal')
	IMPLICIT NONE
	REAL (C_DOUBLE), INTENT(in), dimension(3) :: xin
	integer (C_INT ), INTENT(in),value :: sli
	REAL (C_DOUBLE), INTENT(out), dimension(3) :: xout

 	xout = MATMUL(TRANSPOSE(saillist(sli)%mt(1:3,1:3)), xin)
END  SUBROUTINE   VtoGlobal 

  subroutine SetTransMatrix(sli, mt) bind(C,name='cf_SetTransMatrix')
        IMPLICIT NONE
        integer (C_INT ), INTENT(in),value :: sli	
        real(C_DOUBLE), dimension(4,4) :: Mt 
        saillist(sli)%mt = mt
  end  subroutine SetTransMatrix
    subroutine SetInvTransMatrix(sli, imt) bind(C,name='cf_SetInvTransMatrix')
        IMPLICIT NONE
        integer (C_INT ), INTENT(in),value :: sli	
        real(C_DOUBLE), dimension(4,4) ::  IMt
        saillist(sli)%imt = imt
  end  subroutine SetInvTransMatrix
  
  function GetTransMatrix(sli, m) result (rc) bind(C,name='cf_GetTransMatrix')
        IMPLICIT NONE
        integer (C_INT ), INTENT(in),value :: sli
        real(C_FLOAT), dimension(4,4),intent(out) :: m
        integer (C_INT ) :: rc
        m = saillist(sli)%mt
        rc=1
  end   function GetTransMatrix
  function GetInvTransMatrix(sli, m) result (rc) bind(C,name='cf_GetInvTransMatrix')
        IMPLICIT NONE
        integer (C_INT ), INTENT(in),value :: sli
        real(C_FLOAT), dimension(4,4),intent(out) :: m
        integer (C_INT ) :: rc
        m = saillist(sli)%imt
        rc=1
  end   function GetInvTransMatrix



SUBROUTINE  ApplyNewTransMatrix (sn, mt, imt) 
use dofoprototypes_f

IMPLICIT NONE
!
!	Apply transformation to Nodes, Sliding Fixities and Polylines
!
!
	INTEGER , intent(in) :: sn
	REAL (kind=double), dimension(4,4):: mt,imt
	TYPE ( nodelist) ,POINTER :: nodes
	INTEGER :: i ,err

	REAL (kind=double), dimension(3) ::xx
	if(.not. associated(saillist)) return
	if(sn > ubound(saillist,1)) return	
	if(sn < lbound(saillist,1)) return

    if(any(abs( saillist(sn)%mt -mt )> 1e-8)) then
	nodes=>saillist(sn)%nodes

	if(ASSOCIATED(nodes%Xlist)) THEN
		do i =1,  nodes%Ncount
		   if(nodes%xlist(i)%m_Nused)  THEN
                       CALL XtoLocal(sn , nodes%xlist(i)%XXX,  xx)
                        nodes%xlist(i)%XXX = MATMUL(TRANSPOSE(mt(1:3,1:3)), xx)+ mt(4,1:3)! CALL XtoGlobal(  mt ,xx,  nodes%xlist(i)%XXX)
	   	   ENDIF	
		ENDDO
	ENDIF
        err=0
        call DofoTransformAll(sn,mt,err)
        call BeamsTransformAll(sn,mt,err)
    endif
    saillist(sn)%mt = mt
    saillist(sn)%imt = imt
10  FORMAT (4f10.4)

END  SUBROUTINE  ApplyNewTransMatrix

function Set_Hoisted(n,p_h, err) result(iret)
use dofoprototypes_f
use rx_control_flags_f
IMPLICIT NONE
INTEGER,INTENT (in)  :: n,p_h   		! the sail index
INTEGER ,INTENT (out) :: err
integer :: iret
logical :: oldhflag
 
err=0 	
	 if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
		err = 1; iret=1
		return
	 endif
	if( .not. is_used(n)) then 
		saillist(n)%rd%hoisted =.false. ! just in case
		err=2;iret=2
		return
	endif
	err=0;iret=0
	oldhflag = saillist(n)%rd%hoisted
    if(p_h ==0 .and.oldhflag ) then ! we are dropping
                write(outputunit,*) 'dropping ',trim( saillist(n)%sailname)
		saillist(n)%rd%hoisted =.false.
		iret= Set_active(N,.false.);
                call unresolve_Connects(saillist(n)%sailname, len_trim( saillist(n)%sailname))
    endif
    if(p_h /=0 .and. .not. oldhflag ) then   ! we are hoisting
        write(outputunit,*) 'hoisting ',trim( saillist(n)%sailname)
		saillist(n)%rd%hoisted = .true.
		iret=Set_active(N,.true.)
		Mass_Set_Flag=1
    endif
end function set_Hoisted

function Set_active(n,p_a) result(iret)
use connect_intrf_f
IMPLICIT NONE
integer, intent(in) :: n
logical,INTENT (in)  :: p_a   		! the sail index
integer :: iret,count
logical :: oldflag

	iret=0
	oldflag = saillist(n)%rd%SL_Active 	
	
    if(.not. p_a  .and.oldflag ) then ! we are setting inactive
		saillist(n)%rd%SL_Active=.false.
		call unresolve_Connects(saillist(n)%sailname, len_trim( saillist(n)%sailname) ) 
    endif
    if(p_a  .and. .not. oldflag ) then   ! we are activating
		saillist(n)%rd%hoisted = .true.
		saillist(n)%rd%SL_Active=.true.
		call unresolve_Connects(saillist(n)%sailname, len_trim( saillist(n)%sailname)) ! shouldnt be necessary
                call Resolve_Connects(count);
    endif
end function set_active

pure  logical function is_Hoisted(n) 
	IMPLICIT NONE
	INTEGER,INTENT (in)  :: n   		! the sail index
	
	is_hoisted = .false. 
	if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)  ) THEN
		return
	else if ( n > SailCount) then 
		return
	else
		if(is_used(n))	is_hoisted = saillist(n)%rd%hoisted
	endif
end function is_Hoisted

elemental logical(c_int) function is_active(n)
	IMPLICIT NONE
	INTEGER,INTENT (in)  :: n   		! the sail index
		
	is_active = .false.

	if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
        return
	else if ( n > SailCount) then 
        return
    endif
	if( is_used(n)) then
		is_active = saillist(n)%rd%SL_active
	endif
end function is_active

pure logical(c_int) function is_active_sail(s)
	IMPLICIT NONE
	type (sail), intent(in) :: s	
	is_active_sail = .false.

	if(0 /= s%rd%SailIsUsed) then
		is_active_sail = s%rd%SL_active
	endif
end function is_active_sail	

pure  function Get_SLI(s) result(sli)
	IMPLICIT NONE
	type (sail), intent(in) :: s	
	integer(c_int) :: sli
	sli = s%rd%sli
end function Get_SLI	

elemental logical(c_int) function is_used(n)
	IMPLICIT NONE
	INTEGER,INTENT (in)  :: n   		! the sail index
	is_used = .false.
	if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
		return
	else if ( n > SailCount) then 
		return
 	endif
 
	is_used =  (0 /= saillist(n)%rd%SailIsUsed)
end function is_used



function FindHoistedSailByName (nin,L,cptr) result (sn) bind(C,name="cf_findhoistedsailbyname" )
use parse_f
USE ftypes_f

IMPLICIT NONE
	integer(c_int),intent(in),value ::L
        character(len=1),dimension(L), intent(in) ::nin
	integer(kind=cptrsize),intent(out) ::cptr
	character(len=32) :: n,ns 
	integer  :: sn
        n = TRANSFER(nin,n)
        n = trim(n)
	call denull(n)
	cptr=0
    !write(outputunit,*) '(FindHoistedSailByName)::    <', trim(n),'>'
	DO sn=1,SailCount
		!write(outputunit,*) '(FindHoistedSailByName) test <',TRIM(saillist(sn)%sailname),">",is_used(sn),saillist(sn)%rd%hoisted
	    	if(.not. is_used(sn)) then; write(outputunit,*) 'its not used'; cycle; endif
	    	if(.not. saillist(sn)%rd%hoisted) then; cycle; endif
	    	if(.not. saillist(sn)%rd%SL_active) write(outputunit,*) 'hoisted but not active:: ',TRIM(saillist(sn)%sailname)
		ns = trim(saillist(sn)%sailname)
		call denull(ns)	
	    if(n .eq.ns )  then
             	!write(outputunit,*) ' found sail <', trim(n),'>  sli = ',sn
            	cptr = saillist(sn)%m_SailPtr
            	return
!	    else
!			write(outputunit,*)n,'>'
!			write(outputunit,*)ns,'>'
	    endif
    enddo 
    ! write(outputunit,*) '(FindHoistedSailByName):: NO hoisted sail with sailname <', n,'>'
     cptr= 0
     sn=-1;
end function FindHoistedSailByName 

subroutine SetModelRowIndices()  ! walk through all live nodes setting their fnodeRowIndex
    USE ftypes_f
    IMPLICIT NONE
	TYPE ( nodelist) ,POINTER :: nodes
	INTEGER ::  sli,r,n

    r=1;
	DO sli=1,SailCount
	    if (0== is_Hoisted(sli) ) cycle
	    if (0== is_active(sli)) cycle
    	nodes=>saillist(sli)%nodes
        if(.not. associated(nodes%xlist)) cycle
        if(nodes%ncount> ubound(nodes%xlist,1) ) then
         write(outputunit,*)'nodes%ncount> ubound(nodes%xlist,1) ', nodes%ncount, ubound(nodes%xlist,1) 
        endif
        DO  N=1,nodes%ncount
            if(.not. nodes%xlist(n)%m_Nused) then
                nodes%xlist(n)%fnodeRowIndex=-7
                cycle 
            endif
            nodes%xlist(n)%fnodeRowIndex=r
            r=r+3
          ENDDO
	ENDDO
	
#ifdef _DEBUG
   write(outputunit,*) 'counted ', r-3, ' rows total'  
#endif

end  subroutine SetModelRowIndices
SUBROUTINE PrintSailStructures(unit)
USE ftypes_f
IMPLICIT NONE
	INTEGER :: unit
	TYPE ( nodelist) ,POINTER :: nodes
	TYPE ( edgelist)  ,POINTER :: edges
	TYPE ( triList)  ,POINTER    ::    tris
	TYPE ( stringlist) ,POINTER  :: strings
	TYPE (dofoList) ,POINTER  :: l_dofos
	TYPE (battenList) ,POINTER  :: battens
	 	TYPE ( flinklist) ,POINTER :: flinks
        INTEGER ::  sn,istat
	write(unit,*) 'SAIL STRUCTURE LIST'

	DO sn=1,SailCount
	write(unit,*)
        write(unit,' ("MODEL ",i4,3x,A    )',iostat=istat)  sn ,TRIM(saillist(sn)%sailname)
        write(unit,5,iostat=istat)' used',' hoist-ed',' active',' RowIndex '
5	FORMAT( a8,4A10,a8)
        write(unit,110,iostat=istat) 	is_used(sn), &
		 	saillist(sn)%rd%hoisted , &
			saillist(sn)%rd%SL_active,& 
			saillist(sn)%RowIndex   
110	FORMAT( 8x,3L10,7i10)

        write(unit,*,iostat=istat) ' trans matrix'
        write(unit,'(4f10.4 )',iostat=istat)  saillist(sn)%mt


	nodes=>saillist(sn)%nodes
	edges =>saillist(sn)%edges
 	tris   =>saillist(sn)%tris
	strings =>saillist(sn)%strings
	l_dofos =>saillist(sn)%dofos
	battens =>saillist(sn)%battens
	flinks=>saillist(sn)%flinks

	write(unit,*)  '        LIST             count     Nused   ubound    block'
20	FORMAT( A20,i10,i10, i10,i10)
	if(associated(nodes%xlist)) then
		write(unit,20) 'Nodes      ', nodes%Ncount, count(nodes%xlist%m_Nused), UBOUND(nodes%xlist),  nodes%block
	else
		write(unit,*) '        nodes   not associated '
	endif
	if(associated(tris%list)) then
		write(unit,20) 'tris       ', tris%count,count(tris%list%used), UBOUND(tris%list),  tris%block
	else
		write(unit,*) '        tris    not associated '
	endif
	if(associated(edges%list)) then
		write(unit,20) 'edges      ', edges%count,count(edges%list%m_Eused), UBOUND(edges%list),  edges%block
	else
		write(unit,*) '        edges   not associated '
	endif
	if(associated(strings%list)) then
!  count(strings%list%o%used) doesnt derefernce
		write(unit,20) 'strings    ' , strings%count,-999, UBOUND(strings%list),  strings%SLblock
	else
		write(unit,*) '        strings not associated '
	endif
	if(associated(saillist(sn)%fbeams%list)) then
                write(unit,20) 'beams      ',saillist(sn)%fbeams%count,count(saillist(sn)%fbeams%list%m_used), UBOUND(saillist(sn)%fbeams%list),saillist(sn)%fbeams%fblblock
	endif
	if(associated(l_dofos%list)) then   ! count(dofos%list%used)
		write(unit,20) 'dofos      ',l_dofos%count,-999, UBOUND(l_dofos%list),  l_dofos%dblock
	else
		write(unit,*) '        dofos not associated '
	endif
	if(associated(battens%list)) then
		write(unit,20) 'battens      ', battens%count,count(battens%list%used), UBOUND(battens%list),  battens%m_Bblock
	else
		write(unit,*) '        battens not associated '
	endif
	if(associated(flinks%list)) then
		write(unit,20) 'FLinks      ', flinks%count,count(flinks%list%m_used), UBOUND(flinks%list),  flinks%block
	else
		write(unit,*) '        FLinks not associated '
	endif	
	
	
	ENDDO
	DO sn=1,SailCount
		!call   PrintFstrings (sn,unit) 
	enddo


END SUBROUTINE PrintSailStructures

SUBROUTINE GetNextSail(n,error) ! returns index of a free entry
 use cfromf_f  ! for zeroptr
use vectors_f 
IMPLICIT NONE
INTEGER,INTENT(out)  :: n
INTEGER,INTENT (out)  :: error
! A) the array isnt allocated > allocate it  and get rid of the freelist
! B) The freelist has a entry -	B1 in the middle
! 				B2 out of bounds
!
!	we keep on popping until we find a 'good' value. This will serve for
!	garbage-collection of the freelist 
!
!  C) The freelist is vacant - or had only bad values >> append, reallocating if necessary
! 			
	TYPE (sail) , dimension(:) ,POINTER :: newspace
	type (sail) , pointer ::thesail
	character (len=128) :: Mname,MIKFileName,sailname,torep 

	error = 0
	if(.NOT. associated(saillist) ) THEN   ! Checks OK
		 ALLOCATE(newspace( SailBlock))
		saillist=>newspace
 		CALL ClearList(g_sailfreelist)  ! garbage collection
		n=1
		SailCount=1	
		saillist(n)%rd%SailIsUsed= 1
		saillist(n)%rd%sli= 1  
	else 
		 garbagePop : DO
			n= Pop(g_sailfreelist,error) 	
			if(error ==1) EXIT		! nothing retrieved
			if(n <= UBOUND(saillist,1) .and. n >= LBOUND(saillist,1) .and.  n <= SailCount) EXIT
		ENDDO   garbagePop
		if(error /= 0) THEN 	
			n = SailCount + 1
			SailCount=n
		    saillist(n)%rd%sli= n
			error = 0	
		endif

		if( n > UBOUND(saillist,1))  THEN
			write(outputunit,*) ' BUG in reallocate saillist '
			CALL reallocatesail(saillist, ubound(saillist,1) + SailBlock, error)
		ENDIF
		if(error > 0) return
	ENDIF
	thesail=>saillist(n)
	 saillist(n)%rd%SailIsUsed= 1; 	 thesail%rd%sli=n
	Mname = "Mname"
	MIKFileName = "MIKFileName"
	sailname = "sailname"
	torep = "torep"

	thesail%mt= IdentityMatrix(4)
	thesail%imt= IdentityMatrix(4)

	saillist(n)%rd%hoisted=.false.	! means display and run if changed . Boat Too
	saillist(n)%rd%SL_active=.false.	! means run in analysis May 2003 NOT  USED
 	saillist(n)%rd%sli = n 	! index into saillist
	saillist(n)%RowIndex=0	! the first row (indexed from zero) of its part of the global stiffness matrix
	saillist(n)%m_SailPtr=0	! struct SAIL*

	NULLIFY(saillist(n)%nodes%xlist); 	    NULLIFY(saillist(n)%nodes%freelist);
	NULLIFY(saillist(n)%edges%list);    	NULLIFY(saillist(n)%edges%freelist);
	NULLIFY(saillist(n)%tris%list);	        NULLIFY(saillist(n)%tris%freelist);
	NULLIFY(saillist(n)%strings%list); 	    NULLIFY(saillist(n)%strings%freelist);
        NULLIFY(saillist(n)%dofos%list); 	    !NULLIFY(saillist(n)%dofos%m_freelist);
	NULLIFY(saillist(n)%battens%list);  	NULLIFY(saillist(n)%battens%freelist); 
	NULLIFY(saillist(n)%fbeams%list);  	NULLIFY(saillist(n)%fbeams%freelist); 
	saillist(n)%nodes%Ncount=0
	saillist(n)%edges%count=0
	saillist(n)%tris%count=0
	saillist(n)%strings%count=0
	saillist(n)%dofos%count=0
	saillist(n)%battens%count=0
        saillist(n)%fbeams%count=0
	
END SUBROUTINE GetNextSail

	SUBROUTINE reallocatesail(list, nin, error)
	USE ftypes_f
	IMPLICIT NONE
	TYPE (sail), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of list
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (sail), dimension(UBOUND(list,1) ) :: temp
	extent = UBOUND(list,1) 
	if(extent > 100000) then
		write(outputunit,*) ' dodgy reallocateSail',extent
	endif
	n = nin
	if(n < 1) THEN
		write(outputunit,*) ' reallocatesail to small n =',n
		n=1
	ENDIF

	temp = list					! OldExtent items
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	extent = min(extent, n)
	list(1:extent) = temp(1:extent) 
	if(ubound(list,1) > extent) then
		list(extent+1:)%rd%SailIsUsed= 0  ! May 2003 
	endif

	END SUBROUTINE reallocatesail



SUBROUTINE FreeSail(n, error) ! makes entry N available to others
 
use dofoprototypes_f

IMPLICIT NONE
INTEGER,INTENT (in)  :: n
INTEGER,INTENT (out)  :: error
integer ::ok
TYPE (sail), pointer :: this
TYPE ( nodelist),pointer 	:: nodes
TYPE ( edgelist),pointer 	:: edges
TYPE ( triList) ,pointer   	:: tris
TYPE ( stringlist) ,pointer	:: strings
TYPE (dofoList),pointer 	:: l_dofos
TYPE (battenList),pointer 	:: battens

!  USUAL CONDITIONS
! A) n is the last one. decrement SailCount and realloc if SailCount+SailBlock < ubound
! B) n isnt the last one, so just push N onto the freelist
! 
! 
!  UNUSUAL CONDITIONS
!   n > SailCount. means something went wrong before
!  the list isnt allocated > do nothing
!   used flag already false saillist(n)%used= .FALSE. 
!   its already in the freelist

	error = 1
	if(.NOT. associated(saillist) ) THEN  
		write(outputunit,*) ' skip FreeSail on unassociated list'
		return
	ENDIF
	if(   n > SailCount) THEN
		write(outputunit,*) ' FreeSail:  off the end N > SailCount',n,SailCount
		return
	ENDIF
	if( n <=  ubound(saillist,1)  .and. n >=  lbound(saillist,1) ) THEN
		if(.not. is_used(n) ) THEN
			 write(outputunit,*) ' FreeSail:  sail already vacant ',n
			return
		ENDIF
	ELSE
		write(outputunit,*) ' Freesail: SailCount out of range!! ',SailCount, lbound(saillist,1) &
		, ubound(saillist,1) 
	ENDIF
	write(outputunit,*) ' FreeSail ',n,'**********************************************'

	this=>saillist(n)
	nodes=> this%nodes
	edges=>this%edges
	tris=>this%tris
	strings=>this%strings
	!L_restraints=>this%m_restraints
	l_dofos=>this%dofos
	battens=>this%battens

! leak. Need to clear all these lists
	ok= DeleteAllDOFOs(n,error)	  ! we put this first because (BUG) removeallFnodes doesnt unhook te DOFOS.
    call removeAllFBattens(n,error)
    call removeAllfbeams(n,error)    
    call removeAllFStrings(n,error)
    call removeAllFTris(n,error)	
    call removeAllFEdges(n,error)
    call removeAllFlinks(n,error)	 
    call removeAllFNodes(n,error)	

 	 
	saillist(n)%rd%SailIsUsed= 0	
	error=0
	if(n == SailCount) THEN
		SailCount = SailCount-1
		if( SailCount+SailBlock < ubound(saillist,1) ) THEN
			CALL reallocatesail(saillist, ubound(saillist,1) - SailBlock, error)
		ENDIF
	ELSE
		CALL push( g_sailfreelist, N ,error)
	ENDIF 
 END SUBROUTINE FreeSail

SUBROUTINE RemoveSail(n, error) ! removes all reference to this sail 
USE realloc_f
!use connect_intrf_f
IMPLICIT NONE
INTEGER,INTENT (in)  :: n
INTEGER,INTENT (out)  :: error

!  USUAL CONDITIONS
!    The analysis arrays may or may not be currently allocated
!     Pointers might not be associated
! 
! 
!  UNUSUAL CONDITIONS
!  
!  	 n out of bounds
!  	 not USED
!  	 hoisted
!    	 Pointers might not be associated

	write(outputunit,*) ' RemoveSail ',n,'---------------------------------------'
	 if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
		error = 1
		return
	 endif
	if(.not. is_used(n)) THEN
		error = 1
		return
	 endif

        call unresolve_Connects(saillist(n)%sailname, len_trim( saillist(n)%sailname) )

         CALL FreeSail(n, error)  ! to make the slot available
END SUBROUTINE RemoveSail

integer function set_SailType(n,sailname,l3, error)
use parse_f
	IMPLICIT NONE
	INTEGER,INTENT (in)  :: n   ,l3
	INTEGER,INTENT (out)  :: error 

	CHARACTER (len=l3),intent(in)    :: 	sailname
	INTEGER ::L,LS
	set_SailType = 0

	error = 0
	 if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
		error = 1
		write(outputunit,*) ' try to ChangeSailStrings on ',n,' out of range ',LBOUND(saillist,1),UBOUND(saillist,1)
		return
	 endif

	L = min(len(sailname),6)
	LS = LEN_TRIM(sailname)
	saillist(n)%sailname= sailname(:LS)
	call denull(saillist(n)%sailname )
end function set_SailType


integer function Set_sailptr(n,sPtr, err)
	IMPLICIT NONE
	INTEGER,INTENT (in)  :: n  		! the sail index
	integer(kind=cptrsize),intent(in) :: sPtr
	INTEGER ,INTENT (out) :: err
	err=0		
	 if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
		err = 1
	 else
		err = 0
		if( is_used(n)) then 
			saillist(n)%m_sailptr = sPtr
		else
			err=2
		endif
	endif
	Set_sailptr = err
end function set_sailptr

function getsailpointer(n,err) result(psp)
!use cfromf_f  ! for zeroptr
	IMPLICIT NONE
	INTEGER,INTENT (in)  :: n   		! the sail index
	INTEGER ,INTENT (out) :: err	
	integer(kind=cptrsize) :: psp	
	psp = 0
	 if(n < LBOUND(saillist,1) .or. n > UBOUND(saillist,1)) THEN
		err = 1
	else if ( n > SailCount) then 
		err=2
	 else
		err = 0		
		if(is_used(n)) psp = saillist(n)%m_sailptr  ! sd->isail
	endif
end function getsailpointer

END MODULE  saillist_f





