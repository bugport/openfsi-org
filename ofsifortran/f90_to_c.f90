
! see f90_to_c.h 
! august 2006 for Intel fortran, ptrs are integer(kind=cptrsize) 
!
! this file is in two parts.
! the first is a n interface block which should be 'use'd by the fortran source
! containing the procedure.
! the second part consists of stub functions, which call the fortran function (often a module procedure)
! stub functions are wasteful.

! extern "C" void   cf_analysis_prep();
subroutine cfanalysis_prep() bind(C,name="cf_analysis_prep")
use solveimplicit_f

call  Analysis_Prep()

end  subroutine cfanalysis_prep


   
subroutine cfclose_all_fortran_files() bind(C,name="cf_close_all_fortran_files")
    call caff()

end  subroutine cfclose_all_fortran_files


 SUBROUTINE cf_rlx_go(iexit,tmax,V_Damp,flagsin) bind(C,name="cf_rlx_go")  ! loses batten info
    USE, INTRINSIC :: ISO_C_BINDING
    use parse_f
    IMPLICIT NONE
    CHARACTER(len=1),dimension(256) , intent(in) :: flagsin! CAREFUL LENgTH

    real(c_double), intent(in) ::tmax,V_Damp
    INTEGER(c_int), intent(out) :: Iexit
    CHARACTER(len=256):: flags
    flags=TRANSFER(flagsin,flags)
    flags=trim(flags)
    call denull(flags)

    call rlx_gogo(iexit,tmax,V_Damp,flags)
end SUBROUTINE cf_rlx_go

function cf_write_nastran(sli,p_fname,L) result (ok) bind(C,name="cf_write_nastran")
    use basictypes_f  ! for outputunit
    USE, INTRINSIC :: ISO_C_BINDING
    use nastran_interface_f

    implicit none
    integer(c_int), intent(in),value :: sli,L !,len
    character(len=1),dimension(L),intent(in) :: p_fname
    !DEC$ ATTRIBUTES REFERENCE :: p_fname
    integer(c_int) :: ok,i

    character(len=256) :: fname	  ! local
    fname = TRANSFER(p_fname,fname)
    i = index(fname,char(0))
    if(i .gt. 0) then
        fname = fname(:i)
    else
        fname = trim(fname(:255))
    endif
    ok =  write_Nastran(sli,fname)
END function cf_write_nastran

module  cftesting
contains


function cf_codetestnoargs( ) result(iret) bind(C,name="cf_codetestnoargs")
    USE, INTRINSIC :: ISO_C_BINDING
    use basictypes_f  ! for outputunit
	IMPLICIT NONE
    INTEGER (c_int) :: Iret
    logical :: a
    a = C_ASSOCIATED(C_NULL_PTR)

    write(outputunit,*) 'C null ptr ',C_ASSOCIATED(C_NULL_PTR),' (shouldbe false) '
    iret=0

    if(a ) then
      write(outputunit,*) ' C_ASSOCIATED NULL PTR wrong'; iret=1
    else 
      write(outputunit,*) ' C_ASSOCIATED(C_NULL_PTR) seems OK'
    endif
end function cf_codetestnoargs
end module  cftesting

!function cf_runstress(conv,V_Damp,flags) result(iret) bind(C,name="cf_runstress")
!    USE, INTRINSIC :: ISO_C_BINDING
!    use parse_f
!    IMPLICIT NONE
!	CHARACTER(len=256) :: flags
!	!DEC$ ATTRIBUTES REFERENCE :: flags
!    real(c_double):: conv,V_Damp
!    INTEGER (c_int) :: Iret
!    iret=0

!    call denull(flags)
!  call  relax32(conv,V_Damp,flags) ! ONLY EVER CALL for debugging.
! end function cf_runstress

subroutine cf_model_hardcopy()  bind(C,name="cf_model_hardcopy")
use feaprint_f
    call model_hardcopy();
end subroutine cf_model_hardcopy


function cf_get_tension_range(sli,tmin,tmax) result(iret) bind(C )
	use basictypes_f
	use saillist_f
	implicit none
	integer, intent(in) :: sli
	real(kind=double), intent(out) :: tmax,tmin
	integer iret,c,n
	TYPE ( stringlist) ,POINTER :: strings
	real (kind = double), allocatable, dimension (:) :: t
	tmax = 1.0; tmin=0.0;
! if the model has strings with tension, return max and min , iret=1
! else iret=0
	iret=0
	
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return


	strings=>saillist(sli)%strings
	if(.not. ASSOCIATED(strings%list)) return
	c = strings%count
	if( c<1) return
	allocate(t(c))
	do n = 1,c 
	if(.not. associated( strings%list(n)%o) ) cycle
		if (.not. strings%list(n)%o%used .or. .not. associated( strings%list(n)%o%tq_s ) ) then
			t(n) = 0.0
		else
			t(n) = strings%list(n)%o%tq_s(1)
			iret=1
		endif
	enddo
	tmax = maxval( t )
	tmin = minval( t )
	deallocate(t)

end function cf_get_tension_range


function cf_get_one_string_tension(sli,nn,t) result(iret) bind(C)
!!ALIAS: "cf_get_one_string_tension" ::  cf_get_one_string_tension
use basictypes_f
use saillist_f
	implicit none
	integer, intent(in) :: sli,nn  ! i is the index of the string
	real(kind=double), intent(out) :: t
	integer iret
	TYPE ( stringlist) ,POINTER :: strings
    t=0.
	iret=0
	if(.not. associated(saillist)) return
	if(sli > ubound(saillist,1)) return	
	if(sli < lbound(saillist,1)) return


	strings=>saillist(sli)%strings
	if(.not. ASSOCIATED(strings%list)) return
	if(NN > UBOUND(strings%list,1)) return
	if(NN  <LBOUND(strings%list,1)) return
	if(.not. associated( strings%list(nn)%o)) return

	if( strings%list(NN)%o%Nn /=NN) write(outputunit,*) ' Attempt to Get tension from wrong string',nn, strings%list(NN)%o%Nn
	if(NN  > strings%count) return

	if(.not. strings%list(NN)%o%used) return 
	if(.not. associated( strings%list(NN)%o%tq_s )) return
	if(strings%list(NN)%o%d%Ne  <1) then
            return
	endif
	t = strings%list(NN)%o%tq_s(1)
	iret=1
end function cf_get_one_string_tension
! extern "C"  int cf_get_one_string_listing(const int model,const i, char*buf);


function get_one_string_listing(sli,nn,resout) result(iret) bind(C,name="cf_get_one_string_listing")
use basictypes_f
use saillist_f
        implicit none
        integer, intent(in),value :: sli,nn
        character(len=1),dimension(2048), intent(out) :: resout
         integer iret
        TYPE ( stringlist) ,POINTER :: strings
        TYPE ( string ) ,POINTER :: s
        integer:: STAT_VALUE
          character(len=248):: b,c
         character(len=2048):: t
          character(len=2048) :: res
        iret=0; STAT_VALUE=0
        if(.not. associated(saillist)) return
        if(sli > ubound(saillist,1)) return
        if(sli < lbound(saillist,1)) return


        strings=>saillist(sli)%strings
        if(.not. ASSOCIATED(strings%list)) return
        if(NN > UBOUND(strings%list,1)) return
        if(NN  <LBOUND(strings%list,1)) return
        if(NN  > strings%count) return
        if(.not. associated( strings%list(nn)%o)) return
        if(.not. strings%list(NN)%o%used) return
        if( strings%list(NN)%o%Nn /=NN) then
            write(outputunit,*) ' Attempt to Get tension from wrong string',nn, strings%list(NN)%o%Nn
            return
         endif


        if(.not. associated( strings%list(NN)%o%tq_s )) return

         s=>strings%list(NN)%o
        iret=1

        write(b,'( a16, " = ",G12.5, a )', IOSTAT=STAT_VALUE) "m_zisLessTrim", s%d%m_zisLessTrim,C_CARRIAGE_RETURN ;
        write(c,'( a16, " = ",G12.5, a )', IOSTAT=STAT_VALUE) "EAS", s%d%EAS,C_CARRIAGE_RETURN
        t = trim(b)//trim(c)

        write(b,'( a16, " = ",G12.5, a)', IOSTAT=STAT_VALUE) "tis", s%d%tis,C_CARRIAGE_RETURN;         t = trim(t)//trim(b)
        write(b,'( a16, " = ",G12.5, a)', IOSTAT=STAT_VALUE) "m_trim", s%d%m_trim,C_CARRIAGE_RETURN;   t = trim(t)//trim(b)
        write(b,'( a16, " = ",G12.5, a)', IOSTAT=STAT_VALUE) "m_mass",  s%d%m_mass,C_CARRIAGE_RETURN;   t = trim(t)//trim(b)
        write(b,'( a16, " = ",G12.5, a)', IOSTAT=STAT_VALUE) "m_zt_s",  s%d%m_zt_s,C_CARRIAGE_RETURN;   t = trim(t)//trim(b)
        write(b,'( a16, " = ",I12, a  )', IOSTAT=STAT_VALUE) "m_TCnst",  s%d%m_TCnst,C_CARRIAGE_RETURN;   t = trim(t)//trim(b)

        write(b,'( a16, " = ",I12," " ,a )', IOSTAT=STAT_VALUE) "m_Slider", s%d% m_Slider,C_CARRIAGE_RETURN;  t = trim(t)//trim(b)
        write(b,'( a16, " = ",I12," " ,a )', IOSTAT=STAT_VALUE) "m_CanBuckle3",  s%d%m_CanBuckle3,C_CARRIAGE_RETURN;  t = trim(t)//trim(b)
        write(b,'( a12, " = ",I16," " ,a )', IOSTAT=STAT_VALUE) "m_Scptr",  s%d%m_Scptr,C_CARRIAGE_RETURN ;   t = trim(t)//trim(b)
        write(b,'( a12, " = ",I16," " ,a )', IOSTAT=STAT_VALUE) "m_ExpCptr",  s%d%m_ExpCptr,C_CARRIAGE_RETURN ;t = trim(t)//trim(b)
        write(b,'( a16, " = ",I12," " ,a)', IOSTAT=STAT_VALUE) "Ne",  s%d%Ne,C_CARRIAGE_RETURN      ;        t = trim(t)//trim(b)
        write(b,'( a16, " = ",G12.5, a )') "m_tqOut",  s%d%m_tqOut;       t = trim(t)//trim(b)

        res = trim(t)//C_NULL_CHAR
  !      CONVERT RES TO RESOUT
        resout=TRANSFER(res,resout)
end function get_one_string_listing

function  CF_get_coordsbysli(sli,n,v) result(err)   bind(C,name="cf_get_coords")
    USE, INTRINSIC :: ISO_C_BINDING
	use coordinates_f
	implicit none
	integer(C_INT),intent(in),value	:: sli,n
	integer	:: err
	real(C_DOUBLE),dimension(3) :: v
	v = get_Cartesian_X(n,sli)
	err=0

end function cf_get_coordsbysli

function cf_get_raw_residual_bysli(sli,n,v) result(err)  bind(C,name="cf_get_raw_residual")
	use coordinates_f
	implicit none
	integer(c_int),intent(in),value	:: sli,n
	integer(c_int)	:: err
	real(c_double),dimension(3) :: v
	v = get_Raw_r(n,sli)
	err=0
end function cf_get_raw_residual_bysli

 
SUBROUTINE cf_get_gvalue(sli,n,gval,error)  bind(C,name="cf_get_gvalue")   ! NO bounds checking
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    IMPLICIT NONE
    INTEGER(c_int), intent(in), value :: sli,n
    integer(c_int),intent(out) ::  Error
    real(c_float),intent(out) :: gval
    type(sail), pointer :: s
    type(fnode), pointer :: thenode
    
	error=1; gval=0.;
	s=>saillist(sli)	
	thenode=> s%nodes%xlist(n)
    if(.not. thenode%m_Nused) return
    gval=thenode%G_angle; error=0
END  SUBROUTINE cf_get_gvalue

SUBROUTINE cf_get_svalue(sli,n,sval,error)  bind(C,name="cf_get_svalue")  ! NO bounds checking
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    IMPLICIT NONE
    INTEGER(c_int), intent(in), value :: sli,n
    integer(c_int),intent(out) ::  Error
    real(c_float),intent(out) :: sval
    type(sail), pointer :: s
    type(fnode), pointer :: thenode
    
	error=1; sval=0.;
	s=>saillist(sli)
	if(n>ubound(s%nodes%xlist ,1)) return	
	thenode=> s%nodes%xlist(n)
    if(.not. thenode%m_Nused) return
    sval=thenode%s_angle; error=0
END  SUBROUTINE cf_get_svalue
 
SUBROUTINE  cf_PrintConnects(unit) bind(C,name="cf_printconnects")
USE connects_f
IMPLICIT NONE
	INTEGER(c_int) , intent(in),value :: unit
 
 	call  Print_Connects(unit)
end SUBROUTINE  cf_PrintConnects

SUBROUTINE  cf_PrintSailStructures(unit) bind(C, name= 'cf_printsailstructures' )
USE sailList_f
IMPLICIT NONE
	INTEGER(c_int) , intent(in),value :: unit
	call  PrintSailStructures(unit)

end SUBROUTINE  cf_PrintSailStructures


SUBROUTINE  cf_ApplyNewTransform (sli, mt_new, imt_new)  bind(C,name="cf_applynewtransform")
USE, INTRINSIC :: ISO_C_BINDING
USE saillist_f
use basictypes_f
use connects_f
IMPLICIT NONE
	INTEGER(c_int) , intent(in),value :: sli
	REAL (c_double),intent(in), dimension(4,4):: mt_new,imt_new
	call ApplyNewTransMatrix (sli, mt_new, imt_new) 
end SUBROUTINE  cf_ApplyNewTransform 

! returns index of a free entry
SUBROUTINE cf_GetNextSail(n,error)  BIND(C, name="cf_getnextsail")
USE, INTRINSIC :: ISO_C_BINDING
USE saillist_f
IMPLICIT NONE
INTEGER(c_int),intent(inout)  :: n   
INTEGER :: error  
	call GetNextSail(n,error) 
!write(outputunit,*) ' cf_getnextsail gives n=',n,' err=',error,'************************************************'
end SUBROUTINE cf_GetNextSail

! removes all reference to this sail 
SUBROUTINE cf_RemoveSail(n, error)   BIND(C, name="cf_removesail" )
USE saillist_f
USE, INTRINSIC :: ISO_C_BINDING
IMPLICIT NONE
INTEGER(c_int), intent(in), value  :: n
INTEGER  :: error
call RemoveSail(n, error)
end SUBROUTINE cf_RemoveSail
 
!
!  The sail enquiry functions
!

integer function cf_is_hoisted(n, error) result(res)  bind(C,name="cf_ishoisted")
	USE saillist_f
	IMPLICIT NONE
	INTEGER(C_INT),value  :: n   		! the sail index
	INTEGER(C_INT)   :: error		! means this space not blank
	error=0
	res = is_hoisted(n)
end function cf_is_hoisted

integer(C_INT) function cf_isactive(n, error) bind(C,name="cf_isactive")
	USE saillist_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in),value  :: n   		! the sail index
	INTEGER(C_INT) ,INTENT (out) :: error 		! means this space not blank
	error=0
	cf_isactive = is_active(n)
end function cf_isactive

integer(kind=cptrsize) function cf_getsailpointer(sli, error) bind(C,name="cf_getsailpointer")   ! returns pointer to the class RXSail instance
	USE saillist_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in),value  :: sli   		! the sail index
	INTEGER(C_INT) ,INTENT (out)      :: error		! means this space not blank
 
	cf_getsailpointer = getsailpointer(sli,error)
end function cf_getsailpointer

!
!  The sail set functions
!

function cf_setHoisted(n, h, error) result(res) bind(C,name="cf_sethoisted"  )
	USE saillist_f
	!use  cftesting
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in),value  :: n,h   		! n=the sail index
	INTEGER (C_INT),INTENT (out) :: error		! means this space not blank
	integer(C_INT) :: res
!	res =  cf_codetestnoargs()
	res = set_hoisted(n,h,error)
end function cf_setHoisted

function cf_setactive(n, h, error) result(res) bind(C,name="cf_setactive")
        use, intrinsic :: ISO_C_BINDING
	USE saillist_f
	IMPLICIT NONE
        INTEGER(C_INT),INTENT (in),VALUE  :: n,h   		!n= the sail index
	INTEGER(C_INT) ,INTENT (out) :: error
 	integer(C_INT) :: res
	error=0		! means this space not blank
	res = set_active(n,(h.ne.0))
end function cf_setactive

integer function cf_setsailptr(sli, h, error) result(res) bind(C,name= "cf_setsailptr")
	USE saillist_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in),value  :: sli  		! the sail index
	integer(kind=cptrsize) ,intent(in) ::h
	INTEGER(C_INT) ,INTENT (out) :: error		! means this space not blank
	res = set_sailptr(sli,h,error)
end function cf_setsailptr


integer function cf_setsailtype(n,psailname,l3, error) bind(C,name="cf_setsailtype")
USE saillist_f
IMPLICIT NONE
INTEGER(C_INT),INTENT (in),value  :: n ,  l3
INTEGER(C_INT),INTENT (out)  :: error 
        CHARACTER (len=1),dimension(l3),INTENT(in)    :: 	psailname

        CHARACTER (len=l3)             :: 	sailname
        sailname = TRANSFER(psailname,sailname)
	cf_setsailtype = set_SailType(n,sailname,l3, error)
end function cf_setsailtype


subroutine cf_printAllGle(u) bind(C,name="cf_printallgle")
	use gle_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in)   ::  u
	call printAllGle(u)
end subroutine cf_printAllGle

INTEGER FUNCTION  cf_Create_Gle(pm,lm,cptr,nn,Kmat,nodes,F0,X0,p_has_tt, pnames) bind(C,name="cf_create_gle") ! called by resolve.c
	use gle_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in) ,value  ::  lm,nn, p_has_tt
	integer(kind=cptrsize) ,intent(in),value ::cptr
        CHARACTER (len=1),dimension(lm) , INTENT (in)     ::  pm
        CHARACTER (len=1) , INTENT (in)   ,dimension(64*64)       ::  pnames
	REAL  (C_DOUBLE), INTENT (in)   ,dimension (3*nn)    :: F0, X0
	REAL  (C_DOUBLE), INTENT (in)   ,dimension (9*nn*nn) :: Kmat
	INTEGER(C_INT)     , INTENT (in)   ,dimension (nn)      :: nodes
        CHARACTER (len=lm)      ::  m
        CHARACTER (len=64)   ,dimension(64)       ::  names
    m=transfer(pm,m)
    names=transfer(pnames,names)
cf_Create_Gle =  Create_Gle(m,lm,cptr,nn,Kmat,nodes,F0,X0,p_has_tt, names)
end FUNCTION  cf_Create_Gle

subroutine cf_gle_Export_Forces() bind(C,name="cf_gle_export_forces" )
	use gle_f
	IMPLICIT NONE
	call gle_Export_Forces
end  subroutine cf_gle_Export_Forces

subroutine cf_Delete_Gle(i)  bind(C,name="cf_delete_gle")
	use gle_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in)   ::  i
	call  Delete_Gle(i) 
end  subroutine cf_Delete_Gle

INTEGER(C_INT) FUNCTION  cf_Create_Connect(pm,lm,ps,ls,cptr) bind(C,name="cf_create_connect") ! called by resolve.c
	USE connects_f
	IMPLICIT NONE
	INTEGER(C_INT),INTENT (in)  ::  lm,ls
	integer(kind=cptrsize) ,intent(in) ::cptr
        CHARACTER (len=1),dimension(lm),intent(in)    ::  pm
        CHARACTER (len=1),dimension(ls),intent(in)    ::  ps

        CHARACTER (len=lm)    ::  m
        CHARACTER (len=ls)     ::  s
        m=transfer(pm,m)
        s=transfer(ps,s)
	cf_Create_Connect = Create_Connect(m,lm,s,ls,cptr) 
end FUNCTION  cf_Create_Connect
!  OK for crash above hereINTELCRASH


INTEGER(C_INT) FUNCTION cf_Delete_Connect(cptr) bind(C,name="cf_delete_connect")! called when C object deleted
	USE connects_f
	IMPLICIT NONE
	integer(kind=cptrsize),INTENT (in)  ::  cptr ! by ref (ie void**)
 	cf_Delete_Connect = Delete_Connect(cptr) 
end FUNCTION cf_Delete_Connect

subroutine cf_Resolve_Connects(count) bind(C,name="cf_resolve_connects")
        use connect_intrf_f
        USE, INTRINSIC :: ISO_C_BINDING
	IMPLICIT NONE
        integer(c_int), intent(out) ::count
!DEC$ ATTRIBUTES REFERENCE ::count
        CALL  Resolve_Connects(count)
end subroutine cf_Resolve_Connects

! extern "C"  void  cf_unresolveOneConnect(const int n);
subroutine  unresolveOneConnect(n) bind(C,name="cf_unresolveOneConnect")
        use ftypes_f
        use connects_f
        use connect_intrf_f
        IMPLICIT NONE
        INTEGER , intent(in),value :: n
        call UnResolve_One_Connect(ConList%list(n)%o)

end subroutine unresolveOneConnect

subroutine cf_UnResolve_Connects(pname, l) bind(C,name="cf_unresolve_connects")
	IMPLICIT NONE
	INTEGER , intent(in) :: l
        CHARACTER (len=1),dimension(l),intent(in)    ::  pname

        CHARACTER (len=l)     ::  name
        name=transfer(pname,name)
	call UnResolve_Connects(name, l)

end  subroutine cf_UnResolve_Connects

subroutine cf_TypeConnects() bind(C  )
	USE connects_f
	IMPLICIT NONE
	call TypeConnects()
end subroutine cf_TypeConnects
! I think this returns the first 3 node-node connects which bridge model1 and model2
SUBROUTINE cf_Get_First_Three_Connects(pmodel1,m1,pmodel2, m2,nn1,nn2,c)  bind(C,name="cf_get_first_three_connects")
	USE connects_f
	IMPLICIT NONE
	INTEGER , intent(in) :: m1,m2	! lengths of the model names
        CHARACTER (len=1),dimension(m1),intent(in) :: pmodel1
        CHARACTER (len=1),dimension(m2),intent(in) ::pmodel2
	INTEGER ,  intent(out), dimension (*)  :: nn1,nn2
	INTEGER , intent(out) :: c   ! count of nodes in x1,x2
        CHARACTER (len=m1)  :: model1
        CHARACTER (len=m2) ::model2
        model1=transfer(pmodel1,model1)
        model2=transfer(pmodel2,model2)
	call Get_First_Three_Connects(model1,m1,model2, m2,nn1,nn2,c) 
end SUBROUTINE cf_Get_First_Three_Connects
 
FUNCTION cf_setstringtrim(model,i,val) result(ok)  bind(C,name="cf_setstringtrim")
	use basictypes_f
	USE stringList_f
	IMPLICIT NONE
	INTEGER,INTENT (in)				::  model,i
	real(kind=double), intent(in) :: val
	integer ok,err
	ok=0
	err =  SetStringTrim (model,i,val) 
	if(err==0) ok=1
end FUNCTION cf_SetStringTrim


function cf_createfortranstring(sli,err) result(n) bind(C,name="cf_createfortranstring")
    USE, INTRINSIC :: ISO_C_BINDING
#ifdef DEBUG    
    use saillist_f ! for debug check
#endif
    use stringlist_f
    implicit none
    integer(C_INT), intent(in),value :: sli 
    integer(C_INT)   :: n,err
    n=  NextFreeString(sli,err)
#ifdef DEBUG
    if(sli /= saillist(sli)%strings%list(n)%o%m_sli) write(outputunit,*) 'mismatch!!!!!'
	if(n /= saillist(sli)%strings%list(n)%o%nn) write(outputunit,*) 'mismatch!!!!!'
#endif
end function cf_createfortranstring 


!extern "C"  int cf_removefortranstring(const int sli, const int n, int*err);
function  cf_removefortranstring(sli,nn, err) result(ok)  bind(C,name="cf_removefortranstring")
    USE, INTRINSIC :: ISO_C_BINDING
  
    use stringlist_f
    implicit none
    integer(C_INT), intent(in),value :: sli ,nn
    integer(C_INT)   :: err,ok;   ok =1
    call RemoveFortranString (sli, NN,err)
    if(err /=0) ok=0
end function  cf_removefortranstring
 

!EXTERN_C int cf_createbeamelement( const int sli, const int n1,const int n2,
!const double beta,zi,ti,mass
!const double *emp
!const char*name
!int *err)

function createbeamelement(sli,p_n1,p_n2,beta,p_zi,p_ti,p_mass, emp,axLoadFlag,pnamein,lname,&
      & err) result(n) bind(C,name="cf_createbeamelement")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f ! for debug check
    use fbeamList_f
    use parse_f
    implicit none
        integer(C_INT) , intent(in),value :: sli,p_n1,p_n2,lname
	real(c_double), intent(in),value ::beta,p_zi,p_ti,p_mass
	real(c_double), intent(in), dimension(5) ::emp 
        integer(C_INT), intent(in),value :: axLoadFlag
        character(len=1),dimension(lname),intent(in) ::pnamein
        integer(C_INT) , intent(out) :: err
    integer(C_INT)   :: n 
    character (len=lname) :: name
    integer ok;
    name = transfer(pnamein,name)
    n=   NextFreeBeam(sli,err)
    call denull(name);
    ok = CreateFortranfbeam (sli, N ,p_n1,p_n2,p_zi,p_ti, p_mass,emp,axLoadFlag,beta,trim(name),err)
end function createbeamelement

function changebeamelement(sli,nn,beta,p_zi,p_ti,p_mass, emp,axLoadFlag,namein,lname, err) result(ok) bind(C,name="cf_changebeamelement")
    USE, INTRINSIC :: ISO_C_BINDING
    use fbeamList_f
    use parse_f
    implicit none
         integer(C_INT), intent(in),value :: sli,nn ,lname
        real(c_double), intent(in),value ::beta,p_zi,p_ti,p_mass
        real(c_double), intent(in), dimension(5) ::emp
        integer(C_INT), intent(in),value :: axLoadFlag
        character(len=1),dimension(lname),intent(in) ::namein
        integer(C_INT) , intent(out) :: err

    character (len=lname) :: name
    integer ok;
    name = transfer(namein,name)
    ok = index(name,char(0));

    call denull(name);
    ok = ChangeFortranfbeam (sli, Nn ,p_zi,p_ti, p_mass,emp,axLoadFlag,beta,trim(name),err)
end function changebeamelement

! EXTERN_C int cf_one_initial_beamaxismatrix(const int sli, const int n,
!										   const double *s,const double *e);
function cf_one_initial_beamaxismatrix(sli,n,s,e) result (rc) bind(C,name="cf_one_initial_beamaxismatrix")
   USE, INTRINSIC :: ISO_C_BINDING
   use ftypes_f
    use saillist_f
    use fbeamList_f
    use tanpure_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n 
    real(C_DOUBLE), intent(in), dimension(3) :: s,e
    type(beamelement), pointer :: be
    integer(c_int) :: rc ,kk
   
    be=>saillist(sli)%fbeams%list(n)
    rc =one_beam_preliminaries(be,s,e)
    if(rc /= BEAM_OK) write(*,*) ' one_beam_preliminaries NOT OK'
    kk= one_initial_beamaxismatrix(be,s,e)
    if(kk /= BEAM_OK) write(*,*) ' one_initial_beamaxismatrix NOT OK'
    rc = rc+  kk
    
end function cf_one_initial_beamaxismatrix

!extern "C" int cf_one_beamelementresult(const int sli, const int n, double f[10]);
function one_beamelementresult(sli,n,f) result (rc) bind(C,name="cf_one_beamelementresult")
   USE, INTRINSIC :: ISO_C_BINDING
   use ftypes_f
    use saillist_f
    use fbeamList_f
    use tanpure_f
    use tanglobals_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
    real(C_DOUBLE), intent(out), dimension(*) :: f
    type(beamelement), pointer :: be
    integer(c_int) :: rc
    rc=0
   if(n> saillist(sli)%fbeams%count) return

    be=>saillist(sli)%fbeams%list(n)
    f(1:10) =   one_local_element_force(be,be%zi,G_Bowing,G_BowShear)
    rc = 1
end function one_beamelementresult
! extern "C" int cf_one_beamelementtrigraph(const int sli, const int n, double* f); // f is 9 long

function one_beamelementtrigraph(sli,n,f) result (rc) bind(C,name="cf_one_beamelementtrigraph")
   USE, INTRINSIC :: ISO_C_BINDING
   use ftypes_f
    use saillist_f
    use fbeamList_f
    use tanpure_f
    use tanglobals_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
    real(C_DOUBLE), intent(out), dimension(*) :: f
    type(beamelement), pointer :: be
    integer(c_int) :: rc
    rc=0
   if(n> saillist(sli)%fbeams%count) return

    be=>saillist(sli)%fbeams%list(n)

    f(1:9) =   one_local_element_trigraph(be) !,be%zi,G_Bowing,G_BowShear)
    rc = 1
end function one_beamelementtrigraph


function  removebeamelement(sli,nn, err) result(ok)  bind(C,name="cf_removebeamelement")
    USE, INTRINSIC :: ISO_C_BINDING
    use fbeamList_f
    implicit none
    integer(C_INT), intent(in),value :: sli ,nn
    integer(C_INT)   :: err,ok;   ok =1
    call RemoveFortranBeam (sli, NN,err)
    if(err /=0) ok=0
end function  removebeamelement

function  cf_drawfbeams(sli,seg) result (rc) bind(C,name="cf_drawfbeams")
    USE, INTRINSIC :: ISO_C_BINDING
    use fbeamList_f
    integer(C_INT), intent(in),value :: sli
    integer(kind=cptrsize) , value :: seg
    integer(C_INT) ::rc

    rc= DrawFbeams (sli,seg)

end function cf_drawfbeams

function cf_createfortranbatten(sli,err) result(n) bind(C,name="cf_createfortranbatten")
    USE, INTRINSIC :: ISO_C_BINDING
    use battenf90_f
    implicit none
    integer(C_INT), intent(in),value :: sli 
    integer(C_INT)   :: n,err
 
    n=    NextFreeBatten(sli,err)
end function cf_createfortranbatten

function cf_removefortranbatten(sli,n, err) result(ok)  bind(C,name="cf_removefortranbatten")
    USE, INTRINSIC :: ISO_C_BINDING
    use battenf90_f
    implicit none
    integer(C_INT), intent(in),value :: sli ,n
    integer(C_INT)   :: err,ok; err=0;  ok =1
    call RemoveFortranBatten (sli, N,err)
    if(err /=0)  then 
    write(outputunit,*)"Remove Batten not clean ",err; ok=0
    endif
end function  cf_removefortranbatten

function cf_initialisebatten(sli,n,count,cptr,pt, err) result(ok) bind(C,name="cf_initialisebatten")
    USE, INTRINSIC :: ISO_C_BINDING
    use battenf90_f
     use parse_f
    implicit none
    integer(C_INT), intent(in),value :: sli ,n,count
    integer(kind=cptrsize), intent(in), value ::cptr
    character(len=1),dimension(32) , intent(in) ::pt
    integer(C_INT)   :: err,ok; 
        character(len=32)   ::t
        t=transfer(pt,t)
     call denull(t)    
      ok =1
    
    call initialise_batten(sli,n,count,cptr,t,err)
    if(err /=0)  then 
        err=1/0; ok=0
    endif
end function cf_initialisebatten

	

!extern "C" int cf_fillbatten( const int sli, const int n ,const double zi,const double trim ,
!		const double ea, const int Islide,  const int*const se, const int*const rev,const double* const ei); 



integer (C_INT) function cf_fillbatten(sli,n, zi,trim,EA,Islide,se,rev,ei) bind(C,name="cf_fillbatten") !  sets bo and xib to zero
 USE, INTRINSIC :: ISO_C_BINDING
     use battenf90_f
        implicit none
        integer(C_INT), intent(in),value ::sli,n,Islide
        integer(c_int), dimension(*), intent(in) :: se,rev
        real (C_DOUBLE), intent(in),value:: zi,trim,ea
        real(c_DOUBLE), intent(in), dimension (*) ::ei

        cf_fillbatten=fillbatten(sli,n,se,rev, zi,trim,EA,Islide,ei)
end function cf_fillbatten

!  extern "C" int cf_drawlinks(const int sli,HC_KEY k);

function  cf_drawlinks(sli,p_ptr) result (rc) bind(C,name="cf_drawlinks")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  flinkList_f
    implicit none
    integer(C_INT), intent(in),value :: sli 
    integer(kind=cptrsize) , value :: p_ptr
     integer(C_INT) ::rc  
 
    rc= DrawFlinks (sli,p_ptr)

end function cf_drawlinks
function  CF_insert_flink(p_sn,p_N ,p_n1,p_n2,p_ea,p_zi,p_ti, p_mass,p_ptr) result (rc) bind(C,name="cf_insert_flink")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  flinkList_f
    implicit none
    integer(C_INT), intent(in),value :: p_sn 
    integer(C_INT), intent(in),value :: p_N ,p_n1,p_n2
    real(c_double), intent(in),value ::p_ea,p_zi,p_ti,p_mass
    integer(kind=cptrsize) ,intent(in), value :: p_ptr
    integer::err,nn
    integer(C_int) ::rc  
    nn=p_N
    err=0
    rc= CreateFortranFlink (p_sn, p_N ,p_n1,p_n2,p_ea,p_zi,p_ti, p_mass,p_ptr,err) ! should place it at N
end  function   CF_insert_flink

! extern "C" int cf_flinkcount(const int sli  );
function   cf_flinkcount(sli) result (t) bind(C,name="cf_flinkcount")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  flinkList_f
    implicit none
    integer(C_int) ,intent(in), value :: sli
    integer(C_int)  ::t
    t=saillist(sli)%flinks%count
end  function cf_flinkcount

! extern "C" double cf_getflinktension(const void*fptr);
function   cf_getflinktension(sli,nn) result (t)  bind(C,name="cf_getflinktension")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  flinkList_f
    implicit none
    integer(C_int) ,intent(in), value :: sli,nn
    real(c_double)  ::t 
    TYPE(flink),pointer  ::FL 
    fl=>saillist(sli)%flinks%list(nn)
    t = FL%m_TLINK
end  function cf_getflinktension

!extern "C" int cf_setflinkproperties(const int sli, const int n, const double ea,const double ti,const double mass);
! // returns 1 if something is different
function cf_setflinkproperties(sli, nn,ea,ti, mass) result (rc) bind(C,name="cf_setflinkproperties")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    USE  flinkList_f
    implicit none
    integer(C_int) ,intent(in), value :: sli,nn
    real(c_double),intent(in), value :: ea,ti, mass
    TYPE(flink),pointer  ::FL 
    integer(c_int) :: rc
    rc=0
    fl=>saillist(sli)%flinks%list(nn)
    if(abs( fl%m_EA - ea) > 1e-10)          rc=1
    if(abs( fl%m_ti - ti) > 1e-10)          rc=1
    if(abs( fl%m_mul - mass) > 1e-10)     rc=1
   
!    if(rc>0)    write(outputunit,'(a,i7,a ,3g12.3,a ,3g12.3)') 'flink ', fl%n, ' ea,ti,m were ',fl%m_ea,fl%m_ti,fl%m_mul,' now ',ea,ti,mass
 
    fl%m_ea = ea  
    fl%m_ti= ti        
    fl%m_mul= mass       
end  function cf_setflinkproperties

!extern "C" int cf_getflinkproperties(const int sli, const int n, double *ea,double *ti, double*zi, double *mass); // returns 0 if failed
function cf_getflinkproperties(sli, nn,ea,ti,zi, mass) result (rc) bind(C,name="cf_getflinkproperties")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    USE  flinkList_f
    implicit none
    integer(C_int) ,intent(in), value :: sli,nn
    real(c_double),intent(out)  :: ea,ti, mass,zi
    TYPE(flink),pointer  ::FL 
    integer(c_int) :: rc
    rc=1
    fl=>saillist(sli)%flinks%list(nn)
    ea= fl%m_ea    
    ti= fl%m_ti    
    zi = fl%m_Zlink0   
    mass = fl%m_mul      
end  function cf_getflinkproperties

!extern "C" int cf_setflinklength(const void*fptr, const double zi); // ZI is in analysis units (metres)
function   cf_setflinklength(sli,nn,zi) result (rc)  bind(C,name="cf_setflinklength")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    USE  flinkList_f
    implicit none
    integer(C_int) ,intent(in), value :: sli,nn
    real(c_double),intent(in), value ::zi 
    TYPE(flink),pointer  ::FL 
    integer(c_int) :: rc
    rc=0
    fl=>saillist(sli)%flinks%list(nn)
    if(abs( fl%m_Zlink0 - zi) > 1e-10) then
    !  write(outputunit,'(a,i7,a,g12.5,a,g12.5)') ' flink ', fl%n, ' length was ',FL%m_Zlink0 ,' is ', zi
     rc=1
    endif
    fl%m_Zlink0 = zi

end  function cf_setflinklength

integer (c_int) function  CF_insert_tri3(p_sn,p_N ,p_ptr) bind(C,name="cf_insert_tri3")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  triList_f
    implicit none
    integer(C_INT), intent(in),value :: p_sn 
    integer(C_INT), intent(in),value :: p_N 
    integer(kind=cptrsize),intent(in) :: p_ptr
    integer::err,nn
    
    nn=p_N
    err=0
    call CreateFortranTri (p_sn, p_N ,p_ptr,err) ! should place it at N
    CF_insert_tri3 = nn
end  function   CF_insert_tri3


integer (c_int) function  CF_remove_tri3(p_sn,p_N ) bind(C,name="cf_remove_tri3")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  triList_f
    implicit none
    integer(C_INT), intent(in),value :: p_sn 
    integer(C_INT), intent(in),value :: p_N 
    integer ::err; err=0
    
    call RemoveFortranTri (p_sn, p_N,err)
    if (err ==0) then
       CF_remove_tri3=1
    else
    write(*,*) ' RemoveFortranTri err= ',err
        CF_remove_tri3=0    
    endif
end function  CF_remove_tri3
integer(c_int) function cf_ReserveForJanet(sli,n) result (ok) bind(C,name="cf_reserveforjanet")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    USE  stringList_f
    implicit none
    integer(C_INT), intent(in),value :: sli,n 
    ok =  ReserveEdgeSpace(sli, n) + ReserveStringSpace(sli, n) 
end function cf_ReserveForJanet

function  CF_insert_edge(p_sn,p_N ,p_ptr) result(nn) bind(C,name="cf_insert_edge")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    implicit none
    integer(C_INT), intent(in),value :: p_sn 
    integer(C_INT), intent(in),value :: p_N 
    integer(kind=cptrsize) :: p_ptr
    integer (c_int)::err,nn
    
    nn=p_N
    err=0
    call CreateFortranEdge (p_sn, p_N ,p_ptr,err) ! should place it at N

	saillist(p_sn)%edges%list(NN)%m_Dlink=0.0
	saillist(p_sn)%edges%list(NN)%m_Delta0=0.0
	saillist(p_sn)%edges%list(NN)%m_tlink=0.0		
	saillist(p_sn)%edges%list(NN)%N=NN
	saillist(p_sn)%edges%list(NN)%m_Eused= .true.
 
end  function   cf_insert_edge
!extern "C" 	int    cf_set_edgenodes(const int sli,const int n,const int n1,const int n2);
function  cf_set_edgenodes(p_sn,nn ,p_n1,p_n2) result(ok) bind(C,name="cf_set_edgenodes")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    implicit none
    integer(C_INT), intent(in),value :: p_sn ,nn
    integer(C_INT), intent(in),value :: p_n1, p_n2

    integer (c_int)::ok
    ok=1
	saillist(p_sn)%edges%list(NN)%m_L12(1) = p_n1; 
	saillist(p_sn)%edges%list(NN)%m_L12(2) = p_n2; 
	saillist(p_sn)%edges%list(NN)%m_Eused= .true.
 	saillist(p_sn)%nodes%xlist(p_n1 )%m_ElementCount =saillist(p_sn)%nodes%xlist(p_n1 )%m_ElementCount  +1  
 	saillist(p_sn)%nodes%xlist(p_n2 )%m_ElementCount =saillist(p_sn)%nodes%xlist(p_n2 )%m_ElementCount  +1  
g_PleaseHookElements=.true.
end  function   cf_set_edgenodes



integer (c_int) function  CF_remove_edge(p_sn,p_N )  bind(C,name="cf_remove_edge")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    implicit none
    integer(C_INT), intent(in),value :: p_sn 
    integer(C_INT), intent(in),value :: p_N 
    integer ::err; err=0
    
    call RemoveFortranEdge (p_sn, p_N,err) 
    if (err ==0) then
       CF_remove_edge=1
    else
        CF_remove_edge=0    
    endif
end function  CF_remove_edge

!extern "C" int  cf_get_edgenodeno( const int sli,const int n, const int k);
 function  CF_get_edgenode(sli,n,  k) result( p) bind(C,name="cf_get_edgenode")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    use cfromf_f  ! for zeroptr
    implicit none
    integer(C_INT), intent(in),value :: sli 
    integer(C_INT), intent(in),value :: n,k 
     type(fedge), pointer ::e
     type(fnode),pointer ::thenode
     integer :: nodeno
     integer(kind=cptrsize) ::p
     p=0     
     e=>saillist(sli)%edges%list(N)
     if(.not. e%m_Eused) then
   
         return 
     endif 
    
    nodeno = e%m_L12(k+1)
    thenode=> saillist(sli)%nodes%xlist(nodeno)
    if(.not. thenode%m_Nused)  return
    p = thenode%m_RXFEsiteptr
    
end function  CF_get_edgenode

!extern "C"  double cf_get_edgelength(const int sli,const int n);
real (C_DOUBLE) function CF_get_edgelength(sli, n) bind(C,name="cf_get_edgelength")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli 
    integer(C_INT), intent(in),value :: n
    cf_get_edgelength =	 saillist(sli)%edges%list(N)% m_ZLINK0 
end function cf_get_edgelength

!extern "C" 	int    cf_set_edgeflag(const int sli,const int n,const int i, const double f);
integer (c_int) function  cf_set_edgeflag(sli, n,i,f) bind(C,name="cf_set_edgeflag")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    integer(C_INT), intent(in),value :: sli,n,i
    real (C_DOUBLE) , intent(in), value :: f  !  initial chord/arc 
	saillist(sli)%edges%list(N)%IsOnSurf=(i .ne. 0)
	saillist(sli)%edges%list(N)%ChOverArcInit = f	
    cf_set_edgeflag =1
end function  cf_set_edgeflag
!extern "C" 	int    cf_set_edgelength(const int sli,const int n,const double p_zi);
integer (c_int) function  cf_set_edgelength(sli, n,p_zi) bind(C,name="cf_set_edgelength")
    USE, INTRINSIC :: ISO_C_BINDING
    USE  edgeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
    real (C_DOUBLE) , intent(in), value :: p_zi;
 
	saillist(sli)%edges%list(N)% m_ZLINK0=p_zi   

    cf_set_edgelength =1
end function  cf_set_edgelength

 integer (c_int) function CF_CreateFortranNode(sli,n,ptr,err) bind(C,name= "cf_createfortrannode")
    USE,INTRINSIC :: ISO_C_BINDING
    USE nodeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
    integer(kind=cptrsize) ,  intent(in),value  :: ptr  ! Site *
    integer(C_INT), intent(out)  :: err  

    err =0;
    call CreateFortranNode(sli,n,ptr,err)
    CF_CreateFortranNode=err
end function CF_CreateFortranNode

! EXTERN_C int cf_issixnode(const int sli, const int i );
function cf_issixnode(sli,n )  result(rc)  bind(C,name="cf_issixnode")
    USE,INTRINSIC :: ISO_C_BINDING
    USE nodeList_f
    use saillist_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
     integer(C_INT) :: rc  
    rc= IsSixNode(saillist(sli)%nodes%xlist(n))
 end function cf_issixnode
! EXTERN_C int 	cf_makesixnode(const int sli,const int n,const int n2);
function cf_makesixnode(sli,n1,n2 )  result(rc) bind(C,name="cf_makesixnode")
    USE,INTRINSIC :: ISO_C_BINDING
    USE nodeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n1,n2
     integer(C_INT) :: rc  
    saillist(sli)%nodes%xlist(n1)%sixnoderef   = n2
     saillist(sli)%nodes%xlist(n2)%sixnoderef  =-n1 
     saillist(sli)%nodes%xlist(n2)%m_ElementCount =saillist(sli)%nodes%xlist(n2)%m_ElementCount+1 
     rc=1
 end function cf_makesixnode

 !extern "C"  void cf_set_nodalload(const int sli, const int i, const double *p);
 subroutine  set_nodalload(sli,n,p) bind(C,name="cf_set_nodalload")
     USE, INTRINSIC :: ISO_C_BINDING
     use saillist_f
     USE  nodeList_f
     implicit none
     integer(C_INT), intent(in),value :: sli
     integer(C_INT), intent(in),value :: n
     real(C_DOUBLE), intent(in), dimension(3) :: p
     saillist(sli)%nodes%xlist(n)%m_Nodal_Load=p
 end subroutine  set_nodalload

!extern "C"  void cf_get_nodalload(const int sli, const int i, double *p);
subroutine  get_nodalload(sli,n,p) bind(C,name="cf_get_nodalload")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    USE  nodeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n 
    real(C_DOUBLE), intent(out), dimension(3) :: p
    p =  saillist(sli)%nodes%xlist(n)%p ! m_Nodal_Load
end subroutine  get_nodalload

! extern "C"  void cf_set_trace( const int sli, const int nn,const int i);
subroutine cf_set_trace(sli,n,i) bind(C,name="cf_set_trace")
    USE, INTRINSIC :: ISO_C_BINDING
    use saillist_f
    USE  nodeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n,i
    saillist(sli)%nodes%xlist(n)%m_trace = (i .ne. 0)

end subroutine cf_set_trace

function cf_FillFNode(sli,n,x,err) result(rv) bind(C,name="cf_FillFNode"  )  ! X is in model space
    USE, INTRINSIC :: ISO_C_BINDING
    USE  nodeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
    real(C_DOUBLE) , intent(in),dimension(3)  :: x
    integer(C_INT), intent(out)  :: err  
    integer(c_int) :: rv

    err =0;
    call FillFNode(sli,n,x,err) ! must unresolve any connects referring to this node.
    rv=err
end function cf_FillFNode
function CF_removefortrannode(sli,n,err) result(rv) bind(C,name=  "cf_removefortrannode" )
    USE, INTRINSIC :: ISO_C_BINDING
    USE  nodeList_f
    implicit none
    integer(C_INT), intent(in),value :: sli
    integer(C_INT), intent(in),value :: n
    integer(C_INT), intent(out)      :: err  

   integer(c_int) :: rv
    err =0;
    call RemoveFortranNode(sli,n,err)
    rv=err

end function CF_removefortrannode

subroutine cf_UpdateAllTeesBySV() bind(C,name="cf_updateallteesbysv")
    call UpdateAllTeesBySV()
end subroutine cf_UpdateAllTeesBySV

!function cfDestroyDofo (sn, NN)  result(err) bind(C,name="cf_de lete_dofo")
!    USE, INTRINSIC :: ISO_C_BINDING
!    use dofoprototypes_f
!
!    INTEGER (c_int), intent(in),value :: sn,nn
!    INTEGER(c_int)  :: err
 !   err= DestroyDofo(sn,nn)
!end function  cfDestroyDofo

! extern "C"  int cf_test (const int*vv ,const int n);
function  cftest (vv, N)  result(rv) bind(C,name="cf_test")
    USE, INTRINSIC :: ISO_C_BINDING
     INTEGER (c_int), intent(in),value :: n
    INTEGER (c_int), intent(in), dimension(n) :: vv

    INTEGER(c_int)  :: rv
    rv=32
    call beamtest(vv,n,rv)

end function  cftest
! EXTERN_C int  cf_elementTest(double* a, double*b,double*c);
function  cf_elementTest (a,b,c)  result(rv) bind(C,name="cf_elementTest")
    USE, INTRINSIC :: ISO_C_BINDING
    use solveimplicit_f
     real (c_double), intent(in),dimension(*) :: a,b,c
     INTEGER(c_int)  :: rv
    rv=32
    call elementtest(a ,b,c,rv)
end function  cf_elementTest
