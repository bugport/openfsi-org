!C  28/4/97   in  Read_One_Model, 'error' should be type I4 not logical 
!C  27/4/96 openlogfile finally removed. SOme DBG writes temporarily
!C   29/11/94 areacalc added as debug 
!C  26/11/94 Lifting Line enabled
!C 
!x_   date: Saturday  8/1/1994
!x_   translated from LAHEY FFTOSTD output
!x_
!x_   date: Saturday  8/1/1994
!x_   translated from LAHEY FFTOSTD output
!x_

  
subroutine caff()
    use basictypes_f  ! for outputunit
    IMPLICIT NONE
    INTEGER i,c
    LOGICAL*4 o, n
    character*128 name
    c=0
	DO i=7,1024
		inquire(unit=i,opened=o,named=n,name=name,err=10)
		if(o) Then
			c=c+1
!c 			write(outputunit,*) 'file ',i,' is OPENED'
			if(n) then
!c 				write(outputunit,*) 'name is ',name
			else
				write(outputunit,*) 'NO NAME '
			endif
			CLOSE(i,err=10)
!c 			write(outputunit,*) ' CLOSING ',i
		endif
	goto 11
10	write(outputunit,*) 'error inquiring on ',i
11	continue
	enddo
!c          write(outputunit,*) c,' files were open '
	end subroutine caff

subroutine RELAX32(conv,V_Damp,flags) ! ONLY EVER CALL for debugging.
    USE, INTRINSIC :: ISO_C_BINDING
      IMPLICIT NONE
      CHARACTER*256 :: flags 
      INTEGER retval 
      real(c_double):: conv,V_Damp

     	CALL RLX_GOGO(retval,conv,V_Damp,Flags) ! files all active models RLX_GO(flags,iexit,tmax,R_Damp,V_Damp) 

	END

!******************************************************************
SUBROUTINE RLX_GOGO(iexit,tmax,V_Damp,flags)  ! loses batten info
   use relaxsq_f 
	USE  saillist_f
	use shapegen_f
	use ttmass_f
	use rx_control_flags_f
	use cfromf_f
	use math_f
    IMPLICIT NONE
    CHARACTER(len=*), intent(in) :: flags
    real(kind=double), intent(in) ::tmax,V_Damp
    INTEGER, intent(out) :: Iexit     
    integer ::sli
    type(sail), pointer :: s

    LOGICAL No_Wind , l_SC ,Shape_Has_Changed
    logical  LL
    logical::  debugging =.false.
    INTEGER  Iret,k
    LOGICAL NoShape,  LL_Exp
    REAL*8 test_A0, testpressure
    common /idealinc/ test_A0

	CALL FlagSet(flags)
    call TestFloatingPoint()
  ! call  KMP_SET_LIBRARY_TURNAROUND()
     CALL CHECK_CL('ID',LL_Exp)
     Test_A0 = -1.0D00
     if(LL_Exp) THEN
        write(outputunit,*)' enter A0 '
        read(*,*) Test_A0
      ENDIF
#ifdef WINTEST
!    debugging =.true.
!    testpressure=-0 ! 50
#endif
        CALL CHECK_CL('LL',LL)
	if(LL) then
		write(outputunit,*) ' are you sure that AWA_Fact is 0'
	endif

        l_SC= .FALSE.

        CALL CHECK_CL('SC',l_SC)
        CALL CHECK_CL('NW',No_wind)
        CALL CHECK_CL('NS',NoShape)

      Iexit = 1
      Shape_Has_Changed = .TRUE.
loop1: DO WHILE ((IExit .LT. 2) .AND.  Shape_Has_Changed)

        IEXIT = 1
loop2:  DO WHILE (iexit .eq. 1)
 ifnw:       IF (.false. ) THEN
! ifnw:     IF (.not. No_wind) THEN       

	    else if (debugging) then ifnw

		 	write(outputunit,*) ' debug - pressure= ', testpressure
		 	do sli=1,sailcount
		 	    s=>saillist(sli)
		 	    if(s%tris%count>0) s%tris%list%prest=testpressure
		 	enddo
        ENDIF  ifnw
 
!C         CALL RUN DR(500,tmax,iexit,R)  just to show its possible 
		   ! write(outputunit,*) ' now runDR'
            CALL RUNDR(150000,tmax, iexit,V_Damp)
		call  Post_Relax_Forces()

        ENDDO loop2

   ifn3:  IF (Iexit .ne.2) THEN	! Iexit 0=OK, 1=not conv, 2=user abort
               Shape_Has_Changed = .FALSE.
   ifnns2:      if(.not.NoShape) then
                   do sli=1,sailcount
                       s=>saillist(sli)
                       if(is_Active(sli)) THEN
                           CALL Measure_Tolerances(sli,Iret)
                           Shape_Has_Changed= ( Iret .ne. 0)
                       ENDIF
                   ENDDO
                   Shape_Has_Changed = Shape_Has_Changed .AND. l_SC
                   ! do this early because anchor_loads uses it
                   if(.not. Shape_Has_Changed) k= Post_Summary_By_SLI(0,"Status"//char(0),"1 Converged"//char(0))
                   open(501,file='totalforces.txt')
                   call Print_Force_Totals(501)
                   close(501)
                   CALL anchor_loads(tmax)
               ENDIF ifnns2
          endif ifn3

      ENDDO loop1
      END SUBROUTINE RLX_GOGO


      SUBROUTINE get_our_cl(a)
	use parse_f
	implicit none
      CHARACTER*(*) a
      character*256 comline
      common/qwerty/comline
      a = comline
	call denull(a)
	

      END SUBROUTINE get_our_cl

      SUBROUTINE putcl(a)
	use parse_f
	implicit none
      CHARACTER*(*) a
      character*256 comline
      common/qwerty/comline
      comline = a
	call denull(comline)
     
      END
  	SUBROUTINE UPC(s1,s2)
	CHARACTER*(*) s1,s2
	integer i,c
	s2=s1
	do i=1,len_trim(s2)
		c = ichar(s2(i:i))
		if(c >= ichar('a') .and. c <= ichar('z')) then
			s2(i:i) = char(c + ichar('A') - ichar('a'))
		 endif
	enddo
	END SUBROUTINE UPC  
