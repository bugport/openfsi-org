http://www.qtcentre.org/wiki/index.php?title=Undocumented_qmake#Custom_tools
There is also a .CONFIG property, which itself has multiple special flags you can set using syntax identical to the main CONFIG variable:

        combine � call the compiler with a list of all the files in the source variable, rather than once for each file,
        explicit_dependencies � The comment in the source reads "compiler.CONFIG+=explicit_dependencies means that ONLY compiler.depends gets to cause Makefile dependencies",
        function_verify � see also .verify_function above,
        ignore_no_exist � do not generate an error (warning?) if the files in the source variable do not exist,
        moc_verify � I *think* this makes sure that the file should be run through the moc preprocessor before adding it as a moc target.
        no_dependencies � do not do dependency generation on the files in the source variable,
        no_link � the files that are created should not be added to OBJECTS � i.e., they are not compiled code which should be linked,
        target_predeps � I *think* this makes sure that the custom compiler is run as the first thing in the project...
        verify � not sure what this does... 


make: Entering directory `/home/r3/Documents/qt/openFSI/ofsifortran'
basictypes.f90 \
bar_elements.f90 
cfromf_declarations.f90 \
dofo_subtypes.f90 \
linklist.f90 \
ftypes.f90 \
dofoprototypes.f90 \
fglobals.f90 \
gletypes.f90 \
parse.f90 \
realloc.f90 \
rlxflags.f90 \
vectors.f90 \
saillist.f90 \
coordinates.f90 \
batstuf.f90 \
mathconstants.f90 \
math.f90 \
batn_els.f90 \
invert.f90 \
links_pure.f90 \
batten.f90 \
edgelist.f90 \
kbat_els.f90 \
kbatten.f90 \
stiffness.f90 \
battenf90.f90 \
beamelementtype.f90 \
sf_vectors.f90 \
nodalrotations.f90 \
tanpure.f90 \
beamElementMethods.f90 \
hoopsinterface.f90 \
nodelist.f90 \
beamelementlist.f90 \
bspline.f90 \
connectstuf.f90 \
dofoarithmetic.f90 \
removeelement.f90 \
fnoderef.f90 \
fspline.f90 \
stringsort.f90 \
fstrings.f90 \
rxfoperators.f90 \
stringlist.f90 \
connects.f90 \
dofolist.f90 \
dofo.f90 \
flinklist.f90 \
gle.f90 \
wrinkleFromMathematica.f90 \
nonlin.f90 \
tris.f90 \
feaprint.f90 \
nastraninterface.f90 \
plotps.f90 \
trilist.f90 \
f90_to_c.f90 \
flagset.f90 \
flopen.f90 \
Fslidecurve.f90 \
gravity.f90 \
isnan.f90 \
msvc_fortran_utils.f90 \
mtkaElement.f90 \
nlstuf.f90 \
nograph.f90 \
oonatristuf.f90 \
sf_tris_pure.f90 \
ttmass9.f90 \
rximplicitsolution.f90 \
relaxsq.f90 \
shapegen.f90 \
relax32.f90 \
shape.f90 \
solvecubic.f90 \
solve.f90 \
spline.f90 \
sspline.f90 \
tohoops.f90 \
tris_pure.f90 \
