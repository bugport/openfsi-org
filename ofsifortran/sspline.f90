!   date: Saturday  8/1/1994
!   translated from LAHEY FFTOSTD output
!
      SUBROUTINE SPLINE(XGIVEN,YGIVEN,XP,YP,NG,NP)
      use basictypes_f  ! for outputunit
      IMPLICIT NONE
! PARAMETERS
      real*8 xgiven(*) ,Ygiven(*)
      INTEGER ng,np
!returns
      real*8 xp(*),yp(*)

!
!   THIS IS A SPLINE ROUTINE WHICH FITS A FLEXIBLE PIECEWISE
!   SPLINE THROUGH THE GIVEN POINTS AND EVALUATES THE 'POINT' POINTS.
!
!   IT ALSO CONTAINS THE 'FAST' SPLINE AND THREEDEE ROUTINES
!
!      IMPLICIT REAL*8(A-H,O-Z),INTEGER(I-N)
!
!
!    XG[1]=0  XG[NG]=L  YG[1]=YG[NG]=0
!
      REAL*8 XG(ng),YG(ng),G(ng,ng),Y(ng),W(ng),C(ng),D(ng),E(ng)
      REAL*8 L ,Xs,Ys,xf,yf,grad, A, x, yy
      INTEGER I,nggg, iold, j

      COMMON/CHECK/I,NGGG

      G=0.0
      NGGG=NG
      DO 3 I=1,NP
3      YP(I)=0.0
!
      IF(NG.GT.ng) THEN
      write(outputunit,*)' FATAL ERROR IN BSPLINE'
      write(outputunit,*)' More than ',ng,' given points'
      write(outputunit,*)' Number of given points is',NG
      STOP
      ENDIF
!
      XS=XGIVEN(1)
      YS=YGIVEN(1)
      XF=XGIVEN(NG)
      YF=YGIVEN(NG)
      GRAD=(YF-YS)/(XF-XS)
      DO 6 I=1,NP
6      XP(I)=XP(I)-XS
      DO 7 I=1,NG
      XG(I)=XGIVEN(I)-XS
7      YG(I)=YGIVEN(I)-YS-XG(I)*GRAD
      L=XG(NG)
!
      IOLD=0
!     IF(NGOLD.NE.NG) IOLD=1
!      DO 5 I=1,NG
!  5   IF(XG(I) .NE. XGOLD(I)) IOLD=1
      iold=1
!
!   IOLD IS 0 IF WE CAN USE OLD A,Y
!
      IF(IOLD .EQ. 0) THEN
!
!      write(outputunit,*)'SPLINE: USING OLD A'
!      DO 2 I=1,NG
!  2   XG(I)=XGOLD(I)
!      DO 4 I=1,NG-2
!      DO 4 J=1,NG-2
!  4   G(I,J)=GOLD(I,J)

      ELSE

      DO 10 I=2,NG-1
!                         I IS SPLINE NUMBER
      A=XG(I)
      CALL SETPAR(L,A,C(I),D(I),E(I))
      DO 10 J=2,NG-1
      X=XG(J)
      CALL EVAL(X,G(J-1,I-1),L,A,C(I),D(I),E(I))
!  GOLD(J-1,I-1)=G(J-1,I-1)
10	  CONTINUE
!
      ENDIF
!
      DO 700 I=2,NG-1
700    Y(I-1)=YG(I)
!       NGOLD=NG
!      DO 600 I=1,NG
! 600  XGOLD(I)=XG(I)
      CALL SOLVE(NG-2,G,Y,W,ng)
!
      DO 50 J=1,NP
      X=XP(J)
      IF(X.EQ.L) THEN
      YP(J)=YG(NG)
      ELSE

      DO 51 I=2,NG-1
      A=XG(I)
      CALL EVAL(X,YY,L,A,C(I),D(I),E(I))
51     YP(J)=YY*W(I-1)+YP(J)
      ENDIF
50     CONTINUE
!
      DO 8 I=1,NP
      YP(I)=YP(I)+YS+XP(I)*GRAD
8      XP(I)=XP(I)+XS
      RETURN
      END

      SUBROUTINE SETPAR(L,A,C,D,E)
      use basictypes_f  ! for outputunit
      IMPLICIT NONE
      REAL*8 L,A,C,D,E
      integer I,nggg
      COMMON/CHECK/I,NGGG

      IF (L .EQ. A) THEN
      WRITE(0,*)' POINT IS AT END POINT'
      write(outputunit,*)' I,NG,L,A',I,NGGG,L,A
      STOP
      ENDIF
      D=-A*A/6.0-L*L/3.0
      E=-L*(L*L/3.0+D)
      C=A/(L-A)*(L*A/2.0+D)
      RETURN
      END
      SUBROUTINE EVAL(X,RET,L,A,C,D,E)
      IMPLICIT NONE
      REAL*8 X,RET,L,A,C,D,E
      IF( X .LE. A) THEN
      RET=(L-A)* X*(X**2/6.0+C) /L
      ELSE
      RET=A/L* (X* (X* (L/2.0-X/6.0) +D) +E)
      ENDIF
      END
