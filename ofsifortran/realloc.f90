MODULE realloc_f
INTERFACE reallocate
	MODULE PROCEDURE reallocatedouble
        MODULE PROCEDURE reallocateInt
!	MODULE PROCEDURE reallocatesail moved to saillist
	MODULE PROCEDURE reallocateXlist
	MODULE PROCEDURE reallocateElist
	MODULE PROCEDURE reallocateTlist
	MODULE PROCEDURE reallocateStringlist
	MODULE PROCEDURE reallocateConnectList
	MODULE PROCEDURE reallocateNConnType
	MODULE PROCEDURE reallocateGLEList
	MODULE PROCEDURE reallocatedofoholderList
	MODULE PROCEDURE reallocateBattenList
	MODULE PROCEDURE reallocateFLinkList
	MODULE PROCEDURE reallocateFbeamList
	module procedure reallocatedoforef
	module procedure reallocatefnoderef_p 
END INTERFACE 
CONTAINS


	SUBROUTINE reallocateXlist(list, nin, error)
	USE ftypes_f
	IMPLICIT NONE
	TYPE (Fnode), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of list
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (Fnode), dimension(UBOUND(list,1) ) :: temp
	extent = UBOUND(list,1) 
	if(extent > 1000000) then
		write(outputunit,*) ' big reallocate XList',extent
	endif
	n = nin
	if(n < 1) THEN
		write(outputunit,*) ' reallocate Xlist too small n =',n
		n=1
	ENDIF
	if(.not. associated(list) ) then
		write(outputunit,*) ' xlist not associated '
		! pa use
	else
		temp = list
	endif					! OldExtent items
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	extent = min(extent, n)
	list(1:extent) = temp(1:extent) 
	if(ubound(list,1) > extent) then
 !!!dir$ loop count min(512) intel compiler 2015 raises a warning
		list(extent+1:)% m_Nused=.false.  ! May 2003 
	endif
	END SUBROUTINE reallocateXlist


	SUBROUTINE reallocateElist(Elist, nin, error) ! copied not checked
	USE ftypes_f
	IMPLICIT NONE
	TYPE (Fedge), dimension(:) ,POINTER :: Elist
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of Elist
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (Fedge), dimension(UBOUND(Elist,1) ) :: temp
	extent = UBOUND(Elist,1) 
        if(extent > 100000000) then
		write(outputunit,*) ' dodgy reallocate EList',extent
	endif	
	n = nin
	if(n < 1) THEN
		write(outputunit,*) ' reallocateElist too small n =',n,' BUT WHY??????'
		n=1
	ENDIF

	temp = Elist					! OldExtent items
	DEALLOCATE(Elist,stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	ALLOCATE( Elist(n),stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	extent = min(extent, n)
	Elist(1:extent) = temp(1:extent) 
	if(ubound(Elist,1) > extent) then
		Elist(extent+1:)%m_Eused=.false.  ! May 2003 
	endif
	END SUBROUTINE reallocateElist
 	
SUBROUTINE  reallocateFLinkList(list, nin, error) ! copied not checked
	USE ftypes_f
	IMPLICIT NONE
	TYPE (FLink), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of Elist
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it

	TYPE (FLink), dimension(UBOUND(list,1) ) :: temp
    n = nin
    if(n < 1) THEN
	    write(outputunit,*) ' reallocate links too small n =',n,' BUT WHY??????'
	    n=1
    ENDIF
	if(associated(list)) then
	    extent = UBOUND(list,1) 
	    temp = list					! OldExtent items
	    DEALLOCATE(list,stat=istat)
	    if(istat /=0) THEN
		    error = 1
		    return
	    endif
	 else
	   extent=0 
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	extent = min(extent, n)
	list(1:extent) = temp(1:extent) 
	if(ubound(list,1) > extent) then
		list(extent+1:)%m_used=.false.  
	endif
END SUBROUTINE reallocateFLinkList

SUBROUTINE  reallocateFbeamList(list, nin, error) ! copied not checked
	USE ftypes_f
	IMPLICIT NONE
	TYPE (beamelement), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of Elist
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (beamelement), dimension(UBOUND(list,1) ) :: temp
	    n = nin	
	    error=0
	    if(n < 1) THEN
		    write(outputunit,*) ' reallocate beamlist too small n =',n,' BUT WHY??????'
		    n=1
	    ENDIF	    
	    
	if(associated(list)) then
	    extent = UBOUND(list,1) 
	    temp = list					! OldExtent items
	    DEALLOCATE(list,stat=istat)
	    if(istat /=0) THEN
		    error = 1
		    return
	    endif
	 else
	   extent=0 
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	extent = min(extent, n)
	list(1:extent) = temp(1:extent) 
	if(ubound(list,1) > extent) then
		list(extent+1:)%m_used=.false.  
	endif
END SUBROUTINE reallocateFbeamList

SUBROUTINE reallocateTlist(list, nin, error) ! copied not checked
	USE ftypes_f
	IMPLICIT NONE
	TYPE (Tri), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of Tlist
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (Tri), dimension(UBOUND(list,1) ) :: temp
	extent = UBOUND(list,1) 
        if(extent > 10000000) then
		write(outputunit,*) ' dodgy reallocate TList',extent
	endif	
	n = nin
	if(n < 1) THEN
		write(outputunit,*) ' reallocatesail to small n =',n
		n=1
	ENDIF

	temp = list					! OldExtent items
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		error = 1
		return
	endif

	extent = min(extent, n)
	list(1:extent) = temp(1:extent) 
	if(ubound(list,1) > extent) then
		list(extent+1:)%used=.false.  ! May 2003 
	!else 
	!	write(outputunit,*) ' cant set used on extent+1 in tlist'
	endif

END SUBROUTINE reallocateTlist


SUBROUTINE reallocateStringList(list, nin, error) 
	USE ftypes_f
	IMPLICIT NONE
	TYPE (stringholder), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n, old_n,k
!
! A) Getting bigger. n > extent of StringList
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it

	type (string), pointer :: t
	TYPE (stringholder), dimension(: ) ,allocatable :: temp
	if(associated(list)) then
		extent = UBOUND(list,1)  
	else 
		extent=0
	endif
	old_n = extent; n = nin

	if(associated(list) ) then
		allocate(temp(old_n),stat=istat)
		temp = list					! OldExtent items
		DEALLOCATE(list,stat=istat)
		if(istat /=0) THEN
			write(outputunit,*) ' deallocate failed in  reallocateStringList n_old = ', old_n
			error = 1
			deallocate(temp,stat=istat)
			return
		endif
	else
		extent =0  ; old_n = extent
	endif
	! write(outputunit,*) ' reallocate String list from ', old_n,' to ', nin
	if(n .eq. 0) then
		NULLIFY(list); 
	else
		ALLOCATE( list(n),stat=istat)
		if(istat /=0) THEN
			error = 1
			deallocate(temp,stat=istat)
			return
		endif
		extent = min(old_n, n)
		if(extent > 0) list(1:extent) = temp(1:extent) 
	endif
	if( n > old_n) then
		if(old_n > 0) then
			if(.not. associated(list(old_n)%o)) then
				 ALLOCATE(list(old_n)%o)
				write(outputunit,*) ' fixup on string ', old_n
                                !pause
			endif
		endif
		do k= old_n+1,n
			ALLOCATE(list(k)%o)
			t=>list(k)%o
			t%used=.false. ! ; write(outputunit,*) '    alloc for string ',k
			nullify(t%se);
			nullify(t%rev); 
			nullify(t%tq_s);
			t%m_Connects=>NULL()
			t%m_sli=-9
		enddo
	else  ! n the same or smaller
		do k= n+1, old_n
			 DEALLOCATE(temp(k)%o)
			 nullify(temp(k)%o)
		enddo
	endif
    error = 0
END SUBROUTINE reallocateStringList

SUBROUTINE reallocateConnectList(list, nin, error) ! copied not checked
	USE ftypes_f
	IMPLICIT NONE
	TYPE (ConnectHolder), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n,old_n,k
!
! A) Getting bigger. n > extent of  List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (ConnectHolder), pointer, dimension(:) :: l_temp
	NULLIFY(l_temp)

	n = nin
	error = 0
	if(n < 1) THEN
		write(outputunit,*) ' reallocate COnnect too small n =',n
		n=1
	ENDIF
	if(associated(list)) then
		extent = UBOUND(list,1) ; old_n = extent
		allocate(l_temp(extent))
		l_temp = list					! OldExtent items
		DEALLOCATE(list,stat=istat)
		if(istat /=0 .and. old_n /=0) THEN
			write(outputunit,*) ' deallocate connect failed extent was ',old_n
			error = 1
			return
		endif
	else
		!write(outputunit,*) 'reallocateConnectList on not assoc. N = ', nin
		extent=0; old_n=0
	endif
	
	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' allocate connect failed '
		error = 1
		return
	endif

	extent = min(extent, n)
	if(associated(l_temp) ) list(:extent) = l_temp(:extent) 
	if( n > old_n) then
		do k= old_n+1,n
			ALLOCATE(list(k)%o)
			list(k)%o%used=.false. 
		enddo
	elseif(associated(l_temp)) then ! n the same or smaller
		do k= n+1, ubound(l_temp,1)
			DEALLOCATE(l_temp(k)%o)
		enddo
	else
		write(outputunit,*) ' neither n > oldN nor allocated temp'
	endif
		
	if(associated(l_temp) ) deallocate(l_temp)
END SUBROUTINE reallocateConnectList

SUBROUTINE reallocateNConnType(list, nin, error) 
	USE ftypes_f

	IMPLICIT NONE
	TYPE (NConnType), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it


	TYPE (NConnType), dimension(UBOUND(list,1) ) :: temp
	extent = UBOUND(list,1) 
	if(extent > 100000) then
		write(outputunit,*) ' dodgy reallocate NConnType ',extent
	endif	
	n = nin
	error = 0
	if(n < 1) THEN
		write(outputunit,*) ' reallocate Nconntype too small n =',n
		n=1
	ENDIF

	temp = list					! OldExtent items
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' deallocate Nconntype failed '
		error = 1
		return
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' allocate Nconntype failed '
		error = 1
		return
	endif

	extent = min(extent, n)
	list(:extent) = temp(:extent) 

END SUBROUTINE reallocateNConnType


SUBROUTINE reallocateGleList(list, nin, error) ! copied not checked
!	USE ftypes_f
	USE gletypes_f
	IMPLICIT NONE
	TYPE (gle), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it

	TYPE (gle), dimension(UBOUND(list,1) ) :: temp
	extent = UBOUND(list,1) 
	if(extent > 100000) then
		write(outputunit,*) ' dodgy reallocate GLE List',extent
	endif	
	n = nin
	error = 0
	if(n < 1) THEN
		write(outputunit,*) ' reallocate Gle too small n =',n
		n=1
	ENDIF

	temp = list					! OldExtent items
	DEALLOCATE(list,stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' deallocate gle failed '
		error = 1
		return
	endif

	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' allocate gle failed '
		error = 1
		return
	endif

	extent = min(extent, n)
	list(:extent) = temp(:extent) 
	if(ubound(list,1) > extent) then
		list(extent+1:)%used=.false.  ! May 2003 
!	else 
!		write(outputunit,*) ' cant set used on extent+1 in glelist'
	endif
	END SUBROUTINE reallocateGleList

SUBROUTINE reallocatedofoholderList(p_list, nin, error) ! Uses the freelist not checked
	USE ftypes_f
	IMPLICIT NONE
        type (dofolist) :: p_list !  , pointer
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent =0,n, old_n,k
	TYPE (dofoholder), dimension(: ) ,allocatable :: temp
	
   ! list=>p_list%list
! A) Getting bigger. n > extent of StringList
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
 !  push all the new indices to the freelist
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it

	if(associated(p_list%list))  extent = UBOUND(p_list%list,1)  

	old_n = extent; n = nin
     
	if(associated(p_list%list) ) then
		allocate(temp(old_n),stat=istat)
		temp = p_list%list					! OldExtent items
		DEALLOCATE(p_list%list,stat=istat)
		if(istat /=0) THEN
			write(outputunit,*) 'deallocate failed = ', old_n; error = 1; deallocate(temp,stat=istat); 
			deallocate(temp,stat=istat) ; return
		endif
	else
		extent =0  ; old_n = extent
	endif

	if(n .eq. 0) then
		NULLIFY(p_list%list) 
	else
		ALLOCATE( p_list%list(n),stat=istat)
		if(istat /=0) THEN
			error = 1; deallocate(temp,stat=istat); return
		endif
		extent = min(old_n, n)
                if(extent > 0) p_list%list(:extent) = temp(:extent)
	endif
ifbigger:	if( n > old_n) then
		        do k= n, old_n+1,-1
		            if(associated( p_list%list(k)%o )) write(outputunit,*) ' Associated!!!!',k
			        p_list%list(k)%o =>NULL(); 
			        p_list%list(k)%used=.false.
		        enddo
	        else ifbigger ! n the same or smaller
		        do k= n+1, old_n
 			         if(associated( temp(k)%o) ) then 
			             DEALLOCATE(temp(k)%o); write(outputunit,*) ' dealloc strange'
			         endif
		        enddo
	        endif ifbigger
    deallocate(temp,stat=istat)
    error = 0
END SUBROUTINE reallocatedofoholderList

	
SUBROUTINE reallocateBattenList(list, nin, error) ! copied not checked
	USE ftypes_f
	IMPLICIT NONE
	TYPE (batten), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of  List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it

	TYPE (batten), pointer, dimension(: ) :: temp 
	temp=>NULL(); extent=0
!	write(outputunit,*) "N battens= ",nin
	if(associated(list)) then
			extent = UBOUND(list,1) 
			if(extent > 0) then
				allocate(temp(extent))
				temp = list	
			endif
			DEALLOCATE(list,stat=istat)
			if(istat /=0) THEN
				write(outputunit,*) ' deallocate batten failed '
				error = 1
				return
			endif	
	endif
	n = nin
	error = 0
	if(n < 0) THEN
		write(outputunit,*) ' reallocate batten too small n =',n
		n=0
	ENDIF
		

	if(n .gt. 0) then
			ALLOCATE( list(n),stat=istat)
			if(istat /=0) THEN
				write(outputunit,*) ' allocate batten failed '
				error = 1
				return
			endif

			extent = min(extent, n)
			if( associated(temp)) list(:extent) = temp(:extent) 
			if(ubound(list,1) > extent) then
				list(extent+1:)%used=.false.  ! May 2003 
			endif
	endif
	deallocate(temp,stat=istat)
END SUBROUTINE reallocatebattenList

SUBROUTINE reallocateDouble(list, nin, error)
        USE ftypes_f
        IMPLICIT NONE
        Real (Kind=double), dimension(:) ,POINTER :: list
        INTEGER, INTENT (IN) :: nin
        INTEGER, INTENT (OUT) :: error
        INTEGER :: istat, extent,n,old_n
!
! A) Getting bigger. n > extent of List
!	Put (extent) items safe into temp
!	allocate N
!	copy extent items into it
!
! B) getting smaller.
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it
        Real (Kind=double), dimension(UBOUND(list,1) ) :: temp
        extent=0
        if(associated(list) ) then
                extent = UBOUND(list,1)
        endif

        if(extent > 10000000) then
                write(outputunit,*) ' dodgy reallocate DOuble',extent
        endif
        n = nin
        error = 0
        if(n < 1) THEN
                write(outputunit,*) ' reallocate double too small n =',n
                n=1
        ENDIF
        if(associated(list)) then
                extent = UBOUND(list,1) ; old_n = extent
                temp = list					! OldExtent items
                DEALLOCATE(list,stat=istat)
                if(istat /=0 .and. old_n /=0) THEN
                        write(outputunit,*) ' deallocate double failed extent was ',old_n
                        error = 1
                        return
                endif
        else
                extent=0; old_n=0
        endif
        ALLOCATE( list(n),stat=istat)
        if(istat /=0) THEN
                write(outputunit,*) ' allocate double failed '
                error = 1
                return
        endif

        extent = min(extent, n)
        list(:extent) = temp(:extent)
END SUBROUTINE reallocateDouble

SUBROUTINE reallocateInt(list, nin, error)
	USE ftypes_f
	IMPLICIT NONE
        Integer, dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n,old_n
!
! A) Getting bigger. n > extent of List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it
        Integer, dimension(UBOUND(list,1) ) :: temp
	extent=0
	if(associated(list) ) then
		extent = UBOUND(list,1)
	endif

        if(extent > 10000000) then
		write(outputunit,*) ' dodgy reallocate DOuble',extent
	endif	
	n = nin
	error = 0
	if(n < 1) THEN
		write(outputunit,*) ' reallocate double too small n =',n
		n=1
	ENDIF
	if(associated(list)) then
		extent = UBOUND(list,1) ; old_n = extent
		temp = list					! OldExtent items
		DEALLOCATE(list,stat=istat)
		if(istat /=0 .and. old_n /=0) THEN
			write(outputunit,*) ' deallocate double failed extent was ',old_n
			error = 1
			return
		endif
	else
		extent=0; old_n=0
	endif
	ALLOCATE( list(n),stat=istat)
	if(istat /=0) THEN
		write(outputunit,*) ' allocate double failed '
		error = 1
		return
	endif

	extent = min(extent, n)
	list(:extent) = temp(:extent) 
END SUBROUTINE reallocateInt

SUBROUTINE  reallocatedoforef(list, nin, error) 
	USE ftypes_f

	IMPLICIT NONE
	TYPE (doforef), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
	INTEGER :: istat, extent,n
!
! A) Getting bigger. n > extent of List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it
	TYPE (doforef), dimension(: ),pointer :: temp
	nullify(temp)
	n = nin
	error = 0
	if(associated(list) ) then
	    extent = UBOUND(list,1) 
            if(extent > 10000000) then
		    write(outputunit,*) ' dodgy reallocate doforef ',extent
	    endif	

        allocate(temp(extent))
	    temp = list					! OldExtent items
	
	    DEALLOCATE(list,stat=istat)
	    if(istat /=0) THEN
		    write(outputunit,*) ' deallocate doforef failed '
		    error = 1
		    return
	    endif
    endif	
	if(n>0) then
	    ALLOCATE( list(n),stat=istat)
	    if(istat /=0) THEN
		    write(outputunit,*) ' allocate doforef failed '
		    error = 1
		    return
	    endif
        if(associated(temp)) then
	        extent = min(extent, n)
	        list(:extent) = temp(:extent) 
	        deallocate(temp)
	    endif
	endif
END SUBROUTINE  reallocatedoforef
 
SUBROUTINE reallocatefnoderef_p(list, nin, error) 
	USE ftypes_f

	IMPLICIT NONE
	TYPE (fnoderef_p), dimension(:) ,POINTER :: list
	INTEGER, INTENT (IN) :: nin
	INTEGER, INTENT (OUT) :: error
        INTEGER :: err, extent,n
!
! A) Getting bigger. n > extent of List
!	Put (extent) items safe into temp
!	allocate N 
!	copy extent items into it
!
! B) getting smaller. 
!	Put (extent)  items safe
!	allocate
!	copy (new extent) items into it
	TYPE (fnoderef_p), dimension(: ),pointer :: temp
	nullify(temp)
	n = nin
	error = 0
	if(associated(list) ) then
	    extent = UBOUND(list,1) 
            if(extent > 10000000) then
		    write(outputunit,*) ' dodgy reallocate doforef ',extent
	    endif	

        allocate(temp(extent),stat=err);
        if(err/= 0) then
                write(*,*) ' allocate failed '
                error=1
                return
        endif
	    temp = list					! OldExtent items
	
            DEALLOCATE(list,stat=err)
            if(err /=0) THEN
		    write(outputunit,*) ' deallocate doforef failed '
		    error = 1
		    return
	    endif
    endif	
	if(n>0) then
            ALLOCATE( list(n),stat=err)
            if(err /=0) THEN
		    write(outputunit,*) ' allocate doforef failed '
		    error = 1
		    return
	    endif
        if(associated(temp)) then
	        extent = min(extent, n)
	        list(:extent) = temp(:extent) 
	        deallocate(temp)
	    endif
	endif
END SUBROUTINE  reallocatefnoderef_p
END MODULE realloc_f




