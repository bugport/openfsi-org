#ifndef RXDBLINKTHREAD_H
#define RXDBLINKTHREAD_H

#include <QThread>
#include <QMutex>
#ifndef NOMYSQL
class RXDBLinkThreadLibMySQL : public QThread
{
    Q_OBJECT

public:
    explicit RXDBLinkThreadLibMySQL(QObject *parent, class RXDBLinkMySQL*db) ;
    ~RXDBLinkThreadLibMySQL();

protected:
    void run();
signals:
     void RXDBCommand(QObject *client, const QString &s);
public slots:

   public:
     class RXDBLinkMySQL *m_db;
     QMutex m_mtx;

};
#endif
#endif // RXDBLINKTHREAD_H
