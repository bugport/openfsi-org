
/* rxdblinkthread.cpp
  the class RXDBLinkThread  pumps input fields from the mirror database and applies any changes.
  there are some problems:
  1) often the format of text (leading blanks, trailing zeroes, etc) is changed.
    we could deal with that via a cleverer comparison.
  2) more seriously, because it runs in its own thread it can only send commands (apply: header: value).
  3) WHen the apply... command is processed (in the worker thread) it sets the needs_flags at entity and model level
    but these flags have no effect until the flow of the worker thread comes to a Bring-All-To-Date
    The solutions would be:
        1) the worker thread does  a BATD when it finishes processing every command.
        2) the pump thread issues a BATD command once it has finished processing the DB.
  */
// THIS VERSION ONLY WORKS WITH RXDBLinkMySQL
#include <QtCore>
#include "debug.h"
#include <QThread>
#include <QSqlQuery>
#include "rxmirrorqt.h"
#include "rxmainwindow.h"
#include "RXDBLink.h"
#ifndef NOMYSQL
#include "rxdblinkthread.h"

RXDBLinkThreadLibMySQL::RXDBLinkThreadLibMySQL(QObject *parent, class RXDBLinkMySQL*db) :
    QThread(parent),
    m_db(db)
{
    connect(this,SIGNAL(RXDBCommand(QObject*,QString)),g_rxmw,SIGNAL(RXCommand(QObject*,QString)));
}
RXDBLinkThreadLibMySQL::~RXDBLinkThreadLibMySQL()
{
    m_db->m_PleaseEndPump = true;
    wait();
    qDebug()<<"RXDBLinkThread  killed";
}

void RXDBLinkThreadLibMySQL::run()
{   int count=0;
    this->setPriority( QThread::LowPriority);
    QString qmirrorQuery =  QString( "SELECT * FROM " ) + RXMIRRORTABLE ;
    DBRESULT r;
    int rc ; unsigned int err;
    while (! m_db->m_PleaseEndPump )
    {
        count=0;
        rc=m_db->QQuery(qmirrorQuery.toStdString(),r,&err); // insulated
        /*
here we will compare the results with the previous set.
If something in the table has changed we emit an 'apply' command;
which should then apply_summary_value it.
*/
        string old;
        QString line;
        for(size_t i=0;i<r.size();i++) {
            string & h = r[i][0];
            string & d = r[i][1];
            string & c2 = r[i][2];
            string & c3 = r[i][3];
            if(c3=="1")  // output only
                continue;

            if(m_db->m_lastbuffer.find(h)!=m_db->m_lastbuffer.end() )  // the old exists
                old = m_db->m_lastbuffer[h];
            else
            {
                m_db->m_lastbuffer[h]=d;
                continue;
            }
            if(old==d)
                continue;

            m_db->m_lastbuffer[h]=d;
            string q = "apply:"+h+":"+d;
            line = QString(q.c_str()) + "! old was " + QString(old.c_str());
            emit this->RXDBCommand(g_rxmw,line); // forwards to signal RXCommand
            count++;
            qDebug()<< "From DB(RXDBLinkThreadLibMySQL): c2=" <<c2.c_str()<<" c3=" <<c3.c_str()<<"line="<<line;



        }
#ifdef RXQT
        this->msleep(523); //  // a strange number so it will eventually fall out of step with any other timers
        if(count)
            emit g_rxmw->RXCommand(g_rxmw,QString("process changes"));
        if( this->m_mtx.tryLock()){
            qDebug()<< " Mirror pump is locked ";
          this->m_mtx.unlock();
        }
        this->m_mtx.lock();
        this->m_mtx.unlock();
    } //while
    return;
#else
        Sleep(5000);
    } //while
#endif
}
#endif

