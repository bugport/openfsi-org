#ifndef RXH5FDDSMLINK_H
#define RXH5FDDSMLINK_H
/* The rxh5fddsmlink manages a connection to Paraview via H5FDDSM

It has methods to
    Initialize  = initiate a connection to PV
    Finalize    = close the connection.
    InitializeTransfer
    BuildOneModel
    FinishTransfer


*/
#ifndef LINK_H5FDDSM
    #warning "NOT configured for H5FDDSM build"
#else
    #warning "Configured for H5FDDSM build"
    #include <mpi.h>

    #include "hdf5.h"
    #include "hdf5_hl.h"
#endif
#include <QObject>

class rxh5fddsmlink : public QObject
{
    Q_OBJECT
public:
    rxh5fddsmlink(int pUsedsm,QObject *parent = 0);
    virtual ~rxh5fddsmlink();
    
signals:
    
public slots:

public:
    int Connect();
    int Disconnect();
    int StartTransfer();
    int TransferOneModel(class RXSail *s);
    int FinishTransfer();
    int IsOK();

private:
    int usedsm;
    int m_status;
    int MPI_Size;
    int MPI_Rank;
#ifdef LINK_H5FDDSM
    hid_t file_id;
#endif
    int staticInterComm;
    class H5FDdsmManager *dsmManager;
#ifdef LINK_H5FDDSM
    MPI_Comm comm;
#endif
};

#endif // RXH5FDDSMLINK_H
