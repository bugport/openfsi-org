#include "StdAfx.h"
#include <QDebug>
#ifdef LINK_H5FDDSM
#include "RX_FESite.h"
#include "RX_FETri3.h"
#include "RX_FEBeam.h"
#include "RX_FEPocket.h"
#include "RX_FEEdge.h"
#include "RXSail.h"
#include <iostream>
#include <fstream>
#include "etypes.h"
#include "f90_to_c.h"

#include "H5FDdsm.h"
#include "H5FDdsmManager.h"
#include "rxh5fddsmlink.h"

#pragma  message ("compiling "__FILE__  "for h5fddsm ")

using namespace std;
rxh5fddsmlink::rxh5fddsmlink(int pUsedsm , QObject*parent) :
    QObject(parent)
  ,usedsm(pUsedsm)
  , m_status (0)
  , MPI_Size(0)
  , MPI_Rank(0)
  , file_id(0)
  , staticInterComm(0)
  , dsmManager(0)
{
    comm = MPI_COMM_WORLD;
}

rxh5fddsmlink::~rxh5fddsmlink()
{
    this->Disconnect();
}
int rxh5fddsmlink::Disconnect()
{
    H5close();
    if(!dsmManager)
        return 0;
    if (usedsm) {
        if(dsmManager->GetIsConnected())

            dsmManager->Disconnect();
        dsmManager->Destroy();
        if (staticInterComm) {
            MPI_Barrier(comm);
            MPI_Comm_free(&comm);
        }
      //  MPI_Finalize();
    }
    dsmManager=0;
    return 1;
}
int rxh5fddsmlink::Connect()
{
    int rc=0;
    if (usedsm) {
        int argc=0;
        char**argv=0;
        int flag;
        // start MPI. We are using mpich2
        MPI_Initialized(&flag);
        if(!flag)
          MPI_Init(&argc,&argv);
        MPI_Comm_size(MPI_COMM_WORLD, &MPI_Size); // get size and rank
        MPI_Comm_rank(MPI_COMM_WORLD, &MPI_Rank);
        //
        // Create a DSM manager and read server generated config
        //
        dsmManager = new H5FDdsmManager();
        dsmManager->ReadConfigFile();
        // if using a static commmunicator, set it up. Untested here : For advanced users!
        if (dsmManager->GetUseStaticInterComm()) {
            assert(0);
            H5FDdsmInt32 color = 2; // 1 for server, 2 for client
            MPI_Comm_split(MPI_COMM_WORLD, color, MPI_Rank, &comm);
            MPI_Comm_rank(comm, &MPI_Rank);
            MPI_Comm_size(comm, &MPI_Size);
            staticInterComm = H5FD_DSM_TRUE;
        }
        // trigger connection using all our settings
        dsmManager->SetMpiComm(comm);
        dsmManager->SetIsServer(H5FD_DSM_FALSE);
        dsmManager->Create();
        // set the global manager to ours (only ever use one manager)
        H5FD_dsm_set_manager(dsmManager);
    }
    return rc;
}

int rxh5fddsmlink::StartTransfer()
{
    herr_t err;
    if (usedsm) {
        if(dsmManager)
        {
            if(!this->dsmManager->GetIsConnected()){
                cout<<" dsmManager exists but says it isnt connected. Let's try anyway' "<<endl;
              //  return 0;
            }
            hid_t  fapl_id;
            fapl_id = H5Pcreate(H5P_FILE_ACCESS);
            err = H5Pset_fapl_dsm(fapl_id, comm, NULL, 0);
            file_id = H5Fcreate("dsm", H5F_ACC_TRUNC, H5P_DEFAULT, fapl_id);
            H5Pclose(fapl_id);
            if(file_id<0)
            {
                cout<<" but H5FCreate failed"<<endl;
                return 0;
            }
            H5FD_dsm_lock();
        }
    }
    else {
        file_id = H5Fcreate("MyH5Data.h5", H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);
        cout << "H5Fcreate done ( MyH5Data.h5 ) " << endl;
    }
    return 1;
}
int rxh5fddsmlink::TransferOneModel(class RXSail *sail)
{

    int k, rc=1;
    ON_3dPoint p;
    int sli = sail->SailListIndex();
    // create  a group named for the sail

    QString sname(QString::fromStdString( sail->GetType() ) ) ;
    QString ss = "/"+sname ;
    hid_t group_id = H5Gcreate (file_id, qPrintable(ss), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);

// UNSTRUCTURED Grid
// Geometry
    unsigned int npts =  sail->m_Fnodes.size(); // trouble is - we dont want the six-nodes(rotational dofs)
    double  *myPoints = new double[3*npts];
    double*ptr = myPoints;
    for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
        RX_FESite* f = *it;
        if(!f)
            for(k=0;k<3;k++) *(ptr++)=0;
        else  {
            p = f->DeflectedPos();
            *(ptr++)=p.x ; *(ptr++)=p.y; *(ptr++)=p.z;
        }
    }
    hsize_t  pdims[1]; *pdims = 3*npts;
    m_status = H5LTmake_dataset(file_id,
                                qPrintable(ss+"/Points"),1,pdims,H5T_NATIVE_DOUBLE,myPoints);
// point vector data - displacements
    ptr = myPoints;
    for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
        RX_FESite* f = *it;
        if(!f)
            for(k=0;k<3;k++) *(ptr++)=0;
        else  {
            ON_3dVector dm = f->DeflectedPos(RX_MODELSPACE) - f->ToONPoint() ; // in model space
            p = sail->VToGlobal(dm);
            *(ptr++)=p.x ; *(ptr++)=p.y; *(ptr++)=p.z;
        }
    }
    hsize_t  pvdims[2] =  {npts,3};
    m_status = H5LTmake_dataset(file_id,
                                qPrintable(ss+"/Disp"),2,pvdims,H5T_NATIVE_DOUBLE,myPoints);

// point vector data - residuals
    ptr = myPoints;
    double r[3], tot[3];
    memset(tot,0,3*sizeof(double));
    for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
        RX_FESite* f = *it;
        if(!f)
            for(k=0;k<3;k++) *(ptr++)=0;
        else  {
          cf_get_raw_residual(sli,f->GetN(),r);
          *(ptr++)=r[0] ; *(ptr++)=r[1]; *(ptr++)=r[2];
        }
    }
    m_status = H5LTmake_dataset(file_id,
                                qPrintable(ss+"/Resids"),2,pvdims,H5T_NATIVE_DOUBLE,myPoints);


    ptr = myPoints;
    for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
      RX_FESite* f = *it;
      if(!f)
        for(k=0;k<3;k++) *(ptr++)=0;
      else  {
          cf_get_nodalload(sli, f->GetN(),r);
        *(ptr++)=r[0] ; *(ptr++)=r[1]; *(ptr++)=r[2];
         tot[0]+=r[0];       tot[1]+=r[1];       tot[2]+=r[2];
      }
    }
    m_status = H5LTmake_dataset(file_id,
                                qPrintable(ss+"/Loads"),2,pvdims,H5T_NATIVE_DOUBLE,myPoints);
     delete[]   myPoints;

//   Topology and cell data
// count active tris
    int nt=0;
    for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)  {
        RX_FETri3* t = *it;
        if(t) nt++;
    }
    if(nt>0)
    {
#ifndef TOPOLOGY_MIXED
        int tRank=2;
        hsize_t edims[2]={nt,3};
        double *b;
        int *myConnections = new int[3*nt] ; //{  1,2,3,  4,3,2,  3,4,5,   6,5,4};
        int *a = myConnections;

        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            *(a++)= t->Node(0); *(a++)= t->Node(1); *(a++)= t->Node(2) ;
        }
#else
        // When we do this  we find that we miss one cell. I suspect the last . There's a hole in the PV mesh
        // so PV complains that there too many pts in the attribute sets.
        assert(0);
        int tRank= 1;
        hsize_t edims[1]={nt*4};
        double *b;
        int *myConnections = new int[4*nt] ; //{  1,2,3,  4,3,2,  3,4,5,   6,5,4};
        int *a = myConnections;

        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            *(a++)=0x4; *(a++)= t->Node(0); *(a++)= t->Node(1); *(a++)= t->Node(2) ;
        }
#endif
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/topology"),tRank,edims,H5T_NATIVE_INT,myConnections);
        delete[] myConnections;
// cell data;
        hsize_t adims[1]; *adims = nt;
        double *d = new double[nt] ;
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            *(b++)= t->GetPressure();
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Pressure"),1,adims,H5T_NATIVE_DOUBLE,d);

        TriStressStruct tss;
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            *(b++)=tss.m_princ[0] ;// Upr_Principle_Stress
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Upr_Princ_Stress"),1,adims,H5T_NATIVE_DOUBLE,d);
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            *(b++)=tss.m_princ[1] ;// Upr_Principle_Stress
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Lwr_Princ_Stress"),1,adims,H5T_NATIVE_DOUBLE,d);

        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressResults(&tss);
            *(b++)=tss.m_cre ;// Upr_Principle_Stress
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Crease"),1,adims,H5T_NATIVE_DOUBLE,d);
        delete[] d;

// cell tensor data
        hsize_t tdims[2]={nt,9};
        d = new double[9*nt] ;
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStressTensor(b); b+=9;
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Stress"),2,tdims,H5T_NATIVE_DOUBLE,d);
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStrainTensor(b); b+=9;
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Strain"),2,tdims,H5T_NATIVE_DOUBLE,d);
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetStiffnessTensor(b); b+=9;
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Elasticity"),2,tdims,H5T_NATIVE_DOUBLE,d);
        delete[] d;

//cell vector data
        hsize_t vdims[2]={nt,3};
        d = new double[3*nt] ;
        b = d;
        for( vector<RX_FETri3*>::iterator it=sail->m_FTri3s.begin (); it !=sail->m_FTri3s.end();++it)   {
            RX_FETri3* t = *it;
            if(!t) continue;
            t->GetLocalAxis(b);b+=3;
        }
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/WarpDirection"),2,vdims,H5T_NATIVE_DOUBLE,d);
        delete[] d;

    }// c>0
    m_status= H5Gclose(group_id);
   double t=0;
    int do_lines=1;
    if(do_lines)  {// we have strings, fbeams, battens, flinks we only do the first two
        ss = ss+"_lines" ;
        ON_3dPoint p;
        class RX_FELinearObject*st;
        class RX_FEString* s;
        class RX_FEBeam*  b;
        vector<RXEntity* > stringlist ,  beamlist;
        vector<RXEntity* >::iterator it;
        int  ncells=0, nints=0,npts=0;
        sail->MapExtractList(PCE_STRING,  stringlist,PCE_ISRESOLVED) ;
        //  sail->MapExtractList(POCKET,  battenlist,PCE_ISRESOLVED) ;
           sail-> MapExtractList(PCE_BEAMSTRING,  beamlist,PCE_ISRESOLVED) ;
        if(!(stringlist.size() + beamlist.size()))//  +pcfiles.size()  ) ) TODO put PCFiles back in
            return 0;
        int sli = sail->SailListIndex();

        group_id = H5Gcreate (file_id, qPrintable(ss), H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
        rc=1;
        // count up to the first rotational node
        npts=0;
        for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
            RX_FESite* f = *it;
            if(f)
                if(cf_issixnode(sli,  f->GetN()) <00 )
                    break;
            npts++;
        }

        double  *myPoints = new double[3*npts];
        double*ptr = myPoints;
        for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
            RX_FESite* f = *it;
            if(!f)
                for(k=0;k<3;k++) *(ptr++)=0;
            else  {
                if(cf_issixnode(sli,  f->GetN()) <00 )
                    break;
                p = f->DeflectedPos();
                *(ptr++)=p.x ; *(ptr++)=p.y; *(ptr++)=p.z;
            }
        }
        hsize_t  pdims[1]; *pdims = 3*npts;
        m_status = H5LTmake_dataset(file_id,
                                    qPrintable(ss+"/Points"),1,pdims,H5T_NATIVE_DOUBLE,myPoints);
// point vector data
        ptr = myPoints;
        for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
            RX_FESite* f = *it;
            if(!f)
                for(k=0;k<3;k++) *(ptr++)=0;
            else  {
                if(cf_issixnode(sli,  f->GetN()) <00 )
                    break;
                ON_3dVector dm = f->DeflectedPos(RX_MODELSPACE) - f->ToONPoint() ; // in model space
                p = sail->VToGlobal(dm);
                *(ptr++)=p.x ; *(ptr++)=p.y; *(ptr++)=p.z;
            }
        }
        hsize_t  pvdims[2] =  {npts,3};
        m_status = H5LTmake_dataset(file_id,
                                    qPrintable(ss+"/Disp"),2,pvdims,H5T_NATIVE_DOUBLE,myPoints);

        ptr = myPoints;
        for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
          RX_FESite* f = *it;
          if(!f)
            for(k=0;k<3;k++) *(ptr++)=0;
          else  {
            if(cf_issixnode(sli,  f->GetN()) <00 )
              break;
            cf_get_raw_residual(sli,f->GetN(),r);
            *(ptr++)=r[0] ; *(ptr++)=r[1]; *(ptr++)=r[2];
          }
        }
        m_status = H5LTmake_dataset(file_id,
                                    qPrintable(ss+"/Resids"),2,pvdims,H5T_NATIVE_DOUBLE,myPoints);

        ptr = myPoints;
        for( vector<RX_FESite*>::iterator it=sail->m_Fnodes.begin (); it !=sail->m_Fnodes.end();++it)  {
          RX_FESite* f = *it;
          if(!f)
            for(k=0;k<3;k++) *(ptr++)=0;
          else  {
            if(cf_issixnode(sli,  f->GetN()) <00 )
              break;
              cf_get_nodalload(sli, f->GetN(),r);
            *(ptr++)=r[0] ; *(ptr++)=r[1]; *(ptr++)=r[2];
          }
        }
        m_status = H5LTmake_dataset(file_id,
                                    qPrintable(ss+"/Loads"),2,pvdims,H5T_NATIVE_DOUBLE,myPoints);
        delete[]   myPoints;

// count active strings
      stringlist.insert(stringlist.begin(), beamlist.begin(),beamlist.end()); // beams first

        nints=0; ncells=0;
        for (it = stringlist.begin (); it != stringlist.end(); it++)  {
            RXEntity_p p  = *it;
            s = (RX_FEString*)  dynamic_cast<class RX_FEString*>(p);
            std::vector<int>nnn = s->GetNodeList();
            if(!nnn.size())
                continue;
            ncells+=(nnn.size()-1);  // icarus work-around: one pl per segment
        } // for it
         nints=4*ncells;
//topology
        int*myConnections = new int[nints] ; //{  1,2,3,  4,3,2,  3,4,5,   6,5,4};
        int* a = myConnections;

        for (it = stringlist.begin (); it != stringlist.end(); it++)  {
            RXEntity_p p  = *it;
            st = (RX_FELinearObject*)  dynamic_cast<class RX_FEString*>(p);
            if(!st)
                st = (RX_FELinearObject*)  dynamic_cast<class RX_FEBeam*>(p);
            if(!st)
                continue;
            std::vector<int>nnn = st->GetNodeList();
            if(!nnn.size())
                continue;
            if(0)   // we'd like  to pass complete polylines but ICARUS doesnt like them
            {
                *(a++)=2 ; // code for a polyline
                *(a++)=nnn.size();
                for (std::vector<int>::iterator ii= nnn.begin();ii!=nnn.end();++ii)
                    *(a++)=*ii  ;
            }
            else {   // a set of SHORT polylines because ICARUS cannot count them oherwise
                for(int i=1;i<nnn.size();i++) {
                     *(a++)=2 ; // code for a polyline
                     *(a++)=2 ; // 2 points
                    *(a++)=nnn[i-1]; *(a++) = nnn[i];
                }
            }
        } // for it
        int tRank=1;
        hsize_t edims[1]={nints};
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/topology"),tRank,edims,H5T_NATIVE_INT,myConnections);
        delete[] myConnections;
 // topology done

  //We-d like to write the string names but xdmf doesnt support this.
            /* beam results are (see tanpure.f90)
   m13	=>  force(1)
   m23	=> 	force(2)
   m12	=> 	force(3)
   m22	=> 	force(4)
   mt	=> 	force(5)
   p	=> 	force(6)
   sF12=> 	force(7)
   SF22=> 	force(8)
   sF13=> 	force(9)
   SF23=> 	force(10)  */
            //  tension
        double *tensions = new double[ncells];
        double*dptr=tensions;
            for (it = stringlist.begin (); it != stringlist.end(); it++)  {
                RXEntity_p p  = *it;
                st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
                if(!st)
                    continue;
                std::vector<int>nnn = st->GetNodeList();
                if(!nnn.size())
                    continue;
                b =  dynamic_cast<class RX_FEBeam*>(p);
                if(b) {
                    double f[10];
                    std::vector<int>  ee = b-> Elements() ;
                    assert (ee.size()+1 == nnn.size() );
                    std::vector<int>::const_iterator ii;
                    for(ii=ee.begin(); ii !=ee.end(); ++ii) {
                        if( cf_one_beamelementresult(sli, *ii, f)  )
                            *(dptr++) = f[5];
                        else
                             *(dptr++) = 0.0;
                    }
                    continue;
                }
                s=dynamic_cast<class RX_FEString*>(p);
                if(s) {
                    double*ttt = s->m_FPtq;
                    int ns = s->GetN();
                    if(!cf_get_one_string_tension(&sli,&ns, &t))
                        t=0;
                    for(int i=1; i<nnn.size();i++)
                       {
                        *(dptr++) = ttt[i-1];
                    }
                }

            } // for it
            tRank=1;
            *edims = ncells;
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/tension"),tRank,edims,H5T_NATIVE_DOUBLE,tensions);
// torque
        dptr=tensions;
                    for (it = stringlist.begin (); it != stringlist.end(); it++)  {
                        RXEntity_p p  = *it;
                        st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
                        if(!st)
                            continue;
                        std::vector<int>nnn = st->GetNodeList();
                        if(!nnn.size())
                            continue;
                        b =  dynamic_cast<class RX_FEBeam*>(p);
                        if(b) {
                            double f[10];
                            std::vector<int>  ee = b-> Elements() ;
                            assert (ee.size()+1 == nnn.size() );
                            std::vector<int>::const_iterator ii;
                            for(ii=ee.begin(); ii !=ee.end(); ++ii) {
                                if( cf_one_beamelementresult(sli, *ii, f)  )
                                    *(dptr++) = f[4];
                                else
                                     *(dptr++) = 0.0;
                            }
                            continue;
                        }
                        s=dynamic_cast<class RX_FEString*>(p);
                        if(s) {
                            double*ttt = s->m_FPtq;
                            int ns = s->GetN();
                            if(!cf_get_one_string_tension(&sli,&ns, &t))
                                t=0;
                            for(int i=1; i<nnn.size();i++)
                               {
                                *(dptr++) = 0;
                            }
                        }

                    } // for it
                    tRank=1;
                    *edims = ncells;
                m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/torsion"),tRank,edims,H5T_NATIVE_DOUBLE,tensions);
// now the question how to write the beam moments
// we could put a moment vector on the end nodes - but that is multiples per node
// we could just put the mean of m1 and m2 onto the cells

// My
        dptr=tensions;
            for (it = stringlist.begin (); it != stringlist.end(); it++)  {
                RXEntity_p p  = *it;
                st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
                if(!st)
                    continue;
                std::vector<int>nnn = st->GetNodeList();
                if(!nnn.size())
                    continue;
                b =  dynamic_cast<class RX_FEBeam*>(p);
                if(b) {
                    double f[10];
                    std::vector<int>  ee = b-> Elements() ;
                    assert (ee.size()+1 == nnn.size() );
                    std::vector<int>::const_iterator ii;
                    for(ii=ee.begin(); ii !=ee.end(); ++ii) {
                        if( cf_one_beamelementresult(sli, *ii, f)  )
                            *(dptr++) = fabs (f[0]) > fabs (f[1]) ? f[0] : f[1];   //  m13+m13
                        else
                             *(dptr++) = 0.0;
                    }
                    continue;
                }
                s=dynamic_cast<class RX_FEString*>(p);
                if(s) {
                        t=0;
                    for(int i=1; i<nnn.size();i++)
                       {
                        *(dptr++) = t;
                    }
                }

            } // for it
            tRank=1;
            *edims = ncells;
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/My"),tRank,edims,H5T_NATIVE_DOUBLE,tensions);

 // Mz
       dptr=tensions;
            for (it = stringlist.begin (); it != stringlist.end(); it++)  {
                RXEntity_p p  = *it;
                st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
                if(!st)
                    continue;
                std::vector<int>nnn = st->GetNodeList();
                if(!nnn.size())
                    continue;
                b =  dynamic_cast<class RX_FEBeam*>(p);
                if(b) {
                    double f[10];
                    std::vector<int>  ee = b-> Elements() ;
                    assert (ee.size()+1 == nnn.size() );
                    std::vector<int>::const_iterator ii;
                    for(ii=ee.begin(); ii !=ee.end(); ++ii) {
                        if( cf_one_beamelementresult(sli, *ii, f)  )
                            *(dptr++) = fabs (f[2]) > fabs (f[3]) ? f[2] : f[3];
                        else
                             *(dptr++) = 0.0;
                    }
                    continue;
                }
                s=dynamic_cast<class RX_FEString*>(p);
                if(s) {
                        t=0;
                    for(int i=1; i<nnn.size();i++)
                       {
                        *(dptr++) = t;
                    }
                }

            } // for it
            tRank=1;
            *edims = ncells;
        m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/Mz"),tRank,edims,H5T_NATIVE_DOUBLE,tensions);

// cell tensor data
                hsize_t tdims[2]={ncells,9}; //nt is count of linear elements
                double*d = new double[9*ncells] ;
                dptr = d;
                for (it = stringlist.begin (); it != stringlist.end(); it++)  {
                    RXEntity_p p  = *it;
                    st = (RX_FELinearObject*)  dynamic_cast<RX_FELinearObject*>(p);
                    if(!st) continue;
                    std::vector<int>nnn = st->GetNodeList();
                    if(!nnn.size())   continue;
                    b =  dynamic_cast<class RX_FEBeam*>(p);
                    if(b) {
                        std::vector<int>  ee = b-> Elements() ;
                        std::vector<int>::const_iterator ii;
                        for(ii=ee.begin(); ii !=ee.end(); ++ii) {
                             cf_one_beamelementtrigraph(sli, *ii, dptr)  ; dptr+=9;
                        }
                        continue;
                    }
                    s=dynamic_cast<class RX_FEString*>(p);
                    if(s) {
                            t=0;
                        for(int i=1; i<nnn.size();i++)
                           {
                            for(k=0;k<9;k++) *(dptr++) = t;
                        }
                    }
                } // for it
                m_status = H5LTmake_dataset(file_id,qPrintable(ss+"/BeamAxes"),2,tdims,H5T_NATIVE_DOUBLE,d);
                delete[] d;

// end cell data
        m_status= H5Gclose(group_id);
        delete[] tensions;
    } // do_lines
    return rc;
}
int rxh5fddsmlink::FinishTransfer()
{
    int rc=1;
    //
    // manually unlock (release) the DSM and send a NEW_DATA message
    //
    if (usedsm) {
        herr_t status =H5Fclose(file_id);
        H5FD_dsm_unlock(H5FD_DSM_NOTIFY_DATA);
        //  cout << "DSM unlocked" << endl;
        if(status <0){
            cout << "DSM closed with err=" <<status<< endl;
            H5Eprint(H5E_DEFAULT, stdout);
        }
        //        else
        //            cout << "DSM closed OK" << endl;

    }
    // writing to file, close it
    else {
        herr_t status = H5Fclose (file_id);
        if(status >=0)
            cout << "HDF5 File 'MyH5Data.h5' closed OK" << endl;
        else {
            cout << "HDF5 File closed with err=" <<status<< endl;
            H5Eprint(H5E_DEFAULT, stdout);
        }
    }

    return rc;
}

int rxh5fddsmlink::IsOK()
{
    return( this->dsmManager!=0);
}
#endif
