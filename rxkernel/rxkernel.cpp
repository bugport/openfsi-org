#include "debug.h"
#include "rxkernel.h"
#include "rxmainwindow.h"
#include "RelaxWorld.h"
#include <QDebug>
 #include <QDateTime>
#include <QFile>
#include <iostream>
using namespace std;
#include "mainstuf.h"
#include "script.h"


int RXStart(int argc, char**argv, class QObject* TheWorld){
    int rc=0;
 //   cout<<"(rxkernel) RX Start"<<endl;
    relaxinit_1();
    g_World->SetClient(TheWorld);
    return rc;
}

int RXExecute(const QString line,class QObject * owner){
    int rc=0;
    qDebug( )<<"\nbef_ESL\t"<<QDateTime::currentDateTime () <<"\t"<<line ;
    rc =  Execute_Script_Line(line ,  0, owner);
    qDebug( )<<"aft_ESL\t"<<QDateTime::currentDateTime () <<"\t"<<line ;

    return rc;
}

int RXEnd(){
    int rc=0;
    qDebug() <<"(rxkernel) RX End";
    End_RelaxWorld();
    return rc;
}

//int RXMsgBox (const QString &s,QObject *client )
//{
// int level=2;
//    return rxWorker::displayMsgBox(client,s,level );
//}
int RXShutDown ( )
{
    return  rxWorker::shutdown() ;
}


QString RXGetExistingFileName(QObject *client,
                              const QString caption,
                              const QString dir,
                              const QString filter){

    return  rxWorker::rxOpenFileName(client,caption,dir,filter );
}

QString RXGetSaveFileName(QObject *client,
                          const QString caption,
                          const QString dir,
                          const QString filter){

    return   rxWorker::rxSaveFileName(client,caption,dir,filter );
}
QString RXGetDirName(QObject *client,
                     const QString caption,
                     const QString dir,
                     const QString filter){

    return   rxWorker::rxDirName(client,caption,dir,filter );
}
