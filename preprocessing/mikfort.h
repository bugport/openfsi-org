 /*     SUBROUTINE Print_Mik_File(filename,top,bot,NumOfStripes,fair) 
      IMPLICIT NONE
*     THIS Routine  MAKES THE ARRAY XSTRIP
*     
*     arguments
      
      CHARACTER*256 filename
      real*8 top,bot
      integer NumOfStripes ,fair
      
      INTEGER NumOnStripe
*     how many points to return for each stripe
      
*     returns
    

*/
#ifndef RX_MIKFORT_H
#define RX_MIKFORT_H

EXTERN_C void print_mik_file_(char*f, double*t,double*b,int*N,int*fair);

#endif //#ifndef RX_MIKFORT_H

