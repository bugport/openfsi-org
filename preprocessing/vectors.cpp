/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Part of meshlib.a
 *
 * Modified :
	Dec 97	speeded up Apparent angle calc. Added  extern int Compute_Perp_Transform(VECTOR*,float d[3][3]);
	24/3/97 PC_Apparent Angle was working on asin not acos and this may have
given trouble in finding geodesics on kites.
 
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. free.
 *						changed strdup to STRDUP
 */


/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Part of meshlib.a
 *
 * Modified :
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

/*   1st June 1994. PC_Vector_Difference added. Faster than vector_OP
	  also.  PC_Vector_OP . one operation removed
 date: Thursday  3/2/1994
 for NEC. C++ style Comments removed */
/* FILE: VECTORS.C
date  Thursday 2 April 1992

these are some general-purpose vector routines
*/
/*     vprint
     vprint
     PC_Vector_Compare
     PC_Vector_Copy
     PC_Vector_Op
     PC_Normal_Offset
     PC_Polyline_Length
     PC_Find_Chord */

#include "StdAfx.h"
#include "RelaxWorld.h"
#include "RXSRelSite.h"
#include "vectors.h" 


ON_3dVector * PostMult_Vect_By_Trigraph(float *d, const ON_3dVector v,ON_3dVector *vd) 
{
/* assumes d = 3 * 3 matrix */

vd->x = v.x * d[0] + v.y * d[3] + v.z * d[6];
vd->y = v.x * d[1] + v.y * d[4] + v.z * d[7];
vd->z = v.x * d[2] + v.y * d[5] + v.z * d[8];


return(vd);
}

double distV_sq(VECTOR *s, VECTOR *t) {
double dx,dy,dz,ret;
	dx = s->x - t->x;
	dy = s->y - t->y;
	dz = s->z - t->z;
	ret = dx*dx + dy*dy + dz*dz;

	return(ret);
}
double dists2D(const VECTOR *s,const VECTOR *t){
	double dx,dy;
	dx = s->x - t->x;
	dy = s->y - t->y;

	return sqrt( dx*dx + dy*dy);
}

double distV(VECTOR *s, VECTOR *t)
{
	return(sqrt(distV_sq(s,t)));
}


int PC_Compute_Perp_Transform(ON_3dVector*n,double d[3][3]){
/* returns D, a matrix which will rotate a vector by 90deg about n */

	d[0][0]=    0.0;	d[0][1]=    n->z;	d[0][2]=   -n->y;
	d[1][0]=	-n->z;	d[1][1]=    0.0;	d[1][2]=    n->x;
	d[2][0]=   n->y;	d[2][1]=  -n->x;	d[2][2]=	0.0;

return 1;
}

int PC_Compute_3Matrix_Product(double a[3][3], double b[3][3], double r[3][3])
{
int i,j,k;
	memset(r,0, 9*sizeof(double));
	for(i=0;i<3;i++) {
		for(j=0;j<3;j++) {		
			for(k=0;k<3;k++) {
				r[i][k] += a[i][j]*b[j][k];
			}
		}
	}

return 1;
}//int PC_Compute_3Matrix_Product

int Compute_TrigraphXY(ON_3dVector x,ON_3dVector y,float d[3][3]){
ON_3dVector z;
int rc=1;

		if (x.LengthSquared() < FLOAT_TOLERANCE* FLOAT_TOLERANCE) {
			g_World->OutputToClient(" (Compute_TrigraphXY)zero length chord",5);
			x.x=(float)1.0;  
			rc=0;
		}
		y.Unitize();  
		x.Unitize (); 
		
		z=ON_CrossProduct(x,y); 
		z.Unitize ();  
	
		d[0][0] = x[0]; d[0][1]=x[1];	d[0][2]=x[2];
		d[1][0] = y[0]; d[1][1]=y[1];	d[1][2]=y[2];
		d[2][0] = z[0]; d[2][1]=z[1];	d[2][2]=z[2]; 

 
		if(0) {  cout<< "Compute_TrigraphXY"<<endl;
				printf(" %f %f %f\n", d[0][0], d[0][1], d[0][2]);
				printf(" %f %f %f\n", d[1][0], d[1][1], d[1][2]);
				printf(" %f %f %f\n", d[2][0], d[2][1], d[2][2]);
		}
	 return(rc);
}

int Compute_TrigraphXZ(const ON_3dVector &xin,const ON_3dVector &zin,float d[3][3]){
	int iret=1;
  /* a trigraph with X along xin, Y as z X x and z as x X y	   */
	double t = xin.LengthSquared();
	if( t < (FLOAT_TOLERANCE *FLOAT_TOLERANCE )){
			g_World->OutputToClient(" (Compute_TrigraphXZ)zero length chord",3);
		//	x->x=(float)1.0;
			return 0;
		}
		ON_3dVector  y,x ,z ; x = xin/ sqrt(t);		d[0][0] = x[0]; d[0][1]=x[1];	d[0][2]=x[2];
		y = ON_CrossProduct(zin,x); y.Unitize();	d[1][0] = y[0]; d[1][1]=y[1];	d[1][2]=y[2];
		z = ON_CrossProduct(xin,y); z.Unitize();	d[2][0] = z[0]; d[2][1]=z[1];	d[2][2]=z[2]; 

	 return(iret);
}
#ifdef HOOPS
int Compute_TrigraphXZ(VECTOR*x,VECTOR*z,  float d[3][3]){
	int iret=1;
  /* a trigraph with X along xin, Y as z X x and z as x X y	   */

		if (HC_Compute_Vector_Length(x) < FLOAT_TOLERANCE) {
			g_World->OutputToClient(" (Compute_TrigraphXZ)zero length chord",1);
			x->x=(float)1.0;
			iret=0;
		}

		HC_Compute_Normalized_Vector(x,&(d[0][0]));
   		HC_Compute_Cross_Product(z,x,&d[1][0]);			   /* Y */
  		HC_Compute_Normalized_Vector(&d[1][0],&d[1][0]);
		HC_Compute_Cross_Product(&d[0][0],&d[1][0],&d[2][0]);

	 return(iret);
}

 
int Compute_TrigraphYZ(VECTOR*y,VECTOR*z,  float d[3][3]){

  /* a trigraph with 	x as y X z
  							 y as y
  							 z as x X y */		

		if (HC_Compute_Vector_Length(y) < FLOAT_TOLERANCE) {
			g_World->OutputToClient(" (Compute_TrigraphYZ)zero length chord",1);
			y->y=(float)1.0;
		}
		HC_Compute_Normalized_Vector(y,&d[1][0]);  
		HC_Compute_Normalized_Vector(&d[1][0],&d[1][0]);  
		HC_Compute_Cross_Product(&d[1][0],z,&(d[0][0]));
		HC_Compute_Normalized_Vector(&(d[0][0]),&(d[0][0]));	
	
		HC_Compute_Cross_Product(&d[0][0],&d[1][0],&d[2][0]);	
  
	 return(1);
}



#endif
void V2D(VECTOR*a,DVECTOR*b) {
	b->x=(double) a->x;
	b->y=(double) a->y;
	b->z=(double) a->z;
  }
double VD_Compute_Dot_Product(DVECTOR* a, DVECTOR*b) {
 return(a->x*b->x+	a->y*b->y+	   a->z*b->z );
}
void VD_Compute_Cross_Product(DVECTOR *a,DVECTOR*b,DVECTOR*n) {
	  n->x =  a->y * b->z - a->z * b->y;
	  n->y =  a->z * b->x - a->x * b->z; 
	  n->z =  a->x * b->y - a->y * b->x;

}
double PC_True_Angle(const ON_3dVector a,const ON_3dVector b,int *err){
/* a and b are not necessarily unit length, 
We return the True angle between a and b  (acos(dot)) */
char buf[256];
double a1,la,lb,d;


	 d =    ON_DotProduct(a,b); 
  	 la = a.Length ();
 	 lb = b.Length ();
  	 if (la < 10E-10 || lb < 10E-10) {
		sprintf(buf,"True_Angle(on) on zero length vector\n (%f %f %f) and (%f %f %f)", a.x,a.y,a.z,b.x,b.y,b.z);
		g_World->OutputToClient (buf,1); 
		if(*err) *err=1;
		return((double) 0.0);
	}

 	a1= d/(la*lb);
 	if (a1 >(double) 1.0) a1 = (double)  1.0;
	if (a1 <(double)-1.0) a1 = (double) -1.0;
	 a1 = acos(a1); 
	 if(*err) *err=0;
	return(a1);
}

double PC_Signed_True_Angle(VECTOR a, VECTOR b, ON_3dVector  ppview,int *err) {

/* a and b are not necessarily unit length, nor is the view vector.
We return the angle between a and b when viewed along v */

DVECTOR ad,bd,nd1,nd2,viewd,nd;
double a1=0.,l1,l2,  dot,cosine;

/* double sine, crossLen,asine; */


	V2D(&a,&ad);
	V2D(&b,&bd); 

	viewd.x=ppview.x; viewd.y=ppview.y; viewd.z=ppview.z; 

 	VD_Compute_Cross_Product(&ad,&viewd,&nd1);
	VD_Compute_Cross_Product(&bd,&viewd,&nd2);	/* nd1 and nd2 are in view plane */
	/* angle is asin of mod(nd) /la/lb */
	l1 = VD_Compute_Dot_Product(&ad,&ad); if(l1 < 10E-20 ) {
		g_World->OutputToClient ("PC_Signed_True_Angle on zero vector(1)",1);  *err=1; return(0.0); }
 	l2 = VD_Compute_Dot_Product(&bd,&bd); if(l2 < 10E-20 ) {
		g_World->OutputToClient ("PC_Signed_True_Angle on zero vector(2)",1);  *err=1; return(0.0); }

	VD_Compute_Cross_Product(&nd1,&nd2,&nd);

	dot =  VD_Compute_Dot_Product(&ad,&bd);
	cosine = dot /sqrt(l1* l2) ;
 	if (cosine >=(double) 1.0) cosine = (double)  1.0;
	else if (cosine <=(double)-1.0) cosine = (double) -1.0;
	a1 = acos(cosine); 
	
/* 	crossLen = sqrt(VD_Compute_Dot_Product(&nd,&nd));
	 sine = crossLen /sqrt(l1* l2)  ;
	 asine = asin(sine);   this was IN until 24/3/97 I dont know why

	 if(fabs(a1 - asine) > 0.001) 
		 printf("Found the K-4 Bug  acos = %f asin=%f ", a1*57.293, asine*57.293); */
	
 if(*err) *err=0; 
  
	if (0.0 <VD_Compute_Dot_Product(&nd,&viewd) ) 
		return(a1);
							
	return(-a1);
	}
double PC_Apparent_Angle(const ON_3dVector a, const ON_3dVector b, const ON_3dVector view) {

/* a and b are not necessarily unit length, nor is the view vector.
We return the angle between a and b when viewed along v */

ON_3dVector nd1,nd2,nd;
double a1,l1,l2,  dot,cosine;

 	nd1 = ON_CrossProduct(a,view);
	nd2 = ON_CrossProduct(b,view);	/* nd1 and nd2 are in view plane */
	/* angle is asin of mod(nd) /la/lb */
	l1 = nd1.LengthSquared(); if(l1 < 10E-20 ) {  g_World->OutputToClient ("PC_Apparent_Angle on zero vector(1)",1);  return(0.0); }
 	l2 = nd2.LengthSquared(); if(l2 < 10E-20 ) {  g_World->OutputToClient ("PC_Apparent_Angle on zero vector(2)",1);  return(0.0); }

	nd=ON_CrossProduct(nd1,nd2);

	dot =  ON_DotProduct(nd1,nd2);
	cosine = dot /sqrt(l1* l2) ;
 	if (cosine >=(double) 1.0) cosine = (double)  1.0;
	else if (cosine <=(double)-1.0) cosine = (double) -1.0;
	a1 = acos(cosine); 
	

	if (0.0 <ON_DotProduct(nd,view) ) 
		return(a1);
							
	return(-a1);
	}

void  PC_Vector_Difference(const VECTOR *p1,const VECTOR *p2, VECTOR *p3){
     p3->x=p1->x-p2->x;
     p3->y=p1->y-p2->y;
     p3->z=p1->z-p2->z;
}

void PC_Vector_Difference(const Site *p1,const VECTOR *p2, VECTOR *p3){
     p3->x=p1->x-p2->x;
     p3->y=p1->y-p2->y;
     p3->z=p1->z-p2->z;
}
ON_3dVector  PC_Vector_Difference(const Site * p1,const Site * p2){
	ON_3dVector p3;
     p3.x=p1->x-p2->x;
     p3.y=p1->y-p2->y;
     p3.z=p1->z-p2->z;
	 return p3;
}

void PC_Vector_Copy(const VECTOR p1, VECTOR *ptr) {

	 memcpy(ptr,&p1,sizeof(VECTOR)); // this is PC_Vector_Copy
}
void PC_Vector_Copy(const ON_3dPoint p1, VECTOR *ptr) {
      ptr->x=p1.x;
     ptr->y=p1.y;
     ptr->z=p1.z;	  
}
void PC_Vector_Copy(const VECTOR p1, ON_3dPoint *ptr) {
      ptr->x=p1.x;
     ptr->y=p1.y;
     ptr->z=p1.z;	  
}

int PC_Vector_Rotate(ON_3dVector &x,const double angle,const ON_3dVector Z){ // in RADIANS

		ON_3dVector OB,OE,OF;
		double oc, c,s;
		 
		OB=ON_CrossProduct(x,Z);
		oc = ON_DotProduct(x,Z); // often zero
		OE = ON_CrossProduct(Z,OB);
		c = cos(angle); s =  sin(angle);
		OF = OE * c + OB * s;

		x =  Z*oc + OF;
	return 1;
}

 
double PC_Dist_sq(const VECTOR *A,const VECTOR *B){
 double dx,dy,dz,t;
	dx = (double) (A->x - B->x);
	dy = (double) (A->y - B->y);
	dz = (double) (A->z - B->z);
	t = dx*dx + dy*dy+ dz*dz;
	return(t);
 	}
double PC_Dist_sq(const Site *A,const VECTOR *B){
 double dx,dy,dz,t;
	dx = (double) (A->x - B->x);
	dy = (double) (A->y - B->y);
	dz = (double) (A->z - B->z);
	t = dx*dx + dy*dy+ dz*dz;
	return(t);
 	}

double PC_Dist(const VECTOR *A,const VECTOR *B){
	double t = sqrt(PC_Dist_sq(A,B));
	if (t < 10.0E-06) t = 0.0;  
	return(t);
 	}
double PC_Dist(const Site *A,const VECTOR *B){
	double t = sqrt(PC_Dist_sq(A,B));
	if (t < 10.0E-06) t = 0.0;  
	return(t);
 	}
double PC_Polyline_Length(int c,VECTOR p[]){

	int k;
	double l;
	l=0.0;
	for (k=0;k<c-1;k++) {
		l=l+PC_Dist(&(p[k]),&(p[k+1]));
		}
return(l);
}
  

