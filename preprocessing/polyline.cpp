/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 *
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * Part of meshlib.a
 *
 
 * Modified :
 *  7.7.96 some speed optimisations in find poly tangent. TYPE and use arc to get ref length
 *     20/7/95 PC_Intersection returns the s1,s2 and mean of (a1,a2) if lines parallel
 *		PC_Drop_Perpendicular added - for RLX file reader
 *    13/04/95   Find_Intersections didnt work OK if  Seed2 was not zero Fixed
 *  10/1/95  Find_Intersections returns point from curve2, NOT curve 1
    in the hope of batpatching moulded designs
   Massive change to Offset system in the hope of speeding the build
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */


/*   date: Thursday  3/2/1994
 for NEC. C++ style Comments removed */

/* file: polyline.c
date:  Tuesday 28 December 1993

These are the polyline handling routines 
 MArch 1996 the of set routines moved to arclngth.c
 so they can be handled more tightly

 */
#include "StdAfx.h"
#include "RXSail.h"
#include "RelaxWorld.h"
#include "RXGraphicCalls.h"

#include "RXSeamcurve.h"
#include "RXOffset.h"
#include "vectors.h"
#include "RXCurve.h"
#include "hoopcall.h"

#include "sailutil.h" 
#include "panel.h"
#include "etypes.h"

#include "arclngth.h"


#ifndef strieq
#define	 strieq(a,b)  (_stricmp(a,b)==0)
#endif
#include "polyline.h"
int SeamCurve_Find_Straight(const ON_3dPoint e1,const ON_3dPoint e2, VECTOR**p,int*c,float*chord){
    /* e1 and e2 are the start and end points.
 *p is allocateable
 *c is the number of points we end up with
 returns 1 ifOK
*/

    *p=(VECTOR *)REALLOC(*p, 2*sizeof(VECTOR)); if(! (*p)) return 0;
    PC_Vector_Copy(e1,&((*p)[0]));
    PC_Vector_Copy(e2,&((*p)[1]));
    *c=2;
    *chord = e1.DistanceTo (e2);
    return 1;
}
int PC_Compute_Polyline_Mean(VECTOR *p, int c, VECTOR *v) {  /* beware 2D only */

    int k;
    float grad,n,sigX=(float)0.0,sigY=(float)0.0,sigZ=(float)0.0,sigXY=(float)0.0,sigXsq=(float)0.0;
    float d;
    ON_3dVector vt;
    if (c<2) return(1);

    for (k=0;k<c;k++){
        sigX=sigX      + p[k].x;
        sigY=sigY      + p[k].y;
        sigZ=sigZ      + p[k].z;

        sigXY=sigXY    + p[k].x*p[k].y;
        sigXsq=sigXsq  + p[k].x*p[k].x;
    }
    n=(float) c;
    d= sigX*sigX-n*sigXsq;
    if (d!=(float)0.0) {
        grad=(sigX*sigY-n*sigXY)/d;
        vt.x=(float)1.0;
        vt.y=grad;
        vt.z=(float)0.0;
    }
    else {
        vt.x=(float)0.0;
        vt.y=(float)1.0;
        vt.z=(float)0.0;
    }
    vt.Unitize (); PC_Vector_Copy(vt,v);
    return(1);
}


int PCP_Polyline_Remove_Close_Points(VECTOR*p, int*c, double tol) {
    double d, tolsq = tol*tol;
    VECTOR *last;
    int k, count = 0;
    if(!(*c)) return 0;
    last = &(p[0]);
    for(k=1;k<(*c)-1;k++) {
        d=distV_sq(&(p[k]),last);
        if(d < tolsq) {
            memmove(&(p[k]),&(p[k+1]), ((*c)-k-1) *sizeof(VECTOR)); // dest,source, amount
            k--; (*c)--;
            count ++;
        }
        else
            last = &(p[k]);
    }
    while(distV_sq(&(p[(*c)-2]), &(p[(*c)-1]))< tolsq ){
        k = (*c)-2;
        memmove(&(p[k]),&(p[k+1]),  sizeof(VECTOR));
        (*c)--;count ++;
    }
    return count;
}
int PC_Polyline_Copy(VECTOR*dest, VECTOR*source, int count) {
    memcpy(dest,source, count*sizeof(VECTOR));

    return 1;
}

int PC_Polyline_Compare(VECTOR*p1, int c1, VECTOR*p2, int c2, double tol) { /* return 1 if different */
    int k;
    if(c1-c2) return 1;

    for(k=0;k<c1 ;k++) {
        if(PC_Dist_sq(&(p1[k]),&(p2[k])) > tol*tol)
            return 1;
    }
    return 0;
}

int Append_To_Poly(VECTOR**pptr, int *count,VECTOR *pin,int cin) {
    /* (p,count) is an existing polyline. We append cin vectors from Pin, REALLOCing as necessary */
    int i,k,nc;   /* new count */
    VECTOR *p;
    p = *pptr;

    nc = *count+cin;
    p= (VECTOR *)REALLOC(p,	nc*sizeof(VECTOR));
    if(p==NULL) {g_World->OutputToClient("No memory in Append_To_Poly",2); return(0);}
    k=0;
    for (i = *count;i<nc;i++)   PC_Vector_Copy(pin[k++], &p[i]);
    *count=nc;
    *pptr = p;
    return(1);
}

double AKM_Compute_Dot_Product(VECTOR *v1,VECTOR *v2)
{
    double retval;

    retval = (v1->x * v2->x) + (v1->y * v2->y) + (v1->z * v2->z);


    return(retval);
}



int PC_Dist_Between_Lines(VECTOR a1,VECTOR b1,VECTOR a2,VECTOR b2,float *s1,float *s2,VECTOR *v,float *dis) {
    /*0 fail,1 OK 2 parallel */

    /* returns the   normal distance between two lines. as well
  as the distances along the line segments from their starts.
  Lines are defines as from a1 to b1 and from a2 to b2
  If the two line segments are parallel,
  s1 and s2 are the distances along each segment of the first
  */

#ifndef HOOPS

    cout<< "PC_Dist_Between_Lines needs hoops "<<endl;
    return 0;
#else
    VECTOR v1,v2,d1,d2,da;
    float x1,x2;
    float det,m11,m12,m21,m22;
    float i11,i12,i21,i22;


    PC_Vector_Difference(&b1,&a1,&d1);
    /* a2,b2 are on the same side if ((b1-a1)X(a2-a1)) . ((b1-a1) X (b2-a1)) >=0.
  if this is IsTrueEdge, return 0.
   Similarly    if ((b2-a2)X(a1-a2)) . ((b2-a2) X (b1-a2)) >= 0. return 0 */
    {
        VECTOR a,x1,x2;
        PC_Vector_Difference(&a2,&a1,&da);
        HC_Compute_Cross_Product(&d1,&da,&x1);
        PC_Vector_Difference(&b2,&a1,&a);
        HC_Compute_Cross_Product(&d1,&a,&x2);
        if( AKM_Compute_Dot_Product(&x1,&x2) >FLOAT_TOLERANCE) return(0);

        PC_Vector_Difference(&b2,&a2,&d2);

        HC_Compute_Cross_Product(&d2,&da,&x1);	/* wrong sign */
        PC_Vector_Difference(&b1,&a2,&a);
        HC_Compute_Cross_Product(&d2,&a,&x2);
        if( AKM_Compute_Dot_Product(&x1,&x2) < FLOAT_TOLERANCE) return(0);
    }




    HC_Compute_Normalized_Vector(&d1,&v1);
    HC_Compute_Normalized_Vector(&d2,&v2);

    m11 = (float) 1.0;
    m12 = -(float) AKM_Compute_Dot_Product(&v1,&v2);
    m21 = -m12;
    m22 = (float) - 1.0;

    PC_Vector_Difference(&a1,&a2,&da);

    x1 = (float)AKM_Compute_Dot_Product(&da,&v1);
    x2 = (float)AKM_Compute_Dot_Product(&da,&v2);

    det = m11*m22 - m12*m21;
    if(det >= -FLOAT_TOLERANCE) {		    /* the lines are parallel */
        VECTOR X;
        float sa2,sb2;
        /*rxerror("lines parallel",1);
        the intersection point is defined as the first in {a2,b2} along  v1
    s1 is lesser of ( a2-a1).v1 and (b2-a1).v1  	*/

        /* a2 from a1 */
        PC_Vector_Difference(&a2,&a1,&X);
        sa2 = (float) HC_Compute_Dot_Product(&X,&v1);
        /* b2 from a1 */
        PC_Vector_Difference(&b2,&a1,&X);
        sb2 = (float)HC_Compute_Dot_Product(&X,&v1);	/* b2 from a1 */


        if( sa2 < sb2) { *s1 = sa2; 	PC_Vector_Copy(a2,v);  *s2=(float) 0.0; }
        else 		    { *s1 = sb2; 	PC_Vector_Copy(b2,v); *s2 = (float) PC_Dist(&b2,&a2); }

        HC_Compute_Cross_Product(&v1,&da,&X);
        *dis = (float) HC_Compute_Vector_Length(&X);

        return(2);
    }


    i11 = m22/det; i22 = m11/det;
    i12 = -m12/det; i21 = -m21/det;

    *s1 = -(i11*x1 + i12 * x2);
    *s2 = -(i21*x1 + i22 * x2);

    da.x = a1.x + *s1 * v1.x;	   /* swapped 10/1/95 in the hope of batpatching moulded designs */
    da.y = a1.y + *s1 * v1.y;
    da.z = a1.z + *s1 * v1.z;

    v->x = a2.x + *s2 * v2.x;
    v->y = a2.y + *s2 * v2.y;
    v->z = a2.z + *s2 * v2.z;

    *dis = (float) PC_Dist(&da,v);


    return(1);
#endif
}
int Find_Polyline_Intersection(VECTOR*p1,VECTOR *p2,int c1,int c2,double *seed1,double *seed2,double* s1max,double *s2max,VECTOR *v) {

    /* if it finds an intersection between {p1,c1} and {p2,c2} it returns it in v
 and returns 1. Else v is undefined and return value 0
 *s1 and *s2 are arclengths used to seed the search. New values are returned
if successful
*s1max and*s2max are upper limits on s1 and s2: search fails if a cross is found
where s1>=s1max or s2 >=s2max. 
This is to enable searching along psides

  In the event that two line segments are parallel, it returns the first crossing point on p1
 */

    int i1,i2,iret;
    double l1,l2,d1,d2;
    float s1,s2,d;
    l1=0.0;

    for (i1=0;i1<c1-1;i1++) {
        l2=0.0;
        d1=  PC_Dist(&(p1[i1]),&(p1[i1+1]));
        if(l1+d1>= *seed1-FLOAT_TOLERANCE && l1 < *s1max+FLOAT_TOLERANCE) {  /* an optimisation */
            for (i2=0;i2<c2-1;i2++) {
                d2 = PC_Dist(&(p2[i2]),&(p2[i2+1]));
                if (l2 + d2 >  *seed2-FLOAT_TOLERANCE && l2 < *s2max+FLOAT_TOLERANCE){
                    iret=  PC_Dist_Between_Lines(p1[i1],p1[i1+1],p2[i2],p2[i2+1],&s1,&s2,v,&d); /*0 fail,1 OK 2 parallel */

                    if(iret==1) {		 /* a cross */
                        /*	if (FLOAT_TOLERANCE > d) {	*/
                        if((s1 + l1) >=*s1max ) return(0);
                        if((s2 + l2)>=*s2max ) return(0);
                        if (s1 >=-1E-6 && s2 >=-1E-6 && s1-d1 <1E-6 && s2-d2 <1E-6) {
                            if ((s1+l1 > *seed1)&&(s2+l2 > *seed2) ){
                                *seed1 = s1 + l1;
                                *seed2 = s2 + l2;
                                return(iret);
                            }
                        }
                        /*
    }
     else rxerror(" cross but D too big",1);  */
                    }
                    /* else if(iret==2) {       }	 */
                }
                l2 = l2 + d2;
            }
        }
        l1 = l1 + d1;

    }
    return(0);

}

int Place_On_Poly_By_X(float offset,VECTOR *p,int c,VECTOR*v)  {
    float ds,factor,f2;
    int k, k0,k1,k0found,k1found;
    k0=0;
    /* 1) find k0 which is the knot index where
  EITHER K0 = c-2 (or less if dist(c-2,c-1) is small)
  OR		K0 = 0 and offset <0
  OR 		K0+1 is the first past the offset

 2) find K1 which is first knot index >K0 where dist(k0,k1) is not small
 */

    k0= k0found = 0;
    k = 0;
    do {
        if(p[k].x>offset || k==c-1)	{ k0found = 1; k0 = k-1;}
        if(k0<0) k0=0;
        k++;
    } while(!k0found);

    /* here k0 is ok. K1 will be k0+1 unless this is too close */
    k1found = 0;
    k1 = k0+1; if(k1 >=c) g_World->OutputToClient(" Place_On_Poly",66);
    do {
        ds = p[k1].x-p[k0].x ;
        if (ds < FLOAT_TOLERANCE) {
            if(k1 < c-1) k1++;
            else if (k0>0) k0--;
            else { g_World->OutputToClient(" short polyline",1); k1found = 1; }
        }
        else k1found =1;
    } while(!k1found);

    if(ds < FLOAT_TOLERANCE)		factor = (float) 0.0;
    else 							factor = (offset-p[k0].x)/(p[k1].x-p[k0].x);

    f2 = (float) 1.0 - factor;
    (*v).x = p[k0].x*f2 + p[k1].x*factor;
    (*v).y = p[k0].y*f2 + p[k1].y*factor;
    (*v).z = p[k0].z*f2 + p[k1].z*factor;
    return(1);
}


/*************************************************/


int Place_On_Poly(float offset,VECTOR *p,int c,ON_3dPoint &v)  {
    double ds,factor,f2;
    int k, k0,k1,k0found,k1found;
    double*s ;
    k0=0;
    /* 1) find k0 which is the knot index where
  EITHER K0 = c-2 (or less if dist(c-2,c-1) is small)
  OR		K0 = 0 and offset <0
  OR 		K0+1 is the first past the offset

 2) find K1 which is first knot index >K0 where dist(k0,k1) is not small
 */

    s=new double[c];
    s[0]= 0.0;
    for (k=1;k<c;k++) {
        s[k] = s[k-1] +  PC_Dist(&(p[k-1]),&(p[k]));
    }

    k0= k0found = 0;
    k = 0;
    do {
        if(s[k]>offset || k==c-1)	{ k0found = 1; k0 = k-1;}
        if(k0<0) k0=0;
        k++;
    } while(!k0found);

    /* here k0 is ok. K1 will be k0+1 unless this is too close */
    k1found = 0;
    k1 = k0+1; if(k1 >=c) g_World->OutputToClient(" Place_On_Poly",2);
    do {
        ds = s[k1]-s[k0] ;
        if (ds < FLOAT_TOLERANCE) {
            if(k1 < c-1) k1++;
            else if (k0>0) k0--;
            else { g_World->OutputToClient(" short polyline",1); k1found = 1; }
        }
        else k1found =1;
    } while(!k1found);

    if(ds < FLOAT_TOLERANCE)		factor = (float) 0.0;
    else 							factor = (offset-s[k0])/(s[k1]-s[k0]);

    f2 = (float) 1.0 - factor;
    v.x = p[k0].x*f2 + p[k1].x*factor;
    v.y = p[k0].y*f2 + p[k1].y*factor;
    v.z = p[k0].z*f2 + p[k1].z*factor;
    delete []s;
    return(1);
}


/*************************************************/
int Place_On_Poly(float offset,VECTOR *p,int c,VECTOR*v)  {
    float ds,factor,f2;
    int k, k0,k1,k0found,k1found;
    static float*s =NULL;
    // static VECTOR*pold=NULL;
    // static int  cold = -1;
    k0=0;
    /* 1) find k0 which is the knot index where
  EITHER K0 = c-2 (or less if dist(c-2,c-1) is small)
  OR		K0 = 0 and offset <0
  OR 		K0+1 is the first past the offset

 2) find K1 which is first knot index >K0 where dist(k0,k1) is not small
 */
    if(true){//	if(p!=pold || c!=cold) {
        if(s) RXFREE(s);
        s= (float *)MALLOC((c+1)*sizeof(float)); if(!s) g_World->OutputToClient("MEMORY",4);
        s[0]=(float)0.0;
        for (k=1;k<c;k++) {
            s[k] = s[k-1] + (float) PC_Dist(&(p[k-1]),&(p[k]));
        }
        //pold=p; cold=c;
    }
    k0= k0found = 0;
    k = 0;
    do {
        if(s[k]>offset || k==c-1)	{ k0found = 1; k0 = k-1;}
        if(k0<0) k0=0;
        k++;
    } while(!k0found);

    /* here k0 is ok. K1 will be k0+1 unless this is too close */
    k1found = 0;
    k1 = k0+1; if(k1 >=c) g_World->OutputToClient(" Place_On_Poly",2);
    do {
        ds = s[k1]-s[k0] ;
        if (ds < FLOAT_TOLERANCE) {
            if(k1 < c-1) k1++;
            else if (k0>0) k0--;
            else { g_World->OutputToClient(" short polyline",1); k1found = 1; }
        }
        else k1found =1;
    } while(!k1found);

    if(ds < FLOAT_TOLERANCE)		factor = (float) 0.0;
    else 							factor = (offset-s[k0])/(s[k1]-s[k0]);

    f2 = (float) 1.0 - factor;
    (*v).x = p[k0].x*f2 + p[k1].x*factor;
    (*v).y = p[k0].y*f2 + p[k1].y*factor;
    (*v).z = p[k0].z*f2 + p[k1].z*factor;
    return(1);
}


/*************************************************/
int Find_Poly_Tangent(RXEntity_p p_ptr,RXOffset *p_offin,int p_side,ON_3dVector *p_v) {
    //
    /* the entity must be a seamcurve, in which case <offset> is needed
It may be a seam, in which case <side> is used 
p_ptr : Seam curve or seam entity (INPUT)
p_offin : offset along the seamcurve, needed if seamcurve (INPUT)  
p_side : side of the seam, needed if seam (INPUT)
p_v : tangent vector (unit vector)(OUTPUT)
*/
    double l_offset,l_delta;
    sc_ptr np;
    if (p_ptr->TYPE==SEAMCURVE || p_ptr->TYPE==PCE_NODECURVE) {
        /* Get the seam and go offset along it */
        np = (sc_ptr  )p_ptr;

        if((p_side <0) ||p_side>2)
        {
            p_ptr->OutputToClient("Find_Poly_Tangent side out of range",3);
        }

        if (!( np->m_pC[p_side]->HasGeometry()))
        {
            char s[256];
            sprintf(s,"(Get_Position)\n cant interpolate a seam with 0 or 1 pts\n %s (a %s)",p_ptr->name() ,(*p_ptr).type());
            p_ptr->OutputToClient(s,3);

        }

        //	double l_refl = GetArc(np,p_side); // this is very expensive.
        //	l_delta  =  fabs( 0.005*l_refl) ;

        l_delta = p_ptr->Esail->m_Cut_Dist;

        l_offset = (p_offin->Evaluate(np,p_side));

        ON_3dPoint l_p;
        ON_3dVector l_t;

        np->m_pC[p_side]->Find_Tangent(l_offset,&l_p,&l_t,l_delta);

        l_t.Unitize ();
        p_v->x=l_t.x; 	p_v->y=l_t.y; 	p_v->z=l_t.z;

    }
    else   {
        char str[256];
        sprintf(str," Find_Poly_Tangent does not support %s\n",p_ptr->type());
        p_ptr->OutputToClient(str,1);
        (*p_v).x=(float)0.0; (*p_v).y=(float)0.0;(*p_v).z=(float)0.0; return(0);
    }
    return(1);
}//int Find_Poly_Tangent


int Insert_Into_Poly(VECTOR **p,int *c,VECTOR v,int ka) {
    /* inserts v into p just after ka */

    int k;
    if(ka < 0) g_World->OutputToClient(" Insert_Into_Poly",3);
    k = max( (*c)+2, ka+3);	  /* re a l o c size */
    *p = (VECTOR *)REALLOC (*p, k *sizeof(VECTOR));
    for (k= *c -1;k>ka;k--) {
        PC_Vector_Copy((*p)[k],&((*p)[k+1]));
    }
    PC_Vector_Copy(v,&((*p)[ka+1]));
    (*c)++;
    return(1);

}

#ifndef NEVER
int PC_Normalise_Polyline(VECTOR *p,const int c, const ON_3dVector &z){
    /* transforms the polyline so it goes from (0,0,0) to (1,0,0) */

    ON_3dPoint x0,x1,x;
    ON_3dVector xc,yl,r ;
    int k;
    double chord ;


    PC_Vector_Copy(p[0],&x0);
    PC_Vector_Copy(p[c-1],&x1);
    xc =x1-x0;  chord=xc.LengthAndUnitize();
    if(chord < FLOAT_TOLERANCE) return(0);

    yl = ON_CrossProduct(z,xc); // HC_Compute_Cross_Product(&z,&xl,&yl);

    for(k=0;k<c;k++) {
        PC_Vector_Copy(p[k],&x);
        r = x-x0;//		    PC_Vector_Difference(&p[k],&x0 ,&r);
        p[k].x = (float) ON_DotProduct(r,xc)/chord;//	p[k].x = (float)( HC_Compute_Dot_Product(&r,&xl)/chord);
        p[k].y = (float)(ON_DotProduct(r,yl)/chord);//	p[k].y = (float)( HC_Compute_Dot_Product(&r,&yl)/chord);
        p[k].z = (float) (ON_DotProduct(r,z)/chord);//	p[k].z = (float) (HC_Compute_Dot_Product(&r,&z)/chord);
    }
    p[c-1].x=1.0e00;    p[c-1].y=0.0e00;    p[c-1].z=0.0e00;
    return(1);
}
#else
int PC_Normalise_Polyline(VECTOR *p,int c, const ON_3dVector &p_zz) {
    /* transforms the polyline so it goes from (0,0,0) to (1,0,0) */
#ifndef HOOPS
    cout<<  " PC Normalise Polyline needs hoops "<<endl;
    assert(0);  GOAT
        #else
    VECTOR x0,xl,yl,r , z;
    int k;
    double chord = PC_Dist(p ,&(p[c-1]));
    z.x=p_zz.x;  z.y=p_zz.y;  z.z=p_zz.z;
    if(chord < FLOAT_TOLERANCE) return(0);

    PC_Vector_Copy(p[0],&x0);
    PC_Vector_Difference(&(p[c-1]),&x0 ,&xl);

    HC_Compute_Normalized_Vector(&xl,&xl);

    HC_Compute_Cross_Product(&z,&xl,&yl);

    for(k=0;k<c;k++) {
        PC_Vector_Difference(&p[k],&x0 ,&r);
        p[k].x = (float)( HC_Compute_Dot_Product(&r,&xl)/chord);
        p[k].y = (float)( HC_Compute_Dot_Product(&r,&yl)/chord);
        p[k].z = (float) (HC_Compute_Dot_Product(&r,&z)/chord);
    }
#endif
    return(1);
}
#endif
int Polygon_Centroid( VECTOR p[], int c, ON_3dPoint *cen){

    int k;
    if (c<2) return(0);
    cen->x =	cen->y = cen->z =0.0;
    for (k=0;k<c;k++){
        cen->x += p[k].x;
        cen->y += p[k].y;
        cen->z += p[k].z;
    }

    cen->x 	 /= (float) c;
    cen->y 	 /=(float) c;
    cen->z	 /=(float) c;
    return(1);
}

//int Polygon_Label(const char *s, VECTOR p[],int c){

//	int k;
//	float grad,n,sigX=(float)0.0,sigY=(float)0.0,sigZ=(float)0.0,sigXY=(float)0.0,sigXsq=(float)0.0;
//	float xm,ym,zm,d;
//	if (c<2) return(0);

//	for (k=0;k<c;k++){
//		sigX=sigX      + p[k].x;
//		sigY=sigY      + p[k].y;
//		sigZ=sigZ      + p[k].z;

//		sigXY=sigXY    + p[k].x*p[k].y;
//		sigXsq=sigXsq  + p[k].x*p[k].x;
//		}
//		n=(float) c;
//		d= sigX*sigX-n*sigXsq;
//		if (!isnan(d) && fabs(d) > 0.0001 ) {
//			grad=(sigX*sigY-n*sigXY)/d;
//			}
//		else grad=(float) 1000.0;
//		HC_Open_Segment("label");
//			HC_Set_Text_Path(1.0,grad,0.0);
//			HC_Set_Text_Font("size=15px,transforms=on,rotation=follow path");
//			xm=sigX/n;
//			zm=sigZ/n;
//			ym=sigY/n;
//			HC_Insert_Text(xm,ym,zm,s);
//		HC_Close_Segment();
//		return(1);
//		}
int PCP_Poly_Planar(int c,VECTOR *p){
    // converts the poly into a planar poly.
    int k;
    //VECTOR q;
    //Polygon_Centroid(p,c,&q);
    for(k=0;k<c;k++) {
        p[k].z = 0.0; //q.z;
    }
    return 1;
}

int Shrink_Poly(int c,VECTOR *p,double f)	{
    int k;
    float x0=(float)0.0,y0=(float)0.0,z0=(float)0.0;
    float f2;
    for(k=0;k<c;k++) {
        x0=x0+ p[k].x;
        y0=y0+ p[k].y;
        z0=z0+ p[k].z;
    }
    f2 = (float)1.0 - f;
    for(k=0;k<c;k++) {
        p[k].x = p[k].x*f + x0/(float)c * f2;
        p[k].y = p[k].y*f + y0/(float)c * f2;
        p[k].z = p[k].z*f + z0/(float)c * f2;
    }
    return(1);
}
int PC_Intersection(const ON_3dPoint a1,const ON_3dPoint  b1,const ON_3dPoint  a2,
                    const ON_3dPoint  b2, ON_3dPoint &v,float*s1r,float*s2r) {
    /*0 fail,1 OK 2 parallel */

    /* returns the point where the two lines cross
  Lines are defines as from a1 to b1 and from a2 to b2
  The lines are NOT clipped.
  july 1995 now returns s1,s2 (if retval 1)
     and .5(a1+a2) if retval 2

  */


    ON_3dVector v1,v2,d1,d2,da;
    double d_x1,d_x2;
    float det,m11,m12,m21,m22;
    float i11,i12,i21,i22;
    float s1,s2;


    d1= b1-a1;
    /* a2,b2 are on the same side if ((b1-a1)X(a2-a1)) . ((b1-a1) X (b2-a1)) >=0.
  if this is IsTrueEdge, return 0.
   Similarly    if ((b2-a2)X(a1-a2)) . ((b2-a2) X (b1-a2)) >= 0. return 0 */
    {
        ON_3dVector a,x1,x2;
        da=a2-a1;
        x1=ON_CrossProduct( d1,da );
        a=b2-a1; //PC_Vector_Difference(&b2,&a1,&a);
        x2=ON_CrossProduct(d1,a);

        d2= b2-a2;
        x1=ON_CrossProduct( d2,da );/* wrong sign */

        a=b1-a2; // PC_Vector_Difference(&b1,&a2,&a);
        x2=ON_CrossProduct( d2,a); // HC Compute_Cross_Product(&d2,&a,&x2);   /* maybe not needed */
    }

    v1= d1; v1.Unitize ();
    v2=d2; v2.Unitize();

    m11 = (float) 1.0;
    m12 = -ON_DotProduct(v1,v2);
    m21 = -m12;
    m22 = (float) - 1.0;

    da = a1-a2;

    d_x1 = ON_DotProduct(da,v1);
    d_x2 = ON_DotProduct(da,v2);

    det = m11*m22 - m12*m21;
    if(det >= -FLOAT_TOLERANCE) {		    /* the lines are parallel */
        v.x = (float) 0.5*(a1.x+a2.x);
        v.y = (float) 0.5*(a1.y+a2.y);
        v.z = (float) 0.5*(a1.z+a2.z);
        *s1r=0.0; *s2r=0.0;
        return(2);
    }


    i11 = m22/det; i22 = m11/det;
    i12 = -m12/det; i21 = -m21/det;

    s1 = -(i11*d_x1 + i12 * d_x2);
    s2 = -(i21*d_x1 + i22 * d_x2);

    v.x = a1.x + s1 * v1.x;
    v.y = a1.y + s1 * v1.y;
    v.z = a1.z + s1 * v1.z;

    da.x = a2.x + s2 * v2.x;
    da.y = a2.y + s2 * v2.y;
    da.z = a2.z + s2 * v2.z;

    *s1r=s1; *s2r=s2;
    return(1);

}

int PC_IntersectionDEAD(VECTOR a1,VECTOR b1,VECTOR a2,VECTOR b2,VECTOR *v,float*s1r,float*s2r) {
    /*0 fail,1 OK 2 parallel */

    /* returns the point where the two lines cross
  Lines are defines as from a1 to b1 and from a2 to b2
  The lines are NOT clipped.
  july 1995 now returns s1,s2 (if retval 1)
     and .5(a1+a2) if retval 2

  */
#ifndef HOOPS
    cout<< " this PC_Intersection needs hoops "<<endl;
    assert(0);
    return 0;
#else
    VECTOR v1,v2,d1,d2,da;
    float x1,x2;
    float det,m11,m12,m21,m22;
    float i11,i12,i21,i22;
    float s1,s2;


    PC_Vector_Difference(&b1,&a1,&d1);
    /* a2,b2 are on the same side if ((b1-a1)X(a2-a1)) . ((b1-a1) X (b2-a1)) >=0.
  if this is IsTrueEdge, return 0.
   Similarly    if ((b2-a2)X(a1-a2)) . ((b2-a2) X (b1-a2)) >= 0. return 0 */
    {
        VECTOR a,x1,x2;
        PC_Vector_Difference(&a2,&a1,&da);
        HC_Compute_Cross_Product(&d1,&da,&x1);
        PC_Vector_Difference(&b2,&a1,&a);
        HC_Compute_Cross_Product(&d1,&a,&x2);

        PC_Vector_Difference(&b2,&a2,&d2);

        HC_Compute_Cross_Product(&d2,&da,&x1);	/* wrong sign */
        PC_Vector_Difference(&b1,&a2,&a);
        HC_Compute_Cross_Product(&d2,&a,&x2);   /* maybe not needed */
    }

    HC_Compute_Normalized_Vector(&d1,&v1);
    HC_Compute_Normalized_Vector(&d2,&v2);

    m11 = (float) 1.0;
    m12 = -(float) AKM_Compute_Dot_Product(&v1,&v2);
    m21 = -m12;
    m22 = (float) - 1.0;

    PC_Vector_Difference(&a1,&a2,&da);

    x1 = (float)AKM_Compute_Dot_Product(&da,&v1);
    x2 = (float)AKM_Compute_Dot_Product(&da,&v2);

    det = m11*m22 - m12*m21;
    if(det >= -FLOAT_TOLERANCE) {		    /* the lines are parallel */
        v->x = (float) 0.5*(a1.x+a2.x);
        v->y = (float) 0.5*(a1.y+a2.y);
        v->z = (float) 0.5*(a1.z+a2.z);
        *s1r=0.0; *s2r=0.0;
        return(2);
    }


    i11 = m22/det; i22 = m11/det;
    i12 = -m12/det; i21 = -m21/det;

    s1 = -(i11*x1 + i12 * x2);
    s2 = -(i21*x1 + i22 * x2);

    (*v).x = a1.x + s1 * v1.x;
    (*v).y = a1.y + s1 * v1.y;
    (*v).z = a1.z + s1 * v1.z;

    da.x = a2.x + s2 * v2.x;
    da.y = a2.y + s2 * v2.y;
    da.z = a2.z + s2 * v2.z;

    *s1r=s1; *s2r=s2;
    return(1);
#endif
}

int PC_Drop_PerpendicularDead(const VECTOR ain, VECTOR*p,int c,float tol,float*ss,float*dist,int Ignore_Z) {
    /*    where a is the point in question
       p is the polyline representation of the curve
       c is the no. of points in the polyline
       tol is used twice -
           its used as the step length along the polyline for each iteration
    its also used to stop iteration (s change is less than tol)

       ss is initially the length along the polyline to start
       ss is set to the length along the polyline where the perpendicular hits
       dist is the perpendicular distance.

        try s = s as initial length along polyline
 find tangent at s
   find location (r) of perpendicular between a and poly
 measure distance along this tangent then add this distance to s
 increment s by ((a-r) dot t)
 loop if inc > tol
 dist is (a-r) cross t
 TOL has a double purpose. Delta for tangent calc, and convergence limit
  */
#ifndef HOOPS
    cout<<"  this PC_Drop_Perpendicular needs HOOPS"<<endl;
    assert(0);
    return 0;
#else
    VECTOR a,r,r2,t,ra,rat;
    float s,inc;
    int count = 100;
    s = *ss;
    a.x = ain.x; a.y = ain.y;
    if(Ignore_Z) a.z=0.0;
    else a.z = ain.z;
    do {
        Place_On_Poly(s,p,c,&r);
        Place_On_Poly((float)(s+tol),p,c,&r2);
        if(Ignore_Z) { r.z=0.0; r2.z=0.0;}
        PC_Vector_Difference(&r2,&r,&t);
        PC_Vector_Difference(&a,&r,&ra);
        if(!HC_Compute_Normalized_Vector(&t,&t)) { cout<< "PC_Drop_Perp short T\n"<<endl; return(0);}
        inc = (float) HC_Compute_Dot_Product(&t,&ra);
        HC_Compute_Cross_Product(&ra,&t,&rat);
        *dist = (float) HC_Compute_Vector_Length(&rat);
        s = s + inc;
    }
    while(fabs(inc)>tol&&count--);
    *ss = s;
    if(count<=0)  cout<< "PC_Drop_Perp. Iteration failed\n"<<endl;
    return(count>0);
#endif
}

int Group_Polyline( VECTOR *p, int *c, int n, double p_tol){

    /* if there are more than n members of p, cluster them into n points
   at 1/n,2/n, 3/n, etc
   STRIP values at 0.0 or 1.0
      */
    float xmax,xmin,dx,x,xl,xh;
    float y,z,*Y,*Z;
    int j,k;

    if(*c <= n) return(0);
    printf(" grouping from %d to %d\n", *c,n);
    Y = (float *)MALLOC(n*sizeof(float));
    Z = (float *)MALLOC(n*sizeof(float));
    xmax = (float) 1.0;
    xmin = (float) 0.0;
    dx = (xmax - xmin)/(float)(n+1);

    x = (float) 0.0;
    xl = (float) p_tol;
    for(k=0;k<n;k++) {
        x = x + dx;
        xh = x + dx/(float) 2.0;
        if(k==n-1) xh=xmax-(float) p_tol;
        y=z=(float) 0.0;
        for(j=0;j<*c;j++) {
            if(p[j].x >= xl && p[j].x < xh) {
                y=y+p[j].y;
                z=z+p[j].z;
                /*	printf(" %d %f %f into %d (%f)\n",j,p[j].x,p[j].y, k,x); */
            }
        }
        Y[k] = y; Z[k]=z;
        xl = xh;
    }
    *c=n;
    x = dx;
    for(k=0;k<*c;k++) {
        p[k].x = x;
        p[k].y= Y[k];
        p[k].z= Z[k];
        /*	   printf("grouped %d %f %f %f\n",k,x,Y[k],Z[k]); */
        x = x + dx;
    }
    RXFREE(Y); RXFREE(Z);
    return(1);
}


int PolyPrint(const char*a, const VECTOR*p,const int c) {
    int k;
    /* if(!g_TRACE) return 0; */
    printf(" %s",a);
    for(k=0;k<c;k++) printf(" %d\t%15.6f\t%15.6f\t%15.6f\n",k, p[k].x,p[k].y,p[k].z);
    return 1;
}

