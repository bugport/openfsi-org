/* RelaxII source code
 * Copyright Peter Heppel Associates 1994
 * 
 * written by 	A K Molyneaux
 * 		P Heppel
 *
 * 
 *
 * Modified :
 Oct 2002 str n cpy removed
 * Oct 96  the PC_AEROMIK entity doesbles for the general ext aero interface
 *	the Kermarec flag tells intpress.c whether its a MIK interface or an Ext interface
 *   29/11/94  aeromik called
 *   28/11/94     PH added 	 Make_Aeromik_Geometry
 *	14.11.94		Adam Molyneaux	changed memory alloc to ours eg. RXFREE.
 *						changed STRDUP to STRDUP
 */

#include "StdAfx.h"
#include "RXGraphicCalls.h"
#include "RXSail.h"
#include "RXEntityDefault.h"
#include "RelaxWorld.h" 
 
#include "vectors.h"
#include "entities.h"
#include "stringutils.h"
#include "mikfort.h"
#include "pansin.h"
#include "RX_FESite.h"
#include "aeromik.h"


 
	void print_mik_file_(char* filename,double*top,double*bot,int*NumOfStripes,int*fair) {
	  char s[256];
	  sprintf(s,"DUMMY AEROMIK called with filename=<%s> top=%f bot=%f Num=%d fair=%d",filename,*top,*bot,*NumOfStripes,*fair);
	  g_World->OutputToClient(s,1);
	}

int Make_Aeromik_Geometry(RXEntity_p e){

/* NOTES. 
	CALLs Print_Mik_File which assumes that the models are loaded in FORTRAN 

*/
	 char*a;
	struct PC_AEROMIK *np; 
	char buf[256];
	if (!strieq(e->type(),"aeromik")) return(0);
 	np = (PC_AEROMIK*)(e->dataptr);
	memset(buf,' ',sizeof(char)*256);
	strcpy(buf,np->mikfilename); // was strncpy
	print_mik_file_(buf,&(np->top),&(np->bottom),&(np->nsects),&(np->fair));/*,256L);  */


	 a = getenv("AEROMIK_PROGRAM");
	 if(!a) {
	 	e->OutputToClient(" No external aerodynamic program (AEROMIK_PROGRAM) enabled",2);
		}
	else
		if(system(a))
		 e->OutputToClient(" Please check external aerodynamic program definition",2);

 return(1);
}



 int Aeromik_V(SAIL *theSail, RXEntity_p ae, const RXEntity_p pe,double u, int level,float*v)
 {

 /* designed to be called when reading Miks pressure files to generate a v value
    because Mik leaves them as zero 
    Uses a similar method to Compute_Stripes.c (*/
#ifndef WIN32
	struct PC_AEROMIK *ap; 
	struct PC_PANSAIL *pp;
	double ule,vle,ute,vte,v2;
	double grad,z;
	ON_3dPoint r,srot;
#endif
	if (!strieq((*ae).type(),"aeromik")) return(0);
	if (!strieq((*pe).type(),"pansail")) return(0);
#ifdef WIN32
	theSail->OutputToClient(" this routine NOT implemented in MSW",1);
	return(0);
#else
 	ap = (PC_AEROMIK*)ae->dataptr;
	pp = (PC_PANSAIL *)pe->dataptr;

	z = ap->bottom + (double) (level)/(double) (ap->nsects) * (ap->top - ap->bottom);


 /* 
	   find v value at leading edge
	   find v value - trailing edge
	   interpolate v value from u value
	   return it
 */
	  vle = (float)0.0;
	  ule = (float)0.0;
	  do {
		  v2 = vle + 0.001;
		  r=      theSail->CoordsAt(ON_2dPoint(ule,v2),RX_GLOBAL);//	Get_Global_Coords( ule,v2,theSail ,r);
		  srot =theSail->CoordsAt(ON_2dPoint(ule,vle),RX_GLOBAL);// Get_Global_Coords( ule,vle,theSail ,srot);
		  if(fabs(r.z-srot.z)>FLOAT_TOLERANCE) {
			  grad = 0.001/(r.z-srot.z);
		  }
		  else {
		  	grad = 1.0/50.0;
			g_World->OutputToClient ("bad luff interpolation",2);
			}
		  vle = vle + grad * (z-srot.z);

	   }while(fabs(srot.z-z) > 0.001);

	
	  vte = vle;
	  ute = (float)1.0;
	  do {
		  v2 = vte + 0.001;
		  r    =theSail->CoordsAt(ON_2dPoint(ute,v2),RX_GLOBAL);//Get_Global_Coords( ute,v2 ,theSail ,r);
		  srot =theSail->CoordsAt(ON_2dPoint(ute,vte),RX_GLOBAL);//Get_Global_Coords( ute,vte,theSail ,srot);
		  if(fabs(r.z-srot.z)>FLOAT_TOLERANCE) {
			  grad = 0.001/(r.z-srot.z);
		  }
		  else {
		  	grad = 1.0/50.0;
			g_World->OutputToClient ("bad luff interpolation",2);
			}
		  vte = vte + grad * (z-srot.z);

	   }while(fabs(srot.z-z) > 0.001);
   
   	   *v = vle*(1.0-u) + vte*u;
   return(1);
#endif
 }
 
int Resolve_Aeromik_Card(RXEntity_p e) {
  
  /* 
    aeromik	mikfile.txt	1.67	32.1	16	-5	main	pres.mai	genoa	pres.gen	
    
    */
  char mikfilename[256],s2[64],s3[64],s4[64],s5[64];
  char*dum;
  int k,retval=0;
  int cnt;
  cout<<"Resolve_Aeromik_Card disabled: "<<e->name()<<endl;
#ifdef HOOPS
  if(HC_Parse_String(e->GetLine(),":" ,1,mikfilename)) {

    if(HC_Parse_String(e->GetLine(),":" ,2,s2)) {
      if(HC_Parse_String(e->GetLine(),":" ,3,s3)) {
	if(HC_Parse_String(e->GetLine(),":" ,4,s4) ){
	  if(HC_Parse_String(e->GetLine(),":" ,5,s5)) {
	    struct PC_AEROMIK *p = (PC_AEROMIK*)CALLOC(1,sizeof(struct PC_AEROMIK));
	//    if (p==NULL) {g_World->OutputToClient ("out of memory",2); return(0);}
	    p->mikfilename=STRDUP(mikfilename);
		p->Kermarec = 1;
	    p->bottom = strtod(s2,&dum);
	    p->top= strtod(s3,&dum);
	    p->nsects = atoi(s4);
	    p->fair = atoi(s5);
	    
	    k = 6;
	    cnt=0;
	    while(HC_Parse_String(e->GetLine(),":" ,k,s3)) {

	      if(!HC_Parse_String(e->GetLine(),":" ,k+1,s4))
		break;
	      cnt++;
	      k += 2;
	      p->files=(char **)REALLOC(p->files,(cnt+1) *sizeof(char*));
	      p->sailnames=(char **)REALLOC(p->sailnames,(cnt+1) *sizeof(char*));
	      PC_Strip_Leading(s3);
	      PC_Strip_Trailing(s3);
	      PC_Strip_Leading(s4);
	      PC_Strip_Trailing(s4);
	      p->sailnames[cnt-1]= STRDUP(s3);
	      p->sailnames[cnt] = NULL;
	      p->files[cnt-1]= STRDUP(s4);
	      p->files[cnt] = NULL;    /* marks end of array */
	   
	    }
	    e->PC_Finish_Entity("aeromik",mikfilename,p,(long)0,NULL," ",e->GetLine());
	    
	    e->Needs_Resolving=0;
	    e->Needs_Finishing=0;
	    e->SetNeedsComputing(0);
	    retval=1;
	    
	  }
	}
      }
    }
  }
#endif
  return(retval);
}
